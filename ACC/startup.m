% ADVANCED CELL CLASSIFIER (ACC) MAIN SCRIPT
%
% A user-friendly solution to classify and analyse your cells.
%
% PROJECT LEADER:
%       Peter Horvath
%
% MAIN DEVELOPERS:
%       Tamas Balassa
%       Filippo Piccinini
%       Csaba Molnar
%       Lassi Paavolainen
%       Abel Szkalisity
%       Attila Beleon
%
% PROJECT WEBSITE:
%       http://www.cellclassifier.org/
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2016 Peter Horvath
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% close all previous windows
clearvars -global CommonHandles;
delete(findall(0,'Type','figure'));

% Path declaration
[ACCPATH, ~, ~] = fileparts(mfilename('fullpath'));
addpath(genpath(fullfile(ACCPATH(1:end-3),'ACC')));
addpath(genpath(fullfile(ACCPATH(1:end-3),'LIBS')));

%Set image processing toolbox to not to display axes with imshow
if checkToolboxByName('Image Processing Toolbox')
    iptsetpref('ImshowAxesVisible','off')
end

% for the resolution on mac
if ismac
    set(groot,'ScreenPixelsPerInch',96);
end

% Main Graphical User Interface (GUI) to play the software
ACC_main_GUI()