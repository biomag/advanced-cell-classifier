function varargout = ACC_main_GUI(varargin)
% ACC_MAIN_GUI M-file for ACC_main_GUI.fig
%      ACC_MAIN_GUI, by itself, creates a new ACC_MAIN_GUI or raises the existing
%      singleton*.
%
%      H = ACC_MAIN_GUI returns the handle to a new ACC_MAIN_GUI or the handle to
%      the existing singleton*.
%
%      ACC_MAIN_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ACC_MAIN_GUI.M with the given input arguments.
%
%      ACC_MAIN_GUI('Property','Value',...) creates a new ACC_MAIN_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ACC_main_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ACC_main_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ACC_main_GUI

% Last Modified by GUIDE v2.5 04-Nov-2023 00:47:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ACC_main_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @ACC_main_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ACC_main_GUI is made visible.
function ACC_main_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ACC_main_GUI (see VARARGIN)

% Choose default command line output for ACC_main_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ACC_main_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

TitleImage = imread(['img' filesep 'title.bmp']);
imagesc(TitleImage, 'Parent', handles.mainView);

CommonHandles.VersionInfo = 2.12;
% initialize SALT, open the ini file, find SALT path and add to MatLab
% search path
% initialize SALT and get the list of classifiers
CommonHandles.SALT.initialized = 0;
CommonHandles = parseIniFile(CommonHandles);
if CommonHandles.SALT.initialized
    sacInit(CommonHandles.SALT.folderName);
    %disp('Suggest a learner toolbox (SALT) successfully initialized');
    global CommonHandles; %#ok<*REDEF> %redef is required because sacInit destroys all global variables;
    CommonHandles.ClassifierNames = sacGetSupportedList('classifiers');
else
    CommonHandles.ClassifierNames = {'LibSVM' 'Neural Network' 'Logistic.Weka' ...
        'MultilayerPerceptron.Weka' 'RBFNetwork.Weka' 'SimpleLogistic.Weka' 'SMO.Weka' 'RandomCommittee.Weka' 'RandomForest.Weka' ...
        'BayesNet.Weka' 'NaiveBayes.Weka' 'AdaBoostM1.Weka' 'Bagging.Weka' 'Dagging.Weka' 'END.Weka' 'EnsembleSelection.Weka' ...
        'LogitBoost.Weka' 'BFTree.Weka' 'FT.Weka' 'J48.Weka' 'RandomTree.Weka' 'REPTree.Weka'};
end

%Add Mulan for regression (Weka extention for multiple target)
[ACCPATH, ~, ~] = fileparts(mfilename('fullpath'));
javaaddpath(fullfile(ACCPATH(1:end-3),'LIBS','mulan','Clus.jar'));
javaaddpath(fullfile(ACCPATH(1:end-3),'LIBS','mulan','mulan.jar'));
CommonHandles.ACCPATH = ACCPATH;
CommonHandles.TrackingPath = [];
CommonHandles.AlignPath = [];
commonHandlesVersionControl(CommonHandles, handles);

disablePrediction(1,handles);

set(handles.figure1, 'Name', 'Advanced Cell Classifier');
set(handles.mainView, 'XTickLabel', []); set(handles.mainView, 'YTickLabel', []);


% --- Outputs from this function are returned to the command line.
function varargout = ACC_main_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(~, ~, ~)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

set(0,'DefaultUicontrolBackgroundColor',[0.9412    0.9412    0.9412]);
set(0,'defaultUicontrolFontSize',8);

dir_list = dir(['img' filesep 'type*.bmp']);

offset = 10;

if exist(['img' filesep 'type.txt'])
    ftNamesTxt = fopen(['img' filesep 'type.txt'], 'r');
    customFTNames = 1;
else
    customFTNames = 0;
end


% --- Executes during object creation, after setting all properties.
function mainView_CreateFcn(~, ~, ~)
% hObject    handle to mainView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate mainView


% --- Executes on mouse press over axes background.
function mainView_ButtonDownFcn(~, ~, ~)
% hObject    handle to mainView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonDownFcn(hObject, ~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% position of pointer in coordinates of mainView axes
pt = get(handles.mainView,'CurrentPoint');

% limits of actual axes
xLimits = xlim(handles.mainView);
yLimits = ylim(handles.mainView);

% check if the click happened on the main image (between axes limits)
if (pt(1, 1) > xLimits(1)) && (pt(1, 1) < xLimits(2)) ...
        && (pt(1, 2) > yLimits(1)) && (pt(1, 2) < yLimits(2)) ...
        && (CommonHandles.Success == 1) && (CommonHandles.RandomTrain ~= 2)
    
    selType = get(hObject,'SelectionType');
    if strcmp(selType,'normal')
        % select closest point
        dist = (pt(1, 1) + CommonHandles.MainView.XBounds(1) - CommonHandles.SelectedMetaData(:, 1)).^2 + (pt(1, 2) + (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)) - CommonHandles.SelectedMetaData(:, 2)).^2;
        [~, CommonHandles.SelectedCell] = min(dist);
        % refresh view
        CommonHandles = showSelectedCell(CommonHandles);
    else
        CommonHandles.DragImage.State = 1;
        CommonHandles.DragImage.OriginalPosition = pt(1,1:2);
        set(gcf,'Pointer','fleur');
    end
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        button = questdlg('Save workspace before quit?','Save','Save','Quit','Cancel','Save');
        if strcmp(button, 'Save')
            toolbarSaveProject_ClickedCallback(hObject, eventdata, handles);
        elseif strcmp(button, 'Cancel')
            return;
        end
    end
end

if isfield(CommonHandles,'PlateSelectWindow')
    delete(CommonHandles.PlateSelectWindow);
end

delete(hObject);
clear all;
close all;


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, ~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% resize function
% do not allow to resize it smaller than half binned image (1392, 1040)
minSize = [946 620];
posRect = get(hObject, 'Position');
set(groot,'Units','pixels');
screensize = get( groot, 'Screensize' );
if CommonHandles.Success &&  posRect(3) == screensize(3)
    if isfield(CommonHandles,'PlateSelectWindow')
        posRect(3) = posRect(3) - CommonHandles.PlateSelectWindow.Position(3);
        CommonHandles.PlateSelectWindow.Position(1) = posRect(1)+posRect(3);
        CommonHandles.PlateSelectWindow.Position(2) = posRect(2);
        CommonHandles.PlateSelectWindow.Position(4) = posRect(4);
        select_plate_wellGUI('figure1_SizeChangedFcn',CommonHandles.PlateSelectWindow,[],guidata(CommonHandles.PlateSelectWindow));
    end
end

if posRect(4) < minSize(2) || posRect(3) < minSize(1)
    posRect(1) = 50;
    posRect(2) = 50;
    posRect(3) = minSize(1);
    posRect(4) = minSize(2);
end

set(hObject, 'Position', posRect);
posRect = get(hObject, 'Position');

% Aling right controls to the edge
% main window
if isfield(CommonHandles, 'MainViewHandle')
    mainRect = get(CommonHandles.MainViewHandle, 'Position');
    set(CommonHandles.MainViewHandle, 'Position', [mainRect(1) mainRect(2) posRect(3)-150 posRect(4)-100]);
    set(handles.windowCellView, 'Position', [(posRect(3) - 135) / posRect(3) 0.01 (130 / posRect(3)) 0.98]);
    subRect = get(handles.windowCellView, 'Position');
end


% --- Executes on scroll wheel click while the figure is in focus.
% --- Zooming in and out on main selected image
function figure1_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

if isfield(CommonHandles,'CurrentImage')
    if ~isempty(CommonHandles.CurrentImage)
        XBounds = CommonHandles.MainView.XBounds;
        YBounds = CommonHandles.MainView.YBounds;
        
        windowPosition = get(hObject, 'Position');
        
        mainViewBoundaries = getpixelposition(handles.mainView);
        pos = get(gcf,'CurrentPoint');
        
        % mouse is over the main window
        if pos(1)>0 && pos(2)>0 && pos(1)<windowPosition(3) && pos(2)<windowPosition(4)
            
            if mainViewBoundaries(4)/mainViewBoundaries(3) > CommonHandles.CurrentImageSizeX/CommonHandles.CurrentImageSizeY
                ratio = mainViewBoundaries(3)/CommonHandles.CurrentImageSizeY;
            else
                ratio = mainViewBoundaries(4)/CommonHandles.CurrentImageSizeX;
            end
            
            % pictureBoundaries stores the position of displayed image in the figure coordinate system in pixels
            pictureBoundaries(3) = CommonHandles.CurrentImageSizeY * ratio;
            pictureBoundaries(4) = CommonHandles.CurrentImageSizeX * ratio;
            pictureBoundaries(1) = (mainViewBoundaries(3)-pictureBoundaries(3))/2 + mainViewBoundaries(1);
            pictureBoundaries(2) = (mainViewBoundaries(4)-pictureBoundaries(4))/2 + mainViewBoundaries(2);
            
            if eventdata.VerticalScrollCount>0 % we're zooming out
                newSizeX = (XBounds(2) - XBounds(1))*1.2;
                newSizeY = (YBounds(2) - YBounds(1))*1.2;
                if newSizeX>CommonHandles.CurrentImageSizeY-1 newSizeX = CommonHandles.CurrentImageSizeY-1; end
                if newSizeY>CommonHandles.CurrentImageSizeX-1 newSizeY = CommonHandles.CurrentImageSizeX-1; end
            else % we're zooming in
                % Check the size limit of cropped image to avoid big changes in
                % the size of the main view
                if (XBounds(2)-XBounds(1))*(YBounds(2)-YBounds(1))>2500
                    newSizeX = (XBounds(2) - XBounds(1))*0.85;
                    newSizeY = (YBounds(2) - YBounds(1))*0.85;
                else
                    newSizeX = (XBounds(2) - XBounds(1));
                    newSizeY = (YBounds(2) - YBounds(1));
                end
            end
            calcXYBounds(newSizeX, newSizeY, pos, pictureBoundaries);
            CommonHandles = showSelectedCell(CommonHandles);
        end
        
    end
end

function calcXYBounds(newSizeX, newSizeY, pos, pictureBoundaries)
%CALCXYBOUNDS Calculates and stores in CommonHandles the new bounds of the
% cropped image part to be displayed in the main view.
%   INPUTS:
%       newSizeX:           the new horizontal size of the cropped part
%       newSizeY:           the new vertical size of the cropped part
%       pos:                the horizontal and vertical position of the
%                           cursor in main figure coordinate system in pixels
%       pictureBoundaries:  the position of the displayed image in main
%                           figure coordinate system in pixels in format
%                           [horizontalOfBottomLeft verticalOfBottomLeft
%                           width height].

global CommonHandles;
XBounds = CommonHandles.MainView.XBounds;
YBounds = CommonHandles.MainView.YBounds;

% Store the cursor position in image pixel coordinates
posXInRegPlane = (pos(1) - pictureBoundaries(1))/pictureBoundaries(3)*(XBounds(2) - XBounds(1))+XBounds(1);
posYInRegPlane = (pos(2) - pictureBoundaries(2))/pictureBoundaries(4)*(YBounds(2) - YBounds(1))+YBounds(1);

XNewLower = posXInRegPlane - (pos(1)-pictureBoundaries(1))*newSizeX./pictureBoundaries(3);
if XNewLower<1
    XNewLower = 1;
end
XNewUpper = XNewLower + newSizeX;
if XNewUpper>CommonHandles.CurrentImageSizeY
    XNewUpper = CommonHandles.CurrentImageSizeY;
    XNewLower = XNewUpper - newSizeX;
end
YNewLower = posYInRegPlane - (pos(2)-pictureBoundaries(2))*newSizeY./pictureBoundaries(4);
if YNewLower<1
    YNewLower = 1;
end
YNewUpper = YNewLower + newSizeY;
if YNewUpper>CommonHandles.CurrentImageSizeX
    YNewUpper = CommonHandles.CurrentImageSizeX;
    YNewLower = YNewUpper - newSizeY;
end

CommonHandles.MainView.XBounds = [ XNewLower XNewUpper ];
CommonHandles.MainView.YBounds = [ YNewLower YNewUpper ];

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject, ~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

if isfield(CommonHandles,'DragImage') && CommonHandles.DragImage.State
    posTemp = get(handles.mainView,'CurrentPoint');
    pos = posTemp(1,1:2);
    windowPosition = get(hObject, 'Position');
    posDiff = pos - CommonHandles.DragImage.OriginalPosition;
    XBounds = CommonHandles.MainView.XBounds;
    YBounds = CommonHandles.MainView.YBounds;
    
    %get the picture size
    mainViewBoundaries = getpixelposition(handles.mainView);
    
    if pos(1)>0 && pos(2)>0 && pos(1)<windowPosition(3) && pos(2)<windowPosition(4)
        if mainViewBoundaries(4)/mainViewBoundaries(3) > CommonHandles.CurrentImageSizeX/CommonHandles.CurrentImageSizeY
            ratio = mainViewBoundaries(3)/CommonHandles.CurrentImageSizeY;
        else
            ratio = mainViewBoundaries(4)/CommonHandles.CurrentImageSizeX;
        end
        
        % pictureBoundaries stores the position of displayed image in the figure coordinate system in pixels
        pictureBoundaries(3) = CommonHandles.CurrentImageSizeY * ratio;
        pictureBoundaries(4) = CommonHandles.CurrentImageSizeX * ratio;
        pictureBoundaries(1) = (mainViewBoundaries(3)-pictureBoundaries(3))/2 + mainViewBoundaries(1);
        pictureBoundaries(2) = (mainViewBoundaries(4)-pictureBoundaries(4))/2 + mainViewBoundaries(2);
        
        posYDiffRegPlane = posDiff(2)*(YBounds(2)-YBounds(1))./pictureBoundaries(4)./10;
        posXDiffRegPlane = posDiff(1)*(XBounds(2)-XBounds(1))./pictureBoundaries(3)./10;
        
        if max(round(XBounds - posXDiffRegPlane))<CommonHandles.CurrentImageSizeY && min(round(XBounds - posXDiffRegPlane))>0
            CommonHandles.MainView.XBounds = XBounds - posXDiffRegPlane;
        end
        if max(round(YBounds + posYDiffRegPlane))<CommonHandles.CurrentImageSizeX && min(round(YBounds + posYDiffRegPlane))>0
            CommonHandles.MainView.YBounds = YBounds + posYDiffRegPlane;
        end
        CommonHandles = showSelectedCell(CommonHandles);
    end
end

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(~, ~,~)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

CommonHandles.DragImage.State = 0;
set(gcf,'Pointer','arrow');


%-------------------------------------------------------------------------
%% START: FILE MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


function toolbarNewProject_ClickedCallback(~, ~, handles)
% NEWPROJECT pushtool
% hObject    handle to toolbarNewProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

%save the previous data path (if exist) if the selection fails.
if isfield(CommonHandles,'DirName')
    oldPath = CommonHandles.DirName;
end

%clear batch
clearvars -global bgAL;

% ask user for the data set
[~,guiMessage] = load_data_setGUI;

if (exist('CommonHandles','var') && CommonHandles.Success == 1)
    if strcmp(guiMessage,'OK')
        recoverGUI(handles);
        CommonHandles.PlateSelectWindow = select_plate_wellGUI();
    else
        CommonHandles.DirName = oldPath;
    end
end


% --------------------------------------------------------------------
function toolbarOpenProject_ClickedCallback(~, ~, handles)
%OPENPROJECT PUSHTOOL
% hObject    handle to toolbarOpenProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

runningCommonHandles = CommonHandles;

if isfield(CommonHandles, 'DirName')
    [fileOpen,pathOpen] = uigetfile(fullfile(CommonHandles.DirName,'*.mat'),'Select workspace');
else
    [fileOpen,pathOpen] = uigetfile('*.mat','Select workspace');
end

if ~isequal(fileOpen,0)
    clear('CommonHandles');
    load([pathOpen fileOpen], 'CommonHandles');
    
    % enable the prediction if a model exists in the loaded file
    disablePrediction(0,handles);
    
    %check whether the used data path exists
    if exist('CommonHandles', 'var') && ...
            isfield(CommonHandles, 'DirName') && ...
            isfield(CommonHandles, 'ImageFolder') && ...
            isfield(CommonHandles, 'MetaDataFolder') && ...
            isfield(CommonHandles, 'OriginalImageFolder')
        errorCode = checkProjectFolder(CommonHandles.DirName,0,CommonHandles.ImageFolder,CommonHandles.MetaDataFolder,CommonHandles.OriginalImageFolder);
    else
        CommonHandles = runningCommonHandles;
        errordlg('The project file is corrupted or invalid!')
        return
    end
    tryAgain = 1;
    while errorCode && tryAgain
        
        switch errorCode
            case 1
                errorText = 'The provided project folder does not contain any subfolders (plates).';
            case 2
                errorText = 'None of the plate folders are valid.';
            case 3
                errorText = 'The project folder does not contain all the plates listed in the project.';
            case 4
                errorText = 'The project folder does not exist.';
            otherwise
                errorText = 'Misterious error during load!';
        end
        tryAgain = 0;
        choice = questdlg(sprintf('%s\n Do you want to specify another path?', errorText),'Wrong path','Yes','No','Yes');
        switch choice
            case 'Yes'
                tryAgain = 1;
            case 'No'
                tryAgain = 0;
        end
        if tryAgain
            CommonHandles.DirName = uigetdir(CommonHandles.DirName,'Specify a new data path!');
            if ~isequal(CommonHandles.DirName,0)
                errorCode = checkProjectFolder(CommonHandles.DirName,0,CommonHandles.ImageFolder,CommonHandles.MetaDataFolder,CommonHandles.OriginalImageFolder);
            else
                errorCode = 4;
                tryAgain = 0;
            end
        end
    end
    
    if ~errorCode
        try
            deleteClassButtons(runningCommonHandles);
            commonHandlesVersionControl(runningCommonHandles,handles);
            recoverGUI(handles);
            
            clearvars -global bgAL;
            
            CommonHandles.PlateSelectWindow = select_plate_wellGUI();
            refreshClassesList;
        catch e
            warning(getReport(e));
            CommonHandles = runningCommonHandles;
        end
    else
        CommonHandles = runningCommonHandles;
    end
    
end


% --------------------------------------------------------------------
function toolbarSaveProject_ClickedCallback(~, ~, ~)
% SAVEPROJECT_PUSHTOOL
% hObject    handle to toolbarSaveProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        [SaveFile, SavePath] = uiputfile('*.mat','Save Plateset As');
        CommonHandles.ProjectName = SaveFile;
        if ~isequal(SaveFile,0)
            CurrentOriginalImage = CommonHandles.CurrentOriginalImage;
            CurrentImage = CommonHandles.CurrentImage;
            CommonHandles.CurrentOriginalImage = [];
            CommonHandles.CurrentImage = [];
            
            CommonHandlesToReserve = CommonHandles;
            
            %Clear the saved GUI objects, we'll use the old matlab GUI savings.
            CommonHandles = clearGUIhandles(CommonHandles);
            
            if ~(isempty(CommonHandles.AllFeatures) && isempty(CommonHandles.AllFeaturesMapping))
                saveSamplingPoolChoice = questdlg('Would you like to save the loaded sampling pool? It could take for a long time. And also deletes the sampling pool.','Save sampling pool','Save sampling pool','Do not save','Do not save');
                
                if ~strcmp(saveSamplingPoolChoice,'Save sampling pool')
                    CommonHandles.AllFeatures = {};
                    CommonHandles.AllFeaturesMapping = {};
                    CommonHandles.ActiveLearning = 0; %if sampling pool is deleted then turn off Active learnings
                    CommonHandles.ActiveRegression.on = 0;
                end
            end
            
            varToSave = 'CommonHandles'; %#ok<NASGU> Used in the script below.
            saveVarScript;
            
            CommonHandles = CommonHandlesToReserve;
            set(CommonHandles.MainWindow, 'Name', ['Advanced Cell Classifier - ' CommonHandles.ProjectName] );
            CommonHandles.CurrentOriginalImage = CurrentOriginalImage;
            CommonHandles.CurrentImage = CurrentImage;
        end;
    end
end


% --------------------------------------------------------------------
function menuFile_Callback(hObject, eventdata, handles)
% hObject    handle to menuFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuNewProject_Callback(hObject, eventdata, handles)
% hObject    handle to menuNewProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% New project menu button
toolbarNewProject_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function menuOpenProject_Callback(hObject, eventdata, handles)
% hObject    handle to menuOpenProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Open project menu button
toolbarOpenProject_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function menuSaveProject_Callback(hObject, eventdata, handles)
% hObject    handle to menuSaveProject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Save project menu
toolbarSaveProject_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function menuChangeSettings_Callback(hObject, eventdata, handles)
% hObject    handle to menuChangeSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        changeSettings_GUI;
        uiwait(gcf);
    end
end;


% --------------------------------------------------------------------
function menuAddPlates_Callback(hObject, eventdata, handles)
% hObject    handle to menuAddPlates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% starts a folder selector and adds a folder to the list

global CommonHandles;

% create string list
% list of the folder containing the plates and exclude already chosen ones
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        DirListStr = [];
        DirList = dir(CommonHandles.DirName);
        DirStr = {DirList.name};
        for i=3:size(DirList, 1)
            if exist([char(CommonHandles.DirName) filesep char(DirStr(i))],'dir')
                DirListStr = [DirListStr; cellstr(DirStr(i))];
            end;
        end;
        
        DirListStr = setdiff(DirListStr, CommonHandles.PlatesNames);
        
        if size(DirListStr,1) > 0
            [SelectedPlates,isOK] = listdlg('PromptString','Select plate(s):', 'Name', 'Add plates', 'SelectionMode','multiple','ListString', DirListStr);
            
            if (isOK)
                if isfield(CommonHandles,'AllFeaturesMapping') && ~isempty(CommonHandles.AllFeaturesMapping)
                    btn = questdlg('Would You like to load the selected plates to the sampling pool?','Yes');
                    if strcmp(btn, 'Yes')
                        loadingFlag = 1;
                    else
                        loadingFlag = 0;
                    end
                else
                    loadingFlag = 0;
                end
                
                for i=1:length(SelectedPlates)
                    addPlate(DirListStr(SelectedPlates(i)),loadingFlag);
                end
                CommonHandles.SelectedPlate = 1;
                CommonHandles.SelectedImage = 1;
                CommonHandles.SelectedCell  = 1;
                set(CommonHandles.PlateListHandle,'Value', CommonHandles.SelectedPlate);
                select_plate_wellGUI;
            end;
        end
    end
end


% --------------------------------------------------------------------
function menuRemovePlates_Callback(~, ~, ~)
% hObject    handle to menuRemovePlates (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if length(CommonHandles.PlatesNames) > 1
            
            [SelectedPlates,isOK] = listdlg('PromptString','Select plate(s):', 'Name', 'Remove plates', 'SelectionMode','multiple','ListString', CommonHandles.PlatesNames);
            
            if (isOK)
                
                if length(SelectedPlates) < length(CommonHandles.PlatesNames)
                    % ask to be sure
                    button = questdlg('The sampling pool will be deleted too. Do you really want to delete the selected plates from the experiment?','Delete...') ;
                    
                    if strcmp(button, 'Yes')
                        
                        %ask what to do with the training set
                        answer = questdlg('Do you want to also delete the training samples from this plate?');
                        if ( strcmp(answer,'Yes') || strcmp(answer,'No'))
                            if strcmp(answer,'Yes')
                                remFromTS = 1;
                            else
                                remFromTS = 0;
                            end
                            
                            for i=length(SelectedPlates):-1:1
                                removePlate(SelectedPlates(i),remFromTS);
                            end
                            CommonHandles.AllFeatures = {};
                            CommonHandles.AllFeaturesMapping = {};
                            CommonHandles.AllCoordinates = {};
                            CommonHandles.SelectedPlate = 1;
                            CommonHandles.SelectedImage = 1;
                            CommonHandles.SelectedCell  = 1;
                            set(CommonHandles.PlateListHandle,'Value', CommonHandles.SelectedPlate);
                            select_plate_wellGUI;
                            refreshClassesList();
                        end
                    end;
                    
                else
                    h = msgbox('It is not possible to delete all the plates...', 'Delete warning', 'warn');
                end;
                
            end;
            
        else
            h = msgbox('This experiment containes not enough plates to delete...', 'Delete warning', 'warn');
        end;
    end
end


% --------------------------------------------------------------------
function menuImportTrainedData_Callback(hObject, eventdata, handles)
% hObject    handle to menuImportTrainedData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% Here one can import already labeled data
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        [matFileName,pathName,~] = uigetfile('*.mat','Select the project file containing labeled data.');
        
        if matFileName ~= 0
            importData = load([pathName matFileName]);
            
            if isfield(importData.CommonHandles,'PlateSelectWindow')
                if ishandle(importData.CommonHandles.PlateSelectWindow)
                    delete(importData.CommonHandles.PlateSelectWindow)
                end
            end
            
            % check dataset comaptibility
            lengthFlag = length(importData.CommonHandles.Classes) == length(CommonHandles.Classes);
            nameFlag = 1;
            if lengthFlag
                for i=1:length(importData.CommonHandles.Classes)
                    if ~strcmp(importData.CommonHandles.Classes{i}.Name, CommonHandles.Classes{i}.Name)
                        nameFlag = 0;
                    end
                end
            end
            
            if lengthFlag && nameFlag
                
                insertCounter = 0;
                
                imgCounter = ones(length(importData.CommonHandles.Classes), 1);
                
                for j=1:length(CommonHandles.Classes)
                    oldClassNames{j} = importData.CommonHandles.Classes{j}.Name;
                end
                
                %save current CommonHandles cell selection
                currentTmpCell = makeTempCellData();
                
                for i=1:length(importData.CommonHandles.TrainingSet.Class)
                    classIndex = find(importData.CommonHandles.TrainingSet.Class{i, :} == 1);
                    
                    % check if the current image is in the dataset
                    alreadyInTrainingSet = isInTrainingSet( importData.CommonHandles.TrainingSet.ImageName{i},...
                        importData.CommonHandles.TrainingSet.CellNumber{i}, 0, CommonHandles, importData.CommonHandles.TrainingSet.Features{i});
                    
                    % insert current cell
                    if ~alreadyInTrainingSet
                        %Set CommonHandles data to the original
                        CommonHandles.SelectedCell = importData.CommonHandles.TrainingSet.CellNumber{i};
                        CommonHandles.SelectedImageName = importData.CommonHandles.TrainingSet.ImageName{i};
                        CommonHandles.SelectedOriginalImageName = importData.CommonHandles.TrainingSet.OriginalImageName{i};
                        CommonHandles.SelectedMetaDataFileName = importData.CommonHandles.TrainingSet.MetaDataFilename{i};
                        
                        saveClassificationData(classIndex,importData.CommonHandles.TrainingSet.Features{i});
                        %Possible regression position load
                        if isfield(importData.CommonHandles.TrainingSet,'RegPos')
                            CommonHandles.TrainingSet.RegPos{end} = importData.CommonHandles.TrainingSet.RegPos{i};
                        end
                        
                        addToClassImagesByObject(importData.CommonHandles.ClassImages(oldClassNames{classIndex}).Images{imgCounter(classIndex)});
                        
                        insertCounter=insertCounter+1;
                        imgCounter(classIndex) = imgCounter(classIndex)+1;
                    end
                end
                jumpToCell(currentTmpCell,0); %reset the ruined CommonHandles fields
                refreshClassesList();
                msgbox([num2str(insertCounter) ' cells were successfully inserted.']);
                
            elseif ~lengthFlag
                warndlg('Number of the classes are not the same!');
            elseif ~nameFlag
                warndlg('The naming of the classes are not consistent!');
            else
                warndlg('Incompatible dataset was selected!');
            end
        end
    end
end


% % --------------------------------------------------------------------
% function menuChangeDataFolder_Callback(hObject, eventdata, handles)
% % hObject    handle to menuChangeDataFolder (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global CommonHandles;
%
% if isfield(CommonHandles, 'Success')
%     if (CommonHandles.Success == 1)
%         newFolder = uigetdir(CommonHandles.DirName);
%
%         if newFolder ~= 0
%             CommonHandles.DirName = newFolder;
%             CommonHandles.SelectedPlate = 1;
%             CommonHandles.SelectedImage = 1;
%             select_plate_wellGUI;
%         end;
%     end
% end


%-------------------------------------------------------------------------
%% END: FILE MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


%-------------------------------------------------------------------------
%% START: VISUALIZATION MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


% --------------------------------------------------------------------
function toolbarCellNumbers_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarCellNumbers, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            if (CommonHandles.Success == 1)
                CommonHandles.ShowCellNumbers = 1;
                CommonHandles = showSelectedCell(CommonHandles);
            end
        else
            if (CommonHandles.Success == 1)
                CommonHandles.ShowCellNumbers = 0;
                CommonHandles = showSelectedCell(CommonHandles);
            end
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarCellNumbers, 'State', 'off');
        else
            set(handles.toolbarCellNumbers, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarCellNumbers, 'State', 'off');
    else
        set(handles.toolbarCellNumbers, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarCellNumbers_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarCellNumbers_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarCellClass_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarCellClass, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            if (CommonHandles.Success == 1)
                CommonHandles.DisplayAnnotatedCells = 1;
                CommonHandles = showSelectedCell(CommonHandles);
            end
        else
            if (CommonHandles.Success == 1)
                CommonHandles.DisplayAnnotatedCells = 0;
                CommonHandles = showSelectedCell(CommonHandles);
            end
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarCellClass, 'State', 'off');
        else
            set(handles.toolbarCellClass, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarCellClass, 'State', 'off');
    else
        set(handles.toolbarCellClass, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarCellClass_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarCellClass_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarCellClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarContours_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarContours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarContours, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            if strcmp(get(handles.toolbarIntensityStretch, 'State'), 'on')
                set(handles.toolbarIntensityStretch, 'State', 'off');
                toolbarIntensityStretch_ClickedCallback(hObject, eventdata, handles);
            end
            CommonHandles.ShowContour = 1;
            if (CommonHandles.Success == 1)
                CommonHandles = showSelectedCell(CommonHandles);
            end
        else
            CommonHandles.ShowContour = 0;
            if (CommonHandles.Success == 1)
                CommonHandles = showSelectedCell(CommonHandles);
            end
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarContours, 'State', 'off');
        else
            set(handles.toolbarContours, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarContours, 'State', 'off');
    else
        set(handles.toolbarContours, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarContours_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarContours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarContours_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarContours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarRedChannel_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarRedChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarRedChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            CommonHandles.MainView.Red = 1;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        else
            CommonHandles.MainView.Red = 0;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarRedChannel, 'State', 'off');
        else
            set(handles.toolbarRedChannel, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarRedChannel, 'State', 'off');
    else
        set(handles.toolbarRedChannel, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarRedChannel_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarRedChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarRedChannel_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarRedChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarGreenChannel_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarGreenChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarGreenChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            CommonHandles.MainView.Green = 1;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        else
            CommonHandles.MainView.Green = 0;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarGreenChannel, 'State', 'off');
        else
            set(handles.toolbarGreenChannel, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarGreenChannel, 'State', 'off');
    else
        set(handles.toolbarGreenChannel, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarGreenChannel_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarGreenChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarGreenChannel_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarGreenChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarBlueChannel_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarBlueChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarBlueChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            CommonHandles.MainView.Blue = 1;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        else
            CommonHandles.MainView.Blue = 0;
            if (CommonHandles.Success == 1)
                showSelectedCell(CommonHandles);
            end;
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarBlueChannel, 'State', 'off');
        else
            set(handles.toolbarBlueChannel, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarBlueChannel, 'State', 'off');
    else
        set(handles.toolbarBlueChannel, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarBlueChannel_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarBlueChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarBlueChannel_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarBlueChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarColorView_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarColorView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarColorView, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            CommonHandles.ColorView = 1;
            if CommonHandles.Success == 1
                CommonHandles = showSelectedCell(CommonHandles);
            end
        else
            CommonHandles.ColorView = 0;
            if CommonHandles.Success == 1
                CommonHandles = showSelectedCell(CommonHandles);
            end
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarColorView, 'State', 'off');
        else
            set(handles.toolbarColorView, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarColorView, 'State', 'off');
    else
        set(handles.toolbarColorView, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarColorView_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarColorView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarColorView_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarColorView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarIntensityStretch_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarIntensityStretch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarIntensityStretch, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            if strcmp(get(handles.toolbarContours, 'State'), 'on')
                set(handles.toolbarContours, 'State', 'off');
                toolbarContours_ClickedCallback(hObject, eventdata, handles);
            end;
            tempVal_toolbarIntensityStretch = 1;
            CommonHandles.MainView.StrechImage = tempVal_toolbarIntensityStretch;
            selectedCell = CommonHandles.SelectedCell;
            CommonHandles = loadSelectedImage(CommonHandles);
            CommonHandles.SelectedCell = selectedCell;
            CommonHandles = showSelectedCell(CommonHandles);
        else
            tempVal_toolbarIntensityStretch = 0;
            CommonHandles.MainView.StrechImage = tempVal_toolbarIntensityStretch;
            selectedCell = CommonHandles.SelectedCell;
            CommonHandles = loadSelectedImage(CommonHandles);
            CommonHandles.SelectedCell = selectedCell;
            CommonHandles = showSelectedCell(CommonHandles);
        end
    else
        if strcmp(StateString, 'on')
            set(handles.toolbarIntensityStretch, 'State', 'off');
        else
            set(handles.toolbarIntensityStretch, 'State', 'on');
        end
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarIntensityStretch, 'State', 'off');
    else
        set(handles.toolbarIntensityStretch, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarIntensityStretch_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbarIntensityStretch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarIntensityStretch_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbarIntensityStretch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarColorViewSet_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarColorViewSet (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        try
            if strcmp(get(handles.toolbarIntensityStretch, 'State'), 'on')
                set(handles.toolbarIntensityStretch, 'State', 'off');
                toolbarIntensityStretch_ClickedCallback(hObject, eventdata, handles);
            end;
            if strcmp(get(handles.toolbarContours, 'State'), 'on')
                set(handles.toolbarContours, 'State', 'off');
                toolbarContours_ClickedCallback(hObject, eventdata, handles);
            end;
            uiwait(setIntensityGUI);
        catch ME
            warning('Color view set can not be performed');
        end
    end
end


% --------------------------------------------------------------------
function menuVisualization_Callback(hObject, eventdata, handles)
% hObject    handle to menuVisualization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuCellNumbers_Callback(hObject, eventdata, handles)
% hObject    handle to menuCellNumbers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarCellNumbers, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarCellNumbers, 'State', 'off');
        else
            set(handles.toolbarCellNumbers, 'State', 'on');
        end
        toolbarCellNumbers_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuCellClasses_Callback(hObject, eventdata, handles)
% hObject    handle to menuCellClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarCellClass, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarCellClass, 'State', 'off');
        else
            set(handles.toolbarCellClass, 'State', 'on');
        end
        toolbarCellClass_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuContours_Callback(hObject, eventdata, handles)
% hObject    handle to menuContours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarContours, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarContours, 'State', 'off');
        else
            set(handles.toolbarContours, 'State', 'on');
        end
        toolbarContours_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuRedChannel_Callback(hObject, eventdata, handles)
% hObject    handle to menuRedChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarRedChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarRedChannel, 'State', 'off');
        else
            set(handles.toolbarRedChannel, 'State', 'on');
        end
        toolbarRedChannel_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuGreenChannel_Callback(hObject, eventdata, handles)
% hObject    handle to menuGreenChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarGreenChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarGreenChannel, 'State', 'off');
        else
            set(handles.toolbarGreenChannel, 'State', 'on');
        end
        toolbarGreenChannel_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuBlueChannel_Callback(hObject, eventdata, handles)
% hObject    handle to menuBlueChannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarBlueChannel, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarBlueChannel, 'State', 'off');
        else
            set(handles.toolbarBlueChannel, 'State', 'on');
        end
        toolbarBlueChannel_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuImageColours_Callback(hObject, eventdata, handles)
% hObject    handle to menuImageColours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarColorView, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarColorView, 'State', 'off');
        else
            set(handles.toolbarColorView, 'State', 'on');
        end
        toolbarColorView_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuStretchImageIntensity_Callback(hObject, eventdata, handles)
% hObject    handle to menuStretchImageIntensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarIntensityStretch, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarIntensityStretch, 'State', 'off');
        else
            set(handles.toolbarIntensityStretch, 'State', 'on');
        end
        toolbarIntensityStretch_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuColorsSetting_Callback(hObject, eventdata, handles)
% hObject    handle to menuColorsSetting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        toolbarColorViewSet_ClickedCallback(hObject, eventdata, handles)
    end
end


%-------------------------------------------------------------------------
%% END: VISUALIZATION MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


%-------------------------------------------------------------------------
%% START: Annotation MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


% --------------------------------------------------------------------
% pushtool for creating classes, placed on menubar
function toolbarCreateClass_ClickedCallback(~, ~, ~)
% hObject    handle to toolbarCreateClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        CommonHandles.HC.Node.createClass = 0;
        uiwait(createClasses_GUI);
    end
end


% --------------------------------------------------------------------
% pushtool for deleting a class, placed on menubar
function toolbarDeleteClass_ClickedCallback(~, ~, ~)
% hObject    handle to toolbarDeleteClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if isfield(CommonHandles,'Classes')
    if ~isempty(CommonHandles.Classes)
        uiwait(deleteClass_GUI);
    end
end


% --------------------------------------------------------------------
% pushtool for loading saved classes, placed on menubar
function toolbarLoadClasses_ClickedCallback(~, ~, ~)
% hObject    handle to toolbarLoadClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        [classesFile, classesPath] = uigetfile('*.mat','Load classes (.mat file)');
        if ischar(classesFile)
            if isempty(CommonHandles.Classes)
                button = 'Yes';
            else
                % if there is already existing Classes, delete them or skip loading new one
                button = questdlg('Do you want to remove/replace all existing classes?','Load classes','Yes','No','No');
            end
            if strcmp(button,'Yes')
                
                CommonHandles.Classes = {};
                %Clear out the complete training set
                deleteFromTrainingSet(-1);
                
                %classesFile = get(handles.classesPath, 'String');
                loadClasses(classesPath, classesFile);
                closeClassContainingFigures();
            else
                disp('Do not remove classes');
            end
        end
    end
end


% --------------------------------------------------------------------
% pushtool for saving the current classes, placed on menubar
% saved file can be find in Classes as Classes.mat
% TODO set the path and filename
function toolbarSaveClasses_ClickedCallback(~, ~, ~)
% hObject    handle to toolbarSaveClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%uiwait(saveClasses_GUI);
global CommonHandles;

if isfield(CommonHandles,'Classes')
    if ~isempty(CommonHandles.Classes)
        [classesFile, classesPath] = uiputfile('Classes.mat','Save classes (.mat file)');
        if ischar(classesFile)
            tmp_classes = CommonHandles.Classes;
            save([classesPath, classesFile],'tmp_classes');
        end
    end
end


% --------------------------------------------------------------------
function toolbarRandomAnnotation_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarRandomAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarRandomAnnotation, 'State');
if (isfield(CommonHandles, 'Success') && (CommonHandles.Success == 1) && isfield(CommonHandles,'Classes') && ~isempty(CommonHandles.Classes) )
    if strcmp(StateString, 'on')
        set(handles.toolbarActiveLearningRun,'State','off');
        toolbarActiveLearningRun_ClickedCallback(handles.toolbarActiveLearningRun,0,handles);
        set(handles.toolbarActiveRegression,'State','off');
        toolbarActiveRegression_ClickedCallback(handles.toolbarActiveRegression,0,handles);
        CommonHandles.RandomTrain = 1;
    else
        CommonHandles.RandomTrain = 0;
    end
else
    if strcmp(StateString, 'on')
        set(handles.toolbarRandomAnnotation, 'State', 'off');
    else
        set(handles.toolbarRandomAnnotation, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarRandomAnnotation_OffCallback(~, ~, ~)
% hObject    handle to toolbarRandomAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarRandomAnnotation_OnCallback(~, ~, ~)
%RANDOM train toggle tool
% hObject    handle to toolbarRandomAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarFindSimilarCellsRun_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarFindSimilarCellsRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
menuFindSimilarCellsRun_Callback(hObject, eventdata, handles);

% --------------------------------------------------------------------
function toolbarActiveLearningRun_ClickedCallback(~, eventdata, handles)
% hObject    handle to toolbarActiveLearningRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(handles.toolbarActiveLearningRun, 'State');
if ( isfield(CommonHandles, 'Success') && CommonHandles.Success == 1 && isfield(CommonHandles, 'TrainingSet')) && isnumeric(CommonHandles.SelectedClassifier)
    %changed off AL
    if strcmp(StateString, 'off')
        CommonHandles.ActiveLearning = 0;
    else
        
        %check for the sampling pool.
        if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
            uiwait(msgbox('It is necessary to generate a sampling pool to use active learning!','Request to generate sampling pool'));
            uiwait(loadAllFeaturesGUI());
        end
        %if the sampling pool generation was succesful
        if (isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures))
            %if AL doesn't exist popup the AL_settings
            if (~isfield(CommonHandles,'AL') || ~isa(CommonHandles.AL,'ActiveLearner'))
                uiwait(AL_GUI);
            end
            
            if (isfield(CommonHandles,'AL') && isa(CommonHandles.AL,'ActiveLearner'))
                %if it is successful to start AL, then change off random.
                if (CommonHandles.RandomTrain)
                    set(handles.toolbarRandomAnnotation,'State','off');
                    toolbarRandomAnnotation_ClickedCallback(handles.toolbarRandomAnnotation, eventdata, handles);
                end
                if (CommonHandles.ActiveRegression.on)
                    set(handles.toolbarActiveRegression,'State','off');
                    toolbarActiveRegression_ClickedCallback(handles.toolbarActiveRegression,0,handles);
                    pause(0.01);
                end
                activeLearningStart();
            else
                set(handles.toolbarActiveLearningRun,'State','off');
            end
            
        else
            set(handles.toolbarActiveLearningRun,'State','off');
        end
    end
else
    if ~isnumeric(CommonHandles.SelectedClassifier)
        warndlg('You must retrain a classifier after using phenotype finder');
    end
    if strcmp(StateString, 'on')
        set(handles.toolbarActiveLearningRun, 'State', 'off');
    else
        set(handles.toolbarActiveLearningRun, 'State', 'on');
    end
end


% --------------------------------------------------------------------
function toolbarActiveLearningRun_OffCallback(~, ~, ~)
% hObject    handle to toolbarActiveLearningRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function toolbarActiveLearningRun_OnCallback(~, ~, ~)
% hObject    handle to toolbarActiveLearningRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function toolbarActiveRegression_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarActiveRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

StateString = get(hObject, 'State');
if ( isfield(CommonHandles, 'Success') && CommonHandles.Success == 1 && isfield(CommonHandles, 'TrainingSet')) && isnumeric(CommonHandles.SelectedClassifier)
    %changed off AL
    if strcmp(StateString, 'off')
        CommonHandles.ActiveRegression.on = 0;
    else
        
        %check for the sampling pool.
        if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
            uiwait(msgbox('It is necessary to generate a sampling pool to use active regression!','Request to generate sampling pool'));
            uiwait(loadAllFeaturesGUI());
        end
        %if the sampling pool generation was succesful
        if (isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures))
            %if AL doesn't exist popup the AL_settings
            if (~isfield(CommonHandles,'ALRegression') || ~isa(CommonHandles.ALRegression,'ALalgorithm') || ~isfield(CommonHandles.ActiveRegression,'selectedClass') || strcmp(CommonHandles.ActiveRegression.selectedClass,''))
                uiwait(ActiveRegression_GUI);
            end
            
            for i=1:length(CommonHandles.Classes)
                if strcmp(CommonHandles.ActiveRegression.selectedClass,CommonHandles.Classes{i}.Name)
                    selectedRegressionClass = i;
                    break;
                end
            end
            if exist('selectedRegressionClass','var') && strcmp(CommonHandles.Classes{selectedRegressionClass}.Type,'Regression')
                if isfield(CommonHandles.RegressionPlane{selectedRegressionClass},'Predictor') && isa(CommonHandles.RegressionPlane{selectedRegressionClass}.Predictor,'Predictor')
                    if (isfield(CommonHandles,'ALRegression') && isa(CommonHandles.ALRegression,'ALalgorithm'))
                        %if it is successful to start AL, then change off random.
                        if (CommonHandles.RandomTrain)
                            set(handles.toolbarRandomAnnotation,'State','off');
                            toolbarRandomAnnotation_ClickedCallback(handles.toolbarRandomAnnotation, eventdata, handles);
                            pause(0.01);
                        end
                        if (CommonHandles.ActiveLearning)
                            set(handles.toolbarActiveLearningRun,'State','off');
                            toolbarActiveLearningRun_ClickedCallback(handles.toolbarActiveLearningRun,eventdata, handles)
                        end
                        activeLearningStart(1);
                    else
                        set(hObject,'State','off');
                    end
                else
                    set(hObject,'State','off');
                    warndlg('You must train a predictor in the selected regression plane!');
                end
            else
                set(hObject,'State','off');
            end
        else
            set(hObject,'State','off');
        end
    end
else
    if ~isnumeric(CommonHandles.SelectedClassifier)
        warndlg('You must retrain a classifier after using phenotype finder');
    end
    if strcmp(StateString, 'on')
        set(hObject, 'State', 'off');
    end
end

% --------------------------------------------------------------------
function toolbarActiveRegression_OffCallback(~, ~, ~)
% hObject    handle to toolbarActiveRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function toolbarActiveRegression_OnCallback(~, ~, ~)
% hObject    handle to toolbarActiveRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function toolbarPhenotypeFinderRun_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarPhenotypeFinderRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if CommonHandles.ActiveLearning == 1
    CommonHandles.ActiveLearning = 0;
    set(handles.toolbarActiveLearningRun, 'State', 'off');
end

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
            uiwait(msgbox('It is necessary to generate a sampling pool to use phenotype finder!','Request to generate sampling pool'));
            uiwait(loadAllFeaturesGUI());
        end
        
        if isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures) && CommonHandles.SALT.initialized && length(CommonHandles.Classes) > 1 && ~isempty(CommonHandles.TrainingSet.Features)
            CommonHandles.SelectedClassifier = 'OneClassClassifier';
            
            CommonHandles.AllFeaturesReduced = setupDataForOutlierTree();
            
            CommonHandles.outlierTreeHandle = outlierTree_GUI();
        end
    end
end


% --------------------------------------------------------------------
function toolbarLoadAllFeatures_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarLoadAllFeatures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        loadAllFeaturesGUI();
    end
end

% --------------------------------------------------------------------
function menuAnnotation_Callback(hObject, eventdata, handles)
% hObject    handle to menuAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuCreateClass_Callback(hObject, eventdata, handles)
% hObject    handle to menuCreateClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarCreateClass_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function menuDeleteClass_Callback(hObject, eventdata, handles)
% hObject    handle to menuDeleteClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarDeleteClass_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function menuLoadClasses_Callback(hObject, eventdata, handles)
% hObject    handle to menuLoadClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarLoadClasses_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function menuSaveClasses_Callback(hObject, eventdata, handles)
% hObject    handle to menuSaveClasses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarSaveClasses_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function menuRandomAnnotation_Callback(hObject, eventdata, handles)
% hObject    handle to menuRandomAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarRandomAnnotation, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarRandomAnnotation, 'State', 'off');
        else
            set(handles.toolbarRandomAnnotation, 'State', 'on');
        end
        toolbarRandomAnnotation_ClickedCallback(hObject, eventdata, handles);
    end
end


% --------------------------------------------------------------------
function menuFindSimilarCells_Callback(hObject, eventdata, handles)
% hObject    handle to toolbarFindSimilarCellsRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuFindSimilarCellsSettings_Callback(hObject, eventdata, handles)
% hObject    handle to menuFindSimilarCellsSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
if isfield(CommonHandles, 'Success') && CommonHandles.Success == 1
    uiwait(findSimilarCellsSettingsGUI());
end

% --------------------------------------------------------------------
function menuFindSimilarCellsRun_Callback(hObject, eventdata, handles)
% hObject    handle to menuFindSimilarCellsRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
            uiwait(msgbox('It is necessary to generate a sampling pool to use similar cell finder!','Request to generate sampling pool'));
            uiwait(loadAllFeaturesGUI());
        end
        
        if ~isfield(CommonHandles,'FindSimilarCellsSettings')
            uiwait(findSimilarCellsSettingsGUI());
        end
        
        if isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures)
            min_similarity = CommonHandles.FindSimilarCellsSettings.MinSimilarityScore;
            similar = CommonHandles.FindSimilarCellsSettings.MaxSimilarCount;
            max_feat_similarity = CommonHandles.FindSimilarCellsSettings.MaxFeatureSimilarity;
            feat_variance = CommonHandles.FindSimilarCellsSettings.FeatureVariance;
            if ~isfield(CommonHandles,'SimilarCellsFeatures')
                all_features = prepareFindSimilarCells(CommonHandles.AllFeatures, max_feat_similarity, feat_variance);
                CommonHandles.SimilarCellsFeatures = all_features;
            else
                all_features = CommonHandles.SimilarCellsFeatures;
            end
            
            if isempty(all_features)
                errordlg(sprintf(['There is no similar cell to the selected one by the given parameters.\n',...
                    'Minimum similarity rate: %0.2f'], min_similarity), 'Unique cell');
                return
            end
            
            currentFeatVect = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,:);
            [indx] = find(any(all(bsxfun(@eq,currentFeatVect(1,3:end),CommonHandles.AllFeatures),2),3));
            if ~isempty(indx)
                cell_features = all_features(indx,:);
                similarCells = findSimilarCells(cell_features, all_features, min_similarity, similar);
            else
                errordlg('The selected cell is not loaded to the sampling pool.');
                return;
            end
            
            waitBarHandle = waitbar(0,'Finding similar cells.');
            counter = 0;
            
            CommonHandles.SimilarCells = struct('Images', [], 'selected', [], 'sHandle', []);
            
            %% TODO
            % this part should be optimized
            tempCurrentCellInfo = makeTempCellData();
            for i=2:length(similarCells)
                counter = counter + 1;
                featmap = CommonHandles.AllFeaturesMapping{similarCells(i)};
                cellInfo.CellNumber = featmap.CellNumberInImage;
                cellInfo.OriginalImageName = featmap.ImageName;
                [~,name,ext] = fileparts(featmap.ImageName);
                cellInfo.ImageName = [char(CommonHandles.PlatesNames(featmap.CurrentPlateNumber)) filesep CommonHandles.ImageFolder filesep name ext];
                cellInfo.PlateName = featmap.CurrentPlateNumber;
                jumpToCell(cellInfo,0);
                % Add to SimilarCells
                cellImg.Image = CommonHandles.CurrentSelectedCell;
                cellImg.ClassName = 'Similar cells';
                cellImg.CellNumber = CommonHandles.SelectedCell;
                cellImg.ImageName = CommonHandles.SelectedImageName;
                cellImg.OriginalImageName = CommonHandles.SelectedOriginalImageName;
                cellImg.PlateName = CommonHandles.SelectedPlate;
                CommonHandles.SimilarCells.Images{i-1} = cellImg;
                
                donePercent = double(counter/length(similarCells));
                waitText = sprintf('Getting the cells... %d%% done', int16(donePercent * 100));
                waitbar(donePercent, waitBarHandle, waitText);
            end
            
            close(waitBarHandle);
            
            if isempty(similarCells)
                errordlg('No similar cells were found. Please, try to set lower value into Min similarity score setting and try again.','No similar cells found');
            else
                pause(1);
                if ~isempty(CommonHandles.ShowSimilarCellsHandle) && ishandle(CommonHandles.ShowSimilarCellsHandle)
                    delete(CommonHandles.ShowSimilarCellsHandle);
                end
                CommonHandles.ShowSimilarCellsHandle = showTrainedCells_GUI('UserData',0);
                
            end
            
            jumpToCell(tempCurrentCellInfo,1);
        end
    end
end

% --------------------------------------------------------------------
function menuActiveLearning_Callback(~, ~, ~)
% hObject    handle to menuActiveLearning (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuActiveLearningSettings_Callback(~, ~, handles)
% hObject    handle to menuActiveLearningSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
%check for the sampling pool.
if (isfield(CommonHandles, 'Success') && CommonHandles.Success == 1)
    if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
        uiwait(msgbox('It is necessary to generate a sampling pool to use active learning!','Request to generate sampling pool'));
        uiwait(loadAllFeaturesGUI());
    end
    if ( isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures) && isfield(CommonHandles, 'TrainingSet'))
        if (isnumeric(CommonHandles.SelectedClassifier))
            uiwait(AL_GUI);
            %if we resume from AL_GUI here with true AL, then the state change of the toggle
            %tool is not done elsewehere
            if (CommonHandles.ActiveLearning)
                set(handles.toolbarActiveLearningRun,'State','on');
                toolbarActiveLearningRun_ClickedCallback(handles.toolbarActiveLearningRun,0,handles);
            end
        else
            warndlg('You must retrain after using the ''Phenotype finder'' module.');
        end
    end
end

% --------------------------------------------------------------------
function menuActiveLearningRun_Callback(hObject, eventdata, handles)
% hObject    handle to menuActiveLearningRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
StateString = get(handles.toolbarActiveLearningRun, 'State');
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if strcmp(StateString, 'on')
            set(handles.toolbarActiveLearningRun, 'State', 'off');
        else
            set(handles.toolbarActiveLearningRun, 'State', 'on');
        end
        toolbarActiveLearningRun_ClickedCallback(hObject, eventdata, handles);
    end
end

% --------------------------------------------------------------------
function menuActiveRegressionSettings_Callback(~, ~, handles)
% hObject    handle to menuActiveRegressionSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
%check for the sampling pool.
if (isfield(CommonHandles, 'Success') && CommonHandles.Success == 1)
    if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
        uiwait(msgbox('It is necessary to generate a sampling pool to use active learning!','Request to generate sampling pool'));
        uiwait(loadAllFeaturesGUI());
    end
    if ( isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures) && isfield(CommonHandles, 'TrainingSet'))
        if (isnumeric(CommonHandles.SelectedClassifier))
            uiwait(ActiveRegression_GUI);
            %if we resume from AL_GUI here with true AL, then the state change of the toggle
            %tool is not done elsewehere
            if (CommonHandles.ActiveRegression.on)
                set(handles.toolbarActiveRegression,'State','on');
                toolbarActiveRegression_ClickedCallback(handles.toolbarActiveRegression,0,handles);
            end
        else
            warndlg('You must retrain after using the ''Phenotype finder'' module.');
        end
    end
end

% --------------------------------------------------------------------
function menuActiveRegressionRun_Callback(~, eventdata, handles)
% hObject    handle to menuActiveRegressionRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
StateString = get(handles.toolbarActiveRegression, 'State');
if isfield(CommonHandles, 'Success') && CommonHandles.Success == 1
    if strcmp(StateString, 'on')
        set(handles.toolbarActiveRegression, 'State', 'off');
    else
        set(handles.toolbarActiveRegression, 'State', 'on');
    end
    toolbarActiveRegression_ClickedCallback(handles.toolbarActiveRegression, eventdata, handles);
end



% --------------------------------------------------------------------
function menuPhenotypeFinder_Callback(hObject, eventdata, handles)
% hObject    handle to menuPhenotypeFinder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuOccBintreeSettings_Callback(hObject, eventdata, handles)
% hObject    handle to menuOccBintreeSettings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        outlierTreeSettingsGUI();
    end
end


% --------------------------------------------------------------------
function menuPhenotypeFinderRun_Callback(hObject, eventdata, handles)
% hObject    handle to menuPhenotypeFinderRun (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        toolbarPhenotypeFinderRun_ClickedCallback(hObject, eventdata, handles)
    end
end


% --------------------------------------------------------------------
function menuLoadAllFeatures_Callback(hObject, eventdata, handles)
% hObject    handle to menuLoadAllFeatures (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarLoadAllFeatures_ClickedCallback(hObject, eventdata, handles)


%-------------------------------------------------------------------------
%% END: Annotation MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


%-------------------------------------------------------------------------
%% START: CLASSIFICATION MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


% --------------------------------------------------------------------
function toolbarClassificationSetting_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarClassificationSetting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if CommonHandles.SALT.initialized && length(CommonHandles.Classes) > 1 && ~isempty(CommonHandles.TrainingSet.Features)
    train_algorithmGUI;
    uiwait(gcf);
end

disablePrediction(0, handles);


% --------------------------------------------------------------------
function toolbarPredictImage_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarPredictImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles.SALT,'model')
    if ~isempty(CommonHandles.SALT.model) && isnumeric(CommonHandles.SelectedClassifier)
        CommonHandles.CurrentPrediction = {};
        
        % for i=1:size(CommonHandles.SelectedMetaData, 1)
        %     features(i,:) = CommonHandles.SelectedMetaData(i, 3:size(CommonHandles.SelectedMetaData, 2));
        % end
        features = CommonHandles.SelectedMetaData(:,3:end);
        
        [ out, props, CommonHandles ] = predictClassifier( features, CommonHandles);
        %Predict regression also
        outClass = num2cell(out);
        outReg = cell(length(outClass),1);
        for i=1:length(CommonHandles.RegressionPlane)
            if isfield(CommonHandles.RegressionPlane{i},'Predictor') && isa(CommonHandles.RegressionPlane{i}.Predictor,'Predictor')
                classIndices = find(out == i);
                if ~isempty(classIndices)
                    regOut = CommonHandles.RegressionPlane{i}.Predictor.predict(features(classIndices,:)');
                    for j=1:length(classIndices)
                        outReg{classIndices(j)} = regOut(:,j)';
                    end
                end
            end
        end
        
        % print out percentages
        classString = [];
        for i=1:size(props, 2)
            classString = [classString 'C' num2str(i) '=' num2str((length(find(out == i))/length(out))*100) '%; '];
        end
        
        fprintf('Distribution of cell numbers among classes on the selected image:\n');
        disp(classString);
        
        jointPrediction = cell(length(outClass),2);
        jointPrediction(:,1) = outClass;
        jointPrediction(:,2) = outReg;
        CommonHandles.CurrentPrediction = jointPrediction;
        
        CommonHandles = showSelectedCell(CommonHandles);
    elseif strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
        warndlg('You must retrain after using the ''Phenotype finder'' module.');
    end
end


% --------------------------------------------------------------------
function toolbarPredictPlate_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarPredictPlate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles.SALT,'model')
    if ~isempty(CommonHandles.SALT.model) && isnumeric(CommonHandles.SelectedClassifier)
        create_reportGUI;
        uiwait(gcf);
    elseif strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
        warndlg('You must retrain after using the ''Phenotype finder'' module.');
    end
end


% --------------------------------------------------------------------
function menuClassification_Callback(hObject, eventdata, handles)
% hObject    handle to menuClassification (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuClassificationSetting_Callback(hObject, eventdata, handles)
% hObject    handle to menuClassificationSetting (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarClassificationSetting_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function menuPredictImage_Callback(hObject, eventdata, handles)
% hObject    handle to menuPredictImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarPredictImage_ClickedCallback(hObject, eventdata, handles);


% --------------------------------------------------------------------
function menuPredictPlate_Callback(hObject, eventdata, handles)
% hObject    handle to menuPredictPlate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
toolbarPredictPlate_ClickedCallback(hObject, eventdata, handles);


% Export feature-based statistics
function menuStatistics_Callback(hObject, eventdata, handles)
% hObject    handle to menuStatistics (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles, 'Success') && CommonHandles.Success == 1
    if ~isfield(CommonHandles, 'Report') || ~isfield(CommonHandles.Report, 'CellPrediction')
        errordlg('Please, predict the selected plate first','Cell prediction missing');
    else
        export_feature_statisticsGUI;
        uiwait(gcf);
    end
end

%-------------------------------------------------------------------------
%% END: CLASSIFICATION MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


%-------------------------------------------------------------------------
%% START: HELP MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


% --------------------------------------------------------------------
function menuHelp_Callback(hObject, eventdata, handles)
% hObject    handle to menuHelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menuAbout_Callback(hObject, eventdata, handles)
% hObject    handle to menuAbout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
Message = {'SOFTWARE', ...
    'Advanced Cell Classifier (ACC)', ...
    'version 3.1', ...
    'July, 2020', ...
    ' ', ...
    'WEBSITE', ...
    'http://www.cellclassifier.org/', ...
    ' ', ...
    'MAIN CONTACT', ...
    'Peter Horvath, Ph.D.', ...
    'Synthetic and Systems Biology Unit', ...
    'Biological Research Centre (BRC)',...
    'Szeged, Hungary',...
    'horvath.peter@brc.hu',...
    ' ',...
    ' '};
msgbox(Message,'About us')


% --------------------------------------------------------------------
function menuDocumentation_Callback(hObject, eventdata, handles)
% hObject    handle to menuDocumentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

docFileName = 'ACC_Documentation_20170620.pdf';

if ismac
    system(['open ' docFileName]);
elseif isunix
    system(['evince ' docFileName ' &']);
elseif ispc
    winopen(docFileName);
else
    disp('Unkown platform');
end


% --------------------------------------------------------------------
function menuRPdocumentation_Callback(hObject, eventdata, handles)
% hObject    handle to menuRPdocumentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

docFileName = 'ACC_RegressionPlane_Documentation_20170620.pdf';

if ismac
    system(['open ' docFileName]);
elseif isunix
    system(['evince ' docFileName ' &']);
elseif ispc
    winopen(docFileName);
else
    disp('Unkown platform');
end

%-------------------------------------------------------------------------
%% END: HELP MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


%-------------------------------------------------------------------------
%% START: CAMI MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------

function camiPushtool_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to camiPushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        if ~isfield(CommonHandles,'AllFeatures') || isempty(CommonHandles.AllFeatures)
            uiwait(msgbox('It is necessary to generate a sampling pool to use upload module!','Request to generate sampling pool'));
            uiwait(loadAllFeaturesGUI());
        end
        %check whether the loadAllFeatures worked well or it was closed.
        if isfield(CommonHandles,'AllFeatures') && ~isempty(CommonHandles.AllFeatures)
            if isfield(CommonHandles.SALT,'model')
                if ~isempty(CommonHandles.SALT.model) && isnumeric(CommonHandles.SelectedClassifier)
                    CommonHandles.UploadCAMIOFigure = uploadSelectedCellsGUI();
                elseif strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
                    warndlg('You must retrain after using the ''Phenotype finder'' module.');
                end
            else
                warndlg('You must train a classifier before using upload module.');
            end
        end
    end
end

%-------------------------------------------------------------------------
%% END: CAMI MENU AND TOOLBAR ITEMS
%-------------------------------------------------------------------------


% --------------------------------------------------------------------
function menuSubClusters_Callback(hObject, eventdata, handles)
% hObject    handle to menuSubClusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Function to create subclusters for classes
global CommonHandles;

if isfield(CommonHandles,'Success') && CommonHandles.Success
    launchSubClusterCreator();
end


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles
switch eventdata.Character
        
    case 't' % Trajectory window
        trajAnnotation
        
    case 'E' % export training set for Deep Learning
        trSetExporter2DRP
        
    case 'A' % Trajectory window
        accImSeqAnimation
    otherwise
end


% --------------------------------------------------------------------
function TrajectoryLevelAnnotation_OffCallback(hObject, eventdata, handles)
% hObject    handle to TrajectoryLevelAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
CommonHandles.TrajLevelAnnotation = 0;

% --------------------------------------------------------------------
function TrajectoryLevelAnnotation_OnCallback(hObject, eventdata, handles)
% hObject    handle to TrajectoryLevelAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
CommonHandles.TrajLevelAnnotation = 1;

answer = questdlg('How do you want to provide tracking information?','Trackging method selection','Tracking file','Calculate now','Tracking file');
if isempty(answer)
    error('It is required to select a tracking method');
    CommonHandles.TrajLevelAnnotation = 0;
    hObject.State = 'off';
end
if strcmp(answer,'Tracking file')
    [trackfile,trackpath] = uigetfile('*.csv','Please select a tracking file', CommonHandles.DirName);
    
    if ~isempty(trackfile)
        trackTable = readtable(fullfile(trackpath,trackfile));
        toCheckFields = {'ImageName','Plate','TrackID','tracking__center_x','tracking__center_y','Frame'};
        bool = checkTable(toCheckFields,trackTable);
        if bool == 0
            CommonHandles.TrajLevelAnnotation = 0;
            hObject.State = 'off';
        end
        CommonHandles.Tracking = trackTable;
    else
        CommonHandles.TrajLevelAnnotation = 0;
        hObject.State = 'off';
    end
elseif strcmp(answer,'Calculate now')
    trackTable = TrackingWizardGUI();
    if isempty(trackTable)
        error('ACC cannot produce trajectories without tracking information');
        CommonHandles.TrajLevelAnnotation = 0;
        hObject.State = 'off';
    end
    CommonHandles.Tracking = trackTable;
end


% --------------------------------------------------------------------
function TrackingWizard_Callback(hObject, eventdata, handles)
% hObject    handle to TrackingWizard (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
TrackingWizardGUI()


% --------------------------------------------------------------------
function animateImageSeq_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to animateImageSeq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        accImSeqAnimation
    end
end


% --------------------------------------------------------------------
function exportTrainingImages_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to exportTrainingImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles
if isfield(CommonHandles, 'Success') && isfield(CommonHandles,'TrainingSet')
    if (CommonHandles.Success == 1)
        trSetExporter2DRP
    end
end

% --------------------------------------------------------------------
function openImSeqScope_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to openImSeqScope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles
if isfield(CommonHandles, 'Success')
    if (CommonHandles.Success == 1)
        trajAnnotation
    end
end
