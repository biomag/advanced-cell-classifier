function [ rowVector ] = convert2RowVector( vector )
% AUTHOR:   Abel Szkalisity
% DATE:     April 22, 2016
% NAME:     convert2RowVector
%
% convert2RowVector It is often a problem that a vector is not in right
% dimensions, and the programmer should remember whether the vector was a
% column or a row vector. (even knowing that in MatLab the vector is column by
% default, but that's not true for all the cases)
%
% INPUT:
%   vector          input vector to be converted
%
% OUTPUT:
%   rowVector       the same but row vector
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if (size(vector,2)<size(vector,1))
   rowVector = vector'; 
else
   rowVector = vector;
end

end

