function imgOut = createWhiteBgImg(img)
% AUTHOR:	Peter Horvath
% DATE: 	April 22, 2016
% NAME: 	createWhiteBgImg
% 
% To create an image with white background starting from an existing image
%
% INPUT:
%   img             Input image.
%
% OUTPUT:
%   imgOut          Output image of the same size of the input image
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

img = double(img);
imgr = img(:,:,1); imgg = img(:,:,2); imgb = img(:,:,3);

maxi = 255;
hmaxi = 127;

% reds
imgg1 = maxi-imgr;
imgb1 = maxi-imgr;
s = find(imgr <= hmaxi);
l = find(imgr > hmaxi);
imgr1 = ones(size(imgr)) * 255; 
% imgr1 = maxi-imgr;
% imgr1(l) = imgr(l);

% greens
imgr2 = maxi-imgg;
imgb2 = maxi-imgg;
s = find(imgg <= hmaxi);
l = find(imgg > hmaxi);
% imgg2 = maxi-imgg;
% imgg2(l) = imgg(l);
imgg2 = ones(size(imgg)) * 255; 

% blues
imgr3 = maxi-imgb;
imgg3 = maxi-imgb;
s = find(imgb <= hmaxi);
l = find(imgb > hmaxi);
% imgb3 = maxi-imgb;
% imgb3(l) = imgb(l);
imgb3 = ones(size(imgb)) * 255; 

imgrr = reshape([imgr1 imgr2 imgr3], [size(imgr,1) size(imgr,2) 3]);
imggg = reshape([imgg1 imgg2 imgg3], [size(imgg,1) size(imgg,2) 3]);
imgbb = reshape([imgb1 imgb2 imgb3], [size(imgb,1) size(imgb,2) 3]);

imgmr = mean(imgrr, 3);
imgmg = mean(imggg, 3);
imgmb = mean(imgbb, 3);

% imgmr = imadjust(uint8(imgmr));
% imgmg = imadjust(uint8(imgmg));
% imgmb = imadjust(uint8(imgmb));

imgOut = reshape([imgmr imgmg imgmb], [size(imgmr,1) size(imgmr,2) 3]);

% scale the intensities a bit
imgOut = uint8((double(imgOut) - 192) * 4);

% imshow(uint8(imgOut))
% 
% imwrite(uint8(imgOut), 'out.bmp');