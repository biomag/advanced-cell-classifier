function out = createWekaFile(CommonHandles, outputFileName, mapClasses, deleteClassPattern)
% Weka dataset creator

% INPUT
%   CommonHandles       - CommonHandles
%   outputFileName      - filename
%   mapClasses          - list of numbers for the classes (ex. in case of 10 - [1:10])
%   deleteClassPattern  - binary list, 1 to keep the class - 0 to delete it
%                         (ex. in case to delete 2nd and 4th of 5 - [1 0 1 0 1])

% OUTPUT
%   out                 - output .arff file for Weka


fid = fopen([outputFileName '.arff'], 'w');

fprintf(fid, '@relation ''HTS data set''\n');

for i=1:(length(CommonHandles.TrainingSet.Features{1}))

    fprintf(fid, '@attribute ''feature%d'' real\n', i);
    
end;

fprintf(fid, '@attribute class {');
for i=1:max(mapClasses)
    fprintf(fid, '%d ', i);
end;
fprintf(fid, '}\n');

fprintf(fid, '@data\n');

for i=1:length(CommonHandles.TrainingSet.Features)
    
    fv = CommonHandles.TrainingSet.Features{i};
    
    cv = CommonHandles.TrainingSet.Class{i};    
    
    [maxi, maxv] = max(cv);
    
    if deleteClassPattern(maxv)
        
        for j=1:length(fv)
            
            fprintf(fid, '%f,', fv(j));
            
        end;
        
        fprintf(fid, '%d\n', mapClasses(maxv));
        
    end
    
    i/length(CommonHandles.TrainingSet.Features)
    
end;

fclose(fid);

out = 0;

