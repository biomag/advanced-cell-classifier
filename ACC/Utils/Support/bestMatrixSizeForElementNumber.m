function [minRow,minCol] = bestMatrixSizeForElementNumber(elementNumber)
% AUTHOR:   Abel Szkalisity
% DATE:     March 08, 2017
% NAME:     bestMatrixSizeForElementNumber
%
% calculates which is the 'optimal' including matrix for a number of
% entries. (the optimal means here the most compact representation in a
% sense that the maximum size of an edge is as small as possible). By
% default the row is not greater than the column size.
%
% INPUT:
%   elementNumber   The number of elements that we want to locate
%
% OUTPUT:
%   minRow          The row size of the optimal matrix
%   minCol          The colomn size of the optimal matrix
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    [curRow,curCol] = closestDivisor(elementNumber);
    minRow = curRow; minCol = curCol;
    maxSide = max(curRow,curCol);
    globalSize = elementNumber;
    while globalSize<2*elementNumber        
        globalSize = globalSize + 1;
        [curRow,curCol] = closestDivisor(globalSize);
        if max(curRow,curCol)<maxSide %minimizing maxSide
            minRow = curRow; minCol = curCol;
            maxSide = max(curRow,curCol);
        end
        if globalSize>(elementNumber + minCol)
            break;
        end
    end    
end