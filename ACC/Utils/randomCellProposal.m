function [classStruct] = randomCellProposal(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     Aug 03, 2016
% NAME:     randomCellProposal
%
% Suggest a randomly selected cell and gives back a structure with the
% fields of the selected cell. Fields covers: SelectedImageName,
% SelectedImage, CellNumber, SelectedOriginalImageName, SelectedPlate
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%global CommonHandles;
        tempSIN = CommonHandles.SelectedImageName;
        tempSI = CommonHandles.SelectedImage;
        tempSC = CommonHandles.SelectedCell;
        tempSOIN = CommonHandles.SelectedOriginalImageName;
        tempSP = CommonHandles.SelectedPlate;

        CommonHandles.SelectedPlate = ceil(rand()*length(CommonHandles.PlatesNames));
        ImageList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep '*' CommonHandles.ImageExtension]);        
        ImageListStr = char(ImageList.name);
        ImageListStr = cellstr(ImageListStr);                   
        
        MaxImage = length(ImageListStr);
        flag = 0;
        maxIteration = 10000;
        iter = 1;
        while (flag == 0) && (iter < maxIteration)
            iter = iter + 1;
            %select a random image
            CommonHandles.SelectedImage = ceil(MaxImage*rand);                                                           
            CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep ImageListStr{CommonHandles.SelectedImage}];            
            CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep ImageListStr{CommonHandles.SelectedImage}];            
            
            %read the meta data, too see the number of cells (it is double read, as it is done within LoadSelectedImage too)
            [~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
            [~,metaData] = readMetaData(CommonHandles, fileNameexEx);            
            
            first = size(metaData,1);
            second = size(metaData,2);
            if first==1 || second==1
                temp = zeros(first,second);
            else
                temp = 0;
            end
            if ~isequal(metaData,temp)
                %select random cell within image
                CommonHandles.SelectedCell = ceil(size(metaData,1)*rand);                
                if ~isInTrainingSet(CommonHandles.SelectedImageName,CommonHandles.SelectedCell,0,CommonHandles,metaData(CommonHandles.SelectedCell,3:end))
                    flag = 1;
                end
            end
        end
        
        if (iter == maxIteration)
            throw(MException('ACC_EXC:notEnoughCellToChoose','ACC cannot select a random cell.'));
        end
        
        %reload saved data to CH, and give back in the structure the
        %proposal
        classStruct.CellNumber = CommonHandles.SelectedCell;
        CommonHandles.SelectedCell = tempSC;
        
        classStruct.ImageName = CommonHandles.SelectedImageName;
        CommonHandles.SelectedImageName = tempSIN;
        
        classStruct.SelectedImage = CommonHandles.SelectedImage;
        CommonHandles.SelectedImage = tempSI;
        
        classStruct.OriginalImageName = CommonHandles.SelectedOriginalImageName;
        CommonHandles.SelectedOriginalImageName = tempSOIN;
        
        classStruct.PlateName = CommonHandles.SelectedPlate;
        CommonHandles.SelectedPlate = tempSP;

end

