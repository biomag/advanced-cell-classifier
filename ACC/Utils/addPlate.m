function addPlate(plateName,loadingFlag)
% AUTHOR:	Tamas Balassa
% DATE: 	June 20, 2016
% NAME: 	addPlate
% 
% To add one plate to the dataset
%
% INPUT:
%        plateName         The name of the plate that will be added
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
        
    % add to plate_well_gui list
    CommonHandles.PlatesNames = [CommonHandles.PlatesNames plateName'];
    CommonHandles.PlatesNames = sort(CommonHandles.PlatesNames);
    [~,indx] = ismember(plateName,CommonHandles.PlatesNames);
    
    % update plates in AllFeaturesMapping
    % this can be optimized - dont go through the AFM for each plate insert
    % its not necessary yet
    if isfield(CommonHandles,'AllFeaturesMapping') && ~isempty(CommonHandles.AllFeaturesMapping)
       for i=1:size(CommonHandles.AllFeaturesMapping,2)
           if CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber >= indx
               CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber = CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber + 1;
           end
       end
       
       % expand CommonHandles fields
       if loadingFlag
           [features, mapping, coords] = loadAllFeatures(CommonHandles, CommonHandles.SamplingRatioForAllFeatures, 1, indx);
           [cleanFeatures,cleanMapping,cleanCoords] = clearEmptyRowsFromPlate(features,mapping,coords);
           CommonHandles.AllFeatures = [CommonHandles.AllFeatures; cleanFeatures];
           CommonHandles.AllFeaturesMapping = [CommonHandles.AllFeaturesMapping cleanMapping];
           CommonHandles.AllCoordinates = [CommonHandles.AllCoordinates; cleanCoords];
       end
    end
    
    
    % add to the ClassImages
    if CommonHandles.ClassImages.Count == size(CommonHandles.Classes,2)
        for i=1:size(CommonHandles.Classes,2)
            classImgs = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name);
            imgs = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name).Images;
            for j=1:size(imgs,2)
                if imgs{j}.PlateName >= indx
                    imgs{j}.PlateName = imgs{j}.PlateName + 1;
                end
            end
            classImgs.Images = imgs;
            CommonHandles.ClassImages(CommonHandles.Classes{i}.Name) = classImgs;
        end
    end    