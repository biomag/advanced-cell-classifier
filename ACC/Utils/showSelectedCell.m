function CommonHandles = showSelectedCell(CommonHandles)
% AUTHOR:	Peter Horvath
% DATE: 	April 22, 2016
% NAME: 	showSelectedCell
% 
% Displays the actual part of current image after zooming.
%   More detailed: First it draws out the apparent part of the selected
%   image and puts on the appropriate cell numbers and the rectangle for
%   the selected cell. Then it cuts out and shows the 'icon' cells.
% 
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if (CommonHandles.ShowContour)
    imageToShow = CommonHandles.CurrentImage;
else
    imageToShow = CommonHandles.CurrentOriginalImage;
end

%cutSize = 50;
cutSize = CommonHandles.CutSize;

imageToShow(:, :, 1) = imageToShow(:, :, 1) * CommonHandles.MainView.Red;
imageToShow(:, :, 2) = imageToShow(:, :, 2) * CommonHandles.MainView.Green;
imageToShow(:, :, 3) = imageToShow(:, :, 3) * CommonHandles.MainView.Blue;

if ~CommonHandles.ColorView
    
    imageToShow = max(imageToShow,[], 3);
    
end

% cut and display visible part of the original image after zooming
showedImage = imageToShow(CommonHandles.CurrentImageSizeX-int16(round(CommonHandles.MainView.YBounds(2)))+1:CommonHandles.CurrentImageSizeX-int16(round(CommonHandles.MainView.YBounds(1)+1)), int16(round(CommonHandles.MainView.XBounds(1))):int16(round(CommonHandles.MainView.XBounds(2))), :);
imshow(showedImage, 'Parent', CommonHandles.MainViewHandle);

classflag = 0;
for i=1:size(CommonHandles.SelectedMetaData, 1)
    
    % checking if cell is inside visible part of the image
    if CommonHandles.SelectedMetaData(i, 2)>(CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2))&&CommonHandles.SelectedMetaData(i, 2)<(CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(1))...
            &&CommonHandles.SelectedMetaData(i, 1)>CommonHandles.MainView.XBounds(1)&&CommonHandles.SelectedMetaData(i, 1)<CommonHandles.MainView.XBounds(2)
        if ~isempty(CommonHandles.CurrentPrediction) % is Prediction
            classflag = 1;            
            CellNumberText = sprintf('{%d}',CommonHandles.CurrentPrediction{i,1});                       
        else
            CellNumberText = sprintf('%d', i);
        end
        
        if CommonHandles.ColorView
            c = colormap('lines');
        else
            c = colormap('gray');
        end
        
        if i~=CommonHandles.SelectedCell
            if CommonHandles.ShowCellNumbers == 1
                if classflag % prediction
                    text(CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, CellNumberText, 'Color', 'white', 'FontSize', 8, 'EdgeColor', c(CommonHandles.CurrentPrediction{i,1}, :), 'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                else                    
                    if isfield(CommonHandles,'TrainingSet') && ~isempty(CommonHandles.TrainingSet.Features) && CommonHandles.DisplayAnnotatedCells == 1                                                
                        idx = isInTrainingSet(CommonHandles.SelectedImageName,i,0,CommonHandles,CommonHandles.SelectedMetaData(i,3:end));
                        if idx ~= 0 % annotated object
                            classVector = CommonHandles.TrainingSet.Class{idx};
                            index = find(classVector,1);
                            text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, CellNumberText, 'Color', 'white', 'FontSize', 15, 'EdgeColor', c(index,:), 'LineWidth', 5,'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                        else
                            text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, CellNumberText, 'Color', 'white' ,'FontSize', 7, 'EdgeColor', 'white', 'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                        end
                    else
                        text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, CellNumberText, 'Color', 'white' ,'FontSize', 7, 'EdgeColor', 'white', 'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                    end
                end
            else
                if isfield(CommonHandles,'TrainingSet') && ~isempty(CommonHandles.TrainingSet.Features) && CommonHandles.DisplayAnnotatedCells == 1                       
                        idx = isInTrainingSet(CommonHandles.SelectedImageName,i,0,CommonHandles,CommonHandles.SelectedMetaData(i,3:end));
                        if idx ~= 0
                            classVector = CommonHandles.TrainingSet.Class{idx};
                            index = find(classVector,1);
                            text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, num2str(index), 'Color', 'white' ,'FontSize', 7, 'EdgeColor', c(index,:), 'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                        end
                end
            end
        else
            if classflag
                if ~isempty(CommonHandles.CurrentPrediction{i,2}) %if there is regression for that cell
                    CellNumberText = sprintf('{%d - %s}',CommonHandles.CurrentPrediction{i,1},mat2str(CommonHandles.CurrentPrediction{i,2}/1000,3));
                end
                text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000 , CellNumberText, 'Color', 'white', 'FontSize', 12,  'BackgroundColor','r', 'EdgeColor', c(CommonHandles.CurrentPrediction{i,1}, :), 'LineWidth', 2,  'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
                %text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)) , CellNumberText, 'Color', 'white', 'FontSize', 12,  'BackgroundColor','r', 'EdgeColor', c(b(i),:), 'LineWidth', 2,  'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
            else 
                idx = isInTrainingSet(CommonHandles.SelectedImageName,i,0,CommonHandles,CommonHandles.SelectedMetaData(i,3:end));
                if idx>0 && ~isempty(CommonHandles.TrainingSet.RegPos{idx})
                    if CommonHandles.DisplayAnnotatedCells == 1
                        CellNumberText = sprintf('%d - %s', i, mat2str(CommonHandles.TrainingSet.RegPos{idx}/1000,3));                    
                    end
                end
                text( CommonHandles.SelectedMetaData(i, 1)-CommonHandles.MainView.XBounds(1), CommonHandles.SelectedMetaData(i, 2) - (CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)),1000, CellNumberText, 'Color', 'white', 'FontSize', 10, 'EdgeColor', 'blue', 'LineWidth', 2,  'HorizontalAlignment','center', 'Parent', CommonHandles.MainViewHandle);
            end
            
            % boundaries of frame rectangle for current cell
            if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-CommonHandles.MainView.XBounds(1) < cutSize+1
                cutx1=1;
            else
                cutx1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-CommonHandles.MainView.XBounds(1)-cutSize);
            end
            
            if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-(CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2)) < cutSize+1
                cuty1=1;
            else
                cuty1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-(CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2))-cutSize);
            end
            
            if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-CommonHandles.MainView.XBounds(2) > (CommonHandles.MainView.XBounds(2)-CommonHandles.MainView.XBounds(1) -cutSize)
                cutx2 = floor(CommonHandles.MainView.XBounds(2)-CommonHandles.MainView.XBounds(1));
            else
                cutx2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-CommonHandles.MainView.XBounds(1)+cutSize);
            end
            
            if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) > (CommonHandles.CurrentImageSizeX-cutSize)
                cuty2 = floor(CommonHandles.MainView.YBounds(2)-CommonHandles.MainView.YBounds(1));
            else
                cuty2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-(CommonHandles.CurrentImageSizeX-CommonHandles.MainView.YBounds(2))+cutSize);
            end
            rectangle('Position',[cutx1, cuty1, cutx2-cutx1, cuty2-cuty1],'EdgeColor', 'white', 'LineWidth',1, 'Parent', CommonHandles.MainViewHandle);
        end
    end
end % endfor all cells

% TODO we should not change small icon in case of zooming, only by clicking

% boundaries of the part of original image containing current cell for small view
if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) < cutSize+1
    cutx1=1;
else
    cutx1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) < cutSize+1
    cuty1=1;
else
    cuty1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) > (CommonHandles.CurrentImageSizeY -cutSize)
    cutx2=CommonHandles.CurrentImageSizeY;
else
    cutx2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)+cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) > (CommonHandles.CurrentImageSizeX-cutSize)
    cuty2=CommonHandles.CurrentImageSizeX;
else
    cuty2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)+cutSize);
end

if ~(CommonHandles.ShowContour)
    ViewImage = CommonHandles.CurrentOriginalImage(cuty1:cuty2, cutx1:cutx2, :);
else
    ViewImage = CommonHandles.CurrentImage(cuty1:cuty2, cutx1:cutx2, :);
end

CommonHandles.CurrentSelectedCell = CommonHandles.CurrentOriginalImage(cuty1:cuty2, cutx1:cutx2, :);

imshow(ViewImage, 'Parent', CommonHandles.SmallColorViewHandle);


% in case we will do not have the original images, this process can easily
% get rid of the contour
% if ~(CommonHandles.ShowContour)
%     ViewImage(:,:, 1) = medfilt2(ViewImage(:,:, 1), [7 7]);
%     ViewImage(:,:, 2) = medfilt2(ViewImage(:,:, 2), [7 7]);
%     ViewImage(:,:, 3) = medfilt2(ViewImage(:,:, 3), [7 7]);
% end;

rViewImage = ViewImage;
gViewImage = ViewImage;
bViewImage = ViewImage;

if CommonHandles.ColorView
    rViewImage(:,:,2:3) = 0;
    gViewImage(:,:,[1 3]) = 0;
    bViewImage(:,:,1:2) = 0;
else
    rViewImage(:,:,2) = rViewImage(:,:,1); rViewImage(:,:,3) = rViewImage(:,:,1);
    gViewImage(:,:,1) = gViewImage(:,:,2); gViewImage(:,:,3) = gViewImage(:,:,2);
    bViewImage(:,:,1) = bViewImage(:,:,3); bViewImage(:,:,2) = bViewImage(:,:,3);
end;

imshow(rViewImage, 'Parent', CommonHandles.SmallRedViewHandle);
imshow(gViewImage, 'Parent', CommonHandles.SmallGreenViewHandle);
imshow(bViewImage, 'Parent', CommonHandles.SmallBlueViewHandle);
