function trSetExporter2DRP
% AUTHOR:	Attila Beleon
% DATE: 	October 29, 2023
% NAME: 	trSetExporter2DRP
% 
% Exports the trainingset.
%
% OUTPUT: - Cropped thumbnails of cells as images
%         - 2D Coordinates on the Regression Plane saved as images
%           (strange format but this was suported by the built in matlab
%           deeplearning functions at the time this function was developed.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

ouputPath = uigetdir('pwd','Select folder to export regression training set!');
outputAttributes.outputPath = ouputPath;
answerPadding = questdlg('Would you like to apply padding on your exported images?','Padding','No padding','Padding','No padding');
if isempty(answerPadding)
    return
end
outputAttributes.Padding = answerPadding;
if strcmp(answerPadding,'Padding')
    answerPadVal = questdlg('What should be the padding value?','Padding value','Most Frequent Color','Mirror','Zero','Zero');
    if isempty(answerPadVal)
        return
    end
    outputAttributes.PadValue = answerPadVal;
end

zStack = questdlg('Is raw data has multiple z-stacks?','Z-stack','Yes','No','No');
if isempty(zStack)
    return
end
if strcmp(zStack,'Yes')
    dynamicFocusAnswer = questdlg('Would you like to combine z-stacks?','Dynamic focus','Yes','No','Yes');
    if isempty(dynamicFocusAnswer)
        return
    end
    if strcmp(dynamicFocusAnswer,'No')
        zID = inputdlg('Enter the ID of the z-stack you would like to use:','Z-stack ID',1,"1");
        if isempty(zID) || isempty(zID{:}) || isnan(str2double(zID{:}))
            return
        end
    end
end

try
    answer2 = Settings_GUI({...
        struct('name', 'Crop Size', 'type', 'int', 'default', 100, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [0 512])),...
        },...
        'infoText', 'Enter the size of desired crops in pixel!', 'title', 'Crop Size');
    outputAttributes.cropSize = answer2{1};
catch
    return
end
cropSize = answer2{1};
outputAttributes.cropSize = cropSize;
halfCropSize = round(cropSize/2);
if ~isempty(ouputPath)
    global CommonHandles
    outputAttributes.ProjectName = CommonHandles.ProjectName;
    splitImputFolderName = strsplit(CommonHandles.SelectedImageName,filesep);
    for cl = 1:numel(CommonHandles.Classes)
        classNames{cl,1} = CommonHandles.Classes{cl}.Name;
    end
    classID = listdlg('PromptString',{'Select a class to save as', 'annotated data for training','a deep learning model.',''},...
        'SelectionMode','single',...
        'ListString',classNames);
    outputAttributes.ClassID = classID;
    outputAttributes.ClassName = classNames{classID};
    if isfield(CommonHandles,'Tracking') && ~isempty(CommonHandles.Tracking)
        trackTable = CommonHandles.Tracking;
    end
    RegPos = CommonHandles.TrainingSet.RegPos;
    imName = CommonHandles.TrainingSet.OriginalImageName;
    origImName = CommonHandles.TrainingSet.OriginalImageName;
    class = CommonHandles.TrainingSet.Class;
    CellNumber = CommonHandles.TrainingSet.CellNumber;
    imExt = CommonHandles.ImageExtension;
    
    classIdx = cell2mat(class')';
    idx = logical(classIdx(:,classID));
    
    filteredImName = imName(idx);
%     filteredOrigImName = origImName(idx);
    filteredRegPos = RegPos(idx);
    filteredCellNumber = CellNumber(idx);
    
    rawOrACC = questdlg('Select the source of images','Image Source','Raw','ACC','ACC');
    if strcmp(rawOrACC, 'Raw')
        % raw image files
        rawPath = uigetdir([],'Select the path of raw images');
        rawImList = dir(fullfile(rawPath,'*.tiff'));
        rawImNames = {rawImList.name};
        outputAttributes.imageSource = rawPath;
        
        % load attribute file
        [attFile, attPath] = uigetfile(fullfile(rawPath,'*.txt'),'Select attribute file');
        fileID = fopen(fullfile(attPath,attFile));
        attributes = textscan(fileID, '%s%s');
        attributes = [attributes{1}, attributes{2}];
        nCh = str2double(attributes{strcmp(attributes(:,1),'numOfChannels'),2});
        channelIdx = zeros(nCh,1);
        outputAttributes.attPath = attPath;
        for attrCh = 1:nCh
            channelIdx(attrCh) = str2double(attributes{strcmp(attributes(:,1),['ch',num2str(attrCh)]),2});
        end
    else
        outputAttributes.imageSource = 'ACC_project';
    end
    
    
    imPath = fullfile(ouputPath,'Images');
    labelPaht =fullfile(ouputPath,'Labels');
    mkdir(imPath)
    mkdir(labelPaht)
    t = datetime('now');
    tString = ['_', num2str(t.Year), '_',num2str(t.Month), '_', num2str(t.Day), '_', num2str(t.Minute),'_id'];
    
    featureFileID = fopen(fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder,'featureNames.acc'));
    featureNameList = textscan(featureFileID,'%s');
    featureNameList = featureNameList{:};
    varTypes = ['string';repmat({'double'},length(featureNameList),1)];
    featureMatrix = table('Size',[length(filteredCellNumber),length(featureNameList)+1],'VariableTypes',varTypes,'VariableNames',['FileNames';featureNameList]);
    
    wb = waitbar(0,'Exporting training set elements. Please wait...');
    for i = 1:length(filteredImName)
        fullImage = [];
        if strcmp(rawOrACC, 'Raw')
            [filePath,accImName,ext] = fileparts(filteredImName{i});
            opName = convertFileNames_BIAS2Operetta(accImName);
            splitOpName = strsplit(opName,{'p','-ch0'});
            nameIdx = contains(rawImNames,splitOpName{1}) & contains(rawImNames,splitOpName{3});
            filteredRawNames = rawImNames(nameIdx)';
            for ch = 1:nCh
                chId = contains(filteredRawNames,['-ch',num2str(ch)]);
                if strcmp(zStack,'Yes')
                    if strcmp(dynamicFocusAnswer,'Yes')
                        fullImage(:,:,channelIdx(ch)) = dynamicFocus(rawPath,filteredRawNames(chId),0,0);
                    else
                        nameIdx2 = contains(filteredRawNames,['p',sprintf('%02d',str2double(zID))]);
                        fullImage(:,:,channelIdx(ch)) = imread(fullfile(rawPath,filteredRawNames{nameIdx2 & chId}));
                    end
                else
                    fullImage(:,:,channelIdx(ch)) = imread(fullfile(rawPath,filteredRawNames{chId}));
                end
            end
            while size(fillImage,3) < 3
                ch = ch+1;
                fullImage(:,:,ch) = zeros(size(fullImage,[1 2]));
            end
        else
            [filePath,accImName,ext] = fileparts(filteredImName{i});
            fullImage = imread(fullfile(CommonHandles.DirName,filteredImName{i}));
        end
        filePath = strsplit(filePath,filesep);
        Plate = filePath{1};
        cellID = filteredCellNumber{i};
        if isfield(CommonHandles,'Tracking') && ~isempty(CommonHandles.Tracking)
            selectedIdx = find(strcmp(trackTable.Plate, Plate) .* trackTable.ObjectID == cellID .* strcmp(trackTable.ImageName, [accImName,ext]));
            imName = [accImName,'_',tString,num2str(trackTable.TrackID(selectedIdx)),'_objID',num2str(trackTable.ObjectID(selectedIdx))];
        else
            imName = [accImName,'_',tString,num2str(i)];
        end
        
        if cropSize > 0
        % Cropped image
        SelectedMetaData = load(fullfile(CommonHandles.DirName,Plate,CommonHandles.MetaDataFolder,[accImName,'.txt']));
        featureMatrix.FileNames(i) = imName;
        featureMatrix{i,2:end} = SelectedMetaData(cellID, :);
        cx = SelectedMetaData(cellID, 1);
        cy = SelectedMetaData(cellID, 2);
        cropX = [cx - halfCropSize, cx - halfCropSize + cropSize - 1];
        cropY = [cy - halfCropSize, cy - halfCropSize + cropSize - 1];
        [im_sy, im_sx, ~] = size(fullImage);
        crop = fullImage(max(1,cropY(1)):min(im_sy, cropY(2)), max(1,cropX(1)):min(im_sx,cropX(2)), :);
        if strcmp(answerPadding,'Padding') && (cropX(1)<1 || cropY(1)<1 || cropX(2)>im_sx || cropY(2)>im_sy)
            if strcmp(answerPadVal,'Zero')
                padValue = 0;
            elseif strcmp(answerPadVal,'Mirror')
                padValue = 'symmetric';
            else
                [padValue, ~] = findMostFrequentColor(crop);
            end
            crop = calcPad4thumb(cropY, cropX, im_sy, im_sx, padValue, crop);
        end
        else
            crop = fullImage;
        end

        if ~isempty(crop) && ~isempty(filteredRegPos{:,i})
            imwrite(uint16(crop),fullfile(imPath,[Plate,'_',imName,'.tif']),'Compression','none')
            
            % % % Label coordinates as 2 px image
            imwrite(uint16(filteredRegPos{:,i} * 10),fullfile(labelPaht,[Plate,'_',imName,'.tif']));
        else
            warning(['Cell with ID_', num2str(filteredCellNumber{i}),' on image ',filteredImName{i},' does not added to the training set correctly. Please re-annotate this cell before saving the project!']);
        end
        if ishandle(wb)
            waitbar(i/length(filteredImName),wb,'Exporting training set elements. Please wait...');
        end
    end
    outputAttributes.NumberOfInstances = i;
    writestruct(outputAttributes,fullfile(ouputPath,'trSet_attributes.xml'))
    writetable(featureMatrix,fullfile(ouputPath,'FeatureMatrix.xlsx'));
    if ishandle(wb)
        waitbar(i/length(filteredImName),wb,'Export completed.');
    end
end