function [] = refreshClassesList()
% AUTHOR:	Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	refreshClassesList
% 
% To refresh the list of classes.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% this function refreshes the classes
%{   
    * it's called every time when something changes around the buttons
    * Classfields are the number of labels per class
    * 

    TODO
    manualTrain_callback and addSampleToTrainingSet should be separated
%}

global CommonHandles;
offset = 10;
incW = 0;

if isfield(CommonHandles,'TrainingSet') && ~isempty(CommonHandles.TrainingSet)...
        && isfield(CommonHandles,'Classes') && ~isempty(CommonHandles.Classes)
    % refresh number of cells
    cellNumInClass = sum(cell2mat(CommonHandles.TrainingSet.Class'),2)';
    for i=1:length(cellNumInClass)
        CommonHandles.Classes{i}.LabelNumbers = cellNumInClass(i);
    end
end

deleteClassButtons(CommonHandles);

for i=1:size(CommonHandles.Classes,2)
    incW = size(CommonHandles.Classes{i}.Icon,1);
    CommonHandles.Classbuttons{i} = uicontrol('Parent', CommonHandles.MainWindow ,'Style', 'pushbutton', 'String', '', 'CData', im2double(CommonHandles.Classes{i}.Icon), 'Position', [offset 15 (incW+5) (incW+5)], 'Callback', {@manualTrain_Callback, i, CommonHandles.MainWindow.SelectionType}, 'TooltipString', sprintf('%d: %s',i,CommonHandles.Classes{i}.Name));
    CommonHandles.ClassTextFields{i} = uicontrol('Parent', CommonHandles.MainWindow ,'Style', 'text', 'String', CommonHandles.Classes{i}.LabelNumbers, 'Position', [offset 1 incW+5 14],'FontUnits','pixels');
    CommonHandles.ShowTrainedCells{i} = uicontrol('Parent', CommonHandles.MainWindow, 'Style', 'pushbutton', 'String', '', 'Position', [offset+1 1 14 14], 'BackgroundColor', 'blue', 'Callback', {@showCells_Callback, i});
    %REGRESSION-REGEX
    if strcmp(CommonHandles.Classes{i}.Type,'Regression')        
        CommonHandles.ShowRegressionPlaneButtons{i} = uicontrol('Parent', CommonHandles.MainWindow, 'Style', 'pushbutton', 'String', 'R','FontSize',6, 'Position', [offset+incW-1-12 1 14 14], 'Callback',...
            ['global CommonHandles; CommonHandles.RegressionPlaneHandle = regressionPlane(''UserData'',struct(''idInTrainingSet'',0,''classIndex'',' num2str(i) ',''prediction'',-1));']);
    else
        CommonHandles.ShowRegressionPlaneButtons{i} = [];
    end
    offset = offset + 5 + incW;
    
    if(strcmp(CommonHandles.Classes{i}.Type,'Child') && strcmp(CommonHandles.Classes{i-1}.Type,'Normal'))
       set(CommonHandles.Classbuttons{i-1},'Enable','off');
       set(CommonHandles.ShowTrainedCells{i-1},'Enable','off');
    end
end

end


function manualTrain_Callback(hObject, ~, counter, selectionType)
% here user clicked to teach
global CommonHandles;

CommonHandles.MaxCluster = length(CommonHandles.Classes);

% if database is open
if (CommonHandles.Success == 1)
%     if CommonHanles.TrajLevelAnnotation == 1
        
%     else
        addSampleToTrainingSet(counter);
%     end
    % skip to the next image
    if CommonHandles.RandomTrain == 0
        
        %if we use active learning
        if (isfield(CommonHandles,'ActiveLearning') && CommonHandles.ActiveLearning)                        
            activeLearningStart();
        elseif (CommonHandles.ActiveRegression.on)
            activeLearningStart(1);
        else
            %we use passive learning and we jump to the next cell
            %if the next cell is on the selected picture
            if CommonHandles.SelectedCell < size(CommonHandles.SelectedMetaData, 1)
                CommonHandles.SelectedCell = CommonHandles.SelectedCell + 1;
                CommonHandles = showSelectedCell(CommonHandles);
            else
                %the next cell is on the next picture
                MaxImage = length(get(CommonHandles.ImageListHandle, 'String'));
                MaxPlate = length(get(CommonHandles.PlateListHandle, 'String'));
                if MaxImage > CommonHandles.SelectedImage
                    %jump to the first cell of the next image
                    ImageList = get(CommonHandles.ImageListHandle,'String');
                    index = getNextNonEmptyImage();
                    CommonHandles.SelectedImage = index;
                    cellInfo.CellNumber = 1;
                    cellInfo.ImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];                  
                    cellInfo.OriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
                    cellInfo.PlateName = CommonHandles.SelectedPlate;
                    jumpToCell(cellInfo,1);
                else
                    % jump next plate
                    CommonHandles.SelectedPlate = CommonHandles.SelectedPlate + 1;
                    if CommonHandles.SelectedPlate <= MaxPlate
                        CommonHandles.SelectedImage = 1;
                        
                        % the image list must be refreshed
                        ImageList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep '*' CommonHandles.ImageExtension]);
                        ImageListStr = [];
                        for i=1:length(ImageList)
                            ImageListStr = [ImageListStr; cellstr(ImageList(i).name)];
                        end
                        set(CommonHandles.ImageListHandle, 'String', ImageListStr);
                        
                        % setting up the struct for jumping
                        cellInfo.CellNumber = 1;
                        cellInfo.ImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep ImageList(CommonHandles.SelectedImage).name];
                        cellInfo.OriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep ImageList(CommonHandles.SelectedImage).name];
                        cellInfo.PlateName = CommonHandles.SelectedPlate;
                        
                        jumpToCell(cellInfo,1);
                    end
                end
            end
        end
    elseif CommonHandles.RandomTrain == 1        
        structure = randomCellProposal(CommonHandles);                
        jumpToCell(structure,1);
    end
    
     % display the size of the data set 
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i = 1: length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);        
    end

    refreshClassesList();
end
end

function showCells_Callback(hObject, eventdata, counter)     
    
    global CommonHandles;
    
    labelNumber = CommonHandles.Classes{counter}.LabelNumbers;
    
    if labelNumber > 0
        %To avoid duplicated trained cells GUI
        if isfield(CommonHandles,'ShowTrainedCellsFig')
            if ishandle(CommonHandles.ShowTrainedCellsFig)
                delete(CommonHandles.ShowTrainedCellsFig);
            end
        end
        CommonHandles.ShowTrainedCellsFig = showTrainedCells_GUI('UserData',counter);
    end
end