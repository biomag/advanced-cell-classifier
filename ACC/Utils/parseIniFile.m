function CommonHandles = parseIniFile(CommonHandles)
% AUTHOR:   Peter Horvath
% DATE:     April 22, 2016
% NAME:     parseIniFile
% 
% To set parameters read from the "acc.ini" file.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[PATH, ~, ~] = fileparts(mfilename('fullpath'));

fIni = fopen('acc.ini', 'r');
while 1
    tline = fgetl(fIni);
    if ~ischar(tline), break, end
    
    % salt
    if ismac
        sFoldPos = strfind(tline, 'SALTFolderForMac=');
    elseif isunix
        sFoldPos = strfind(tline, 'SALTFolderForLinux=');
    elseif ispc
        sFoldPos = strfind(tline, 'SALTFolderForWindows=');
    else
        disp('Cannot recognize platform');
    end
    
    if ~isempty(sFoldPos)
        sFoldPos = strfind(tline, '=');
        CommonHandles.SALT.folderName = [PATH filesep '..' filesep  '..' filesep tline(sFoldPos(end)+1:end)];
        CommonHandles.SALT.initialized=1;
        addpath(genpath(CommonHandles.SALT.folderName));
    end             
    
    % new plate type
    pTPos = strfind(tline, 'plateType=');
    if ~isempty(pTPos)
        if isfield(CommonHandles, 'PlateTypeInfo')
            idx = numel(CommonHandles.PlateTypeInfo) + 1;
        else
            idx  = 1;
        end
        sFoldPos = strfind(tline, '=');
        cPos = strfind(tline, ',');
        plateTypeName = tline(sFoldPos(1)+1:cPos(1)-1);
        col = str2num(tline(cPos(1)+1+1:cPos(2)-1));
        row = str2num(tline(cPos(2)+1+1:end));
        CommonHandles.PlateTypeInfo{idx}.name = plateTypeName;
        CommonHandles.PlateTypeInfo{idx}.col = col;
        CommonHandles.PlateTypeInfo{idx}.row = row;
        
    end
    
    % image file types
    iTPos = strfind(tline, 'imageType=');
    if ~isempty(iTPos)
        if isfield(CommonHandles, 'ImageExtensionNames')
            idx = numel(CommonHandles.ImageExtensionNames) + 1;
        else
            idx  = 1;
        end
        sFoldPos = strfind(tline, '=');
        extName = tline(sFoldPos(1)+1:end);
        CommonHandles.ImageExtensionNames{idx}.name = extName;        
    end        
        
end
fclose(fIni);
