function accImSeqAnimation

global CommonHandles

projectFolder = CommonHandles.DirName;
isContour = CommonHandles.ShowContour;
ext = CommonHandles.ImageExtension;
selectedImagePath = strsplit(CommonHandles.SelectedImageName,filesep);
if isContour
    imFolder = CommonHandles.ImageFolder;
else
    imFolder = CommonHandles.OriginalImageFolder;
end

imListAll = dir(fullfile(projectFolder,selectedImagePath{1},imFolder,['*',ext]));
fileNames = {imListAll.name}';
% TODO
% Get separator from user and info about the different image name sections

if isfield(CommonHandles,'TrackingPath') && isfolder(CommonHandles.TrackingPath)
    trackPath = CommonHandles.TrackingPath;
else
    trackPath = uigetdir('',"Select ""Tracking Folder""");
end

try
    trackingParams = readstruct(fullfile(trackPath,'TrackingParameters.xml'));
    CommonHandles.TrackingPath = trackPath;
catch
    warndlg({"Invalid Tracking Path"; ...
        "Please select a valid tracking folder or use the Tracking Wizard in the file menu"})
end
separators = trackingParams.separators;
fieldIDidx = trackingParams.fieldIDidx;
frameIDidx = trackingParams.frameIDidx;
bM = 1; % brigthness Multiplier

allFields = cell(length(fileNames),length(separators)+1);
keyFields = cell(length(fileNames),length(fieldIDidx));
for i=1:length(fileNames)
    [~,exEx,~] = fileparts(fileNames{i});
    if isempty(separators)
        allFields(i,:) = {exEx};
    else
        allFields(i,:) = strsplit(exEx,separators);
    end
    keyFields(i,:) = allFields(i,fieldIDidx);
end
framesInSequence = str2double(unique(allFields(:,2)));

concatCommand = 'strcat(';
for i=1:size(keyFields,2)-1
    concatCommand = [concatCommand 'keyFields(:,' num2str(i) '),']; %#ok<AGROW> not much
end
if isempty(i), i = 0; end
concatCommand = [concatCommand 'keyFields(:,' num2str(i+1) '));'];
[jointFieldIDs] = eval(concatCommand);

[uniqueFieldNames,~] = unique(jointFieldIDs);

frameIDs = str2double(allFields(:,frameIDidx));
selectedImNameSplitted = strsplit(selectedImagePath{end},[separators,'.']);

selectedFieldIdx = find(strcmp([selectedImNameSplitted{fieldIDidx}],uniqueFieldNames));

%%

for i = 1:length(selectedFieldIdx)
    rowBool = true(length(fileNames),1);
    for ii = 1:size(keyFields,2)
        rowBool = rowBool & strcmp(allFields(:,fieldIDidx(ii)),keyFields(selectedFieldIdx(i),ii));
    end
    currRowIds = find(rowBool); % Indexes of file names which make a sequence
    currFrameIDs = frameIDs(currRowIds); % Frame IDs of the sequence (not necessarily sorted)
    if numel(currFrameIDs) ~= numel(framesInSequence)
        errorFieldName = keyFields{selectedFieldIdx(i),1};
        if size(keyFields,2) > 1
            for ee = 2:size(keyFields,2)
                errorFieldName = [errorFieldName, 'x', keyFields{selectedFieldIdx(i),ee}];
            end
        end
        error(sprintf(['File system inconsistency.\nCheck files in plate named:\n', plateName,...
            '\nField: ' errorFieldName, ' has less frame than the maximum in the data.']))
    end

    [~,incIdx] = sort(currFrameIDs); % Sorted indexes
end

%%
aligmentFileName = [keyFields{selectedFieldIdx(i),1},separators{1},'$',separators{2},keyFields{selectedFieldIdx(i),2},'.csv'];
if isfield(CommonHandles,'AlignPath') && isfolder(CommonHandles.AlignPath)
    alignPath = CommonHandles.AlignPath;
else
    alignPath = uigetdir('',"Select ""Alignment Folder""");
end
try
    alignmentData = readmatrix(fullfile(alignPath,selectedImagePath{1},aligmentFileName));
    CommonHandles.AlignPath = alignPath;
catch
    warndlg({"Invalid Tracking Path"; ...
        "Please select a valid tracking folder or use the Tracking Wizard in the file menu"})
end

tWB = waitbar(0,'Please wait...','Name','Total progress');
for fID = 1:length(selectedFieldIdx) %field ID
    waitbar((fID/length(selectedFieldIdx)),tWB,'Please wait...','Name','Total progress');
    idx = currRowIds(incIdx(1));
    tempIm = imread(fullfile(projectFolder,selectedImagePath{1},imFolder,fileNames{idx}));
    imsize = size(tempIm);
    f = figure('Units', 'pixels');
    pause(1)
    ax = axes;
    minShift_x = min(alignmentData(:,2));
    minShift_y = min(alignmentData(:,1));
    maxShift_x = max(alignmentData(:,2));
    maxShift_y = max(alignmentData(:,1));
    imPlaceHolderSize = [imsize(1) + (maxShift_x - minShift_x), imsize(2) + (maxShift_y - minShift_y)];
    ax.XLim = [0 imPlaceHolderSize(2)];
    ax.YLim = [0 imPlaceHolderSize(1)];
    title(ax,['Frame ', '1']);
    pause(1)

    imPlaceHolder = uint8(zeros(imPlaceHolderSize(1), imPlaceHolderSize(2), 3));
    shift_x = minShift_x+alignmentData(1);
    shift_y = minShift_x+alignmentData(2);
    imPlaceHolder(1+shift_x:imsize(1) + shift_x,1+shift_y:imsize(2) + shift_y,:) = tempIm;


    image(ax,'CData',imPlaceHolder*bM,'AlphaData', 1);
    aniMovie = struct('cdata',cell(1,1+10*(length(currRowIds)-1)),'colormap',cell(1,1+10*(length(currRowIds)-1)));
    aniMovie(1) = getframe(f);

    for fr = 2:length(currRowIds)
        idx = currRowIds(incIdx(fr));
        tempIm = imread(fullfile(projectFolder,selectedImagePath{1},imFolder,fileNames{idx}));
        imPlaceHolder = uint8(zeros(imPlaceHolderSize(1), imPlaceHolderSize(2), 3));
        shift_x = minShift_x+alignmentData(1);
        shift_y = minShift_x+alignmentData(1);
        imPlaceHolder(1+shift_x:imsize(1) + shift_x,1+shift_y:imsize(2) + shift_y,:) = tempIm;
        imCurr = image(ax,...
            'CData',imPlaceHolder*bM,...
            'AlphaData', 0);
        for j = 1:10
            imCurr.AlphaData = j/10;
            if j == 5, title(ax, ['Frame ', num2str(fr)]); end
            drawnow
            aniMovie((fr-2)*10+j+1) = getframe(f);
        end
    end
    pause(1)
    [expFileName, expPath] = uiputfile('*.mp4','Save video','ImSeq_Animation');
    if ~isempty(expPath) & isfolder(expPath)
        v = VideoWriter(fullfile(expPath,expFileName),'MPEG-4');
        v.Quality = 95;
        v.FrameRate = 15;
        open(v)
        wb = waitbar(0,'Please wait...','Name','Rendering your video');
        L = length(aniMovie);
        for i = 1:L
            if mod(i,10)==0
                waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
                pause(0.1)
            end
            writeVideo(v, aniMovie(i))
        end
        close(wb)
        close(v)
    end
    close(f)
    close(tWB)
    fprintf('%d/%d ready\n',fID,length(selectedFieldIdx));
end