function imgToShow = fillBoundaries(imgToShow,color,width)
    if nargin<3
        width = 1;
    end
    width = min([width size(imgToShow,1) size(imgToShow,2)]);
    color = reshape(color,1,1,3);
    R = size(imgToShow,1);
    C = size(imgToShow,2);
    imgToShow(1:width,:,:) = repmat(color,width,C);
    imgToShow(end-width+1:end,:,:) = repmat(color,width,C);
    imgToShow(:,1:width,:) = repmat(color,R,width);
    imgToShow(:,end-width+1:end,:) = repmat(color,R,width);    
end
