function closeClassContainingFigures()
% AUTHOR:   Abel Szkalisity
% DATE:     September 25, 2016
% NAME:     closeClassContainingFigures
%
% Close all figures containing stored information about the classes.
% Usually these figures copy information by startup from the
% CommonHandles.Classes, the purpose of close is to force the reopen and
% thus provide always consistent information.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

if ishandle(CommonHandles.UploadCAMIOFigure)
    close(CommonHandles.UploadCAMIOFigure);
end
if ishandle(CommonHandles.ShowTrainedCellsFig)
    close(CommonHandles.ShowTrainedCellsFig);
end

end

