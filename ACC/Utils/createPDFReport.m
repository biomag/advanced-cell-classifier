function createPDFReport(outputName, plateName, hitRate, cellNumber, h1)
% AUTHOR:   Peter Horvath
% DATE:     April 22, 2016
% NAME:     createPDFReport
%
% This function creates a report figure of cell number hit-rate
% and saves as pdf.
%
% INPUT:
%   outputName: name of the pdf file potentially with path
%   plateName: plate name that will be in the header of the report
%   hitRate: an nxm matrix of hit-rates
%   cellNumber: an nxm matrix of cell numbers
% OUTPUT:
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


warning('off', 'all');

[r, c] = size(hitRate);

xLabels = 1:r;
yLabels = char('A'):char('A'+c); yLabels = yLabels';


% font size
smallFont = 6; midFont = 8; largeFont = 10;
if c > 12
    smallFont = 4; midFont = 6; largeFont = 8;
end

% create report
subplot(4, 2, 1);
imagesc((hitRate)');
colormap(hot);
colorbar;
title('Hit-rate', 'FontSize', largeFont);
set(gca,'XTick',1:r)
set(gca,'YTick',1:c)
set(gca,'XTickLabel',1:r)
set(gca,'YTickLabel',yLabels);
set(gca,'FontSize',smallFont);
xlabel('col');
ylabel('row');
caxis([0 1]);

subplot(4, 2, 2);
imagesc((cellNumber)');
colormap(hot);
colorbar;
title('Cell number', 'FontSize', largeFont);
set(gca,'XTick',1:r)
set(gca,'YTick',1:c)
set(gca,'XTickLabel',1:r)
set(gca,'YTickLabel', yLabels);
set(gca,'FontSize',smallFont);
xlabel('col');
ylabel('row');
cmax = prctile(cellNumber(:), 98);
caxis([0 cmax]);

subplot(4, 2, 3);
hist(hitRate(:), linspace(0, 1, 32), 'FaceColor', 'r', 'EdgeColor', 'y');
title('Hit-rate distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
h = findobj(gca,'Type','patch');
h.FaceColor = [.25 .25 .25];
h.EdgeColor = 'k';
ylabel('hit-rate');
xlabel('frequency');
set(gca,'FontSize',midFont);
axis([0 1 0 inf]);

subplot(4, 2, 4);
cellMax = prctile(cellNumber(:), 98);
if cellMax == 0
    cellMax = 1;
end
hist(cellNumber(:), linspace(0, cellMax, 32), 'FaceColor', 'r', 'EdgeColor', 'y');
title('Cell number distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
h = findobj(gca,'Type','patch');
h.FaceColor = [.5 .5 .5];
h.EdgeColor = 'k';
ylabel('cell number');
xlabel('frequency');
set(gca,'FontSize',midFont);
axis([0 cellMax 0 inf]);

subplot(4, 2, 5);
bar(mean(hitRate), 'FaceColor', [.25 .25 .25]); hold on;
errorbar(1:c, mean(hitRate), zeros(1, c), std(hitRate)./2, 'k', 'linestyle', 'none');
title('Row-wise hit-rate distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
set(gca,'XTick',1:c)
set(gca,'XTickLabel', yLabels);
ylabel('hit-rate');
xlabel('row');
set(gca,'FontSize',midFont);
axis([0 c+1 0 1]);

subplot(4, 2, 6);
bar(mean(cellNumber), 'FaceColor', [.5 .5 .5]); hold on;
errorbar(1:c, mean(cellNumber), zeros(1, c), std(cellNumber)./2, 'k', 'linestyle', 'none');
title('Row-wise cell number distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
set(gca,'XTick',1:c)
set(gca,'XTickLabel', yLabels);
ylabel('cell number');
xlabel('row');
set(gca,'FontSize',midFont);
axisMax = max(mean(cellNumber)) + max(std(cellNumber));
if axisMax == 0
    axisMax = 1;
end
axis([0 c+1 0 axisMax]);

subplot(4, 2, 7);
bar(mean(hitRate'), 'FaceColor', [.25 .25 .25]); hold on;
errorbar(1:r, mean(hitRate'), zeros(1, r), std(hitRate')./2, 'k', 'linestyle', 'none');
title('Column-wise hit-rate distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
set(gca,'XTick',1:r)
set(gca,'XTickLabel', 1:r);
ylabel('hit-rate');
xlabel('column');
set(gca,'FontSize',midFont);
axis([0 r+1 0 1]);


subplot(4, 2, 8);
bar(mean(cellNumber'), 'FaceColor', [.5 .5 .5]); hold on;
errorbar(1:r, mean(cellNumber'), zeros(1, r), std(cellNumber')./2, 'k', 'linestyle', 'none');
title('Column-wise cell number distribution', 'FontSize', largeFont);
set(gca,'FontSize',8);
set(gca,'XTick',1:r)
set(gca,'XTickLabel', 1:r);
ylabel('cell number');
xlabel('column');
set(gca,'FontSize',midFont);
axisMax = max(mean(cellNumber)) + max(std(cellNumber));
if axisMax == 0
    axisMax = 1;
end
axis([0 r+1 0 axisMax]);


ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');
h = text(0.5, 1,['Statistics of plate: ' plateName],'HorizontalAlignment','center','VerticalAlignment', 'top','Interpreter','none');
set(h,'FontWeight','bold');

print(h1, '-dpdf', outputName,'-fillpage');

close(h1);

warning('on', 'all');
