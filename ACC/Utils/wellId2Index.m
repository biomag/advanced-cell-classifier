function [ row,col ] = wellId2Index( wellID )
% AUTHOR:   Abel Szkalisity
% DATE:     Oct 31, 2016
% NAME:     wellId2Index
%
% Transforms a wellId string to row and column indices. Like: C02 -> [3,2]
%
% INPUT:
%   wellID          string with format #?? e.g. F12, the wellID.
%
% OUTPUTS:
%   row             the row of the well
%   col             the column of the well
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

row = wellID(1)-'A'+1;
col = str2double(wellID(2:3));

end

