function removePlate(plateNumber,remFromTS)
% AUTHOR:	Tamas Balassa
% DATE: 	June 17, 2016
% NAME: 	removePlate
% 
% To remove one plate from the dataset
%
% INPUT:
%        plateNumber         The number of the plate that will be removed
%        remFromTS           Remove from training set. Bool. If it is true
%                            then the plate elements are also deleted from
%                            the training set, otherwise not. Optional,
%                            default is false.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    if nargin<2
        remFromTS = 0;
    end
    
    % update plates in AllFeaturesMapping
    if isfield(CommonHandles,'AllFeaturesMapping') && ~isempty(CommonHandles.AllFeaturesMapping)
       for i=1:size(CommonHandles.AllFeaturesMapping,2)
           if CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber == plateNumber
               CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber = 0;
           elseif CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber > plateNumber
               CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber = CommonHandles.AllFeaturesMapping{i}.CurrentPlateNumber - 1;
           end
       end
    end
    
    % update plates in ClassImages
    if CommonHandles.ClassImages.Count == size(CommonHandles.Classes,2)
        for i=1:size(CommonHandles.Classes,2)
            classImgs = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name);
            imgs = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name).Images;
            for j=size(imgs,2):-1:1
                if imgs{j}.PlateName == plateNumber
                    if remFromTS
                        %then remove from classes
                        imgs(j) = [];
                    else
                        %only set it's index to 0
                        imgs{j}.PlateName = 0;  
                    end                                          
                elseif imgs{j}.PlateName > plateNumber
                    imgs{j}.PlateName = imgs{j}.PlateName - 1;
                end
            end
            classImgs.Images = imgs;
            CommonHandles.ClassImages(CommonHandles.Classes{i}.Name) = classImgs;
        end
    end
    
    if remFromTS
        %delete from trainingSet
        for i=length(CommonHandles.TrainingSet.MetaDataFilename):-1:1
            SSP = strsplit(CommonHandles.TrainingSet.MetaDataFilename{i},{'\','/'});
            if strcmp(SSP{1},CommonHandles.PlatesNames{plateNumber})
                deleteFromTrainingSet(i);
            end
        end
    end
    
    % remove the plate from the plates
    CommonHandles.PlatesNames(plateNumber) = [];