function [names] = getSpecClassList()
% AUTHOR:	Abel Szkalisity
% DATE: 	October 21, 2018
% NAME: 	getSpecClassList
% 
% Reorganized function for reuse. The original (and new also) purpose is
% to fetch a stringlist (cellarray) from the global CommonHandles
% structure, where the elements of the stringlist are the class names for
% those classes into which it is possible to move cells. (The problematic
% case is the parent-child class relation, where the specification says
% that the parent class is not accepting any more cells for annotation).
%
% INPUT:
%   global CommonHandles
%
% OUTPUT:
%   names         	Cellarray with the appropriate class names.
%
% COPYRIGHT ???

    global CommonHandles;
    names = [];
    pParent = '';
    for i=1:length(CommonHandles.Classes)
        if strcmp(CommonHandles.Classes{i}.Type,'Child')
            names{end+1} = CommonHandles.Classes{i}.Name;
            pParent = '';
        elseif strcmp(CommonHandles.Classes{i}.Type,'Normal') || strcmp(CommonHandles.Classes{i}.Type,'Regression')
            if ~strcmp(pParent,'')
                names{end+1} = pParent;
            end
            pParent = CommonHandles.Classes{i}.Name;
        end
    end
    if ~strcmp(pParent,'')
        names{end+1} = pParent;
        pParent = '';
    end

end

