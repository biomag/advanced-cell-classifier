function saveClassificationData(classPosition,featureVector)
% AUTHOR:   Abel Szkalisity
% DATE:     July 29, 2016
% NAME:     saveClassificationData
%
% Stores data of the lastly classified cell to the last entry of
% CommonHandles's TrainingSet field.
%
% INPUT:
%   classPosition	The index of the class where classify the last cell
%   featureVector   The feature vector of the cell to add to the
%                   trainingSet
%
% CommonHandles is a global input-output.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

CommonHandles.TrainingSet.MetaDataFilename = [CommonHandles.TrainingSet.MetaDataFilename; CommonHandles.SelectedMetaDataFileName];
CommonHandles.TrainingSet.ImageName = [CommonHandles.TrainingSet.ImageName; CommonHandles.SelectedImageName];
CommonHandles.TrainingSet.OriginalImageName = [CommonHandles.TrainingSet.OriginalImageName; CommonHandles.SelectedOriginalImageName];
CommonHandles.TrainingSet.CellNumber = [CommonHandles.TrainingSet.CellNumber; CommonHandles.SelectedCell];
ClassVector = zeros(CommonHandles.MaxCluster,1);
ClassVector(classPosition) = 1;
CommonHandles.TrainingSet.Class = [CommonHandles.TrainingSet.Class; ClassVector];
CommonHandles.TrainingSet.Features = [CommonHandles.TrainingSet.Features; featureVector];
CommonHandles.TrainingSet.Timestamp = [CommonHandles.TrainingSet.Timestamp; {clock}];
CommonHandles.TrainingSet.RegPos = [CommonHandles.TrainingSet.RegPos, cell(1,1)];

indices = bloomFilter(featureVector);
CommonHandles.TrainingSet.BloomFilter(indices) = CommonHandles.TrainingSet.BloomFilter(indices)+1;


end

