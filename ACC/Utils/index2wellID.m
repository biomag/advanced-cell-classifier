function [ wellID ] = index2wellID( row,col )
% AUTHOR:   Abel Szkalisity
% DATE:     Oct 31, 2016
% NAME:     index2wellID
%
% Transforms row and column indices to well id. Like: [3,2] -> C02
%
% INPUTS:
%   row             the row of the well
%   col             the column of the well
%
% OUTPUT:
%   wellID          string with format #?? e.g. F12, the wellID.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

wellID = ['A'+row-1 num2str(col,'%02d')];
end