function [outimg, className, mapping] = createClassImage(counter)
% AUTHOR:	Lassi Paavolainen, Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	createClassImage
% 
% To create the table-like image containing the "icon images" of a class of
% annotated cells.
%
% INPUT:
%   counter         Identification number assigned to the class.
%
% OUTPUT:
%   outimg          Contain the table for containing the "icon images".
%   className       Name of the class.
%   mapping         Contains PlateName, ImageName, OriginalImageName and
%                   CellNumber for each cell shown.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;

    imgsize = CommonHandles.ClassImageInfo.ImageSize;
    sepsize = CommonHandles.ClassImageInfo.SepSize;
    cols = CommonHandles.ClassImageInfo.Cols;
    width = cols * imgsize(1) + (cols+1) * sepsize;

    if counter > 0
        className = CommonHandles.Classes{counter}.Name;
        classCells = CommonHandles.ClassImages(className).Images;
    else
        className = 'Similar cells';
        classCells = CommonHandles.SimilarCells.Images;
    end
    
    numimgs = size(classCells,2);
    rows = ceil(numimgs / cols);
    height = rows * imgsize(2) + (rows+1) * sepsize;
    outimg = uint8(zeros(height,width,3));
    outimg(:,:,:) = 255;
    
    mapping = {};
    x = 1;
    y = 1;
    for i = 1:numimgs
        classImg = classCells{i};
        resImg = im2uint8(imresize(classImg.Image, imgsize));
        if x > cols
           x = 1;
           y = y + 1;
        end
        offsetw = imgsize(1) * (x-1) + sepsize * x;
        offseth = imgsize(2) * (y-1) + sepsize * y;
        outimg(offseth:offseth+imgsize(2)-1,offsetw:offsetw+imgsize(1)-1,:) = resImg;
        subimgmeta.CellNumber = classImg.CellNumber;
        subimgmeta.ImageName = classImg.ImageName;
        subimgmeta.OriginalImageName = classImg.OriginalImageName;
        subimgmeta.PlateName = classImg.PlateName;
        mapping{y,x} = subimgmeta;
        x = x + 1;
    end
end