function recreateBloomFilter()
% AUTHOR:   Abel Szkalisity
% DATE:     March 24, 2017
% NAME:     recreateBloomFilter
%
% Creates a bloom filter to the training set if the first element of the
% training set is not yet in the filter.
%
% global input-output: CommonHandles
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

trainingSetSize = length(CommonHandles.TrainingSet.Features);
if trainingSetSize~=0
    if ~all(CommonHandles.TrainingSet.BloomFilter(bloomFilter(CommonHandles.TrainingSet.Features{1})))
        for i=1:trainingSetSize
            indices = bloomFilter(CommonHandles.TrainingSet.Features{i});
            CommonHandles.TrainingSet.BloomFilter(indices) = CommonHandles.TrainingSet.BloomFilter(indices)+1;
        end
    end

end

