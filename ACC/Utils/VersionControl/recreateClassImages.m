function recreateClassImages()
% AUTHOR:	Abel Szkalisity
% DATE: 	April 22, 2016
% NAME: 	recreateClassImages
% 
% This function tries to recover the class images from the training set
% data it is very similar to the recreateClasses.m Here we suppose that we
% the Classes array is already filled in.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

% empty the previous ClassImages
CommonHandles.ClassImages = containers.Map;

reserveCH = CommonHandles;
classMatrix = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.TrainingSet.Class{1}),length(CommonHandles.TrainingSet.Class))';
counter = 1;

waitBarHandle = waitbar(0,'Recreate cell images...');
for i=1:length(CommonHandles.Classes)            
        
    firstIndex = find(classMatrix(:,i));
    
    for k=1:length(firstIndex)
        %splitted image name
        SINbyfilesep = strsplit(CommonHandles.TrainingSet.ImageName{firstIndex(k)},{'/','\'});
        %lastbyfilesep = SINbyfilesep{end};
        %SINbyunderscore = strsplit(lastbyfilesep,'_');
        for j=1:length(CommonHandles.PlatesNames)
            if strcmp(CommonHandles.PlatesNames{j},SINbyfilesep{1})
                break;
            end
        end
        cellStruct.ImageName = CommonHandles.TrainingSet.ImageName{firstIndex(k)};
        cellStruct.OriginalImageName = CommonHandles.TrainingSet.OriginalImageName{firstIndex(k)};        
        cellStruct.CellNumber = CommonHandles.TrainingSet.CellNumber{firstIndex(k)};                
        cellStruct.PlateName = j;
        jumpToCell(cellStruct,0);
        
        addToClassImages(i);
        
        donePercent = double(counter/(size(classMatrix,1)));
        waitText = sprintf('Class %s/%s; Create icons...  %d%% done', num2str(i),  num2str(length(CommonHandles.Classes)), int16(donePercent * 100));
        if ishandle(waitBarHandle)
            waitbar(donePercent, waitBarHandle, waitText);
        end
        counter = counter + 1;
        
    end
            
end

if ishandle(waitBarHandle)
    close(waitBarHandle);
end
CommonHandles.SelectedImageName = reserveCH.SelectedImageName;
CommonHandles.SelectedOriginalImageName = reserveCH.SelectedOriginalImageName;
CommonHandles.SelectedCell = reserveCH.SelectedCell;

end

