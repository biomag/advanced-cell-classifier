function closeSubGUI(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     February 28, 2017
% NAME:     closeSubGUI
%
% Closes the potentially opened GUIs that are specified below in the
% deleteGUIList. Proposed usage: call if new data is loaded or for some
% reason the GUI need to be reset.
%
% INPUT:
%   CommonHandles   ACC's data structure, that contains the handles (or in
%                   newer version the graphical objects) for the GUIs that
%                   need to be closed.
%
% OUTPUT:
%   The function has the side effect that the given figures are closed.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


    deleteGUIList = {...       
        'HC.ShowCellsFig',...
        'outlierTreeHandle',...        
        'ShowHighProbCellsHandle',...
        'ShowSimilarCellsHandle',...
        'ShowTrainedCellsFig',...
        'PredictRegressionGUI',...
        'trainReg_GUI',...
        'colorFrameHandle',...
        'RegressionPlaneHandle',...
        'UploadCAMIOFigure',...          
        };
    for i=1:length(deleteGUIList)
        if (isfieldRecursive(CommonHandles,deleteGUIList{i}))
            isOpened = eval(['ishandle(CommonHandles.' deleteGUIList{i} ');']);
            if (isOpened)
                eval(['close(CommonHandles.',deleteGUIList{i},');']);
            end        
        end 
    end
    
    if isfield(CommonHandles,'RegressionPlane')
        for idx = 1:length(CommonHandles.RegressionPlane)
            if isfield(CommonHandles.RegressionPlane{idx},'trajGUIhandle') && ~isempty(CommonHandles.RegressionPlane{idx}.trajGUIhandle) && ishandle(CommonHandles.RegressionPlane{idx}.trajGUIhandle)
                close(CommonHandles.RegressionPlane{idx}.trajGUIhandle);
            end
        end
    end
end

