function CommonHandles = resetOSInClassImgs(CommonHandles)
% AUTHOR:	Abel Szkalisity
% DATE: 	Aug 27, 2018
% NAME: 	resetOSInClassImgs
% 
% This function iterates through all the entries in
% CommonHandles.ClassImgs which contains the information about the members
% of each class such as icons and jump to it data. Then during the
% iteration it changes the OS path file separators to the appropriate one.
% It decides based on the 1st entry if the whole iteration is needed or
% not. (i.e. if the OS was changed since the last usage)
%
%   INPUT-OUTPUT:
%       CommonHandles   ACC structure for storing information
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


tempKeys = CommonHandles.ClassImages.keys;
for i=1:length(tempKeys)
    if isempty(CommonHandles.ClassImages(tempKeys{i}).Images), continue; end
    tempStruct = CommonHandles.ClassImages(tempKeys{i}).Images{1};
    %if the filesep splits the struct then we are ok
    if length(strsplit(tempStruct.ImageName,filesep)) > 1, return; end
    tempClass = CommonHandles.ClassImages(tempKeys{i});
    for j=1:length(tempClass.Images)
        SIN = strsplit(tempClass.Images{j}.ImageName,{'\','/'});
        tempClass.Images{j}.ImageName = [SIN{end-2} filesep SIN{end-1} filesep SIN{end}];
        SIN = strsplit(tempClass.Images{j}.OriginalImageName,{'\','/'});
        tempClass.Images{j}.OriginalImageName = [SIN{end-2} filesep SIN{end-1} filesep SIN{end}];
    end
    CommonHandles.ClassImages(tempKeys{i}) = tempClass;
end


end