function clearDuplicationsFromTrainingSet()
% AUTHOR:   Abel Szkalisity
% DATE:     May 26, 2020
% NAME:     drawRegressionPlane
%
% Clears duplicated trainin samples from the TS. Duplicated training
% samples may be present only in legacy projects, in new ones they are
% prevented by other measures.
%
% INPUT-OUTPUT: The global CommonHandles structure is modified in this
%               function.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


global CommonHandles;
indicesToDelete = [];
nofClasses = length(CommonHandles.Classes);
indicesToDeletePerClass = cell(1,nofClasses);

for i=1:length(CommonHandles.TrainingSet.ImageName)
    [ alreadyInTrainingSet ] = isInTrainingSetAll( CommonHandles.TrainingSet.ImageName{i}, CommonHandles.TrainingSet.CellNumber{i}, false, CommonHandles);
    if length(alreadyInTrainingSet)~=1
%         fprintf('Multiplication: ');
%         for j=alreadyInTrainingSet
%             fprintf('%d ',j);
%         end        
%         fprintf('\n');
        
        indicesForThisCellInClassImgs = [];
        
        %Colletct also which indices to delete from the classimgs
        currClass = find(CommonHandles.TrainingSet.Class{i});
        classimgs = CommonHandles.ClassImages(CommonHandles.Classes{currClass}.Name);
        for j=1:length(classimgs.Images)
            if strcmp(classimgs.Images{j}.ImageName,CommonHandles.TrainingSet.ImageName{i}) && classimgs.Images{j}.CellNumber == CommonHandles.TrainingSet.CellNumber{i}
                indicesForThisCellInClassImgs = [indicesForThisCellInClassImgs j];
            end
        end
        
        if length(indicesForThisCellInClassImgs) ~= length(alreadyInTrainingSet)
%             disp('That''s really really strange'); %--> search also in other classes
            perClassIndices = cell(1,nofClasses);
            for klass = setdiff(1:nofClasses,currClass)
                if CommonHandles.ClassImages.isKey(CommonHandles.Classes{klass}.Name)
                    classimgs = CommonHandles.ClassImages(CommonHandles.Classes{klass}.Name);
                    for j=1:length(classimgs.Images)
                        if strcmp(classimgs.Images{j}.ImageName,CommonHandles.TrainingSet.ImageName{i}) && classimgs.Images{j}.CellNumber == CommonHandles.TrainingSet.CellNumber{i}
                            perClassIndices{klass} = [perClassIndices{klass} j];
                        end
                    end
                end
            end
            perClassIndices{currClass} = indicesForThisCellInClassImgs;
            
            %Check if like this it is correct
            if sum(cellfun(@length,perClassIndices)) == length(alreadyInTrainingSet)
                %Delete ALL as it is impossible to say which class is the
                %correct
                indicesToDelete = [indicesToDelete alreadyInTrainingSet];
                for iii=1:nofClasses, indicesToDeletePerClass{iii} = [indicesToDeletePerClass{iii} perClassIndices{iii}]; end
            else
%                 disp('Problem is even greater');
            end
            
        else
            indicesToDelete = [indicesToDelete alreadyInTrainingSet(2:end)];
            indicesToDeletePerClass{currClass} = [indicesToDeletePerClass{currClass} indicesForThisCellInClassImgs(2:end)];
        end
    elseif alreadyInTrainingSet ~= i
%         disp('The index is unique but it''s at another place');
    end
end

%unify
indicesToDelete = unique(indicesToDelete);
for i=1:nofClasses, indicesToDeletePerClass{i} = unique(indicesToDeletePerClass{i}); end

% disp(indicesToDelete);
% disp(indicesToDeletePerClass);

if ~isempty(indicesToDeletePerClass)
%     disp('All collected');
end

if ~isempty(indicesToDelete)
    deleteFromTrainingSet(indicesToDelete);
    refreshClassesList;
end
for i=1:nofClasses
    if ~isempty(indicesToDeletePerClass{i})
        classImg = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name);
        classImg.Images(indicesToDeletePerClass{i}) = [];
        CommonHandles.ClassImages(CommonHandles.Classes{i}.Name) = classImg;
    end
end

end

function [ alreadyInTrainingSet ] = isInTrainingSetAll( imageName, cellNumber, original, CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 24, 2016
% NAME:     isInTrainingSet
%
% Checks whether the provided imageName, cellNumber pair is already listed
% in the training set.
%
% CUSTOM VERSION TO FIND ALL INSTANCES NOT JUST THE FIRST ONE!
%
% INPUT:
%   imageName     	The queried image name.
%   cellNumber      The number of the cell within that image
%   original        Bool value. If true then imageName is compared with the
%                   originalImageName, if its false then with the contour
%                   image
%   CommonHandles   Not global input because the function is used in PCT.
%   features        OPTIONAL. This is the feature vector of the queried
%                   cell. If it is provided then the set membership is
%                   first examined with a Bloom-filter.
%
% OUTPUT:
%   alreadyInTrainingSet    the index in the training set if the pair is already in
%                           and zero if it's not.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%global CommonHandles;

alreadyInTrainingSet = [];


if original
    for i=1:length(CommonHandles.TrainingSet.OriginalImageName)    
        if (strcmp(imageName,CommonHandles.TrainingSet.OriginalImageName{i}) && cellNumber == CommonHandles.TrainingSet.CellNumber{i})
            alreadyInTrainingSet(end+1) = i;            
        end
    end
else
    for i=1:length(CommonHandles.TrainingSet.ImageName)    
        if (strcmp(imageName,CommonHandles.TrainingSet.ImageName{i}) && cellNumber == CommonHandles.TrainingSet.CellNumber{i})
            alreadyInTrainingSet(end+1) = i;                        
        end
    end
end

%Do in this way as this requires a big matrix of the feats (sanity check)
if nargin>4 && alreadyInTrainingSet == 0
    feats = cell2mat(CommonHandles.TrainingSet.Features);
    [alreadyInTrainingSet] = find(any(all(bsxfun(@eq,features,feats),2),3));
    if isempty(alreadyInTrainingSet)
        alreadyInTrainingSet = 0;
    end
end

end


