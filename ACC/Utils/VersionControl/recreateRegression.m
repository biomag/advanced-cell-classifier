function recreateRegression
% AUTHOR:   Abel Szkalisity
% DATE:     August 16, 2016
% NAME:     recreateRegression
%
% This function updates old regression format to the new (ACCv2.0) one.
% Basically it moves data from the old CH.Regression{}.ImagePositions to the
% CH.TrainingSet.RegPos accordingly. This function removes the old
% Regression and RegressionLastClass fields from CH.
%
%   CommonHandles is a global input-output
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

if isfield(CommonHandles,'Regression') && ~isempty(CommonHandles.Regression) && isfield(CommonHandles,'RegressionClass')
    regIndices = find(CommonHandles.RegressionClass);
    %for all regression classes
    for i=convert2RowVector(regIndices)        
        %move simultaneously on training set and regression
        k = 1;        
        for j = 1:length(CommonHandles.Regression{i}.ImagePosition)
            while ( find(CommonHandles.TrainingSet.Class{k})~= i)
                k = k+1;                
            end
            CommonHandles.TrainingSet.RegPos{k} = CommonHandles.Regression{i}.ImagePosition{j};            
            k = k+1;
        end
    end
end

if isfield(CommonHandles,'Regression') 
    CommonHandles = rmfield(CommonHandles,'Regression');    
end
if isfield(CommonHandles,'RegressionLastClass')
    CommonHandles = rmfield(CommonHandles,'RegressionLastClass');
end

end

