function matchOSfilesep()
% AUTHOR:	Abel Szkalisity
% DATE: 	August 25, 2017
% NAME: 	matchOSfilesep
%
% Changes the file separator according to the current operating system in
% the global CommonHandles variable.
%
% INPUT-OUTPUT: global CommonHandles
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

CommonHandles = iterateOverCellAndStruct(CommonHandles,@changeOSFilesepInChar);
for i=1:length(CommonHandles.Classes)    
    if CommonHandles.ClassImages.isKey(CommonHandles.Classes{i}.Name)
        classImgs = CommonHandles.ClassImages(CommonHandles.Classes{i}.Name);
        classImgs = iterateOverCellAndStruct(classImgs,@changeOSFilesepInChar);
        CommonHandles.ClassImages(CommonHandles.Classes{i}.Name) = classImgs;                
    end
end

end

function output = changeOSFilesepInChar(input)
%change filesep only if the variable is string
    if ischar(input)
        splitted = strsplit(input,{'\','/'});
        output = strjoin(splitted,filesep);
    else
        output = input;
    end
end

function currElement = iterateOverCellAndStruct(currElement, funcToExec)
%Iterates over embedded structures, cell arrays recursively and if reaches
%the final level it executes the final level function

if isstruct(currElement)
    fieldNames = fieldnames(currElement);
    for i=1:length(fieldNames)
        currElement.(fieldNames{i}) = iterateOverCellAndStruct(currElement.(fieldNames{i}),funcToExec);
    end
elseif iscell(currElement)
    for i=1:length(currElement)
        currElement{i} = iterateOverCellAndStruct(currElement{i},funcToExec);
    end
else
    currElement = funcToExec(currElement);
end

end