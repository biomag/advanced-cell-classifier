function [ classImgObj,idxInItsClass ] = getClassImgObjByTrainingSetID(CommonHandles, id, mergedClassVectors)
% AUTHOR:   Abel Szkalisity
% DATE:     June 28, 2016
% NAME:     getClassImgObjByTrainingSetID
%
% To fetch the corresponding classImage structure by the index of the cell
% in training set.
%
% INPUT:
%   CommonHandles       the global CH, gived as parameter so that the function
%                       can be used in batch.
%   id                  index of cell in training set
%   mergedClassVectors  if you want to speed up the work of this function
%                       you can provide the mergedClassVectors for it.
%
% OUTPUT:
%   classImgObj     the corresponding classImage structure.
%   idxInItsClass   the relative index of the cell in its class
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

className = CommonHandles.Classes{logical(CommonHandles.TrainingSet.Class{id})}.Name;

%we assume that the order within classImages is the same as in the training
%set
cimgs = CommonHandles.ClassImages(className);
if nargin<3
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
end
idxInItsClass = sum(mergedClassVectors(1:id,logical(CommonHandles.TrainingSet.Class{id})));

%for i=1:length(cimgs.Images)
%    if (cimgs.Images{i}.CellNumber == CommonHandles.TrainingSet.CellNumber{id} && strcmp(cimgs.Images{i}.ImageName,CommonHandles.TrainingSet.ImageName{id}))
        classImgObj = cimgs.Images{idxInItsClass};
%        return;
%    end
%end

end

