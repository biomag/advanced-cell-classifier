function newData = backgroundAL(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 25, 2016
% NAME:     backgroundAL
%
% To run the active learning proposal in background. Wrap function-
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%Make sure to have the javapaths also on the parallel workers.
sacInit(CommonHandles.SALT.folderName);
%Propose a new cell.
[newData.plate,newData.image,newData.cellNumber] = CommonHandles.AL.proposeNextCell(50,CommonHandles);

