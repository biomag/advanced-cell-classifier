classdef ActiveLearner
% AUTHOR:	Abel Szkalisity
% DATE: 	November 21, 2019
% NAME: 	ActiveLearner
%
% Abstract class to represent active learning algorithms.
% This class is an interface for different query strategies. 
%
% NOTE: The class must have a constructor which waits for only one
% parameter a cellarray, which follows the format given back by the
% getParameters static method.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC-RP) Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath, Abel Szkalisity
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties
        sizeOfInitialSet
    end
    
    methods (Abstract)        
        % proposeNextCell The query strategy is implemented here
        % The most important function of ActiveLearners. If it is called,
        % it must propose samples for labeling to the oracle. The function
        % should not have to jump the GUI only give back the possible
        % samples by providing its identification (plate, image, and cell
        % number). They're all by numbers. It can happen that we want more
        % than one cell to propose, we can ask for that by setting
        % nofCells.
        %
        % INPUTS:
        %   nofCells         an integer, the required amount of proposed
        %                    cells (warning: it is not assured during the
        %                    calls of this function, that nofCells is less
        %                    then the number of all available cells, hence
        %                    this needs to be checked in any
        %                    implementation)            
        %   CommonHandles    containing the training data and the
        %                    selected classifier, the ACC basic variable.
        %   
        % OUTPUTS:
        %   plate            an integer array with nofCells element. Each
        %                    entry identifies the plate number (not name)
        %                    for a single proposed cell
        %   image            A cellarray containing strings having the
        %                    length of nofCells. Each entry gives back the
        %                    image name of the proposed cell (the contour
        %                    image name with it's full path)
        %   cellNumber       an integer array having again nofCells
        %                    entries each identifying the ID of the
        %                    proposed cell within the image.
        %   
        % NOTE: even if there are not enough samples in the sampling pool
        % to satisfy nofCells pieces of proposals still the arrays must be
        % filled in with 0s and empty cell array entries.                
        [plate,image,cellNumber] = proposeNextCell(obj,nofCells,CommonHandles)        
        
        % getParameterValues Query for the values of user parameters
        % This functions returns the actual parameters of the active
        % learner in a similar format to getParameters function. See the
        % output of fetchUIControlValues.
        % 
        % OUTPUTS:
        %   paramarray       a cellarray with the parameter values. The order 
        %                    of the parameters is the same as in the
        %                    getParameters function.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
        paramarray = getParameterValues(obj);
    end
    
    methods (Abstract,Static)
        % getParameters To define the user-tunable parameters
        % This function is used to define the user-definable parameters for
        % the module. ACC is going to create an approriate GUI for asking
        % the parameters from the users defined here. The output must
        % follow the convention of the matlab_settings_gui (located in
        % ACC\Utils\ActiveRegression\Utils\matlab_settings_gui) or
        % alternatively at https://github.com/szkabel/matlab_settings_gui.
        %
        % The single output parameter should be matching with the first
        % input of generateUIControls function.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
        paramarray = getParameters();
    end
    
    methods (Static)        
        function [bool,msg] = checkParameters(~)
        % checkParameters To check the user provided parameters
        %  Checks for a parameter array wheter it is a valid constructor
        %  parameter list in the form described above. It has a default
        %  constant true implementation but it can be overwritten in the
        %  child classes.
        %
        %  INPUT:
        %   paramcell       the parameter values to be checked. Matches
        %                   with the output of fetchUIControlValues.
        %
        %  OUTPUTS:
        %   bool            a boolean integer indicating the validness of
        %                   the parameters
        %   msg             an error msg which will be displayed to the
        %                   user if the parameters are not correct.
        %
        %  We note that the int typed parameters are checked to be numbers
        %  (can be double also!), and the ENUMS can get only values which
        %  are listed in teh values array. Any other checks must be
        %  implemented here.
        %
        %   See also generateUIControls, fetchUIControlValues, fillOutUIControls
            bool = 1;
            msg = '';
        end
    end
    
end

