classdef CommitteeMembers < ActiveLearner
% AUTHOR:   Abel Szkalisity
% DATE:     April 22, 2016
% NAME:     CommitteeMembers
%
% Implementation of the query by committee AL strategy.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    properties
        %The number of committee members
        sizeOfCommittee        
    end
    
    methods        
        % Constructor
        % INPUT:        varying length cell array to follow template
        %               Here only one element the sizeOfCommitte an int.
        %   The paramateres are coming in the same order described in the
        %   getParameters static function.
        function [obj] = CommitteeMembers(paramcell)        
            obj.sizeOfCommittee = paramcell{1};            
            obj.sizeOfInitialSet = 2*obj.sizeOfCommittee;
        end
        
        % Function implementing the sample proposal.
        % See the super class for details.
        function [plate,image,cellNumber] = proposeNextCell(obj,nofCells,CommonHandles)        
            %global CommonHandles;
            plate = zeros(1,nofCells);
            image = cell(1,nofCells);
            cellNumber = zeros(1,nofCells);
            %if there is not enough training samples to start AL
            if (~isfield(CommonHandles,'TrainingSet') || size(CommonHandles.TrainingSet.Class,1)<obj.sizeOfInitialSet)
                %random proposal within the plate
                for i=1:nofCells
                    cellInfo = randomCellProposal(CommonHandles);
                    %randomCellProposal gives back the proposed cell in CommonHandles and we extract the info from there.                
                    cellNumber(i) = cellInfo.cellNumber;
                    image{i} = cellInfo.SelectedOriginalImageName;
                    plate(i) = cellInfo.SelectedPlate;
                end
            else
                %Committee members use cross folds
                savedTrainingSet = CommonHandles.TrainingSet;
                out = zeros(size(CommonHandles.AllFeatures,1), obj.sizeOfCommittee);
                for i=1:obj.sizeOfCommittee
                    indexValues = 1:length(CommonHandles.TrainingSet.Features);
                    usedIndices = portion(indexValues,obj.sizeOfCommittee,i);
                    CommonHandles.TrainingSet.Features = CommonHandles.TrainingSet.Features(usedIndices);
                    CommonHandles.TrainingSet.Class = CommonHandles.TrainingSet.Class(usedIndices);
                    CommonHandles = trainClassifier(CommonHandles);
                    [out(:,i),~,CommonHandles] = predictClassifier(CommonHandles.AllFeatures,CommonHandles);                    
                end
                
                %TODO: replace the for with MATLAB syntax
                
                %save place for entropy
                entropy = zeros(1,size(out,1));
                
                for i=1:length(entropy)                    
                    selectedRow = out(i,:);
                    for j=1:max(selectedRow)
                        if (sum(selectedRow == j)>0)
                            entropy(i) = entropy(i) - (sum(selectedRow == j)/obj.sizeOfCommittee)*log(sum(selectedRow == j)/obj.sizeOfCommittee);
                        end                        
                    end                    
                end
                
                [sortedValues,sortedIndices] = sort(entropy,'descend');   
                maxIndices = sortedIndices(sortedValues == sortedValues(1));
                %if there are more cell with maximum entropy then we
                %randomly permutate the maximal cells.
                if nofCells < length(maxIndices)
                    randIndices = randperm(length(maxIndices));
                    sortedIndices(1:length(maxIndices)) = sortedIndices(randIndices);
                end
                
                %propose cells which are not in the training set
                i = 0;
                j = 1;
                while i<nofCells && j<length(sortedIndices)
                    if ~isInTrainingSet(CommonHandles.AllFeaturesMapping{sortedIndices(j)}.ImageName,CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CellNumberInImage,1,CommonHandles,CommonHandles.AllFeatures(sortedIndices(j),:))
                    
                        i=i+1;
                        plate(i) = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CurrentPlateNumber;
                        image{i} = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.ImageName;
                        cellNumber(i) = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CellNumberInImage;                        
                    end
                    j = j+1;
                end                
                
                CommonHandles.TrainingSet = savedTrainingSet;
            end
        end
        
        function paramarray = getParameterValues(obj)
            paramarray{1} = obj.sizeOfCommittee;
        end
    end
    
    methods (Static)        
        function paramarray = getParameters()
            paramarray{1}.name = 'Size of committee:';
            paramarray{1}.type = 'int';            
        end
        function [bool,msg] = checkParameters(paramcell)
            [bool,msg] = checkNumber(paramcell{1},1,1,[2,Inf],'The size of the committee ');            
        end
    end
        
    
end

