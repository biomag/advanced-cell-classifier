classdef UncertaintySampling < ActiveLearner
    
    properties
        %the type of uncertainty sampling: MAX, MARGIN, ENTROPY
        type;
    end
    
    methods
        function [obj] = UncertaintySampling(paramcell)
            obj.sizeOfInitialSet = 5;
            obj.type = paramcell{1};
        end
        
        function [plate,image,cellNumber] = proposeNextCell(obj,nofCells,CommonHandles)            
            %global CommonHandles;
            plate = zeros(1,nofCells);
            image = cell(1,nofCells);
            cellNumber = zeros(1,nofCells);
            %if there is not enough training samples to start AL
            if (~isfield(CommonHandles,'TrainingSet') || size(CommonHandles.TrainingSet.Class,1)<obj.sizeOfInitialSet)
                %random proposal within the plate
                for i=1:nofCells
                    cellInfo = randomCellProposal(CommonHandles);
                    %randomCellProposal gives back the proposed cell in CommonHandles and we extract the info from there.
                    cellNumber(i) = cellInfo.cellNumber;
                    image{i} = cellInfo.SelectedOriginalImageName;
                    plate(i) = cellInfo.SelectedPlate;
                end
            else
                CommonHandles = trainClassifier(CommonHandles);
                [~,prop,CommonHandles] = predictClassifier(CommonHandles.AllFeatures,CommonHandles);  
                %reserve place
                margin = zeros(1,size(prop,1));
                
                for i=1:length(margin)
                    selectedSortedRow = sort(prop(i,:),'descend');
                    switch obj.type
                        case 'MAX'
                            margin(i) = selectedSortedRow(1);
                        case 'MARGIN'
                            if length(selectedSortedRow)>1
                                margin(i) = selectedSortedRow(1)-selectedSortedRow(2);
                            else 
                                margin(i) = selectedSortedRow(1);
                            end
                        case 'ENTROPY'                            
                            margin(i) = sum(selectedSortedRow(selectedSortedRow > 0).*log(selectedSortedRow(selectedSortedRow > 0)));                            
                    end                    
                end
                
                if strcmp(obj.type,'ENTROPY')
                    [sortedValues,sortedIndices] = sort(margin,'descend');
                else
                    [sortedValues,sortedIndices] = sort(margin);
                end
                
                maxIndices = sortedIndices(sortedValues == sortedValues(1));
                %if there are more cell with maximum rank value then we
                %randomly permutate the maximal cells.
                if nofCells < length(maxIndices)
                    randIndices = randperm(length(maxIndices));
                    sortedIndices(1:length(maxIndices)) = sortedIndices(randIndices);
                end
                
                %propose cells which are not in the training set
                i = 0;
                j = 1;
                while i<nofCells && j<length(sortedIndices)
                    if ~isInTrainingSet(CommonHandles.AllFeaturesMapping{sortedIndices(j)}.ImageName,CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CellNumberInImage,1,CommonHandles,CommonHandles.AllFeatures(sortedIndices(j),:))
                        i=i+1;
                        plate(i) = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CurrentPlateNumber;
                        image{i} = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.ImageName;
                        cellNumber(i) = CommonHandles.AllFeaturesMapping{sortedIndices(j)}.CellNumberInImage;                        
                    end
                    j = j+1;
                end           
            end
        end
        
        function paramarray = getParameterValues(obj)
            paramarray{1} = obj.type;
        end
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'Type of uncertainty:';
            paramarray{1}.type = 'enum';            
            paramarray{1}.values = {'MAX','MARGIN','ENTROPY'};
        end
    end
    
end

