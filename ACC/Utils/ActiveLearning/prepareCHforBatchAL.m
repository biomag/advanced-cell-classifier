function [cleanedCH] = prepareCHforBatchAL(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 30, 2020
% NAME:     prepareCHforBatchAL
%
% To create a simplified CommonHandles with only those fields utmost
% necessary for batch processing and hence enhancing batch submission time.
%
% INPUT:
%  CommonHandles    The usual structure
%
% OUTPUT:
%   cleanedCH       The CommonHandles structure only with the fields
%                   neccessary for training and prediction.
%
% COPYRIGHT
% Advanced Cell Classifier Toolbox. All rights reversed.
% Copyright (C) 2019 Peter Horvath
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

fieldsToKeep = {...
    'Classes';...
    'ActiveRegression';...
    'ACCPATH';...
    'LabeledInstances';...
    'SALT';...
    'ClassifierNames';...
    'SelectedClassifier';...
    'ProjectName';...
    'TrainingSet';...
    'AllFeatures';...
    'AllFeaturesMapping';...
    'selectedRegressionClass';...
    'ALRegression';...
    'AL'
    };

for i=1:length(fieldsToKeep)
    if isfieldRecursive(CommonHandles,fieldsToKeep{i})
        eval(['cleanedCH.' fieldsToKeep{i} ' = CommonHandles.' fieldsToKeep{i} ';']);
    end
end

%More special fields

%Regression predictor
nofRPs = length(CommonHandles.RegressionPlane);
cleanedCH.RegressionPlane = cell(1,nofRPs);
for i=1:nofRPs
    if ~isempty(CommonHandles.RegressionPlane{i}) && isfield(CommonHandles.RegressionPlane{i},'Predictor')
        cleanedCH.RegressionPlane{i}.Predictor = CommonHandles.RegressionPlane{i}.Predictor;
    end
end

%Plate name from class images
cleanedCH.ClassImages = containers.Map();
classNames = CommonHandles.ClassImages.keys();
for i=1:length(classNames)
    cimgs = CommonHandles.ClassImages(classNames{i});
    redCimgs.Images = cell(1,length(cimgs.Images));
    for j=1:length(cimgs.Images)
        redCimgs.Images{j}.PlateName = cimgs.Images{j}.PlateName;
    end
    cleanedCH.ClassImages(classNames{i}) = redCimgs;
end


end

