function activeLearningStart(reg)
% AUTHOR:   Abel Szkalisity
% DATE:     April 26, 2016
% NAME:     activeLearningQuery
%
% To encapsulate the functionality of the startup of activeLearning
% including active regression.

% INPUT:
%   reg          	A bool value indicating whether active learning is used
%                   for regression or not.
%
% 
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
currentCellInBatch = CommonHandles.ALBatched.currentCellInBatch;
batchSize = CommonHandles.ALBatched.batchSize;

%Turn on active learning
if nargin<1
    reg = 0;
end
if reg   
    CommonHandles.ActiveRegression.on = 1;    
else
    CommonHandles.ActiveLearning = 1;    
end

%put info to the screen that we're thinking
screensize = get(0,'Screensize');
infoD = dialog('Name','Active learning','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','The machine is learning and will ask a question soon...');
pause(0.2);

%ask from active learner to propose one cell
global bgAL;

if CommonHandles.ALBatched.usePCT
    if isempty(bgAL)
        h = msgbox('Sending data to batch processing','Batch processing');
        if reg            
            %clearedCH = clearGUIhandles(CommonHandles);            
            clearedCH = prepareCHforBatchAL(CommonHandles);
            bgAL = batch('backgroundALRegression',1,{clearedCH});             
        else
            %clearedCH = clearGUIhandles(CommonHandles);
            clearedCH = prepareCHforBatchAL(CommonHandles);
            bgAL = batch('backgroundAL',1,{clearedCH});
        end
        if ishandle(h), close(h); end
    else
        t = findTask(bgAL);
        disp(t.State);
        disp(bgAL.State);
        if strcmp(t.State,'finished')
            try
                outparams = fetchOutputs(bgAL);
                disp('New AL batch is ready');
                batchedALData = outparams{1};
                CommonHandles.ALBatched.data = batchedALData;            
                %clearedCH = clearGUIhandles(CommonHandles);
                clearedCH = prepareCHforBatchAL(CommonHandles);
                h = msgbox('Sending data to batch processing','Batch processing');
                if reg                    
                    bgAL = batch('backgroundALRegression',1,{clearedCH});                        
                else                                 
                    bgAL = batch('backgroundAL',1,{clearedCH});                    
                end
                if ishandle(h), close(h); end
                currentCellInBatch = 1;
            catch e
                disp(getReport(e));
                disp('Unkown error in background work. Try AL without parallel computing toolbox.');
                %clearedCH = clearGUIhandles(CommonHandles);
                clearedCH = prepareCHforBatchAL(CommonHandles);
                h = msgbox('Sending data to batch processing','Batch processing');
                if reg
                    bgAL = batch('backgroundALRegression',1,{clearedCH});
                else                    
                    bgAL = batch('backgroundAL',1,{clearedCH});
                end
                if ishandle(h), close(h); end
            end
        end
    end            
else    
    
    batchedALData = CommonHandles.ALBatched.data;
    if currentCellInBatch == 1 || isempty(batchedALData) || ~isfield(batchedALData,'plate')
        if reg
            batchedALData = backgroundALRegression(CommonHandles);
        else
            [batchedALData.plate,batchedALData.image,batchedALData.cellNumber] = CommonHandles.AL.proposeNextCell(batchSize,CommonHandles);    
        end
    end
    
    CommonHandles.ALBatched.data = batchedALData;
     
end

%increase currentCell to the next proposal which is not already in the
%training set. (even if it is checked in AL algorithms because of the background run it can choose a training set element)
batchedALData = CommonHandles.ALBatched.data;
while ( ~isempty(batchedALData) && currentCellInBatch <= length(batchedALData.cellNumber) && isInTrainingSet(batchedALData.image{currentCellInBatch}, batchedALData.cellNumber(currentCellInBatch) , 0, CommonHandles))
    currentCellInBatch = currentCellInBatch + 1;
end

try

    %if the AL is not ready either with the first run or we ran out of
    %available cells or if the AL couldn't propose all the required cells      
    if  isempty(batchedALData) || currentCellInBatch>length(batchedALData.cellNumber) ||...
            batchedALData.cellNumber(currentCellInBatch)==0 || batchedALData.plate(currentCellInBatch)==0 ||...
            isempty(batchedALData.image{currentCellInBatch})
        %do random cell proposal
        disp('Random cell proposal.');
        h = msgbox('The proposed cell is random.','Random cell proposal');
        pause(2);
        if ishandle(h), close(h); end
        cellInfo = randomCellProposal(CommonHandles);        
    else
        %when we use the previous batched data
        disp('Active choice');        
        cellInfo.CellNumber = batchedALData.cellNumber(currentCellInBatch);
        cellInfo.OriginalImageName = batchedALData.image{currentCellInBatch};
        [~,name,ext] = fileparts(batchedALData.image{currentCellInBatch});
        cellInfo.ImageName = [char(CommonHandles.PlatesNames(batchedALData.plate(currentCellInBatch))) filesep CommonHandles.ImageFolder filesep name ext];
        cellInfo.PlateName = batchedALData.plate(currentCellInBatch);
    end
    
    if ishandle(infoD)
        delete(infoD);
    end

catch e
    disp(getReport(e));
    warndlg('Ran out of cells to annotate','ACC cannot jump to a random cell. Probably you classified all of them!');
    CommonHandles.ActiveLearning = 0;    
    return;
end
   
%jump to selection
jumpToCell(cellInfo,1);

if ~(CommonHandles.ALBatched.usePCT) && (currentCellInBatch == batchSize)
    CommonHandles.ALBatched.currentCellInBatch = 1;
else
    CommonHandles.ALBatched.currentCellInBatch = currentCellInBatch + 1;
end

