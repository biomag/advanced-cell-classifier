function featureList = getFeatureList()

global CommonHandles;

% feature name list
if ~exist([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.MetaDataFolder filesep 'featureNames.acc'],'file')
    featureList = [];
    return;
end

inF = fopen([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.MetaDataFolder filesep 'featureNames.acc'], 'r');

counter = 1;
while 1
    tline = fgetl(inF);
    if ~ischar(tline), break, end
    featureList{counter} = tline;
    counter = counter + 1;
end

fclose(inF);

end
