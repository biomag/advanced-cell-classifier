function calcDerivFeatures(projectPath, plateIDs, metaDataFolder, metaDataFileName, TrackingPath, metaExt)

featureNames = textread(fullfile(projectPath,plateIDs{1},metaDataFolder,metaDataFileName), '%s', 'delimiter', '\n');
numOfFeature = numel(featureNames);
idx = listdlg('ListString', featureNames(3:end),... % the first 2 values are the coordinates of the cell
                'SelectionMode','multiple',...
                'Name','Feature Selection',...
                'OKString','Apply Filter',...
                'PromptString','Select features to calculate their derivatives:');
N = numel(idx);
numSpecs = struct('integer',1,'scalar',1,'limits',[1,10]);
derivTimeFrame = Settings_GUI({...
                struct('name', 'Time Frame +/-', 'type', 'int', 'default', 1, 'numSpecs', numSpecs)},...
                'infoText', 'Enter the size of time frame to calculate derivativs!', 'title', 'Derivative time Frame');
derivTimeFrame = derivTimeFrame{1};

analFolders = dir(fullfile(projectPath,plateIDs{1}));
dirIdx = [analFolders.isdir];
dirIdx(1:2) = 0;
subfoldersNames = {analFolders(dirIdx).name};
strSpecs = struct('forbidden', {subfoldersNames}, 'capital', [], 'number', [], 'glyph', 0, 'space', 0);
derivFolderName = Settings_GUI({...
                struct('name', 'Metadata Folder', 'type', 'str', 'default', ['anal2_deriv_t',num2str(derivTimeFrame)], 'strSpecs', strSpecs),...
                },...
                'infoText', 'Enter the name of the new metadata folder!', 'title', 'New folder');
derivFolderName = derivFolderName{1};

newFeatureNames = featureNames;
for i = 1:N
    newFeatureNames = [newFeatureNames; ['dx',num2str(derivTimeFrame),'_',featureNames{i+2}]];
end

featureNamesTable = table(newFeatureNames);

fillFeaturesWithZeros(N,projectPath,plateIDs,metaDataFolder,featureNamesTable, metaExt, derivFolderName);

'test';

for p = 1:numel(plateIDs)
    splittedPath = strsplit(TrackingPath,filesep);
    fileName = [plateIDs{p},'_',splittedPath{end}, '_track.csv'];
    trackTable = readtable(fullfile(CommonHandles.TrackingPath,plateIDs{p},fileName));
    uniqueIDs = unique(trackTable.TrackID);
    wb = waitbar(0,{'Calculating derivative features';...
        [plateIDs{p},'   ',num2str(p),'/',num2str(numel(plateIDs))];...
        'TrackID';...
        'Time point'});
    for u = 1:size(uniqueIDs,1)
        currentID_table = trackTable(trackTable.TrackID == uniqueIDs(u),:);
        n = size(currentID_table,1);
        for t = 1:n
            if ishandle(wb)
                waitbar(u/size(uniqueIDs,1),wb,{'Calculating derivative features';...
                    [plateIDs{p},'   ',num2str(p),'/',num2str(numel(plateIDs))];...
                    ['TrackID   ',num2str(u),'/',num2str(size(uniqueIDs,1))];...
                    ['Time point   ',num2str(t),'/',num2str(n)]});
            end
            preID = max([1, t-derivTimeFrame]);
            postID = min([n, t+derivTimeFrame]);

            pre_objID = currentID_table.ObjectID(preID);
            mid_objID = currentID_table.ObjectID(t);
            post_objID = currentID_table.ObjectID(postID);

            pre_imName = strsplit(currentID_table.ImageName{preID},'.');
            mid_imName = strsplit(currentID_table.ImageName{t},'.');
            post_imName = strsplit(currentID_table.ImageName{postID},'.');
            
            pre_metaData = load(fullfile(projectPath,plateIDs{p},metaDataFolder,[pre_imName{1},metaExt]));
            mid_metaData = load(fullfile(projectPath,plateIDs{p},derivFolderName,[mid_imName{1},metaExt]));
            post_MetaData = load(fullfile(projectPath,plateIDs{p},metaDataFolder,[post_imName{1},metaExt]));

            dx_features = (post_MetaData(post_objID,idx+2) - pre_metaData(pre_objID,idx+2))./2;
            mid_metaData(mid_objID,numOfFeature+1:end) = dx_features;
            writematrix(mid_metaData,fullfile(projectPath,plateIDs{p},derivFolderName,[mid_imName{1},metaExt]))
        end
    end
end