function deleteClassButtons(CommonHandles)
% AUTHOR:   Abel Szkalisity
% DATE:     March 9, 2017
% NAME:     deleteClassButtons
%
% Deletes all the classification buttons, from CommonHandles and from GUI
%
% INPUT:
%   CommonHandles       The CommonHandles structure from which we delete
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

for i=1:length(CommonHandles.Classbuttons)
    delete(CommonHandles.Classbuttons{1})
    delete(CommonHandles.ClassTextFields{1})
    delete(CommonHandles.ShowTrainedCells{1})    
    if ishandle(CommonHandles.ShowRegressionPlaneButtons{1})
        delete(CommonHandles.ShowRegressionPlaneButtons{1});
    end
    CommonHandles.Classbuttons(1) = [];
    CommonHandles.ClassTextFields(1) = [];
    CommonHandles.ShowTrainedCells(1) = [];
    CommonHandles.ShowRegressionPlaneButtons(1) = [];
end


end

