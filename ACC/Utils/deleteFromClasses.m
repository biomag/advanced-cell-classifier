function [] = deleteFromClasses(name)
% AUTHOR:	Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	deleteFromClasses
% 
% Deletes a class/button from the list, with the given name. 
% we must delete data from the TrainingSet and from the Classes too.
%
% INPUT:
%   name            Name of the class to be deleted.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

i = length(CommonHandles.Classes);
while (i > 0)
    if(strcmp(CommonHandles.Classes{i}.Name,name))                
        
        j = length(CommonHandles.TrainingSet.Class);
        while (j > 0)
            if (CommonHandles.TrainingSet.Class{j}(i) == 1)
                deleteFromTrainingSet(j);
            else
                CommonHandles.TrainingSet.Class{j}(i) = [];
            end
            j=j-1;
        end        
        CommonHandles.ClassImages.remove(CommonHandles.Classes{i}.Name);
        if strcmp(CommonHandles.Classes{i}.Type,'Regression')
            CommonHandles.ActiveRegression.on = 0;
            actLearnToggleTool = findobj(CommonHandles.MainWindow,'Tag','toolbarActiveRegression');
            set(actLearnToggleTool,'State','off');
            if strcmp(CommonHandles.ActiveRegression.selectedClass,CommonHandles.Classes{i}.Name)
                CommonHandles.ActiveRegression.selectedClass = '';
            end
            if ishandle(CommonHandles.RegressionPlaneHandle)
                close(CommonHandles.RegressionPlaneHandle);
            end
            if ~isempty(CommonHandles.RegressionPlane)
                CommonHandles.RegressionPlane(i) = [];
            end
        end
        CommonHandles.Classes(i) = [];                
    end
    i=i-1;
end

end

