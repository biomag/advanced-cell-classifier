function [] = deleteFromClassImages( cellNumber, imageName, counter )
% AUTHOR:	Lassi Paavolainen, Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	deleteFromClassImages
% 
% Deletes a annotated cell from the training set list. 
% we must delete data from the TrainingSet and from the Classes too.
%
% INPUT:
%   cellNumber          Number of the cell in the image.
%   imageName           Name of the image containing the cell.
%   counter             Indentification number of the class.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

className = CommonHandles.Classes{counter}.Name;

name = strsplit(imageName,{'/','\'});
segm_name = fullfile(char(name{1}),CommonHandles.ImageFolder,char(name{3}));
orig_name = fullfile(char(name{1}),CommonHandles.OriginalImageFolder,char(name{3}));

for i = 1:size(CommonHandles.TrainingSet.CellNumber,1)
    if (CommonHandles.TrainingSet.CellNumber{i} == cellNumber) && ((strcmp(CommonHandles.TrainingSet.ImageName{i},segm_name)) || (strcmp(CommonHandles.TrainingSet.ImageName{i},orig_name)))
        % Remove from TrainingSet
        deleteFromTrainingSet(i);
        
        % Search for classImage to remove
        classImgObj = CommonHandles.ClassImages(className);
        for j = 1:length(classImgObj.Images)
            if (classImgObj.Images{j}.CellNumber == cellNumber) && (strcmp(classImgObj.Images{j}.ImageName,imageName))
                classImgObj.Images(j) = [];
                CommonHandles.ClassImages(className) = classImgObj;
                break;
            end
        end
        
        CommonHandles.Classes{counter}.LabelNumbers = CommonHandles.Classes{counter}.LabelNumbers - 1;
        
        refreshClassesList();
        
        break;
    end
end
