function [ plateNumber ] = getPlateNumberByName( plateName )
% AUTHOR:   Abel Szkalisity
% DATE:     July 9, 2016
% NAME:     getPlateNumberByName
%
% Get the id in the Plate list of a specific plateName
%
% INPUT:
%   plateName       The string of the plate's name
%   (Hidden input: global CommonHandles)
%
% OUTPUT:
%   plateNumber     index of the string in PlatesNames. 0 if it doesn't
%                   exist
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

plateNumber = 0;

for i=1:length(CommonHandles.PlatesNames)
    if strcmp(CommonHandles.PlatesNames{i},plateName)
        plateNumber = i;        
        break;        
    end    
end


end

