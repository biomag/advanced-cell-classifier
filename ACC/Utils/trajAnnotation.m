function trajAnnotation
% AUTHOR:	Attila Beleon
% DATE: 	October 29, 2023
% NAME: 	trajAnnotation
% 
% If the project contains image sequences from different timepoints this
% function by using tracking information, collects all the thumbnalis and
% coordinates of the selected cell from all the available timepoints, then 
% opens the Image Sequence Scope to visualize the sequence.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles

selectedPlateIdx = CommonHandles.SelectedPlate;
PlateNames = CommonHandles.PlatesNames;
if isfield(CommonHandles,'TrackingPath') && isempty(CommonHandles.TrackingPath)
    helpdlg('Missing tracking information.','Tracking')
    answer = questdlg('How do you want to provide tracking information?','Trackging method selection','Tracking file','Calculate now','Tracking file');
    switch answer
        case 'Tracking file'
            trackpath = uigetdir('Tracking folder', CommonHandles.DirName);
            if ischar(trackpath)
                CommonHandles.TrackingPath = trackpath;
            else
                return
            end
        case 'Calculate now'
            try
                TrackingWizardGUI();
            catch
                CommonHandles.TrackingPath = [];
                return
            end
        otherwise
            error('It is required to provide tracking information.');
    end
    helpdlg('Tracking path has been set.','Tracking')
else
    if ~isfield(CommonHandles,'TrackedPlateName') || ~strcmp(CommonHandles.TrackedPlateName,PlateNames{selectedPlateIdx})
        try
            splittedPath = strsplit(CommonHandles.TrackingPath,filesep);
            fileName = [PlateNames{selectedPlateIdx},'_',splittedPath{end}, '_track.csv'];
            trackTable = readtable(fullfile(CommonHandles.TrackingPath,PlateNames{selectedPlateIdx},fileName));
            toCheckFields = {'ImageName','Plate','TrackID','tracking__center_x','tracking__center_y','Frame'};
            bool = checkTable(toCheckFields,trackTable);
            if bool == 0
                error('Tracking file is corrupter or invalid.')
            end
            CommonHandles.TrackedPlateName = PlateNames{selectedPlateIdx};
            CommonHandles.Tracking = trackTable;
        catch
            waitfor(errordlg('Tracking file is corrupted or missing or the path is wrong in the case of the selected Plate.'))
            waitfor(helpdlg('Run tracking on the selected plate, then move the files to the same directory with respect of the folder structure.'))
            helpdlg('To open the Tracking Wizardc, select ''File'' menu then ''Tracking Wizard''');
            return
        end
    end
    trackTable = CommonHandles.Tracking;
    cellID = CommonHandles.SelectedCell;
    plateID = CommonHandles.SelectedPlate;
    imName = strsplit(CommonHandles.SelectedImageName,filesep);
    Plate = CommonHandles.PlatesNames{plateID};
    selectedIdx = find(strcmp(trackTable.Plate, Plate) & trackTable.ObjectID == cellID & strcmp(trackTable.ImageName, imName{end}));

    origJ2C.PlateName = plateID;
    origJ2C.ImageName = CommonHandles.SelectedImageName;
    origJ2C.OriginalImageName = fullfile(imName{1},CommonHandles.OriginalImageFolder,imName{3});
    origJ2C.CellNumber = cellID;
    TrackID = 0;
    if ~isempty(selectedIdx)
        TrackID = trackTable.TrackID(selectedIdx);
        Track_idx = find(trackTable.TrackID == TrackID);
        wb = waitbar(0,'Collecting images. Please wait...');
        j2Cstructs(length(Track_idx),1).PlateName = [];
        CutSize = CommonHandles.CutSize;
        thumbnails = uint8(zeros(CutSize*2+1,CutSize*2+1,3,length(Track_idx)));
        numOfObj = length(Track_idx);
        for i=1:numOfObj
            j2Cstructs(i,1).PlateName = plateID;
            j2Cstructs(i,1).ImageName = fullfile(trackTable.Plate{Track_idx(i)},CommonHandles.ImageFolder,trackTable.ImageName{Track_idx(i)});
            j2Cstructs(i,1).OriginalImageName = fullfile(trackTable.Plate{Track_idx(i)},CommonHandles.OriginalImageFolder,trackTable.ImageName{Track_idx(i)});
            j2Cstructs(i,1).CellNumber = trackTable.ObjectID(Track_idx(i));
            jumpToCell(j2Cstructs(i,1),false, 'loadType', 'full');
            [x, y, ~] = size(CommonHandles.CurrentSelectedCell);
            xDif = (CutSize*2+1) - x;
            yDif = (CutSize*2+1) - y;
            if xDif ~=  0 || yDif ~= 0
                tempIm = padarray(CommonHandles.CurrentSelectedCell,ceil([xDif/2 yDif/2]),0,'both');
                thumbnails(:,:,:,i) = tempIm(1:(CutSize*2+1),1:(CutSize*2+1),:);
            else
                thumbnails(:,:,:,i) = CommonHandles.CurrentSelectedCell;
            end
            alreadyInTrainingSet = isInTrainingSet(CommonHandles.SelectedImageName,...
                CommonHandles.SelectedCell,...
                0,...
                CommonHandles,...
                CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end));
            if alreadyInTrainingSet
                counter = find(CommonHandles.TrainingSet.Class{alreadyInTrainingSet});
                j2Cstructs(i,1).ClassID = CommonHandles.Classes{counter}.Name;
            else
                j2Cstructs(i,1).ClassID = [];
            end

            if ishandle(wb)
                wb = waitbar(i/numOfObj,wb,sprintf('%d / %d Collecting images. Please waait...',i,numOfObj));
            end
        end
        if ishandle(wb)
            close(wb);
        end
    else
        j2Cstructs(1,1).PlateName = plateID;
        j2Cstructs(1,1).ImageName = fullfile(CommonHandles.PlatesNames{CommonHandles.SelectedPlate},CommonHandles.ImageFolder,CommonHandles.SelectedImageName);
        j2Cstructs(1,1).OriginalImageName = fullfile(CommonHandles.PlatesNames{CommonHandles.SelectedPlate},CommonHandles.OriginalImageFolder,CommonHandles.SelectedImageName);
        j2Cstructs(1,1).CellNumber = CommonHandles.SelectedCell;
        thumbnails = CommonHandles.CurrentSelectedCell;
        helpdlg('This image is not part of any trajectories.')
    end
    classNames = cell(length(CommonHandles.Classes),1);
    classType = cell(length(CommonHandles.Classes),1);
    for cl = 1:length(CommonHandles.Classes)
        classNames{cl,1} = CommonHandles.Classes{cl}.Name;
        classType{cl,1} = CommonHandles.Classes{cl}.Type;
    end
    waitfor(ImageSequenceScope(j2Cstructs,thumbnails,classNames,classType,TrackID))
    jumpToCell(origJ2C,false, 'loadType', 'full');
end