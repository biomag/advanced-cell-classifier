function fillFeaturesWithZeros(N,projectPath,plateIDs,metaDataFolder,featureNamesTable, metaExt, derivFolderName)

for p = 1:numel(plateIDs)
    fileList = dir(fullfile(projectPath,plateIDs{p},metaDataFolder,['*',metaExt]));
    mkdir(fullfile(projectPath,plateIDs{p},derivFolderName))
    writetable(featureNamesTable, fullfile(projectPath,plateIDs{p},derivFolderName, 'featureNames.acc'), 'FileType', 'text', 'WriteVariableNames', 0);
    for f = 1:length(fileList)
        origMetaData = load(fullfile(projectPath,plateIDs{p},metaDataFolder,fileList(f).name));
        newMetaData = [origMetaData,zeros(size(origMetaData,1),N)];
%         csvwrite(fullfile(projectPath,plateIDs{p},derivFolderName,fileList(f).name),newMetaData)
        writematrix(newMetaData,fullfile(projectPath,plateIDs{p},derivFolderName,fileList(f).name))
    end

end

