function recoverGUI(handles) %#ok<INUSD> it is used in eval
% AUTHOR:	Abel Szkalisity
% DATE: 	April 22, 2016
% NAME: 	recoverGUI
% 
% The CommonHandles contains all the neccessary data to reload a previous
% state of ACC. However just by loading in CommonHandles it won't set back
% the GUI elements to their states. (like state of Pushtools, size of
% window etc.) This function does these state resets.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles; %#ok<NUSED> It is used in eval.
            
    %data pairs to recover toggleTool states. First element is the name of
    %the 'indicator' variable in CommonHandles, second is the name of the
    %handle in the GUI. If the 'indicator' field in CommonHandles is true
    %the tool's state will be on, else it will be off.
    toggleTools = {...
         {'ActiveLearning','toolbarActiveLearningRun'},...
         {'ActiveRegression.on','toolbarActiveRegression'},...
         {'RandomTrain','toolbarRandomAnnotation'},...
         {'ColorView','toolbarColorView'},...
         {'ShowContour','toolbarContours'},...
         {'MainView.Red','toolbarRedChannel'},...
         {'MainView.Green','toolbarGreenChannel'},...
         {'MainView.Blue','toolbarBlueChannel'},...
         {'ShowCellNumbers','toolbarCellNumbers'},...
         {'DisplayAnnotatedCells','toolbarCellClass'},...
         {'MainView.StrechImage','toolbarIntensityStretch'},...
        };
    for i=1:length(toggleTools)
        if eval(['CommonHandles.' toggleTools{i}{1} ';'])
            eval(['set(handles.' toggleTools{i}{2} ',''State'',''on'');']);
        else
            eval(['set(handles.' toggleTools{i}{2} ',''State'',''off'');']);
        end
    end
    
end

