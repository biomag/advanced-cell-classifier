function addToClassImagesByObject( classImg )
% AUTHOR:    Abel Szkalisity
% DATE:     July 29, 2016
% NAME:     addToClassImagesByObject
%
%
% INPUT:
%   classImg        A structure of the most important data of the cell
%                   which will be added to the ClassImages field of
%                   CommonHandles
%
%   CommonHandles is a global input and output.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

if ~CommonHandles.ClassImages.isKey(classImg.ClassName)
    newclass = struct('Images',[],'selected',[],'sHandle',[]);
    CommonHandles.ClassImages(classImg.ClassName) = newclass;
end
cimgs = CommonHandles.ClassImages(classImg.ClassName);
cimgs.Images{end+1} = classImg;
CommonHandles.ClassImages(classImg.ClassName) = cimgs;

end

