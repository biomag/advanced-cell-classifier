function [] = moveClassImage( cellNumber, imageName, clickedClass )
% AUTHOR:   Tamas Balassa
% DATE:     April 22, 2016
% NAME:     moveClassImage
% 
% To move the images of cells from one class to another. This function also
% changes the class vector, but it doesn't add the image to the classImages
%
% INPUT:
%   cellNumber      Number of the selected cell in the image.
%   imageName       Name of the image containing the cell.
%   clickedClass   	Identifier of the clicked class.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

for i = 1:size(CommonHandles.TrainingSet.CellNumber)
    if (CommonHandles.TrainingSet.CellNumber{i} == cellNumber) && (strcmp(CommonHandles.TrainingSet.ImageName{i},imageName))
        [~,counter] = ismember(1,CommonHandles.TrainingSet.Class{i},'rows');
        className = CommonHandles.Classes{counter}.Name;
        prevClassVector = CommonHandles.TrainingSet.Class{i};
        features = CommonHandles.TrainingSet.Features{i};
        deleteFromTrainingSet(i);
        saveClassificationData(clickedClass,features);
        
        % Search for classImage to remove (anyway we remove, if we'll rollback we should put it to the end to keep consistence)
        classImgObj = CommonHandles.ClassImages(className);
        for j = 1:length(classImgObj.Images)
            if (classImgObj.Images{j}.CellNumber == cellNumber) && (strcmp(classImgObj.Images{j}.ImageName,imageName))
                classImgObj.Images(j) = [];
                CommonHandles.ClassImages(className) = classImgObj;
                break;
            end
        end        
        
        %if the target class is regression
        if strcmp(CommonHandles.Classes{clickedClass}.Type,'Regression')            
            CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',length(CommonHandles.TrainingSet.RegPos),'classIndex',clickedClass,'classID',CommonHandles.Classes{clickedClass}.ID,'prediction',0));
            uiwait(CommonHandles.RegressionPlaneHandle);
            %if the regPlane did not added the position to the training set
            if isempty(CommonHandles.TrainingSet.RegPos{end})
                %rollback
                CommonHandles.TrainingSet.Class{end} = prevClassVector;
                classImages = CommonHandles.ClassImages(CommonHandles.Classes{clickedClass}.Name);
                classImages.Images(end) = [];
                CommonHandles.ClassImages(CommonHandles.Classes{clickedClass}.Name) = classImages;
                warndlg('The last annotated cell was removed from training set because the regression position was not provided.','Regression warning');
                %if we rollback the image should be replaced within the
                %original classImages
                addToClassImages(find(prevClassVector));
                return;
            end
        end
        %if the source class was regression typed
        if strcmp(CommonHandles.Classes{counter}.Type,'Regression')            
            if ishandle(CommonHandles.RegressionPlaneHandle)
%                 close(CommonHandles.RegressionPlaneHandle);
            end
        end                
        
        refreshClassesList();
        
        break;
    end
end

end
