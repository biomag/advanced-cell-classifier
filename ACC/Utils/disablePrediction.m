function disablePrediction(disable, handles)
% AUTHOR:   Tamas Balassa
% DATE:     Feb 1, 2017
% NAME:     disablePrediction
% 
% This function enables or disables the user to use the prediction
% functions. We needed this for the cases when there is no training model
% and the toolbar buttons can be confusing.
%
% INPUT:
%   disable         It is boolean. 1 to disable the prediction buttons or
%                   0 to enable them
%   handles         The handles from main window.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


    global CommonHandles;

    if disable
        set(handles.menuPredictImage,'Enable','off');
        set(handles.menuPredictPlate,'Enable','off');
        set(handles.toolbarPredictImage,'Enable','off');
        set(handles.toolbarPredictPlate,'Enable','off');
    else    
        if isfield(CommonHandles,'SALT') && isfield(CommonHandles.SALT,'model') && ~isempty(CommonHandles.SALT.model)
            set(handles.menuPredictImage,'Enable','on');
            set(handles.menuPredictPlate,'Enable','on');
            set(handles.toolbarPredictImage,'Enable','on');
            set(handles.toolbarPredictPlate,'Enable','on');
        else
            set(handles.menuPredictImage,'Enable','off');
            set(handles.menuPredictPlate,'Enable','off');
            set(handles.toolbarPredictImage,'Enable','off');
            set(handles.toolbarPredictPlate,'Enable','off');
        end
    end
