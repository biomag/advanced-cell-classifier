function varargout = wellLayoutWizard(varargin)
% WELLLAYOUTWIZARD MATLAB code for wellLayoutWizard.fig
%      WELLLAYOUTWIZARD, by itself, creates a new WELLLAYOUTWIZARD or raises the existing
%      singleton*.
%
%      H = WELLLAYOUTWIZARD returns the handle to a new WELLLAYOUTWIZARD or the handle to
%      the existing singleton*.
%
%      WELLLAYOUTWIZARD('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in WELLLAYOUTWIZARD.M with the given input arguments.
%
%      WELLLAYOUTWIZARD('Property','Value',...) creates a new WELLLAYOUTWIZARD or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before wellLayoutWizard_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to wellLayoutWizard_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help wellLayoutWizard

% Last Modified by GUIDE v2.5 17-Aug-2020 00:32:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @wellLayoutWizard_OpeningFcn, ...
                   'gui_OutputFcn',  @wellLayoutWizard_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before wellLayoutWizard is made visible.
function wellLayoutWizard_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to wellLayoutWizard (see VARARGIN)

% Choose default command line output for wellLayoutWizard
handles.output = hObject;

global CommonHandles

colmax = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;
rowmaxnum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;

emptyDataCell = cell(rowmaxnum, colmax);

handles.layoutTable.Data = emptyDataCell;
handles.layoutTable.ColumnEditable = true(ones(1, colmax));
splittedName = split(CommonHandles.SelectedImageName, filesep);
handles.fileNameExample.String = splittedName(end);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes wellLayoutWizard wait for user response (see UIRESUME)
uiwait(handles.well_Layout_figure);


% --- Outputs from this function are returned to the command line.
function varargout = wellLayoutWizard_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.layoutTable.Data;
delete(handles.well_Layout_figure);

% --- Executes on button press in apply_Button.
function apply_Button_Callback(hObject, eventdata, handles)
% hObject    handle to apply_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles

if any(any(cellfun(@isempty,handles.layoutTable.Data)))
   errordlg('Please fill all the cells', 'Empty Cell')
   return
else
    for plateidx = 1:length(CommonHandles.Report.SelectedPlates)
        checkEmpty = 1;
        [maxrow, maxcol] = size(handles.layoutTable.Data);
        for row = 1:maxrow
            for col = 1:maxcol
                FileNamePrefix =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) filesep  CommonHandles.ImageFolder filesep '*' handles.layoutTable.Data{row, col} '*'];
                % list them
                MetadataFiles = dir(FileNamePrefix);
                if checkEmpty
                    if isempty(MetadataFiles)
                        checkEmpty = 0;
                    end
                end
            end
        end
    end
    if checkEmpty
        errordlg('All the wells are empty!', 'Incorrect file names')
        answer = questdlg('Would you like to try again?','Name correction','Yes','No','No');
        if ~strcmp(answer,'Yes')
            handles.layoutTable.Data = [];
            close(handles.well_Layout_figure)
        end
    else
        close(handles.well_Layout_figure)
    end
end


% --- Executes when user attempts to close well_Layout_figure.
function well_Layout_figure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to well_Layout_figure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
if isequal(get(handles.well_Layout_figure, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(handles.well_Layout_figure);
else
    % The GUI is no longer waiting, just close it
    delete(handles.well_Layout_figure);
end
