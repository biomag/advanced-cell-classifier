function deleteFromTrainingSet( idx )
% AUTHOR:   Abel Szkalisity
% DATE:     Sept 10, 2016
% NAME:     deleteFromTrainingSet
%
% To delete instance from the training set. It is also used to delete the
% complete training set all at once. 
% This function only removes all corresponding entries from
% CommonHandles.TrainingSet fields. It does NOT deletes classImages.
%
% INPUT:
%   idx             Index of instance within TrainingSet to delete.
%                   Exceptionally -1 to delete all entries at once.
%
%   CommonHandles   global input-output
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

TrainingSetFieldList = {...
    'MetaDataFilename',...
    'ImageName',...
    'OriginalImageName',...
    'CellNumber',...
    'Features',...
    'Class',...
    'Timestamp',...
    'RegPos'...
    };

global CommonHandles;
if idx ~= -1
    origFeatureVector = CommonHandles.TrainingSet.Features{idx};
    
    %Delete only the idx index
    for i=1:length(TrainingSetFieldList)        
        eval(['CommonHandles.TrainingSet.' TrainingSetFieldList{i} '(idx) = [];']);
    end
    
    indices = bloomFilter(origFeatureVector);
    CommonHandles.TrainingSet.BloomFilter(indices) = max(0,CommonHandles.TrainingSet.BloomFilter(indices)-1);
elseif idx == -1
    %Clear everything
    for i=1:length(TrainingSetFieldList)        
        eval(['CommonHandles.TrainingSet.' TrainingSetFieldList{i} ' = {};']);
    end
    %Reinitialize bloomFilter
    CommonHandles.TrainingSet.BloomFilter = bloomFilter();
end

end

