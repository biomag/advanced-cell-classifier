function addToClassImages(counter)
% AUTHOR:   Lassi Paavolainen, Tamas Balassa
% DATE:     April 22, 2016
% NAME:     addToClassImages
% 
% This function adds the currently selected cell to the ClassImages map.
% Counter is the class index for the currently selected cell. It works
% using the variable "global CommonHandles" to extract the info of the
% currently selected cell.
%
% INPUT:
%   counter         It is the index of an already created class.
%
% OUTPUT:
%   The currently selected cell is annotated in the class with index
%   "counter".
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

classImg.Image = CommonHandles.CurrentSelectedCell;
classImg.ClassName = CommonHandles.Classes{counter}.Name;
classImg.CellNumber = CommonHandles.SelectedCell;
classImg.ImageName = CommonHandles.SelectedImageName;
classImg.OriginalImageName = CommonHandles.SelectedOriginalImageName;
classImg.PlateName = CommonHandles.SelectedPlate;

addToClassImagesByObject(classImg);

end

