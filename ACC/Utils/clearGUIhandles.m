function [ CommonHandles ] = clearGUIhandles( CommonHandles )
% AUTHOR:   Abel Szkalisity
% DATE:     March 13, 2017
% NAME:     clearGUIhandles
%
% Deletes all GUI handles from CommonHandles. Note: CommonHandles is ACC's
% big structure but here it is not global. All new GUIs that are stored in
% ACC must be listed here.
%
% INPUT:
%   CommonHandles   The ACC structure
%
% OUTPUT:
%   CommonHandles   The ACC structure without any graphical stuff.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

CommonHandles.HC.ShowCellsFig = [];
CommonHandles.MainViewHandle = [];            
CommonHandles.MainWindow = [];
CommonHandles.outlierTreeHandle = [];
CommonHandles.RegressionPlaneHandle = [];            
for i=1:length(CommonHandles.RegressionPlane)
    CommonHandles.RegressionPlane{i}.mainFigureHandle = [];
    CommonHandles.RegressionPlane{i}.selectedAxes = [];
    CommonHandles.RegressionPlane{i}.dragging = [];
    CommonHandles.RegressionPlane{i}.trajGUIhandle = [];
end                        
CommonHandles.ShowHighProbCellsHandle = [];
CommonHandles.ShowSimilarCellsHandle = [];            
CommonHandles.ShowTrainedCellsFig = [];
CommonHandles.SmallColorViewHandle = [];
CommonHandles.SmallRedViewHandle   = [];
CommonHandles.SmallGreenViewHandle = [];
CommonHandles.SmallBlueViewHandle  = [];            

CommonHandles.Classbuttons = [];

CommonHandles.PlateSelectWindow = [];
CommonHandles.PlateListHandle = [];
CommonHandles.ImageListHandle = [];
CommonHandles.UploadCAMIOFigure = [];

CommonHandles.colorFrameHandle = [];
CommonHandles.trainReg_GUI = [];
CommonHandles.PredictRegressionGUI = [];



end

