function [ alreadyInTrainingSet ] = isInTrainingSet( imageName, cellNumber, original, CommonHandles, features, feats)
% AUTHOR:   Abel Szkalisity
% DATE:     May 24, 2016
% NAME:     isInTrainingSet
%
% Checks whether the provided imageName, cellNumber pair is already listed
% in the training set.
%
% INPUT:
%   imageName     	The queried image name.
%   cellNumber      The number of the cell within that image
%   original        Bool value. If true then imageName is compared with the
%                   originalImageName, if its false then with the contour
%                   image
%   CommonHandles   Not global input because the function is used in PCT.
%   features        OPTIONAL. This is the feature vector of the queried
%                   cell. If it is provided then the set membership is
%                   first examined with a Bloom-filter.
%   feats           OPTIONAL. The complete feature matrix of the training
%                   set.
%
% OUTPUT:
%   alreadyInTrainingSet    the index in the training set if the pair is already in
%                           and zero if it's not.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%global CommonHandles;

alreadyInTrainingSet = 0;

%Bloom filter
if nargin>4
    indices = bloomFilter(features);
    if ~all(CommonHandles.TrainingSet.BloomFilter(indices))
        return;
    end    
end

%
if original
    for i=1:length(CommonHandles.TrainingSet.OriginalImageName)    
        if (strcmp(imageName,CommonHandles.TrainingSet.OriginalImageName{i}) && cellNumber == CommonHandles.TrainingSet.CellNumber{i})
            alreadyInTrainingSet = i;
            break;
        end
    end
else
    for i=1:length(CommonHandles.TrainingSet.ImageName)    
        if (strcmp(imageName,CommonHandles.TrainingSet.ImageName{i}) && cellNumber == CommonHandles.TrainingSet.CellNumber{i})
            alreadyInTrainingSet = i;
            break;
        end
    end
end

%Do in this way as this requires a big matrix of the feats
if nargin>4 && alreadyInTrainingSet == 0
    if nargin<6
        feats = cell2mat(CommonHandles.TrainingSet.Features);
    end
    [alreadyInTrainingSet] = find(any(all(bsxfun(@eq,features,feats),2),3));
    if isempty(alreadyInTrainingSet)
        alreadyInTrainingSet = 0;
    end
end


