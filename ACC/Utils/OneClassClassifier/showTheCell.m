function CommonHandles = showTheCell( CommonHandles )
% AUTHOR:	Tamas Balassa
% DATE: 	Feb 5, 2016
% NAME: 	showTheCell
% 
% Sets the current cell in the CommonHandles.CurrentSelectedCell variable
% without jumping there.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
cutSize = 50;

cutSize = CommonHandles.CutSize;

% boundaries of the part of original image containing current cell for small view
if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) < cutSize+1
    cutx1=1;
else
    cutx1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) < cutSize+1
    cuty1=1;
else
    cuty1= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)-cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1) > (CommonHandles.CurrentImageSizeY -cutSize)
    cutx2=CommonHandles.CurrentImageSizeY;
else
    cutx2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 1)+cutSize);
end

if CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2) > (CommonHandles.CurrentImageSizeX-cutSize)
    cuty2=CommonHandles.CurrentImageSizeX;
else
    cuty2= floor(CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 2)+cutSize);
end

CommonHandles.CurrentSelectedCell = CommonHandles.CurrentOriginalImage(cuty1:cuty2, cutx1:cutx2, :);

end

