function [outimg, mapping] = createCellsImage(img,leaf)
% AUTHOR:	Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	createCellsImage
% 
% To create the table-like image containing the "icon images" of a subtree
% in the phenotype finder tree.
%
% INPUT:
%   img             The images of cells which are in the subtree
%   leaf            Can be 0 or 1. 1 if the node is a leaf, else 0
%
% OUTPUT:
%   outimg          Contain the table for containing the "icon images".
%   mapping         Contains PlateName, ImageName, OriginalImageName and
%                   CellNumber for each cell shown.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    global CommonHandles;

    imgsize = CommonHandles.ClassImageInfo.ImageSize;
    sepsize = CommonHandles.ClassImageInfo.SepSize;
    cols = CommonHandles.ClassImageInfo.Cols;
    width = cols * imgsize(1) + (cols+1) * sepsize;
    
    if leaf
        numimgs = length(img);
    else
        numimgs = 1;
    end
    rows = ceil(numimgs / cols);
    height = rows * imgsize(2) + (rows+1) * sepsize;
    outimg = uint8(zeros(height,width,3));
    outimg(:,:,:) = 255;
    
    mapping = {};
    x = 1;
    y = 1;
    for i = 1:numimgs
        classImg = img{i};
        resImg = im2uint8(imresize(classImg.cellImg, imgsize));
        if x > cols
           x = 1;
           y = y + 1;
        end
        offsetw = imgsize(1) * (x-1) + sepsize * x;
        offseth = imgsize(2) * (y-1) + sepsize * y;
        outimg(offseth:offseth+imgsize(2)-1,offsetw:offsetw+imgsize(1)-1,:) = resImg;
        subimgmeta.CellNumber = classImg.cell;
        subimgmeta.ImageName = classImg.contourImage;
        subimgmeta.OriginalImageName = classImg.image;
        subimgmeta.PlateName = classImg.plate;
        mapping{y,x} = subimgmeta;
        x = x + 1;
    end    
end