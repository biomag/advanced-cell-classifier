function addHCSamplesToExistingClass(className,featureMatrix)
% AUTHOR:	Tamas Balassa
% DATE: 	February 1, 2016
% NAME: 	addHCSamplesToExistingClass
% 
% This method adds the selected cells featurevectors (so its a matrix) to
% the given class (className).
%
% INPUT:
%        className         Name of the class (string)
%        featureMatrix     Matrix containing the featurevectors of cells
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    for i=1:size(CommonHandles.Classes,2)
        classNames{i} = CommonHandles.Classes{i}.Name;
    end
    indexOfClass = find(strcmp(classNames,className));
    
    for i=1:size(featureMatrix,1)
        
        alreadyInTrainingSet = 0;
        for ii=1:length(CommonHandles.TrainingSet.Features)
            if isequal(featureMatrix(i,:),CommonHandles.TrainingSet.Features{ii})
                alreadyInTrainingSet = 1;
            end
        end
        
        if ~alreadyInTrainingSet
    
            [~,indx] = ismember(featureMatrix(i,:),CommonHandles.OCC.LowProb.featureVector,'rows');
            currentCellNumberInImage = CommonHandles.OCC.LowProb.selectedCell{indx};
            currentImageName = CommonHandles.OCC.LowProb.imageName{indx};
            
            addToTrainingSet(indexOfClass, featureMatrix(i,:));

            classImg.Image = CommonHandles.OCC.LowProb.img{indx};
            classImg.ImageName = CommonHandles.OCC.LowProb.contourImageName{indx};
            classImg.CellNumber = currentCellNumberInImage;
            classImg.ClassName = className;
            classImg.OriginalImageName = currentImageName;
            classImg.PlateName = CommonHandles.OCC.LowProb.plateName{indx};
            if ~CommonHandles.ClassImages.isKey(classImg.ClassName)
                newclass = struct('Images',[],'selected',[],'sHandle',[]);
                CommonHandles.ClassImages(classImg.ClassName) = newclass;
            end
            cimgs = CommonHandles.ClassImages(classImg.ClassName);
            cimgs.Images{end+1} = classImg;
            CommonHandles.ClassImages(classImg.ClassName) = cimgs;
        end
    end
    
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i = 1: length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);        
    end
    
    refreshClassesList();
    