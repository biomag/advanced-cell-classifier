function reducedSamples = setupDataForOutlierTree()
% AUTHOR:	Tamas Balassa
% DATE: 	January 17, 2016
% NAME: 	setupDataForOutlierTree
% 
% This is the main method for preparing the data for the outlier tree. This
% one trains and predicts, so the outliers are obtained here.
%
% OUTPUT:
%        reducedSamples         AllFeatures reduced by using the infogain
%                               method
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

if size(CommonHandles.AllFeatures,1)<CommonHandles.HC.Default.NumberOfOutliers
    CommonHandles.HC.Default.NumberOfOutliers = size(CommonHandles.AllFeatures,1);
end

waitBarHandle = waitbar(0,'Converting data.');
counter = 0;

CommonHandles.OCC.LowProb = {};

tmpSelCell = CommonHandles.SelectedCell;
tmpSelImageName = CommonHandles.SelectedImageName;
tmpSelOriginalImageName = CommonHandles.SelectedOriginalImageName;
tmpSelPlate = CommonHandles.SelectedPlate;

data = convertACC2SALT(CommonHandles);
convertInstances(data);
set(get(findobj(waitBarHandle,'type','axes'),'title'), 'string', 'Reducing features.');

reducedSamples = CommonHandles.AllFeatures(:,CommonHandles.HC.Default.InfoGainAttributes);

for i=1:size(data.instances,1)
    for j=1:length(CommonHandles.HC.Default.InfoGainAttributes)
       reducedTrainingSamples(i,j) = data.instances(i,CommonHandles.HC.Default.InfoGainAttributes(j));
    end
end

set(get(findobj(waitBarHandle,'type','axes'),'title'), 'string', 'Training.');
CommonHandles.SALT.model = svmtrain(data.labels', reducedTrainingSamples, CommonHandles.HC.Default.TrainParams);

set(get(findobj(waitBarHandle,'type','axes'),'title'), 'string', 'Predicting.');
[~, ~ ,props] = svmpredict(ones(size(reducedSamples,1),1), reducedSamples, CommonHandles.SALT.model);

[~,index] = sort(props);

if CommonHandles.HC.Default.NumberOfOutliers == 0
    CommonHandles.HC.Default.NumberOfOutliers = size(reducedSamples,1);
end

CommonHandles.OCC.Dendogram.selectedFeatures = zeros(CommonHandles.HC.Default.NumberOfOutliers,size(CommonHandles.SelectedMetaData,2)-2);

sortedIndex = sort(index(1:CommonHandles.HC.Default.NumberOfOutliers));

set(get(findobj(waitBarHandle,'type','axes'),'title'), 'string', 'Getting the outliers.');
for i=1:CommonHandles.HC.Default.NumberOfOutliers
    counter = counter + 1;
    currentCell = sortedIndex(i);
    if i~=1 && i<=CommonHandles.HC.Default.NumberOfOutliers-1
        if ~strcmp(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName)
            CommonHandles.SelectedCell = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
            CommonHandles = showTheCell(CommonHandles);
            CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
            CommonHandles.OCC.LowProb.featureVector(i,:) = CommonHandles.AllFeatures(currentCell,:);
            CommonHandles.OCC.LowProb.imageName{i} = CommonHandles.AllFeaturesMapping{currentCell}.ImageName;
            imgName = strsplit(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,filesep);
            contourImgName = [imgName{1} filesep CommonHandles.ImageFolder filesep imgName{3}];
            CommonHandles.OCC.LowProb.contourImageName{i} = contourImgName;
            CommonHandles.OCC.LowProb.selectedCell{i} = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
            CommonHandles.OCC.LowProb.plateName{i} = CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber;
            CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
            
            imgNameNext = strsplit(CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName,filesep);
            contourImgName = [imgNameNext{1} filesep CommonHandles.ImageFolder filesep imgNameNext{3}];
            CommonHandles.SelectedImageName = contourImgName;
            CommonHandles.SelectedOriginalImageName = CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName;
            CommonHandles = loadSelectedImage(CommonHandles,'currPlateNumber',CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.CurrentPlateNumber);
            [~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
            [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.CurrentPlateNumber);
        else
            CommonHandles.SelectedCell = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
            CommonHandles = showTheCell(CommonHandles);
            CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
            CommonHandles.OCC.LowProb.featureVector(i,:) = CommonHandles.AllFeatures(currentCell,:);
            CommonHandles.OCC.LowProb.imageName{i} = CommonHandles.AllFeaturesMapping{currentCell}.ImageName;
            imgName = strsplit(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,filesep);
            contourImgName = [imgName{1} filesep CommonHandles.ImageFolder filesep imgName{3}];
            CommonHandles.OCC.LowProb.contourImageName{i} = contourImgName;
            CommonHandles.OCC.LowProb.selectedCell{i} = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
            CommonHandles.OCC.LowProb.plateName{i} = CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber;
            CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
        end
    elseif i==CommonHandles.HC.Default.NumberOfOutliers || (i<=CommonHandles.HC.Default.NumberOfOutliers-1 && i>1 && strcmp(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName))
        CommonHandles.SelectedCell = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
        CommonHandles = showTheCell(CommonHandles);
        CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
        CommonHandles.OCC.LowProb.featureVector(i,:) = CommonHandles.AllFeatures(currentCell,:);
        CommonHandles.OCC.LowProb.imageName{i} = CommonHandles.AllFeaturesMapping{currentCell}.ImageName;
        imgName = strsplit(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,filesep);
        contourImgName = [imgName{1} filesep CommonHandles.ImageFolder filesep imgName{3}];
        CommonHandles.OCC.LowProb.contourImageName{i} = contourImgName;
        CommonHandles.OCC.LowProb.selectedCell{i} = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
        CommonHandles.OCC.LowProb.plateName{i} = CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber;
        CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
    elseif i==1
        imgName = strsplit(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,filesep);
        contourImgName = [imgName{1} filesep CommonHandles.ImageFolder filesep imgName{3}];
        CommonHandles.SelectedImageName = contourImgName;
        CommonHandles.SelectedOriginalImageName = CommonHandles.AllFeaturesMapping{currentCell}.ImageName;
        CommonHandles = loadSelectedImage(CommonHandles,'currPlateNumber',CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber);
        [~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
        [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber);
        CommonHandles.SelectedCell = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
        CommonHandles = showTheCell(CommonHandles);
        CommonHandles.OCC.LowProb.img{i} = CommonHandles.CurrentSelectedCell;
        CommonHandles.OCC.LowProb.featureVector(i,:) = CommonHandles.AllFeatures(currentCell,:);
        CommonHandles.OCC.LowProb.imageName{i} = CommonHandles.AllFeaturesMapping{currentCell}.ImageName;
        CommonHandles.OCC.LowProb.contourImageName{i} = contourImgName;
        CommonHandles.OCC.LowProb.selectedCell{i} = CommonHandles.AllFeaturesMapping{currentCell}.CellNumberInImage;
        CommonHandles.OCC.LowProb.plateName{i} = CommonHandles.AllFeaturesMapping{currentCell}.CurrentPlateNumber;
        CommonHandles.OCC.Dendogram.selectedFeatures(i,:) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end);
        
        if ~strcmp(CommonHandles.AllFeaturesMapping{currentCell}.ImageName,CommonHandles.AllFeaturesMapping{sortedIndex(2)}.ImageName)
            imgName = strsplit(CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName,filesep);
            contourImgName = [imgName{1} filesep CommonHandles.ImageFolder filesep imgName{3}];
            CommonHandles.SelectedImageName = contourImgName;
            CommonHandles.SelectedOriginalImageName = CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.ImageName;
            CommonHandles = loadSelectedImage(CommonHandles,'currPlateNumber',CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.CurrentPlateNumber);           
            [~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
            [~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.AllFeaturesMapping{sortedIndex(i+1)}.CurrentPlateNumber);
        end
    else
        disp('Something is wrong, please check it!');
    end
    
    donePercent = double(counter/CommonHandles.HC.Default.NumberOfOutliers);
    waitText = sprintf('Getting the outliers... %d%% done', int16(donePercent * 100));
    waitbar(donePercent, waitBarHandle, waitText);
end
close(waitBarHandle);

CommonHandles.SelectedImageName = tmpSelImageName;
CommonHandles.SelectedOriginalImageName = tmpSelOriginalImageName;
CommonHandles.SelectedPlate = tmpSelPlate;
CommonHandles = loadSelectedImage(CommonHandles);
CommonHandles.SelectedCell = tmpSelCell;
CommonHandles = showSelectedCell(CommonHandles);
[~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
[CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.SelectedPlate);

end