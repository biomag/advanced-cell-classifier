function addHCSamplesToTrainingSet(featureMatrix)
% AUTHOR:	Tamas Balassa
% DATE: 	February 1, 2016
% NAME: 	addHCSamplesToTrainingSet
% 
% This method adds the selected cells featurevectors (so its a matrix) to
% a new class.
%
% INPUT:
%        featureMatrix     Matrix containing the featurevectors of cells
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    cellCounter = 0;
    
    for i=1:size(featureMatrix,1)        
        [~,indx] = ismember(featureMatrix(i,:),CommonHandles.OCC.LowProb.featureVector,'rows');
        currentCellNumberInImage = CommonHandles.OCC.LowProb.selectedCell{indx};
        currentImageName = CommonHandles.OCC.LowProb.imageName{indx};
        
        alreadyInTrainingSet = isInTrainingSet(CommonHandles.OCC.LowProb.contourImageName{indx},currentCellNumberInImage,0,CommonHandles,featureMatrix(i,:));
    
        if ~alreadyInTrainingSet
            selinfo = struct;
            selinfo.CellNumber = currentCellNumberInImage;
            selinfo.ImageName = CommonHandles.OCC.LowProb.contourImageName{indx};
            selinfo.OriginalImageName = currentImageName;
            selinfo.PlateName = CommonHandles.OCC.LowProb.plateName{indx};
            jumpToCell(selinfo,0);
            
            addToTrainingSet(CommonHandles.MaxCluster, featureMatrix(i,:));     
            cellCounter = cellCounter + 1;
        
            selinfo.Image = CommonHandles.OCC.LowProb.img{indx};
            selinfo.ClassName = CommonHandles.Classes{CommonHandles.MaxCluster}.Name;
            if ~CommonHandles.ClassImages.isKey(selinfo.ClassName)
                newclass = struct('Images',[],'selected',[],'sHandle',[]);
                CommonHandles.ClassImages(selinfo.ClassName) = newclass;
            end
            cimgs = CommonHandles.ClassImages(selinfo.ClassName);
            cimgs.Images{end+1} = selinfo;
            CommonHandles.ClassImages(selinfo.ClassName) = cimgs;
        end
    end
    
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i = 1: length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);        
    end
    
    if cellCounter == 0
        deleteFromClasses(CommonHandles.Classes{end}.Name);
    end
    
    refreshClassesList();
end
