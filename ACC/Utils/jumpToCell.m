function jumpToCell(structure, visible, varargin)
% AUTHOR:   Tamas Balassa, Abel Szkalisity
% DATE:     May 18, 2016
% NAME:     jumpToCell
% 
% This function will change the values of the current selected cell. It
% will use the values from the given structure and this will cause the
% software to jump to that cell. One can set this jump to be visible or
% not. (Basically if we want to get the details of different cells
% then invisible is faster. If we want to jump to an exact cell then it's
% supposed to be visible.)
%
% INPUT:
%   structure         It must contains the following values (with the exact
%                       fields)
%
%                           -CellNumber
%                           -ImageName
%                           -OriginalImageName
%                           -PlateName (number)
%
%   visible           It can be 1 (the jumping will be visible) or 0 (the
%                     jumping will be invisible, but the values will be set)
%   
%   OPTIONAL NAME-VALUE Pairs
%   loadType          This must be a string with the following possible values and meanings:
%                     'full': we would like to jump to the cell and load
%                     all information including metadata AND images
%                     'onlyMeta': loads in only the metadata, but the
%                     image fields of CommonHandles will NOT be set
%                     
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

p = inputParser();
p.addParameter('loadType','full',...
    @(x) any(validatestring(x,{...
        'full','onlyMeta'...
        })));
    
p.parse(varargin{:});

global CommonHandles;

if structure.PlateName == 0
    errordlg('You want to jump to a cell which is on a removed plate.');
else
    sameImage = 0;
    if strcmp(structure.ImageName,CommonHandles.SelectedImageName) && strcmp(structure.OriginalImageName,CommonHandles.SelectedOriginalImageName)
        sameImage = 1;
    end
    CommonHandles.SelectedImageName = structure.ImageName;
    CommonHandles.SelectedOriginalImageName = structure.OriginalImageName;
    CommonHandles.SelectedPlate = structure.PlateName;
    if ~sameImage
        if visible
            CommonHandles = loadSelectedImage(CommonHandles);
        else
            CommonHandles = loadSelectedImage(CommonHandles,'currPlateNumber',CommonHandles.SelectedPlate,'loadType',p.Results.loadType);
        end
    else
        %set the bounds to full.
        CommonHandles.MainView.XBounds = [1 CommonHandles.CurrentImageSizeY];
        CommonHandles.MainView.YBounds = [1 CommonHandles.CurrentImageSizeX];
    end    
    
    %This part seems to be unused    
    %[~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);
    %[~,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);
    
    if visible
        % update plate and image lists 
        
        
        %PlateList = get(CommonHandles.PlateListHandle,'String');

        %for iip=1:length(PlateList)
        %    if strfind(CommonHandles.SelectedImageName,PlateList{iip})>0
        %        set(CommonHandles.PlateListHandle,'Value',iip);
        set(CommonHandles.PlateListHandle,'Value',structure.PlateName);
        %        plateNumber = iip;
        %    end
        %end

        %CommonHandles.SelectedPlate = plateNumber;

        ImageList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep '*' CommonHandles.ImageExtension]);

        ImageListStr = char(ImageList.name);
        ImageListStr = cellstr(ImageListStr);
        set(CommonHandles.ImageListHandle, 'String', ImageListStr);


        for ii=1:length(ImageListStr)
            if strfind(CommonHandles.SelectedImageName,ImageListStr{ii})>0
                set(CommonHandles.ImageListHandle,'Value',ii);
                imageNumber = ii;
            end
        end

        set(CommonHandles.ImageListHandle, 'Value', imageNumber);

        % showimage function call
        %CommonHandles = loadSelectedImage(CommonHandles,'currPlateNumber',plateNumber);
        CommonHandles.SelectedCell = structure.CellNumber;
        CommonHandles = showSelectedCell(CommonHandles);
        
        % bring to front the image selector
        figure(CommonHandles.MainWindow);
        ImgSelectorParent = get(CommonHandles.ImageListHandle,'Parent');
        figure(ImgSelectorParent);
    else
        CommonHandles.SelectedCell = structure.CellNumber;
        CommonHandles = showTheCell(CommonHandles);
    end

end