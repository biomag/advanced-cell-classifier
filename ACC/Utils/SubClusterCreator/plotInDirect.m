function plotInDirect(clusterCandidates,tit,featuresMatrix)
%to plot clusters in the pcaSpace if available

color = getManyColor();

pcaScores = featuresMatrix;

if iscell(clusterCandidates)
    for i=1:length(clusterCandidates)
        figure;
        title(tit{i});
        for j=1:max(unique(clusterCandidates{i}))
            plot(pcaScores(clusterCandidates{i} == j,1),pcaScores(clusterCandidates{i} == j,2),'*','Color',color(j,:)./255);
            hold on;
        end
    end
else
    figure;
    for j=1:max(unique(clusterCandidates))
        plot(pcaScores(clusterCandidates == j,1),pcaScores(clusterCandidates == j,2),'*','Color',color(j,:)./255);
        title(tit);
        hold on;
    end
end

hold off;

end