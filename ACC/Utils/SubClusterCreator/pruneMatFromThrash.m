function pruneMatFromThrash(matFile)
%This function is to remove all thrash entries from the jump2CellMap cellarray
%stored in the matFile.

load(matFile,'jump2CellMap');

toDelete = false(1,length(jump2CellStructs)); %#ok<NODEF>loaded in

for i=1:numel
    if jump2CellStructs{i}.clusterIdx == -1
        toDelete(i) = true;
    end
end
jump2CellStructs(toDelete) = []; %#ok<NASGU> saved down

save(matFile,'jump2CellStructs');

end