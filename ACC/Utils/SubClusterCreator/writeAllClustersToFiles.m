function writeAllClustersToFiles(targetDir,sortByFilesMap,maxClustID,perImgTargetDir)
%Writes the CL2M machine learning layer import compatible files to disk
%into an AllClusters directory within the target.
%The only representative bool, indicates if we would like to create the
%data from all the cells or just the reduced set (stored as clusterIdx in the jump2Cell arrays)

global CommonHandles;
if nargin<4
    perImgTargetDir = fullfile(targetDir,'AllClusters');
else
    perImgTargetDir = fullfile(targetDir,perImgTargetDir);
end

if ~exist(perImgTargetDir,'dir'), mkdir(perImgTargetDir); end
jointFile = fopen(fullfile(targetDir,'AllClusters.csv'),'w');

fileKeys = sortByFilesMap.keys();
firstFileArray = sortByFilesMap(fileKeys{1});
headerCsv = jump2CellStruct2Csv(firstFileArray{1});
fprintf(jointFile,'%s\n',headerCsv);

colors = getManyColor;

[answerReindex] = questdlg('Do you have extra files for reindexing? (CL2M might not index consecutively and the compatibility can be regained with this file)','CL2M extra files','Yes','No','Yes');
if strcmp(answerReindex,'Yes')    
    extraFilesDir = uigetdir(CommonHandles.DirName);
else
    extraFilesDir = [];
end

h = waitbar(0,'Creating csv files...'); 
 
for i=1:numel(fileKeys)
    %Write to joint file
    currentJ2CArray = sortByFilesMap(fileKeys{i});    
    if ~isempty(extraFilesDir)
            d = dir(fullfile(extraFilesDir,['*' fileKeys{i} '*']));
            if length(d)==1
                newIndices = csvread(fullfile(extraFilesDir,d(1).name));
            else
                newIndices = [];
            end
    else
        newIndices = [];
    end        
    for j=1:numel(currentJ2CArray)
        %check if we have new indices
        if ~isempty(newIndices)
            currentJ2CArray{j}.CellNumber = newIndices(currentJ2CArray{j}.CellNumber);
        end        
        [~,dataCsv] = jump2CellStruct2Csv(currentJ2CArray{j});        
        fprintf(jointFile,'%s\n',dataCsv);                
    end    
            
    %Write to single files    
    singleFileToClusterCsv(currentJ2CArray,fileKeys{i},maxClustID,perImgTargetDir,colors);    
    
    if ishandle(h), waitbar(i/numel(fileKeys),h,sprintf('Creating csv files... (%d/%d)',i,numel(fileKeys))); end
end

fclose(jointFile);

if ishandle(h), close(h); end

end

function [headerCsv,dataCsv] = jump2CellStruct2Csv(j2C)
    header = fieldnames(j2C);
    headerCsv = strjoin(header,',');
    dataStr = cell(1,length(header));
    for i=1:length(dataStr)
        if isnumeric(j2C.(header{i}))
            dataStr{i} = num2str(j2C.(header{i}));
        else
            dataStr{i} = j2C.(header{i});
        end
    end
    dataCsv = strjoin(dataStr,',');
end