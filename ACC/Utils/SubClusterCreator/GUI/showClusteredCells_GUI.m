function varargout = showClusteredCells_GUI(varargin)
% SHOWTRAINEDCELLS_GUI MATLAB code for showClusteredCells_GUI.fig
%      SHOWTRAINEDCELLS_GUI, by itself, creates a new SHOWTRAINEDCELLS_GUI or raises the existing
%      singleton*. NOTE: this figure is not singleton as it is used for
%      multiple purposes (for show similar cells and trained cells too)
%
%      H = SHOWTRAINEDCELLS_GUI returns the handle to a new SHOWTRAINEDCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWTRAINEDCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWTRAINEDCELLS_GUI.M with the given input arguments.
%
%      SHOWTRAINEDCELLS_GUI('Property','Value',...) creates a new SHOWTRAINEDCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showClusteredCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showClusteredCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showClusteredCells_GUI

% Last Modified by GUIDE v2.5 15-Oct-2018 15:01:36

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showClusteredCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showClusteredCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showClusteredCells_GUI is made visible.
function showClusteredCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showClusteredCells_GUI (see VARARGIN)

% Choose default command line output for showClusteredCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showClusteredCells_GUI wait for user response (see UIRESUME)
% uiwait(handles.showClusteredCellsFig);

%varargin
% counter, thrash, csvFile, allClustersNow

%counter is a string with the full path to the folder
counter = varargin{1};
counter = counter{1};
uData.counter = counter;
uData.thrash = varargin{2};
uData.clusterMat = varargin{3};
handles.classesList.String = varargin{4};

gridParams = struct('imgsize',[128 128],'sepsize',10,'cols',8);
uData.gridParams = gridParams;

j2C = load(uData.clusterMat,'jump2CellMap');
[img,className,~] = createClusterImage(counter,gridParams,j2C.jump2CellMap);

ax = axes('Parent',hObject);
imHandle = imshow(img,'Parent',ax);
hSP = imscrollpanel(hObject,imHandle);
uData.hSP = hSP;
set(hSP,'Units','pixels','Position',[10 70 1200 500],'Tag','scrollPanel');
api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);
api.setImageButtonDownFcn({@scrollPanel_ButtonDownFcn,hObject});

%clear up previous selections        
    classImgObj.sHandle = [];
    classImgObj.selected = [];
    uData.classImgObj = classImgObj;
    set(handles.showClusteredCellsFig,'UserData',uData);
    
    set(hObject, 'Name', ['Class name: ' className]);

placeItemsOnGUI(handles);    
    



% % --- Outputs from this function are returned to the command line.
function varargout = showClusteredCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function scrollPanel_ButtonDownFcn(hObject,eventdata,fHandle)
% dummy code needed for old matlab
curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);
uData = get(fHandle, 'UserData');
counter = uData.counter;

j2c = load(uData.clusterMat,'jump2CellMap');
[~,~,mapping] = createClusterImage(counter,uData.gridParams,j2c.jump2CellMap);

classImgObj = uData.classImgObj;

[selimgInfo,hit,xcoord,ycoord] = selectCellFromClusterImage(x,y,mapping,uData.gridParams);

if hit
    if isfield(selimgInfo,'ImageName'), disp(selimgInfo.ImageName); end
    % Get axes object
    hSp = get(fHandle,'Children');
    axes = findobj(hSp,'Type','Axes');
    seltype = get(fHandle,'SelectionType');
    
    % Remove previous rectangle if exists, seltype == normal or counter>0
    % seltype == 'alt' for ctrl-click, 'extend' for shift-click
    if strcmp(seltype,'normal')
        classImgObj = removeRectangles(classImgObj);
    end
    toDelete = 0;
    if strcmp(seltype,'alt')
        if ~isempty(classImgObj.selected)
            for i=1:length(classImgObj.selected)
                if ~isempty(fieldnames(classImgObj.selected{i})) && strcmp(classImgObj.selected{i}.ImageName,selimgInfo.ImageName)
                    delete(classImgObj.sHandle{i});
                    classImgObj.sHandle(i) = [];
                    classImgObj.selected(i) = [];
                    toDelete = 1;
                    break;
                end
            end
        end
    end
    
    if ~toDelete
        % Draw borders around the image
        if ~isempty(selimgInfo)
            classImgObj = putRectangle(classImgObj,uData,xcoord,ycoord,selimgInfo,axes);
        end    
    end
   uData.classImgObj = classImgObj;
   set(fHandle,'UserData',uData);
end


% --- Executes on button press in unclassifierButton.
function unclassifierButton_Callback(hObject, eventdata, handles)
% hObject    handle to unclassifierButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figHandle = get(hObject,'Parent');
uData = get(figHandle, 'UserData');
counter = uData.counter;
classImgObj = uData.classImgObj;
thrashFolder = uData.thrash;

msgbox('Moving images to thrash', 'Images thrasing');
if ~isempty(classImgObj.selected)
    for i=1:length(classImgObj.selected)
        if ~isempty(fieldnames(classImgObj.selected{i}))   
            %delete image
            movefile(fullfile(counter,classImgObj.selected{i}.ImageName),fullfile(thrashFolder,classImgObj.selected{i}.ImageName));
            delete(classImgObj.sHandle{i});
            %delete also from csv            
            modifyEntryInMat(uData.clusterMat,{classImgObj.selected{i}.ImageName},-1);
        end
    end
    classImgObj.selected = [];
    classImgObj.sHandle = [];
end

sp = findobj(figHandle,'Tag','scrollPanel');
refreshClassClusterImage(counter, sp,uData.gridParams);


% --- Executes on button press in selectAll.
function selectAll_Callback(hObject, eventdata, handles)
% hObject    handle to selectAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uData = get(handles.showClusteredCellsFig,'UserData');

counter = uData.counter;

j2c = load(uData.clusterMat,'jump2CellMap');
[~,~,mapping] = createClusterImage(counter,uData.gridParams,j2c.jump2CellMap);

classImgObj = uData.classImgObj;

hSp = get(handles.showClusteredCellsFig,'Children');
axes = findobj(hSp,'Type','Axes');

%select all should work as such that if not everything is selected then it
%is select all, if all is selected then it is select none

%if less is selected
if length(classImgObj.selected) < sum(sum(~cellfun('isempty',mapping)))
    classImgObj = removeRectangles(classImgObj);
    for i=1:size(mapping,2)
        for j=1:size(mapping,1)
            if ~isempty(mapping{j,i})
                classImgObj = putRectangle(classImgObj,uData,i,j,mapping{j,i},axes);
            end
        end
    end
else
    classImgObj = removeRectangles(classImgObj);
end

uData.classImgObj = classImgObj;
set(handles.showClusteredCellsFig,'UserData',uData);


% --- Executes on button press in jumpToCell.
function jumpToCell_Callback(hObject, eventdata, handles)
% hObject    handle to jumpToCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figHandle = get(hObject,'Parent');
uData = get(figHandle, 'UserData');
classImgObj = uData.classImgObj;

info = classImgObj.selected;

if ~isempty(info) && length(info) == 1 && isfield(info{1},'ImageName')    
    jumpToCell(info{1}.jump2Cell,1)
else
    errordlg('No cell or multiple cells were selected.','Jump to cell error');
end


% --- Executes on button press in addToCluster.
function addToCluster_Callback(hObject, eventdata, handles)
% hObject    handle to addToCluster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figHandle = get(hObject,'Parent');
uData = get(figHandle, 'UserData');
counter = uData.counter;
classImgObj = uData.classImgObj;

if isempty(classImgObj.selected)
    errordlg('Please, select cell or cells to be added to the cluster', 'Add to cluster error');
else
    clusterNameString = get(handles.classesList,'String');
    clusterName = clusterNameString{get(handles.classesList,'Value')};

    S = strsplit(clusterName,'_');
    target = str2double(S{end});    
    S = strsplit(counter,filesep);
    clusterDir = strjoin(S(1:end-1),filesep);    
    nofSelected = length(classImgObj.selected);
    selectedImageNames = cell(1,nofSelected);
    % Collect features and preview images of all cells
    for i=1:nofSelected
        selinfo = classImgObj.selected{i};
        movefile(fullfile(counter,selinfo.ImageName),fullfile(clusterDir,clusterName,selinfo.ImageName));
        delete(classImgObj.sHandle{i});        
        selectedImageNames{i} = selinfo.ImageName;
    end
    modifyEntryInMat(uData.clusterMat,selectedImageNames,target);
    classImgObj.selected = [];
    classImgObj.sHandle = [];
    
    sp = findobj(figHandle,'Tag','scrollPanel');
    refreshClassClusterImage(counter, sp,uData.gridParams);
           
end

% --- Executes on selection change in classesList.
function classesList_Callback(hObject, eventdata, handles)
% hObject    handle to classesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns classesList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from classesList


% --- Executes during object creation, after setting all properties.
function classesList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in newCluster.
function newCluster_Callback(hObject, eventdata, handles)
% hObject    handle to newCluster (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figHandle = get(hObject,'Parent');
uData = get(figHandle, 'UserData');
counter = uData.counter;
classImgObj = uData.classImgObj;

if isempty(classImgObj.selected)
    errordlg('Please, select cell or cells to be added to the cluster', 'Add to cluster error');
else
    S = strsplit(counter,filesep);
    clusterDir = strjoin(S(1:end-1),filesep);
    d = dir([clusterDir filesep 'group_*']);
    maxID = 0;
    for i=1:numel(d)
        if isfolder(fullfile(clusterDir,d(i).name)) && length(strsplit(d(i).name,'_'))>1
            S = strsplit(d(i).name,'_');
            base = S{1};
            nofDigits = length(S{end});
            if str2double(S{end})>maxID && str2double(S{end})<Inf
                maxID = str2double(S{end}); 
            end
        end
    end    
    target = maxID + 1;
    className = [base '_' num2str(target,['%0' num2str(nofDigits) 'd'])];
    mkdir(fullfile(clusterDir,className));
    
    selectedImageNames = cell(1,length(classImgObj.selected));
    for i=1:length(classImgObj.selected)
        selinfo = classImgObj.selected{i};
        movefile(fullfile(counter,selinfo.ImageName),fullfile(clusterDir,className,selinfo.ImageName));
        delete(classImgObj.sHandle{i});
        selectedImageNames{i} = selinfo.ImageName;        
    end
    modifyEntryInMat(uData.clusterMat,selectedImageNames,target);
    
    classImgObj.selected = [];
    classImgObj.sHandle = [];
    
    sp = findobj(figHandle,'Tag','scrollPanel');
    refreshClassClusterImage(counter, sp, uData.gridParams);
end

close;
clusterSelector;


% --- Executes when showClusteredCellsFig is resized.
function showClusteredCellsFig_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to showClusteredCellsFig (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

placeItemsOnGUI(handles);

function placeItemsOnGUI(handles)
    uData = handles.showClusteredCellsFig.UserData;
    counter = uData.counter;
    xWidth = handles.showClusteredCellsFig.Position(3);
    yWidth = handles.showClusteredCellsFig.Position(4);
    sp = findobj(handles.showClusteredCellsFig,'Tag','scrollPanel');
    handles.classesList.Position(2) = 27;
    handles.addToCluster.Position(2) = 27;
    handles.newCluster.Position(2) = 27;
    handles.addToClass.Position(2) = 27;
    handles.newClassButton.Position(2) = 27;
    handles.unclassifierButton.Position(2) = 27;
    handles.jumpToCell.Position(2) = 27;
    handles.selectAll.Position(2) = 27;
    sp.Position(3:4) = [xWidth-100, yWidth-100];   

    



% --- Executes on scroll wheel click while the figure is in focus.
function showClusteredCellsFig_WindowScrollWheelFcn(hObject, eventdata, handles)
% hObject    handle to showClusteredCellsFig (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

cP = get(gcf,'CurrentPoint');
uData = get(handles.showClusteredCellsFig, 'UserData');
pP = get(uData.hSP,'Position');

if cP(1)>pP(1) && cP(1)< pP(1)+pP(3) && cP(2)>pP(2) && cP(2)< pP(2)+pP(4)
    api = iptgetapi(uData.hSP);
    visLoc = api.getVisibleLocation();
    if eventdata.VerticalScrollCount>0        
        visLoc(2) = visLoc(2)+30;    
    else
        visLoc(2) = visLoc(2)-30;    
    end
    api.setVisibleLocation(visLoc);
end



% --- Executes on button press in addToClass.
function addToClass_Callback(hObject, eventdata, handles)
% hObject    handle to addToClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

availableClassNames = getSpecClassList;

[idx,ok] = listdlg('ListString',availableClassNames,'PromptString','Select the destination class for the elements:');

if ~ok, return; end

uData = get(handles.showClusteredCellsFig,'UserData');

putBatchSamplesToTrainingSet(uData.classImgObj,idx);

%It is important also to create a reset to original button for the clusters
%if someone wants to start over.


% --- Executes on button press in newClassButton.
function newClassButton_Callback(hObject, eventdata, handles)
% hObject    handle to newClassButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

tmpCellInfo = makeTempCellData();

uData = get(handles.showClusteredCellsFig,'UserData');
if ~isempty(uData.classImgObj.selected)
    jumpToCell(uData.classImgObj.selected{1}.jump2Cell,0);
end

global CommonHandles;
oldClassNames = cell(1,length(CommonHandles.Classes));
for i=1:length(oldClassNames), oldClassNames{i} = CommonHandles.Classes{i}.Name; end

CommonHandles.HC.Node.createClass = 0;
uiwait(createClasses_GUI);

for i=1:length(CommonHandles.Classes)    
    if ~ismember(CommonHandles.Classes{i}.Name,oldClassNames)
        %newClassName = CommonHandles.Classes{i}.Name;
        newClassIdx = i;
        break;
    end
end

putBatchSamplesToTrainingSet(uData.classImgObj,newClassIdx);

jumpToCell(tmpCellInfo,0);


% --- To remove all highlight rectangles from the panel
function classImgObj = removeRectangles(classImgObj)
    if ~isempty(classImgObj.selected)
        for i=1:length(classImgObj.selected)
            if ~isempty(fieldnames(classImgObj.selected{i}))
                delete(classImgObj.sHandle{i});
            end
        end
        classImgObj.sHandle = [];
        classImgObj.selected = [];
    end

function classImgObj = putRectangle(classImgObj,uData,xcoord,ycoord,selimgInfo,axes)
    lw = 3;
    imgxsize = uData.gridParams.imgsize(1);
    imgysize = uData.gridParams.imgsize(2);
    sepsize = uData.gridParams.sepsize;
    drawX = (xcoord-1) * imgxsize + (xcoord) * sepsize - lw;
    drawY = (ycoord-1) * imgysize + (ycoord) * sepsize - lw;
    hRec = rectangle('Position',[drawX, drawY, imgxsize + 2*lw - 1, imgysize + 2*lw - 1],'EdgeColor', 'red', 'LineWidth',lw, 'Parent', axes);
    classImgObj.sHandle{end+1} = hRec;
    classImgObj.selected{end+1} = selimgInfo;
    
function putBatchSamplesToTrainingSet(classImgObj,counter)
    global CommonHandles;
    className = CommonHandles.Classes{counter}.Name;
    modifyClass = 0;
    
    nofChange = 0;
            
    tempCurrentCellInfo = makeTempCellData();
    h = waitbar(nofChange/length(classImgObj.selected),strrep(sprintf('Cells are placed to class %s. (%d/%d)',className,nofChange,length(classImgObj.selected)),'_','\_'));
    for i=1:length(classImgObj.selected)
        selinfo = classImgObj.selected{i}.jump2Cell;        

        alreadyInTrainingSet = isInTrainingSet( selinfo.ImageName, selinfo.CellNumber, 0, CommonHandles);

        if ~alreadyInTrainingSet
            jumpToCell(selinfo,0);            

            featureVector = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 3:size(CommonHandles.SelectedMetaData, 2));
        
            addToClassImages(counter);
            addToTrainingSet(counter,featureVector);                        
            nofChange = nofChange + 1;
        else
            
            if ~modifyClass                
                f = figure;   
                imshow(CommonHandles.CurrentSelectedCell,'Parent',gca());                
                button = questdlg(sprintf('Would you like to change the class of this cell?\nOriginal class: %s\nTarget class: %s\nImage name: %s\nCell number: %d',CommonHandles.Classes{logical(CommonHandles.TrainingSet.Class{alreadyInTrainingSet})}.Name,CommonHandles.Classes{counter}.Name,CommonHandles.SelectedImageName,CommonHandles.SelectedCell),'Reclassify?','Yes','Yes to all','No','Yes to all');                
            end
            if ishandle(f), close(f); end
            
            if strcmp(button,'Yes') || strcmp(button,'Yes to all')
                modifyClass = 1;
            end                                       
            
            if modifyClass
                % get cell info
                jumpToCell(selinfo,0);                        
                % add cell to ClassImages
                addToClassImages(counter);
                % add cell to TrainingSet
                moveClassImage(CommonHandles.SelectedCell, CommonHandles.SelectedImageName, counter);                  
                nofChange = nofChange + 1;
            end
            
            if strcmp(button,'Yes')
                modifyClass = 0;
            end

        end
        if ishandle(h)
            waitbar(nofChange/length(classImgObj.selected),h,strrep(sprintf('Cells are placed to class %s. (%d/%d)',className,nofChange,length(classImgObj.selected)),'_','\_'));
        end
    end
    
    %To update the class numbers
    refreshClassesList();
    if ishandle(h), close(h); end
    msgbox(['You''ve successfully added ' num2str(nofChange) ' cells to ' className ' class']);
    jumpToCell(tempCurrentCellInfo,0);

