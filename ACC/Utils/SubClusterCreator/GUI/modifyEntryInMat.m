function  modifyEntryInMat( matFile, imageNameList,value )
%cluster csv removal
%   sets entries to -1 to indicate that it is not in a cluster
%
% NOTE: This function does not keep the sortByFilesMap and the jump2CellMap
% consistent any more to enhance user experience (fasten the reordering) (as sortByFilesMap can be extremely huge)

load(matFile,'jump2CellMap');

for i=1:length(imageNameList)
    [~,imgNameKey,~] = fileparts(imageNameList{i});   
    cellImageObj = jump2CellMap(imgNameKey);    
    cellImageObj.clusterIdx = value;
    jump2CellMap(imgNameKey) = cellImageObj;    
end

save(matFile,'jump2CellMap');

end

