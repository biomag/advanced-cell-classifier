function varargout = clusterSelector(varargin)
% CLUSTERSELECTOR MATLAB code for clusterSelector.fig
%      CLUSTERSELECTOR, by itself, creates a new CLUSTERSELECTOR or raises the existing
%      singleton*.
%
%      H = CLUSTERSELECTOR returns the handle to a new CLUSTERSELECTOR or the handle to
%      the existing singleton*.
%
%      CLUSTERSELECTOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLUSTERSELECTOR.M with the given input arguments.
%
%      CLUSTERSELECTOR('Property','Value',...) creates a new CLUSTERSELECTOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before clusterSelector_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to clusterSelector_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help clusterSelector

% Last Modified by GUIDE v2.5 21-Jun-2019 11:31:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @clusterSelector_OpeningFcn, ...
                   'gui_OutputFcn',  @clusterSelector_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before clusterSelector is made visible.
function clusterSelector_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to clusterSelector (see VARARGIN)
%
%   varargin optional arguments:
%       arg1:       clusterDir the directory where the cluster directories
%       are located (optional)
%       arg2:       the full path and filename of the mat file containing
%       the .mat file for the cluter information (must go along with arg1)

% Choose default command line output for clusterSelector
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;

% UIWAIT makes clusterSelector wait for user response (see UIRESUME)
% uiwait(handles.figure1);

uData = get(handles.figure1,'UserData');
if ~isfield(uData,'clusterDir')
    if nargin>3
        uData.clusterDir = varargin{1};
        uData.clusterMat = varargin{2};
    else        
        if ~isempty(CommonHandles)
            startDir = CommonHandles.DirName;
        else
            startDir = pwd;
        end
        %uData.clusterDir = 'E:\kaggle\Clustering\clusters\Kmeans-cosine-Best5Cluster';%'E:\kaggle\Clustering\clusters\Kmeans-cosine-Best5Cluster';    
        uData.clusterDir = uigetdir(startDir,'Select folder of clusters');
        if isnumeric(uData.clusterDir), close(hObject); return; end
        %uData.clusterCsv = 'E:\kaggle\Clustering\predictedStyles_Kmeans-cosine-Best5Cluster.csv';
        [f,p] = uigetfile(fullfile(uData.clusterDir,'*.mat'),'Select the cluster mat file');
        if isnumeric(f), close(hObject); return; end
        uData.clusterMat = fullfile(p,f);
        if ~checkClusterConsistency(uData.clusterDir,uData.clusterMat)
            answer = questdlg(sprintf('The selected folder structure and .mat file are not consistent.\nDo you want to reorganize the folder structure\naccording to the mat file?'),'Recreate clusters','Yes','No','Yes');
            if ~isnumeric(answer) && strcmp(answer,'Yes')
                remakeClusterConsistency(uData.clusterDir,uData.clusterMat);
            else
                msgbox('It is not possible to launch with non-consistent folder and mat structures');
                close(hObject);
                return;
            end
        end
    end
    
    if (isnumeric(uData.clusterDir) && uData.clusterDir == 0) || (isnumeric(uData.clusterMat) && uData.clusterMat == 0 )
        close(hObject);
        return;
    end
    
    [a,~,c] = fileparts(uData.clusterMat); 
    newMatFile = fullfile(a,['.TMP_clusters_' timeString(6) c]);
    if ~exist(newMatFile,'file') %It should not exist... copy only the neccessary part
        load(uData.clusterMat,'jump2CellMap');
        save(newMatFile,'jump2CellMap');
    else
        disp('Erroneous behaviour: TMP FILE should not exist.')
    end
    uData.originalClusterMat = uData.clusterMat;
    uData.clusterMat = newMatFile;
    uData.thrash = fullfile(uData.clusterDir,'group_Inf'); %TRASH
    if ~exist(uData.thrash,'dir'), mkdir(uData.thrash); end %TRASH
end
d = dir(fullfile(uData.clusterDir,'group*'));
dF = d(logical(cell2mat({d.isdir})));

set(handles.popupmenu1,'String',{dF.name});
set(handles.figure1,'UserData',uData);


% --- Outputs from this function are returned to the command line.
function varargout = clusterSelector_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if ~isempty(handles)
    varargout{1} = handles.output;    
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = handles.figure1.UserData;
currF = handles.popupmenu1.String{handles.popupmenu1.Value};


showClusteredCells_GUI({fullfile(uData.clusterDir,currF)},uData.thrash,uData.clusterMat,handles.popupmenu1.String);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%{
global CommonHandles;

uData = get(handles.figure1,'UserData');

j2c = load(uData.clusterMat);
jump2CellMap = j2c.jump2CellMap;

clusterString = handles.popupmenu1.String;

allFeatures = cell(length(jump2CellMap.keys),1);
clusterIdx = zeros(length(jump2CellMap.keys),1);

Keys = jump2CellMap.keys;

%load('debugTemp.mat');
%{
h = waitbar(0,sprintf('Fetching features (%d/%d)',0,length(Keys)));
for i = 1:length(Keys)
    if ishandle(h), waitbar(i/length(Keys),h,sprintf('Fetching features (%d/%d)',i,length(Keys))); end
    j2cStruct = jump2CellMap_get(jump2CellMap,Keys{i});
    %jumpToCell(j2cStruct,0);
    allFeatures{i} = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,:);
    clusterIdx(i) = j2cStruct.clusterIdx;
end
if ishandle(h), close(h); end
%featuresMatrix = cell2mat(allFeatures);

%save('debugTemp.mat','featuresMatrix','clusterIdx');
%}
%load('debugTemp.mat');

featureList = getFeatureList();

if ~isempty(featureList)
    [selectedFeatures,ok] = listdlg('ListString',featureList,'PromptString','Select features to be shown');
end
if ok
    featuresMatrix = featuresMatrix(:,selectedFeatures);
end

featuresMatrixCentered = featuresMatrix - repmat(mean(featuresMatrix),size(featuresMatrix,1),1);
featuresMatrixNormalized = featuresMatrixCentered./repmat(std(featuresMatrix),size(featuresMatrix,1),1);

aggregatedMatrix = zeros(length(clusterString),length(selectedFeatures));
ids = unique(clusterIdx);

for i = 1:length(ids)
    aggregatedMatrix(i,:) = mean(featuresMatrixNormalized(clusterIdx == ids(i),:));
end

figure();

imagesc(aggregatedMatrix);
title('Aggregated for showed features');
[pcaCoeff,~,latent] = pca(featuresMatrixNormalized);

%Suggestion, to replace this really simple thing: to create a GUI to play around with PCA or LDA also to see
%which features play the most important role to separate the data

latentPercent = latent./sum(latent);

summa = 0; i = 1;
while summa<0.99
    summa = summa + latentPercent(i);
    i = i+1;
end

nnPercentThreshold = i;
PCALabels = cell(1,nnPercentThreshold);
for i=1:nnPercentThreshold
    PCALabels{i} = ['PCA-' num2str(i,'%03d')];
end

figure;
imagesc(latentPercent(1:nnPercentThreshold)')
title('The explained variance of the principal components for the 99% of the total');

figure;
imagesc(pcaCoeff(:,1:nnPercentThreshold))
title('Principal compoenents for 99% vs. original features');
xticks(1:nnPercentThreshold);
yticks(1:length(selectedFeatures));
xticklabels(PCALabels);
yticklabels(featureList(selectedFeatures));

figure;
imagesc(pcaCoeff(:,1:5));
title('1st 5 principal compoenents vs. original featuers');
xticks(1:5);
yticks(1:length(selectedFeatures));
xticklabels(PCALabels(1:5));
yticklabels(featureList(selectedFeatures));
%}


% --- Executes on button press in button_exportToCL2M.
function button_exportToCL2M_Callback(hObject, eventdata, handles)
% hObject    handle to button_exportToCL2M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
[targetDir] = uigetdir(uData.clusterDir,'Select a folder to save the representative elements');

if isnumeric(targetDir), return; end

load(uData.clusterMat,'jump2CellMap');
sortByFilesMap = sortCellsByFileName(jump2CellMap.values);

d = dir(fullfile(uData.clusterDir,'group*'));
dF = d(logical(cell2mat({d.isdir})));

writeAllClustersToFiles(targetDir,sortByFilesMap,length(dF)-1,['ClustersByHand_' timeString(5)]); %-1 to get rid of Inf folder




% --- Executes on button press in button_saveClusters.
function button_saveClusters_Callback(hObject, eventdata, handles)
% hObject    handle to button_saveClusters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[j2MapFile,j2MapPath] = uiputfile('*.mat','Please specify a name to save your clustering',['ClustersByHand_' timeString(5) '.mat']);

if ~isnumeric(j2MapFile)
    uData = get(handles.figure1,'UserData');
    copyfile(uData.clusterMat,fullfile(j2MapPath,j2MapFile));    
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
uData = get(handles.figure1,'UserData');

if ~isempty(uData)
    button = questdlg('Do you want to save clusters before quit?','Quit question','Yes, save','No / I''ve saved it already','Cancel','Yes, save');
    if strcmp(button, 'Yes, save')
        button_saveClusters_Callback(handles.button_saveClusters, [], handles)    
    elseif strcmp(button, 'Cancel')
        return;
    end        

    if isfield(uData,'clusterMat') && exist(uData.clusterMat,'file')
        delete(uData.clusterMat);
    end
end

delete(hObject);
