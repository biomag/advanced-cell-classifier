function singleFileToClusterCsv(j2C,fileKey,maxClustID,perImgTargetDir,colors)
%Writes a single line into a separate file according to CL2M nomenclature.    
    tgtCsvName = fullfile(perImgTargetDir,[fileKey '.csv']);
    perImgCsvF = fopen(tgtCsvName,'w');
    %Header        
    fprintf(perImgCsvF,'id,PREDICTED CLASS ID');        
    for ii=1:maxClustID
        fprintf(perImgCsvF,',CLASS %d #%02x%02x%02x Cluster_%d',ii,colors(ii,1),colors(ii,2),colors(ii,3),ii);
    end
    fprintf(perImgCsvF,'\n');
    for i=1:numel(j2C)                    
        %Data lines        
        if exist('onlyRepresentativeBool','var')
            writeSingleLineToCL2M(j2C{i},perImgCsvF,maxClustID);
        else
            writeSingleLineToCL2M(j2C{i},perImgCsvF,maxClustID);
        end
    end
    fclose(perImgCsvF);
end

function writeSingleLineToCL2M(j2C,perImgCsvF,maxClustID)    
    fprintf(perImgCsvF,'%d,%d',j2C.CellNumber,j2C.clusterIdx);                    
    if isfield(j2C,'allClusterDistance') && length(j2C.allClusterDistance) == maxClustID
        for ii=1:maxClustID
            fprintf(perImgCsvF,',%f',j2C.allClusterDistance(ii));
        end
    else
        fprintf(perImgCsvF,',%s',strjoin(repmat({'Inf'},1,maxClustID),','));
    end        
    fprintf(perImgCsvF,'\n');     
end
