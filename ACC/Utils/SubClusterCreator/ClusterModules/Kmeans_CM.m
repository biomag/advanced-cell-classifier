classdef Kmeans_CM < ClusterModule
    %Kmeans_CM a wrapper for kmeans
    %CM stands at the end for ClusterModule so that name conflict is
    %avoided with original kmeans
    
    properties                
        k
        nofBestInputs
        distanceMeasure
        mergeBool
        toPlotInPCA
        useParallel
        onlinePhase
    end
    
    methods
        function [jump2CellStructs,j2CFiltered,maxClustId] = clusterCells(obj, featuresMatrix, jump2CellStructs)
            rng(20010911);
            if strcmp(obj.useParallel,'Yes')
                options = statset('UseParallel',true); 
            else
                options = statset('UseParallel',false); 
            end
            [clusterCandidates,~,~,D] = kmeans(featuresMatrix,obj.k,'Replicates',30,'MaxIter',10000','Distance',obj.distanceMeasure,'Options',options,'OnlinePhase',obj.onlinePhase);
            idx = obj.filterBest(D,clusterCandidates);
            if strcmp(obj.mergeBool,'Yes')
                %disp('stop');
            end
            kk = 1;
            j2CFiltered = cell(1,length(jump2CellStructs));
            for i=1:length(jump2CellStructs)
                if idx(i)~=-1
                    j2CFiltered{kk} = jump2CellStructs{i};
                    j2CFiltered{kk}.clusterIdx = idx(i);
                    kk = kk + 1;
                end                        
                %jump2CellStructs{i}.clusterIdx = idx(i);
                jump2CellStructs{i}.clusterIdx = clusterCandidates(i);
                jump2CellStructs{i}.allClusterDistance = D(i,:);
            end
            j2CFiltered(kk:end) = [];
            if obj.toPlotInPCA
                plotInPCA(clusterCandidates,'PCA: Clusters for all cells',featuresMatrix);
                plotInPCA(idx,['PCA: Clusters for best ' num2str(obj.nofBestInputs)],featuresMatrix);
            end            
            maxClustId = obj.k;
        end
        
        function paramarray = getParameterValues(obj)
            paramarray{1} = obj.k;
            paramarray{2} = obj.nofBestInputs;
            paramarray{3} = obj.distanceMeasure;
            paramarray{4} = obj.useParallel;
            paramarray{5} = obj.onlinePhase;
            paramarray{6} = obj.mergeBool;
            paramarray{7} = obj.toPlotInPCA;
        end
        
        function fillParams(obj,paramarray)
            obj.k = paramarray{1};
            obj.nofBestInputs = paramarray{2};
            obj.distanceMeasure = paramarray{3};
            obj.useParallel = paramarray{4};
            obj.onlinePhase = paramarray{5};
            obj.mergeBool = paramarray{6};
            obj.toPlotInPCA = paramarray{7};
        end
        
        function str = paramsToString(obj)
            str = [...
                class(obj) ...
                'k_' num2str(obj.k) '_' ...
                'bI_' num2str(obj.nofBestInputs) '_' ...
                'dM_' obj.distanceMeasure '_' ...
                'uP_' obj.useParallel '_' ...
                'oP_' obj.onlinePhase '_' ...
                'mB_' obj.mergeBool ...
                ];           
        end
        
        function updatedIdx = filterBest(obj,distM,idx)
            updatedIdx = idx;
            for i=1:obj.k                
                thisClusterID = find(idx == i);                
                distance = distM(thisClusterID,i);
                [~,sortedIdx] = sort(distance);
                updatedIdx(thisClusterID(sortedIdx(obj.nofBestInputs+1:end))) = -1;
            end
        end
        
        
    end
    
    methods (Static)
        function paramarray = getParameters()
            paramarray{1}.name = 'k';            
            paramarray{1}.type = 'int';
            paramarray{1}.default = 15;
            paramarray{2}.name = '# of best cells to keep';
            paramarray{2}.type = 'int';
            paramarray{2}.default = 50;
            paramarray{3}.name = 'distanceMeasure';
            paramarray{3}.type = 'enum';
            paramarray{3}.values = {'correlation','cosine'};
            paramarray{3}.default = 1;
            paramarray{4}.name = 'Use parallel?';
            paramarray{4}.type = 'enum';
            paramarray{4}.values = {'No','Yes'};
            paramarray{4}.default = 1;
            paramarray{5}.name = 'Online phase?';
            paramarray{5}.type = 'enum';
            paramarray{5}.values = {'off','on'};
            paramarray{5}.default = 1;
            paramarray{6}.name = 'Merge close clusters?';
            paramarray{6}.type = 'enum';
            paramarray{6}.values = {'Yes', 'No'};       
            paramarray{6}.default = 1;
            paramarray{7}.type = 'checkbox';
            paramarray{7}.name = 'Plot result in PCA?';
            paramarray{7}.default = 1;            
        end
    end
end

