function bool = checkClusterConsistency(folder,mapLocation)
%Function that checks if the folder structure is consistent with the map
%stored at the mapLocation. The map is a jump2CellMap where each key is an
%image in the cluster folder structure (essentially a cell), and the value
%is a jump2Cell structure AND the cluster IDx (group_IDx) in which folder
%thecorresponding image should be placed.
%Warning: the folder numbering is assumed to be 3 digit long and the type
%to be png.
    
j2C = load(mapLocation,'jump2CellMap');

jump2CellMap = j2C.jump2CellMap;

imgNames = jump2CellMap.keys();

bool = 1;
for i=1:numel(imgNames)
    clusterIdx = jump2CellMap(imgNames{i}).clusterIdx;
    if ~exist(fullfile(folder,['group_' num2str(clusterIdx,'%03d')],[imgNames{i} '.png']),'file')
        bool = 0;
        break;
    end    
end

end