classdef (Abstract) ClusterModule < handle
    %ClusterModule To Perform clustering.
    %   This class encapsulates different clustering algorithms. They can
    %   have their own parameters, but must have the following functions to
    %   make it possible to create a parameter GUI from them.
    %   For more info check out ActiveLearner.m or Predictor.m abstract
    %   classes.
    
    properties        
    end
    
    methods (Abstract)
        paramarray = getParameterValues(obj);
        [jump2CellStructs,j2CFiltered,maxCluster] = clusterCells(obj,featuresMatrix, jump2CellStructs);
        %Jump2CellStructs in the result is almost the same as in the input.
        %The only difference that each entry in the output cellarray is
        %extended with a field called 'clusterIdx' that contains the
        %cluster index for each cell.
        %j2CFiltered is a much smaller array containing information
        %similarly to the previous output, but only for a specified smaller
        %number of cells per cluster.
        %The feature matrix contains observations as rows.        
        %maxCluster is the ID of the maximal cluster
        
        fillParams(obj,paramcell);
        %To update the module parameters automatically from the paramcell
        %given back by the parameter (Settings_GUI) gui.
        
        str = paramsToString(obj);
        %To generate a string describing the parameters used in this module
    end           
    
    methods (Abstract,Static)
         paramarray = getParameters();
    end
    
    methods (Static)
         function [bool,msg] = checkParameters(~)
            bool = 1;
            msg = '';
         end
    end
end

