function [jump2CellMap] = sortCellsToFolders(targetDir,j2CFiltered)
%Based on Kaggle 0_Preprocessing/maskGeneration/sortToFolders
%This is an ACC hacking code also.
%The jump2CellMap is a map with key-value pairs
%   the key is the string with the img id
%   makeThumb is a bool which indicates if we want to create the small
%   thumbnail images
    global CommonHandles;
    [ tempCurrentCellInfo ] = makeTempCellData();    
        
    infoStr = 'Creating thumbnails...';    
    h = waitbar(0,infoStr);

    jump2CellMap = containers.Map();    
    for i=1:numel(j2CFiltered)                        
        
        if ishandle(h), waitbar(i/numel(j2CFiltered),h,sprintf([infoStr '%d/%d'],i,numel(j2CFiltered))); end

        str = j2CFiltered{i};
        target = j2CFiltered{i}.clusterIdx;                        
        jumpToCell(str,0);
        tcd = fullfile(targetDir,['group_' num2str(target,'%03d')]);
        if ~isfolder(tcd), mkdir(tcd); end
        [~,imageNameID,~] = fileparts(str.ImageName);
        cellImageName = ['ID' num2str(i,'%07d') '_P' num2str(str.PlateName,'%02d') '_' imageNameID '_Cell' num2str(str.CellNumber,'%04d') ];
        jump2CellMap(cellImageName) = j2CFiltered{i};
        imwrite(CommonHandles.CurrentSelectedCell,fullfile(tcd,[cellImageName '.png']));
    end    
    
    jumpToCell(tempCurrentCellInfo,0);%set back ACC to the correct image
    if ishandle(h), close(h);        
end