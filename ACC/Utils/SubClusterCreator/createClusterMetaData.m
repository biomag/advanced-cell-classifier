%Creates a metadata file from the current clustering

if ~iscell(featureFile), featureFile = {featureFile}; end

metaDataF = fopen(fullfile(targetDir,'clusterMetaData.txt'),'w');
fprintf(metaDataF,...
    ['Class used for clustering: %s\n' ...
    'Single cell prediction path: %s\n' ...
    'Single cell prediction file: %s\n'...        
    'The used clustering algorithm ID: %s\n',...
    'The used data type: %s\n'...
    ],...
    classString,...
    pathSingle,...
    fileSingle,...    
    clusterM.paramsToString(),...
    dataType...
    );

fprintf(metaDataF,...
    '\nThe feature files are coming from %s\nwith their paths:\n',featurePath);
for i=1:length(featureFile)    
    fprintf(metaDataF,'%s\n',featureFile{i});    
end

fprintf(metaDataF,'\nThe used features for clustering were the following:\n\n');

for i=1:length(featureListIdx)
    fprintf(metaDataF,'%s\n',featureList{featureListIdx(i)});
end

%TODO: include here the extended classification prediction file.

fclose(metaDataF);
