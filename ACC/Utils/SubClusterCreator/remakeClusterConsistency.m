function remakeClusterConsistency(folder,mapLocation)
%Function that recreates cluster consistency between the jump2CellMap
%stored at mapLocation and the folder structure below the folder directory.
%It assumes that the mapLocation is the correct clustering and reorganizes
%the folder structure in turn.
    
j2C = load(mapLocation,'jump2CellMap');

jump2CellMap = j2C.jump2CellMap;

imgNames = jump2CellMap.keys();
h = waitbar(0,sprintf('Recreating clusters %d%%',floor(0)));

for i=1:numel(imgNames)
    clusterIdx = jump2CellMap(imgNames{i}).clusterIdx;
    actGroupName = ['group_' num2str(clusterIdx,'%03d')];
    if ~exist(fullfile(folder,actGroupName,[imgNames{i} '.png']),'file')
        %If the file is not at the correct place then search for it
        d = dir(folder);
        for j=1:numel(d)
            if exist(fullfile(folder,d(j).name),'dir') && ~strcmp(d(j).name,actGroupName)
                if exist(fullfile(folder,d(j).name,[imgNames{i} '.png']),'file')
                    movefile(fullfile(folder,d(j).name,[imgNames{i} '.png']),fullfile(folder,actGroupName,[imgNames{i} '.png']));
                    fprintf('Moving:\n%s\nto:\n%s\n',fullfile(d(j).name,[imgNames{i} '.png']),fullfile(actGroupName,[imgNames{i} '.png']))
                end
            end
        end
    end    
    if ishandle(h), waitbar(i/numel(imgNames),h,sprintf('Recreating clusters %d%%',floor(i/numel(imgNames)*100))); end
end

if ishandle(h), close(h); end

end