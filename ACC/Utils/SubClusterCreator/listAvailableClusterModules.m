function clusterModules = listAvailableClusterModules()
% This functions should list all the available cluster modules
% OUTPUT: cellarray with the strings of the cluster module names.

%list the  folder and the implementations
wS = what(['Utils' filesep 'SubClusterCreator' filesep 'ClusterModules']);
w = struct2cell(wS);
m = w{2};
% for only the math files
for i = 1:length(m)
    %see its name without .m
    splitted = strsplit(m{i},'.');
    %rename
    m{i} = splitted{1};
end
%return the cellarray
clusterModules = m;

