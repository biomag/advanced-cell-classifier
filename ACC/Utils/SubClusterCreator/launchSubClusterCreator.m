function launchSubClusterCreator()
% AUTHOR:	Abel Szkalisity
% DATE: 	October 5, 2018
% NAME: 	launchSubClusterCreator
% 
% This function prepares and manages the later life cycle of the SubCluster
% Creator Module. This module asks for a specific class, and a plate
% prediction (in single cell mode), and then loads in all the samples which
% were predicted to the selected class (in the selected prediction). Then
% it creates a clustering on those samples, selects some of the best images
% from the clusters (where it is applicable, for instance in k-means it is
% meaningful to select the 50 cells closest to the cluster centers). Then
% it creates a folder structure to the ACC directory (called clusterStore),
% to where it saves down the different clusterings. There is a GUI that
% shows you the different clusters and makes it possible to create new ACC
% class from clusters, to delete elements from them etc.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
%Check if project is loaded
if ~(isfield(CommonHandles,'Success') && CommonHandles.Success), return; end

%info to users
answer = questdlg(sprintf('Welcome to SubCluster Creator!\nPlease select an option!'),'Welcome','Create new clusters','Open existing clusters','Create new clusters');

%creating folder if it doesn't exist
clusterFolderName = 'SubClusterCreator';
if ~exist(fullfile(CommonHandles.DirName,clusterFolderName),'dir')
    mkdir(fullfile(CommonHandles.DirName,clusterFolderName));
end

%init msgbox
h = msgbox('Info will come here');

if isempty(answer), return; end

if strcmp(answer,'Create new clusters')
    
    answer = questdlg(sprintf('Do you want to cluster all cells or just specific classes?')...
        ,'Base class selection','Specific classes','All classes','Specific classes');    
    if isempty(answer), return; end
    
    if strcmp(answer,'Specific classes')
        [fileSingle,pathSingle] = uigetfile(fullfile(CommonHandles.DirName,'Output','*.csv'),'Please specify a single cell prediction result file');
        %path = 'D:\Abel\SZBK\Projects\Marcsi\FFPE_Hella_project\Output';
        %file = 'B16_2048_selected_singleCellData.csv';
        if isnumeric(fileSingle), return; end

        if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {'Reading single cell information...'}; end
        dataT = readtable(fullfile(pathSingle,fileSingle));           
    
        answer = questdlg(sprintf('Do you want to load in features for new single cell prediction \n OR \n to use previously loaded one?')...
            ,'Load features','Load in new prediction','Use previously loaded','Load in new prediction');

        if isempty(answer), return; end
        if strcmp(answer,'Load in new prediction')
            for i=1:numel(CommonHandles.Classes), classNames{i} = CommonHandles.Classes{i}.Name; end
            %WARNING: The loaded data should come from the same class structure
            %to make this list valid. (i.e. one should not change the number and order of classes as in that case the class names and numbers will not match)
            if length(unique(dataT.Class)) ~= length(classNames)
                warndlg('The number of classes in the prediction and in the current project doesn''t agree');
            end
            [classToCollect,~] = listdlg('PromptString','Select a class for feature load','ListString',classNames,'SelectionMode','multiple');                    

            if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {['Collecting features for classes: ' num2str(classToCollect) ' ...' ]}; end
            [featuresInClass,jump2CellStructs,classString,featurePath,featureFile] =...
                collectFeaturesForClass(dataT,classToCollect); %#ok<ASGLU> used later in script
        elseif strcmp(answer,'Use previously loaded')
            [featureFile,featurePath] = uigetfile(fullfile(CommonHandles.DirName,'*.mat'),'Specify a location and filename for the features','MultiSelect','on');
            if isnumeric(featureFile), return; end
            if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {'Loading previously collected features...'}; end
            if ~iscell(featureFile), featureFile = {featureFile}; end
            
            initCell = cell(1,length(featureFile));
            fIC = initCell; j2CS = initCell; cTC = initCell; cS = initCell;
            for i=1:length(featureFile)
                load(fullfile(featurePath,featureFile{i}),'featuresInClass','jump2CellStructs','classToCollect','classString');
                fIC{i} = featuresInClass; j2CS{i} = jump2CellStructs; cTC{i} = classToCollect; cS{i} = classString;
            end
            disp('stop');
        end        
    elseif strcmp(answer,'All classes')
        pathSingle = 'NR:All classes are loaded'; %#ok<NASGU>
        fileSingle = 'NR:All classes are loaded'; %#ok<NASGU>
        answer = questdlg(sprintf('Do you want to load in features for new single cell prediction \n OR \n to use previously loaded one?')...
            ,'Load features','Load in new prediction','Use previously loaded','Load in new prediction');

        if isempty(answer), return; end
        
        if strcmp(answer,'Load in new prediction')            
            classToCollect = Inf;  
            classString = 'ALL-Cells'; 
            [ features, mapping, coords] = loadAllFeatures( CommonHandles, 1, true);
            jump2CellStructs = cell(length(mapping),1);
            for i=1:length(mapping)
                jump2CellStructs{i}.CellNumber = mapping{i}.CellNumberInImage;
                jump2CellStructs{i}.ImageName = mapping{i}.ImageName;
                sIN = strsplit(jump2CellStructs{i}.ImageName,{'/','\'});                
                jump2CellStructs{i}.OriginalImageName = fullfile(sIN{1},CommonHandles.OriginalImageFolder,sIN{3});
                jump2CellStructs{i}.PlateName = mapping{i}.CurrentPlateNumber; 
            end
            featuresInClass = mat2cell([coords features],ones(1,size(features,1)),size(features,2)+2);
            if ishandle(h) 
                pause(0.1); 
                h.get('CurrentAxes').Children.String = sprintf(...
                'Feature collection has completed!\n Please specify a file name for the features for the prediction of this class.\nWe recommend to add the name of the machine learning model to this filename.\nYou can use this file later on to create different clusterings for this class.'); 
            end

            [featureFile,featurePath] = uiputfile('.mat','Specify a location and filename for these features',...
            fullfile(CommonHandles.DirName,['ClassInfo_C' num2str(classToCollect,'%02d')]));            

            if ~isnumeric(featureFile), save(fullfile(featurePath,featureFile),'-v7.3','featuresInClass','jump2CellStructs','classToCollect','classString'); end            
        elseif strcmp(answer,'Use previously loaded')
            [featureFile,featurePath] = uigetfile(fullfile(CommonHandles.DirName,'*.mat'),'Specify a location and filename for the features');            
            if isnumeric(featureFile), return; end
            if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {'Reading single cell information...'}; end            
            
            load(fullfile(featurePath,featureFile),'featuresInClass','jump2CellStructs','classToCollect','classString'); %#ok<NASGU> used in script
        end
    end
    
    if ~exist('featuresInClass','var') || ~exist('jump2CellStructs','var'), disp('missing features'); return; end
    
    prepareForClustering;
    performClustering;        
    
    %both above scripts can return without having the proper parameters
    %specified
    if exist('targetDir','var') && exist('targetFile','var') && ~isnumeric(targetDir) && ~isnumeric(targetFile) && strcmp(answerACCViewer,'Yes')
        clusterSelector(targetDir,targetFile); %the vars are coming from the above 2 scripts
    end
    
elseif strcmp(answer,'Open existing clusters')
    %warndlg(sprintf('The create class from cluster, the add to class etc. options will work\nonly if you load in a previous clustering from the current project!'));
    clusterSelector();
end

if ishandle(h), close(h); end

end