function value = jump2CellMap_get(jump2CellMap,key)
% AUTHOR:	Abel Szkalisity
% DATE: 	October 30, 2018
% NAME: 	jump2CellMap_get
% 
% Used for sub-cluster creator.
% A function to overcome the compatibility issues with the jump2CellStructs
% stored in the jump2CellMap-s. It regenerates the structures with the
% proper file separators for the running OS.
% THIS FUNCTION MUST BE USED FOR ACCESSING DATA FROM THE jump2CellMap!
%
% INPUTS:
%   jump2CellMap    The map object storing the jump2cell structures
%   key             The imageID of the clustered cell (this imageID is the name of the image in the cluster)
%
% OUTPUTS:
%   value           The proper jumpToCell structure.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
%The script needs ACC to run and the proper project loaded in so that itf
%can jump to the correct cells.

if jump2CellMap.isKey(key)
    value = jump2CellMap(key);

    splittedImgName = strsplit(value.ImageName,{'\','/'});
    splittedOrigImgName = strsplit(value.OriginalImageName,{'\','/'});
    value.ImageName = fullfile(splittedImgName{:});
    value.OriginalImageName = fullfile(splittedOrigImgName{:});

end


end