%perform clustering
%Needs ACC running and 
%       featuresMatrixNormalized
%       jump2CellStructs
%in the workspace

%include here the clustering also
global CommonHandles;

dataTypeList = { 'directSpace',...
    'PCAreducedSpace'};
[dataTypeIdx,ok] = listdlg('ListString',dataTypeList,'PromptString','Select data type');
if ~ok, return; end
dataType = dataTypeList{dataTypeIdx};

clusterModules = listAvailableClusterModules();

[selection,ok] = listdlg('PromptString','Please select a clustering algorithm',...
    'ListString',clusterModules,...
    'SelectionMode','single');

if ~ok, return; end

clusterM = eval([clusterModules{selection} '();']);
clusterParamArray = clusterM.getParameters;
try
    paramcell = Settings_GUI(clusterParamArray,'infoText',['Parameters for ' clusterModules{selection} ' clustering.']);
catch e
    warndlg('Please specify parameters!');
    return;
end

clusterM.fillParams(paramcell);

targetDir = fullfile(CommonHandles.DirName,...
    clusterFolderName,...
    'ClusterStore',...    
    [timeString(5) '_jump2CellStructs_C' num2str(classToCollect,'%02d')]);
if ~exist(targetDir,'dir'), mkdir(targetDir); end

%writes all neccesssary data to file
createClusterMetaData;

if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {'Clustering...'}; end

switch dataType
    case 'directSpace'
        [jump2CellStructs,j2CFiltered,maxClustID] = clusterM.clusterCells( featuresMatrixNormalized, jump2CellStructs);
    case 'PCAreducedSpace'
        [jump2CellStructs,j2CFiltered,maxClustID] = clusterM.clusterCells( pcaReduced, jump2CellStructs);
end

targetFile = fullfile(targetDir,'jump2CellMap.mat');
save(targetFile,'jump2CellStructs','j2CFiltered');

%Copy the neccessary images to folders
[answerACCViewer] = questdlg('Do you want to create small thumbnail images for the most representative cells? (required for viewing in ACC)','Cluster thumbnails for representative cells','Yes','No','Yes');
if strcmp(answerACCViewer,'Yes')
    jump2CellMap = sortCellsToFolders(targetDir,j2CFiltered);
else
    jump2CellMap = [];    
end

[answer] = questdlg('Do you want to create csv files with cluster information for each cell?','Cluster csv for all cells','Yes','No','Yes');
if strcmp(answer,'Yes')
    [sortByFilesMap] = sortCellsByFileName(jump2CellStructs);    
    writeAllClustersToFiles(targetDir,sortByFilesMap,maxClustID);
else
    sortByFilesMap = [];
end

if ishandle(h), pause(0.1); h.get('CurrentAxes').Children.String = {'Saving variables...'}; end

save(targetFile,'-v7.3','jump2CellMap','jump2CellStructs','j2CFiltered','sortByFilesMap');

if ishandle(h), close(h); end
