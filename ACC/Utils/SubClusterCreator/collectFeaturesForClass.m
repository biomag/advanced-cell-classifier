function [featuresInClasses,jump2CellStructs,classString,featurePath,featureFile] = collectFeaturesForClass(dataT,classesToCollect)
% AUTHOR:	Abel Szkalisity
% DATE: 	October 5, 2018
% NAME: 	collectFeaturesForClass
% 
% Collects features for the selected class and saves it down to the cluster
% main folder so that it can be reused later on.
%
% INPUTS:
%   dataT       A table that contains the single cell classification
%               prediction data. See what is the output of the PredictPlate
%               if single cell is checked.
%   classToCollect An index which specifies which class's features needs to
%               be loaded
%
% OUTPUTS:
%   featuresInClass: A cellarray containing row vectors with the features
%               of the cells predicted to the selected class
%   jump2CellStructs: Structures for each sample that was predicted to the
%               selectedClass. These can be used as input for jumpToCell.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
%The script needs ACC to run and the proper project loaded in so that itf
%can jump to the correct cells.

global CommonHandles;

nofClasses = length(classesToCollect);
allFeatures = cell(1,nofClasses);
allStructs = cell(1,nofClasses);
fullStr = [];
for iii=1:nofClasses
    classToCollect = classesToCollect(iii);    
    classIndices = find(dataT.Class == classToCollect);
    classString = CommonHandles.Classes{classToCollect}.Name;    
    NinClass = length(classIndices);

    %Data collected
    featuresInClass = cell(NinClass,1);
    jump2CellStructs = cell(NinClass,1);    

    h = waitbar(0,sprintf('Collecting features for class %s',classString));

    for i=1:NinClass
        str.CellNumber = dataT.ObjectNumber(classIndices(i));
        plateName = dataT.PlateName{classIndices(i)};
        str.ImageName = fullfile(plateName, CommonHandles.ImageFolder, [dataT.ImageName{classIndices(i)} CommonHandles.ImageExtension]);
        str.OriginalImageName = fullfile(plateName, CommonHandles.OriginalImageFolder, [dataT.ImageName{classIndices(i)} CommonHandles.ImageExtension]);
        str.PlateName = getPlateNumberByName(plateName);
        jump2CellStructs{i} = str;
        jumpToCell(str,0);

        featuresInClass{i} = CommonHandles.SelectedMetaData(str.CellNumber,:);            

        waitbar(i/NinClass,h,sprintf('Collecting features for class %s\n Cells done: %d/%d',classString,i,NinClass));
    end

    msgbox(sprintf(...
        'Feature collection has completed!\n Please specify a file name for the features for the prediction of this class.\nWe recommend to add the name of the machine learning model to this filename.\nYou can use this file later on to create different clusterings for this class.'...
        ));

    [featureFile,featurePath] = uiputfile('.mat','Specify a location and filename for these features',...
        fullfile(CommonHandles.DirName,['ClassInfo_C' num2str(classToCollect,'%02d')]));

    
    if ~isnumeric(featureFile), save(fullfile(featurePath,featureFile),'-v7.3','featuresInClass','jump2CellStructs','classToCollect','classString'); end
    if ishandle(h), close(h), end
    fullStr = [fullStr '-' classString];
    allFeatures = featuresInClass;
    allStructs = jump2CellStructs;
end

classString = fullStr;
featuresInClasses = vertcat(allFeatures{:});
jump2CellStructs = vertcat(allStructs{:});
