%prepareForClustering after collectFeaturesForClass script

featuresMatrix = cell2mat(featuresInClass);
%rows are the observations

%Select features
featureList = getFeatureList();

%Remove those data entries which have Inf or Nan in them
idxToDelete = union(find(any(isnan(featuresMatrix),2)),find(any(isinf(featuresMatrix),2)));
featuresMatrix(idxToDelete,:) = [];
jump2CellStructs(idxToDelete) = [];

if ~isempty(featureList)
    [featureListIdx,ok] = listdlg('ListString',featureList,'PromptString','Select features to be used for clustering');
end

if ok
    featuresMatrix = featuresMatrix(:,featureListIdx);
else
    featureListIdx = 1:size(featuresMatrix,2);
end

featuresMatrixCentered = featuresMatrix - repmat(mean(featuresMatrix),size(featuresMatrix,1),1);
featuresMatrixNormalized = featuresMatrixCentered./repmat(std(featuresMatrix),size(featuresMatrix,1),1);

[pcaCoeff,pcaScores,latent] = pca(featuresMatrixNormalized);

latentPercent = latent./sum(latent);
summa = 0; i = 1;
while summa<0.99
    summa = summa + latentPercent(i);
    i = i+1;
end

pcaReduced = pcaScores(:,1:i-1);