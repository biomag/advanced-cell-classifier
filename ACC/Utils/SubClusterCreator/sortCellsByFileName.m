function [sortByFilesMap] = sortCellsByFileName(jump2CellStructs)
%sortCellsByFileName
%   sortByFiles is another map which key is the imageName. The value is a
%   cellarray of all jump2Cell structs connected to that image

sortByFilesMap = containers.Map();

infoStr = 'Sorting cells by filename...';    
h = waitbar(0,infoStr);

for i=1:numel(jump2CellStructs)                        
    if ishandle(h), waitbar(i/numel(jump2CellStructs),h,sprintf([infoStr '%d/%d'],i,numel(jump2CellStructs))); end
        
    str = jump2CellStructs{i};
    [~,fileExEx,~] = fileparts(str.ImageName);
    if isKey(sortByFilesMap,fileExEx)
        sortByFilesMap(fileExEx) = [sortByFilesMap(fileExEx) {str}];
    else
        sortByFilesMap(fileExEx) = {str};
    end

end

if ishandle(h), close(h);  
end

