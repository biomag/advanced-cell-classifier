function [arrayout,arrayres] = portion( arrayin, n, index )
% AUTHOR:   Abel Szkalisity
% DATE:     April 26, 2016
% NAME:     portion
%
% It gives back a 'negative' slice of the arrayin in arrayout.
% It is negative, because we just leave out one slice of the whole
% arrayin. n is the number of slices we made from arrayin, and index is
% the place of the gap. It is designed in the way that if you call it
% with every index from 1:n then the gaps are nearly a classification
% (there might be empty portions) of the arrayin elements.
% 
% OUTPUT:
%   out             a 1D array with the predicted classes. It has as many
%                   rows as the features.
% OUTPUT:
%   arrayout        the negative slice of the arrayin
%   arrayres        the 'positive' slice, the residual, the part which we
%                   left out from the arrayout.
%   !all are ROW vectors!
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

arrayin = convert2RowVector(arrayin);

nofElements = size(arrayin,2);

%The bottom border of the gap.
if floor((index-1)/n*nofElements)==(index-1)/n*nofElements && index-1~=0
    gapDown = floor((index-1)/n*nofElements)-1;
else
    gapDown = floor((index-1)/n*nofElements);
end

%The top border of the gap.
gapTop = ceil((index)/n*nofElements);

if index==n
    arrayout = arrayin([1:gapDown gapTop+1:end]);
    arrayres = arrayin(gapDown+1:gapTop);
else
    arrayout = arrayin([1:gapDown gapTop:end]);
    arrayres = arrayin(gapDown+1:gapTop-1);
end

end

