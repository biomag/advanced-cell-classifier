function refreshTrSetMetaData(oldFolderName, newFolderName)

global CommonHandles
trSet = CommonHandles.TrainingSet;
n = length(trSet.CellNumber);

for i = 1:n
    fileName = trSet.MetaDataFilename{i};
    splitedName = strsplit(fileName,oldFolderName);
    newFileName = fullfile(splitedName{1}, newFolderName, splitedName{2});
    fVector = readmatrix(fullfile(CommonHandles.DirName,newFileName));
    trSet.Features{i, 1} = fVector(trSet.CellNumber{i},3:end);
    trSet.MetaDataFilename{i} = newFileName;
end

CommonHandles.TrainingSet = trSet;