function physicalExperiment = parsePhysicalExperiment(xmlFileText, markerImageDescriptorXMLFileName, markerImageFileName)
% AUTHOR:   Csaba Molnar
% DATE:     July 29, 2016
% NAME:     parsePhysicalExperiment
%
% This function creates the structure that describes the physical experiment
%
% INPUT:
%   xmlFileText                         XML text of the main screen
%   markerImageDescriptorXMLFileName    full path of XML file of the marker
%                                       image
%   markerImageFileName                 full path of the image containing
%                                       the marker
% OUTPUT:
%   physicalExperiment                  struct containing screen attributes
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

[markerFromXOffset, markerFromYOffset] = registerMarker(markerImageDescriptorXMLFileName, markerImageFileName);

imageResolutionX = str2num(getXMLValue(xmlFileText, 'ImageResolutionX'));
imageResolutionY = str2num(getXMLValue(xmlFileText, 'ImageResolutionY'));
imageSizeX = str2num(getXMLValue(xmlFileText, 'ImageSizeX'));
imageSizeY = str2num(getXMLValue(xmlFileText, 'ImageSizeY'));
creationTimeStamp = getXMLValue(xmlFileText, 'MeasurementStartTime');
if ~isempty(creationTimeStamp)
    creationTimeArray = sscanf(creationTimeStamp, '%d-%d-%dT%d:%d:%f+%d:%d');
    creationTimeStamp = sprintf('%04d-%02d-%02dT%02d:%02d:%06.3f+%02d:%02d', creationTimeArray);
else
    creationTimeArray = clock;
    creationTimeStamp = sprintf('%04d-%02d-%02dT%02d:%02d:%06.3f+%02d:%02d', [creationTimeArray 0 0]);
end
physicalExperimentName = getXMLValue(xmlFileText, 'PlateID');
if isempty(physicalExperimentName)
    physicalExperimentName = CommonHandles.PlatesNames{1};
end
physicalExperimentOriginalID = getXMLValue(xmlFileText, 'MeasurementID');
if isempty(physicalExperimentOriginalID)
    physicalExperimentOriginalID = [physicalExperimentName '_' creationTimeStamp];
end

peDescription = '';

physicalExperiment = struct('experimentIDOrignal',physicalExperimentOriginalID,... % from XML
    'experimentName',physicalExperimentName,... % from XML
    'url',physicalExperimentName,...
    'contourDir',CommonHandles.ImageFolder,... % from ACC CH
    'metaDataDir',CommonHandles.MetaDataFolder,... % from ACC CH
    'colourImageDir',CommonHandles.OriginalImageFolder,... % from ACC CH
    'nucleiDir',CommonHandles.CAMI.PhysicalExperiment.NucleiDir,... % from ACC input
    'cellDir',CommonHandles.CAMI.PhysicalExperiment.CellsDir,... % from ACC input
    'description',peDescription,... % from ACC input
    'creationTimeStamp',creationTimeStamp,... % millisecond digits should be shortened from original, from XML
    'pixelSizeX',imageResolutionX,... % from XML
    'pixelSizeY',imageResolutionY,... % from XML
    'imageSizeX',imageSizeX,... % from XML
    'imageSizeY',imageSizeY,... % from XML
    'markerXOffset',markerFromXOffset,... % from XML
    'markerYOffset',markerFromYOffset); % from XML
