function operettaName = convertSSS2OperettaName(SSSName)
% AUTHOR:   Peter Horvath
% DATE:     January 1, 2015
% NAME:     convertSSS2OperettaName
%
% This function converts ACC image name format (SSS) to PerkinElmer
% Operetta image name
%
% INPUT:
%   SSSName         image name in ACC format (SSS)
%
% OUTPUT:
%   operettaName    image name in Operetta format
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

wellNamePos = strfind(SSSName, '_w');
wellName = SSSName(wellNamePos+2:wellNamePos+4);
r = wellName(1) - 'A' + 1;
c = str2num(wellName(2:3));

fieldPos = strfind(SSSName, '_f');
fieldSuccPos = strfind(SSSName, '_z');
field = str2num(SSSName(fieldPos + 2:fieldSuccPos-1));

operettaName = sprintf('r%0.2dc%0.2df%0.2dp%0.2d', r, c, field, 1);




