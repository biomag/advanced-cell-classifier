function value = getXMLValue(text, tag)
% AUTHOR:   Peter Horvath
% DATE:     January 1, 2015
% NAME:     getXMLValue
%
% This function gets the value of the first occurrence of XML tag
%
% INPUT:
%   text        XML text
%   tag         XML tag
%
% OUTPUT:
%   value      text value of XML tag
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


openTagPos = strfind(text, ['<' tag]);
if ~isempty(openTagPos)
    valueStart = strfind(text(openTagPos(1):end), '>') + openTagPos(1);
    valueStop = strfind(text(openTagPos(1):end), ['</' tag]) + openTagPos(1);
    
    value = text(valueStart(1): valueStop(1)-2);
else
    value = '';
end
