function [ cellImage ] = getCurrentCellImage( ImageName, Coordinates)
%GETCURRENTCELL Gives data to upload a cell

%cutSize = 50;

global CommonHandles;
cutSize = CommonHandles.CutSize;

OriginalImage = imread([CommonHandles.DirName filesep ImageName]);

% [imPath,fileNameexEx,~] = fileparts(ImageName);
% [~,metaData] = readMetaData(CommonHandles,fileNameexEx,currentPlateNumber);

% boundaries of the part of original image containing current cell for small view
if Coordinates(1) < cutSize+1
    cutx1=1;
else
    cutx1= floor(Coordinates(1)-cutSize);
end

if Coordinates(2) < cutSize+1
    cuty1=1;
else
    cuty1= floor(Coordinates(2)-cutSize);
end

if Coordinates(1) > (CommonHandles.CurrentImageSizeY -cutSize)
    cutx2=CommonHandles.CurrentImageSizeY;
else
    cutx2= floor(Coordinates(1)+cutSize);
end

if Coordinates(2) > (CommonHandles.CurrentImageSizeX-cutSize)
    cuty2=CommonHandles.CurrentImageSizeX;
else
    cuty2= floor(Coordinates(2)+cutSize);
end


cellImage = OriginalImage(cuty1:cuty2, cutx1:cutx2, :);

end

