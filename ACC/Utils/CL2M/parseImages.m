function [imagesMetadata, imageFileList] = parseImages(xmlFileText,generatedPEID)
% AUTHOR:   Csaba Molnar
% DATE:     August 1, 2016
% NAME:     parseImages
%
% This function creates the structures of metadata that describes the
%                       images of the screen
%
% INPUT:
%   xmlFileText         Path of the descriptor file belonging to the screen
%   generatedPEID       generated database ID of the uploaded screen
%
% OUTPUT:
%   imagesMetadata      array of structs containing image object metadata
%   imageFileList       full path of images to upload in cell array
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

imExt = CommonHandles.ImageExtension;
imagesList = dir(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1}, CommonHandles.ImageFolder, ['*' imExt])); % extension should be read from CommonHandles

numOfImages = length(imagesList);
imagesMetadata(numOfImages) = struct('idInPlate',[],...
    'imageFileName',[],...
    'xPos',[],...
    'yPos',[],...
    'plateName',[],...
    'physicalExperimentID',[]);

physicalExperimentName = getXMLValue(xmlFileText, 'PlateID');
if isempty(physicalExperimentName)
    physicalExperimentName = CommonHandles.PlatesNames{1};
end

for i=1:numOfImages
    imageName = imagesList(i).name;
    
    operettaImageName =  convertSSS2OperettaName(imageName);
    currentImagePos = strfind(xmlFileText, operettaImageName);
    if ~isempty(currentImagePos)
        currentImagePosEnd = strfind(xmlFileText(currentImagePos(1):end), '</Image>');
        if ~isempty(currentImagePosEnd)
            currentImageText = xmlFileText(currentImagePos(1):currentImagePos(1) + currentImagePosEnd(1));
        else
            currentImageText = '';
        end
    else
        currentImageText = '';
    end
    
    currentImageCenterX = str2num(getXMLValue(currentImageText, 'PositionX'));
    currentImageCenterY = str2num(getXMLValue(currentImageText, 'PositionY'));
    fieldID = str2num(getXMLValue(currentImageText, 'FieldID'));
    if isempty(fieldID)
        fieldID = str2num(imageName(strfind(imageName,'_f')+2:strfind(imageName,'_z')-1));
    end
    
    imagesMetadata(i).idInPlate = fieldID;
    imagesMetadata(i).imageFileName = imageName;
    imagesMetadata(i).xPos = currentImageCenterX;
    imagesMetadata(i).yPos = currentImageCenterY;
    imagesMetadata(i).plateName = physicalExperimentName;
    imagesMetadata(i).physicalExperimentID = generatedPEID;
    
end

imageFileList = cell(numOfImages,1);

for i=1:numOfImages
    imageFileList{i} = fullfile(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1}, CommonHandles.OriginalImageFolder, imagesList(i).name));
end

