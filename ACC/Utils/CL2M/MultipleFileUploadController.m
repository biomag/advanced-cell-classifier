classdef MultipleFileUploadController < handle
% AUTHOR:   Csaba Molnar
% DATE:     July 29, 2016
% NAME:     MultipleFileUploadController
%
% Implementation of the controller class to upload images.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    properties (Access = private)
        hostURL
        httpClient
        postMethod
        parts
        fileList
        parameters
    end
    
    methods (Access = public)
        % Constructor
        % INPUT:
        %   URL             URL path of upload service
        %   fileListName    parameter of files during upload request
        %                   (default on CAMIO server is 'imageFiles')
        %   fileList        cell array of full path of images to upload
        %   parameters      cell array of parameter name and parameter
        %                   value pairs consequtively for upload request
        %                   (imageMetadata parameter is required for images
        %                   of the screen)
        function obj = MultipleFileUploadController(URL, fileListName, fileList, parameters)
            
            obj.hostURL = URL;
            obj.fileList = fileList;
            obj.parameters = parameters;
            
            obj.httpClient = org.apache.commons.httpclient.HttpClient;
            obj.postMethod = org.apache.commons.httpclient.methods.PostMethod(URL);
            
            sparts = javaArray('org.apache.commons.httpclient.methods.multipart.StringPart',length(parameters)/2);
            for i=1:2:length(parameters)/2
                sparts(i) = org.apache.commons.httpclient.methods.multipart.StringPart(parameters(i),parameters(i+1));
            end
            fparts = javaArray('org.apache.commons.httpclient.methods.multipart.FilePart',length(fileList));
            for i=1:length(fileList)
                fparts(i) = org.apache.commons.httpclient.methods.multipart.FilePart(fileListName,java.io.File(fileList(i)));
            end
            
            obj.parts = [sparts fparts];
            clear sparts;
            clear fparts;
        end
        
        %Function   post request method containing image files and
        %           corresponding metadata
        function status = postAll(self)
            try
                self.postMethod.setRequestEntity(org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity(self.parts,self.postMethod.getParams));
                status = self.httpClient.executeMethod(self.postMethod);
            catch e
                fprintf('%s\n', e.identifier);
            end
            
        end
        
        function postMethod = getPostMethod(self)
            postMethod = self.postMethod;
        end
        
        function httpClient = getHttpClient(self)
            httpClient = self.httpClient;
        end
               
        function fileList = getFileList(self)
            fileList = self.fileList;
        end
    end
    
end

