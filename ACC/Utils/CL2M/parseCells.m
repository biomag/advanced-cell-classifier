function cells = parseCells(cellMapping, uploadStructure, generatedImageIndices, generatedLEID)
% AUTHOR:   Csaba Molnar
% DATE:     August 1, 2016
% NAME:     parseCells
%
% This function creates the structures of metadata that describes the
%                       images of the screen
%
% INPUT:
%   cellMapping             structure array containing information from selection
%                           phase
%   uploadStructure         structure array containing information about most
%                           likely cells of a class
%   generatedImageIndices   array of generated indices of images of the
%                           screen; the images were uploaded in
%                           alphabetical order
%   generatedLEID           generated database id of logical experiment
%
% OUTPUT:
%   cells                   array of structs containing cell objects
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


global CommonHandles;

imagesList = dir(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1}, CommonHandles.ImageFolder, ['*' CommonHandles.ImageExtension]));

cols = CommonHandles.ClassImageInfo.Cols;

cellMappingStruct = cat(1,cellMapping{:});
numOfCells = numel(find(cat(1,cellMappingStruct.Selected)));

imageNames = {imagesList.name};

cells(numOfCells) = struct( 'idInImage','',...
    'commentOnUpload', '',...
    'commentOnCut', '',...
    'commentOther', '',...
    'status', 0,...
    'xPos', 0,... % from ACC
    'yPos', 0,... % from ACC
    'contourString', '',...
    'thumbnailString', '',...
    'imageID', 0,...
    'logicalExperimentID', 0);

numimgs = length(uploadStructure.Img);

j=0;
x=1;
y=1;

for i = 1:numimgs
    if x>cols
        x=1;
        y=y+1;
    end
    
    if cellMapping{y,x}.Selected
        j = j+1;
        
        [~,imageBaseName,ext] = fileparts(uploadStructure.ImageName{i});
        imageName = [imageBaseName ext];
        
        idx = find( cellfun( @(x) strcmp(x, imageName), imageNames) );
        imageID = generatedImageIndices(idx);
        
        %%% collecting contour points (in byte array to store it in blob object)
        metaDataFileList = dir(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, [imageBaseName '*'])); % extension should be read from CommonHandles
        
        [~,imBaseName,~] = fileparts(metaDataFileList(1).name);
        try
            pixContours = [];
            labeledCellsImage = [];
            labeledCellsImageList = dir(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1}, CommonHandles.CAMI.PhysicalExperiment.(CommonHandles.CAMI.PhysicalExperiment.SelectedUploadDir), [imBaseName '*']));
            numberOfLayers = length(labeledCellsImageList);
            for l=1:numberOfLayers
                labeledCellsImage(:,:,l) = imread(fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{1}, CommonHandles.CAMI.PhysicalExperiment.(CommonHandles.CAMI.PhysicalExperiment.SelectedUploadDir), labeledCellsImageList(l).name));
            end
            labeledCellsImage = labeledCellsImage == uploadStructure.SelectedCell{i};
            emptyLayerIdx = [];
            for l=1:length(labeledCellsImageList)
                if max(reshape(labeledCellsImage(:,:,l),1,[]))==0
                    emptyLayerIdx = [emptyLayerIdx, l]; 
                end
            end
            labeledCellsImage(:,:,emptyLayerIdx) = [];
            currentCell = labeledCellsImage;
            
            % 1 by n row containing the contour points in pixel coordinates
            pixContoursTemp = reshape(uint16(cell2mat(bwboundaries(currentCell)))',[],1)';
            % change x and y coordinates
            pixContours(1:2:length(pixContoursTemp)) = pixContoursTemp(2:2:length(pixContoursTemp));
            pixContours(2:2:length(pixContoursTemp)) = pixContoursTemp(1:2:length(pixContoursTemp));
            % hex string containing the contour points
            contourData = reshape(cat(2, dec2hex(pixContours,4))',[],1)';           
        catch
            contourData = '';
        end
        
        outTempForThumbDir = tempdir;
        thumbName = sprintf('img_%s_cellNum_%04d%s', imageNames{idx}, uploadStructure.SelectedCell{i}, ext);
        imwrite(uploadStructure.Img{i},fullfile(outTempForThumbDir,thumbName));
        fid = fopen(fullfile(outTempForThumbDir,thumbName));
        thumbData = fread(fid);
        fclose(fid);
        delete(fullfile(outTempForThumbDir,thumbName));
        
        hexThumbData = reshape(dec2hex(thumbData,2)',1,[]);
        
        cells(j) = struct( 'idInImage',uploadStructure.SelectedCell{i},...
            'commentOnUpload', cellMapping{y,x}.CommentOnUpload,...
            'commentOnCut', '',...
            'commentOther', '',...
            'status', 1,...
            'xPos', single(uploadStructure.Coord{i}(1)),... %first in the structure
            'yPos', single(uploadStructure.Coord{i}(2)),... %first in the structure
            'contourString', ['0x' contourData],...
            'thumbnailString', hexThumbData,...
            'imageID', imageID,...
            'logicalExperimentID', generatedLEID);
    end
    
    x = x+1;
end

end