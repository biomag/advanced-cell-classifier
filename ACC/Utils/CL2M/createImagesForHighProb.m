function [outimg, mapping] = createImagesForHighProb(uploadStructure)
% AUTHOR:   Csaba Molnar
% DATE:     May 15, 2016
% NAME:     createImagesForHighProb
%
% This function gets the value of the first occurrence of XML tag
%
% INPUT:
%   uploadStructure        cell array of structs with fields 
%    'Img': image of the cell with a small surrounding (it will be resized
%    into a fixed size defined in CommonHandles.ClassImageInfo.ImageSize
%    'FeatureVector': feature vector of cell
%    'ImageName': name of the image of the cell
%    'SelectedCell': (number of cell in image)
%    'PlateName': number of plate in the platelist
%    'Coord': coordinates in pixels
%
% OUTPUT:
%   value      text value of XML tag
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    imgsize = CommonHandles.ClassImageInfo.ImageSize;
    sepsize = CommonHandles.ClassImageInfo.SepSize;
    cols = CommonHandles.ClassImageInfo.Cols;
    width = cols * imgsize(1) + (cols+1) * sepsize;
    
    numimgs = length(uploadStructure.Img);
    rows = ceil(numimgs / cols);
    height = rows * imgsize(2) + (rows+1) * sepsize;
    outimg = uint8(zeros(height,width,3));
    outimg(:,:,:) = 255;
    
    mapping = {};
    x = 1;
    y = 1;
    for i = 1:numimgs
        classImg = uploadStructure.Img{i};
        resImg = imresize(classImg, imgsize);
        if x > cols
           x = 1;
           y = y + 1;
        end
        offsetw = imgsize(1) * (x-1) + sepsize * x;
        offseth = imgsize(2) * (y-1) + sepsize * y;
        outimg(offseth:offseth+imgsize(2)-1,offsetw:offsetw+imgsize(1)-1,:) = resImg;
        subimgmeta.CellNumber = uploadStructure.SelectedCell{i};
        subimgmeta.ImageName = uploadStructure.ImageName{i};
        subimgmeta.PlateName = uploadStructure.PlateName{i};
        subimgmeta.Coords = uploadStructure.Coord{i};
        subimgmeta.Selected = 1;
        subimgmeta.CommentOnUpload = '';
        mapping{y,x} = subimgmeta;
        
        x = x + 1;
    end
end