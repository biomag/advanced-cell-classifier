function [ message ] = uploadExperiment( imgMapping, uploadStructure, classID )
% AUTHOR:   Csaba Molnar
% DATE:     July 29, 2016
% NAME:     uploadExperiment
%
% This function creates the structures and send them to the application
% server.
%
% INPUT:
%   imgMapping      structure array containing information from selection
%                   phase
%   uploadStructure structure array containing information about most
%                   likely cells of a class
%   classID         ID of the class of selected cells
%
% OUTPUT:
%   message         message about success of upload
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

hostBaseName = ['http://' CommonHandles.CAMI.Network.Host ':' CommonHandles.CAMI.Network.Port '/' CommonHandles.CAMI.Network.Path];

lePrompt = {'Enter logical experiment name','Enter description of the logical experiment'};
defAnswer = {CommonHandles.PlatesNames{1},sprintf('%s cells from experiment %s', CommonHandles.Classes{classID}.Name,CommonHandles.PlatesNames{1})};
leDlgAnswer = inputdlg(lePrompt, 'Experiment details', [1; 3], defAnswer);

if isempty(leDlgAnswer)    
    message = 'Upload canceled by user!';
elseif isempty(leDlgAnswer{1})
    errordlg('Experiment name cannot be empty!');
    message = 'No logical experiment name was given!';
else
    missingMetadata = 0;
    mainDescriptorXML = fullfile(CommonHandles.CAMI.PhysicalExperiment.DescriptorXML);
    [path,~,~] = fileparts(mainDescriptorXML);
    if isempty(path)
        mainDescriptorXML = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, mainDescriptorXML);
    end
    
    if exist(mainDescriptorXML,'file')
        xmlFileText = fileread(mainDescriptorXML);
    else
        xmlFileText = '';
        missingMetadata = 1;
    end
    
    markerXML = CommonHandles.CAMI.PhysicalExperiment.MarkerXML;
    [path, ~, ~] = fileparts(markerXML);
    if isempty(path)
        markerXML = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, markerXML);
    end
    if ~exist( markerXML, 'file' )
        markerXML = [];
        missingMetadata = 1;
    end
    
    markerImage = CommonHandles.CAMI.PhysicalExperiment.MarkerImage;
    [path, ~, ~] = fileparts(markerImage);
    if isempty(path)
        markerImage = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, markerImage);
    end
    if ~exist( markerImage, 'file' )
        markerImage = [];
        missingMetadata = 1;
    end
    
    if missingMetadata
        choice = questdlg('Not all descriptor files are available for a complete upload! Do you want to continue the process?','Missing data','Yes','No','No');
        switch choice
            case 'No'
                return;
        end
    end
    
    physicalExperiment = parsePhysicalExperiment(xmlFileText,markerXML,markerImage);
    physicalExperimentJSON = savejson('',physicalExperiment);
    
    peUploadURL = [hostBaseName '/resources/screens/upload/'];
    
    peAnswer = urlread(peUploadURL,'POST',...
        {'physicalExperiment',physicalExperimentJSON});
    
    if ~isempty(strfind(peAnswer, 'Existing physical experiment!'))
        
        fprintf('Physical experiment is already uploaded: %s\n', physicalExperiment.experimentIDOrignal);
        generatedPEID = int16(str2double(peAnswer(length('Existing physical experiment! ID: '):end)));
        
    elseif strcmp('Successful insert! ID: ',peAnswer(1:length('Successful insert! ID: ')))
        
        fprintf('Physical experiment is inserted successfully: %s\n', physicalExperiment.experimentIDOrignal);
        generatedPEID = int16(str2double(peAnswer(length('Successful insert! ID: '):end)));
        [imagesMetadata, imageFileList] = parseImages(xmlFileText,generatedPEID);
        
        imagesJSON = savejson('',imagesMetadata);
        
        parameters = {'imagesMetadata',imagesJSON};
        
        imagesUploadURL = [hostBaseName '/resources/images/upload/'];
        
        mfuc = MultipleFileUploadController(imagesUploadURL, 'imageFiles', imageFileList, parameters);
        status = mfuc.postAll;
        
        if status == 200
            fprintf('Post request for images finished successfully!\n');
        elseif status == 500
            fprintf('Server error occured, STATUS: %d\n',status);
        else
            fprintf('Unknown server error occured, STATUS: %d\n',status);
        end
        
    end
    
    logicalExperiment = parseLogicalExperiment(leDlgAnswer{1},leDlgAnswer{2},generatedPEID);
    logicalExperimentJSON = savejson('',logicalExperiment);
    
    leUploadURL = [hostBaseName '/resources/experiments/upload/'];
    logicalExperimentAnswer = urlread(leUploadURL,...
        'POST',{'logicalExperiment',logicalExperimentJSON});
    
    imGETURL = [hostBaseName  '/resources/images/getimagesforscreen'];
    imAnswer = urlread(imGETURL,...
        'POST',{'physicalExperimentID',num2str(generatedPEID)});
    
    generatedImageIndices = loadjson(imAnswer);
    generatedLEID = int16(str2double(logicalExperimentAnswer(length('Successful insert! ID: '):end)));
    
    cells = parseCells(imgMapping, uploadStructure, generatedImageIndices, generatedLEID);
    
    cellUploadURL = [hostBaseName '/resources/cells/uploadonecell/'];
    
    for i=1:length(cells)
        cellsJSON = savejson('',cells(i));
        urlread(cellUploadURL,'POST',{'cell',cellsJSON});
    end
    
    message = 'Upload was succesful!';
    uiwait(msgbox(['You uploaded successfully ' num2str(length(cells)) ' cells to CAMIO!']));
    if ishandle(CommonHandles.ShowHighProbCellsHandle)
        close(CommonHandles.ShowHighProbCellsHandle);
    end    
    if ishandle(CommonHandles.UploadCAMIOFigure)
        close(CommonHandles.UploadCAMIOFigure);
    end
end

end

