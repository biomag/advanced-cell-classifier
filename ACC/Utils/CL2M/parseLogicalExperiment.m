function logicalExperiment = parseLogicalExperiment(leExperimentName,leDescription,generatedPEID)
% AUTHOR:   Csaba Molnar
% DATE:     August 1, 2016
% NAME:     parseLogicalExperiment
%
% This function creates the structur that describes the physical experiment
%
% INPUT:
%   leExperimentName        logical experiment name set by user
%   leDescription           description of logical experiment
%   generatedPEID           generated database ID of the uploaded screen
%
% OUTPUT:
%   logicalExperiment       struct containing metadata of logical
%                           experiment
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

logicalExperiment = struct('experimentName',leExperimentName,...
                           'physicalExperimentID',generatedPEID,...
                           'description',leDescription);

