function [markerFromXOffset, markerFromYOffset] = registerMarker(markerImageDescriptorXMLFileName, markerImageFileName)
% AUTHOR:   Csaba Molnar
% DATE:     July 28, 2016
% NAME:     registerMarker
%
% This function registers the theoretic marker with the brightfield image
% of the burned marker and returns with the metrical coordinates of the
% center of the marker
%
% INPUT:
%   markerImageDescriptorXMLFileName        Path of the descriptor file
%                                           belonging to the marker image
%   markerImageFileName                     Path of the marker image file
%
% OUTPUT:
%   markerFromXOffset, markerFromYOffset    Metrical coordinates of the
%                                           marker in the screening
%                                           microscopy coordinate system
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% TODO maybe parse the marker size from the Zeiss contour file of the
% marker
markerMetricalSizeX = 50*10^-6; % m
markerMetricalSizeY = 50*10^-6; % m

try
    markerXMLText = fileread(markerImageDescriptorXMLFileName);
    markerImg = imread(markerImageFileName);
    
    [~, imageBaseName, ~] = fileparts(markerImageFileName);
    imagePos = strfind(markerXMLText, imageBaseName);
    markerXMLText = markerXMLText(imagePos:end);
    
    [H, W, ~] = size(markerImg);
    
    pixelSizeX = str2double(getXMLValue(markerXMLText,'ImageResolutionX'));
    pixelSizeY = str2double(getXMLValue(markerXMLText,'ImageResolutionY'));
    
    markerImgOffsetX = str2double(getXMLValue(markerXMLText,'PositionX'));
    markerImgOffsetY = str2double(getXMLValue(markerXMLText,'PositionY'));
    
    markerPixSizeX = int16(markerMetricalSizeX/pixelSizeX);
    markerPixSizeY = int16(markerMetricalSizeY/pixelSizeY);
    
    % marker is the theoretical template that is burned into the surface of
    % the slide:
    %
    %        *
    %        *
    %        *
    %     *******
    %      * *
    %       **
    %        *
    
    [xgrid,ygrid] = meshgrid(1:markerPixSizeX,1:markerPixSizeY);
    xgrid = int16(xgrid);
    ygrid = int16(ygrid);
    
    diagpart = imdilate((xgrid+int16(markerPixSizeX/2)==ygrid), strel('disk',1,4));
    
    marker = uint16(zeros(markerPixSizeX,markerPixSizeY)+1);
    marker(diagpart) = 0;
    marker(end/2-1:end/2+1,:) = 0;
    marker(:,end/2-1:end/2+1) = 0;
    
    corrMap = normxcorr2(marker, im2double(markerImg)); % size of corrMap is bigger than the original image, and it is extended by the size of the template
    [markerPixRow,markerPixCol] = find(corrMap==max(corrMap(:)));
    markerPixRow = double(markerPixRow-markerPixSizeY/2);
    markerPixCol = double(markerPixCol-markerPixSizeX/2);
    
    % validation of registration
    registrationFigureHandle = figure;
    set(registrationFigureHandle, 'Name', 'Position of detected marker');
    imshow(markerImg,[]);
    hold on;
    scatter(markerPixCol, markerPixRow, markerPixSizeX*markerPixSizeY, [1 1 0], 'Marker', '+');
    plot(markerPixCol, markerPixRow, 'ro');
    choice = questdlg('Is the marker detected in the right position?', ...
        'Check marker','Yes', 'Set it manually','Set it manually');
    while ~strcmp(choice,'Yes')
    
            hold off;
            imshow(markerImg,[]);
            hold on;
            [markerPixRow, markerPixCol] = ginput(1);
            scatter(markerPixRow, markerPixCol, markerPixSizeX*markerPixSizeY, [1 1 0], 'Marker', '+');
            choice = questdlg('Is the marker detected in the right position?', ...
                     'Check marker','Yes', 'Set it manually','Set it manually');
    end
    close(registrationFigureHandle);
    
    markerFromXOffset = ((markerPixCol-W/2)*pixelSizeX + markerImgOffsetX)*10^6;
    markerFromYOffset = ((-markerPixRow+H/2)*pixelSizeY + markerImgOffsetY)*10^6;
catch
    markerFromXOffset = [];
    markerFromYOffset = [];
end