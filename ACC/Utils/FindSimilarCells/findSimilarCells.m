function [ SimCells ] = findSimilarCells( sim_cell, data, sim_perc, numb)
% AUTHOR:   Lassi Paavolainen, Kaisa Kujala
% DATE:     April 22, 2016
% NAME:     findSimilarCells
%
% Finds similar cells that have more than sim_perc cosine similarity.
%
% INPUT:
%   sim_cell        Queried cell feature vector
%   data            Matrix with rows as cells and cols as features
%   sim_perc        Minimum similarity value to consider cell as similar
%   numb            Maximum number of similar cells to return
%                   (+1 the queried cell)
%
% OUTPUT:
%   SimCells        Indices of found similar cells, returns also queried
%                   cell
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

numb = numb + 1; % Return also the same cell
[cell, ~] = size(data);
i=1;
sim=[];

waitBarHandle = waitbar(0,'Finding similar cells.');
counter = 0;

for x = 1:cell
    perc = cosine_similarity(sim_cell, data(x,:));
    if (perc > sim_perc)
        sim(i,1) = perc;
        sim(i,2) = x;
        i=i+1;
    end

    counter = counter + 1;    
    donePercent = double(counter/cell);
    waitText = sprintf('Finding the cells... %d%% done', int16(donePercent * 100));
    waitbar(donePercent, waitBarHandle, waitText);
end

close(waitBarHandle);

% sorts the values from large to small
sim = sortrows(sim, [-1 -2]);
if size(sim,1) < numb
    numb = size(sim,1);
end

if numb > 1
    SimCells = sim(1:numb,2);
else
    SimCells = [];
end

end
