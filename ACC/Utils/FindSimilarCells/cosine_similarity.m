function [ cos_value ] = cosine_similarity( F1, F2 )
% AUTHOR:   Kaisa Kujala, Lassi Paavolainen
% DATE:     April 22, 2016
% NAME:     cosine_similarity
%
% Calculates the cosine similarity of two vectors F1 to F2:
% (F1 dot F2)/{||F1||*||F2||)
%
% INPUT:
%   F1              Feature vector 1
%   F2              Feature vector 2
%
% OUTPUT:
%   cos_value       Cosine similarity value
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

sumxx = 0; sumxy = 0; sumyy = 0;
for i = 1 : length(F1)
    x = F1(i);
    y = F2(i);
    sumxx = sumxx + x*x;
    sumxy = sumxy + x*y;
    sumyy = sumyy + y*y;
end
cos_value = sumxy/sqrt(sumxx*sumyy);
end
