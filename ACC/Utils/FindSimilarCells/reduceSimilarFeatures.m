function [ data ] = reduceSimilarFeatures( data, simpar )
% AUTHOR:   Lassi Paavolainen, Kaisa Kujala
% DATE:     April 22, 2016
% NAME:     reduceSimilarFeatures
%
% Reduces the number of features by calculating a pairwise cosine
% similarity. The pairs that have similarity higher than the given
% parameter are removed by removing one of the features.
%
% INPUT:
%   data            Feature matrix with cells as rows and features as
%                   columns
%   simpar          Maximum allowed similarity between features
%
% OUTPUT:
%   data            Reduced feature matrix
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[cell,col] = size(data);
elim = zeros(1,col);
for x1 = 1:col-1
    if elim(1,x1) > 0 % If feature already set to be removed
        continue
    end
    for x2 = x1+1:col
        if elim(1,x2) > 0 % If feature already set to be removed
            continue
        end
        if (cosine_similarity(data(:,x1),data(:,x2)) > simpar)
            elim(1,x2) = x2;
        end
    end
end

% erases unused data columns
elim(elim==0) = [];
data(:, elim(:)) = [];
end
