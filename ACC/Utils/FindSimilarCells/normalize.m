function [ scaled ] = normalize( array )
% AUTHOR:   Lassi Paavolainen, Kaisa Kujala
% DATE:     April 22, 2016
% NAME:     normalize
%
% Normalizes a feature over cells so that first a subset is created
% including only features with Z-score in range [-1,1]. Second,
% mean and std is calculated from the subset. Finally, all features are
% shifted and scaled so that the mean of a subset is 0, mean-std = -1 and
% mean + std = 1.
%
% INPUT:
%   array           Vector of a feature of all cells
%
% OUTPUT:
%   scaled          Vector of normalized feature
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

stdar = std(array);
meanar = mean(array);
loval = meanar - stdar;
hival = meanar + stdar;
filt_array = array(array > loval & array < hival);

if stdar > 0.0
    scaled = (array - mean(filt_array))/std(filt_array);
else
    scaled = array - mean(array);
end

end
