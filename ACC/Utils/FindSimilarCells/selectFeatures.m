function [ matrix ] = selectFeatures(feature_data, par)
% AUTHOR:   Lassi Paavolainen, Kaisa Kujala
% DATE:     April 22, 2016
% NAME:     selectFeatures
%
% Select features that have variance larger than given parameter value.
%
% INPUT:
%   feature_data    Feature matrix with cells as rows and features as
%                   columns
%   par             Variance threshold for feature selection
%
% OUTPUT:
%   matrix          Feature matrix with reduced number of features
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[cell_amount, column_amount]= size(feature_data);
tmp_matrix=zeros(cell_amount, column_amount);

variance = d_var(feature_data, column_amount, par);
u = 0;
for y = 1:column_amount
    if (var(feature_data(:,y))> variance)
        u = u+1;
        tmp_matrix(:,u)= feature_data(:,y);
    end
end

matrix = tmp_matrix(:, 1:u);
