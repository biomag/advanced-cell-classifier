function [ variance ] = d_var( x, col, par )
% AUTHOR:   Kaisa Kujala, Lassi Paavolainen
% DATE:     April 22, 2016
% NAME:     d_var
%
% Calculates a variance threshold value that will keep user-defined
% percentage of features when removing features with low variance.
%
% INPUT:
%   x               Matrix with rows as cells and columns as features
%   col             Number of cells
%   par             Relative value for finding the threshold. For instance
%                   value 0.6 means threshold is selected so that 60% of
%                   cells with low variance are removed.
%
% OUTPUT:
%   variance        Calculated variance threshold
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

tmp = zeros(col,1);
for i = 1:col 
    value = var(x(:,i));
    tmp(i) = value;
end
Rank = sort(tmp); 
variance = Rank(round(col*par));
end
