function [ final_features ] = prepareFindSimilarCells( features, max_feat_similarity, feat_variance )
% AUTHOR:   Lassi Paavolainen, Tamas Balassa
% DATE:     April 22, 2016
% NAME:     prepareFindSimilarCells
%
% Prepares find similar cells search. Performs three steps:
%   1) Normalizes features
%   2) Selects features with high variance
%   3) Reduces features with high similarity
%
% INPUT:
%   features               Feature matrix with cells as rows and features
%                          as columns
%   max_feat_similarity    Maximum feature similarity for feature reduction
%   feat_variance          Defines the ratio of features to be removed
%                          based on low variance
%
% OUTPUT:
%   final_features         Reduced feature matrix for similar cell search
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

waitBarHandle = waitbar(0,'Preparing similar cells.');
counter = 0;

normalized_features = zeros(size(features));
for i=1:size(features,2)
    normalized_features(~isnan(features(:,i)),i) = normalize(features(~isnan(features(:,i)),i));   
    
    counter = counter + 1;    
    donePercent = double(counter/size(features,2));
    waitText = sprintf('Normalizing... %d%% done', int16(donePercent * 100));
    waitbar(donePercent, waitBarHandle, waitText);
end

close(waitBarHandle);

sel_features = selectFeatures(normalized_features, feat_variance);
final_features = reduceSimilarFeatures(sel_features, max_feat_similarity);

end
