function [paramcell] = Settings_GUI(varargin)
    %SETTINGS_GUI To overload the uiwait settings GUI

    global currentParametersForCrossValidation;
    paramcell = currentParametersForCrossValidation.regParams;
end

