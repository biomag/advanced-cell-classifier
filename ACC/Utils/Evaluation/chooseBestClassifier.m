function [evalTable] = chooseBestClassifier(evalTable)

for i=1:size(evalTable,1)
    [~,bestIdx] = max(cell2mat(evalTable.ClassifierResults{i}(~cellfun(@ischar,evalTable.ClassifierResults{i}(:,2)) & ~cellfun(@isempty,evalTable.ClassifierResults{i}(:,2)),2)));
    evalTable.bestClassifier{i} = evalTable.ClassifierResults{i}{bestIdx,1};
    evalTable.bestClassifierIdx{i} = bestIdx;
end

end

