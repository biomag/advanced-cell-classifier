function [fileName,pathString] = uigetfile(~,titleString)
%Function overload for automatic GUI options for testing

global currentParametersForCrossValidation;

%Switch for the titleString
if strcmp(titleString,'Select workspace')
    [pathString,fileName,exex] = fileparts(currentParametersForCrossValidation.projectPath);
    fileName = [fileName exex];
    pathString = [pathString '\'];
end

end

