function [evalTable] = chooseBestRegressor(evalTable)

for i=1:size(evalTable,1)
    if ~isempty(evalTable.RegressionResults{i})
        nofRegressionClasses = length(evalTable.RegressionResults{i});
        bestRegressors = cell(nofRegressionClasses,4);
        for j=1:nofRegressionClasses
            [~,bestRegressorIdx] = min(cell2mat(evalTable.RegressionResults{i}{j}(:,4)));
            bestRegressors{j,1} = evalTable.RegressionResults{i}{j}{bestRegressorIdx,1};
            bestRegressors{j,2} = evalTable.RegressionResults{i}{j}{bestRegressorIdx,2};
            bestRegressors{j,3} = evalTable.RegressionResults{i}{j}{bestRegressorIdx,3};
            bestRegressors{j,4} = evalTable.RegressionResults{i}{j}{bestRegressorIdx,4};
        end
        evalTable.bestRegressor{i} = bestRegressors;
    end
end

end

