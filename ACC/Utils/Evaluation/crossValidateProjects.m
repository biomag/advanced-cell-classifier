%Script to run all available classifiers on the given projects and provide
%a performance measure of those (by cross validation).

%WARNING: AFTER USING THIS SCRIPT RENAME THE UIGET FUNCTION NAMES!

%Doc on project paths:
%The project path to be loaded and the list of classes that should be
%regression classes on which regression evaluation should be run as well.
projectPaths = {...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191020_Classification_Full.mat',[];...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191020_Classification_ST7.mat',[]';...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191020_Classification_ST9.mat',[]';...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191017_Regression_ST9_Copy190725_Vilja_ST9_duplicationRemoved.mat',1;...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191017_Regression_ST7.mat',1;...
    'G:\Abel\SZBK\Projects\Vilja\LipidStudy\Annotations\FinalTrainings\191017_Regression_Full.mat',1;...
    };
%projectPaths = {'C:\Users\szkabel\Documents\tmpSingleRegProject.mat',[1]};

%Now go through on Regression models and also on parameters
listOfRegressors = trainRegression_GUI('listAvailableRegressors');

listOfWekaRegressors = listWekaRegressors;

%NOTE: here you should specify the paramcells for each of those methods if
%needed. Each entry here is for one method above
regParameterCells = {...
    { ... % GPML
        {'on','meanConst','covSEiso','likGauss'}, ...
        {'on','meanConst','covSEard','likGauss'}, ...
        {'on','meanConst','covNNone','likGauss'}, ...
        {'on','meanLinear','covSEiso','likGauss'}, ...
        {'on','meanLinear','covSEard','likGauss'}, ...
        {'on','meanLinear','covNNone','likGauss'}, ...
        {'on','meanNN','covSEiso','likGauss'}, ...
        {'on','meanNN','covSEard','likGauss'}, ...
        {'on','meanNN','covNNone','likGauss'}, ...
        {'off','meanConst','covSEiso','likGauss'}, ...
        {'off','meanConst','covSEard','likGauss'}, ...
        {'off','meanConst','covNNone','likGauss'}, ...
        {'off','meanLinear','covSEiso','likGauss'}, ...
        {'off','meanLinear','covSEard','likGauss'}, ...
        {'off','meanLinear','covNNone','likGauss'}, ...
        {'off','meanNN','covSEiso','likGauss'}, ...
        {'off','meanNN','covSEard','likGauss'}, ...
        {'off','meanNN','covNNone','likGauss'}, ...
    }; ...        
    { ... %MulanEoC        
        {listOfWekaRegressors{1},'2'}, ...
        {listOfWekaRegressors{2},'2'}, ...
        {listOfWekaRegressors{3},'2'}, ...
        {listOfWekaRegressors{4},'2'}, ...
        {listOfWekaRegressors{5},'2'}, ...
    }; ...
    { ... %MulanMTStack
        {listOfWekaRegressors{1},listOfWekaRegressors{1}}, ...
        {listOfWekaRegressors{1},listOfWekaRegressors{2}}, ...
        {listOfWekaRegressors{1},listOfWekaRegressors{3}}, ...
        {listOfWekaRegressors{1},listOfWekaRegressors{4}}, ...
        {listOfWekaRegressors{1},listOfWekaRegressors{5}}, ...
        {listOfWekaRegressors{2},listOfWekaRegressors{1}}, ...
        {listOfWekaRegressors{2},listOfWekaRegressors{2}}, ...
        {listOfWekaRegressors{2},listOfWekaRegressors{3}}, ...
        {listOfWekaRegressors{2},listOfWekaRegressors{4}}, ...
        {listOfWekaRegressors{2},listOfWekaRegressors{5}}, ...
        {listOfWekaRegressors{3},listOfWekaRegressors{1}}, ...
        {listOfWekaRegressors{3},listOfWekaRegressors{2}}, ...
        {listOfWekaRegressors{3},listOfWekaRegressors{3}}, ...
        {listOfWekaRegressors{3},listOfWekaRegressors{4}}, ...
        {listOfWekaRegressors{3},listOfWekaRegressors{5}}, ...
        {listOfWekaRegressors{4},listOfWekaRegressors{1}}, ...
        {listOfWekaRegressors{4},listOfWekaRegressors{2}}, ...
        {listOfWekaRegressors{4},listOfWekaRegressors{3}}, ...
        {listOfWekaRegressors{4},listOfWekaRegressors{4}}, ...
        {listOfWekaRegressors{4},listOfWekaRegressors{5}}, ...
        {listOfWekaRegressors{5},listOfWekaRegressors{1}}, ...
        {listOfWekaRegressors{5},listOfWekaRegressors{2}}, ...
        {listOfWekaRegressors{5},listOfWekaRegressors{3}}, ...
        {listOfWekaRegressors{5},listOfWekaRegressors{4}}, ...
        {listOfWekaRegressors{5},listOfWekaRegressors{5}}, ...
    }; ...    
    { ... %NNPredictor        
        { 30 }, ...
        { 70 }, ...
        { 100 }, ...
        { [70 40] }, ...
        { [70 40 20] }, ...
        { [70 40 20 4] }, ...
    }; ...
    { ... %WekaGaussian
        {};
    }; ...
    { ... %Weka KNN
        {};
    }; ...
    { ... %Weka Linear
        {};
    }; ...
    { ... %Weka MLP
        {};
    }; ...
    { ... %Weka RepTree
        {};
    }; ...
    { ... %Weka RandomForest
        {};
    }; ...
    { ... %Weka SMO
        {};
    }; ...
};

[evalPath, ~, ~] = fileparts(mfilename('fullpath'));

global currentParametersForCrossValidation;
global CommonHandles;

try
    movefile(fullfile(evalPath,'uigetfileSpec.m'),fullfile(evalPath,'uigetfile.m'));
    movefile(fullfile(evalPath,'Settings_GUISpec.m'),fullfile(evalPath,'Settings_GUI.m'));
    addpath(evalPath);

    evalTable = table([],{},{},'VariableNames',...
        {'ProjectNameAndPath',...
        'ClassifierResults',...
        'RegressionResults'});

    %ACC must be opened when this script is running

    for i=1:size(projectPaths,1)
        currentParametersForCrossValidation.projectPath = projectPaths{i};

        %Open project
        ACC_main_GUI('toolbarOpenProject_ClickedCallback',CommonHandles.MainWindow, [], guidata(CommonHandles.MainWindow));

        %Run classification only if there are more than 1 class
        if length(CommonHandles.Classes)>1, singleClass = false; else, singleClass = true; end
        
        if ~singleClass
            %Run only if this analysis haven't been run yet
            if size(evalTable,1) < i
                classResults = cell(length(CommonHandles.ClassifierNames),3);    
                classesToRun = setdiff(1:length(CommonHandles.ClassifierNames),[11,15]); %Logistic Regression and LibSVM seemed to run forever (first stopped after a full night, latter after 1 hour)        
                for j=1:length(CommonHandles.ClassifierNames)        
                    tic;
                    if ismember(j,classesToRun)
                        fprintf('**********----------**********\nProject: %s\nTraining has started with classifier %s at: %s\n',projectPaths{i},CommonHandles.ClassifierNames{j},timeString(6));
                        try
                            %Train classifier        
                            trainFig = train_algorithmGUI;

                            trainHandles = guidata(trainFig);    
                            trainHandles.popupmenu1.Value = j;
                            trainHandles.checkbox1.Value = 1;        
                            train_algorithmGUI('pushbutton3_Callback',trainFig,[],trainHandles);
                            if ishandle(trainFig), close(trainFig); end

                            h = findall(0,'Name','Model Performance');
                            crossValHandles = guidata(h);
                            accuracyStr = strsplit(crossValHandles.text3.String,' ');
                            accuracy = str2double(accuracyStr{1});
                            classResults{j,1} = CommonHandles.ClassifierNames{j};
                            classResults{j,2} = accuracy;
                        catch e
                            classResults{j,1} = CommonHandles.ClassifierNames{j};
                            classResults{j,2} = e.message;
                        end
                        fprintf('Finished at: %s\n**********----------**********\n',timeString(6));
                    else
                        classResults{j,1} = CommonHandles.ClassifierNames{j};
                    end        
                    classResults{j,3} = toc;
                end

                evalTable.ProjectNameAndPath{i} = projectPaths{i,1};
                evalTable.ClassifierResults{i} = classResults;
            end
            
            %Choose the best classifier for all
            evalTable = chooseBestClassifier(evalTable);
        end       

        %Run regression analysis if needed        
        if ~isempty(projectPaths{i,2})            
            
            if ~singleClass
                %Train again with the best classifier
                trainFig = train_algorithmGUI;

                trainHandles = guidata(trainFig);    
                trainHandles.popupmenu1.Value = evalTable.bestClassifierIdx{i};
                trainHandles.checkbox1.Value = 0; %No performance measure in this case
                train_algorithmGUI('pushbutton3_Callback',trainFig,[],trainHandles);
                if ishandle(trainFig), close(trainFig); end
            end

            %Open the regression planes one-by-one
            resultsByPlanes = cell(length(projectPaths{i,2}),1);
            for j=1:length(projectPaths{i,2})
                classIdx = projectPaths{i,2}(j);
                
                counter = 1;
                regressionResults = cell(sum(cellfun(@length,regParameterCells)),5);

                CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',0,'classIndex',classIdx,'prediction',-1));

                for k=1:length(listOfRegressors)                                    
                    for kk = 1:length(regParameterCells{k})
                        try
                            trainRegFig = trainRegression_GUI('UserData',struct('classIndex',classIdx));
                            trainRegHandles = guidata(trainRegFig);
                            trainRegHandles.regressionList.Value = k;

                            currentParametersForCrossValidation.regParams = regParameterCells{k}{kk};

                            fprintf('**********----------**********\nProject: %s\nTraining has started with regressor %s (Params #%d) at: %s\n',projectPaths{i},listOfRegressors{k},kk,timeString(6));
                            tic;
                            trainRegression_GUI('trainRegressionButton_Callback',trainRegFig,[],trainRegHandles);

                            regressionPlane('toolbarRegressionPerformance_OnCallback',CommonHandles.RegressionPlaneHandle,[],guidata(CommonHandles.RegressionPlaneHandle));

                            h = findall(0,'Name','Regression performance');
                            if ishandle(h), close(h); end

                            %Fill in required values                        

                            regressionResults{counter,1} = listOfRegressors{k};
                            regressionResults{counter,2} = regParameterCells{k}{kk};
                            regressionResults{counter,3} = CommonHandles.RegressionPlane{classIdx}.CrossValidation.PerformanceChange{end}.RMSE;
                            regressionResults{counter,4} = CommonHandles.RegressionPlane{classIdx}.CrossValidation.PerformanceChange{end}.RRMSE;
                            regressionResults{counter,5} = toc;
                            counter = counter + 1;         

                            fprintf('Finished at: %s\n**********----------**********\n',timeString(6));
                        catch e
                            regressionResults{counter,1} = listOfRegressors{k};
                            regressionResults{counter,2} = regParameterCells{k}{kk};
                            regressionResults{counter,3} = e.message;
                            regressionResults{counter,4} = [];
                            regressionResults{counter,5} = toc;
                            
                            counter = counter + 1;         

                            fprintf('Finished with error: %s\nat: %s**********----------**********\n',e.message,timeString(6));
                        end
                    end

                end            
                resultsByPlanes{j} = regressionResults;
            end
            
            evalTable.RegressionResults{i} = resultsByPlanes;

        end        
    end
    movefile(fullfile(evalPath,'uigetfile.m'),fullfile(evalPath,'uigetfileSpec.m'));
    movefile(fullfile(evalPath,'Settings_GUI.m'),fullfile(evalPath,'Settings_GUISpec.m'));

catch e
    movefile(fullfile(evalPath,'uigetfile.m'),fullfile(evalPath,'uigetfileSpec.m'));
    movefile(fullfile(evalPath,'Settings_GUI.m'),fullfile(evalPath,'Settings_GUISpec.m'));

    %Display error but rename files for sure
    rethrow(e);
end
