function convertInstances(data)
% AUTHOR:   Tamas Balassa
% DATE:     Jarnuary 19, 2016
% NAME:     convertInstances
% 
% This function will get the best descriptive attributes with the help of
% weka's info gain method.
%
% INPUT:
%   data         Specific data structure from ACC. (created by convertACC2SALT)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% define the number of features, classes, and instances
numfeats = size(data.instances,2);
numclass = numel(data.classNames); 
N = size(data.instances, 1);

% enumerate the attributes (features)
attributes = weka.core.FastVector(numfeats+1);
for i=1:numfeats
    attributes.addElement(weka.core.Attribute(['feature' num2str(i)]));
end

% enumerate the classes
classvalues = weka.core.FastVector(numclass);    
for i=1:numclass
    classvalues.addElement(['class' num2str(i)]);
end
attributes.addElement(weka.core.Attribute('Class', classvalues));

% create WEKA data class
trainingdata = weka.core.Instances('training_data', attributes, N);
trainingdata.setClassIndex(trainingdata.numAttributes() - 1);

% fill trainingdata with instances containing values from 'data'
w = 1;
for i = 1:N
    inst = weka.core.DenseInstance(w, [data.instances(i,:) 0]);
    inst.setDataset(trainingdata);
  	classLabel = data.labels(i);
    inst.setClassValue(['class' num2str(classLabel)]);
    trainingdata.add(inst);
end

global CommonHandles;

attrSel = weka.attributeSelection.AttributeSelection();
subsetEval = weka.attributeSelection.InfoGainAttributeEval();
searchMethod = weka.attributeSelection.Ranker();
attrSel.setEvaluator(subsetEval);
attrSel.setSearch(searchMethod);
attrSel.SelectAttributes(trainingdata);
tmpSelectedAttr = attrSel.selectedAttributes();
selectedAttr  = tmpSelectedAttr(1:CommonHandles.HC.Default.SupportInfoIG) + 1;
CommonHandles.HC.Default.InfoGainAttributes = selectedAttr;

end

