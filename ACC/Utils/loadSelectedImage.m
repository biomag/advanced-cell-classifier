function CommonHandles = loadSelectedImage(CommonHandles,varargin)
% AUTHOR:	Peter Horvath, Tamas Balassa, Abel Szkalisity
% DATE: 	May 7, 2020
% NAME: 	loadSelectedImage
% 
% To load in the MainView the image selected in the "Plate Window"
%
% Optional NAME-VALUE pairs:
%   currPlateNumber      The number of the current plate to be read in. If
%                        missing it means that we do NOT show the cell!
%   loadType             'full','onlyMeta' decides if to load everything OR
%                        only the metadata (features)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

p = inputParser();
p.addParameter('currPlateNumber',[]);
p.addParameter('loadType','full');
p.parse(varargin{:});

[~, fileNameexEx, ~] = fileparts(CommonHandles.SelectedImageName);

if isempty(p.Results.currPlateNumber)
    [CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);
else
    [CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, p.Results.currPlateNumber);
end

if CommonHandles.SelectedCell < 0 || CommonHandles.SelectedCell > size(CommonHandles.SelectedMetaData, 1)        
    CommonHandles.SelectedCell = 1;
end

if strcmp(p.Results.loadType,'full')

    CommonHandles.CurrentImage = imread([CommonHandles.DirName filesep CommonHandles.SelectedImageName]);
    CommonHandles.CurrentImageSizeX = size(CommonHandles.CurrentImage,1);
    CommonHandles.CurrentImageSizeY = size(CommonHandles.CurrentImage,2);

    % Storing bounds of visible part of the current image
    CommonHandles.MainView.XBounds = [1 CommonHandles.CurrentImageSizeY];
    CommonHandles.MainView.YBounds = [1 CommonHandles.CurrentImageSizeX];

    CommonHandles.CurrentOriginalImageNoTouch = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);

     I = imread([CommonHandles.DirName filesep CommonHandles.SelectedOriginalImageName]);
     if size(I,3) == 1
         I = cat(3,I,I,I);
     end
     if ~isa(I,'uint8')
         I = im2uint8(I);
     end
     
     
     if CommonHandles.MainView.StrechImage == 1

         % Needs to be verifyed this saturation value
         tolr = 0.0;
         tolg = 0.001;
         tolb = 0.005;

         sl(:,1) = [0,1]';%stretchlim(I(:,:,1), tolr); %smalls = find(sl(2,:) < 0.05); sl(2,smalls) = 1;
         sl(:,2) = stretchlim(I(:,:,2), tolg);
         sl(:,3) = stretchlim(I(:,:,3), tolb);
         CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
     else
         sl(:, 1)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
         sl(:, 2)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
         sl(:, 3)  = [CommonHandles.MainView.StrechMin/255, CommonHandles.MainView.StrechMax/255]';
         CommonHandles.CurrentOriginalImage = imadjust(I,sl,[]);
     end


    CommonHandles.CurrentPrediction = {};

    CommonHandles = setImageIntensity(CommonHandles);

    if isempty(p.Results.currPlateNumber)
        CommonHandles = showSelectedCell(CommonHandles);
    end

end