function addSampleToTrainingSet(counter,j2Cstructs)

    global CommonHandles;
    
    if ~exist('j2Cstructs','var') || isempty(j2Cstructs)
        j2Cstructs.PlateName = CommonHandles.SelectedPlate;
%         j2Cstructs.ImageName = fullfile(CommonHandles.PlatesNames{CommonHandles.SelectedPlate},CommonHandles.ImageFolder,CommonHandles.SelectedImageName);
        j2Cstructs.ImageName = CommonHandles.SelectedImageName;
%         j2Cstructs.OriginalImageName = fullfile(CommonHandles.PlatesNames{CommonHandles.SelectedPlate},CommonHandles.OriginalImageFolder,CommonHandles.SelectedImageName);
        j2Cstructs.OriginalImageName = CommonHandles.SelectedImageName;
        
        j2Cstructs.CellNumber = CommonHandles.SelectedCell;
    end
    
    for i = 1:length(j2Cstructs)
        jumpToCell(j2Cstructs(i),false, 'loadType', 'full');
        alreadyInTrainingSet = isInTrainingSet(CommonHandles.SelectedImageName,CommonHandles.SelectedCell,0,CommonHandles,CommonHandles.SelectedMetaData(CommonHandles.SelectedCell,3:end));
        
        
        if ~alreadyInTrainingSet
            
            featureVector = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, 3: size(CommonHandles.SelectedMetaData, 2));
            
            addToClassImages(counter);
            addToTrainingSet(counter,featureVector);
            
        else
            button = questdlg('Would you like to change the class of this cell?','Reclassify?','Yes','No','No');
            
            if strcmp(button, 'Yes')
                
                addToClassImages(counter);
                CommonHandles.Classes{counter}.LabelNumbers = CommonHandles.Classes{counter}.LabelNumbers + 1;
                moveClassImage(CommonHandles.SelectedCell, CommonHandles.SelectedImageName, counter);
                if ishandle(CommonHandles.ShowTrainedCellsFig)
                    close(CommonHandles.ShowTrainedCellsFig)
                end
                
                
            end
        end
    end
    refreshClassesList();
end