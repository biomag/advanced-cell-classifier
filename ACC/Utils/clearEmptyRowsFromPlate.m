function [cleanFeatures,cleanMapping,cleanCoords] = clearEmptyRowsFromPlate(features,mapping,coords)
% AUTHOR:	Tamas Balassa
% DATE: 	June 21, 2016
% NAME: 	clearEmptyRowsFromPlate
% 
% This function go through the sampling pool (CommonHandles.AllFeatures
% etc.) and removes all the entries there which are clearly not cells (the
% total zero rows) They can appear if there isn't any cell on an image.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    cutSample = zeros(1,size(features,2));

    featuresTemp = zeros(size(features,1),size(features,2));
    coordsTemp = zeros(size(coords,1),size(coords,2));
    mappingTemp = cell(1,size(features,1));
    k=1;
    
    waitBarHandle = waitbar(0,'Clearing the rows...');
    
    for iii=1:size(features,1)
        [idx] = find(any(all(bsxfun(@eq,cutSample,features(iii,:)),2),3));
        if isempty(idx)
            featuresTemp(k,:) = features(iii,:);
            mappingTemp{k} = mapping{iii};
            coordsTemp(k,:) = coords(iii,:);
            k=k+1;
        end
        
        donePercent = double(iii/size(features,1));
        waitText = sprintf('Clearing the rows... %d%% done', int16(donePercent * 100));
        waitbar(donePercent, waitBarHandle, waitText);
    end
    
    featuresTemp(k:end,:) = [];
    mappingTemp(k:end) = [];
    coordsTemp(k:end,:) = [];
    
    cleanFeatures = featuresTemp;
    cleanMapping = mappingTemp;
    cleanCoords = coordsTemp;
    
    close(waitBarHandle);