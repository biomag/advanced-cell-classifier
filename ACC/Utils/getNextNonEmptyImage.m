function index = getNextNonEmptyImage()
% AUTHOR:   Tamas Balassa
% DATE:     June 23, 2016
% NAME:     getNextNonEmptyImage
% 
% This function will return with the index of the next non-empty image from
% the imagelist. This is used to skip the empty images.
%
% WARNING - This function modifies CommonHandles.SelectedImage. It has to
% be handled after the call of this function.
%
% OUTPUT:
%   index         The index of the next non-empty image
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    if CommonHandles.SelectedImage ~= 0
        CommonHandles.SelectedImage = get(CommonHandles.ImageListHandle,'Value');
    end
    ImageList = get(CommonHandles.ImageListHandle,'String');

    index = 0;

    for i=(CommonHandles.SelectedImage+1):length(ImageList)
        tempSelectedImage = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(i))];
        [~, fileNameexEx, ~] = fileparts(tempSelectedImage);
        [~,tempSelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);
        first = size(tempSelectedMetaData,1);
        second = size(tempSelectedMetaData,2);
        if first==1 || second==1
            temp = zeros(first,second);
        else
            temp = -1;
        end
        if ~isequal(tempSelectedMetaData,temp)
            [~,index] = ismember(ImageList(i),ImageList);
            break;
        end
    end
    
    if index == 0
        errordlg('All remained images are empty.');
        CommonHandles.SelectedImage = 0;
        index = getNextNonEmptyImage();
    end