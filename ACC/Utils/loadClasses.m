function loadClasses(classesPath, classesFile)
% AUTHOR:	Tamas Balassa
% DATE: 	April 22, 2016
% NAME: 	loadClasses
% 
% Load the data into "CommonHandles.Classes" from the "classesFile" in 
% the "classesPath".
%
% INPUT:
%   classesPath     Path of the selected file.
%   classesFile     Name of the selected file.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

fileName = [classesPath classesFile];
tmp = load(fileName,'-mat');
CommonHandles.Classes = tmp.tmp_classes;

for i=1:length(CommonHandles.Classes)
    CommonHandles.Classes{1,i}.LabelNumbers = 0;
end

CommonHandles.MaxCluster = length(CommonHandles.Classes);
refreshClassesList();

