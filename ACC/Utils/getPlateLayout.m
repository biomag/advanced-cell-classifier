function [ wellIDs,wellAnnotation ] = getPlateLayout( startFolder )
% AUTHOR:   Abel Szkalisity
% DATE:     March 9, 2017
% NAME:     getPlateLayout
%
% Asks for the plate layout from the user.
%
% INPUT:
%   startFolder     A folder in which the user input window is going to
%                   start.
%
% OUTPUT:
%   wellIDs         A cellarray with the wellIDs (strings). Such as A01 etc
%   wellAnnotation  Cellarray with the same length as wellIDs, containing
%                   the corresponding annotation info.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


%function candidate 1.
%uigetfile({'*.xml';'*.txt';'*.*'},'Select screen descriptor XML',fullfile(CommonHandles.DirName));
[csvFile,csvFilePath] = uigetfile({'*.csv','CSV files'},'Specify your plate-layout file (.csv)',startFolder);
wellIDs = [];
wellAnnotation = [];    
if ~csvFile    
    return;
end
[~,~,ext] = fileparts(csvFile);
if ~strcmp(ext,'.csv')
    warndlg('The selected file is not supported. Please convert your plate-layout to csv.');
else
    try        
        [ wellIDs, wellAnnotation ] = csvPlateLayoutToWellsAndAnnotation(fullfile(csvFilePath,csvFile));
    catch
    end
end


end

