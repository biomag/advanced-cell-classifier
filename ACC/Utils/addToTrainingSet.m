function addToTrainingSet( classPosition, featureVector )
% AUTHOR:   Tamas Balassa, Abel Szkalisity
% DATE:     May 18, 2016
% NAME:     addToTrainingSet
% 
% This function adds the currently selected cell to the training set.
% ClassPosition is the class index for the currently selected cell. It works
% using the variable "global CommonHandles" to extract the info of the
% currently selected cell.
% WARNING: The function assumes that the ClassImgObj is already added to
% the appropriate Class map (which stores the icons)
%
% INPUT:
%   classPosition         It is the index of an already created class.
%   featureVector         Featurevector of the cell which will be added
%   params                Struct of parameters, if not set, read from
%                           CommonHandles
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    %Extra check: it still can happen that the user selects the regression
    %class and then forgets to place it on the plane (can't force to do it).
    %Therefore we check at each training set addition if the previous cell
    %classified as regression and it has the position or not
    if ~isempty(CommonHandles.TrainingSet.Class)
        lastClassPos = find(CommonHandles.TrainingSet.Class{end});
        if strcmp(CommonHandles.Classes{lastClassPos}.Type,'Regression')
            if (lastClassPos == classPosition)
                rollbackRegressionAddition(lastClassPos,1); % this is because the last image in the ClassImages now is the new one that needs to be annotated
            else
                rollbackRegressionAddition(lastClassPos,0);
            end
        end
    end
    
    saveClassificationData(classPosition,featureVector);
            
    if strcmp(CommonHandles.Classes{classPosition}.Type,'Regression')
        
        CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',length(CommonHandles.TrainingSet.Features),'classIndex',classPosition,'classID',CommonHandles.Classes{classPosition}.ID,'prediction',0));
        CommonHandles.RegressionPlaneHandle.Children(end).XColor = [0 1 0];
        CommonHandles.RegressionPlaneHandle.Children(end).YColor = [0 1 0];
        uiwait(CommonHandles.RegressionPlaneHandle);
        CommonHandles.RegressionPlaneHandle.Children(end).XColor = [1 1 1];
        CommonHandles.RegressionPlaneHandle.Children(end).YColor = [1 1 1];
               
        rollbackRegressionAddition(classPosition,0);
                
        pause(0.1);              
        %Try to jump focus back to MainGUI
        divisor = '8.4.0.150.421 (R2014b)';
        if (compareVersions(divisor,CommonHandles.MatlabVersion))
            set(CommonHandles.MainWindow,'Visible','off');                       
        end
        set(CommonHandles.MainWindow,'Visible','on');
        figure(CommonHandles.MainWindow);
                
    end

end

function rollbackRegressionAddition(classPosition,posFromLast)
%Deletes the last training set element if its regression position is missing.
%NOTE: it is not checked if the last annotated cell is annotated to a
%regression class, that must be done outside, before calling this function.
%   INPUTS:
%       classPosition   The position of the class in the
%                       CommonHandles.Classed cellarray.
%       posFromLast     How many images to skip from the end when deleting
%                       from classImages.
    global CommonHandles;
    %if the did regression position is missing from the last annotation
    if isempty(CommonHandles.TrainingSet.RegPos{end})
        %rollback
        idx = length(CommonHandles.TrainingSet.RegPos);
        deleteFromTrainingSet(idx);
        classImages = CommonHandles.ClassImages(CommonHandles.Classes{classPosition}.Name);
        classImages.Images(end-posFromLast) = [];
        CommonHandles.ClassImages(CommonHandles.Classes{classPosition}.Name) = classImages;
        warndlg('The last annotated cell was removed from training set because the regression position was not provided.','Regression warning');
    end
end