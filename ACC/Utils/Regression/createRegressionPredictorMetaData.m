function createRegressionPredictorMetaData(tgtFolder,idx,controls)
% AUTHOR:   Abel Szkalisity
% DATE:     July 20, 2019
% NAME:     createRegressionPredictorMetaData
%
% Creates a Metadata file to the regression output (tgtFolder) directory
% sot that the results are documented and reproducible.
%
% INPUT:
%   tgtFolder   The folder to where the file is going to be saved.
%   idx         The index of the current regression class
%   controls    The list of the control wells used.
%
% OUTPUT:
%   The function creates a file named 'ResultAndPredictor_MetaData.txt'.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

f = fopen(fullfile(tgtFolder,'ResultAndPredictor_MetaData.txt'),'w');

pred = CommonHandles.RegressionPlane{idx}.Predictor;
fprintf(f,'Regression results for class: %s (Class #%d)\n*************************\n\n',CommonHandles.Classes{idx}.Name,idx);
fprintf(f,'The report was created with the following Regression Predictor (model):\n\n');
pred.print2file(f);
fprintf(f,'\n*************************\n\n');
fprintf(f,'The following wells/images were used as controls:\n');
for i=1:numel(controls)
    fprintf(f,'%s\n',controls{i});
end

fclose(f);

end