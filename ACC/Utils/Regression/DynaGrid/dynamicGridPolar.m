function dynamicGridPolar(origo, numOfRay, initialTheta, centerShape, centerRadius)

% origo = [0.5 0.5];
% numOfRay = 40;
% initialTheta = 15;
% centerShape = 'circle';
% centerRadius = 0.2;

f = figure;
set(f,'WindowButtonDownFcn',@customWindowsButtonDownFunction);
ax = axes(f);
f.UserData.origo = origo;
f.UserData.oldPosition = [];
f.WindowButtonUpFcn = @customeButtonUp;
c = [0 1 0.8];
f.UserData.color = c;
l = 2;

rayLength = 1;
sectionSize = (2*pi)/numOfRay;
initThetaRad = deg2rad(initialTheta);

theta0 = [0, initThetaRad];
rho = [0 rayLength];
[X, Y] = pol2cart(theta0, rho);
rayArray = plot(ax, X+origo, Y+origo, 'color', 'r', 'LineWidth', l, 'Tag', '0','ButtonDownFcn',@customeLineButtonDown);
rayArray.UserData.theta = theta0(2);
hold on
ax.XLim = [0 1];
ax.YLim = [0 1];
for i = 1:numOfRay-1
    theta = [0, sectionSize] * i + [0 initThetaRad];
    [X, Y] = pol2cart(theta, rho);
    rayArray(i+1, 1) = plot(ax,X+origo,Y+origo,'color',c,'LineWidth',l, 'Tag', num2str(i),'ButtonDownFcn',@customeLineButtonDown);
    rayArray(i+1, 1).UserData.theta = theta(2);
end

switch centerShape
    
    case 'circle'
        midCircle = viscircles(ax,origo,centerRadius, 'color', c, 'LineWidth', l);
        midCircle.Children(1).ButtonDownFcn = {@customeCircleButtonDown};
        midCircle.Children(1).UserData = c;
        delete(midCircle.Children(2));
        
    case 'triangle'
        
    case 'Rectangle'

        sizeX = centerRadius;
        sizeY = centerRadius;
        loX = origo(1) - sizeX/2;
        loY = origo(2) - sizeY/2;
        rekt = rectangle(ax,'Position',[loX loY sizeX sizeY], 'EdgeColor', c,...
                  'LineWidth',l);
    otherwise
            return
end