function lineTrafo(hObject, eventData, origo, phi)

T1xy = eye(3);
T1xy(1:2, 3) = -origo;

Rxy = [cos(phi) sin(phi) 0;...
      -sin(phi) cos(phi) 0;
         0         0     1];

T2xy = eye(3);
T2xy(1:2, 3) = origo;

oldCoor = [hObject.XData(2); hObject.YData(2); 1];

trafoMat = T2xy*Rxy*T1xy;

newCoor = trafoMat * oldCoor;

hObject.XData(2) = newCoor(1);
hObject.YData(2) = newCoor(2);
