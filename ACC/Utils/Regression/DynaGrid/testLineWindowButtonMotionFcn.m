function testLineWindowButtonMotionFcn(hObject,eventdata)

idx = hObject.UserData.SelectedObject;
window =  hObject.UserData.window;
origo = hObject.UserData.origo;
oldPosition = [hObject.Children.Children(end-idx).XData(2), hObject.Children.Children(end-idx).YData(2), 1];
currentPosition = [hObject.Children.CurrentPoint(1, 1:2), 1];

T1xy = eye(3);
T1xy(1:2, 3) = -origo;

oldPositionShifted = T1xy * oldPosition';
currentPositionShifted = T1xy * currentPosition';

phi = calcTheta(currentPositionShifted(1:2)',oldPositionShifted(1:2)',[0 0]);

if  idx ~= 0
    currentTheta = hObject.Children.Children(end-idx).UserData.theta + phi;
    if currentTheta - window(1) > 0.05 && window(2) - currentTheta < 0.05
        hObject.Children.Children(end-idx).UserData.theta = currentTheta;
        lineTrafo(hObject.Children.Children(end-idx), [], origo, phi);
    else
        'test'
    end
else
    multyLineTrafo(hObject.Children.Children(2:end), [], origo, phi);
    origTheta = get(hObject.Children.Children(2:end),'UserData');
    for i = 1:length(origTheta)
        origTheta{i}.theta = origTheta{i}.theta + phi;
    end
    set(hObject.Children.Children(2:end),{'UserData'},origTheta);
end
