function customeButtonUp(hObject, eventdata)
if strcmp(hObject.Pointer, 'hand')
    idx = hObject.UserData.SelectedObject;
    hObject.Pointer = 'arrow';
    hObject.WindowButtonMotionFcn = '';
    while ~strcmp(class(hObject.Children.Children(2)),'matlab.graphics.chart.primitive.Line')
        delete(hObject.Children.Children(2));
    end
    if idx == length(hObject.Children.Children)-1
        hObject.Children.Children(1).Children.Color = hObject.UserData.color;
    elseif idx ~= 0
        hObject.Children.Children(end-idx).Color = hObject.UserData.color;
    else
        hObject.Children.Children(end-idx).Color = [1 0 0];
    end
end