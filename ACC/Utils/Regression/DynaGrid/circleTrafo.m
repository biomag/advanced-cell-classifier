function circleTrafo(hObject, eventData, origo, scaler)

ax = hObject.Parent.Parent;
t = hgtransform('Parent',ax);
set(hObject,'Parent',t)

T1xy = makehgtform('translate',[origo*(-1), 0]);
Sxy = makehgtform('scale',scaler);
T2xy = makehgtform('translate',[origo, 0]);

set(t,'Matrix',T2xy*Sxy*T1xy)