function customWindowsButtonDownFunction(hObject,eventdata)

clickCoor = get(hObject.Children, 'CurrentPoint');
clickCoor = clickCoor(1,1:2);
if clickCoor(1, 1) > 0 && clickCoor(1, 1) < 1 && clickCoor(1, 2) > 0 && clickCoor(1, 2) < 1
    lines = hObject.Children.Children(2:end);
    linesX = cell2mat(get(lines, 'XData'));
    linesY = cell2mat(get(lines, 'YData'));
    dist = sqrt(((linesY(:,2) - clickCoor(2))).^2 + ((linesX(:,2) - clickCoor(1))).^2);
    [~,idx] = mink(dist,2); % index for the closest points, i.e. those with min distance.
    
%     if length(idx) == 2 && abs(idx(1) - idx(2)) == 1
%         set(lines(idx), 'Color', 'b');
%         circle = hObject.Children.Children(1);
%         radius = norm([circle.Children.XData(1)-0.5, circle.Children.YData(1)-0.5]);
%         A_slope = (lines(idx(1)).YData(2) - lines(idx(1)).YData(1)) / (lines(idx(1)).XData(2) - lines(idx(1)).XData(1));
%         B_slope = (lines(idx(2)).YData(2) - lines(idx(2)).YData(1)) / (lines(idx(2)).XData(2) - lines(idx(2)).XData(1));
%         if isinf(A_slope)
%             [xa,ya] = linecirc(A_slope,0.5,0.5,0.5,radius);
%         else
%             A_intercpt = (-0.5 * A_slope) + 0.5;
%             [xa,ya] = linecirc(A_slope,A_intercpt,0.5,0.5,radius);
%         end
%         if isinf(A_slope)
%             [xb,yb] = linecirc(B_slope,0.5,0.5,0.5,radius);
%         else
%             B_intercpt = (-0.5 * B_slope) + 0.5;
%             [xb,yb] = linecirc(B_slope,B_intercpt,0.5,0.5,radius);
%         end
%     else
%         errordlg('Calculation faied','Invalid indexes')
%     end
end