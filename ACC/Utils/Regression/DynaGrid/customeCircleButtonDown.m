function customeCircleButtonDown(hObject, eventdata)
hObject.Color = [0.5 0.5 0.5];
f = hObject.Parent.Parent.Parent;
f.Pointer = 'hand';
if isempty(f.UserData.oldPosition)
    f.UserData.oldPosition = f.Children.CurrentPoint(1, 1:2);
end
length(f.Children.Children)
f.UserData.SelectedObject = length(f.Children.Children)-1;
f.WindowButtonMotionFcn = @testCircleWindowButtonMotionFcn;
