function testCircleWindowButtonMotionFcn(hObject,eventdata)

origo = hObject.UserData.origo;
oldPosition = hObject.UserData.oldPosition;
currentPosition = hObject.Children.CurrentPoint(1, 1:2);

scaler = (norm(origo-currentPosition))/(norm(origo-oldPosition));
circleTrafo(hObject.Children.Children(1).Children, [], origo, scaler);
