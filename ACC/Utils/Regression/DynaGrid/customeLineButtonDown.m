function customeLineButtonDown(hObject, eventdata)
hObject.Color = [0.5 0.5 0.5];
origo = [0.5 0.5];
f = hObject.Parent.Parent;
f.Pointer = 'hand';
idx = str2num(hObject.Tag);
f.UserData.SelectedObject = idx;
if idx == length(f.Children.Children(2:end))-1 % last element
    z = idx; % first element
    w = 1; % last before element
elseif idx == 0 % initial element
    z = -1; % firts element
    w = -length(f.Children.Children(2:end)); % last element
else % somewhere in the middle
    z = -1; % next element
    w = 1; % previous element
end
if ~strcmp(hObject.Tag, '0')
    preCoor = [f.Children.Children(end-idx+w).XData(2), f.Children.Children(end-idx+w).YData(2)];
    postCoor = [f.Children.Children(end-idx+z).XData(2), f.Children.Children(end-idx+z).YData(2)];
    selectedCoor = [hObject.XData(2), hObject.YData(2)];
    preTheta = calcTheta(selectedCoor, preCoor, origo);
    postTheta = calcTheta(selectedCoor, postCoor, origo);
    f.UserData.window = [hObject.UserData.theta - preTheta, hObject.UserData.theta + postTheta];
    f.UserData.window
end
f.WindowButtonMotionFcn = {@testLineWindowButtonMotionFcn};
