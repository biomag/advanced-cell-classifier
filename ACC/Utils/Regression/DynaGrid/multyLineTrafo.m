function multyLineTrafo(hObject, eventData, origo, phi)

T1xy = eye(3);
T1xy(1:2, 3) = -origo;

Rxy = [cos(phi) sin(phi) 0;...
      -sin(phi) cos(phi) 0;
         0         0     1];

T2xy = eye(3);
T2xy(1:2, 3) = origo;

XDarray = cell2mat(get(hObject,'XData'));
YDarray = cell2mat(get(hObject,'YData'));
oldCoor = [XDarray(:,2)'; YDarray(:,2)'; ones(1,length(XDarray(:,2)))];

trafoMat = T2xy*Rxy*T1xy;

newCoor = trafoMat * oldCoor;

XDarray(:,2) = newCoor(1,:);
YDarray(:,2) = newCoor(2,:);

set(hObject,{'XData'}, mat2cell(XDarray, ones(40, 1), 2));
set(hObject,{'YData'}, mat2cell(YDarray, ones(40, 1), 2));

