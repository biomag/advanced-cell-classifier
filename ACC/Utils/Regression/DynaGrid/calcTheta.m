function theta = calcTheta(v1,v2,origo)
% works only vectors 1 unit of magnitude


sv1 = [v1 - origo, 0];
sv2 = [v2 - origo, 0];

t_1 = asin(sv1(1)) * (sv1(2)/abs(sv1(2)));
t_2 = asin(sv2(1)) * (sv2(2)/abs(sv2(2)));

theta = t2-t1;