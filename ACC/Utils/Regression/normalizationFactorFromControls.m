function [ averageOfControls ] = normalizationFactorFromControls(densityOneByOne,controlRows,controlCols)
% AUTHOR:   Abel Szkalisity
% DATE:     March 9, 2017
% NAME:     normalizationFactorFromControls
%
% Calculates the normalization factor from control wells, more precisely
% the average [volume of density estimations] of control wells.
% NOTE: the well-based approach works with unstructured data (the
% well-unstructured data map is provided during the general report
% generation)
%
% INPUT:
%   densityOneByOne To each well the estimated density.
%   controlRows     Row indices of the control wells.
%   controlCols     Column indices of the control wells.
%   
%
% OUTPUT:
%   averageOfControls The average volume of the wells identified by the
%                   controls.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


controlVolumes = zeros(1,length(controlRows));
plateSize = size(densityOneByOne);
k = 1;
%select out the control well IDs.
for i=1:numel(densityOneByOne)
      [currentR,currentC] = ind2sub(plateSize,i);
      for j=1:length(controlRows)
          if currentR == controlRows(j) && currentC == controlCols(j)
              controlVolumes(k) = sum(densityOneByOne{i}(:));
              k = k+1;
          end
      end
end

averageOfControls = mean(controlVolumes);


end

