function plotOfPlotsDREDVIZ( wellIcons, densityOneByOne, tgtFolder,tmpFolder, plateNumber,idx,controlRows,controlCols,plotOfPlots)
% AUTHOR:   Abel Szkalisity
% DATE:     Oct 13, 2016
% NAME:     plotOfPlotDREDVIZ
%
% This function creates all neccessarry files for the meta-visualization of
% the well heatmaps and saves the result into an image. It runs the DREDVIZ
% via system call. Finally it examines what is the variance of a given
% treatment in the meta-visualization positions of the heatmap.
%
% INPUT:
%   wellIcons       A cellarray with heatmap images for all wells. The
%                   heatmap is a density representation of the regression
%                   plane in that well.
%   densityOneByOne Also a cellarray same size as the plate, with a true
%                   density estimation in it. (not like in wellIcons where
%                   we request for representative images)
%   tgtFolder       The target folder to where all the results are saved.
%                   This target folder is always marked with the plate name
%                   and with a timestamp.
%   tmpFolder       The folder to store internal files such as the dredviz
%                   outputs or the generated fake layout.
%   plateNumber     The index of the selected plate within the
%                   CommonHandles structure.
%   idx             Index of the regression to which the report is made.
%                   Neccessary for naming.
%   controlRows     The indices of row of the control well.
%   controlCols     The indices of column of  the control well.
%   plotOfPlots     A structure as described in regressionReportForPlate
%                   describing which methods to run and the plate layouts.
%
% OUTPUT:
%   The files will be written to the projectFolder [(CommonHandles.DirName)
%   filesep basefolder].
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%Turn off latex interpreter in this function
storedTextInterpreter = get(0,'DefaultTextInterpreter');
set(0,'DefaultTextInterpreter','none');

[wellIDs, wellAnnotation] = acquirePlateLayout(tmpFolder,plateNumber,plotOfPlots.wellIDs,plotOfPlots.wellAnnotation);

selectedULMethods = plotOfPlots.selectedULMethods;

[ averageOfControls ] = normalizationFactorFromControls(densityOneByOne,controlRows,controlCols);

infoWindow = msgbox('Visualization methods...');
infoWindow.Children(1).Visible = 'off'; pause(0.1); %turn off the button

%do the normalization
for i = 1:numel(densityOneByOne)
    densityOneByOne{i} = densityOneByOne{i} ./ averageOfControls;
    if any(any(isnan(densityOneByOne{i}) | isinf(densityOneByOne{i})))
        if ishandle(infoWindow), close(infoWindow); end
        error('ACC:Density','NaN or Inf values were produced by the density calculation. Operation is terminated.');
    end
end

%Flatten out the densities and assign the corresponding well annotations (this is the soul of all the stuff that coming after)
[ flattenedByWell, annotationByWell,...
    perWellHeatmapInLine, perWellKDEInLine,...
    flattenedByDrugs, drugNames, replicateIndices ]...
    = flattenWell( densityOneByOne, wellIDs, wellAnnotation, wellIcons );

%Save the annotations output to the temp folder so that it can be reloaded
save(fullfile(tmpFolder,'annotationByWell.mat'),'annotationByWell','perWellHeatmapInLine','perWellKDEInLine');
iconsUsedForMetaVis = askUserForIconsToUse(perWellHeatmapInLine,perWellKDEInLine,plotOfPlots.iconToUse);

distanceMetric = @symmetricKLDforPdist;%'correlation';%'seuclidean';%'euclidean';%
if isa(distanceMetric,'function_handle')
    fnHandleInfo = functions(distanceMetric);
    distanceMetricString = fnHandleInfo.function;
else
    distanceMetricString = distanceMetric;
end
clusterDistanceMetric = 'correlation';

%Info for users to file
fmetaData = fopen(fullfile(tgtFolder,'ResultAndPredictor_MetaData.txt'),'a');
fprintf(fmetaData,'\nThe used distance metric for meta-visualization was: %s\n\tfor clustering: %s\n',distanceMetricString,clusterDistanceMetric);
fclose(fmetaData);


%Run dredviz
if any(ismember(selectedULMethods,'NeRV'))
    distanceOnWells = squareform(pdist(flattenedByWell,distanceMetric));
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'NeRV is creating meta-visualization...'; pause(0.1); end
    try
       nofDrugs = length(drugNames);
       positions_NeRV = runDredviz(distanceOnWells, tmpFolder, plateNumber, idx, nofDrugs);
       if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'Creating and saving NeRV figure...'; pause(0.1); end
       createPlotOfPlotsPDF(positions_NeRV,tgtFolder,iconsUsedForMetaVis,replicateIndices,drugNames,'NeRV');
    catch e
       if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'The plot of plots could not run on your machine due to problems with NeRV!'; pause(2); end       
       disp(getReport(e));      
    end
    
end
    
%T-SNE
if any(ismember(selectedULMethods,'T-SNE'))
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'T-SNE is creating meta-visualization...'; pause(0.1); end
    [tsne_positions] = tsne(flattenedByWell,'Distance',distanceMetric);
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'Creating and saving T-SNE figure...'; pause(0.1); end
    createPlotOfPlotsPDF(tsne_positions,tgtFolder,iconsUsedForMetaVis,replicateIndices,drugNames,'T-SNE');
    tsne_coordinateFileNameWithLocation = createCoordinateFile(tmpFolder,plateNumber,idx,'T-SNE');    
    putPositionsToFile(tsne_coordinateFileNameWithLocation,tsne_positions);
end

%PCA
if any(ismember(selectedULMethods,'PCA'))
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'PCA is creating meta-visualization...'; pause(0.1); end
    [~,pcaScore] = pca(flattenedByWell);
    pca_positions = pcaScore(:,[1 2]);
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'Creating and saving PCA figure...'; pause(0.1); end
    createPlotOfPlotsPDF(pca_positions,tgtFolder,iconsUsedForMetaVis,replicateIndices,drugNames,'PCA');
    pca_coordinateFileNameWithLocation = createCoordinateFile(tmpFolder,plateNumber,idx,'PCA');    
    putPositionsToFile(pca_coordinateFileNameWithLocation,pca_positions);
end

if any(ismember(selectedULMethods,'Hierarchical clustering'))
    if ishandle(infoWindow), infoWindow.Children(2).Children(1).String = 'Clustering treatments...'; pause(0.1); end
    createAndSaveClustergram(...
        flattenedByDrugs,drugNames,tgtFolder,distanceMetric,clusterDistanceMetric,...
        'Treatments comparison (pairwise distance)',...
        'treatmentComparison_Distances');
end

%restore text interpreter
set(0,'DefaultTextInterpreter',storedTextInterpreter);

if ishandle(infoWindow), close(infoWindow); end

end


function createAndSaveClustergram(data,drugNames,outputFolder,distanceMetric,clusterDistanceMetric,titleString,fileString)
%crateAndSaveClustergram
%   Creates and save a clustergram with paper size out of the data
%   matrix which contains pairwise distances between drugs. The
%   leaveOutDrugIndices is a 0/1 array specifying which drugs should be
%   excluded from the analysis.     
    for i=1:length(drugNames)
        drugNames{i} = strrep(drugNames{i},'_','-');
    end       
    pairwiseDistanceData = squareform(pdist(data,distanceMetric));  
    if any(any(isinf(pairwiseDistanceData)))
        warndlg('Clustergram contains "inf" values. Cannot calculate clustergram.')
        return
    end
    cg = clustergram(pairwiseDistanceData,...
        'Symmetric',false,...
        'RowLabels',drugNames,...         
        'ColumnLabels',drugNames,...
        'Colormap',parula,...        
        'RowPDist',clusterDistanceMetric,...
        'ColumnPDist',clusterDistanceMetric...
        );
    cg.RowLabelsRotate = 0;
    cg.ColumnLabelsRotate = 90;
    
    set(0,'ShowHiddenHandles','on');
    allhnds = get(0,'Children');
    %cgfigidx = find();
    cffighnd = allhnds(strcmp('Clustergram',get(allhnds,'Tag')));    
        
    title(titleString);
    set(cffighnd,'PaperType','<custom>','PaperSize',[20 20],'PaperUnits','centimeters');
    if compareVersions('9.0.0.341360 (R2016a)',version)
        print(cffighnd,[outputFolder filesep fileString],'-dpdf','-fillpage');
        savefig(cffighnd,[outputFolder filesep fileString],'compact');
    else
        print(cffighnd,[outputFolder filesep fileString],'-dpdf');
        savefig(cffighnd,[outputFolder filesep fileString]);
    end
    if ishandle(cffighnd), close(cffighnd); end    
    set(0,'ShowHiddenHandles','off');

end
%}

function positions = runDredviz(distanceOnWells, tmpFolder, plateNumber, idx, nofDrugs)
%The restructured run of dredviz

    global CommonHandles;
    
    %Open the txt for the pairwise distances.
    pairwiseWellDistanceFileNameWithLocation = [tmpFolder filesep CommonHandles.PlatesNames{plateNumber} '_pairwiseWellDistance_class' num2str(idx,'%02d') '.txt'];
    coordinateFileNameWithLocation = createCoordinateFile(tmpFolder,plateNumber,idx,'NeRV');

    %print out to the pairwise file the matrix in the SOM_PAK format
    %supported by the software DREDVIZ.
    pairwiseWellDistanceFID = fopen(pairwiseWellDistanceFileNameWithLocation,'w');
    fprintf(pairwiseWellDistanceFID,'%d\n',size(distanceOnWells,2));
    for i=1:size(distanceOnWells,1)
        for j=1:size(distanceOnWells,2)
            fprintf(pairwiseWellDistanceFID,'%f ',distanceOnWells(i,j));
        end
        fprintf(pairwiseWellDistanceFID,'\n');
    end

    fclose(pairwiseWellDistanceFID);

    %DREDVIZ requires a closed file

    %TODO: call dredviz with system call, if not available try to make it,
    %if cannot make ask for the location and name of the make program, and
    %if even that does not help, then revert operation.

    %********* WRITE YOUR CODE HERE
    %CUSTOM SOLUTION:
    calledPath = strsplit(mfilename('fullpath'),filesep);
    calledPath = calledPath(1:end-4);
    nervPath = [strjoin(calledPath,filesep) filesep 'LIBS' filesep 'dredviz' filesep];
    
    if ispc
        system([nervPath ...
            'nerv_win --inputdist ' pairwiseWellDistanceFileNameWithLocation...
            ' --seed 1 ' ...    
            ' --outputfile ' coordinateFileNameWithLocation ...        
            ' --neighbors ' num2str(ceil(size(distanceOnWells,1)/nofDrugs))...
            ]);
    elseif ismac
        system([nervPath ...
            'nerv_mac --inputdist ' pairwiseWellDistanceFileNameWithLocation...
            ' --seed 1 ' ...    
            ' --outputfile ' coordinateFileNameWithLocation ...        
            ' --neighbors ' num2str(ceil(size(distanceOnWells,1)/nofDrugs))...
            ]);        
    elseif isunix
        system([nervPath ...
            'nerv_linux --inputdist ' pairwiseWellDistanceFileNameWithLocation...
            ' --seed 1 ' ...    
            ' --outputfile ' coordinateFileNameWithLocation ...        
            ' --neighbors ' num2str(ceil(size(distanceOnWells,1)/nofDrugs))...
            ]);        
    else
        disp('Uknown platform for DREDVIZ.');
    end
    %
    %UNTIL HERE %%%%%%%%%%%%%%%%%%%%%%%%
    
    %assuming that we have the required file to the target folder and we named it as
    % [plateName]_metaVisualizationCoordinates_class##.txt where ## is the idx.
    % The assumption is true if nerv run correctly.

    coordinateFile = fopen(coordinateFileNameWithLocation,'r');
    fullFile = fscanf(coordinateFile,'%f');    
    fclose(coordinateFile);    
    
    if any(isnan(fullFile))
        error('ACC:DREDVIZ:NaN','DREDVIZ provided NaNs as output positions. Try to increase heatmap resolution');
    end
    columnNumber = fullFile(1); %this should be 2 if everything is correct    
    positions = reshape(fullFile(2:end),columnNumber,[]);
    positions = positions';

end

function [wellIDs, wellAnnotation] = acquirePlateLayout(tmpFolder,plateNumber,wellIDs,wellAnnotation)
%Function to produce 2 cell arrays with equal length. The first one
%contains the wellIDs such as A01, B03 etc, the wellAnnotation is the
%corresponding label
global CommonHandles;
    if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col>0 %if the data is structured
        %Ask for the plate-layout:        
        if ~isempty(wellIDs)
            %overwrite the generated platelayout so that from now on the
            %platelayout is stored in a unique format and at a unique place
            generatedPlateLayoutFID = fopen([tmpFolder filesep CommonHandles.PlatesNames{plateNumber} '_generatedPlateLayout.csv'],'w');
            fprintf(generatedPlateLayoutFID,'%s,%s\n','WellID','Annotation');
            for i=1:length(wellIDs)
                fprintf(generatedPlateLayoutFID,'%s,%s\n',wellIDs{i},wellAnnotation{i});
            end
            fclose(generatedPlateLayoutFID);
        end        
    end
    if isempty(wellIDs)
        [ wellIDs, wellAnnotation ] = csvPlateLayoutToWellsAndAnnotation( fullfile(tmpFolder,[CommonHandles.PlatesNames{plateNumber} '_generatedPlateLayout.csv']));
    end
end

function coordFileName = createCoordinateFile(tmpFolder,plateNumber,idx,type)
    global CommonHandles
    coordFileName = [tmpFolder filesep CommonHandles.PlatesNames{plateNumber} '_metaVisualizationCoordinates_class' num2str(idx,'%02d') '_' type '.txt'];
end

function putPositionsToFile(coordinateFileNameWithLocation,positions)
    coordFile = fopen(coordinateFileNameWithLocation,'w');
    fprintf(coordFile,'%d\n',2);
    for i=1:size(positions,1)
        fprintf(coordFile,'%d %d\n',positions(i,1),positions(i,2));
    end
    fclose(coordFile);
end
