function SI = sparsityIndex(distanceMatrix, innerNodes, leaveOutNodes)
% AUTHOR:   Abel Szkalisity
% DATE:     Nov 3, 2016
% NAME:     sparsityIndex
%
% Calculates a statistic which targets the compactness of a set of objects
% compared to all the other objects. We want to see if in a set of objects
% the intra-distance is much smaller than the inter-distance.
%       We take the mean of distances between all points within the
%       innerNodes and divide by the mean of distances over all the
%       distances between innerNodes and the remaining objects except for
%       the leaveOutNodes.
%
% INPUT:
%   distanceMatrix  An nxn matrix with pariwise distances between objects.
%                   The diagonal should be 0 and the matrix should be
%                   symmetric.
%   innerNodes      An array of indices indentifying the set of objects for
%                   which we calculate the 'sparsity index'. All indices
%                   must be between 1 and n inclusive.
%   leaveOutNodes   Array of indices to leave out from the statistics. This
%                   might be needed to exclude outliers from the analysis.
%
% OUTPUT:
%   SI              The sparsity index.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

n = size(distanceMatrix,1);
innerMatrix = distanceMatrix(innerNodes,innerNodes);
m = length(innerNodes);
indexMatrix = tril(innerMatrix,-1) ~= 0;
innerMeanDistance = sum(innerMatrix(indexMatrix)) / ( m*(m-1)/2); % mean implementation. With this we also cover the unlikely case when 2 objects are exactly at the same position
outerNodes = 1:n;
outerNodes = setdiff(outerNodes,union(innerNodes,leaveOutNodes));
outerMatrix = distanceMatrix(innerNodes,outerNodes);
outerMeanDistance = mean(outerMatrix(:));
SI = innerMeanDistance / outerMeanDistance;

end