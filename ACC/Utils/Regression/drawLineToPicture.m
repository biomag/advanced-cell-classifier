function [ imageWithLine ] = drawLineToPicture( image, p1,p2, colorCode )
% AUTHOR:   Abel Szkalisity
% DATE:     August 1, 2016
% NAME:     drawLineToPicture
%
% Given two points it draws a line from point p1 to point p2 with the rgb
% color colorCode. The points' coordinates are in the coordinate system of
% the picture matrix. If any of the points is out of the image coordinates
% then the original picture is returned.
%
% INPUT:
%   image           An image to draw the line
%   p1,p2           Two element vectors the end points of the line section
%   colorCode       3 element vector [0,1] for the color in RGB.
%
% OUTPUT:
%   imageWithLine   The image with the line.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

i1 = size(image,1);
i2 = size(image,2);

%colorCode = colorCode * double(max(image(:)));

if size(image,3)~=3  
    imageWithLine = repmat(image,[1,1,3]);
else
    imageWithLine = image;
end
flip = 0;

%if the point is within the image
if p1(1)>=1 && p1(2)>=1 && p2(1)>=1 && p2(2)>=1 && p1(1)<=i1  && p2(1)<=i1 && p1(2)<=i2  && p2(2)<=i2
    %swap the points to have p1 at the front
    if p1(1)>=p2(1)
        buff = p1;
        p1 = p2;
        p2 = buff;
    end
        
    if (p2(1)-p1(1)~=0)
    %if the line is not vertical
        slope = (p2(2)-p1(2))/(p2(1)-p1(1));
        if slope>1 || slope<-1         
            i2 = i1;            
            buff = p1(2);
            p1(2) = p1(1);
            p1(1) = buff;
            buff = p2(2);
            p2(2) = p2(1);
            p2(1) = buff;
            slope = 1./slope;
            flip = 1;
            %swap the points to have p1 at the front also after the flip
            if p1(1)>=p2(1)
                buff = p1;
                p1 = p2;
                p2 = buff;
            end        
        end        
        %x and y are in matrix meaning        
        x = p1(1):p2(1);
        lx = length(x);
        y = round( (x-p1(1))*slope + p1(2));
        y(y==0) = 1;
        y(y==i2+1) = i2; 
        if flip
            buff = x;
            x = y;
            y = buff;
        end        
        v = convert2RowVector(x);
        repV = repmat(v,1,3);
        x = repV(:)';        
        v = convert2RowVector(y);
        repV = repmat(v,1,3);
        y = repV(:)';   
        try
        colorIndices = sub2ind(size(imageWithLine),x,y,repmat([1 2 3],1,lx));
        imageWithLine(colorIndices) = repmat(colorCode,lx,1);        
        catch
            disp('error')
        end
    else    
    %if its vertical (because of the rotation it seems horizontal on the picture)
    
        %the p1 is lower
        if p1(2)>=p2(2)
            buff = p1;
            p1 = p2;
            p2 = buff;
        end
        for i=p1(2):p2(2)
            imageWithLine(p1(1),i,:) = colorCode;
        end
    
    end    
end

end

