function [ densityFunction ] = densityEstimationByGaussians(out,x1,x2,sigm)
% AUTHOR:   Abel Szkalisity
% DATE:     Aug 4, 2016
% NAME:     densityEstimationByGaussians
%
% Estimates density by placing Gaussian pdf to the out positions.
%
% WARNING!!!! This function gives back the density estimation in a flipped
% position regarding to the vertical axes. (first dimension of the matrix)
% E.g. the out = [100,100] will be in the UPPER left corner instead of the
% DOWN left corner.
% NOTE: Due to numerical issues the normal density is estimated on the 0,1
% scale instead of the internal regression 0,1000. EVERYTHING IS DOWNSCALED
% BY 1000 after input to this function.
%
% INPUT:
%   out             A 2 by n matrix containing the basis points for the
%                   estimation
%   x1              A vector which determines the evaluation points in the
%                   first dimension (here we're thinking as a usual
%                   coordinate system: first dimension is the horizontal
%                   second is the vertical)
%   x2              The same for the second dimension
%
% OUTPUT:
%   densityFunction A matrix sized according to the length of x1 and
%                   x2. It represents the density function. The matrix
%                   first dimension is sized by x2 and its second
%                   dimension is x1.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[X1,X2] = meshgrid(x1/1000,x2/1000);
F = zeros(length(x2),length(x1));

if nargin<4
    sigm = 1;
end
for i=1:size(out,2)
    mu = [out(1,i) out(2,i)]/1000;
    if isscalar(sigm)
        Sigma = [sigm 0;0 sigm];
    else
        Sigma = [ sigm(i,1) 0;0 sigm(i,2)];
    end    
    F2 = mvnpdf([X1(:) X2(:)],mu,Sigma);
    F2 = reshape(F2,length(x2),length(x1));
    F = F + F2;        
end

densityFunction = F;

end

