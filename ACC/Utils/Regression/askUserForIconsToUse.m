function [iconsUsedForMetaVis] = askUserForIconsToUse(perWellHeatmapInLine,perWellKDEInLine,answer)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     askUserForIconsToUse
%
% Short util finction to select which icon to use (KDE or Heatmap). It
% requires both cellarrays as parameters and returns only the one which was
% selected by string
%
% INPUT:
%   perWellHeatmapInLine    This is a flattened out cellarray (cell vector)
%                   of the well prediction heatmaps
%   perWellKDEInLine    This is a flattened out cellarray (cell vector)
%                   of the well prediction kernel density estimations
%   answer          String for the selection
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if ~isempty(answer)
    switch answer
        case 'Heatmaps (histograms)'
            iconsUsedForMetaVis = perWellHeatmapInLine;
        case 'Kernel density estimations (KDE - smoother)'
            iconsUsedForMetaVis = perWellKDEInLine;
    end
else
    iconsUsedForMetaVis = perWellHeatmapInLine;
end
