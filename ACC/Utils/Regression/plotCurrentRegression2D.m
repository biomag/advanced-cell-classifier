function out = plotCurrentRegression2D(f, bins)
% AUTHOR:   Abel Szkalisity, Peter Horvath
% DATE:     July 11, 2016
% NAME:     plotCurrentRegression2D
%
% Plots to a heatmap the regression plane prediction.
% Modification 2019.08.06.: Normalization and projection to 1-32 was
% removed.
% 
% INPUTS:
%   f               A 2xn matrix with the predicted positions on reg plane
%   bins            The number of cells (bins) for the heatmap (resolution)
%
% OUTPUT:
%   out             An image sized bins+2 x bins+2, the heatmap
%   
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for M1olecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
% f: 2 x n matrix

% bins = 10;

%normalize data:
f = f./1000;

% format data
f(f<0) = 0;
f(f>1) = 1;

hist2d = zeros(bins);

stepS = 1/bins;

for i=1:size(f, 2)

    b1 = floor(f(1, i) / stepS) + 1;
    b2 = floor(f(2, i) / stepS) + 1;
    if b1 > bins, b1 = bins; end
    if b2 > bins, b2 = bins; end
    
    hist2d(bins-b2+1,b1) = hist2d(bins-b2+1,b1) + 1;
    
end

out = hist2d;