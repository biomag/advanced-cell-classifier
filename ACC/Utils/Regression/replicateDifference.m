function d = replicateDifference( distanceMatrix, replicates1, replicates2 )
% AUTHOR:   Abel Szkalisity
% DATE:     Nov 07, 2016
% NAME:     replicateDifference
%
% A difference measure between point sets in the plane. Given a pairwise
% distance matrix the 2 set is identified by indices within the
% distanceMatrix. We calculate the mean distance within the sets and
% between the sets. The resulting value is MO^2 / (MI1*MI2), where MO is
% the mean distance over all pairs of objects (o1,o2) where oi must be the
% part of the ith set, and MIk is the mean distance over all pairs (o1,o2)
% where both o1 and o2 is part of the kth set.
%
% INPUT:
%   distanceMatrix  An nxn matrix with pariwise distances between objects.
%                   The diagonal should be 0 and the matrix should be
%                   symmetric.
%   replicates 1-2  These are arrays of indices identifying the 2 sets to
%                   be compared.
%
% OUTPUT:
%   d               Difference measure
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

innerD1 = distanceMatrix(replicates1,replicates1);
innerD2 = distanceMatrix(replicates2,replicates2);
outerD = distanceMatrix(replicates1,replicates2);

innerD1 = innerD1(tril(innerD1,-1) ~= 0);
innerD2 = innerD2(tril(innerD2,-1) ~= 0);

m1 = length(replicates1);
m2 = length(replicates2);
if m1 ~= 1
    MI1 = sum(innerD1) / (m1*(m1-1)/2);
else
    MI1 = 1;
end
if m2 ~= 1
    MI2 = sum(innerD2) / (m2*(m2-1)/2);
else
    MI2 = 1;
end


MO = mean(outerD(:));

d = MO^2 / (MI1*MI2);

end

 