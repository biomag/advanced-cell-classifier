function [ flattenedByWell, annotationByWell, flattenedWellIcons, flattenedWellKDEs, flattenedByDrug, drugNames, replicateIndices ] = flattenWell( densityOneByOne, wellIDs, wellAnnotation, wellIcons)
% AUTHOR:   Abel Szkalisity
% DATE:     May 23, 2017
% NAME:     flattenWell
%
% Flattens out both the contents of the densityOneByOne and its matrix
% form. It also generates a similar format for the drugs. The function
% excludes the empty wells from the analysis. Also if a well is completely
% empty for a drug that drug is removed from the list.
%
% INPUT:
%   densityOneByOne A well-formatted (like 96 well, 24 well etc.) matrix.
%                   Each entry is a density estimation of the regression
%                   plane for that well.
%   wellIDs         The wellIDs for the annotation
%   wellAnnotation  The annotation labels (matching with wellIDs)
%   wellIcons       The heatmaps is well-format.
%
% OUTPUT:
%   flattenedByWell A matrix that contains the flattened version of the
%                   densities.
%   annotationByWell Information about the given well.
%   flattenedWellIcons The flattened version of the wellIcons (flatten here
%                   means that the wells are organized into a vector not in
%                   the plate format)
%   flattenedWellKDE The Kernel density estimations organized similarly as
%                   the previous output.
%   flattenedByDrug The flattened out densities averaged over the
%                   replicates
%   drugNames       The information about the drug (label)
%   replicateIndices A cellarray as lons as many drugs remained in the
%                   final state (after leaving out those specified in this
%                   file in leaveOutDrugNames and after filtering the ones
%                   that produced empty regression planes)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

%Collect general info
[drugNames,~,destination] = unique(wellAnnotation);
nofDrug = length(drugNames);
replicateIndices = cell(nofDrug,1);
N = numel(densityOneByOne);

%% Leave out drugs
%Here is the possibility to delete some drugs from the analysis. List in a
%cellarray the strings to be excluded. They all removed.
leaveOutDrugNames = {};
%leaveOutDrugNames = {'QD'};
%leaveOutDrugNames = {'QD','ATGL','CD300LG_A','CD300LG_C','CD300LG_G','TM4SF5_G'};
for i=1:nofDrug
    for j=1:length(leaveOutDrugNames)
        %If the i'th drug is needs to be discarded
        if strcmp(drugNames{i},leaveOutDrugNames{j})
            %Collect the wellIDs
            possibleWellIDs = wellIDs(destination == i);                                               
            for jj=1:length(possibleWellIDs)                
                [r,c] = wellId2Index(possibleWellIDs{jj});                
                %Delete the content of those wells
                densityOneByOne{r,c} = [];                                    
            end                     
        end
    end
end

%% Determine the flattened by well set

annotationByWell = cell(1,N);
flattenedWellIcons = cell(1,N);
flattenedWellKDEs = cell(1,N);
densityIndexMatrix = zeros(size(densityOneByOne));
k = 1;
for j=1:size(densityOneByOne,2)
    for i=1:size(densityOneByOne,1)
        if ~isempty(densityOneByOne{i,j})
            densityIndexMatrix(i,j) = k;
            %Here we store that the k'th element of the flattened set                
            wellMat = cell2mat(wellIDs);
            currentWellID = ['A'-1+i num2str(j,'%02d')];
            searchMatrix = repmat(currentWellID,size(wellMat,1),1);
            matchMatrix = searchMatrix == wellMat;
            s = sum(matchMatrix,2);
            annotationByWell{k} = [wellAnnotation{s==3} ' - ' sprintf('%c','A'-1+i) sprintf('%02d',j)];
            flattenedWellIcons{k} = wellIcons{i,j};
            flattenedWellKDEs{k} = flipud(densityOneByOne{i,j});
            k=k+1;                
        end
    end
end

annotationByWell(k:end) = [];
flattenedWellIcons(k:end) = [];
flattenedWellKDEs(k:end) = [];


%% Clear out empty drugs.
%This will clear out both the leave out drugs and the ones that are empty
i = 1;
iAll = 1;
while i<=nofDrug                
    %filter for the possible wells
    possibleWellIDs = wellIDs(destination == iAll);
    %array to store the replicate positions.
    replicateIndices{i} = zeros(1,length(possibleWellIDs));
    k = 1;
    %If the well is not empty
    for j=1:length(possibleWellIDs)
        [r,c] = wellId2Index(possibleWellIDs{j});
        if ( densityIndexMatrix(r,c)~=0 )
            replicateIndices{i}(k) = densityIndexMatrix(r,c);
            k = k+1;
        end
    end
    %remove extra lines from replicatePos.
    replicateIndices{i}(k:end) = [];

    %It can happen that the currect drug is completely lacking cells in
    %the current regression class, therefore we need to exclude it.
    if isempty(replicateIndices{i})
        %exclude this drug from further analysis        
        nofDrug = nofDrug - 1; % decrease drug number
        drugNames(i) = []; % delete drug names
    else        
        i = i+1;
    end
    iAll = iAll + 1;
end
replicateIndices(i:end) = [];

%% Flatten
flattenedByWell = cell(N,1);
flattenedByDrug = cell(nofDrug,1);

%Flatten out
for i=1:N
    %flip the horizontal axis (this is needed because of the note in
    %calculateShiftVectors, i.e. that densityOneByOne stores the data
    %flipped accroding to the first axis). Also don't  transpose back so
    %that the column wise (:) operator is actually becomes row wise
    origDens = fliplr(densityOneByOne{i}'); 
    flattenedByWell{i} = origDens(:)';
end
flattenedByWell = flattenedByWell(~cellfun(@isempty,flattenedByWell));
flattenedByWell = cell2mat(flattenedByWell);

for i=1:nofDrug
    flattenedByDrug{i} = mean(flattenedByWell(replicateIndices{i},:),1);
end
flattenedByDrug = cell2mat(flattenedByDrug);


end

