function createRegressionHTML(results, plateName, targetFolder,whiteImg,wellIcons)
% AUTHOR:   Abel Szkalisity
% DATE:     July 11, 2016
% NAME:     createRegressionHTML
%
% CreateHTML function makes an HTML file on which you can see the results of
% the regression. The results is a huge cell array representing the plate
% and in every cell in it there is a long 2xn array which has all the
% predicted regression coordinates for that well.
%
% INPUT:
%   results         A cellarray by the format of the plate.
%   plateName       The name of the plate
%   targetFolder    The full path of the folder to predict.
%   whiteImg        bool value indicating whether we want to include
%                   thumbnails in the html creation. In this case we use
%                   white backgrounded image the creation of which might
%                   take long, or simply we use original images.
%   wellIcons       Cellarray same size as results. Contains the heatmaps
%                   to plot.
%
% OUTPUT:
%   The files in the [targetFolder filesep HTML] folder.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

htmlFolder = [targetFolder filesep 'HTML'];
if ~exist(htmlFolder,'dir')
    mkdir(htmlFolder);
end

%create whiteBgImages
if whiteImg
    %Create images to show
    if ~exist([htmlFolder filesep 'thumbs'],'dir')
        mkdir([htmlFolder filesep 'thumbs']);
    end
    
    inputFolder = [CommonHandles.DirName filesep plateName filesep CommonHandles.OriginalImageFolder filesep];
    outFolder = [htmlFolder filesep 'thumbs' filesep];

    fileList = dir([inputFolder '*' CommonHandles.ImageExtension]);

    for i=1:numel(fileList)

        im = imread([inputFolder fileList(i).name]);
        
        totalImageName = fileList(i).name;
        [~,onlyImageName,~] = fileparts(totalImageName);

        %imwrite(out, [outputFolder SSIN{1} '_' SSIN{2} '_' SSIN{3} '.jpg']);

        out = createWhiteBgImg(im);

        out = imresize(out, .2);

        imwrite(out, [outFolder filesep onlyImageName '.jpg']);

    end
end

%make folder for the heatmaps
mkdir([htmlFolder filesep 'icons']);

% generate small images
[xs, ys] = size(results);

for i=1:ys
    for j=1:xs                         
        iconImg = imresize(wellIcons{i,j}, [40 40], 'nearest');
        
        fileName = sprintf('dist_%c%0.2d.bmp', i + 'A' - 1, j);
        
        imwrite(uint8(iconImg*255), [htmlFolder filesep 'icons' filesep fileName]);
    end    
end


% write the html

htmlOut = fopen([htmlFolder filesep plateName '.html'], 'w');

% header
fprintf(htmlOut, '<html>\n<head>\n<link rel="stylesheet" media="all" type="text/css" href="regressionPlateCSS.css" />\n</head>\n');

% write col numbers
fprintf(htmlOut, '<table border="0">\n<tr>\n<td> </td>');
for j=1:xs 
    fprintf(htmlOut,'\t<td>%d</td>\n', j); 
end
fprintf(htmlOut, '</tr>\n');

% put the icons
for i=1:ys
    fprintf(htmlOut, '<tr>\n');
    for j=0:xs         
        if j==0
            fprintf(htmlOut,'\t<td>%c</td>\n', i + 'A' - 1); 
        else
            fileNameForHeatmap = sprintf('dist_%c%0.2d.bmp', i + 'A' - 1, j);
            %get an image from the selected well
            row = sprintf('%c',i + 'A' - 1);
            colString = sprintf('%0.2d',j);
            FileNamePrefix =  [CommonHandles.DirName filesep plateName filesep  CommonHandles.OriginalImageFolder filesep plateName '*_w' row colString '*'];
            ImageFiles = dir(FileNamePrefix);
            if ~isempty(ImageFiles)
                onlyImageName = ImageFiles(1).name;
            else
                onlyImageName = [];
            end
            fileNameForImage = [CommonHandles.DirName filesep plateName filesep  CommonHandles.OriginalImageFolder filesep onlyImageName];
            
            if whiteImg && ~isempty(onlyImageName)
                [~,withoutExt,~] = fileparts(onlyImageName);
                fileNameForThumb = [withoutExt '.jpg'];
            else
                fileNameForThumb = [];
            end
            fprintf(htmlOut,'\t<td><div id="pic"><a class="thumbnail"  href="%s" target="_blank" ><img src="icons/%s" /><span><img  src="thumbs/%s" /><br></span></a></div></td>\n', fileNameForImage, fileNameForHeatmap, fileNameForThumb);
            
        end
    end
    fprintf(htmlOut, '</tr>\n');
end


% footer
fprintf(htmlOut, '<! --MATLAB--> \n</body>\n</html>');

%put the css next to the html.
cssLocation = which('regressionPlateCSS.css');
copyfile(cssLocation,[htmlFolder filesep]);

fclose(htmlOut);
