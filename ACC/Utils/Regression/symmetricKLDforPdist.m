function [divergenceList] = symmetricKLDforPdist(a,B)
% AUTHOR:   Abel Szkalisity
% DATE:     July 22, 2019
% NAME:     symmetricKLDfirPdist
%
% Defines the symmetric Kullback-Leibler diveregence function so that it
% can be used with pdist.
%
% INPUT:
%   a               A single probability density function (in a flattened way)
%   B               A list of other prob. density functions (each row is an
%                   observation, must be the same wide as a)
%
% OUTPUT:
%   divergenceList  The difference value between a and the B
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

divergenceList = zeros(size(B,1),1);

for j=1:numel(divergenceList)
    a(a <= eps) = eps;
    B(j,B(j,:) <= eps) = eps;
    divergenceList(j) = symmetricKLD(a,B(j,:));
end

end

