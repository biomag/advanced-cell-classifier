function createRegressionPDFReport(results,plateName,targetFolder,controlRow,controlCol,resolution,shiftVectors,densityOneByOne,shiftPicture,idx,wellIcons)
% AUTHOR:   Abel Szkalisity
% DATE:     July 11, 2016
% NAME:     createRegressionPDFReport
%
% To create a pdf report of the regression results.
%
% INPUT:
%   results         A cellarray containing the regression results. (result
%                   of prediction) 
%   plateName       Name of the plate, used in file name giving.
%   targetFolder    The folder where the pdf is saved
%   controlRow      The indices of the row of the well
%   controlCol      The indices of the column of the well
%   resolution      An integer indicating the report resolution
%   shiftVectors    A cellarray in the format of the plate. Each entry
%                   contains a 4 element array where the 1-2. element is
%                   the starting point of the shift vector, 3-4. element is
%                   the end point of the shift vector. The shift vector
%                   points from the most dense region of the control well
%                   to the current well.
%   shiftPicture    A cellarray similar to shiftVectors. Each entry is the
%                   difference image between the density estimations of the
%                   current well and the average of the controls.
%   densityOneByOne Cellarray having the same size as results. Each entry
%                   is kernel density estimation of the given well.
%   idx             The index of the regression class to which the report
%                   is made. This is only needed here to save the correct
%                   file name which uniquely identifies the result file
%                   even if you have more regression class.
%   wellIcons       A cellarray with the same size as the plate. Each cell
%                   entry contains an image with a heatmap.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

figureForPDF = figure('Units','pixels');
subPlotRow = 3;
subPlotCol = 2;

%General calculations
[r, c] = size(results);

%% Plot for the regression heatmaps
regPlateAxes = subplot(subPlotRow,subPlotCol,1);

colormappedWellIcon = cell(r,c);
for i=1:r, for j=1:c, colormappedWellIcon{i,j} = imgToColormap(wellIcons{i,j},32,'none'); end, end
plotPlate(regPlateAxes,colormappedWellIcon,r,c,'Regression-heatmaps (histogram)',hot(32),resolution);

%% Kernel density heatmap
kernelDensityAxes = subplot(subPlotRow,subPlotCol,3);

colormappedDensity = cell(r,c);
for i=1:r
    for j=1:c
        if ~isempty(densityOneByOne{i,j})
            [colormappedDensity{i,j}] = imgToColormap(densityOneByOne{i,j},32,'ud');
        end
    end
end

plotPlate(kernelDensityAxes,colormappedDensity,r,c,'Kernel density estimations (KDE)',hot(32),resolution);

%% Cell number
cellNumberAxes = subplot(subPlotRow,subPlotCol,5);

cellNumbersImage = cell(r,c);
maxNum = 0;
for i=1:r
    for j=1:c
        if ~isempty(results{i,j})
            currCellNum = size(results{i,j},2);
            cellNumbersImage{i,j} = currCellNum;
            if currCellNum > maxNum, maxNum = currCellNum; end            
        end
    end
end
%normalize the data
colormappedCellNumber = cellNumbersImage;
for i=1:numel(colormappedCellNumber)
    if ~isempty(colormappedCellNumber{i}), colormappedCellNumber{i} = ceil(ones(resolution)*colormappedCellNumber{i}./maxNum.*64); end
end

plotPlate(cellNumberAxes,colormappedCellNumber,r,c,'#cells on the regression plane',hot(64),resolution);

for i=1:r
    for j=1:c
        text(cellNumberAxes,(j-1)*(resolution+2)+resolution/2+2,(i-1)*(resolution+2)+resolution/2+1,num2str(cellNumbersImage{i,j}),'HorizontalAlignment','center','Color','b','FontSize',3);
    end
end

cellNumberAxes.Position(1) = 0.5-cellNumberAxes.Position(3)/2;


%% Do the plot for the arrows
plotForArrows = subplot(subPlotRow,subPlotCol,2);
[rowNum,colNum] = size(results);
controlRowFlipped = rowNum - controlRow + 1;

s = cell2mat(shiftVectors);
controlColumnFirst = s(:,1:4:end);
controlColumnSecond = s(:,2:4:end);
wellColumnFirst = s(:,3:4:end);
wellColumnSecond = s(:,4:4:end);

%convert to column vectors
controlColumnFirst = controlColumnFirst(:);
controlColumnSecond = controlColumnSecond(:);
wellColumnFirst = wellColumnFirst(:);
wellColumnSecond = wellColumnSecond(:);

%filter for empty wells. Empty well is indicated by [0 0] --> [1 1] shift vector
emptyIndices = (controlColumnFirst == 0 & controlColumnSecond == 0 & wellColumnFirst == 1 & wellColumnSecond == 1);
fullIndices = ~emptyIndices;

[first,second] = meshgrid(0:colNum-1,rowNum-1:-1:0);
first = first(:);
second = second(:);

%plot the real vectors
quiver(first(fullIndices)+controlColumnFirst(fullIndices),second(fullIndices)+controlColumnSecond(fullIndices),wellColumnFirst(fullIndices)-controlColumnFirst(fullIndices),wellColumnSecond(fullIndices)-controlColumnSecond(fullIndices),'LineWidth',0.5,'AutoScale','off');
hold on;
%cross out empty wells with red.
plot([first(emptyIndices)+controlColumnFirst(emptyIndices) first(emptyIndices)+wellColumnFirst(emptyIndices)]',[second(emptyIndices)+controlColumnSecond(emptyIndices) second(emptyIndices)+wellColumnSecond(emptyIndices)]','k','LineWidth',0.5);

%highlight control wells
for i=1:length(controlCol)
    plot([controlCol(i)-1 controlCol(i) controlCol(i) controlCol(i)-1 controlCol(i)-1],[controlRowFlipped(i)-1,controlRowFlipped(i)-1,controlRowFlipped(i),controlRowFlipped(i),controlRowFlipped(i)-1],'LineWidth',1.5,'Color','red');
end
%set limits
axis([0 colNum 0 rowNum]);
plot([1:colNum; 1:colNum],[zeros(1,colNum); repmat(rowNum,1,colNum)],'k');
plot([zeros(1,rowNum); repmat(colNum,1,rowNum)],[1:rowNum; 1:rowNum],'k');

title('Shift of KDE maximum compared to control average KDE','FontSize',18);
set(plotForArrows,'YTick',0.5:1:rowNum);
set(plotForArrows,'XTick',0.5:1:colNum);
set(plotForArrows,'XTickLabel',1:c);
yLabels = char('A'):char('A'+r-1); yLabels = yLabels';
set(plotForArrows,'YTickLabel',fliplr(yLabels')');
set(plotForArrows,'FontSize',6);

%% Plot the surface difference
sufDifferenceAxes = subplot(subPlotRow,subPlotCol,4);
%calc total max
maxDiff = max(max(cell2mat(shiftPicture)));
minDiff = min(min(cell2mat(shiftPicture)));
%scale for the colormap
for i=1:r
    for j=1:c
        if ~all(shiftPicture{i,j} == 0)            
            shiftPicture{i,j}(shiftPicture{i,j}>0) = round(ceil(shiftPicture{i,j}(shiftPicture{i,j}>0)./maxDiff*32)+32); 
            shiftPicture{i,j}(shiftPicture{i,j}<=0) = round(32-floor(shiftPicture{i,j}(shiftPicture{i,j}<=0)./minDiff*31));
        else
            shiftPicture{i,j} = round(shiftPicture{i,j}+32);
        end
    end
end

plotPlate(sufDifferenceAxes,shiftPicture,r,c,'Difference of KDE and control average KDE',jet,resolution);
hold on;
for i=1:length(controlCol)
    plot([controlCol(i)-1 controlCol(i) controlCol(i) controlCol(i)-1 controlCol(i)-1].*(resolution+2),[controlRow(i)-1,controlRow(i)-1,controlRow(i),controlRow(i),controlRow(i)-1].*(resolution+2),'LineWidth',1.5,'Color','red');
end

%% Save to disk

outputName = [targetFolder filesep plateName '_regressionReport_class' num2str(idx,'%02d') ];

set(figureForPDF,'PaperPositionMode','manual');
set(figureForPDF,'PaperUnits','centimeters');
set(figureForPDF,'PaperSize',[25 25]);
if compareVersions('9.0.0.341360 (R2016a)',version)    
    print(figureForPDF, '-dpdf', outputName, '-fillpage');
    savefig(figureForPDF,outputName,'compact')
else
    print(figureForPDF, '-dpdf', outputName);
    savefig(figureForPDF,outputName)
end


close(figureForPDF);

end

%% Plot to plate function
function plotPlate(axesToPlot,dataToPlot,maxRow,maxCol,plotTitle,cmap,resolution)

    firstNonEmptyData = find(not(cellfun(@isempty,dataToPlot)),1);    
    if isempty(firstNonEmptyData)
        resolutionRow = resolution;
        resolutionCol = resolution;
    else
        resolutionRow = size(dataToPlot{firstNonEmptyData},1);
        resolutionCol = size(dataToPlot{firstNonEmptyData},2);
    end
    plateWithRegressionHeatmaps = ones([size(dataToPlot).*[(resolutionRow+2) (resolutionCol+2)],3])*0.25; %fill in everything with gray

    for i=1:size(dataToPlot,1)
        for j=1:size(dataToPlot,2)        
            if isempty(dataToPlot{i,j}) || all(dataToPlot{i,j}(:) == 0)
                plateWithRegressionHeatmaps((i-1)*(resolutionCol+2)+2:i*(resolutionCol+2)-1,(j-1)*(resolutionRow+2)+2:j*(resolutionRow+2)-1,:) = 0;
            else
                for k=1:3                    
                    dataToPlot{i,j}(dataToPlot{i,j} == 0) = 1; %correct for 0-s
                    plateWithRegressionHeatmaps((i-1)*(resolutionCol+2)+2:i*(resolutionCol+2)-1,(j-1)*(resolutionRow+2)+2:j*(resolutionRow+2)-1,k) = reshape(cmap(dataToPlot{i,j}(:),k),resolutionRow,resolutionCol);                    
                end
            end
        end
    end
       
    imagesc(axesToPlot,plateWithRegressionHeatmaps);
    colormap(axesToPlot,cmap)
    title(plotTitle,'FontSize',18);
    set(axesToPlot,'YTick',(resolutionRow+2)/2:(resolutionRow+2):maxRow*(resolutionCol+2));
    set(axesToPlot,'XTick',(resolutionCol+2)/2:(resolutionCol+2):maxCol*(resolutionRow+2));
    set(axesToPlot,'XTickLabel',1:maxCol)
    yLabels = char('A'):char('A'+maxRow-1); yLabels = yLabels';
    set(axesToPlot,'YTickLabel',yLabels);
    set(axesToPlot,'FontSize',6);
    caxis([0 1]);

end