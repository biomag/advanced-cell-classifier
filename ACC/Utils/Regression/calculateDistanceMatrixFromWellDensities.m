function [distPerWell,distPerDrug] = calculateDistanceMatrixFromWellDensities(densities,wellIDs,wellAnnotations, distanceMetric)
% AUTHOR:   Abel Szkalisity
% DATE:     May 23, 2017
% NAME:     calculateDistanceMatrixFromWellDensities
%
% Calculates a distance (based on input) between the
% non-empty entries of the input densities. If needed then the densities
% needs to be normalized beforehand. It calculates 2 tpyes of differences,
% the well by well difference between non-empty wells, and the treatment
% differences.
%
% INPUT:
%   densities       A cellarray, each entry should represent a well and
%                   there should be a density estimation within it. Each
%                   density estimation should have the same size.
%   wellIDs         A cellarray with the wellIDs for the wellAnnotation
%   wellAnnotation  A cellarray with same length as wellIDs containing the
%                   labels for the given well. This is the basement for
%                   calculating the treatment differences.
%   distanceMetric  The string that is accepted by the pdist function of
%                   Machine Learning toolbox.
%
% OUTPUT:
%   distPerWell     Distance matrix. The entries in the corresponding matrices
%                   are the non-empty wells of the input cellarray, and
%                   they are read column-wise. (matlab default by calling
%                   (:))
%   distPerDrug     The distance matrix for the treatments
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

nofWells = numel(densities);

%make a large enough distance matrix of which we will delete later on
distM = zeros(nofWells);
ik = 1;

infText = 'Calculate pairwise heatmap differences...';
wb = waitbar(0,infText);

for i=1:nofWells
    if ~isempty(densities{i})        
        jk = ik;
        for j=i:nofWells            
            if ~isempty(densities{j})                
                %do the normalization within the parameters
                %distance = symmetricKLD(densities{i},densities{j});                
                diff = abs(densities{i}-densities{j}); distance = sum(diff(:)); %sum of pixelwise distance
                if isinf(distance) || isnan(distance)
                    distance = max(distM(:))*100;
                    warning('Inf or NaN values were computed as a distance between wells. It was substituted with 100*the so far maximum of the distances but your results might be unreliable.');
                end
                distM(ik,jk) = distance;
                distM(jk,ik) = distance;
                jk = jk+1;               
            end
        end
        ik = ik+1;
    end
    waitbar(1-( (nofWells-i+1)*(nofWells-i)/2) / ((nofWells-1)*nofWells/2),wb,infText);
end

if ishandle(wb)
    close(wb);
end
    
%delete unneccessarry elements
distM(ik:end,:) = [];
distM(:,ik:end) = [];