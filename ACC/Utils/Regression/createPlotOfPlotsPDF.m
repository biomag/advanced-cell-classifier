function createPlotOfPlotsPDF(positions,tgtFolder,realHeatmaps,replicateIndices,drugNames,metaType)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     createPlotOfPlotsPDF
%
% Creates a pdf file and writes it to disk of plot-of-plots: an
% unsupervised visualization of the regression data organized by wells.
% This function is only used for plotting, the calculation is done in
% plotOfPlotsDREDVIZ function.
%
% INPUT:
%   positions       n by 2 array with the 2D positions for each well (1
%                   well by row)
%   tgtFolder       The destination folder where the pdf will be saved
%   realHeatmaps    A flattened cell-array with the icons to be plot at the
%                   positions coordinates
%   replicateIndices A cellarray of indices that identify the well-groups
%                   (a.k.a treatments/drugs). Each entry identifies a drug,
%                   and the indices within tell which positions in the
%                   real-heatmaps array are associated with this specific
%                   drug
%   drugNames       Cellarray as long as replicateIndices, with the drug
%                   names 
%   metaType        A short name of the meta-visualization method which
%                   will be placed on the title of the figure and in its
%                   name
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


%Author: Abel Szkalisity
%creates the plot of plot pdf to tgtFolder
%Init rows and settings
    figureSize = 400;
    heatMapSize = 35;
    legendColNumber = 3;
    rowSize = 10;    
    nofDrugs = length(drugNames);
    legendRowNumber = ceil(nofDrugs / legendColNumber);
    positions = positions - min(positions);
    positions = positions ./ (max(positions)-min(positions));
    positions = positions * figureSize;
        
    % The +2 is to have padding on the top and bottom of the legend.
    legendTop = (legendRowNumber+2)*rowSize;
    metaFigure = figure(...
        'Position',[0 0 figureSize+heatMapSize figureSize+heatMapSize+(legendRowNumber+2)*rowSize],...
        'Units','Pixels',...
        'PaperSize',[figureSize+heatMapSize figureSize+heatMapSize+(legendRowNumber+2)*rowSize]/40,...
        'PaperUnits','centimeters'); %To make sense in cm --> divide by 40
    cmapres = 32;
    cmap = hot(cmapres);

    for i=1:length(realHeatmaps)
        %Images
        a = axes('Parent',metaFigure,'Units','Pixels','Position',[positions(i,1) positions(i,2)+legendTop heatMapSize heatMapSize]);
        imgToShow = zeros(size(realHeatmaps{i},1)+2,size(realHeatmaps{i},2)+2,3);
        imgForColormap = imgToColormap(realHeatmaps{i},cmapres,'none');
        for k=1:3, imgToShow(2:end-1,2:end-1,k) = reshape(cmap(imgForColormap,k),size(imgForColormap,1),size(imgForColormap,2)); end
        idx = findReplicateIndex(replicateIndices,i);
        color = getManyColor(idx)/255;
        imgToShow = fillBoundaries(imgToShow,color);
        imshow(imgToShow,'Parent',a);        
    end
    
    for i=1:nofDrugs
        a = axes('Parent',metaFigure,'Units','Pixels',...
            'Position',...
            [ floor((i-1)/legendRowNumber)*figureSize/legendColNumber+rowSize/4 ...
              legendTop-rowSize-(mod(i-1,legendRowNumber)+1)*rowSize+rowSize/4 ...
              rowSize/2 rowSize/2 ...
            ]);
        color = getManyColor(i)/255;
        imgToShow = zeros(2,2,3);
        imgToShow = fillBoundaries(imgToShow,color);
        imshow(imgToShow,'Parent',a);
        uicontrol('Style','text','String',drugNames{i}, ...
            'Units','Pixel',...            
            'Parent',metaFigure, ...
            'Position', ...
            [ floor((i-1)/legendRowNumber)*figureSize/legendColNumber+rowSize ...
              legendTop-rowSize-(mod(i-1,legendRowNumber)+1)*rowSize ...
              figureSize / legendColNumber - rowSize ...
              rowSize
            ],...
            'FontSize',12);
    end

    if compareVersions('9.0.0.341360 (R2016a)',version)
        print(metaFigure,[tgtFolder filesep 'PlotOfPlots_' metaType],'-dpdf','-fillpage');
    else
        print(metaFigure,[tgtFolder filesep 'PlotOfPlots_' metaType],'-dpdf');
    end
    if ishandle(metaFigure)
        close(metaFigure);
    end

end

function idx = findReplicateIndex(replicateIndices, toFind)
    idx = 0;
    for i=1:length(replicateIndices)
        if ismember(toFind,replicateIndices{i})
            idx = i;
            break;
        end
    end
end

