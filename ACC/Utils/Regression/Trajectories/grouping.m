function grouping(order, groupName, handles, varargin)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     grouping
%
% Group creational, and membership adjusting function.
%
% INPUTS
%   order - 'Create new', 'Add to existing' or 'Remove from group'
%   groupName - char type use input
%   handles - handles struct from plotTrajGUI
%
%   Hint - grouping(_, _, _, 'TrajIDs', '1352') - Trajectory ID
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

narginchk(3, 5)

if any(strcmp(varargin, 'TrajIDs') == 1)
    trajIDsIdx = find(strcmp(varargin, 'TrajIDs')==1);
    trajIDs = varargin{trajIDsIdx+1};
end


pTPC = handles.figure1.UserData;

switch order
    case 'Create new'
        
        groupIndex = length(pTPC.groupList)+1;
        pTPC.groupList(groupIndex).Name = groupName;
        pTPC.groupList(groupIndex).Members = trajIDs;
        
        for i=1:length(trajIDs)
            pTPC.lines(trajIDs(i)).UserData(1) = {union(pTPC.lines(trajIDs(i)).UserData{1}, groupIndex)};
        end
        
    case 'Add to existing'
        
        groupIndex = find(strcmp([pTPC.groupList.Name], groupName));
        pTPC.groupList(groupIndex).Members = union(pTPC.groupList(groupIndex).Members, trajIDs);
        
        for i=1:length(trajIDs)
            pTPC.lines(trajIDs(i)).UserData(1) = {union(pTPC.lines(trajIDs(i)).UserData{1}, groupIndex)};
        end
        
        %check if highlight is applied on this gourp and set colors
        for j = 1:15
            if handles.uipanel_toogleGroup.Children(16-j).Value
                if strcmp(handles.uipanel_toogleGroup.Children(16-j).UserData, groupName)
                    for i=1:length(trajIDs)
                        highLightColor = handles.uipanel_toogleGroup.Children(16-j).BackgroundColor;
                        adjustHighlightColor(j, 1, groupName, highLightColor, handles);
                    end
                end
            end
        end
        
    case 'Remove from group'
        
        groupIndex = find(strcmp([pTPC.groupList.Name], groupName));
        tempMemberList = pTPC.groupList(groupIndex).Members;
        pTPC.groupList(groupIndex).Members = setdiff(tempMemberList,trajIDs);
        
        for i=1:length(trajIDs)
            pTPC.lines(trajIDs(i)).UserData(1) = {setdiff(pTPC.lines(trajIDs(i)).UserData{1}, groupIndex)};
        end
        
        %check if highlight is applied on this gourp and set colors
        for j = 1:15
            if handles.uipanel_toogleGroup.Children(16-j).Value
                if strcmp(handles.uipanel_toogleGroup.Children(16-j).UserData, groupName)
                    for i=1:length(trajIDs)
                        pTPC.lines(trajIDs(i)).UserData(4) = {setdiff(pTPC.lines(trajIDs(i)).UserData{4}, j)};
                        if isempty(pTPC.lines(trajIDs(i)).UserData{4})
                            pTPC.lines(trajIDs(i)).Color = pTPC.lines(trajIDs(i)).UserData{2};
                            pTPC.lines(trajIDs(i)).LineWidth = pTPC.lines(trajIDs(i)).UserData{3};
                        else
                            highLightIdx = pTPC.lines(trajIDs(i)).UserData{4};
                            colorsToMix = cell2mat({handles.uipanel_toogleGroup.Children(16-highLightIdx).BackgroundColor}');
                            hsvHighlights = rgb2hsv(colorsToMix);
                            mixedHSV = [mean(hsvHighlights(:, 1)), 1, 1];
                            mixedRGB = hsv2rgb(mixedHSV);
                            set(pTPC.lines(trajIDs(i)), 'Color', mixedRGB);
                            
                            if numel(pTPC.lines(trajIDs(i)).UserData{4}) >= 2
                                set(pTPC.lines(trajIDs(i)), 'LineWidth', 4);
                            else
                                set(pTPC.lines(trajIDs(i)), 'LineWidth', 2);
                            end
                        end
                    end
                end
            end
        end
        
    otherwise
        return
end
