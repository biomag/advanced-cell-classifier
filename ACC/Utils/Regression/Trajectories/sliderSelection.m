function varargout = sliderSelection(varargin)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     plotTrajGui
%
% This GUI is for selecting trajectory property or cell fature to calculate
% new values for sliders to help to filter the data.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.


% SLIDERSELECTION MATLAB code for sliderSelection.fig
%      SLIDERSELECTION, by itself, creates a new SLIDERSELECTION or raises the existing
%      singleton*.
%
%      H = SLIDERSELECTION returns the handle to a new SLIDERSELECTION or the handle to
%      the existing singleton*.
%
%      SLIDERSELECTION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SLIDERSELECTION.M with the given input arguments.
%
%      SLIDERSELECTION('Property','Value',...) creates a new SLIDERSELECTION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before sliderSelection_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to sliderSelection_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help sliderSelection

% Last Modified by GUIDE v2.5 20-Feb-2020 22:55:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @sliderSelection_OpeningFcn, ...
                   'gui_OutputFcn',  @sliderSelection_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before sliderSelection is made visible.
function sliderSelection_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to sliderSelection (see VARARGIN)

% Choose default command line output for sliderSelection
handles.output = hObject;
trajGUI_handles = varargin{1};
handles.sliderSelection_figure.UserData{1} = get(trajGUI_handles.figure1,'UserData');
handles.sliderSelection_figure.UserData{2} = varargin{2};

trajLevel_list = {'Number of Frames';...
                    'Distance from origin';...
                    'Length of trajectory';...
                    'Distance from coordinate'
                    };
              
global CommonHandles;
featureNameFile = fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, CommonHandles.MetaDataFolder, 'featureNames.acc');
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');

handles.traj_level_listbox.String = trajLevel_list;
handles.cell_level_listbox.String = featureNameList;

startImage = imread('trajLevelFrame.png');
image(handles.sliderSelection_anim_axes,startImage);
handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
handles.sliderSelection_anim_axes.YAxis.Visible = 'off';

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes sliderSelection wait for user response (see UIRESUME)
uiwait(handles.sliderSelection_figure);


% --- Outputs from this function are returned to the command line.
function varargout = sliderSelection_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if isfield(handles, 'output')
    varargout{1} = handles.output;
else
    varargout{1} = [];
end


% --- Executes on selection change in cell_level_listbox.
function cell_level_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to cell_level_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns cell_level_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from cell_level_listbox


% --- Executes during object creation, after setting all properties.
function cell_level_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cell_level_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in method_popupmenu.
function method_popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to method_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns method_popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from method_popupmenu


if strcmp(handles.level_selection_buttonGroup.SelectedObject.String,'Cell Level')
        method = hObject.String{hObject.Value};
    
    switch method
        
        case 'Min'
            animFileName = 'cellLevelMin.gif';
        case 'Max'
            animFileName = 'cellLevelMax.gif';
        case 'Mean'
            animFileName = 'cellLevelMean.gif';
        case 'Median'
            animFileName = 'cellLevelMedian.gif';
        case 'Range'
            animFileName = 'cellLevelRange.gif';
        case 'Sum'
            animFileName = 'cellLevelSum.gif';
        otherwise
            return
    end
    
    [I, cmap] = imread(animFileName,'frames','all');
    colormap(handles.sliderSelection_anim_axes, cmap);
    for j=1:size(I, 4)
        if ~isfield(handles,'sliderSelection_anim_axes') || ~ishandle(handles.sliderSelection_anim_axes)
            return
        end
        image(handles.sliderSelection_anim_axes,I(:,:,:,j));
        colormap(handles.sliderSelection_anim_axes, cmap);
        axis image
        handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
        handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
        drawnow
        pause(.05)
    end
end


% --- Executes during object creation, after setting all properties.
function method_popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to method_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in traj_level_listbox.
function traj_level_listbox_Callback(hObject, eventdata, handles)
% hObject    handle to traj_level_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns traj_level_listbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from traj_level_listbox

if strcmp('Distance from coordinate', hObject.String(get(hObject, 'Value')))
    handles.method_popupmenu.Enable = 'on';
    handles.X_coordiante.Enable = 'on';
    handles.Y_coordiante.Enable = 'on';
else
    handles.method_popupmenu.Enable = 'off';
    handles.X_coordiante.Enable = 'off';
    handles.Y_coordiante.Enable = 'off';
end

method = hObject.String{hObject.Value};
    
switch method
    
    case 'Number of Frames'
        animFileName = 'trajLevelFrame.png';
        handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
        handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
        I = imread(animFileName);
        image(handles.sliderSelection_anim_axes,I)
        handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
        handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
        return
        
    case 'Distance from origin'
        animFileName = 'trajLevelDistance.gif';
        
    case 'Length of trajectory'
        animFileName = 'trajLevelLength.gif';
        
    case 'Distance from coordinate'
        animFileName = 'trajLevelCoordinate.gif';
        
    otherwise
        return
end

[I, cmap] = imread(animFileName,'frames','all');
colormap(handles.sliderSelection_anim_axes, cmap);
for j=1:size(I, 4)
    if ~isfield(handles,'sliderSelection_anim_axes') || ~ishandle(handles.sliderSelection_anim_axes)
        return
    end
    image(handles.sliderSelection_anim_axes,I(:,:,:,j));
    colormap(handles.sliderSelection_anim_axes, cmap);
    axis image
    handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
    handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
    drawnow
    pause(.05)
end



% --- Executes during object creation, after setting all properties.
function traj_level_listbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to traj_level_listbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Y_coordiante_Callback(hObject, eventdata, handles)
% hObject    handle to Y_coordiante (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Y_coordiante as text
%        str2double(get(hObject,'String')) returns contents of Y_coordiante as a double


% --- Executes during object creation, after setting all properties.
function Y_coordiante_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Y_coordiante (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function X_coordiante_Callback(hObject, eventdata, handles)
% hObject    handle to X_coordiante (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of X_coordiante as text
%        str2double(get(hObject,'String')) returns contents of X_coordiante as a double


% --- Executes during object creation, after setting all properties.
function X_coordiante_CreateFcn(hObject, eventdata, handles)
% hObject    handle to X_coordiante (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in level_selection_buttonGroup.
function level_selection_buttonGroup_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in level_selection_buttonGroup 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


coorDistValue = strcmp('Distance from coordinate', handles.traj_level_listbox.String(get(handles.traj_level_listbox, 'Value')));

switch eventdata.NewValue.String
    case 'Trajectory Level'
        handles.cell_level_coordiantes_uipanel.Visible = 1;
        handles.traj_level_listbox.Visible = 1;
        handles.cell_level_listbox.Visible = 0;
        handles.options_uipanel.Title = 'Trajectory Level';
        if coorDistValue
            handles.method_popupmenu.Enable = 'on';
            handles.X_coordiante.Enable = 'on';
            handles.Y_coordiante.Enable = 'on';
        else
            handles.method_popupmenu.Enable = 'off';
            handles.X_coordiante.Enable = 'off';
            handles.Y_coordiante.Enable = 'off';
        end
        cellLevelIm = imread('cellLevel.png');
        image(handles.sliderSelection_anim_axes,cellLevelIm);
        startImage = imread('trajLevelFrame.png');
        image(handles.sliderSelection_anim_axes,startImage);
        handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
        handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
        
    case 'Cell Level'
        handles.cell_level_coordiantes_uipanel.Visible = 0;
        handles.traj_level_listbox.Visible = 0;
        handles.cell_level_listbox.Visible = 1;
        handles.method_popupmenu.Enable = 'on';
        handles.options_uipanel.Title = 'Cell Level';
        cellLevelIm = imread('cellLevel.png');
        image(handles.sliderSelection_anim_axes,cellLevelIm);
        handles.sliderSelection_anim_axes.XAxis.Visible = 'off';
        handles.sliderSelection_anim_axes.YAxis.Visible = 'off';
    otherwise
            return
end


% --- Executes on button press in apply_pushButton.
function apply_pushButton_Callback(hObject, eventdata, handles)
% hObject    handle to apply_pushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


switch handles.level_selection_buttonGroup.SelectedObject.String
    case 'Trajectory Level'
        sliderFun{1} = 'Trajectory Level';
        sliderFun{2} = handles.traj_level_listbox.String{get(handles.traj_level_listbox, 'Value')};
        if strcmp(sliderFun{2}, 'Distance from coordinate')
            if strcmp('--select--', handles.method_popupmenu.String(get(handles.method_popupmenu, 'Value')))
                warndlg('Please select a method from popup');
                return
            else
                sliderFun{3} = handles.method_popupmenu.String{get(handles.method_popupmenu, 'Value')};
            end
            
            if ~isempty(handles.X_coordiante.String) && ~isempty(str2num(handles.X_coordiante.String))
                sliderFun{4} = str2num(handles.X_coordiante.String);
            else
                warndlg('Coordinates must be numeric value.');
                return
            end
            if ~isempty(handles.Y_coordiante.String) && ~isempty(str2num(handles.Y_coordiante.String))
                sliderFun{5} = str2num(handles.Y_coordiante.String);
            else
                warndlg('Coordinates must be numeric value.');
                return
            end
            
        else
            sliderFun{3} = [];
            sliderFun{4} = [];
            sliderFun{5} = [];
        end
        
    case 'Cell Level'
        sliderFun{1} = 'Cell Level';
        sliderFun{2} = handles.cell_level_listbox.String{get(handles.cell_level_listbox, 'Value')};
        if strcmp('--select--', handles.method_popupmenu.String(get(handles.method_popupmenu, 'Value')))
            warndlg('Please select a method from popup');
            return
        else
            sliderFun{3} = handles.method_popupmenu.String{get(handles.method_popupmenu, 'Value')};
        end
        sliderFun{4} = [];
        sliderFun{5} = [];
    otherwise
        errordlg('Something terrible happened.')
end

if handles.sliderSelection_figure.UserData{2} == 1
    handles.sliderSelection_figure.UserData{1}.slider_1_Fun = sliderFun;
elseif handles.sliderSelection_figure.UserData{2} == 2
    handles.sliderSelection_figure.UserData{1}.slider_2_Fun = sliderFun;
else
    errordlg('Something terrible happened.')
end
close(hObject.Parent)

% --- Executes on button press in cancel_pushButton.
function cancel_pushButton_Callback(hObject, eventdata, handles)
% hObject    handle to cancel_pushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.sliderSelection_figure)


% --- Executes when user attempts to close sliderSelection_figure.
function sliderSelection_figure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to sliderSelection_figure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
end
delete(hObject);
