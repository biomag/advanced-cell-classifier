%This scripts creates csv files from the same cell-instances, i.e. matrix
%of the whole trajectory. Each row of the matrix refers to one time-point,
%while the columns are the different dimensions (multi-dimensional
%measurements).

%% Input vars
baseDir = 'W:\beleon\ACC_Project\MitoCheck';

srcDir = fullfile(baseDir,'mitoMerged_3Chan');
trackFile = fullfile(baseDir,'mitoMerged_3Chan\20200323_0212_track.csv');
tgtDir = fullfile(baseDir,'mitoMerged_3Chan');
analDir = 'anal2v2_deriv_1_2_3';
featureFileName = 'featureNames.acc';

derNUM = [1 2 3]; %number of derivative needed i.e.: t = (+/-)1 or (+/-)2 and so on

%% Calc matrices
if ~isfolder(tgtDir), mkdir(tgtDir); end

trackTable = readtable(trackFile);

uniTracks = unique(trackTable.TrackID);

sequenceMatrix = cell(length(uniTracks),1);

tic
for i=493:length(uniTracks)
    currTrackID = uniTracks(i);
    relevantTable = trackTable(trackTable.TrackID == i,:);
    sequenceLength = size(relevantTable,1);
    sequenceMatrix{i} = cell(sequenceLength,1);
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(srcDir,relevantTable.Plate{j},'anal2v2',[imgNameExEx '.txt']);
        dataM = load(fileName);
        sequenceMatrix{i}{j} = dataM(relevantTable.ObjectID(j),:);
    end
    %Save cell to local variable here
    seqMat = cell2mat(sequenceMatrix{i});    
    nofFeatures = size(seqMat,2); %NOTE: Location as well!
    
    %Add the derivatives (only for the non-index features)
    extSeqMat = zeros(sequenceLength,(nofFeatures-2)*length(derNUM)+nofFeatures);
    extSeqMat(:,1:nofFeatures) = seqMat;
    
    %%
    %Do all #t1 derivatives in the middle
    jj = 1;
    for d = derNUM
        if d == 2
            pause(1);
        end
        for j=3:nofFeatures
            for k=d+1:sequenceLength-d
                extSeqMat(k,nofFeatures+jj) = (extSeqMat(k+d,j) - extSeqMat(k-d,j))/2;
            end
        
        

        %Handle case for the extremes
        
            exD = 1;
            while exD <= d
                % start of traj
                extSeqMat(exD,nofFeatures+jj) = (extSeqMat(exD+1,j) - extSeqMat(1,j))/2;
                
                % end of traj
                extSeqMat(sequenceLength-exD+1,nofFeatures+jj) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-exD+1-d,j))/2;
                exD = exD + 1;
                
            end
            jj = jj + 1;
        end
    end
    %%
    
    %%
%     %Do all #t3 derivatives in the middle
%     for j=3:nofFeatures
%         for k=4:sequenceLength-3
%             extSeqMat(k,nofFeatures-2+j) = (extSeqMat(k+3,j) - extSeqMat(k-3,j))/2;
%         end
%     end
%     %Handle case for the extremes
%     for j=3:nofFeatures
%         extSeqMat(1,nofFeatures-2+j) = (extSeqMat(4,j) - extSeqMat(1,j))/2;
%         extSeqMat(2,nofFeatures-2+j) = (extSeqMat(5,j) - extSeqMat(1,j))/2;
%         extSeqMat(3,nofFeatures-2+j) = (extSeqMat(6,j) - extSeqMat(1,j))/2;
%         extSeqMat(sequenceLength-2,nofFeatures-2+j) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-5,j))/2;
%         extSeqMat(sequenceLength-1,nofFeatures-2+j) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-4,j))/2;
%         extSeqMat(sequenceLength,nofFeatures-2+j) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-3,j))/2;
%     end
%%
    sequenceMatrix{i} = extSeqMat;
    

    %Create feature names as well
    % -> First load them, from the first plate
    featureFile = fullfile(srcDir,relevantTable.Plate{1},'anal2v2',featureFileName);
    T = readtable(featureFile,'FileType','text','Delimiter',',','ReadVariableNames',false);    
    extFeatureNames = cell(nofFeatures+(length(derNUM))*(nofFeatures-2),1);
    extFeatureNames(1:nofFeatures) = T.Var1;
    
    jj = nofFeatures+1;
    for ii = 1:length(derNUM)
        for j=3:nofFeatures
            extFeatureNames{jj} = ['firstDer_', num2str(derNUM(ii)), '_', extFeatureNames{j}];
            jj = jj + 1;
        end
    end
    
    
    %Write out the files in ACC format
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(tgtDir,relevantTable.Plate{j}, analDir,[imgNameExEx '.txt']);
        if ~isfolder(fullfile(tgtDir,relevantTable.Plate{j}, analDir)), mkdir(fullfile(tgtDir,relevantTable.Plate{j}, analDir)); end
        csvwrite(fileName,sequenceMatrix{i}(j,:));
        if ~exist(fullfile(tgtDir,relevantTable.Plate{j}, analDir, featureFileName),'file')
%             writecell(extFeatureNames,fullfile(tgtDir,relevantTable.Plate{j},'anal2_derivative_3_corr',featureFileName),'FileType','text');
            extFeaturesNamesTable = table(extFeatureNames);
            writetable(extFeaturesNamesTable,fullfile(tgtDir,relevantTable.Plate{j}, analDir, featureFileName),'FileType','text', 'WriteVariableNames', false);
        end
    end
                           
        toc
    fprintf('%d/%d ready\n',i,length(uniTracks));
end