function aniMovie = multyPathAnimation(inputCell, animResolution, tail, aniName, selpath, colorOrdered)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     multyPathAnimation
%
% MultiplePathAnimation function creating a movie file of visible
% trajectories. The movie file takes time to animate, be patient!
%
% INPUTS
%   inputCell       - coordinates to plot the line [n*2]
%
%   animResolution  - number of steps between timepoints of the cell path
%                     larger value makes the animation slower.
%
%   tail            - determines the length of the animated lines.
%
%
%   aniName         - name of the output movie file
%
%   selpath         - selected path to save the movie file
%
%   colorOrder      - array contains the IDs of highlighted trajectoires
%
% OUTPUTS
%   aniMovie        = object
%   Saves video file to specified directory
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.



M = size(inputCell,1);
f = figure('Units', 'pixels', 'Resize', 'off', 'DockControls', 'off',...
    'MenuBar', 'none', 'ToolBar', 'none', 'HitTest', 'off',...
    'ButtonDownFcn', @(h,e) msgbox('Please don not close or resize this window!'));
ax = axes;
tail = tail*animResolution;
m = 0;
f.WindowState ='fullScreen';

messageHelp = helpdlg('This calculation can run in the background, you can switch windows.');
MessageWarn = warndlg('Please don''t close the animation figure!');


if isempty(colorOrdered)
    endpoint = M;
    c = [0 0 0];
    ms = 10; % marker size
    lw = 0.2; % line width
else
    endpoint = colorOrdered{1, 1}-1;
    c = [0.3 0.3 0.3];
    ms = 10;
    lw = 0.2;
end

try
    k = 0;
    for i = 1:endpoint
        if ~isempty(inputCell{i, 2})
            k = k+1;
            X{k, 1} = matlab.graphics.chart.internal.datachk(inputCell{i, 2}(:, 1));
            Y{k, 1} = matlab.graphics.chart.internal.datachk(inputCell{i, 2}(:, 2));
            
            heads{k, 1} = line('parent',ax,'marker','o','linestyle','none', 'color', c,...
                'LineWidth', lw, 'MarkerSize', ms, 'xdata',X{k, 1}(1, 1),'ydata',Y{k, 1}(1, 1),'Tag',['head', num2str(k)]);
            
            bodies{k, 1} = matlab.graphics.animation.AnimatedLine('Parent',ax, 'color', c,...
                'LineWidth', lw,...
                'MaximumNumPoints',max(1,tail),'tag',['body', num2str(k)]);
            m = max([m, length(X{k, 1})]);
        end
    end
    
    if ~isempty(colorOrdered)
        if size(colorOrdered,1) ~= size(inputCell,1)
            ms = 15;
            lw = 3;
        else
            ms = 12;
            lw = 1;
        end
        
        for i = colorOrdered{1, 1}:M
            if ~isempty(inputCell{i, 2})
                k = k + 1;
                X{k, 1} = matlab.graphics.chart.internal.datachk(inputCell{i, 2}(:, 1));
                Y{k, 1} = matlab.graphics.chart.internal.datachk(inputCell{i, 2}(:, 2));
                c = colorOrdered{[colorOrdered{:, 1}] == i, 2};
                
                heads{k, 1} = line('parent',ax,'marker','o','linestyle','none', 'color', c,...
                    'LineWidth', lw, 'MarkerSize', ms, 'xdata',X{k, 1}(1, 1),'ydata',Y{k, 1}(1, 1),'Tag',['head', num2str(k)]);
                
                bodies{k, 1} = matlab.graphics.animation.AnimatedLine('Parent',ax, 'color', c,...
                    'LineWidth', lw,...
                    'MaximumNumPoints',max(1,tail),'tag',['body', num2str(k)]);
                m = max([m, length(X{k, 1})]);
            end
        end
    end
    
    ax.XLim = [0 1];
    ax.YLim = [0 1];
    title(ax, 'Frame 1');
    ax.TitleFontSizeMultiplier = 2;
    if ishandle(f)
        aniMovie(1) = getframe(f);
    else
        errordlg('You closed the animation figure. The process is terminated.');
        return
    end
    
    % Elongate the Lines
    i = 2;
    while i <= tail
        for g = 1:k
            if ~(isvalid(heads{g, 1}) && isvalid(bodies{g, 1}))
                return
            end
            set(heads{g, 1},'xdata',X{g, 1}(i),'ydata',Y{g, 1}(i));
            addpoints(bodies{g, 1},X{g, 1}(i),Y{g, 1}(i));
        end
        i = i+1;
        if mod(i, animResolution) == 0
            title(ax, ['Frame ', num2str((i/animResolution)+1)]);
        end
        drawnow
        if ishandle(f)
            aniMovie(i-1) = getframe(f);
        else
            errordlg('You closed the animation figure. The process is terminated.');
            return
        end
    end
    drawnow
    
    % Animate the line moving
    
    closedArray = zeros(k, 1);
    
    for i = tail+1:m
        for g = 1:k
            if closedArray(g) == 0
                if ~(isvalid(heads{g, 1}) && isvalid(bodies{g, 1}))
                    return
                end
                try
                    set(heads{g, 1},'xdata',X{g, 1}(i),'ydata',Y{g, 1}(i));
                    addpoints(bodies{g, 1},X{g, 1}(i),Y{g, 1}(i));
                catch
                    closedArray(g) = 1;
                end
            end
        end
        if mod(i, animResolution) == 0
            title(ax, ['Frame ', num2str((i/animResolution)+1)]);
        end
        drawnow
        if ishandle(f)
            aniMovie(i) = getframe(f);
        else
            errordlg('You closed the animation figure. The process is terminated.');
            return
        end
    end
    drawnow
    pause(0.1);
catch
    errordlg('Animation processing is failed', 'Error')
end
fileName = fullfile(selpath, aniName);
try
    v = VideoWriter(fileName,'Motion JPEG AVI');
    open(v)
    wb = waitbar(0,'Please wait...','Name','Rendering your video');
    L = length(aniMovie);
    for i = 1:L
        if mod(i,10)==0 && ishandle(wb)
            waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
        end
        writeVideo(v, aniMovie(i))
    end
catch
    errordlg('Can not render video file.', 'Render Error')
end
if ishandle(wb), close(wb), end
close(v)
if ishandle(f), close(f), end
if ishandle(messageHelp), close(messageHelp), end
if ishandle(MessageWarn), close(MessageWarn), end