function bool = checkTable(toCheckFields,currTable)
boold = 0;
for i=1:length(toCheckFields)
    if ~ismember(toCheckFields{i},currTable.Properties.VariableNames)
        error([toCheckFields{i} ' column is missing from the loaded csv, but required.']);
    end
end
bool = 1;