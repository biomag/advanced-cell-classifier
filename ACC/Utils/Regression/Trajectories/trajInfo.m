function varargout = trajInfo(varargin)
% TRAJINFO MATLAB code for trajInfo.fig
%      TRAJINFO, by itself, creates a new TRAJINFO or raises the existing
%      singleton*.
%
%      H = TRAJINFO returns the handle to a new TRAJINFO or the handle to
%      the existing singleton*.
%
%      TRAJINFO('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAJINFO.M with the given input arguments.
%
%      TRAJINFO('Property','Value',...) creates a new TRAJINFO or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before trajInfo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to trajInfo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help trajInfo

% Last Modified by GUIDE v2.5 15-Jul-2020 13:04:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @trajInfo_OpeningFcn, ...
                   'gui_OutputFcn',  @trajInfo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before trajInfo is made visible.
function trajInfo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to trajInfo (see VARARGIN)

% Choose default command line output for trajInfo
handles.output = hObject;

src = varargin{1};
pTPC = src.figure1.UserData;
handles.UserData = pTPC;

if isempty(pTPC.groupList)
    handles.groupList_listBox.String = [{'All'} {'Visible'}];
else
    handles.groupList_listBox.String = [{'All'} {'Visible'} pTPC.groupList.Name];
end


infoData = cell(pTPC.n,6);
for i = 1:pTPC.n
    
    infoData{i, 1} = i; % Trajectry ID
    
    infoData{i, 2} = pTPC.A{i, 1}(1, 1); % x coordinate
    
    infoData{i, 3} = pTPC.A{i, 1}(1 ,2); % y coordiante
    
    infoData{i, 4} = pTPC.A{i, 14}{1}.ImageName; % Image name of first frame
    
    infoData{i, 5} = size(pTPC.A{i, 1}, 1); % Frame number
    
    sumLength = 0;
    for j=1:size(pTPC.A{i, 1},1)-1
        sumLength = sumLength + norm(pTPC.A{i, 1}(j+1,:) - pTPC.A{i, 1}(j,:));
    end
    infoData{i, 6} = sumLength; % Length of trajectory
    
    infoData{i, 7} = norm(pTPC.A{i, 1}(1,:)-pTPC.A{i, 1}(end,:)); % Distance From Origin
    
    infoData{i, 8} = pTPC.A{i, 14}{1}.CellNumber; % ACC cell ID on first frame
end

handles.infoTable11.UserData = infoData;
[row, col] = size(infoData);
handles.infoTable11.Data = cell(row, col+1);
handles.infoTable11.Data(:,1:col) = infoData;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes trajInfo wait for user response (see UIRESUME)
uiwait(handles.trajInfoFig);


% --- Outputs from this function are returned to the command line.
function varargout = trajInfo_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
% varargout{1} = handles.output;


% --- Executes on selection change in groupList_listBox.
function groupList_listBox_Callback(hObject, eventdata, handles)
% hObject    handle to groupList_listBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns groupList_listBox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from groupList_listBox

pTPC = handles.UserData;
infoData = handles.infoTable11.UserData;

switch hObject.Value
    
    case 1 % All
        [row, col] = size(infoData);
        handles.infoTable11.Data = cell(row, col+1);
        handles.infoTable11.Data(:,1:col) = infoData;
        
    case 2 % Visible
        Idx = 1:pTPC.n;
        visMat = cell2mat(pTPC.A(Idx,9:12)); %matrix that contains the visibility data
        VisibleTraj = Idx(find(all(visMat,2)));
        [row, col] = size(infoData(VisibleTraj, :));
        handles.infoTable11.Data = cell(row, col+1);
        handles.infoTable11.Data(:,1:col) = infoData(VisibleTraj,:);
        
    otherwise
        
        selectedGroupMembers = pTPC.groupList(hObject.Value-2).Members;
        [row, col] = size(infoData(selectedGroupMembers, :));
        handles.infoTable11.Data = cell(row, col+1);
        handles.infoTable11.Data(:,1:col) = infoData(selectedGroupMembers,:);
        
end
        




% --- Executes during object creation, after setting all properties.
function groupList_listBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to groupList_listBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in exportGroup_button.
function exportGroup_button_Callback(hObject, eventdata, handles)
% hObject    handle to exportGroup_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pTPC = handles.UserData;

groupID = handles.groupList_listBox.Value-2;
if groupID == 0 || groupID == -1
    msgbox(sprintf(['This is not a valid group name.\n'...
    'Please select an existing group name, or create a new group of the visible trajectories!']))
    return
end
if ~strcmp(pTPC.groupList(groupID).Name, handles.groupList_listBox.String(handles.groupList_listBox.Value))
    errordlg('Missing group.')
    return
end
selectedGroupName = pTPC.groupList(groupID).Name;
selectedGroupMemers = pTPC.groupList(groupID).Members;
n = length(selectedGroupMemers);
coordinatesToCheck = zeros(n, 2);
imNameToCheck = cell(n,1);
j = 0;
for i = selectedGroupMemers
    j = j + 1;
    coordinatesToCheck(j, :) = pTPC.A{i, 1}(1,:);
    imNameToCheck{j, 1} = pTPC.A{i, 14}{1, 1}.ImageName;
end

[fileName, filePath] = uiputfile(['Trajectory_group_', selectedGroupName{:}, '.mat'], 'Export trajectory group');

save(fullfile(filePath,fileName), 'selectedGroupName', 'selectedGroupMemers', 'coordinatesToCheck','imNameToCheck')







% --- Executes on button press in importGroup_button.
function importGroup_button_Callback(hObject, eventdata, handles)
% hObject    handle to importGroup_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pTPC = handles.UserData;

[fileName, filePath] = uigetfile('*.mat','Select a group to import');
if ~ischar(fileName) || ~ischar(filePath)
    if fileName == 0 || filePath == 0
        return
    end
end
load(fullfile(filePath,fileName), 'selectedGroupName', 'selectedGroupMemers', 'coordinatesToCheck','imNameToCheck');

if ~exist('selectedGroupName','var') ||...
       ~exist('selectedGroupMemers','var') ||...
       ~exist('coordinatesToCheck','var') ||...
       ~exist('imNameToCheck','var')
    warndlg(sprintf(['Corrupted or invalid group file!\n', 'Please select a valid group file to import']))
    return
end

[groupNameBool, dataBool, maxBool] = checkMembers(handles,selectedGroupName,selectedGroupMemers,coordinatesToCheck,imNameToCheck);

if groupNameBool
    groupName = [pTPC.groupList.Name];
    try
        newGroupName = Settings_GUI({...
                struct('name', 'Group name', 'type', 'str', 'default', selectedGroupName, 'strSpecs', struct('forbidden', {groupName}, 'capital', [], 'number', [], 'glyph', [], 'space', 0)),...
                },...
                'infoText', {'This group name already exists.','Enter the name of the new group!'}, 'title', 'Group creation');
    catch e
        if strcmp(e.identifier,'Settings_GUI:noParameterProvided')
            return
        else
            rethrow(e)
        end
    end
    selectedGroupName = newGroupName;
end

if maxBool
    errordlg('There are more elements in the imported group than in the currently loaded project.')
    return
end

    

if dataBool
    answer = questdlg('Some of the group parameters do not match with the project! Whould you like to try anyway?',...
                        'Changes in the project',...
                        'Yes','No','No');
     if ~strcmp(answer, 'Yes')
         msgbox('Import terminated');
         return;
     end
end

fakeHandle.figure1.UserData = pTPC;
try
    grouping('Create new', selectedGroupName, fakeHandle, 'TrajIDs', selectedGroupMemers);
catch
    return
end

handles.groupList_listBox.String = [handles.groupList_listBox.String; selectedGroupName];

         


% --- Executes on button press in exportTable_button.
function exportTable_button_Callback(hObject, eventdata, handles)
% hObject    handle to exportTable_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

exportTable = cell2table(handles.infoTable11.Data);
exportTable.Properties.VariableNames = handles.infoTable11.ColumnName;
exportID_Callback([],[],exportTable);

% --- Executes on button press in close_button.
function close_button_Callback(hObject, eventdata, handles)
% hObject    handle to close_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isequal(get(hObject.Parent, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject.Parent);
end
delete(hObject.Parent);

function [groupNameBool, dataBool, maxBool] = checkMembers(handles,selectedGroupName,selectedGroupMemers,coordinatesToCheck,imNameToCheck)

pTPC = handles.UserData;
n = length(selectedGroupMemers);
maxBool = 0;
if max(selectedGroupMemers) > pTPC.n
    maxBool = 1;
end

i = 0;
if isempty(pTPC.groupList)
    groupNameBool = 0;
else
    groupNameBool = any(strcmp([pTPC.groupList.Name], selectedGroupName));
end
dataBool = 0;
while i < n
    i = i + 1;
    dist = norm(coordinatesToCheck(i, :) - pTPC.A{selectedGroupMemers(i), 1}(1,:));
    imNameBool = strcmp(imNameToCheck{i, 1}, pTPC.A{selectedGroupMemers(i), 14}{1, 1}.ImageName);
    if dist > eps || ~imNameBool
        dataBool = 1;
        i = n;
    end
end