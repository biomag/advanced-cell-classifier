function setEnable(handles, state, Children)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     setEnable
%
% Set enable/desable buttons and functions of the GUI if it's busy.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

n = length(Children);
if ~isempty(Children)
    if ~isprop(Children(1), 'LineStyle')
        for i = 1:n
            if ~isprop(Children(i), 'Enable')
                setEnable(handles, state, Children(i).Children)
            elseif isprop(Children(i), 'Enable')
                Children(i).Enable = state;
            end
        end
    end
end
handles.Startpb.Enable = 'on';