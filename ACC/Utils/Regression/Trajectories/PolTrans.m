function polCoorCell = PolTrans(oldCell)
m = length(oldCell);
polCoorCell = cell(m, 1);
for i = 1:m
    [theta, r] = cart2pol(oldCell{i, 1}(:,1)-0.5, oldCell{i, 1}(:,2)-0.5);
    polCoorCell{i, 1} = [theta, r];
end