% sample trajectori

f = 40; % number of frames

samplePath = 'W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\161124_gfpAPC2cM21-P1-A11_MitoSys2\anal2v4_ez3_ez7\';
sampleName = 'TR1_W0001_P0004_T00';
featureNamesFileName = 'W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\140411_KIF11_MitoSys1\anal2v4_ez3_ez7\featureNames.acc';

featureNameList = textread(featureNamesFileName, '%s', 'delimiter', '\n');
featureNameList = strrep(featureNameList,'::','_');

featureTable = table('Size',[f, length(featureNameList)], 'VariableTypes', repmat("double",1,length(featureNameList)));
featureTable.Properties.VariableNames = featureNameList;


for i = 1:f
    featureTable{i,:} = csvread([samplePath, sampleName, num2str(i,'%02d'), '.txt']);
end

F = figure;
F.WindowState ='fullScreen';
selectedFeature = {'nuc_AreaShape_Area'; 'firstDer_ez3_nuc_AreaShape_Area'; 'firstDer_ez7_nuc_AreaShape_Area'};
fID(1) = find(strcmp(selectedFeature(1), featureNameList));
fID(2) = find(strcmp(selectedFeature(2), featureNameList));
fID(3) = find(strcmp(selectedFeature(3), featureNameList));

X(:,1) = featureTable{:,fID(1)};
X(:,2) = featureTable{:,fID(2)};
X(:,3) = featureTable{:,fID(3)};




orig1 = plot(X(:, 1),'k');
hold on
plot(X(:, 2),'k');
plot(X(:, 3),'k');
ax1 = gca;
ax1.XLim = [-2,45];
ax1.YLim = [-2000, 5000];
ax1.XLabel.String = 'Frames';
ax1.YLabel.String = 'Nuc area';

%%
%Do central 3-points 1st order derivatives
central3 = zeros(1,f);
for k=2:f-1
    central3(k) = (X(k+1, 1) - X(k-1, 1))/2;
end
%Handle case for the extremes
% start of traj
central3(1) = (-3/2)*X(1, 1) + 2*X(2, 1) - X(3, 1)/2;
% central3(1) = (X(2) - X(1))/2;
% end of traj
central3(f) = (3/2)*X(f, 1) - 2*X(f-1, 1) + X(f-2, 1)/2;
% central3(f) = (X(f) - X(f-1))/2;


%%
%Do all central 5-points 1st order derivatives
central5 = zeros(1,f);
for k=3:f-2
    central5(k) = X(k-2, 1)/12 - X(k-1, 1)*8/12 + X(k+1, 1)*8/12 - X(k+2, 1)/12;
end

%Handle case for the extremes
% start #t(1) of traj
central5(1) = (-25*X(1, 1) + 48*X(1+1, 1) - 36*X(1+2, 1) + 16*X(1+3, 1) - 3*X(1+4, 1))/12;
% start #t(2) of traj
central5(2) = (-3*X(2-1, 1) - 10*X(1+0, 1) + 18*X(2+1, 1) - 6*X(2+2, 1) + 1*X(2+3, 1))/12;

% end #t(n-1) of traj
central5(f-1) = (-1*X(f-4, 1) + 6*X(f-3, 1) - 18*X(f-2, 1) + 10*X(f-1, 1) + 3*X(f, 1))/12;
% end #t(n)of traj
central5(f) = (25*X(f, 1) - 48*X(f-1, 1) + 36*X(f-2, 1) - 16*X(f-3, 1) + 3*X(f-4, 1))/12;
%%

%Do all #t3 derivatives in the middle
old5 = zeros(1,f);

for k=4:f-3
    old5(k) = (X(k+3, 1) - X(k-3, 1))/2;
end


%Handle case for the extremes

old5(1) = (X(4, 1) - X(1, 1))/2;
old5(2) = (X(5, 1) - X(1, 1))/2;
old5(3) = (X(6, 1) - X(1, 1))/2;
old5(f-2) = (X(f, 1) - X(f-5, 1))/2;
old5(f-1) = (X(f, 1) - X(f-4, 1))/2;
old5(f) = (X(f, 1) - X(f-3, 1))/2;

%%
%Do mean calculation


hold on
tang3 = plot(central3, 'r');
tang5 = plot(central5, 'g');
otang5 = plot(old5, 'b');
drawnow;
aniMovie(1:3) = getframe(F);
pause(0.5)
t3 = [X(1, 1)-central3(1)*2, X(1, 1)-central3(1), X(1, 1), X(1, 1)+central3(1), X(1, 1)+central3(1)*2];
tt3 = plot([-1, 0, 1, 2, 3], t3, 'r', 'LineWidth', 2);

t5 = [X(1, 1)-central5(1)*2, X(1, 1)-central5(1), X(1, 1), X(1, 1)+central5(1), X(1, 1)+central5(1)*2];
tt5 = plot([-1, 0, 1, 2, 3], t5, 'g', 'LineWidth', 2);

ot5 = [X(1, 1)-old5(1)*2, X(1, 1)-old5(1), X(1, 1), X(1, 1)+old5(1), X(1, 1)+old5(1)*2];
ott5 = plot([-1, 0, 1, 2, 3], ot5, 'b', 'LineWidth', 2);

orig1.Marker = '*';
orig1.MarkerEdgeColor = 'black';
orig1.MarkerIndices = 1;


legend({'Nuc area', 'cenrtal3', 'central5', 'dummy5'});
plot([-2, 45], [0 0], 'k', 'LineWidth', 0.05);

aniMovie(4:6) = getframe(F);
pause(0.5)
y = plot([1 1], [5000 -1200], 'k', 'LineWidth', 0.05);


for i = 2:f
t3 = [X(i, 1)-central3(i)*2, X(i, 1)-central3(i), X(i, 1), X(i, 1)+central3(i), X(i, 1)+central3(i)*2 ];
tt3.XData = tt3.XData+1;
tt3.YData = t3;

t5 = [X(i, 1)-central5(i)*2, X(i, 1)-central5(i), X(i, 1), X(i, 1)+central5(i), X(i, 1)+central5(i)*2 ];
tt5.XData = tt5.XData+1;
tt5.YData = t5;

ot5 = [X(i, 1)-old5(i)*2, X(i, 1)-old5(i), X(i, 1), X(i, 1)+old5(i), X(i, 1)+old5(i)*2];
ott5.XData = ott5.XData+1;
ott5.YData = ot5;

y.XData = y.XData + 1;

orig1.MarkerIndices = i;
drawnow;
pause(0.1)
aniMovie((3*i+1):(3*i+3)) = getframe(F);
pause(0.1)
end

% filename = 'DerivPlot';
% 
% v = VideoWriter(['C:\BRC\ACCnew\ACC\Utils\Regression\Trajectories\', filename],'MPEG-4');
% v.FrameRate = 5;
% open(v)
% wb = waitbar(0,'Please wait...','Name','Rendering your video');
% L = length(aniMovie);
% for i = 1:L
%     if mod(i,10)==0
%         waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
%         pause(0.1)
%     end
%     writeVideo(v, aniMovie(i))
% end
% close(wb)
% close(v)
% % close(F)