function calcSliderValue(pTPC, sliderID)
% AUTHOR:   Attila Beleon
% DATE:     May 11, 2020
% NAME:     calcSliderValue
%
% Calculates the selected property for each trajectory and change the proper
% column of pTPC.A that contains the filering values attached to the slider.
%
% INPUTS
%
%       pTPC  - property class, contains the necessary data
%
%       sliderID - numerical 1 or 2, define the slider
%
% OUTPUT
%
%       This function modifies the pTPC.A cell array.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

if sliderID == 1
    sl = 4;
    sliderFun = pTPC.slider_1_Fun;
elseif sliderID == 2
    sl = 3;
    sliderFun = pTPC.slider_2_Fun;
end

N = pTPC.n;

if strcmp(sliderFun{1}, 'Trajectory Level')
    
    switch sliderFun{2}
        
        case 'Distance from origin'
            for i = 1:N
                pTPC.A{i, sl} = norm(pTPC.A{i, 1}(1,:)-pTPC.A{i, 1}(end,:));
            end
            
        case 'Number of Frames'
            for i = 1:N
                pTPC.A{i, sl} = size(pTPC.A{i, 1}, 1);
            end
            
        case 'Length of trajectory'
            for i = 1:N
                sumLength = 0;
                for j=1:size(pTPC.A{i, 1},1)-1
                    sumLength = sumLength + norm(pTPC.A{i, 1}(j+1,:) - pTPC.A{i, 1}(j,:));
                end
                pTPC.A{i, sl} = sumLength;
            end
        case 'Distance from coordinate'
            
            O = [sliderFun{4}, sliderFun{5}];
            
            if strcmp(sliderFun{3}, 'Max')
                
                for i = 1:N
                    pTPC.A{i, sl} = max(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            elseif strcmp(sliderFun{3}, 'Min')
                
                for i = 1:N
                    pTPC.A{i, sl} = min(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            elseif strcmp(sliderFun{3}, 'Mean')
                
                for i = 1:N
                    pTPC.A{i, sl} = mean(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            elseif strcmp(sliderFun{3}, 'Median')
                
                for i = 1:N
                    pTPC.A{i, sl} = median(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            elseif strcmp(sliderFun{3}, 'Range')
                
                for i = 1:N
                    pTPC.A{i, sl} = range(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            elseif strcmp(sliderFun{3}, 'Sum')
                
                for i = 1:N
                    pTPC.A{i, sl} = sum(vecnorm(O - pTPC.A{i, 1},2,2));
                end
                
            else
                errordlg('Something extraordinary happened. Sorry for that, you are awesome!')
            end
            
            
        otherwise
            errordlg('Something extraordinary happened. Sorry for that, you are awesome!')
    end
    
elseif strcmp(sliderFun{1}, 'Cell Level')
    
    global CommonHandles;
    featureNameFile = fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, CommonHandles.MetaDataFolder, 'featureNames.acc');
    featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
    featureIdx = find(strcmp(sliderFun{2}, featureNameList));
    
    if isempty(pTPC.featureTenzor)
        message = sprintf('Calculating... Please wait!\n\nThis calculation can take long,but necessary only once,\nthen the data will be stored for further selections.');
        wb = waitbar(0, message);
        
        numOfFeatures = length(CommonHandles.SelectedMetaData);
        featureTenzor = zeros(N, pTPC.maxF, numOfFeatures);
        
        for i = 1:N
            j2Cstructs = pTPC.A{i,14};
            featureArray = zeros(size(pTPC.A{i, 1},1),numOfFeatures);
            for j=1:size(pTPC.A{i, 1},1)
                if ~isempty(j2Cstructs{j})
                    cellID = j2Cstructs{j}.CellNumber;
                    jumpToCell(j2Cstructs{j},false, 'loadType', 'onlyMeta');
                    featureArray(j, :) = CommonHandles.SelectedMetaData(cellID,:);
                else
                    featureArray(j, :) = nan;
                end
            end

            validCellFilter = pTPC.A{i,15}(logical(pTPC.A{i,13}));
            featureTenzor(i, validCellFilter, :) = featureArray(logical(pTPC.A{i,13}),:);
            invalidFilter = setdiff(1:pTPC.maxF,validCellFilter);
            featureTenzor(i, invalidFilter, :) = NaN;
            if ishandle(wb)
                waitbar(i/N,wb, message);
            end
        end
        waitbar(i/N,wb, message);
        pTPC.featureTenzor = featureTenzor;
        if ishandle(wb)
            close(wb)
        end
    end

    
    if strcmp(sliderFun{3}, 'Max')
        pTPC.A(:, sl) = num2cell(max(pTPC.featureTenzor(:,:,featureIdx), [], 2, 'omitnan'));

    elseif strcmp(sliderFun{3}, 'Min')
        pTPC.A(:, sl) = num2cell(min(pTPC.featureTenzor(:,:,featureIdx), [], 2, 'omitnan'));
        
    elseif strcmp(sliderFun{3}, 'Mean')
        pTPC.A(:, sl) = num2cell(mean(pTPC.featureTenzor(:,:,featureIdx), 2, 'omitnan'));
        
    elseif strcmp(sliderFun{3}, 'Median')
        pTPC.A(:, sl) = num2cell(median(pTPC.featureTenzor(:,:,featureIdx), 2, 'omitnan'));
        
    elseif strcmp(sliderFun{3}, 'Range')
        pTPC.A(:, sl) = num2cell(range(pTPC.featureTenzor(:,:,featureIdx), 2));
        
    elseif strcmp(sliderFun{3}, 'Sum')
      pTPC.A(:, sl) = num2cell(sum(pTPC.featureTenzor(:,:,featureIdx), 2, 'omitnan'));
        
    else
        errordlg('Something extraordinary happened. Sorry for that, you are awesome!')
    end
else
    errordlg('Something extraordinary happened. Sorry for that, you are awesome!')
end
