baseDir = 'W:\beleon\ACC_Project\Lamellocita\NewMaskExBcorrected2\control\anal2';

featureFiles = dir([baseDir, '\*.txt']);

newDir = [baseDir, '_noMarkers_2f15'];
% mkdir(newDir);

for i = 1:length(featureFiles)
    i
    oldFeatureVector = load(fullfile(baseDir, featureFiles(i).name));
    newFeatureVector = oldFeatureVector(:,[1:46, 119, 122, 125, 128, 131, 134, 138:141]);
    csvwrite(fullfile(newDir, featureFiles(i).name), newFeatureVector);
end

oldFeatureNames = textread(fullfile(baseDir, 'featureNames.acc'), '%s');
newFeatureNames = oldFeatureNames([1:46, 119, 122, 125, 128, 131, 134, 138:141]);
featureNamesTable = table(newFeatureNames);
writetable(featureNamesTable, fullfile(newDir, 'featureNames.acc'), 'FileType', 'text');
