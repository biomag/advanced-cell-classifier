function resetGUI(handles)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     resetGUI
%
% Set all the components of the GUI to default values, and plot
% trajectories.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

pTPC = get(handles.figure1,'UserData');
if size(pTPC.A, 1) == 0
    msgbox('Something went wrong, empty input!','Inputlessness happens');
    return
end

set(handles.figure1,'Pointer','watch');
dispStatus('>> Busy - Plotting trajectories', handles);
pause(0.1);
n = size(pTPC.A, 1);
cla(handles.axesTraj,'reset');
handles.axesTraj.XColor = [1 1 1];
handles.axesTraj.YColor = [1 1 1];
handles.axesTraj.ZColor = [1 1 1];
pTPC.S = 0;                     % radio button selection ID
pTPC.groupList = cell(0);

pTPC.A(:, 9:12) = {true};

calcSliderValue(pTPC, 1)
calcSliderValue(pTPC, 2)

minF = min([pTPC.A{:,3}]);


% Checking the number of frames at column 15 to set the SlideWhow range
col = 0;
row = 0;
ii=1;
while col == row
    [row, col] = size(pTPC.A{ii, 15});
    ii = ii + 1;
end
if row < col
    tempMatFrameIDs = cell2mat(pTPC.A(:, 15)');
else
    tempMatFrameIDs = cell2mat(pTPC.A(:, 15));
end

maxFrameID = max(tempMatFrameIDs);
minFrameID = min(tempMatFrameIDs);
handles.slideShowSlider.Max = maxFrameID;
handles.slideShowSlider.Min = minFrameID;
handles.slideShowSlider.Value = maxFrameID;
handles.slideShowSlider.SliderStep(1) = 1/(maxFrameID-1);
handles.slideShowCurrentValue.String = num2str(floor(maxFrameID));
handles.maxFrameText2.String = num2str(maxFrameID);
handles.meanFrameText2.String = num2str(round((maxFrameID)/2));
handles.minFrameText2.String = num2str(minFrameID);

% Close all LineProp window
linePropHandles = pTPC.linePropHandles;
for i=1:numel(linePropHandles)
    if ~isempty(ishandle(linePropHandles{i}))
        if isvalid(linePropHandles{i})
            close(linePropHandles{i});
        else
            linePropHandles{i} = {[]};
        end
    end
end

% Reset Group Toggle buttons Value, Colour and attached group informations
set(handles.uipanel_toogleGroup.Children, 'BackgroundColor', [0.94 0.94 0.94]);
set(handles.uipanel_toogleGroup.Children, 'ForegroundColor', [0 0 0]);
set(handles.uipanel_toogleGroup.Children, 'FontWeight', 'normal');
set(handles.uipanel_toogleGroup.Children, 'Value', 0);
set(handles.uipanel_toogleGroup.Children, 'UserData', []);
    

pTPC.lines = gobjects(pTPC.n, 1);                  % Trajectory lines
pTPC.statLine = gobjects(1, 1);                    % Statistical trend lines
handles.SelectionTypeButtonGroup1.SelectedObject = [];               % reset Radio button
handles.SelectionTypeButtonGroup2.SelectedObject = [];               % reset Radio button
handles.numOfTraj.String = {'Number of trajectories:' pTPC.n};

% Plotting trajectories
for i=1:n
    B{i, 1} = pTPC.A{i, 1}(:,1);
    B{i, 2} = pTPC.A{i, 1}(:,2);
end

hold on

pTPC.lines = cellfun(@plot, B(:,1), B(:,2),...
    repmat({'-o'},n,1),...
    repmat({'MarkerIndices'},n,1),...
    num2cell(cellfun(@length,B(:,1))),...
    repmat({'ButtonDownFcn'},n,1),...
    repmat({@lineCallback},n,1),...
    repmat({'Tag'},n,1),...
    cellfun(@num2str,num2cell(1:n),'UniformOutput',false)');


% Set axes properties
handles.axesTraj.XLim = [0 1];
handles.axesTraj.XLimMode = 'manual';
handles.axesTraj.YLim = [0 1];
handles.axesTraj.YLimMode = 'manual';

handles.CoorTable1.Data = [];
handles.CoorTable2.Data = [];
handles.pbFreeHand1.BackgroundColor = [0.250 0.250 0.250];
handles.pbFreeHand1.ForegroundColor = [1 1 1];
handles.pbFreeHand2.BackgroundColor = [0.250 0.250 0.250];
handles.pbFreeHand2.ForegroundColor = [1 1 1];

% Line object UserData contains informationss of Group memberships of the
% trajectory, this section sets a clean structure
% 
% UserData{1} - contains group indexes of which the Traj belongs to
% UserData{2} - 1-by-3 array contains the original color of the trajectory
% UserData{3} - line width
% UserData{4} - groupIDs that highlights actually applied on trajectory
% UserData{5} - bool value that indicates is the Line Porperty opened or not


[pTPC.lines.UserData] = deal(cell(5, 1));

for i = 1:n
    pTPC.lines(i).UserData{2} = pTPC.lines(i).Color;
    pTPC.lines(i).UserData{3} = pTPC.lines(i).LineWidth;
end


setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);

% setting SLIDER 1&2 parameters


maxD = max([pTPC.A{:,4}]);
minD = floor(min([pTPC.A{:,4}]));
handles.slider_1.Max = maxD;
handles.slider_1.Min = minD;
handles.act_slider_1_EditText.String = num2str(minD, 2);
handles.max_slider_1_Text.String = num2str(maxD, 2);
handles.mid_slider_1_Text.String = num2str((maxD-minD)/2, 2);
handles.min_slider_1_Text.String = num2str(minD, 2);
handles.slider_1.Value = minD;

handles.slider_1_Name.String = 'Distance from Origin';


handles.slider_2_Name.String = 'Number of Frames';

if pTPC.maxF == minF
    handles.slider_2.Enable = 'off';
    handles.min_slider_2_Text.String = '';
    handles.max_slider_2_Text.String = '';
    handles.mid_slider_2_Text.String = '';
    handles.act_slider_2_EditText.FontSize = 8;
    handles.act_slider_2_EditText.FontWeight = 'normal';
    handles.act_slider_2_EditText.ForegroundColor = [0.5 0.5 0.5];
    handles.act_slider_2_EditText.String = 'Inactive';
    handles.act_slider_2_EditText.Enable = 'off';
else
    handles.slider_2.Enable = 'on';
    handles.act_slider_2_EditText.ForegroundColor = [0 0 1];
    handles.act_slider_2_EditText.Enable = 'on';
    handles.slider_2.Max = pTPC.maxF;
    handles.slider_2.Min = minF;
    handles.act_slider_2_EditText.String = num2str(minF);
    handles.max_slider_2_Text.String = num2str(pTPC.maxF);
    handles.mid_slider_2_Text.String = num2str(round((pTPC.maxF+minF)/2));
    handles.min_slider_2_Text.String = num2str(minF);
    handles.slider_2.SliderStep = [0.01 0.1];
    handles.slider_2.Value = minF;
    handles.slider_2.SliderStep(1) = 1/(pTPC.maxF);
end

nameList = cell(pTPC.n,3);
for i = 1:pTPC.n
    nameList(i,:) = strsplit(pTPC.A{i,14}{1,1}.ImageName,filesep);
end
uniquePlates = unique(nameList(:,1));

for j = 1:size(uniquePlates,1)
    idx = find(contains(nameList(:,1),uniquePlates{j}));
    grouping('Create new', uniquePlates(j), handles, 'TrajIDs', idx);
end

if strcmp(handles.slider_2.Enable, 'off')
    msgbox('Frame slider is inactivated, because the number frames are equal for every trajectories.')
end