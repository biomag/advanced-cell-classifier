function [mergedC]=convert(pathToFile,maxdisp)

%MAXDISP works fine between 55-64, higher value cause combinatoric error,
%lower value cause less accurate ID prediction.

%param is an input structur for the tracks function
param = struct('mem',{2},'good',{2},'dim',{2},'quiet',{1});



if nargin<1
    prompt = 'Enter DIR path!';
    str = input(prompt,'s');
else
    str = pathToFile;
end



d=dir([str '\*.txt']);
n=length(d);
fileNames = strings(n,1);


for i=1:n
fileNames(i,1) = d(i).name;
end

separatedFilaNames = strings(n,6);

for j=1:n
    separatedFilaNames(j,:) = strsplit(fileNames(j),{'f','p','sk','fk'},'CollapseDelimiters',true);
end

%% fieldIDs - ix1 tring array

%  mergedC - ix1 cell array, contains jx3 matrices, x,y coordinates and frameID inside

% cres - ix1 cell array, countains jx5 matrices, x,y coordinates, frameID
% inside


fieldIDs = unique(separatedFilaNames(:,2));
mergedC = cell(length(fieldIDs), 1);
cres = cell(length(fieldIDs), 1);

% currFieldIDs - jx15 string array, contains the of the files with the
% same id

% currFrameIDs - double type array, contains the all the frameIDs on a
% filed

% imageName = [];

for i = 1:length(fieldIDs)
    currFieldIDs = fileNames(separatedFilaNames(:,2) == fieldIDs(i));
    currFrameIDs = str2double(separatedFilaNames(separatedFilaNames(:,2) == fieldIDs(i),4));
    C = cell(length(currFieldIDs), 1);
    %imageName = [imageName; currFieldIDs];
    for j=1:numel(currFieldIDs)       
        
        temp = csvread([str filesep currFieldIDs{j}]);
        m_temp = length(temp(:,1));
        inputmap = zeros(m_temp, 8);
        inputmap(:,1) = temp(:,1);          %x coordinates
        inputmap(:,2) = temp(:,2);          %y coordinates
        inputmap(:,3) = currFrameIDs(j);    %Frame ID
        inputmap(:,4) = temp(:,3);          %Area
        inputmap(:,5) = temp(:,14);         %Mean intesity green
        inputmap(:,6) = temp(:,17);         %Max intesity green
        inputmap(:,7) = temp(:,25);         %Mean intesity red
        inputmap(:,8) = temp(:,28);         %Max intesity red
        C{j} = inputmap;
        
    end
    
    % C - jx1 cell array contains jx10 matraces, contains x,y coordinates
    % and the famreID
    
     mergedC{i} = cell2mat(C);
     
     % R - output from tracks function, it is a double type matrix, contains x,y coordinates, frameID, TrackID, FieldID. 
     
     
     R = tracks(mergedC{i},maxdisp,param);
     R = [R ones(size(R,1),1).*str2double(fieldIDs(i))];
     R(:,9) = R(:,9) + 1000*R(:,10);
     cres{i} = R;
     

     
end
%% This part creating the .csv output
res = cell2mat(cres);
imName = strings(length(res), 1);

for i=1:length(res)
    imName(i, 1) = ['r01c01f' num2str(res(i, 5),'%02d') 'p01-ch1sk' num2str(res(i, 3)) 'fk1fl1.tiff'];
end


headers = {'Plate','Frame','Field','TrackID', ...
              'tracking__center_x','tracking__center_y', ...
              'ImageName', 'Area', 'MeanIntesityGreen', ...
              'MaxIntesityGreen', 'MeanIntesityRed', 'MaxIntesityRed'};
          
data = table(repmat({'ACCPlateFolder'},length(res),1), ...
              res(:,3), res(:,10), res(:,9), ...
              round(res(:,1)), round(res(:,2)), ...
              imName, res(:,4), res(:,5), res(:,6), res(:,7), res(:,8),...
              'VariableNames', headers);
        
              
filename = ['output_',num2str(max(str2double(fieldIDs))),'Filds_maxdisp',num2str(maxdisp),'.csv'];

writetable(data, filename);


end

