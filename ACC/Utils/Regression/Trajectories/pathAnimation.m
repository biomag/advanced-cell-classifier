function pathAnimation(CellImg, animResolution, tail, trajID, x, y)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     pathAnimation
%
% Set all the components of the GUI to default values, and plot
% trajectories.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.
%
% INPUT
%   CellImg         - cellarray contains RGB image of the current cell of each
%                     frame.
%   animResolution  - number of steps interpolated between timepoints of the cell path
%                     larger value makes the animation slower.
%
%   trajID          - ID of the selected trajectory
%
%   x, y            - coordinates to plot the line


% Parse possible Axes input

narginchk(5,7)

x = matlab.graphics.chart.internal.datachk(x);
y = matlab.graphics.chart.internal.datachk(y);

f = uifigure('Units', 'pixels', 'AutoResizeChildren', 'off', 'HitTest', 'off', 'Name', 'Single Cell Animation');
f.Position(3:4) = [800 400];
ax1 = subplot(1,2,1, 'Parent', f, 'HitTest', 'off');
disableDefaultInteractivity(ax1);
imSizeArray = zeros(length(CellImg), 3);

for b = 1:length(CellImg)
    imSizeArray(b, :) = size(CellImg{b});
end

maxSize = max(max(imSizeArray));

ax1.XLim = [0, maxSize];
ax1.YLim = [0, maxSize];
title(ax1,['Frame ', '1']);
ax1.TitleFontSizeMultiplier = 2;
ax1.DataAspectRatio = [1 1 1];

ax2 = subplot(1,2,2, 'Parent', f, 'HitTest', 'off');
title(ax2,['Trajectory ID: ', trajID]);
ax2.XLim = [0, 1];
ax2.YLim = [0, 1];
ax2.TitleFontSizeMultiplier = 2;
ax2.DataAspectRatio = [1 1 1];
disableDefaultInteractivity(ax2);

m = length(x);
k = tail * animResolution;
frameLength = floor(m / (length(CellImg)-1));

playB = uibutton(f);
playB.Position = [30, 20, 100, 25];
playB.Text = 'Play';
playB.Enable = 'off';
playB.ButtonPushedFcn = {@play_Callback, x, y, frameLength, animResolution, tail};

head = line('parent',ax2,'marker','o','linestyle','none',...
    'MarkerSize', 10, 'xdata',x(1),'ydata',y(1),'Tag','head');

body = matlab.graphics.animation.AnimatedLine('Parent',ax2,...
    'LineWidth', 2,...
    'MaximumNumPoints',max(1,k),'tag','body');
try
    % Draw images to ax1 and set Aplha to 0
    im = cell(length(CellImg), 1);
    for i = 1:length(CellImg)
        imsize = size(CellImg{i});
        im(i) = {image(ax1,...
            'XData',[(ax1.XLim(2) - imsize(1))/2, (ax1.XLim(2) + imsize(1))/2],...
            'YData',[(ax1.YLim(2) - imsize(2))/2, (ax1.YLim(2) + imsize(2))/2],...
            'CData',flip(CellImg{i},1),...
            'HitTest', 'off',...
            'AlphaData', 0)};
    end
    im{1}.AlphaData = 1;
    jj = 2;
    
    % Elongate the Line
    for i = 1:k
        
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        if mod(i, frameLength) == 0
            im{jj}.AlphaData = 1;
            title(ax1, ['Frame ', num2str(jj)]);
            jj = jj + 1;
        else
            im{jj}.AlphaData = mod(i, frameLength) / animResolution;
        end
        drawnow
    end
    
    % Animate the line moving
    for i = k+1:m-1
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        
        if mod(i, frameLength) == 0
            im{jj}.AlphaData = 1;
            title(ax1, ['Frame ', num2str(jj)]);
            jj = jj + 1;
        else
            im{jj}.AlphaData = mod(i, frameLength) / animResolution;
        end
        drawnow
    end
    set(head,'xdata',x(end),'ydata',y(end));
    addpoints(body,x(end),y(end));
catch
    errordlg('Animation processing is faild', 'Error')
end

playB.Enable = 'on';

function play_Callback(hObject, eventData, x, y, frameLength, animResolution, tail)


hObject.Enable = 'off';
handles = hObject.Parent;
ax1 = handles.Children(3);
ax2 = handles.Children(2);
im = flip(handles.Children(3).Children);
set(im, 'AlphaData', 0);
im(1).AlphaData = 1;
m = length(x);
k = tail * animResolution;
jj = 2;

cla(ax2)

head = line('parent',ax2,'marker','o','linestyle','none',...
    'MarkerSize', 10, 'xdata',x(1),'ydata',y(1),'Tag','head');

body = matlab.graphics.animation.AnimatedLine('Parent',ax2,...
    'LineWidth', 2,...
    'MaximumNumPoints',max(1,k),'tag','body');

% Elongate the Line
    for i = 1:k
        
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        if mod(i, frameLength) == 0
            im(jj).AlphaData = 1;
            title(ax1, ['Frame ', num2str(jj)]);
            jj = jj + 1;
        else
            im(jj).AlphaData = mod(i, frameLength) / animResolution;
        end
        drawnow
    end
    
    % Animate the line moving
    for i = k+1:m-1
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        
        if mod(i, frameLength) == 0
            im(jj).AlphaData = 1;
            title(ax1, ['Frame ', num2str(jj)]);
            jj = jj + 1;
        else
            im(jj).AlphaData = mod(i, frameLength) / animResolution;
        end
        drawnow
    end
    set(head,'xdata',x(end),'ydata',y(end));
    addpoints(body,x(end),y(end));
hObject.Enable = 'on';