classdef plotTrajPropClass < handle    
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     plotTrajPropClass
%
% Data storege class for Trajectory GUI
% 
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.
    properties
        A;
        n;
        S = zeros(2, 1);
        lines;
        statLine;
        freehandPolygon1;
        freehandPolygon2;
        dispT = cell(5, 1);
        maxF; %max Frame
        imgHandles;
        linePropHandles;
        groupList;
        memberList;
        slider_1_Fun = {'Trajectory Level','Distance from origin',[],[],[]};
        slider_2_Fun = {'Trajectory Level','Number of Frames',[],[],[]};
        trackingFile
        featureTenzor
    end
        
end

