function varargout = plotTrajGui(varargin)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     plotTrajGui
%
% Interpolate new points between regression positions of the currently selected cell.
% These points are necessary to make the animation more smooth. 
%
% INPUTS
% varargin  -  n-by-15 cell array created by "openTrajectoryGUI.m' that contains:
%              - coordinates of regression position
%              - total length of trajectory
%              - number of steps (frames) of trajextory
%              - distance between start coordinate and end coordinate
%              - highest x value
%              - highest y velue
%              - start point (coordinate)
%              - end point (coordinate)
%              - visibility 1 (boolean)
%              - visibility 2 (boolean)
%              - visibility 3 (boolean)
%              - visibility 4 (boolean)
%              - bool array indicating if a position is real or interpolated
%              - 1-by-NumOfFrames array contains structs as information about
%                 each frame (plate name, image name, original image name,
%                 cell ID)
%              - array contains frame IDs of real (not relative)
%                 coordinates
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.
%
% PLOTTRAJGUI MATLAB code for plotTrajGui.fig
%      PLOTTRAJGUI, by itself, creates a new PLOTTRAJGUI or raises the existing
%      singleton*.
%
%      H = PLOTTRAJGUI returns the handle to a new PLOTTRAJGUI or the handle to
%      the existing singleton*.
%
%      PLOTTRAJGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PLOTTRAJGUI.M with the given input arguments.
%
%      PLOTTRAJGUI('Property','Value',...) creates a new PLOTTRAJGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before plotTrajGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to plotTrajGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help plotTrajGui

% Last Modified by GUIDE v2.5 16-Jul-2020 20:12:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @plotTrajGui_OpeningFcn, ...
                   'gui_OutputFcn',  @plotTrajGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before plotTrajGui is made visible.
function plotTrajGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to plotTrajGui (see VARARGIN)

% Choose default command line output for plotTrajGui
handles.output = hObject;

A = varargin{1};        % input data
n = size(A,1);          % length of input data array

pTPC = plotTrajPropClass();
pTPC.A = A;
pTPC.n = n;
pTPC.maxF = max([pTPC.A{:,3}]);
set(handles.figure1,'UserData',pTPC);
handles.CoorPanel2.Position = handles.CoorPanel1.Position;
handles.groupToggleReset.Position(1) = 0.06;
handles.groupToggleReset.Position(2) = 0.942;
handles.CoorTable2.Position = handles.CoorTable1.Position;
handles.SelectionTypeButtonGroup2.Position = handles.SelectionTypeButtonGroup1.Position;
pTPC.imgHandles = cell(1,size(A,1));
guidata(hObject,handles);
handles.axesTraj.HitTest = 'off';
setEnable(handles,'off',handles.figure1.Children);



% UIWAIT makes plotTrajGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = plotTrajGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in Startpb.
function Startpb_Callback(hObject, eventdata, handles)
% hObject    handle to Startpb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isempty(handles.axesTraj.Children)
    answer = questdlg('Would you like to reset the GUI? (You will lose all the filtered data, and group options)', ...
        'Reset', ...
        'Yes','No','No');
    % Handle response
    switch answer
        case 'Yes'
            resetGUI(handles)
            
        otherwise
            return
    end
else
    resetGUI(handles)
end

% --- Executes on button press in Generatepb.
function Generatepb_Callback(hObject, eventdata, handles)
% hObject    handle to Generatepb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
if (isempty(pTPC.freehandPolygon1)||~ishandle(pTPC.freehandPolygon1)) && (isempty(pTPC.freehandPolygon2) || ~ishandle(pTPC.freehandPolygon2))
    dispStatus('>> Please select a region by freehand to filter', handles);
    return; 
end

setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');

if ishandle(pTPC.freehandPolygon1)
    if isempty(handles.SelectionTypeButtonGroup1.SelectedObject)
        dispStatus('Select radio button first!', handles);
        msgbox('Select radio button first of Coordinate Selection 1 panel!')
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        return
    end
    dispStatus('>> Busy - Generating selection...', handles);
    pause(0.1);
    
    xv1 = pTPC.freehandPolygon1.Position(:,1);
    yv1 = pTPC.freehandPolygon1.Position(:,2);
    
    switch pTPC.S(1, 1)
        case 1
            b = cell2mat(pTPC.A(:,7));
            a = inpolygon(b(:, 1),b(:, 2),xv1,yv1);
            if handles.coorNeg_checkbox1.Value
                pTPC.A(:,9) = num2cell(~a);
            else
                pTPC.A(:,9) = num2cell(a);
            end
            
        case 2
            for i=1:pTPC.n
                if handles.coorNeg_checkbox1.Value
                    pTPC.A{i,9} = ~any(inpolygon(pTPC.A{i,1}(:, 1),pTPC.A{i,1}(:, 2),xv1,yv1));
                else
                    pTPC.A{i,9} = any(inpolygon(pTPC.A{i,1}(:, 1),pTPC.A{i,1}(:, 2),xv1,yv1));
                end
            end
            
        case 3
            b = cell2mat(pTPC.A(:,8));
            a = inpolygon(b(:, 1),b(:, 2),xv1,yv1);
            if handles.coorNeg_checkbox1.Value
                pTPC.A(:,9) = num2cell(~a);
            else
                pTPC.A(:,9) = num2cell(a);
            end
    end
end

if ishandle(pTPC.freehandPolygon2)
    if isempty(handles.SelectionTypeButtonGroup2.SelectedObject)
        dispStatus('Select radio button first!', handles);
        msgbox('Select radio button first of Coordinate Selection 2 panel!')
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        return
    end
    dispStatus('>> Busy - Generating selection...', handles);
    pause(0.1);
    
    xv2 = pTPC.freehandPolygon2.Position(:,1);
    yv2 = pTPC.freehandPolygon2.Position(:,2);

    switch pTPC.S(2, 1)
        case 1
            b = cell2mat(pTPC.A(:,7));
            a = inpolygon(b(:, 1),b(:, 2),xv2,yv2);
            if handles.coorNeg_checkbox2.Value
                pTPC.A(:,10) = num2cell(~a);
            else
                pTPC.A(:,10) = num2cell(a);
            end

        case 2
            for i=1:pTPC.n
                if handles.coorNeg_checkbox2.Value
                    pTPC.A{i,10} = ~any(inpolygon(pTPC.A{i,1}(:, 1),pTPC.A{i,1}(:, 2),xv2,yv2));
                else
                    pTPC.A{i,10} = any(inpolygon(pTPC.A{i,1}(:, 1),pTPC.A{i,1}(:, 2),xv2,yv2));
                end
            end
            
        case 3
            b = cell2mat(pTPC.A(:,8));
            a = inpolygon(b(:, 1),b(:, 2),xv2,yv2);
            if handles.coorNeg_checkbox2.Value
                pTPC.A(:,10) = num2cell(~a);
            else
                pTPC.A(:,10) = num2cell(a);
            end
    end
end
    
    
if (~isempty(pTPC.freehandPolygon1) && ishandle(pTPC.freehandPolygon1)) || (~isempty(pTPC.freehandPolygon2) && ishandle(pTPC.freehandPolygon2))
    [visibleTraj, nonVisibleTraj] = checkVisibility(handles);
    set(pTPC.lines(visibleTraj), 'Visible', 1);
    set(pTPC.lines(nonVisibleTraj), 'Visible', 0);
end

delete(pTPC.freehandPolygon1);
handles.pbFreeHand1.BackgroundColor = [0.250 0.250 0.250];
handles.pbFreeHand1.ForegroundColor = [1 1 1];

delete(pTPC.freehandPolygon2);
handles.pbFreeHand2.BackgroundColor = [0.250 0.250 0.250];
handles.pbFreeHand2.ForegroundColor = [1 1 1];


actVisibility = sum(string(get(pTPC.lines, 'Visible')) == "on");
handles.numOfTraj.String = ["Number of trajectories: "; [num2str(actVisibility), ' / ', num2str(pTPC.n)]];
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);


% --- Executes on slider movement.
function slider_1_Callback(hObject, eventdata, handles)
% hObject    handle to slider_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
dispStatus('>> Busy - Generating selection...', handles);
pause(0.1);

value = hObject.Value;

handles.act_slider_1_EditText.String = num2str(value, 4);

get(handles.figure1,'UserData');

boolf = find([pTPC.A{:,4}] < value);
boolt = find([pTPC.A{:,4}] >= value);

if handles.slider_1_checkbox.Value
    pTPC.A(boolf,12) = {true};
    pTPC.A(boolt,12) = {false};
else
    pTPC.A(boolf,12) = {false};
    pTPC.A(boolt,12) = {true};
end
[visibleTraj, nonVisibleTraj] = checkVisibility(handles);
set(pTPC.lines(visibleTraj), 'Visible', 1);
set(pTPC.lines(nonVisibleTraj), 'Visible', 0);

actVisibility = sum(string(get(pTPC.lines, 'Visible')) == "on");
handles.numOfTraj.String = ["Number of trajectories: "; [num2str(actVisibility), ' / ', num2str(pTPC.n)]];
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_2_Callback(hObject, eventdata, handles)
% hObject    handle to slider_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
dispStatus('>> Busy - Generating selection...', handles);
pause(0.1);

value = hObject.Value;

handles.act_slider_2_EditText.String = num2str(value, 4);

boolf = find([pTPC.A{:,3}] < value);
boolt = find([pTPC.A{:,3}] >= value);

if handles.slider_2_checkbox.Value
    pTPC.A(boolf,11) = {true};
    pTPC.A(boolt,11) = {false};
else
    pTPC.A(boolf,11) = {false};
    pTPC.A(boolt,11) = {true};
end

[visibleTraj, nonVisibleTraj] = checkVisibility(handles);
set(pTPC.lines(visibleTraj), 'Visible', 1);
set(pTPC.lines(nonVisibleTraj), 'Visible', 0);

actVisibility = sum(string(get(pTPC.lines, 'Visible')) == "on");
handles.numOfTraj.String = ["Number of trajectories: "; [num2str(actVisibility), ' / ', num2str(pTPC.n)]];
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider_2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end





% --- Executes when selected object is changed in SelectionTypeButtonGroup1.
function SelectionTypeButtonGroup1_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in SelectionTypeButtonGroup1 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUATA)
pTPC = get(handles.figure1,'UserData');

switch eventdata.NewValue.Tag
    case 'Selection1From' , pTPC.S(1, 1) = 1;
        
    case 'Selection1Touch' , pTPC.S(1, 1) = 2;
        
    case 'Selection1To' , pTPC.S(1, 1) = 3;
end



% --- Executes on button press in statButton.
function statButton_Callback(hObject, eventdata, handles)
% hObject    handle to statButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
trajInfo(handles);


% --- Executes during object creation, after setting all properties.
function numOfTraj_CreateFcn(hObject, eventdata, handles)
% hObject    handle to numOfTraj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

    


% --------------------------------------------------------------------
function PrintFigure_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to PrintFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Create a temporary figure with axes.
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
dispStatus('>> Busy - Printing', handles);
pause(0.1);

fig = figure;
fig.Visible = 'off';
figAxes = axes(fig);
% Copy all UIAxes children, take over axes limits and aspect ratio.            
visibleTraj = checkVisibility(handles);
visibleChildrens = handles.axesTraj.XAxis.Parent.Children((pTPC.n+1)-visibleTraj);
copyobj(visibleChildrens, figAxes)
figAxes.XLim = handles.axesTraj.XLim;
figAxes.YLim = handles.axesTraj.YLim;
figAxes.ZLim = handles.axesTraj.ZLim;
figAxes.DataAspectRatio = handles.axesTraj.DataAspectRatio;
print(fig);
% Delete the temporary figure.
delete(fig);
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);


% --------------------------------------------------------------------
function SaveGUI_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to SaveGUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName, path] = uiputfile('TrajectoryGUI.fig');

if path == 0
    setEnable(handles,'on',handles.figure1.Children);
    set(handles.figure1,'Pointer','arrow');
    dispStatus('>> Cancel', handles);
    return
end

savefig(fullfile(path, fileName));


% --------------------------------------------------------------------
function savePNG_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to savePNG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
dispStatus('>> Busy - Creating image', handles);
pause(0.1);

fig = figure;
fig.Visible = 'off';
figAxes = axes(fig);

[fileName, path] = uiputfile('trajScreenShot.png');

if path == 0
    setEnable(handles,'on',handles.figure1.Children);
    set(handles.figure1,'Pointer','arrow');
    dispStatus('>> Cancel', handles);
    return
end

% Copy all UIAxes children, take over axes limits and aspect ratio.   
visibleTraj = checkVisibility(handles);
visibleChildrens = handles.axesTraj.XAxis.Parent.Children((pTPC.n+1)-visibleTraj);
copyobj(visibleChildrens, figAxes)
figAxes.XLim = handles.axesTraj.XLim;
figAxes.YLim = handles.axesTraj.YLim;
figAxes.ZLim = handles.axesTraj.ZLim;
figAxes.DataAspectRatio = handles.axesTraj.DataAspectRatio;
title(fileName);
% Save image

saveas(fig, fullfile(path, fileName));

% Delete the temporary figure.
delete(fig);
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');
dispStatus('>> Done!', handles);




% --- Executes on button press in pbFreeHand2.
function pbFreeHand2_Callback(hObject, eventdata, handles)
% hObject    handle to pbFreeHand2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hObject.Enable = 'off';
setEnable(handles,'off',handles.figure1.Children);
pTPC = get(handles.figure1,'UserData');

dispStatus('>> Select area!', handles);
handles.pbFreeHand2.BackgroundColor = [1 0 0];
handles.pbFreeHand2.ForegroundColor = [0 0 0];
if ishandle(pTPC.freehandPolygon2), delete(pTPC.freehandPolygon2); end
pTPC.freehandPolygon2 = drawpolygon(handles.axesTraj, 'Color', [1 0 0]);
handles.CoorTable2.Data = pTPC.freehandPolygon2.Position;

dispStatus('>> Done!', handles);
hObject.Enable = 'on';
setEnable(handles,'on',handles.figure1.Children);



% --- Executes on button press in pbFreeHandTab1.
function pbFreeHandTab1_Callback(hObject, eventdata, handles)
% hObject    handle to pbFreeHandTab1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.CoorPanel1.Visible = 'off';
handles.CoorPanel1.Visible = 'on';
handles.pbFreeHandTab1.BackgroundColor = [0.3500 0.3500 0.3500];
handles.pbFreeHandTab2.BackgroundColor = [0.2500 0.2500 0.2500];



% --- Executes on button press in pbFreeHandTab2.
function pbFreeHandTab2_Callback(hObject, eventdata, handles)
% hObject    handle to pbFreeHandTab2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.CoorPanel2.Visible = 'on';
handles.CoorPanel1.Visible = 'off';
handles.pbFreeHandTab1.BackgroundColor = [0.2500 0.2500 0.2500];
handles.pbFreeHandTab2.BackgroundColor = [0.3500 0.3500 0.3500];


% --- Executes on button press in pbFreeHand1.
function pbFreeHand1_Callback(hObject, eventdata, handles)
% hObject    handle to pbFreeHand1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hObject.Enable = 'off';
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
dispStatus('>> Select area!', handles);
handles.pbFreeHand1.BackgroundColor = [0 1 0];
handles.pbFreeHand1.ForegroundColor = [0 0 0];
if ishandle(pTPC.freehandPolygon1), delete(pTPC.freehandPolygon1); end
pTPC.freehandPolygon1 = drawpolygon(handles.axesTraj, 'Color', [0 1 0]);
handles.CoorTable1.Data = pTPC.freehandPolygon1.Position;
dispStatus('>> Done!', handles);
hObject.Enable = 'on';
setEnable(handles,'on',handles.figure1.Children);


% --- Executes when selected object is changed in SelectionTypeButtonGroup2.
function SelectionTypeButtonGroup2_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in SelectionTypeButtonGroup2 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');

switch eventdata.NewValue.Tag
    case 'Selection2From' , pTPC.S(2, 1) = 1;
        
    case 'Selection2Touch' , pTPC.S(2, 1) = 2;
        
    case 'Selection2To' , pTPC.S(2, 1) = 3;
end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

pTPC = hObject.UserData;
linePropHandles = pTPC.linePropHandles;

for i=1:numel(linePropHandles)
    if ishandle(linePropHandles{i}), close(linePropHandles{i}); end
end


delete(hObject);


% --- Executes on slider movement.
function slideShowSlider_Callback(hObject, eventdata, handles)
% hObject    handle to slideShowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');

actualFrame = eventdata.Source.Value;
handles.slideShowCurrentValue.String = num2str(floor(actualFrame));
for i = 1:size(pTPC.A,1)
    x = pTPC.A{i,1}(:,1);
    y = pTPC.A{i,1}(:,2);
    frame = pTPC.A{i,15};
    x = x(frame <= actualFrame);
    y = y(frame <= actualFrame);
    if numel(x) == 0
        markerIdx = [];
    else
        markerIdx = numel(x);
    end
    set(pTPC.lines(i), {'XData', 'YData'}, {x, y},...
        'MarkerIndices', markerIdx);
end

drawnow;
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');

% --- Executes during object creation, after setting all properties.
function slideShowText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slideShowText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function act_slider_2_EditText_Callback(hObject, eventdata, handles)
% hObject    handle to act_slider_2_EditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of act_slider_2_EditText as text
%        str2double(get(hObject,'String')) returns contents of act_slider_2_EditText as a double
oldValue = handles.slider_2.Value;
newValue = str2double(eventdata.Source.String);
if ~isnan(newValue) && newValue >= handles.slider_2.Min && newValue <= handles.slider_2.Max
    handles.slider_2.Value = str2double(eventdata.Source.String);
    slider_2_Callback(handles.slider_2,eventdata,handles);
else
    warndlg('Please enter a valid numeric value in the range of the slider!');
    hObject.String = num2str(oldValue);
end

% --- Executes during object creation, after setting all properties.
function act_slider_2_EditText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to act_slider_2_EditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function act_slider_1_EditText_Callback(hObject, eventdata, handles)
% hObject    handle to act_slider_1_EditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of act_slider_1_EditText as text
%        str2double(get(hObject,'String')) returns contents of act_slider_1_EditText as a double
oldValue = handles.slider_1.Value;
newValue = str2double(eventdata.Source.String);
if ~isnan(newValue) && newValue >= handles.slider_1.Min && newValue <= handles.slider_1.Max
    handles.slider_1.Value = str2double(eventdata.Source.String);
    slider_1_Callback(handles.slider_1,eventdata,handles);
else
    warndlg('Please enter a valid numeric value in the range of the slider!');
    hObject.String = num2str(oldValue);
end


% --- Executes during object creation, after setting all properties.
function act_slider_1_EditText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to act_slider_1_EditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function slideShowSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slideShowSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in groupButton.
function groupButton_Callback(hObject, eventdata, handles)
% hObject    handle to groupButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
visibleTraj = checkVisibility(handles);
if isempty(visibleTraj)
    msgbox('There are no visible trajectories on the axes.', 'Empty set');
    return
end

setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');

if isempty(pTPC.groupList)
    dlgButtons = {'Create new','Cancel','Create new'};
else
    dlgButtons = {'Create new','Add to existing','Cancel','Create new'};
end
    
answer1 = questdlg('Would you like to create a new group of the visible trajectories, or add to an already existing group?', ...
	'Group Menu', ...
    dlgButtons{:});
% Handle response
switch answer1
    case 'Create new'
        
        if isfield(pTPC.groupList, 'Name')
            groupName = [pTPC.groupList.Name];
            groupNumber = length(pTPC.groupList)+1;
        else
            groupName = [];
            groupNumber = 1; % counter 
        end
        
        try
            answer = Settings_GUI({...
                struct('name', 'Group name', 'type', 'str', 'default', ['Group_', num2str(groupNumber)], 'strSpecs', struct('forbidden', {groupName}, 'capital', [], 'number', [], 'glyph', [], 'space', 0)),...
                },...
                'infoText', 'Enter the name of the new group!', 'title', 'Group creation');
        catch
            setEnable(handles,'on',handles.figure1.Children);
            set(handles.figure1,'Pointer','arrow');
            return
        end
        
        grouping('Create new', answer, handles, 'TrajIDs', visibleTraj);
        dispStatus('>> Group created.', handles);

    case 'Add to existing'
        
        groupName = [pTPC.groupList.Name];
        [indx,tf] = listdlg('PromptString','Select a group:',...
            'SelectionMode','single',...
            'ListString',groupName);
        if isempty(indx)
            setEnable(handles,'on',handles.figure1.Children);
            set(handles.figure1,'Pointer','arrow');
            return
        end
        
        groupID = pTPC.groupList(indx).Name;
        
        grouping('Add to existing', groupID, handles, 'TrajIDs', visibleTraj);
        dispStatus('>> Visibles added to group.', handles);
        
    otherwise
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        return
end
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');


% --- Executes on button press in allVisible.
function allVisible_Callback(hObject, eventdata, handles)
% hObject    handle to allVisible (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
pause(0.1);
pTPC.A(:, 9:12) = {true};
set(pTPC.lines(:), 'Visible', 1);
handles.numOfTraj.String = ["Number of trajectories: "; [num2str(pTPC.n), ' / ', num2str(pTPC.n)]];
handles.slider_1.Value = handles.slider_1.Min;
handles.slider_2.Value = handles.slider_2.Min;
dispStatus('>> All visible now.', handles);
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');



% --- Executes on button press in togglebutton_group1.
function togglebutton_group1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group1
toogleGroupHighligth(hObject, eventdata, handles)


% --- Executes on button press in togglebutton_group2.
function togglebutton_group2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group2
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group3.
function togglebutton_group3_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group3
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group4.
function togglebutton_group4_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group4
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group5.
function togglebutton_group5_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group5
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group6.
function togglebutton_group6_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group6
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group7.
function togglebutton_group7_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group7
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group8.
function togglebutton_group8_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group8
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group9.
function togglebutton_group9_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group9
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group10.
function togglebutton_group10_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group10
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group11.
function togglebutton_group11_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group11
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group12.
function togglebutton_group12_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group12
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group13.
function togglebutton_group13_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group13
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group14.
function togglebutton_group14_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group14
toogleGroupHighligth(hObject, eventdata, handles)

% --- Executes on button press in togglebutton_group15.
function togglebutton_group15_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton_group15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton_group15
toogleGroupHighligth(hObject, eventdata, handles)




% --- Executes on button press in clear.
function clear_Callback(hObject, eventdata, handles)
% hObject    handle to clear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
setEnable(handles,'off',handles.figure1.Children);
set(handles.figure1,'Pointer','watch');
pause(0.1);
% pTPC.A(:, 9:12) = {false};
set(pTPC.lines(:), 'Visible', 0);
handles.numOfTraj.String = ["Number of trajectories: "; '0 / ', num2str(pTPC.n)];
handles.slider_1.Value = handles.slider_1.Min;
handles.slider_2.Value = handles.slider_2.Min;
dispStatus('>> Axes empty.', handles);
setEnable(handles,'on',handles.figure1.Children);
set(handles.figure1,'Pointer','arrow');


% --- Executes on button press in groupToggleReset.
function groupToggleReset_Callback(hObject, eventdata, handles)
% hObject    handle to groupToggleReset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1,'Pointer','watch');
for i=1:15
    if ~isempty(handles.(sprintf('togglebutton_group%d',i)).UserData)
        gHL_eventdata = struct('Source', handles.(sprintf('togglebutton_group%d',i)), 'EventName', 'Action');
        gHL_eventdata.Source.Value = 0;
        toogleGroupHighligth(handles.(sprintf('togglebutton_group%d',i)), gHL_eventdata, handles)
    end
end
set(handles.uipanel_toogleGroup.Children, 'BackgroundColor', [0.94 0.94 0.94]);
set(handles.uipanel_toogleGroup.Children, 'ForegroundColor', [0 0 0]);
set(handles.uipanel_toogleGroup.Children, 'FontWeight', 'normal');
set(handles.uipanel_toogleGroup.Children, 'Value', 0);
set(handles.uipanel_toogleGroup.Children, 'UserData', []);
set(handles.figure1,'Pointer','arrow');


% --- Executes on button press in coorNeg_checkbox1.
function coorNeg_checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to coorNeg_checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of coorNeg_checkbox1


% --- Executes on button press in coorNeg_checkbox2.
function coorNeg_checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to coorNeg_checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of coorNeg_checkbox2


% --- Executes on button press in slider_1_checkbox.
function slider_1_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to slider_1_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of slider_1_checkbox


% --- Executes on button press in slider_2_checkbox.
function slider_2_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to slider_2_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of slider_2_checkbox


% --- Executes on button press in pushbutton_animate.
function pushbutton_animate_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_animate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
answer = questdlg({['Animating can take a long, time depends on the number of trajectories,',...
    ' the number of frames and the resolution value.'],...
    ' Do you want to create animation?'}, ...
    'Animation', ...
    'Yes','No','No');
% Handle response
switch answer
    case 'Yes'
        pTPC = get(handles.figure1,'UserData');
        setEnable(handles,'off',handles.figure1.Children);
        set(handles.figure1,'Pointer','watch');
        pause(0.1);
        
        % Parameter dialog
        try
            answer = Settings_GUI({...
                struct('name', 'Resolution', 'type', 'int', 'default', 20, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [2 256])),...
                struct('name', 'Tail', 'type', 'int', 'default', 5, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 pTPC.maxF-1])),...
                },...
                'infoText', 'Please enter animation parameters', 'title', 'Animation settings');
        catch
            return
        end
        
        
        if ~isempty(answer)
            if answer{1} > 50
                answer2 = questdlg('Are you sure about to use this high resolution?',...
                    'Resolution high af',...
                    'Sure', 'Not sure', 'Not sure');
                if strcmp(answer2, 'Not sure')
                    setEnable(handles,'on',handles.figure1.Children);
                    set(handles.figure1,'Pointer','arrow');
                    dispStatus('>> Done.', handles);
                    return
                end
            end
            resolution = answer{1};
            tail = answer{2};
        else
            setEnable(handles,'on',handles.figure1.Children);
            set(handles.figure1,'Pointer','arrow');
            dispStatus('>> Done.', handles);
            warndlg('Please enter positive integer values.');
            return
        end
        
        % Path selection dialog
        [fileName, path] = uiputfile('multyCellAnimation.avi');
        
        if path == 0
            setEnable(handles,'on',handles.figure1.Children);
            set(handles.figure1,'Pointer','arrow');
            dispStatus('>> Animation canceled.', handles);
            return
        end
        
        dispStatus('>> Generating animation.', handles);
        dispStatus('>> busy...', handles);
        
        visibleTraj = find(string(get(pTPC.lines, 'Visible')) == "on");
%         [visibleTraj] = checkVisibility(handles);
        
        % Creating interpolated points between coordinates
        Cmet = multyAnimCoorIntPol(pTPC.A(visibleTraj,1), resolution, tail);
        
        % Gether highlight information
        colorInf = cell(0, 2);
        j = 1;
        for i=1:length(visibleTraj)
            if ~isempty(pTPC.lines(visibleTraj(i)).UserData{4})
                colorInf{j, 1} = i;
                colorInf{j, 2} = pTPC.lines(visibleTraj(i)).Color;
                j = j+1;
            end
        end
        if ~isempty(colorInf)
            
            % Sorting the highlight info in order
            [~, idx] = sort(cell2mat(colorInf(:,1)));
            colorOrdered = colorInf(idx,:);
            
            % Sorting the input cell, goal is to move highlighted trajectoriest
            % to the end.
            M = length(Cmet);
            for k=1:(j-1)
                tempCmetElement = Cmet(colorOrdered{k,1}+(1-k), :);
                Cmet = [Cmet(1:colorOrdered{k,1}-k, :); Cmet(colorOrdered{k,1}+(2-k):end, :); tempCmetElement];
                colorOrdered{k, 1} = M-j+k+1;
            end
        else
            colorOrdered = colorInf;
        end
        
        % Generating the animation by frame, and rendering.
        multyPathAnimation(Cmet, resolution, tail, fileName, path, colorOrdered);
        
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        dispStatus('>> Done.', handles);
    otherwise
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        dispStatus('>> Done.', handles);
        return
end



% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

pTPC = get(handles.figure1,'UserData');
switch eventdata.Key
    case 'f'
        if ~isempty(hObject.Children(end).Children)
            prompt = {'Enter trajectory ID:'};
            dlgtitle = 'Look up';
            answer = inputdlg(prompt,dlgtitle);
            if isempty(answer), return, end
            if isnumeric(str2double(answer{1})) && str2double(answer{1}) > 0 && str2double(answer{1}) < pTPC.n
                trajID = str2double(answer{1});
                src = hObject.Children(end).Children(pTPC.n-trajID+1);
                lineCallback(src)
            else
                errordlg(['Please enter a valid positive number, less than ', num2str(pTPC.n +1),'.']);
            end
        end
    case 'q'
        setEnable(handles,'on',handles.figure1.Children);
        set(handles.figure1,'Pointer','arrow');
        a = randi(10);
        if a >= 9
            dispStatus('>> Stop it!', handles);
        elseif a <= 2
            dispStatus('>> Contact us!', handles);
        else
            dispStatus('>> Cheater!!!.', handles);
        end
    case 't' % test
        'test';
    otherwise
        return
end


% --- Executes on button press in slider_1_Name.
function slider_1_Name_Callback(hObject, eventdata, handles)
% hObject    handle to slider_1_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
prev_sliderFun = pTPC.slider_1_Fun;
sliderID = 1;
sliderSelection(handles, sliderID);


if strcmp(prev_sliderFun{1},pTPC.slider_1_Fun{1})
    if strcmp(prev_sliderFun{2},pTPC.slider_1_Fun{2})
        if strcmp(prev_sliderFun{3},pTPC.slider_1_Fun{3})
            if isempty(setdiff(prev_sliderFun{4},pTPC.slider_1_Fun{4}))
                if isempty(setdiff(prev_sliderFun{5},pTPC.slider_1_Fun{5}))
                    return
                end
            end
        end
    end
end

calcSliderValue(pTPC, sliderID);
handles.slider_1_Name.String = pTPC.slider_1_Fun{2};

tempMax = max([pTPC.A{:, 4}]);
tempMin = min([pTPC.A{:, 4}]);

if tempMax == tempMin
    handles.slider_1.Enable = 'off';
    handles.min_slider_1_Text.String = '';
    handles.max_slider_1_Text.String = '';
    handles.mid_slider_1_Text.String = '';
    handles.act_slider_1_EditText.FontSize = 8;
    handles.act_slider_1_EditText.FontWeight = 'normal';
    handles.act_slider_1_EditText.ForegroundColor = [0.5 0.5 0.5];
    handles.act_slider_1_EditText.String = 'Inactive';
    handles.act_slider_1_EditText.Enable = 'off';
else
    handles.act_slider_1_EditText.ForegroundColor = [0 0 1];
    handles.act_slider_1_EditText.Enable = 'on';
    handles.act_slider_1_EditText.FontWeight = 'bold';
    handles.slider_1.Min = tempMin - 0.01; % 0.01 is the step size if the sliders
    handles.slider_1.Max = tempMax + 0.01;   % in case of low values it is necessary.
    handles.slider_1.Value = tempMin;
    handles.act_slider_1_EditText.String = num2str(tempMin, 4);
    handles.max_slider_1_Text.String = num2str(tempMax,4);
    handles.mid_slider_1_Text.String = num2str((tempMax+tempMin)/2, 4);
    handles.min_slider_1_Text.String = num2str(tempMin, 4);
    handles.slider_1.SliderStep = [0.01, 0.1];
end

if strcmp(handles.slider_1.Enable, 'off')
    msgbox('Slider#1 is inactivated, because the min and max values are equal for every trajectories.')
end
dispStatus('>> Slider values calculated!', handles);



% --- Executes on button press in slider_2_Name.
function slider_2_Name_Callback(hObject, eventdata, handles)
% hObject    handle to slider_2_Name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
pTPC = get(handles.figure1,'UserData');
prev_sliderFun = pTPC.slider_2_Fun;
sliderID = 2;
sliderSelection(handles, sliderID);


if strcmp(prev_sliderFun{1},pTPC.slider_2_Fun{1})
    if strcmp(prev_sliderFun{2},pTPC.slider_2_Fun{2})
        if strcmp(prev_sliderFun{3},pTPC.slider_2_Fun{3})
            if isempty(setdiff(prev_sliderFun{4},pTPC.slider_2_Fun{4}))
                if isempty(setdiff(prev_sliderFun{5},pTPC.slider_2_Fun{5}))
                    return
                end
            end
        end
    end
end

calcSliderValue(pTPC, sliderID);
handles.slider_2_Name.String = pTPC.slider_2_Fun{2};

tempMax = max([pTPC.A{:, 3}]);
tempMin = min([pTPC.A{:, 3}]); 

if tempMax == tempMin
    handles.slider_2.Enable = 'off';
    handles.min_slider_2_Text.String = '';
    handles.max_slider_2_Text.String = '';
    handles.mid_slider_2_Text.String = '';
    handles.act_slider_2_EditText.FontSize = 8;
    handles.act_slider_2_EditText.FontWeight = 'normal';
    handles.act_slider_2_EditText.ForegroundColor = [0.5 0.5 0.5];
    handles.act_slider_2_EditText.String = 'Inactive';
    handles.act_slider_2_EditText.Enable = 'off';
else
    handles.slider_2.Enable = 'on';
    handles.act_slider_2_EditText.ForegroundColor = [0 0 1];
    handles.act_slider_2_EditText.Enable = 'on';
    handles.act_slider_2_EditText.FontWeight = 'bold';
    handles.slider_2.Min = tempMin - 0.01; % 0.01 is the step size if the sliders
    handles.slider_2.Max = tempMax + 0.01; % in case of low values it is necessary.
    handles.slider_2.Value = tempMin;
    handles.act_slider_2_EditText.String = num2str(tempMin, 4);
    handles.max_slider_2_Text.String = num2str(tempMax, 4);
    handles.mid_slider_2_Text.String = num2str((tempMax+tempMin)/2, 4);
    handles.min_slider_2_Text.String = num2str(tempMin, 4);
    handles.slider_2.SliderStep = [0.01, 0.1];
end

if strcmp(handles.slider_2.Enable, 'off')
    msgbox('Slider#2 is inactivated, because the min and max values are equal for every trajectories.')
end
dispStatus('>> Slider values calculated!', handles);
