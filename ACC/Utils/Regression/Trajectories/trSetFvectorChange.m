global CommonHandles;
trSet = CommonHandles.TrainingSet;

n = length(trSet.CellNumber);
newAnalFolder = ['anal2', '_onlyBrightfield_2f15']; %'v3_central3\' 'v2_deriv_1_2_3' 'v2_derivative_3'
baseFolder = CommonHandles.DirName;

for i = 1:n
    i
    splitMataDataFilename = strsplit(trSet.MetaDataFilename{i},'\');
    newFileName = fullfile(baseFolder, splitMataDataFilename{1},newAnalFolder,splitMataDataFilename{3});
    fVector = csvread(newFileName);
    trSet.MetaDataFilename{i} = fullfile(splitMataDataFilename{1}, newAnalFolder, splitMataDataFilename{3});
    
    splitImageName = strsplit(trSet.ImageName{i},{'\', '.'});
%     trSet.ImageName{i} = fullfile(splitImageName{1}, newAnalFolder, splitImageName{3}, '.tiff');
    trSet.ImageName{i} = fullfile(splitImageName{1}, newAnalFolder, splitImageName{3}, splitImageName{4});
    
    splitOriginalImageName = strsplit(trSet.OriginalImageName{i},{'\','.'});
%     trSet.OriginalImageName{i} = fullfile(splitOriginalImageName{1}, newAnalFolder, splitOriginalImageName{3}, '.tiff');
    trSet.OriginalImageName{i} = fullfile(splitOriginalImageName{1}, newAnalFolder, splitOriginalImageName{3}, splitOriginalImageName{4});

    
    if sum(fVector(:, 3) == CommonHandles.TrainingSet.Features{i}(1)) < 2
        trSet.Features{i, 1} = fVector(fVector(:, 3) == CommonHandles.TrainingSet.Features{i}(1),3:end);
    else
        trSet.Features{i, 1} = fVector(round(fVector(:, 4), 4) == round(CommonHandles.TrainingSet.Features{i}(2), 4),3:end);
    end
end

CommonHandles.TrainingSet = trSet;