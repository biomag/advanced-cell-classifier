function toogleGroupHighligth(hObject, eventdata, handles)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     toogleGroupHighligth
%
% Assign a group to a button, and turns on/off highlight colour and line
% width.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

pTPC = get(handles.figure1,'UserData');
if ~isempty(pTPC.groupList)
    
    buttonID = str2num(hObject.String);
    if eventdata.Source.Value == 1
        
        if isempty(hObject.UserData)
            gn = [pTPC.groupList(:).Name];
            [indx,tf] = listdlg('PromptString','Select a group:',...
                'SelectionMode','single',...
                'ListString',gn);
            if isempty(indx)
                hObject.Value = 0;
                return
            end
            
            selectedGroup = pTPC.groupList(indx).Name;
            hObject.UserData = selectedGroup;
            try
                highLightColor = uisetcolor;
            catch e
                if strcmp(e.identifier, 'MATLAB:Java:GenericException')
                    s = settings;
                    oldcolorpicker = 'matlab.ui.internal.dialog.ColorChooser';
                    % For reference:
                    % newcolorpicker = 'matlab.ui.internal.dialog.WebColorChooser';
                    s.matlab.ui.dialog.uisetcolor.ControllerName.TemporaryValue = oldcolorpicker;
                    hObject.UserData = selectedGroup;
                else
                    rethrow(e)
                end
            end
            if length(highLightColor) ~= 3
                hObject.Value = 0;
                hObject.UserData = [];
                return
            end
            if ismac
                hObject.ForegroundColor = highLightColor;
                hObject.BackgroundColor = highLightColor;
            else
                hObject.BackgroundColor = highLightColor;
                lumScalF = [0.299 0.587 0.114];
                if sum(highLightColor .* lumScalF) > 0.5
                    hObject.ForegroundColor = [0 0 0];
                else
                    hObject.ForegroundColor = [1 1 1];
                end
            end
            hObject.FontWeight = 'bold';
            dispStatus('>> Group attached.', handles);
        end
        
        buttonState = 1;
        adjustHighlightColor(buttonID, buttonState, hObject.UserData, hObject.BackgroundColor, handles);
        
    elseif eventdata.Source.Value == 0
        
        buttonState = 0;
        adjustHighlightColor(buttonID, buttonState, hObject.UserData, hObject.BackgroundColor, handles);
    end
else
    msgbox('There are no groups to attach!');
    hObject.Value = 0;
    return
end
actVisibility = sum(string(get(pTPC.lines, 'Visible')) == "on");
handles.numOfTraj.String = ["Number of trajectories: "; [num2str(actVisibility), ' / ', num2str(pTPC.n)]];