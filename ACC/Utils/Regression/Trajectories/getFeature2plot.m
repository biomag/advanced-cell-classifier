function outputCell = getFeature2plot(inputCell)

M = length(inputCell);

global CommonHandles;

featureNameFile = [CommonHandles.DirName, '\', CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, '\', CommonHandles.MetaDataFolder, '\FeaturesName.acc'];
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
        [indx,tf] = listdlg('PromptString','Select features:',...
            'ListString',featureNameList, 'ListSize',[350,250]);
        if isempty(indx)
            return
        end
outputCell = inputCell(:,1);

for j = 1:M
    m = length(inputCell{j,1});
    featureArray = zeros(m, numel(indx));
    j2Cstructs = inputCell{j, 14};
    for i=1:m
        if ~isempty(j2Cstructs{i})
            jumpToCell(j2Cstructs{i},false);
            featureArray(i, :) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, indx);
        else
            featureArray(i, :) = nan(1, length(indx));
        end
    end
    outputCell{j, 2} = featureArray;
end
