function varargout = LineProp(varargin)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     LineProp
%
% Line Properities opens as a new GUI window by click-callback function of
% ploted trajectory. LineProp allows to customize the
% apperarance of a single trajectory, sets visibility and other properties.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.
%
%
% LINEPROP MATLAB code for LineProp.fig
%       LINEPROP - Line Property window is an interactive tool with some
%       visual appearence modification ability and group membership adjust
%       possibilites. Cell image thumbnails can be inported from ACC, and
%       Jump_to_cell function included.
%
%      LINEPROP, by itself, creates a new LINEPROP or raises the existin
%      singleton*.
%
%      H = LINEPROP returns the handle to a new LINEPROP or the handle to
%      the existing singleton*.
%
%      LINEPROP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LINEPROP.M with the given input arguments.
%
%      LINEPROP('Property','Value',...) creates a new LINEPROP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LineProp_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LineProp_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LineProp

% Last Modified by GUIDE v2.5 09-Jan-2020 17:10:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LineProp_OpeningFcn, ...
                   'gui_OutputFcn',  @LineProp_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LineProp is made visible.
function LineProp_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LineProp (see VARARGIN)

% Choose default command line output for LineProp
handles.output = hObject;
saveIcon = imread('saveIcon.jpg');
set(handles.anim_save, 'CData', saveIcon);

src = varargin{1};
pTPC = src.Parent.Parent.UserData;
handles.distText.String = num2str(norm([src.XData(end);  src.YData(end)] - [src.XData(1);  src.YData(1)]));
sumLength = 0;

handles.lineProp.Name = ['Line Proprety ', src.Tag];

for i=1:length(src.XData)-1
    sumLength = sumLength + norm([src.XData(i+1);  src.YData(i+1)] - [src.XData(i);  src.YData(i)]);
end

if ~isempty(src.UserData{1})
    handles.popup_group.String = [pTPC.groupList(src.UserData{1}).Name];
end

handles.lengthText.String = num2str(sumLength);
handles.lineWidthEditText.String = num2str(src.UserData{3});
handles.colorButton.BackgroundColor = src.UserData{2};
lumScalF = [0.299 0.587 0.114];

handles.statText2.Position(1) = handles.statText1.Position(1);
handles.statText2.Position(3) = handles.statText1.Position(3);

if sum(src.Color .* lumScalF) > 0.5
    handles.colorButton.ForegroundColor = [0 0 0];
else
    handles.colorButton.ForegroundColor = [1 1 1];
end

trajAxesHandle = src.Parent;
trajData = get(trajAxesHandle.Parent,'UserData');
currentCellData = trajData.A(str2double(src.Tag),:);
trajLength = length(currentCellData{14});


highlightColor = [0.12345 0.12345 .995];
highlightWidth = src.LineWidth+5;
src.Color =  highlightColor;
src.LineWidth = highlightWidth;

handles.IDtext.String = src.Tag;
handles.IDtext.Units = 'pixels';
handles.listbox_cellList.String = cellfun(@num2str,num2cell(1:trajLength),repmat({'%02d'},1,trajLength),'UniformOutput',false);
uData = get(handles.lineProp,'UserData');

uData.src = src;
uData.highlightColor = highlightColor;
uData.highlightWidth = highlightWidth;
uData.trajectoryData = currentCellData;
uData.trajAxesHandle = trajAxesHandle;
set(handles.lineProp,'UserData',uData);

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = LineProp_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in visibilityCB.
function visibilityCB_Callback(hObject, eventdata, handles)
% hObject    handle to visibilityCB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of visibilityCB


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lineWidthEditText_Callback(hObject, eventdata, handles)
% hObject    handle to lineWidthEditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lineWidthEditText as text
%        str2double(get(hObject,'String')) returns contents of lineWidthEditText as a double
if isempty(str2num(hObject.String))
    warndlg('Please enter numeric value', 'Not numeric');
elseif str2num(hObject.String) <= 0
    warndlg('Please enter positive number', 'Negative value');
end


% --- Executes during object creation, after setting all properties.
function lineWidthEditText_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lineWidthEditText (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in apply.
function apply_Callback(hObject, eventdata, handles)
% hObject    handle to apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uData = get(handles.lineProp,'UserData');
pTPC = get(uData.trajAxesHandle.Parent,'UserData');

src = uData.src;
src.Visible = handles.visibilityCB.Value;
pTPC.A(str2num(src.Tag), 9:12) = {logical(handles.visibilityCB.Value)};

src.Color = handles.colorButton.BackgroundColor;
src.UserData{2} = handles.colorButton.BackgroundColor;


if ~isempty(str2double(handles.lineWidthEditText.String))
    if ~isnan(str2double(handles.lineWidthEditText.String))
        if str2double(handles.lineWidthEditText.String) > 0
            src.LineWidth = str2double(handles.lineWidthEditText.String);
            src.UserData{3} = str2double(handles.lineWidthEditText.String);
        else
            warndlg('Invalid line width value!');
        end
    else
        warndlg('Invalid line width value!');
    end
else
    warndlg('Invalid line width value!');
end



% --- Executes when user attempts to close lineProp.
function lineProp_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to lineProp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

uData = get(handles.lineProp,'UserData');
%If there is no change then restore upon closing
try
if all( (uData.highlightColor - uData.src.Color) < 10e-5)
    uData.src.Color = uData.src.UserData{2};
end
if (uData.highlightWidth - uData.src.LineWidth)<10e-5
    uData.src.LineWidth = uData.src.UserData{3};
end
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);
currentTrajIdx = str2double(uData.src.Tag);
pTPC = get(uData.trajAxesHandle.Parent,'UserData');

for i=1:trajLength
    if iscell(pTPC.imgHandles{currentTrajIdx}) &&...
          length(pTPC.imgHandles{currentTrajIdx})>=i &&...
          ishandle(pTPC.imgHandles{currentTrajIdx}{i})
            delete(pTPC.imgHandles{currentTrajIdx}{i}); 
    end
end

src.Parent.Parent.Parent.ShowHiddenHandles = 'on';
m_handles = guidata(uData.src.Parent.Parent);

actVisibility = sum(string(get(pTPC.lines, 'Visible')) == "on");
m_handles.numOfTraj.String = ["Number of trajectories: "; [num2str(actVisibility), ' / ', num2str(pTPC.n)]];
uData.src.UserData(5) = {0};

catch %to close in all scenario    
end


delete(hObject);


% --- Executes on button press in checkbox_showImages.
function checkbox_showImages_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_showImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_showImages

boolOn = get(hObject,'Value');
uData = get(handles.lineProp,'UserData');
j2Cstructs = uData.trajectoryData{14};
iconSize = 0.05;
trajLength = numel(j2Cstructs);
currentTrajIdx = str2double(uData.src.Tag);
boolArrayForReal = uData.trajectoryData{13};

global CommonHandles;

tempCurrentCellInfo = makeTempCellData();

pTPC = get(uData.trajAxesHandle.Parent,'UserData');

if boolOn
    h = msgbox('Please wait while the thumbnails are fetched...');
    for i=1:trajLength
       if ~isempty(j2Cstructs{i})
           jumpToCell(j2Cstructs{i},false);
           currCellImg = CommonHandles.CurrentSelectedCell;           
       else
           currCellImg = ones(25,25,3)*0.5;
       end       
       %delete if there was sg there before
       if iscell(pTPC.imgHandles{currentTrajIdx}) &&...
          length(pTPC.imgHandles{currentTrajIdx})>=i &&...
          ishandle(pTPC.imgHandles{currentTrajIdx}{i})
            delete(pTPC.imgHandles{currentTrajIdx}{i}); 
       end
       pTPC.imgHandles{currentTrajIdx}{i} = image(uData.trajAxesHandle,...
             'XData',[uData.trajectoryData{1}(i,1) - iconSize/2,uData.trajectoryData{1}(i,1) + iconSize/2],...
             'YData',[uData.trajectoryData{1}(i,2) - iconSize/2,uData.trajectoryData{1}(i,2) + iconSize/2],...
             'CData',currCellImg);     
       if ~boolArrayForReal(i)                            
           pTPC.imgHandles{currentTrajIdx}{i}.AlphaData = 0.5;
       end
    end
else               
    for i=1:numel(pTPC.imgHandles{currentTrajIdx})
        if ishandle(pTPC.imgHandles{currentTrajIdx}{i}), delete(pTPC.imgHandles{currentTrajIdx}{i}); end
    end    
end

jumpToCell(tempCurrentCellInfo,0);
if exist('h','var') && ishandle(h)
    close(h);
end

set(handles.lineProp,'UserData',uData);


% --- Executes on selection change in listbox_cellList.
function listbox_cellList_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_cellList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_cellList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_cellList

uData = get(handles.lineProp,'UserData');
pTPC = get(uData.trajAxesHandle.Parent,'UserData');

selectedIdx = get(hObject,'Value');
currentTrajIdx = str2double(uData.src.Tag);
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);

for i=1:trajLength
    if iscell(pTPC.imgHandles{currentTrajIdx}) &&...
          length(pTPC.imgHandles{currentTrajIdx})>=i &&...
          ishandle(pTPC.imgHandles{currentTrajIdx}{i})
        if i==selectedIdx
            pTPC.imgHandles{currentTrajIdx}{i}.Selected = 'on';
        else
            pTPC.imgHandles{currentTrajIdx}{i}.Selected = 'off';
        end
    end
end


% --- Executes during object creation, after setting all properties.
function listbox_cellList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_cellList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_Jump2Cell.
function pushbutton_Jump2Cell_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_Jump2Cell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.lineProp,'UserData');

selectedIdx = get(handles.listbox_cellList,'Value');
j2Cstructs = uData.trajectoryData{14};

if ~isempty(j2Cstructs{selectedIdx})
    jumpToCell(j2Cstructs{selectedIdx},true);
else
    warndlg('Cell was not found for this time point')
end




% --- Executes on button press in colorButton.
function colorButton_Callback(hObject, eventdata, handles)
% hObject    handle to colorButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
c = uisetcolor;
handles.colorButton.BackgroundColor = c;


% --- Executes on selection change in popup_group.
function popup_group_Callback(hObject, eventdata, handles)
% hObject    handle to popup_group (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_group contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_group



% --- Executes during object creation, after setting all properties.
function popup_group_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_group (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showThis.
function showThis_Callback(hObject, eventdata, handles)
% hObject    handle to showThis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
src = handles.lineProp.UserData.src;
pTPC = src.Parent.Parent.UserData;

pTPC.A(:, 9:12) = {false};
pTPC.A(str2num(src.Tag), 9:12) = {true};


set(pTPC.lines(:), 'Visible', 0);
set(pTPC.lines(str2num(src.Tag)), 'Visible', 1);
handles.visibilityCB.Value = 1;

% --- Executes on button press in removeFromGroup.
function removeFromGroup_Callback(hObject, eventdata, handles)
% hObject    handle to removeFromGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
src = handles.lineProp.UserData.src;
pTPC = src.Parent.Parent.UserData;
popupValue = handles.popup_group.Value;
selectedGroup = handles.popup_group.String(popupValue);
if strcmp(selectedGroup, 'N') % No groups
    return
end
trajID = str2num(src.Tag);
src.Parent.Parent.Parent.ShowHiddenHandles = 'on';
m_handles = guidata(src.Parent.Parent);

grouping('Remove from group', selectedGroup, m_handles, 'TrajIDs', trajID);


if ~isempty(pTPC.lines(trajID).UserData{1})
    handles.popup_group.String = [pTPC.groupList(pTPC.lines(trajID).UserData{1}).Name];
    handles.popup_group.Value = 1;
else
    handles.popup_group.String = 'No groups';
    handles.popup_group.Value = 1;
end


groupIndex = find(strcmp([pTPC.groupList.Name], selectedGroup)==1);
src.UserData{4} = setdiff(src.UserData{4},groupIndex);
src.Parent.Parent.Parent.ShowHiddenHandles = 'off';


% --- Executes on button press in addToGroup.
function addToGroup_Callback(hObject, eventdata, handles)
% hObject    handle to addToGroup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

src = handles.lineProp.UserData.src;
pTPC = src.Parent.Parent.UserData;
if isempty(pTPC.groupList)
    msgbox('There is no defined groups yet!');
    return
end
trajID = str2num(src.Tag);
src.Parent.Parent.Parent.ShowHiddenHandles = 'on';
m_handles = guidata(src.Parent.Parent);

gn = [pTPC.groupList.Name];
[indx,tf] = listdlg('PromptString','Select a group:',...
    'SelectionMode','single',...
    'ListString',gn);
groupID = pTPC.groupList(indx).Name;

grouping('Add to existing', groupID, m_handles, 'TrajIDs', trajID);

handles.popup_group.String = [pTPC.groupList(pTPC.lines(trajID).UserData{1}).Name];

src.Parent.Parent.Parent.ShowHiddenHandles = 'off';


% --- Executes on button press in pushbutton_animation.
function pushbutton_animation_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_animation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.lineProp,'UserData');
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);
currentTrajIdx = str2double(uData.src.Tag);
trajID = uData.src.Tag;

global CommonHandles;


pTPC = get(uData.trajAxesHandle.Parent,'UserData');

try
    answer = Settings_GUI({...
            struct('name', 'Resolution', 'type', 'int', 'default', 20, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 256])),...
            struct('name', 'Tail', 'type', 'int', 'default', 5, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 trajLength-1])),...
            },...
        'infoText', 'Please enter animation parameters', 'title', 'Animation settings');
catch
    return
end

resolution = answer{1};
tail = answer{2};

CellImg = cell(trajLength, 1);

wb = waitbar(0, 'Collecting images. Please wait...');

for i=1:trajLength
    if ~isempty(j2Cstructs{i})
        jumpToCell(j2Cstructs{i},false);
        CellImg{i} = CommonHandles.CurrentSelectedCell;
    else
        CellImg{i} = ones(25,25,3)*0.5;
    end
    
    if ishandle(wb)
        waitbar(i/trajLength, wb, 'Collecting images. Please wait...');
    end
end

if ishandle(wb)
    close(wb)
end

Cmet = multyAnimCoorIntPol(pTPC.A(currentTrajIdx, 1),resolution, tail);

pathAnimation(CellImg, resolution, tail, trajID, Cmet{2}(:,1), Cmet{2}(:,2));


% --- Executes on button press in pushbutton10_showImages.
function pushbutton10_showImages_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10_showImages (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.lineProp,'UserData');
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);
trajID = uData.src.Tag;

global CommonHandles;

CellImg = cell(trajLength, 1);

for i=1:trajLength
    if ~isempty(j2Cstructs{i})
        jumpToCell(j2Cstructs{i},false);
        CellImg{i} = CommonHandles.CurrentSelectedCell;
    else
        CellImg{i} = ones(25,25,3)*0.5;
    end
end
f = figure;
montage(CellImg);
title(f.Children, ['Cell ID: ', trajID, '  ||  Group Memberships: [', num2str(uData.src.UserData{1}),']']);




% --- Executes on button press in pushbutton11_features.
function pushbutton11_features_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11_features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.lineProp,'UserData');
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);

global CommonHandles;

featureNameFile = fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, CommonHandles.MetaDataFolder, 'featureNames.acc');
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
        [indx,tf] = listdlg('PromptString','Select features:',...
            'ListString',featureNameList, 'ListSize',[350,250]);
        if isempty(indx)
            return
        end

featureArray = zeros(trajLength, length(indx));

for i=1:trajLength
    if ~isempty(j2Cstructs{i})
        jumpToCell(j2Cstructs{i},false);
        featureArray(i, :) = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, indx);
    else
        featureArray(i, :) = nan(1, length(indx));
    end
end

headers = featureNameList(indx);
for j = 1:length(headers)
    if ~isvarname(headers(j))
        headers(j) = matlab.lang.makeValidName(headers(j));
    end
end

featureTable = array2table(featureArray, 'VariableNames', headers);

featureF = uifigure;
featureF.Name = 'Trajectory Features';

uiTable = uitable(featureF);
uiTable.Data = featureTable;
uiTable.RowName = 'numbered';
uiTable.Position = [30, 60, 500, 300];

fExpB = uibutton(featureF);
fExpB.Position = [30, 20, 100, 25];
fExpB.Text = 'Export';
fExpB.ButtonPushedFcn = {@exportID_Callback, featureTable};



% --- Executes on button press in anim_save.
function anim_save_Callback(hObject, eventdata, handles)
% hObject    handle to anim_save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.lineProp,'UserData');
j2Cstructs = uData.trajectoryData{14};
trajLength = numel(j2Cstructs);
currentTrajIdx = str2double(uData.src.Tag);
trajID = uData.src.Tag;

global CommonHandles;

pTPC = get(uData.trajAxesHandle.Parent,'UserData');

try
    answer = Settings_GUI({...
            struct('name', 'Resolution', 'type', 'int', 'default', 20, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 256])),...
            struct('name', 'Tail', 'type', 'int', 'default', 5, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 trajLength-1])),...
            struct('name', 'Path to save', 'type', 'file', 'default', 'trajAnim.avi', 'fileOpenType', 'put', 'filter', '*.avi')...
        },...
        'infoText', 'Please enter animation parameters', 'title', 'Animation settings');
catch
    return
end

resolution = answer{1};
tail = answer{2};

CellImg = cell(trajLength, 1);

wb = waitbar(0,'Collectiong images. Please wait...');

for i=1:trajLength
    if ~isempty(j2Cstructs{i})
        jumpToCell(j2Cstructs{i},false);
        CellImg{i} = CommonHandles.CurrentSelectedCell;
    else
        CellImg{i} = ones(25,25,3)*0.5;
    end
    
    if ishandle(wb)
        waitbar(i/trajLength, wb, 'Collectiong images. Please wait...');
    end
end

if ishandle(wb)
    close(wb);
end

Cmet = multyAnimCoorIntPol(pTPC.A(currentTrajIdx, 1),resolution,tail);

savePathAnimation(CellImg, resolution, tail, trajID, Cmet{1, 2}(:,1), Cmet{1, 2}(:,2), answer{3});
