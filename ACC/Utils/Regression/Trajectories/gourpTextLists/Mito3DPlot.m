fig = figure;
n = length(A);
for i=1:n
    B{i, 1} = A{i, 1}(:,1);
    B{i, 2} = A{i, 1}(:,2);
    B{i, 3} = outputCell{i, 1}(:,2);
end

hold on
        cellfun(@plot3, B(:,1), B(:,2), B(:,3),...
            repmat({'-o'},n,1),...
            repmat({'MarkerIndices'},n,1),...
            num2cell(cellfun(@length,B(:,1))),...
            repmat({'ButtonDownFcn'},n,1),...
            repmat({@lineCallback},n,1),...
            repmat({'Tag'},n,1),...
            cellfun(@num2str,num2cell(1:n),'UniformOutput',false)');
        hold off
        camproj('perspective')
        xlabel('Reg_X');
        ylabel('Reg_Y');
        zlabel('Nuc area');
        grid on
        ax = fig.Children;
        xtarget = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/2;
        ytarget = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/2;
        ztarget = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/2;
        ax.CameraTarget = [xtarget ytarget ztarget];
        set(fig,'KeyPressFcn',@camera_callbackfunction);


function lineCallback(src,~)
message = src.Tag;
msgbox(message)
end

function camera_callbackfunction(event, handles)

ax = event.Children;
xStep = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/100;
yStep = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/100;
zStep = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/100;

xtarget = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/2;
ytarget = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/2;
ztarget = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/2;

switch event.CurrentCharacter
    
    case '1'
        ax.CameraPosition = [xtarget ytarget (20*ztarget)];
    case '2'
        ax.CameraPosition(2) = ax.CameraPosition(2)+100;
    case '3'
        ax.CameraPosition = [xtarget (-20*ytarget) ztarget];
    case '4'
        ax.CameraPosition(3) = ax.CameraPosition(3)+100;
%         ax.CameraPosition(1) = ax.CameraPosition(1)+100;
    case '5'
        if strcmp(ax.Projection, 'orthographic')
            ax.Projection = 'perspective';
        else
            ax.Projection = 'orthographic';
        end
        
    case '6'
        ax.CameraPosition;
    case '7'
        ax.CameraPosition = [(20*xtarget) ytarget ztarget];
    case '8'
        ax.CameraPosition(2) = event.Children.CameraPosition(2)-100;
    case '9'
        ax.CameraPosition;
    case '+'
        ax.CameraViewAngle = ax.CameraViewAngle + 0.1;
    case '-'
        ax.CameraViewAngle = ax.CameraViewAngle - 0.1;
    otherwise
        return
    end
end