function varargout = FeaturePlotter(varargin)
% FEATUREPLOTTER MATLAB code for FeaturePlotter.fig
%      FEATUREPLOTTER, by itself, creates a new FEATUREPLOTTER or raises the existing
%      singleton*.
%
%      H = FEATUREPLOTTER returns the handle to a new FEATUREPLOTTER or the handle to
%      the existing singleton*.
%
%      FEATUREPLOTTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FEATUREPLOTTER.M with the given input arguments.
%
%      FEATUREPLOTTER('Property','Value',...) creates a new FEATUREPLOTTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FeaturePlotter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FeaturePlotter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FeaturePlotter

% Last Modified by GUIDE v2.5 11-Jul-2019 22:29:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FeaturePlotter_OpeningFcn, ...
                   'gui_OutputFcn',  @FeaturePlotter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FeaturePlotter is made visible.
function FeaturePlotter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FeaturePlotter (see VARARGIN)

% Choose default command line output for FeaturePlotter
handles.output = hObject;

featuresData = varargin{1};
set(handles.figure_featurePlotter,'UserData',featuresData);
handles.listbox_features.String = featuresData.Properties.VariableNames;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FeaturePlotter wait for user response (see UIRESUME)
% uiwait(handles.figure_featurePlotter);


% --- Outputs from this function are returned to the command line.
function varargout = FeaturePlotter_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox_features.
function listbox_features_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_features contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_features


% --- Executes during object creation, after setting all properties.
function listbox_features_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_plot.
function pushbutton_plot_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fIdx = handles.listbox_features.Value;
if length(fIdx) > 3
    errordlg('Too much input. Please select max 3!');
    return
else
    data = handles.figure_featurePlotter.UserData;
    trajCoordinates = featureExtrConverterToPlot(data, fIdx(1), fIdx(2), fIdx(3));
end

fig = figure
n = length(trajCoordinates);
switch length(fIdx)
    
    case 1
        
        for i=1:n
            B{i, 1} = trajCoordinates{i}(:,1);
        end
        
        hold on
        cellfun(@plot, B(:,1),...
            repmat({'-o'},n,1),...
            repmat({'MarkerIndices'},n,1),...
            repmat({1},n,1),...
            repmat({'ButtonDownFcn'},n,1),...
            repmat({@lineCallback},n,1),...
            repmat({'Tag'},n,1),...
            cellfun(@num2str,num2cell(1:n),'UniformOutput',false)');
        hold off
        
        xlabel(data.Properties.VariableNames(fIdx(1)));
        
        
    case 2
        
        for i=1:n
            B{i, 1} = trajCoordinates{i}(:,1);
            B{i, 2} = trajCoordinates{i}(:,2);
        end
        
        hold on
        cellfun(@plot, B(:,1), B(:,2),...
            repmat({'-o'},n,1),...
            repmat({'MarkerIndices'},n,1),...
            repmat({1},n,1),...
            repmat({'ButtonDownFcn'},n,1),...
            repmat({@lineCallback},n,1),...
            repmat({'Tag'},n,1),...
            cellfun(@num2str,num2cell(1:n),'UniformOutput',false)');
        hold off
        
        xlabel(data.Properties.VariableNames(fIdx(1)));
        ylabel(data.Properties.VariableNames(fIdx(1)));
        

        
    case 3
        
        for i=1:n
            B{i, 1} = trajCoordinates{i}(:,1);
            B{i, 2} = trajCoordinates{i}(:,2);
            B{i, 3} = trajCoordinates{i}(:,3);
        end
        
        hold on
        cellfun(@plot3, B(:,1), B(:,2), B(:,3),...
            repmat({'-o'},n,1),...
            repmat({'MarkerIndices'},n,1),...
            repmat({1},n,1),...
            repmat({'ButtonDownFcn'},n,1),...
            repmat({@lineCallback},n,1),...
            repmat({'Tag'},n,1),...
            cellfun(@num2str,num2cell(1:n),'UniformOutput',false)');
        hold off
        camproj('perspective')
        xlabel(data.Properties.VariableNames(fIdx(1)));
        ylabel(data.Properties.VariableNames(fIdx(2)));
        zlabel(data.Properties.VariableNames(fIdx(3)));
        grid on
        ax = fig.Children;
        xtarget = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/2;
        ytarget = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/2;
        ztarget = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/2;
        ax.CameraTarget = [xtarget ytarget ztarget];
        set(fig,'KeyPressFcn',@camera_callbackfunction);


    otherwise
        return
end


function lineCallback(src,~)
message = src.Tag;
msgbox(message)

function camera_callbackfunction(event, handles)

ax = event.Children;
xStep = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/100;
yStep = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/100;
zStep = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/100;

xtarget = (ax.XAxis.Limits(2) - ax.XAxis.Limits(1))/2;
ytarget = (ax.YAxis.Limits(2) - ax.YAxis.Limits(1))/2;
ztarget = (ax.ZAxis.Limits(2) - ax.ZAxis.Limits(1))/2;

switch event.CurrentCharacter
    
    case '1'
        ax.CameraPosition = [xtarget ytarget (20*ztarget)];
    case '2'
        ax.CameraPosition(2) = ax.CameraPosition(2)+100;
    case '3'
        ax.CameraPosition = [xtarget (-20*ytarget) ztarget];
    case '4'
        ax.CameraPosition(3) = ax.CameraPosition(3)+100;
%         ax.CameraPosition(1) = ax.CameraPosition(1)+100;
    case '5'
        if strcmp(ax.Projection, 'orthographic')
            ax.Projection = 'perspective';
        else
            ax.Projection = 'orthographic';
        end
        
    case '6'
        ax.CameraPosition;
    case '7'
        ax.CameraPosition = [(20*xtarget) ytarget ztarget];
    case '8'
        ax.CameraPosition(2) = event.Children.CameraPosition(2)-100;
    case '9'
        ax.CameraPosition;
    case '+'
        a
    case '-'
        a
    otherwise
        return
end

                                    
            
        


