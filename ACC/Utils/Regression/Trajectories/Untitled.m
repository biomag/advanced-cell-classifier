
%   COPY ALL REG RESULTS FILE TO THE SAME FOLDER
% folders = dir;
% mkdir('allReg');
% for i=3:94
%     cd(folders(i).name)
%     filesIn = dir('*.csv');
%     copyfile(filesIn.name,'W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\RegResult\RandF_deriv_424_v2\allReg');
%     cd('..')
% end

%%

% PLOT NUC_AREA/THETA 
% a9 = cell2mat(A(:,9));
% a10 = cell2mat(A(:,10));
% a11 = cell2mat(A(:,11));
% a12 = cell2mat(A(:,12));
% nuc_area = outputCell(a9 & a10 & a11 & a12, :);
% nuc_area = fTenzor1(:,:, 120);
% n = length(nuc_area);

n = 498;
for i=1:n
%     if max(MitoPolarA{i, 1}(:,1)) < 4.7 && min(MitoPolarA{i, 1}(:,1)) > -2.2
        B{i, 1} = polCoorCell{i, 1}(:,1) + 2.3;
        nuc_area = csvread(sprintf('Sequence_%03d.csv', i));
        B{i, 2} = nuc_area(:, 119);
%     end
end
m = length(B);
figure
hold on
cellfun(@plot, B(:,1), B(:,2),...
    repmat({'color'},m,1),...
    repmat({[0.5 0.5 0.5]},m,1));

%%
% Filtering
F = gcf;
fLines = F.Children.Children;
for i = 1:length(fLines)
    B_filtered{i,1} = fLines(i).XData';
    B_filtered{i,2} = fLines(i).YData';
end
nuc_area_filtered = cell2mat(B_filtered(:,2)');
theta_filtered = cell2mat(B_filtered(:,1)');


%%

% PLOT MEDIAN TO NUC_AREA/THETA
% theta = cell2mat(B(:,1)');
% nuc_area = cell2mat(B(:,2)');
newMedian = [0.6:0.05:6.1]';
j=1;
newMedian(j,2) = median(nuc_area_filtered(theta_filtered>0 & theta_filtered<0.7));
i = 0.1;
while i < 5.4
    j = j+1;
    newMedian(j,2) = median(nuc_area_filtered(theta_filtered>0.6 + i & theta_filtered<0.6 + i + 0.1));
    i = i + 0.05;
end
newMedian(j+1,2) = median(nuc_area_filtered(theta_filtered>6));
hold on
plot(newMedian(:,1),newMedian(:,2), 'black', 'LineWidth', 4)
F.Children.XLabel.String = 'Theta';
F.Children.YLabel.String = 'nuc__area';

%%

% PLOT MEAN TO NUC_AREA/THETA
newMean = [0.7:0.1:6.8]';
j=1;
newMean(j,2) = mean(nuc_area(theta>0 & theta<0.7));
i = 0.1;
while i < 6
    j = j+1;
    newMean(j,2) = mean(nuc_area(theta>0.7 + i & theta<0.7 + i + 0.1));
    i = i + 0.1;
end
newMean(j+1,2) = mean(nuc_area(theta>6.7));
hold on
plot(newMean(:,1),newMean(:,2), 'red', 'LineWidth', 4)

%%

%%
%  GET NUC FEATURE AND TRAINING COORDINATES
global CommonHandles;
trSet = CommonHandles.TrainingSet;
trFeatures = cell2mat(trSet.Features);
trNuc_area = trFeatures(:,120);
% trNuc_area = trFeatures(:,358);
regPos = trSet.RegPos';
%%

% ADJUST THETA TO ASCENDING ORDER
alpha = 90; % to rotate 90 counterclockwise
R = [cosd(alpha) -sind(alpha); sind(alpha) cosd(alpha)];

for i=1:length(regPos)
aTemp1 = regPos{i, 1}/1000-0.5; % -0.5 is for setting the origo to (0,;0)
aTemp2(1,1) = aTemp1(1,1);
aTemp2(1,2) = aTemp1(1,2) * (-1);
a = aTemp2 * R;
[theta, r] = cart2pol(a(:,1), a(:,2));
if theta >= 0
    trPolarCoordinates(i,1) = theta;
else
    trPolarCoordinates(i,1) = 2*pi + theta;
end
if trPolarCoordinates(i,1) < 0
    pause(1)
end
trPolarCoordinates(i,2) = r;
end

figure
scatter(trPolarCoordinates(:,1),trNuc_area);

%%

normalized_trFeatures = zeros(size(trFeatures));

for i = 1:size(trFeatures,2)
    normalized_trFeatures(:,i) = rescale(trFeatures(:,i));
end

stdValues = zeros((length(0:0.25:6.25)),size(trFeatures,2));
k = 1;
for j = 0:0.25:6
    for i = 1:size(trFeatures,2)
        filterLayer = j<trPolarCoordinates(:,1) & trPolarCoordinates(:,1)<j+0.25;
        filteredFeatures = normalized_trFeatures(filterLayer, i);
        stdValues(k, i) = std(filteredFeatures);
    end
    k = k+1;
end

%find low std

lowSTDidx = find((sum(stdValues,1)>1 & sum(stdValues,1)<1.5));

figure, plot(stdValues)
ax=gca;
ax.XLabel.String = 'Theta';
ax.YLabel.String = 'std of features';

featureNameFile = [CommonHandles.DirName, '\', CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, '\', CommonHandles.MetaDataFolder, '\featureNames.acc'];
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
featureNameList = strrep(featureNameList,'_',' ');

figure, plot(stdValues(:,lowSTDidx))
ax1=gca;
ax1.XLabel.String = 'Theta';
ax1.YLabel.String = 'std of features';

legend(featureNameList(lowSTDidx+2))

for i = 1:length(stdValues)
f = figure;
scatter(trPolarCoordinates(:,1),trFeatures(:,i));
ax1=gca;
ax1.XLabel.String = 'Theta';
ax1.YLabel.String = 'Feature value';
ax1.Title.String = featureNameList(i+2);
f.WindowState ='fullScreen';
saveas(f,[featureNameList{i+2},'.png'])
close(f)
end
%%

UF = length(trFeatures)/3; % num of unique features
featureNameFile = [CommonHandles.DirName, '\', CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, '\', CommonHandles.MetaDataFolder, '\featureNames.acc'];
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
featureNameList = strrep(featureNameList,'_',' ');
regPosMat = cell2mat(regPos);

for i = 1:UF
f = figure;
i
regPosMat = [regPosMat,trPolarCoordinates,trFeatures(:,i)];
ax1 = subplot(2,2,1);
scatter(trPolarCoordinates(:,1),trFeatures(:,i), 'userdata', regPosMat,'ButtonDownFcn',@scatterClick);
ax1.XLabel.String = 'Theta';
ax1.YLabel.String = 'Feature value';
ax1.Title.String = featureNameList(i+2);

ax2 = subplot(2,2,2);
scatter(trPolarCoordinates(:,1),trFeatures(:,UF+i));
ax2.XLabel.String = 'Theta';
ax2.YLabel.String = 'Feature value';
ax2.Title.String = featureNameList(UF+i+2);

ax3 = subplot(2,2,3);
scatter(trPolarCoordinates(:,1),trFeatures(:,2*UF+i));
ax3.XLabel.String = 'Theta';
ax3.YLabel.String = 'Feature value';
ax3.Title.String = featureNameList(2*UF+i+2);

ax4 = subplot(2,2,4);
scatter(trPolarCoordinates(:,1),trFeatures(:,3*UF+i));
ax4.XLabel.String = 'Theta';
ax4.YLabel.String = 'Feature value';
ax4.Title.String = featureNameList(3*UF+i+2);


f.WindowState ='fullScreen';
saveas(f,[featureNameList{i+2},'.png'])
close(f)
end


% for i=1:length(a)
%     cellImName = cell(1,1);
%     for j = 1:length(a(i).Members)
%         cellImName{j, 1} = A{a(i).Members(j), 14}{1, 1}.ImageName;
%     end
%     a(i).ImName = cellImName;
% end
%%
% for i=1:length(MitoGroups)
%     imName = cell2table(MitoGroups(i).ImName);
%     fileName = MitoGroups(i).Name;
%     if iscell(fileName)
%         imName.Properties.VariableNames = fileName;
%         fileName2 = cell2mat(fileName);
%         writetable(imName,fileName2)
%     elseif ischar(fileName)
%         imName.Properties.VariableNames = {fileName};
%         writetable(imName,fileName)
%     end
% end
%%
% for i = 1:353
%      trSet.Features{i, 1} = trSet.Features{i, 1}(1,goodIndx);
% end
%
%%

folders = dir;

n = length(folders);
goodIndx = setdiff(1:281, indx);

f = waitbar(0,'Please wait...');
for i = 3:n
    cd([folders(i).name,'\anal4'])
    featureFiles = dir('*.txt');
    m = length(featureFiles);
    for j = 1:m
        tempVector = csvread(featureFiles(j).name);
        goodVector = tempVector(goodIndx);
        csvwrite(['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\',folders(i).name,'\anal2\', featureFiles(j).name],goodVector);
    end
    waitbar(i/n,f,'Loading your data');
    cd('..');
    cd('..');
end
close(f);
%%
%

folders = dir;

for i = 3:94
    cd([folders(i).name, '\anal2v2']);
    fFiles = dir('*.txt');
    for j = 1:length(fFiles)
        oldName = fFiles(j).name;
        tempNew = split(oldName, '.tiff');
        newName = [tempNew{1}, tempNew{2}];
        copyfile([folders(i).name, '\anal2v2\', oldName], ['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\', folders(i).name,  '\anal2v2\', newName]);
        delete([folders(i).name, '\anal2v2\', oldName]);
    end
    cd('..')
    cd('..')
end

%%
newf = 'W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\140411_KIF11_MitoSys1\anal2v3_central5\featureNames.acc';

for i = 4:94
    folders(i).name
    copyfile(newf, ['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\', folders(i).name,  '\anal2v3_central5\featureNames.acc']);
end

%%

for i = 4:94
    cd(folders(i).name);
    subFold = dir('*.txt');
    delete(subFold(:).name)
    subACC = dir('*.acc');
    delete(subACC.name)
    cd('..');
end


folder = dir('W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan');
%delete unwanted elements
names = {folder.name}';
splittedNames = split(names, '_Mito');
treatmentsName = unique(splittedNames(:,1));
n = length(treatmentsName);
cMatp = hsv(n);

m = length(A);
for j = 1:n
    currentExperiment = treatmentsName{j, 1};
    c = [1 0 0];
    f = figure;
    hold on
    for i = 1:m
        tempName = A{i, 14}{1}.ImageName;
        splitTempName = split(tempName, {'_MitoSys', '\anal'});
        indexList = strcmp(treatmentsName, splitTempName{1});
        if contains(splitTempName{1}, currentExperiment)
            if strcmp(splitTempName{2}, '1')
                scatter(A{i, 1}(:,1), A{i, 1}(:,2), 40,  'MarkerEdgeColor', c, 'MarkerFaceColor', c, 'Marker', 's');
                %         plot(A{i, 1}(:,1), A{i, 1}(:,2), 'LineWidth', 0.5,'Color', c);
            elseif strcmp(splitTempName{2}, '2')
                scatter(A{i, 1}(:,1), A{i, 1}(:,2), 20, 'MarkerEdgeColor', c, 'MarkerFaceColor', c, 'Marker', 'd');
                %         plot(A{i, 1}(:,1), A{i, 1}(:,2), 'LineWidth', 0.5,'Color', c, 'Marker', 'd');
            else
                error('asdf')
            end
        else
            scatter(A{i, 1}(:,1), A{i, 1}(:,2), 20,  'MarkerEdgeColor', [0.5 0.5 0.5]);
        end
        
    end
    f.WindowState ='fullScreen';
    saveas(f,['expScatter_', currentExperiment,'.png'])
    close(f)
end