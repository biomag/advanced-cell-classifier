function polCoorCell = cumPolTrans(oldCell)

m = length(oldCell);
polCoorCell = cell(m, 1);

% Keep the first time point at it's origin
for i = 1:m
    a = oldCell{i, 1}-0.5; % -0.5 is for setting the origo to (0,;0)
    [theta, r] = cart2pol(a(:,1), a(:,2));
    cumTheta = zeros(length(theta), 1); % cumulated theta value
    cumTheta(1) = theta(1);
    
%   calculating the theta value of the time points
    for j = 2:length(a)
        % while the 'r' is too low, interpolate theta value between
        % the previous and the next time point with higher 'r'.
        if r(j, 1) <= 0.15
            if j == length(a)
                cumTheta(j, 1) = cumTheta(j-1, 1);
            else
                k = j;
                q = 0;
                while k < size(r, 1) && r(k, 1) <= 0.15
                        k = k+1;
                        q = q+1;
                end
                diff = (angleDiff(theta(j-1, 1), theta(j+q, 1)))/(q+1);
                cumTheta(j, 1) = cumTheta(j-1, 1) + diff;
            end
        % If 'r' is not too small, calculate the real theta value
        else
            diff = angleDiff(theta(j-1, 1), theta(j, 1));
            cumTheta(j, 1) = cumTheta(j-1, 1) + diff;
        end
    end
    polCoorCell{i, 1} = [cumTheta, r];
end

figure, polar(theta, r)
figure, polar(cumTheta, r)

% Calculating the difference in theta between two time points with respect of diretion
function diff = angleDiff(a, b)

if (a <= -pi/2 && b >= pi/2) || (a >= pi/2 && b <= -pi/2)
    if a > b
        diff = -((pi-abs(a)) + (pi-abs(b)));
    else
        diff = (pi-abs(a)) + (pi-abs(b));
    end
else
    diff = a-b;
end
if abs(diff) >= pi
    if diff > 0
        diff = -(2 * pi) + diff;
    else
        diff = (2 * pi) + diff;
    end
end