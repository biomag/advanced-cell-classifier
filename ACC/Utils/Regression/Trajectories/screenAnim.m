projectFolder = 'W:\beleon\Projects\Lamello\201105_Lamello_4gen__2020-11-05T13_25_11-Measurement2';
imFilesFull = dir(fullfile(projectFolder,'images','*.tiff'));

for i = 1:length(imFilesFull)
    temp1 = strsplit(imFilesFull(i).name,'-');
    temp2a = strsplit(temp1{1}, {'c','f','p'});
    temp2b = strsplit(temp1{2}, {'ch','sk','fk'});
    imFilesFull(i).iC = str2num(temp2a{2});
    imFilesFull(i).fID = str2num(temp2a{3});
    imFilesFull(i).frame = str2num(temp2b{3});
    imFilesFull(i).chanel = str2num(temp2b{2});
end

fieldIDs = sort(randi(length(imFilesFull),20, 1));

bM = 15; % brigthness Multiplier

num_Of_Fields = 91;
num_of_col = 1;
num_of_frames = 49;

tWB = waitbar(0,'Please wait...','Name','Total progress');
for fID = 47:num_Of_Fields %field ID
    waitbar((fID/num_Of_Fields),tWB,'Please wait...','Name','Total progress');
    for iC = 2:2 % image column ID
        tic
        tempMovie = uint16(zeros(1024,1360,3));
        im = cell(24, 1);
        f = figure('Units', 'pixels');
        pause(1)
        ax = axes;
        ax.XLim = [0 1360];
        ax.YLim = [0 1024];
        f.WindowState ='fullScreen';
        title(ax,['Frame ', '1']);
        pause(1)
        for sk = 1:num_of_frames %frame ID
%             tempMovie = uint16(zeros(1024,1360,3));
            currentFiles = {imFiles([imFiles.iC] == iC & [imFiles.fID] == fID & [imFiles.frame] == sk).name};
            ch1 = imread(fullfile(projectFolder,'Images', currentFiles{1}));
            ch2 = imread(fullfile(projectFolder,'Images', currentFiles{2}));
            ch3 = imread(fullfile(projectFolder,'Images', currentFiles{3}));
            ch4 = imread(fullfile(projectFolder,'Images', currentFiles{4}));
            
            tempMovie = cat(3,ch3*4+ch1/2,ch2*1.5+ch1/2,ch4+ch1/2);
            
            im(sk) = {image(ax,...
                'CData',tempMovie*bM,...
                'AlphaData', 0)};
            
        end
        im{1}.AlphaData = 1;
        
        aniMovie(1) = getframe(f);
        
        for i = 2:sk
            for j = 1:10
                im{i}.AlphaData = j/10;
                if j == 5, title(ax, ['Frame ', num2str(i)]); end
                drawnow
                aniMovie((i-2)*10+j+1) = getframe(f);
            end
        end
        pause(1)
        if iC < 10
            filename = ['r01c0',num2str(iC),'f',num2str(fID),'p01'];
        else
            filename = ['r01c',num2str(iC),'f',num2str(fID),'p01'];
        end
        
        v = VideoWriter(['X:\200312-Ibidi-Phagtest__2020-03-12T09_53_51-Measurement1\Video\', filename],'MPEG-4');
        open(v)
        wb = waitbar(0,'Please wait...','Name','Rendering your video');
        L = length(aniMovie);
        for i = 1:L
            if mod(i,10)==0
                waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
                pause(0.1)
            end
            writeVideo(v, aniMovie(i))
        end
        close(wb)
        close(v)
        close(f)
        toc
        fprintf('%d/%d ready\n',fID,num_Of_Fields);
    end
end