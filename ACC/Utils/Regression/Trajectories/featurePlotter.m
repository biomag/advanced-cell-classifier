% varNames = majorFeaturesTable.features_by_frames{1}.Properties.VariableNames;
k = [1 3 7:14];
for i = 1:length(varNames)
    currVar = varNames{i};
    for j = 1:10
        concatFeatures(:, j) = MitoGroups(k(j)).medianFeatures(:, i);
    end
    mergedFeatureTab(:, i) = mergevars(concatFeatures,...
        concatFeatures.Properties.VariableNames,...
        'NewVariableName',currVar);
end
mergedFeatureTab.Properties.VariableNames = varNames;
figure
a1 = stackedplot(mergedFeatureTab(:, 1:5));
figure
a2 = stackedplot(mergedFeatureTab(:, 6:10));
figure
a3 = stackedplot(mergedFeatureTab(:, 11:15));
figure
a4 = stackedplot(mergedFeatureTab(:, 16:20));
figure
a5 = stackedplot(mergedFeatureTab(:, 21:25));
figure
a6 = stackedplot(mergedFeatureTab(:, 26:30));

% for kk = 1:5
% a1.AxesProperties(kk).LegendVisible = 'off';
% a2.AxesProperties(kk).LegendVisible = 'off';
% a3.AxesProperties(kk).LegendVisible = 'off';
% a4.AxesProperties(kk).LegendVisible = 'off';
% a5.AxesProperties(kk).LegendVisible = 'off';
% a6.AxesProperties(kk).LegendVisible = 'off';
% end

a1.AxesProperties(kk).LegendLabels = LegendLab;
a2.AxesProperties(kk).LegendLabels = LegendLab;
a3.AxesProperties(kk).LegendLabels = LegendLab;
a4.AxesProperties(kk).LegendLabels = LegendLab;
a5.AxesProperties(kk).LegendLabels = LegendLab;
a6.AxesProperties(kk).LegendLabels = LegendLab;

saveas(a1,'intesityComperation1.png');
saveas(a2,'intesityComperation2.png');
saveas(a3,'intesityComperation3.png');
saveas(a4,'intesityComperation4.png');
saveas(a5,'intesityComperation5.png');
saveas(a6,'intesityComperation6.png');

