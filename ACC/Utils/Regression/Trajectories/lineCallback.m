function lineCallback(src,~)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     lineCallback
%
% Click callback function to open LineProp GUI.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

trajAxesHandle = src.Parent;
trajData = get(trajAxesHandle.Parent,'UserData');

if isempty(src.UserData{5}) || src.UserData{5} == 0
    src.UserData(5) = {1};
    trajData.linePropHandles{str2double(src.Tag)} = LineProp(src);
else
    figure(trajData.linePropHandles{str2double(src.Tag)});
    return
end
