function savePathAnimation(CellImg, animResolution, tail, trajID, x, y, fileName)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     pathAnimation
%
% INPUT
%   CellImg         - cellarray contains RGB image of the current cell of each
%                     frame.
%   animResolution  - number of steps interpolated between timepoints of the cell path
%                     larger value makes the animation slower.
%
%   trajID          - ID of the selected trajectory
%
%   x, y            - coordinates to plot the line
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

% Parse possible Axes input

narginchk(7,7)

x = matlab.graphics.chart.internal.datachk(x);
y = matlab.graphics.chart.internal.datachk(y);

f = figure('Units', 'pixels', 'Name', 'Cell Path Animation', 'Resize', 'off', 'DockControls', 'off',...
    'MenuBar', 'none', 'ToolBar', 'none', 'HitTest', 'off',...
    'ButtonDownFcn', @(h,e) msgbox('Please don not close or resize this window!'));
f.WindowState ='fullScreen';
ax1 = subplot(1,2,1);

imSizeArray = zeros(length(CellImg), 3);

for b = 1:length(CellImg)
    imSizeArray(b, :) = size(CellImg{b});
end

maxSize = max(max(imSizeArray));

ax1.XLim = [0, maxSize];
ax1.YLim = [0, maxSize];
title(ax1,['Frame ', '1']);
ax1.TitleFontSizeMultiplier = 2;
ax1.DataAspectRatio = [1 1 1];

ax2 = subplot(1,2,2);
title(ax2,['Trajectory ID: ', trajID]);
ax2.XLim = [0, 1];
ax2.YLim = [0, 1];
ax2.TitleFontSizeMultiplier = 2;
ax2.DataAspectRatio = [1 1 1];

messageHelp = helpdlg('This calculation can run in the background, you can switch windows.');
MessageWarn = warndlg('Please don''t close the animation figure!');

m = length(x);
k = tail * animResolution;
frameLength = floor(m / (length(CellImg)-1));

head = line('parent',ax2,'marker','o','linestyle','none',...
    'MarkerSize', 10, 'xdata',x(1),'ydata',y(1),'Tag','head');

body = matlab.graphics.animation.AnimatedLine('Parent',ax2,...
    'LineWidth', 2,...
    'MaximumNumPoints',max(1,k),'tag','body');

try
    % Draw images to ax1 and set Aplha to 0
    im = cell(length(CellImg), 1);
    for i = 1:length(CellImg)
        imsize = size(CellImg{i});
        im(i) = {image(ax1,...
            'XData',[(ax1.XLim(2) - imsize(1))/2, (ax1.XLim(2) + imsize(1))/2],...
            'YData',[(ax1.YLim(2) - imsize(2))/2, (ax1.YLim(2) + imsize(2))/2],...
            'CData',flip(CellImg{i},1),...
            'AlphaData', 0)};
    end
    im{1}.AlphaData = 1;
    jj = 2;
    
    if ishandle(f)
        aniMovie(1) = getframe(f);
    else
        errordlg('You closed the animation figure. The process is terminated.');
        return
    end
    
    % Elongate the Line
    for i = 1:k
        
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        if mod(i, frameLength) == 0
            im{jj}.AlphaData = 1;
            jj = jj + 1;
        else
            im{jj}.AlphaData = mod(i, frameLength) / animResolution;
        end
        if mod(i, animResolution) == 0
            title(ax1, ['Frame ', num2str((i/animResolution)+1)]);
        end
        drawnow
        if ishandle(f)
            aniMovie(i+1) = getframe(f);
        else
            errordlg('You closed the animation figure. The process is terminated.');
            return
        end
    end
    
    % Animate the line moving
    for i = k+1:m
        if ~(isvalid(head) && isvalid(body))
            return
        end
        set(head,'xdata',x(i),'ydata',y(i));
        addpoints(body,x(i),y(i));
        
        if mod(i, frameLength) == 0
            im{jj}.AlphaData = 1;
            jj = jj + 1;
        else
            im{jj}.AlphaData = mod(i, frameLength) / animResolution;
        end
        if mod(i, animResolution) == 0
            title(ax1, ['Frame ', num2str((i/animResolution)+1)]);
        end
        drawnow
        if ishandle(f)
            aniMovie(i+1) = getframe(f);
        else
            errordlg('You closed the animation figure. The process is terminated.');
            return
        end
    end
catch
    errordlg('Animation processing is failed', 'Error')
end
pause(0.1);
try
    v = VideoWriter(fileName, 'Motion JPEG AVI');
    open(v)
    wb = waitbar(0,'Please wait...','Name','Rendering your video');
    
    L = length(aniMovie);
    for i = 1:L
        if mod(i,10)==0 && ishandle(wb)
            waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
            pause(0.1)
        end
        writeVideo(v, aniMovie(i))
    end
catch
    errordlg('Can not render video file.', 'Render Error')
end
if ishandle(wb), close(wb), end
close(v)
if ishandle(f), close(f), end
if ishandle(messageHelp), close(messageHelp), end
if ishandle(MessageWarn), close(MessageWarn), end