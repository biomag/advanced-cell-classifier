function fTenzor = calcFTenzor(trackFile)

global CommonHandles;

%% Input vars
% baseDir = CommonHandles.DirName;

srcDir = CommonHandles.DirName;
analDir = CommonHandles.MetaDataFolder;
featureFileName = 'featureNames.acc';
trackTable = readtable(trackFile);
featureFile = fullfile(srcDir,trackTable.Plate{1},analDir,featureFileName);
fTable = readtable(featureFile,'FileType','text','Delimiter',',','ReadVariableNames',false);


%% Calc matrices




uniTracks = unique(trackTable.TrackID);
N = length(uniTracks);

F_NUM = size(fTable,1)-2;
frames = max(trackTable.Frame);

fullLengthTraj = 1:frames;
fTenzor = zeros(N, frames, F_NUM);

for i=1:N
    tic
    currTrackID = uniTracks(i);
    relevantTable = trackTable(trackTable.TrackID == i,:);
    currentFrame = relevantTable.Frame';
    for j = 1:length(currentFrame)
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(srcDir,relevantTable.Plate{j},analDir,[imgNameExEx '.txt']);
        dataM = load(fileName);
        fTenzor(i, currentFrame(j), :) = dataM(relevantTable.ObjectID(j),3:end);
    end
    missingFrames = setdiff(fullLengthTraj, currentFrame);
    if ~isempty(missingFrames)
        fTenzor(i, missingFrames, :) = nan;
    end
    toc
    fprintf('%d/%d ready\n',i,length(uniTracks));
end


% thetaMatrix = zeros(N, frames);
% 
% for i = 1:N
%     thetaMatrix(i,:) = polCoorCell{i,1}(:,1)';
% end
% 
% normalized_fTenzor = zeros(N, frames, F_NUM);
% 
% for i = 3:F_NUM
%     normalized_fTenzor(:,:,i) = rescale(fTenzor1(:,:,i));
% end
% 
% stdValues = zeros(498,(length(-2.6:0.2:5)));
% k = 1;
% 
% for j = -2.6:0.2:5
%     filterLayer = j<thetaMatrix & thetaMatrix<j+0.2;
%     filterTenzor = repmat(filterLayer,[1 1 F_NUM-2]);
%     adNANTenzor = normalized_fTenzor.*filterTenzor;
%     adNANTenzor(adNANTenzor == 0) = NaN;
%     stdValues(:, k) = std(adNANTenzor,0,[1 2],'omitnan');
%     k = k+1;
% end
% 
% %find low std
% 
% lowSTDidx = find((sum(stdValues,2)>1 & sum(stdValues,2)<1.5));
% 
% featureNameFile = [CommonHandles.DirName, '\', CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, '\', CommonHandles.MetaDataFolder, '\featureNames.acc'];
% featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
% featureNameList = strrep(featureNameList,'_',' ');
% 
% figure, plot(stdValues(lowSTDidx,:)', 'Legend', featureNameList(lowSTDidx+2))
% ax=gca;
% ax.XLabel.String = 'Theta';
% ax.YLabel.String = 'std of features';
% 
% legend(featureNameList(lowSTDidx+2))