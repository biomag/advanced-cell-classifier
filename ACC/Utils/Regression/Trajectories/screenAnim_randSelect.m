projectFolder = 'W:\beleon\Projects\Lamello\201105_Lamello_4gen__2020-11-05T13_25_11-Measurement2';
imFilesFull = dir(fullfile(projectFolder,'images','*.tiff'));
targetDir = 'D:\201105_Lamello_4gen__2020-11-05T13_25_11-Measurement2\Video\';
mkdir(targetDir);
randSample = 4;

for i = 1:length(imFilesFull)
    temp1 = strsplit(imFilesFull(i).name,'-');
    temp2a = strsplit(temp1{1}, {'r','c','f','p'});
    temp2b = strsplit(temp1{2}, {'ch','sk','fk'});
    imFilesFull(i).iR = str2num(temp2a{2});
    imFilesFull(i).iC = str2num(temp2a{3});
    imFilesFull(i).fID = str2num(temp2a{4});
    imFilesFull(i).frame = str2num(temp2b{3});
    imFilesFull(i).channel = str2num(temp2b{2});
end



bM = 8; % brigthness Multiplier

uniqueFields = unique(arrayfun(@(z) z.fID, imFilesFull));
uniqueRow = unique(arrayfun(@(z) z.iR, imFilesFull));
uniqueCol = unique(arrayfun(@(z) z.iC, imFilesFull));
uniqueFrames = unique(arrayfun(@(z) z.frame, imFilesFull));
uniqueChannels = unique(arrayfun(@(z) z.channel, imFilesFull));

fieldIDs = sort(randi(length(uniqueFields),randSample, 1));

tWB = waitbar(0,'Please wait...','Name','Total progress');
for fID = 1:randSample %field ID
    waitbar((fID/randSample),tWB,'Please wait...','Name','Total progress');
    for iR = 1:numel(uniqueRow)
        for iC = 1:numel(uniqueCol) % image column ID
            tic
            currFrame = uint16(zeros(1024,1360,3));
            im = cell(numel(uniqueFrames), 1);
            f = figure('Units', 'pixels');
            pause(0.2)
            ax = axes;
            ax.XLim = [0 1360];
            ax.YLim = [0 1024];
            %         f.WindowState ='fullScreen';
            f.Visible = 'off';
            title(ax,['Frame ', '1']);
            pause(0.2)
            for sk = 1:length(uniqueFrames) %frame ID
                %             tempMovie = uint16(zeros(1024,1360,3));
                currentFiles = {imFilesFull([imFilesFull.iC] == uniqueCol(iC) & [imFilesFull.fID] == fieldIDs(fID) & [imFilesFull.frame] == uniqueFrames(sk)).name};
                ch1 = imread(fullfile(projectFolder,'Images', currentFiles{1}));
                ch2 = imread(fullfile(projectFolder,'Images', currentFiles{2}));
                ch3 = imread(fullfile(projectFolder,'Images', currentFiles{3}));
                currFrame = cat(3,ch3*1+ch1/3,ch2*1+ch1/3,ch1/3);
%                 figure, image(currFrame*bM);
                
                %             ch4 = imread(fullfile(projectFolder,'Images', currentFiles{4}));
                %             currFrame = cat(3,ch3*4+ch1/2,ch2*1.5+ch1/2,ch4+ch1/2);
                
                im(sk) = {image(ax,...
                    'CData',currFrame*bM,...
                    'AlphaData', 0)};
                
            end
            im{1}.AlphaData = 1;
            
            aniMovie(1) = getframe(f);
            
            for i = 2:sk
                for j = 1:10
                    im{i}.AlphaData = j/10;
                    if j == 5, title(ax, ['Frame ', num2str(i)]); end
                    drawnow
                    aniMovie((i-2)*10+j+1) = getframe(f);
                end
            end
            pause(0.2)
            if iC < 10
                colFiller = 'c0';
            else
                colFiller = 'c';
            end
            
            if iR < 10
                rowFiller = 'r0';
            else
                rowFiller = 'r';
            end
            
            filename = [rowFiller,num2str(uniqueRow(iR)),colFiller,num2str(uniqueCol(iC)),'f',num2str(fieldIDs(fID)),'p01'];
            
            currentFiles{1}
            filename
            
            v = VideoWriter([targetDir, filename],'MPEG-4');
            open(v)
            wb = waitbar(0,'Please wait...','Name','Rendering your video');
            L = length(aniMovie);
            for i = 1:L
                if mod(i,10)==0
                    waitbar((i/L),wb,'Please wait...','Name','Rendering your video');
                    pause(0.1)
                end
                writeVideo(v, aniMovie(i))
            end
            close(wb)
            close(v)
            close(f)
            toc
%             fprintf('%d/%d ready\n',fID*iR + iC,randSample*numel(uniqueCol)*numel(uniqueRow));
        end
    end
end
close(tWB)