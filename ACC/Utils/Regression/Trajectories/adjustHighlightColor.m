function adjustHighlightColor(buttonID, buttonState, selectedGroup, operationVariable, handles)

pTPC = get(handles.figure1,'UserData');

groupIdx = strcmp([pTPC.groupList.Name], selectedGroup);
if 1
    members = checkVisibility(handles, pTPC.groupList(groupIdx).Members);
else
    members = pTPC.groupList(groupIdx).Members;
end


if buttonState == 1 %on \\ operationVariable is the color
    
    highlightWidth = 2;
    set(pTPC.lines(members), 'Color', operationVariable);
    set(pTPC.lines(members), 'LineWidth', highlightWidth);
    set(pTPC.lines(members), 'Visible', 'on');
    
    for i=1:length(members)
        pTPC.lines(members(i)).UserData(4) = {union(pTPC.lines(members(i)).UserData{4}, buttonID)};
        if numel(pTPC.lines(members(i)).UserData{4}) > 1
            highLightIdx = pTPC.lines(members(i)).UserData{4};
            colorsToMix = cell2mat({handles.uipanel_toogleGroup.Children(16-highLightIdx).BackgroundColor}');
            hsvHighlights = rgb2hsv(colorsToMix);
            mixedHSV = [mean(hsvHighlights(:, 1)), 1, 1];
            mixedRGB = hsv2rgb(mixedHSV);
            set(pTPC.lines(members(i)), 'Color', mixedRGB);
            
            if numel(pTPC.lines(members(i)).UserData{4}) >= 2
                set(pTPC.lines(members(i)), 'LineWidth', 4);
            else
                set(pTPC.lines(members(i)), 'LineWidth', 2);
            end
        end
    end
    
elseif buttonState == 0 %off \\ operationVariable is the color
    for i=1:length(members)
        pTPC.lines(members(i)).UserData(4) = {setdiff(pTPC.lines(members(i)).UserData{4}, buttonID)};
        if isempty(pTPC.lines(members(i)).UserData{4})
            pTPC.lines(members(i)).Color = pTPC.lines(members(i)).UserData{2};
            pTPC.lines(members(i)).LineWidth = pTPC.lines(members(i)).UserData{3};
        else
            highLightIdx = pTPC.lines(members(i)).UserData{4};
            colorsToMix = cell2mat({handles.uipanel_toogleGroup.Children(16-highLightIdx).BackgroundColor}');
            hsvHighlights = rgb2hsv(colorsToMix);
            mixedHSV = [mean(hsvHighlights(:, 1)), 1, 1];
            mixedRGB = hsv2rgb(mixedHSV);
            set(pTPC.lines(members(i)), 'Color', mixedRGB);
            
            if numel(pTPC.lines(members(i)).UserData{4}) >= 2
                set(pTPC.lines(members(i)), 'LineWidth', 4);
            else
                set(pTPC.lines(members(i)), 'LineWidth', 2);
            end
        end
    end
    
    a = [pTPC.lines(members).UserData]';
    b = a(:,4);
    tempIdx = find(cellfun('isempty', b));
    [visibleTraj, nonVisibleTraj] = checkVisibility(handles, members(tempIdx));
    
    set(pTPC.lines(visibleTraj), 'Visible', 1);
    set(pTPC.lines(nonVisibleTraj), 'Visible', 0);
end