function Cmet = multyAnimCoorIntPol(inputCell, resolution, tail)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     multyAnimCoorIntPol
%
% This function calculates extra coordinates by interpolating between regression
% positions of the curret selected cell.
% 
% INPUTS
%   inputCell  - cell array that contains the coordinates of regression
%                position.
%
%   resolution - Number of points created between cordinates.
%
%        tail - The length of the tail in frames.
%
% OUTPUTS
%   Cmet       - Array that countains the original and interpolated
%                coordinates for the animatian
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

M = size(inputCell, 1);
Cmet = cell(M, 2);
for j = 1:M
    Q = cell(length(inputCell{j, 1})*resolution, 1);
    if size(inputCell{j, 1}, 1) > tail
        for i=1:length(inputCell{j, 1})-1
            q = zeros(resolution,2);
            e = ((inputCell{j, 1}(i+1,:) - inputCell{j, 1}(i,:))/(resolution)); % length of steps
            for r=0:resolution-1
                q(r+1, :) = inputCell{j, 1}(i,:) + (e*r);
            end
            Q{i} = q;
        end
        Cmet{j, 1} = j;
        Cmet{j, 2} = [cell2mat(Q); inputCell{j, 1}(end,:)];
    else
        Cmet{j, 1} = j;
    end
end