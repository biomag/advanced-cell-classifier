% for i = 1:75
%     cytoRaw.Properties.VariableNames{i} = ['cyto_', cytoRaw.Properties.VariableNames{i}];
%     nucRaw.Properties.VariableNames{i} = ['nuc_', nucRaw.Properties.VariableNames{i}];
% end
%%
% unwantedIDX = [2:12, 14:16, 18:27, 30:75];
% 
% cytoRaw(:, unwantedIDX) = [];
%%
% mito3DFeature = table('size', [20200 12], 'VariableTypes', repmat("double",12,1),...
%     'VariableNames', [cytoRaw.Properties.VariableNames, nucRaw.Properties.VariableNames]);

% for i = 16314:20200
% %     if i == 16313
% %         pause(1)
% %     end
%     cytoTempF = cytoRaw(cytoRaw.cyto_ImageNumber == i, :);
%     if size(cytoTempF, 1) == 1
%         mito3DFeature(i, 1:5) = cytoTempF;
%         mito3DFeature.cyto_obj_num(i) = 1;
%     elseif size(cytoTempF, 1) > 1
%         if cytoTempF.cyto_AreaShape_Volume(1) == cytoTempF.cyto_AreaShape_Volume(2)
%             mito3DFeature(i, 1:5) = cytoTempF(1, :);
%         else
%             mito3DFeature(i, 1:5) = cytoTempF(cytoTempF.cyto_AreaShape_Volume == max(cytoTempF.cyto_AreaShape_Volume), :);
%         end
%         mito3DFeature.cyto_obj_num(i) = 2;
%         
%     else
%         error('missing instance')
%     end
%     
%     nucTempF = nucRaw(nucRaw.nuc_ImageNumber == i, :);
%     if size(nucTempF, 1) == 1
%         mito3DFeature(i, 6:12) = nucTempF;
%         mito3DFeature.nuc_obj_num(i) = 1;
%         mito3DFeature.nuc_obj_distance(i) = 0;
%     elseif size(nucTempF, 1) > 1
%         if nucTempF.nuc_AreaShape_Volume(1) == nucTempF.nuc_AreaShape_Volume(1)
%             mito3DFeature(i, 6:12) = nucTempF(1, :);
%         else
%             mito3DFeature(i, 6:12) = nucTempF(nucTempF.nuc_AreaShape_Volume == max(nucTempF.nuc_AreaShape_Volume), :);
%         end
%         mito3DFeature.nuc_obj_num(i) = 2;
%         a(1) = nucTempF.nuc_AreaShape_Center_X(1);
%         a(2) = nucTempF.nuc_AreaShape_Center_X(2);
%         a(3) = nucTempF.nuc_AreaShape_Center_Y(1);
%         b(1) = nucTempF.nuc_AreaShape_Center_Y(2);
%         b(2) = nucTempF.nuc_AreaShape_Center_Z(1);
%         b(3) = nucTempF.nuc_AreaShape_Center_Z(2);
%         D = norm(a-b);
%         mito3DFeature.nuc_obj_distance(i) = D;
%         
%         
%     else
%         error('missing instance')
%     end
%     
%     
% end

%%

for i = 1:20200
    
    imName = split(ImFeatureRaw.FileName_CytoplasmImage{i},{'m2_', '.tif'});
    inputPathName = ['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\',...
        ImFeatureRaw.Metadata_PlateName{i},'\anal2\',...
        imName{2}, '.txt'];
    outputPathName = ['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\',...
        ImFeatureRaw.Metadata_PlateName{i},'\anal2_plus3d\',...
        imName{2}, '.txt'];
    
    tempFeature = csvread(inputPathName);
    tempFeature(3) = mito3DFeature.cyto_AreaShape_Volume(i);
    tempFeature(7) = mito3DFeature.cyto_AreaShape_Extent(i);
    tempFeature(16) = mito3DFeature.cyto_AreaShape_SurfaceArea(i);
    tempFeature = [tempFeature(1:17), mito3DFeature.cyto_AreaShape_Center_Z(i), tempFeature(18:251)];
    tempFeature(127) = mito3DFeature.cyto_obj_num(i);
    tempFeature(128) = mito3DFeature.nuc_AreaShape_Volume(i);
    tempFeature(132) = mito3DFeature.nuc_AreaShape_Extent(i);
    tempFeature(141) = mito3DFeature.nuc_AreaShape_SurfaceArea(i);
    tempFeature(252) = mito3DFeature.nuc_obj_num(i);
    tempFeature = [tempFeature, mito3DFeature.nuc_obj_distance(i)];
    
    if ~exist(['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\',...
        ImFeatureRaw.Metadata_PlateName{i},'\anal2_plus3d'], 'dir')
        
        mkdir(['W:\beleon\ACC_Project\MitoCheck\mitoMerged_3Chan\',...
        ImFeatureRaw.Metadata_PlateName{i},'\anal2_plus3d'])
    end
    
    
    csvwrite(outputPathName, tempFeature);
end
    
