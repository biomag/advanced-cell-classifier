function plotTrajGUIhandle = openTrajectoryGUI(predictionMode)
% AUTHOR:	Abel Szkalisity
% DATE: 	June 05, 2019
% NAME: 	predictionMode
% 
% To open the trajectory GUI that helps investigating live-imaging data by
% drawing the regression plane trajectories of individual cells over time.
% This function prepares the data for this.
%
% INPUT:
%        predictionMode     A boolean indicating if the regressionPlane
%                           from which this function is called is in
%                           predictionMode or not. In the first case the
%                           data is prepared from the currently selected
%                           iamges.
% OUTPUT:
%        plotTrajGUIhandle  The handle the to plot trajectory GUI object
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2019 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

plotTrajGUIhandle = [];

try
    screensize = get(0,'Screensize');
    h = dialog('Name','Trajectory visualizer','Position',[screensize(3)/2-180, screensize(4)/2-30,500,70]);
    pause(0.1)
    infoText = uicontrol('Parent',h,'Style','text','Units','Pixels','Position',[30 20 440 30],'String','Reading prediction data...', 'HorizontalAlignment', 'center');    
    plateArray = calculatePlateArray(predictionMode,infoText);    
    
    trackTable = getTrajectoryInfo(infoText);
    
    trackIDs = unique(trackTable.TrackID);
            
    nofTrajs = length(trackIDs);
    trajGUIInfoArray = cell(nofTrajs,15);
    k = 1;
    
    if ishandle(infoText), infoText.String = sprintf('Matching trajectories with regression output...'); end; pause(0.1);
    h2 = waitbar(0,sprintf('Matching trajectories with regression output (%d/%d)',0,nofTrajs));
    for i=1:nofTrajs
        currentIndices = find(trackTable.TrackID == trackIDs(i));
        relevantTable = trackTable(currentIndices,:);
        trajLength = length(currentIndices);
        tmpPositions = zeros(trajLength,2);
        j2CstructsAll = cell(trajLength,1);
        frameID = zeros(trajLength,1);
        for j=1:length(currentIndices)
            %search for the best matching object            
            [j2CstructsAll{j},tmpPositions(j,:), frameID(j)] = matchCellFromTrackTable(plateArray, relevantTable, j); %first argument is the jump2CellStruct might be important later on
        end
        currentTrajInfo = convertPositionsToTrajGUI(tmpPositions,j2CstructsAll, frameID);
        if ~isempty(currentTrajInfo)
            trajGUIInfoArray(k,:) = currentTrajInfo;
            k = k+1;
        end
        
        if ishandle(h2), waitbar(i/nofTrajs,h2,sprintf('Matching trajectories with regression output (%d/%d)',i,nofTrajs)); end
    end
    trajGUIInfoArray(k:end,:) = [];
    plotTrajGUIhandle = plotTrajGui(trajGUIInfoArray);
    disp('Ready for gui');
catch e    
    errordlg(e.message);
    disp(getReport(e));
end
if ishandle(h), close(h); end
if exist('h2','var') && ishandle(h2), close(h2); end


end

function plateArray = calculatePlateArray(predictionMode,infoText)
    global CommonHandles;    

    if predictionMode
        plateArray = cell(1,length(CommonHandles.PlatesNames));
        if ishandle(infoText), infoText.String = sprintf('Creating Map based on Image Names to fasten later computation...'); pause(0.1); end
        for i=1:numel(CommonHandles.RegressionPlaneHandle.UserData.predictedIcons)
            jumpToCell(CommonHandles.RegressionPlaneHandle.UserData.predictedIcons{i},0); %Jump to the cell to get the x-y info
            objNumber = CommonHandles.RegressionPlaneHandle.UserData.predictedIcons{i}.CellNumber;
            xPos = CommonHandles.SelectedMetaData(objNumber,1);
            yPos = CommonHandles.SelectedMetaData(objNumber,2);
            [~,imgName,~] = fileparts(CommonHandles.RegressionPlaneHandle.UserData.predictedIcons{i}.ImageName);
            plateArray = addCellToPlateArray(...
                    plateArray,...
                    CommonHandles.RegressionPlaneHandle.UserData.predictedIcons{i}.PlateName,...
                    imgName,...
                    objNumber,...
                    xPos,...
                    yPos,...
                    CommonHandles.RegressionPlaneHandle.UserData.predictedPositions(1,i)/1000,...
                    CommonHandles.RegressionPlaneHandle.UserData.predictedPositions(2,i)/1000);
        end        
    else
        hdlg = helpdlg('Select a previously saved "Regression Result" file! (.csv)', 'regResult');
        uiwait(hdlg)
        
        [predfile,predpath] = uigetfile('*.csv','Please select a regression single cell prediction file',CommonHandles.DirName,'MultiSelect','on');
        
        if isnumeric(predfile), error('Please provide prediction files!'); end
        
        if ~iscell(predfile)
            predfile = {predfile};
        end
        %Create a data structure easier to mine
        plateArray = cell(1,length(CommonHandles.PlatesNames));    
        for j=1:length(predfile)
            predData = readtable(fullfile(predpath,predfile{j}));  
            if ishandle(infoText), infoText.String = sprintf('Prediction data reading is done\n            (%d/%d)',j,length(predfile)); end; pause(0.1);
            pause(0.2);
            toCheckFields = {'ObjectNumber','regPosX','regPosY','ImageName','PlateName','xPixelPos','yPixelPos'};
            checkTable(toCheckFields,predData);           

            if ishandle(infoText), infoText.String = sprintf('Creating Map based on Image Names to fasten later computation\n                           (%d/%d)',j,length(predfile)); end
            pause(0.2);            
            for i=1:size(predData,1)
                plateIdx = getPlateNumberByName(predData.PlateName{i});
                if plateIdx == 0
                    error('Selected "Regression Result" file is not represented in this project!')
                end
                plateArray = addCellToPlateArray(...
                    plateArray,...
                    plateIdx,...
                    predData.ImageName{i},...
                    predData.ObjectNumber(i),...
                    predData.xPixelPos(i),...
                    predData.yPixelPos(i),...
                    predData.regPosX(i),...
                    predData.regPosY(i));
            end
        end
    end
end

function plateArray = addCellToPlateArray(plateArray,plateIdx,imgName,objNumber,xLoc,yLoc,xReg,yReg)
    if isempty(plateArray{plateIdx})
        plateArray{plateIdx} = containers.Map;
        plateArray{plateIdx}(imgName) = [objNumber xLoc yLoc xReg yReg];
    else
        if plateArray{plateIdx}.isKey(imgName)
            currentData = plateArray{plateIdx}(imgName);
            currentData = [currentData; objNumber xLoc yLoc xReg yReg];
            plateArray{plateIdx}(imgName) = currentData;
        else
            plateArray{plateIdx}(imgName) = [objNumber xLoc yLoc xReg yReg];
        end
    end
end

function trackTable = getTrajectoryInfo(infoText)
    global CommonHandles;
    answer = questdlg('How do you want to provide tracking information?','Trackging method selection','Tracking file','Calculate now','Tracking file');
    if isempty(answer)
        error('It is required to select a tracking method');
    end
    if strcmp(answer,'Tracking file')
        [trackfile,trackpath] = uigetfile('*.csv','Please select a tracking file', CommonHandles.DirName, 'MultiSelect','on');
        
        if ishandle(infoText), infoText.String = 'Reading tracking file'; end; pause(0.1);
        if numel(trackfile) > 1
            trackTable = readtable(fullfile(trackpath,trackfile{1}));
            for i = 2:numel(trackfile)
                tempTable = readtable(fullfile(trackpath,trackfile{i}));
                maxID = max(trackTable.TrackID);
                tempTable.TrackID = tempTable.TrackID + maxID;
                trackTable = [trackTable; tempTable];
            end
        else
            trackTable = readtable(fullfile(trackpath,trackfile));
        end
        if ishandle(infoText), infoText.String = 'Tracking data reading is done'; end; pause(0.1);    
        
        toCheckFields = {'ImageName','Plate','TrackID','tracking__center_x','tracking__center_y','Frame'};
        checkTable(toCheckFields,trackTable);                        
        
    elseif strcmp(answer,'Calculate now')
        trackTable = TrackingWizardGUI();
        if isempty(trackTable)
            error('ACC cannot produce trajectories without tracking information');
        end
    end
end

% function checkTable(toCheckFields,currTable)
%     for i=1:length(toCheckFields)
%             if ~ismember(toCheckFields{i},currTable.Properties.VariableNames)                
%                 error([toCheckFields{i} ' column is missing from the loaded csv, but required.']);
%             end        
%     end
% end

function [trajInfo,corrIdx] = convertPositionsToTrajGUI(tmpPositions,j2cStructs, frameID)
%input: n by 2 matrix with the positions, but can contain NaNs
%output: 1 by 12 cellarray obeying the requirements for the TrajectoryGUI.
%The 12th entry is as long as the first and encodes if a position is an
%interpolation OR real regression position
%corrIdx gives back the indices that are real regression positions

if all(all(isnan(tmpPositions)))
    trajInfo = [];
    return;
end

trajInfo = cell(1,15);

corrIdx = find(all(~isnan(tmpPositions),2));
actTrajLength = corrIdx(end)-corrIdx(1)+1;
corrPos = zeros(actTrajLength,2);
j2CcorrectPos = cell(actTrajLength,1);
for i=1:actTrajLength
    tmpIdx = corrIdx(1)+i-1;
    if all(isnan(tmpPositions(tmpIdx,:)))
        nextReal = corrIdx(find(corrIdx == prevReal)+1);
        corrPos(i,:) = ((nextReal-tmpIdx)*tmpPositions(prevReal,:)+(tmpIdx-prevReal)*tmpPositions(nextReal,:)) / (nextReal-prevReal);
    else
        corrPos(i,:) = tmpPositions(tmpIdx,:);
        prevReal = tmpIdx;
    end
    j2CcorrectPos{i} = j2cStructs{tmpIdx};
end
realPos = zeros(1,actTrajLength);
realPos(corrIdx - corrIdx(1)+1) = 1;

trajInfo{1} = corrPos; %positions to show
trajInfo{2} = sum(vecnorm(corrPos(1:end-1,:)-corrPos(2:end,:),2,2)); %total length of trajectory
trajInfo{3} = actTrajLength; %length of the trajectory (frames) (slider #2)
trajInfo{4} = norm(corrPos(1,:)-corrPos(end,:)); %distance between start and end (slider #1)
trajInfo{5} = max(corrPos(:,1)); %highest x
trajInfo{6} = max(corrPos(:,2)); %higest y
trajInfo{7} = corrPos(1,:); %start point
trajInfo{8} = corrPos(end,:); %end point
trajInfo{9} = 1; %visibility 1
trajInfo{10} = 1; %visibility 2
trajInfo{11} = 1; %visibility 3
trajInfo{12} = 1; %visibility 4
trajInfo{13} = realPos; %Bool array indicating if a position is real or interpolated
trajInfo{14} = j2CcorrectPos;
trajInfo{15} = frameID(corrIdx(1):corrIdx(end));
end



