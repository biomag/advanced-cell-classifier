function [orderedVisibleTraj, orderedNonVisibleTraj] = checkVisibility(handles, varargin)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     checkVisibility
%
% Calculate the visibility necessity of trajectories depending on the filtering choices.
%
% OUTPUT
%           orderedVisibileTraj   -  Array that countains the ID of visible
%           trajectories.
%
%           orderedNonVisibleTraj -  Array that countains IDs of
%           non-visible trajectories
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

pTPC = get(handles.figure1,'UserData');

narginchk(1, 2)
if nargin == 2
    Idx = varargin{1};
else
    Idx = 1:pTPC.n;
end

visMat = cell2mat(pTPC.A(Idx,9:12)); %matrix that contains the visibility data
orderedVisibleTraj = Idx(find(all(visMat,2)));
orderedNonVisibleTraj = setdiff(Idx,orderedVisibleTraj);