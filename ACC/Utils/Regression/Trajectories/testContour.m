im1 = mask1;
im2 = mask2;
% selem = ones(3,3); %// square, 8-Negihbours
selem = [0 1 0; 1 0 1; 0 1 0]; %// cross, 4-Neighbours
% selem = [1 0; 1 0; 0 1];
out1 = imerode(im1, selem) ~= imdilate(im1, selem);
skOut1 = bwskel(out1);

out2 = imerode(im2, selem) ~= imdilate(im2, selem);
skOut2 = bwskel(out2);