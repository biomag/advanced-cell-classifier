currentImage = imread('1.png');
imshow(currentImage)
axis tight
set(gca,'nextplot','replacechildren','visible','off')
f = getframe;
[im,map] = rgb2ind(f.cdata,256,'nodither');
im(:,:,1,1) = rgb2ind(f.cdata,map,'nodither');
for k = 2:6
  currentImage = imread([num2str(k), '.png']);
  imshow(currentImage)
  f = getframe;
  im(:,:,1,k) = rgb2ind(f.cdata,map,'nodither');
end
imwrite(im,map,'trajLevelCoordinate.gif','DelayTime',0,'LoopCount',inf)

% trajLevelCoordinate
% trajLevelDistance
% trajLevelLength