function Cmet = animCoorIntPol(inputCell, resolution)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     animCoorIntPol
%
% Interpolate new points between regression positions of the currently selected cell.
% These points are necessary to make the animation more smooth. 
%
% INPUTS
%       inputCell  -  cell array that contains the coordinates of regression
%                     position.
%
%       resolution -  Number of points created between cordinates.
%
% OUTPUT
%           Cmet   -  Array that countaions the coordinates to animate.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

Q = cell(length(inputCell), 1);
for i=1:length(inputCell)-1
    q = zeros(10,2);
    for r=1:resolution
        q(r, :) = inputCell(i,:) + (((inputCell(i+1,:) - inputCell(i,:))/(resolution+1))*r);
    end
    Q{i} = q;
end
Cmet = cell2mat(Q);