trajIDs = MitoGroups(14).Members;
featureNameFile = [CommonHandles.DirName, '\', CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, '\', CommonHandles.MetaDataFolder, '\featureNames.acc'];
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');
[indx,tf] = listdlg('PromptString','Select features:',...
    'ListString',featureNameList, 'ListSize',[350,250]);
if isempty(indx)
    return
end

subHeader = featureNameList(indx);
for j = 1:length(subHeader)
    if ~isvarname(subHeader(j))
        subHeader(j) = matlab.lang.makeValidName(subHeader(j));
    end
end

majorHeader = {'Plate'; 'Cell'; 'features_by_frames'};
subVarType = repmat("double",length(indx),1);
majorVarType = {'string'; 'string'; 'cell'};
majorFeaturesTable = table('Size', [length(trajIDs), 3],'VariableTypes', majorVarType, 'VariableNames', majorHeader);

f = waitbar(0,'Please wait...');
for j=1:length(trajIDs)
    tic
    j2Cstructs = A{trajIDs(j), 14};
    imName = A{trajIDs(j), 14}{1, 1}.ImageName;
    splitName = strsplit(imName,{'\', '.'});
    plateName = splitName{1};
    cellName = splitName{3};
    
    majorFeaturesTable.Plate(j) = plateName;
    majorFeaturesTable.Cell(j) = cellName;
    subfeatureTable = table('Size', [40, length(indx)],'VariableTypes', subVarType, 'VariableNames', subHeader);
    
    for i=1:40
        
        if ~isempty(j2Cstructs{i})
            jumpToCell(j2Cstructs{i},false);
            subfeatureTable{i, :} = CommonHandles.SelectedMetaData(CommonHandles.SelectedCell, indx);
        else
            subfeatureTable{i, :} = nan(1, length(indx));
        end
    end
    majorFeaturesTable.features_by_frames{j} = subfeatureTable;
    toc
    waitbar(j/length(trajIDs),f,'Loading your data');
end
close(f)