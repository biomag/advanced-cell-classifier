%This scripts creates csv files from the same cell-instances, i.e. matrix
%of the whole trajectory. Each row of the matrix refers to one time-point,
%while the columns are the different dimensions (multi-dimensional
%measurements).

%% Input vars
baseDir = 'W:\beleon\ACC_Project\MitoCheck';

srcDir = fullfile(baseDir,'mitoMerged_3Chan');
trackFile = fullfile(baseDir,'mitoMerged_3Chan\20200331_2138_track.csv');
tgtDir = fullfile(baseDir,'mitoMerged_3Chan');
analDir = 'anal2v4_ez3_ez7';
featureFileName = 'featureNames.acc';

%% Calc matrices
if ~isfolder(tgtDir), mkdir(tgtDir); end

trackTable = readtable(trackFile);

uniTracks = unique(trackTable.TrackID);

sequenceMatrix = cell(length(uniTracks),1);

S = 2; % number of extra sets


for i=1:length(uniTracks)
    tic
    currTrackID = uniTracks(i);
    relevantTable = trackTable(trackTable.TrackID == i,:);
    sequenceLength = size(relevantTable,1);
    sequenceMatrix{i} = cell(sequenceLength,1);
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(srcDir,relevantTable.Plate{j},'anal2v4',[imgNameExEx '.txt']);
        dataM = load(fileName);
        sequenceMatrix{i}{j} = dataM(relevantTable.ObjectID(j),:);
    end
    %Save cell to local variable here
    seqMat = cell2mat(sequenceMatrix{i});    
    nofFeatures = size(seqMat,2); %NOTE: Location as well!
    
    %Add the derivatives (only for the non-index features)
    extSeqMat = zeros(sequenceLength, nofFeatures + (nofFeatures-2)*S);
    extSeqMat(:,1:nofFeatures) = seqMat;
    
    %%
      jj = 1;
    %Do all central 3-points t(+/-1) padded 1st order derivatives

    for j=3:nofFeatures
        for k=2:sequenceLength-1
            extSeqMat(k,nofFeatures+jj) = (extSeqMat(k+1,j) - extSeqMat(k-1,j))/2;
        end

        %Handle case for the extremes
        % start of traj
%         extSeqMat(1,nofFeatures+jj) = (-3/2)*extSeqMat(1,j) + 2*extSeqMat(2,j) - extSeqMat(3,j)/2;
        extSeqMat(1,nofFeatures+jj) = (-1*extSeqMat(1,j) + extSeqMat(2,j))/2;
        % end of traj
%         extSeqMat(sequenceLength,nofFeatures+jj) = (3/2)*extSeqMat(sequenceLength,j) - 2*extSeqMat(sequenceLength-1,j) + extSeqMat(sequenceLength-2,j)/2;
        extSeqMat(sequenceLength,nofFeatures+jj) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-1,j))/2;
        jj = jj + 1;
    end
    
    
    %Do all central 5-points 1st order derivatives
    
%     for j=3:nofFeatures
%         for k=3:sequenceLength-2
%             extSeqMat(k,nofFeatures+jj) = extSeqMat(k-2,j)/12 - extSeqMat(k-1,j)*8/12 + extSeqMat(k+1,j)*8/12 - extSeqMat(k+2,j)/12;
%         end
% 
%         %Handle case for the extremes
%         % start #t(1) of traj
%         extSeqMat(1,nofFeatures+jj) = (-25*extSeqMat(1,j) + 48*extSeqMat(1+1,j) - 36*extSeqMat(1+2,j) + 16*extSeqMat(1+3,j) - 3*extSeqMat(1+4,j))/12;
%         % start #t(2) of traj
%         extSeqMat(2,nofFeatures+jj) = (-3*extSeqMat(2-1,j) - 10*extSeqMat(1+0,j) + 18*extSeqMat(2+1,j) - 6*extSeqMat(2+2,j) + 1*extSeqMat(2+3,j))/12;
%         
%         % end #t(n-1) of traj
%         extSeqMat(sequenceLength-1,nofFeatures+jj) = (-1*extSeqMat(sequenceLength-4,j) + 6*extSeqMat(sequenceLength-3,j) - 18*extSeqMat(sequenceLength-2,j) + 10*extSeqMat(sequenceLength-1,j) + 3*extSeqMat(sequenceLength,j))/12;
%         % end #t(n)of traj
%         extSeqMat(sequenceLength,nofFeatures+jj) = (25*extSeqMat(sequenceLength,j) - 48*extSeqMat(sequenceLength-1,j) + 36*extSeqMat(sequenceLength-2,j) - 16*extSeqMat(sequenceLength-3,j) + 3*extSeqMat(sequenceLength-4,j))/12;
%         jj = jj + 1;
%     end
    %%
    
%     %Do all central 3-points 2nd order derivatives
%     for j=3:nofFeatures
%         for k=2:sequenceLength-1
%             extSeqMat(k,nofFeatures+jj) = extSeqMat(k-1,j)-2*extSeqMat(k,j)+extSeqMat(k+1,j);
%         end
% 
%         %Handle case for the extremes
%         % start of traj
%         extSeqMat(1,nofFeatures+jj) = extSeqMat(1,j)-2*extSeqMat(2,j)+extSeqMat(3,j);
%         % end of traj
%         extSeqMat(sequenceLength,nofFeatures+jj) = extSeqMat(sequenceLength-2,j)-2*extSeqMat(sequenceLength-1,j)+extSeqMat(sequenceLength,j);
%         jj = jj + 1;
%     end

%Do all central 3-points t(+/-3) padded 1st order derivatives in the middle
    for j=3:nofFeatures
        for k=4:sequenceLength-3
            extSeqMat(k,nofFeatures+jj) = (extSeqMat(k+3,j) - extSeqMat(k-3,j))/2;
        end
    %Handle case for the extremes
        extSeqMat(1,nofFeatures+jj) = (extSeqMat(4,j) - extSeqMat(1,j))/2;
        extSeqMat(2,nofFeatures+jj) = (extSeqMat(5,j) - extSeqMat(1,j))/2;
        extSeqMat(3,nofFeatures+jj) = (extSeqMat(6,j) - extSeqMat(1,j))/2;
        extSeqMat(sequenceLength-2,nofFeatures+jj) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-5,j))/2;
        extSeqMat(sequenceLength-1,nofFeatures+jj) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-4,j))/2;
        extSeqMat(sequenceLength,nofFeatures+jj) = (extSeqMat(sequenceLength,j) - extSeqMat(sequenceLength-3,j))/2;
        jj = jj + 1;
    end
    
    %%
    sequenceMatrix{i} = extSeqMat;
    

    %Create feature names as well
    % -> First load them, from the first plate
    featureFile = fullfile(srcDir,relevantTable.Plate{1},'anal2v4',featureFileName);
    T = readtable(featureFile,'FileType','text','Delimiter',',','ReadVariableNames',false);    
    extFeatureNames = cell(nofFeatures+(nofFeatures-2)*S,1);
    extFeatureNames(1:nofFeatures) = T.Var1;
    
    jj = nofFeatures+1;
    
    for j=3:nofFeatures
        extFeatureNames{jj} = ['firstDer_ez3_', extFeatureNames{j}];
        jj = jj + 1;
    end
%     
%     for j=3:nofFeatures
%         extFeatureNames{jj} = ['firstDer_5point_', extFeatureNames{j}];
%         jj = jj + 1;
%     end
%     
%     
%     for j=3:nofFeatures
%         extFeatureNames{jj} = ['secondDer_3points_', extFeatureNames{j}];
%         jj = jj + 1;
%     end

    for j=3:nofFeatures
        extFeatureNames{jj} = ['firstDer_ez7_', extFeatureNames{j}];
        jj = jj + 1;
    end
    
    
    %Write out the files in ACC format
    for j=1:sequenceLength
        [~,imgNameExEx,~] = fileparts(relevantTable.ImageName{j});
        fileName = fullfile(tgtDir,relevantTable.Plate{j}, analDir,[imgNameExEx '.txt']);
        if ~isfolder(fullfile(tgtDir,relevantTable.Plate{j}, analDir)), mkdir(fullfile(tgtDir,relevantTable.Plate{j}, analDir)); end
        csvwrite(fileName,sequenceMatrix{i}(j,:));
        if ~exist(fullfile(tgtDir,relevantTable.Plate{j}, analDir, featureFileName),'file')
%             writecell(extFeatureNames,fullfile(tgtDir,relevantTable.Plate{j},'anal2_derivative_3_corr',featureFileName),'FileType','text');
            extFeaturesNamesTable = table(extFeatureNames);
            writetable(extFeaturesNamesTable,fullfile(tgtDir,relevantTable.Plate{j}, analDir, featureFileName),'FileType','text', 'WriteVariableNames', false);
        end
    end
                           
        toc
    fprintf('%d/%d ready\n',i,length(uniTracks));
end