function [data, data2] = createTrackingTable_doubleTrack(platePath,maxdisp,param,plateName,separators,fieldIDidx,frameIDs)
% AUTHOR:	Abel Szkalisity
% DATE: 	July 08, 2018
% NAME: 	createTrackingTable
% 
% Creates an appropriate tracking table utilizing the external function
% track.m (check code for licensing). It creates the tracking for a
% single plate. The connected fields are identified by the image name
% nomenclature.
%
% INPUT:
%   platePath   Path to the plate's metadata (usually anal2)
%   maxdisp     Parameter of track.m the maximum displacement allowed
%               between consecutive frames for a single cell
%   param       Further parameters for track.m. Structure with the
%               following fields: mem good dim quiet. See track.m for doc
%   plateName   The name of the current plate
%   separators  A cellarray of the separators used to split the file names
%               (can be empty, then each image is unique)
%   fieldIDidx  An index list which refers to those fields (separated by
%               separators) which form the unique field identifier. 1
%               refers to one single joint field per plate
%   frameIDs    
%   
%
% OUTPUT:
%   The variable "global CommonHandles" become updated to allow the usage
%   of the current version of ACC with old saved projects.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

d=dir(fullfile(platePath,'*.txt'));

fileNames = {d.name};

if param.align == 1
    if isfield(CommonHandles,'Align')
        answer = questdlg(sprintf(['Alignments are already loaded.\n',...
            'Would you like to use it?\n',...
            '(Calculation can take some time.)']),...
            'Alignment','Use loaded','Calculate','Load new', 'Use loaded');
    else
        answer = questdlg(sprintf(['Please provide alignment info!\n',...
            '(Calculation can take some time.)']),...
            'Alignment','Calculate','Load new', 'Calculate');
    end
end

%convert fieldIDidx to fieldID
if fieldIDidx == 1
    fieldIDidx = [];
else
    fieldIDidx = fieldIDidx - 1;
end

allFields = cell(length(fileNames),length(separators)+1);
keyFields = cell(length(fileNames),length(fieldIDidx));
for i=1:length(fileNames)
    [~,exEx,~] = fileparts(fileNames{i});
    if isempty(separators)
        allFields(i,:) = {exEx};
    else
        allFields(i,:) = strsplit(exEx,separators);
    end
    keyFields(i,:) = allFields(i,fieldIDidx);
end
if ~isempty(fieldIDidx)
    concatCommand = 'strcat(';
    for i=1:size(keyFields,2)-1
        concatCommand = [concatCommand 'keyFields(:,' num2str(i) '),']; %#ok<AGROW> not much
    end
    if isempty(i), i = 0; end
    concatCommand = [concatCommand 'keyFields(:,' num2str(i+1) '));'];
    [jointFieldIDs] = eval(concatCommand);

    [~,uniqueFieldIdx] = unique(jointFieldIDs);
    mergedC = cell(length(uniqueFieldIdx), 1);
    cres = cell(length(uniqueFieldIdx), 1);
    cres2 = cell(length(uniqueFieldIdx), 1);
    imNameResult = cell(length(uniqueFieldIdx), 1);
else    
    mergedC = cell(1, 1);
    cres = cell(1, 1);
    cres2 = cell(1, 1);
    imNameResult = cell(1, 1);
    uniqueFieldIdx = 1;
end

imExt = CommonHandles.ImageExtension;
for i = 1:length(uniqueFieldIdx)
    rowBool = true(length(fileNames),1);
    for ii = 1:size(keyFields,2)
        rowBool = rowBool & strcmp(allFields(:,fieldIDidx(ii)),keyFields(uniqueFieldIdx(i),ii));
    end
    currRowIds = find(rowBool);
    currFrameIDs = frameIDs(currRowIds);
    C = cell(length(currFrameIDs), 1);
    %imageName = [imageName; currFieldIDs];
    [~,incIdx] = sort(currFrameIDs);
    if param.align == 1
        if strcmp(answer,'Calculate')
            inFile = cell(length(currRowIds),1);
            splitPath = strsplit(platePath,CommonHandles.MetaDataFolder);
            imPlatePath = [splitPath{1},CommonHandles.OriginalImageFolder];
            for k = 1:length(currRowIds)
                splitImName = strsplit(fullfile(imPlatePath,fileNames{currRowIds(incIdx(k))}),'.txt');
                inFile{k,1} = [splitImName{1},imExt];
            end
            startIm = 1;
            stopIm = length(currRowIds);
            ch = 2;
            writeOutput = false;
            transList = stackAlignerDRP(inFile, startIm, stopIm, ch, writeOutput);
            fieldID = keyFields{uniqueFieldIdx(i)};
            CommonHandles.Align{i,1} = fieldID;
            CommonHandles.Align{i,2} = transList;
            
            % save alignemtn info
%             csvwrite(fullfile(fieldID),transList);
            
        end
    end
    for j=1:length(currRowIds)
        
        temp = load(fullfile(platePath,fileNames{currRowIds(incIdx(j))}));
        nofCells = length(temp(:,1));
        inputmap = zeros(nofCells, 3);
        if param.align == 1
            if j == 1
                inputmap(:,1) = temp(:,1);          %x coordinates
                inputmap(:,2) = temp(:,2);          %y coordinates
            else
                inputmap(:,1) = temp(:,1) + CommonHandles.Align{i,2}(j-1,2);          %x coordinates
                inputmap(:,2) = temp(:,2) + CommonHandles.Align{i,2}(j-1,1);          %y coordinates
            end
        else
            inputmap(:,1) = temp(:,1);          %x coordinates
            inputmap(:,2) = temp(:,2);          %y coordinates
        end
        inputmap(:,3) = 1:size(temp,1);     %Cell ID (Object ID)
        inputmap(:,4) = currFrameIDs(incIdx(j));    %Frame ID (must be the last)
        C{j} = inputmap;
        
    end
    
    % C - jx1 cell array contains jx10 matraces, contains x,y coordinates
    % and the frameID
    
     mergedC{i} = cell2mat(C);
     
     % R - output from tracks function, it is a double type matrix, contains x,y coordinates, frameID, TrackID, FieldID. 
         
     trackResult = track(mergedC{i},maxdisp,param);
     for qq = 1:length(currRowIds) % 76
         points{qq,1} = mergedC{i}((mergedC{i}(:,4)==qq),1:2);
     end
     [ tracks, ~, ~ ] = simpletracker(points,'MaxLinkingDistance',maxdisp,'MaxGapClosing',param.mem);
     
     for tID = 1:length(tracks) % iteration through trackID-s
         frameIdx = find(~isnan(tracks{tID})); % Get idx of frames are being part of the current sequence
         objIdx = tracks{tID}(frameIdx,1); % Get Object IDs of from each frames.
         if numel(frameIdx) > 10 % Filtering trajectories by length
             for f = 1:length(frameIdx) % Iterating through frames
                 points{frameIdx(f),1}(objIdx(f),3) = objIdx(f);
                 points{frameIdx(f),1}(objIdx(f),4) = frameIdx(f);
                 points{frameIdx(f),1}(objIdx(f),5) = tID;
             end
         end
     end
     

     imNameResult{i} = cell(size(trackResult,1),1);
     for j=1:size(trackResult,1)
         txtImgName = fileNames{currRowIds(find(trackResult(j,4) == currFrameIDs,1))};
         [~,exex,~] = fileparts(txtImgName);
         imNameResult{i}{j} = [exex CommonHandles.ImageExtension];
     end
     
     imNameResult2{i} = cell(size(points,1),1);
     for j=1:size(points,1)
         txtImgName = fileNames{currRowIds(find(points(j,4) == currFrameIDs,1))};
         [~,exex,~] = fileparts(txtImgName);
         imNameResult2{i}{j} = [exex CommonHandles.ImageExtension];
     end
     

     pointsArrey = cell2mat(points);
     [~, sortedIdx] = sort(pointsArrey(:,5));
     sortedPoints = pointsArrey(sortedIdx,:);
     mergedPoints = sortedPoints(sortedPoints(:,5)~=0,:);
     
     trackResult = [trackResult repmat(uniqueFieldIdx(i),size(trackResult,1),1)]; %#ok<AGROW> no, it is actually reset
     mergedPoints = [mergedPoints repmat(uniqueFieldIdx(i),size(mergedPoints,1),1)]; %#ok<AGROW> no, it is actually reset
     cres{i} = trackResult;
     cres2{i} = mergedPoints;
     if i>1 %achieve different trackIDs
         prevMaxTrackID = max(cres{i-1}(:,5)); 
         cres{i}(:,5) = cres{i}(:,5) + prevMaxTrackID;
         
         prevMaxTrackID2 = max(cres2{i-1}(:,5)); 
         cres2{i}(:,5) = cres2{i}(:,5) + prevMaxTrackID2;
     end
end
%% This part creating the .csv output
res = cell2mat(cres);
res2 = cell2mat(cres2);

headers = {'Plate','Frame','Field','ObjectID','TrackID', ...
              'tracking__center_x','tracking__center_y', ...
              'ImageName'};
          
data = table(repmat({plateName},length(res),1), ...
              res(:,4),... %Frame
              res(:,6),... %Field, added as extra
              res(:,3),... %ObjectID
              res(:,5), ... %TrackID
              round(res(:,1)),...
              round(res(:,2)),...
              vertcat(imNameResult{:}),...
              'VariableNames', headers);
          
data2 = table(repmat({plateName},length(res2),1), ...
              res2(:,4),... %Frame
              res2(:,6),... %Field, added as extra
              res2(:,3),... %ObjectID
              res2(:,5), ... %TrackID
              round(res2(:,1)),...
              round(res2(:,2)),...
              vertcat(imNameResult2{:}),...
              'VariableNames', headers);
        
              
end

