function dispStatus(newText, handles)
% AUTHOR:   Attila Beleon
% DATE:     October 11, 2019
% NAME:     dispStatus
%
% This function displays the status of the GUI, and the last several
% actions.
%  
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.



pTPC = get(handles.figure1,'UserData');

if sum(~cellfun('isempty', pTPC.dispT)) == 5
    temp = pTPC.dispT(2:5);
    pTPC.dispT(1:4) = temp;
    pTPC.dispT{5} = newText;
else
    pTPC.dispT{sum(~cellfun('isempty', pTPC.dispT))+1} = newText;
end

handles.dispWindow.String = pTPC.dispT;