function data = createTrackingTable(platePath,maxdisp,param,plateName,separators,fieldIDidx,frameIDs,alignCalculation,ch2align)
% AUTHOR:	Abel Szkalisity
% DATE: 	July 08, 2018
% NAME: 	createTrackingTable
%
% Creates an appropriate tracking table utilizing the external function
% track.m (check code for licensing). It creates the tracking for a
% single plate. The connected fields are identified by the image name
% nomenclature.
%
% INPUT:
%   platePath   Path to the plate's metadata (usually anal2)
%   maxdisp     Parameter of track.m the maximum displacement allowed
%               between consecutive frames for a single cell
%   param       Further parameters for track.m. Structure with the
%               following fields: mem good dim quiet. See track.m for doc
%   plateName   The name of the current plate
%   separators  A cellarray of the separators used to split the file names
%               (can be empty, then each image is unique)
%   fieldIDidx  An index list which refers to those fields (separated by
%               separators) which form the unique field identifier. 1
%               refers to one single joint field per plate
%   frameIDs
%
%
% OUTPUT:
%   The variable "global CommonHandles" become updated to allow the usage
%   of the current version of ACC with old saved projects.
%
%
% UPDATE: Attila Beleon (16, June)
%   New features:
%       - alignment
%       - selectable tracking method
%       - SimpleTrack (Jean-Yves) added
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

d=dir(fullfile(platePath,'*.txt'));

fileNames = {d.name};

% if param.align == 1
%     if isfield(CommonHandles,'Align')
%         alignCalculation = questdlg(sprintf(['Alignments are already loaded.\n',...
%             'Would you like to use it?\n',...
%             '(Calculation can take some time.)']),...
%             'Alignment','Use loaded','Calculate','Load new', 'Use loaded');
%     else
%         alignCalculation = questdlg(sprintf(['Please provide alignment info!\n',...
%             '(Calculation can take some time.)']),...
%             'Alignment','Calculate','Load new', 'Calculate');
%     end
%     if strcmp(alignCalculation,'Calculate')
%         list = {'Red', 'Green', 'Blue'};
%         [indx,~] = listdlg('PromptString',{'Select a channel to perform alignment on it!',...
%                             'Only one channel can be selected at a time.',''},...
%                             'SelectionMode','single','ListString',list);
%         ch2align = indx;
%         writeOutput = false;
%     end
% end

%convert fieldIDidx to fieldID
if fieldIDidx == 1
    fieldIDidx = [];
else
    fieldIDidx = fieldIDidx - 1;
end
numOfNameParts = length(separators)+1;
allFields = cell(length(fileNames),numOfNameParts);
keyFields = cell(length(fileNames),length(fieldIDidx));
frameID_idx = setdiff(1:numOfNameParts,fieldIDidx);
for i=1:length(fileNames)
    [~,exEx,~] = fileparts(fileNames{i});
    if isempty(separators)
        allFields(i,:) = {exEx};
    else
        allFields(i,:) = strsplit(exEx,separators);
    end
    keyFields(i,:) = allFields(i,fieldIDidx);
end
framesInSequence = str2double(unique(allFields(:,2)));
if ~isempty(fieldIDidx)
    concatCommand = 'strcat(';
    for i=1:size(keyFields,2)-1
        concatCommand = [concatCommand 'keyFields(:,' num2str(i) '),']; %#ok<AGROW> not much
    end
    if isempty(i), i = 0; end
    concatCommand = [concatCommand 'keyFields(:,' num2str(i+1) '));'];
    [jointFieldIDs] = eval(concatCommand);
    
    [~,uniqueFieldIdx] = unique(jointFieldIDs);
    mergedAlignedC = cell(length(uniqueFieldIdx), 1);
    mergedOrigC = cell(length(uniqueFieldIdx), 1);
    cres = cell(length(uniqueFieldIdx), 1);
    imNameResult = cell(length(uniqueFieldIdx), 1);
else
    mergedAlignedC = cell(1, 1);
    mergedOrigC = cell(1, 1);
    cres = cell(1, 1);
    imNameResult = cell(1, 1);
    uniqueFieldIdx = 1;
end

imExt = CommonHandles.ImageExtension;
plateID = find(strcmp(CommonHandles.PlatesNames,plateName));
emptyRow = 0;
for i = 1:length(uniqueFieldIdx)
    rowBool = true(length(fileNames),1);
    for ii = 1:size(keyFields,2)
        rowBool = rowBool & strcmp(allFields(:,fieldIDidx(ii)),keyFields(uniqueFieldIdx(i),ii));
    end
    currRowIds = find(rowBool); % Indexes of file names which make a sequence
    currFrameIDs = frameIDs(currRowIds); % Frame IDs of the sequence (not necessarily sorted)
    if numel(currFrameIDs) ~= numel(framesInSequence)
        errorFieldName = keyFields{uniqueFieldIdx(i),1};
        if size(keyFields,2) > 1
            for ee = 2:size(keyFields,2)
                errorFieldName = [errorFieldName, 'x', keyFields{uniqueFieldIdx(i),ee}];
            end
        end
        error(sprintf(['File system inconsistency.\nCheck files in plate named:\n', plateName,...
            '\nField: ' errorFieldName, ' has less frame than the maximum in the data.']))
    end
    alignedContainer = cell(length(currFrameIDs), 1); % Container for object coordiantes
    origContainer = cell(length(currFrameIDs), 1);
    %imageName = [imageName; currFieldIDs];
    [~,incIdx] = sort(currFrameIDs); % Sorted indexes
    if param.align == 1
        pattern = separators{1} + digitsPattern(1,2) + separators{2};
        newPattern = [separators{1},'$',separators{2}];
        [~,exEx,~] = fileparts(fileNames{uniqueFieldIdx(i)});
        fieldID = replaceBetween(exEx,separators{1},separators{2},'$');
        if strcmp(alignCalculation,'Calculate')
            inFile = cell(length(currRowIds),1);
            splitPath = strsplit(platePath,CommonHandles.MetaDataFolder);
            imPlatePath = [splitPath{1},CommonHandles.OriginalImageFolder];
            for k = 1:length(currRowIds)
                splitImName = strsplit(fullfile(imPlatePath,fileNames{currRowIds(incIdx(k))}),'.txt');
                inFile{k,1} = [splitImName{1},imExt];
            end
            startIm = 1;
            stopIm = length(currRowIds);
            writeOutput = 0;
            transList = stackAlignerDRP(inFile, startIm, stopIm, ch2align, writeOutput);
            if ~exist(fullfile(CommonHandles.AlignPath,plateName),'dir')
                mkdir(fullfile(CommonHandles.AlignPath,plateName))
            end
            csvwrite(fullfile(CommonHandles.AlignPath,plateName,[fieldID,'.csv']),transList)
        else
            isOK = 1;
            while isOK
                try
                    transList = csvread(fullfile(CommonHandles.AlignPath,plateName,[fieldID,'.csv']));
                catch
                    waitfor(errordlg(['Wrong path or corrupted aligment path','Missing: ',plateName]))
                    newalign = questdlg('Would you like to calculate alignment for missing plate?','Missing plate',...
                        'Calculate', 'Continue without aligment', 'End process','Calculate');
                    switch newalign
                        case 'Calculate'
                            inFile = cell(length(currRowIds),1);
                            splitPath = strsplit(platePath,CommonHandles.MetaDataFolder);
                            imPlatePath = [splitPath{1},CommonHandles.OriginalImageFolder];
                            for k = 1:length(currRowIds)
                                splitImName = strsplit(fullfile(imPlatePath,fileNames{currRowIds(incIdx(k))}),'.txt');
                                inFile{k,1} = [splitImName{1},imExt];
                            end
                            startIm = 1;
                            stopIm = length(currRowIds);
                            writeOutput = 0;
                            transList = stackAlignerDRP(inFile, startIm, stopIm, ch2align, writeOutput);
                            if ~exist(fullfile(CommonHandles.AlignPath,plateName),'dir')
                                mkdir(fullfile(CommonHandles.AlignPath,plateName))
                            end
                            csvwrite(fullfile(CommonHandles.AlignPath,plateName,[fieldID,'.csv']),transList)
                        case 'Continue without aligment'
                            transList = nan;
                        case 'End process'
                            return
                        otherwise
                            return
                    end
                end
            end
        end
    end
    
    for j=1:length(currRowIds)
        idx = currRowIds(incIdx(j));
        temp = load(fullfile(platePath,fileNames{idx}));
        nofCells = length(temp(:,1));
        alignedCoor = zeros(nofCells, 4);
        origCoor = zeros(nofCells, 4);
        if param.align == 1 && (isempty(transList) || ~isnan(transList(1,1)))
            if j == 1
                alignedCoor(:,1) = temp(:,1);          %x coordinates
                alignedCoor(:,2) = temp(:,2);          %y coordinates
            else
                alignedCoor(:,1) = temp(:,1) + transList(j,2);          %x coordinates
                alignedCoor(:,2) = temp(:,2) + transList(j,1);          %y coordinates
                origCoor(:,1) = temp(:,1);
                origCoor(:,2) = temp(:,2);
            end
        else
            alignedCoor(:,1) = temp(:,1);          %x coordinates
            alignedCoor(:,2) = temp(:,2);          %y coordinates
        end
        alignedCoor(:,3) = 1:size(temp,1);     %Cell ID (Object ID)
        alignedCoor(:,4) = currFrameIDs(incIdx(j));    %Frame ID (must be the last)
        origCoor(:,3) = 1:size(temp,1);
        origCoor(:,4) = currFrameIDs(incIdx(j));
        alignedContainer{j} = alignedCoor;
        origContainer{j} = origCoor;
    end
    
    % C - jx1 cell array contains jx10 matraces, contains x,y coordinates
    % and the frameID
    
    mergedAlignedC{i} = cell2mat(alignedContainer);
    mergedOrigC{i} = cell2mat(origContainer);
    % R - output from tracks function, it is a double type matrix, contains x,y coordinates, frameID, TrackID, FieldID.
    if strcmp(param.trackingMethod,'track.pro')
        trackResult = track(mergedAlignedC{i},maxdisp,param);
    elseif strcmp(param.trackingMethod,'simpleTrack')
        alignedPoints = cell(length(currRowIds),1);
        origPoints = cell(length(currRowIds),1);
        for qq = 1:length(currRowIds) % num of frames
            alignedPoints{qq,1} = mergedAlignedC{i}((mergedAlignedC{i}(:,4)==currFrameIDs(incIdx(qq))),1:2);
            origPoints{qq,1} = mergedOrigC{i}((mergedOrigC{i}(:,4)==currFrameIDs(incIdx(qq))),1:2);
        end
        [ tracks, ~, ~ ] = simpletracker(alignedPoints,'MaxLinkingDistance',maxdisp,'MaxGapClosing',param.mem);
        for tID = 1:length(tracks) % iteration through trackID-s
            frameIdx = find(~isnan(tracks{tID})); % Get idx of frames are being part of the current sequence
            objIdx = tracks{tID}(frameIdx,1); % Get Object IDs of from each frames.
            if numel(frameIdx) > param.good % Filtering trajectories by length
                for f = 1:length(frameIdx) % Iterating through frames
                    origPoints{frameIdx(f),1}(objIdx(f),3) = objIdx(f);
                    origPoints{frameIdx(f),1}(objIdx(f),4) = frameIdx(f);
                    origPoints{frameIdx(f),1}(objIdx(f),5) = tID;
                end
            end
        end
        zz = length(origPoints);
        while zz > 0
            if size(origPoints{zz}, 2) < 5
                origPoints(zz) = [];
            end
            zz = zz-1;
        end
        pointsArrey = cell2mat(origPoints);
        if isempty(pointsArrey)
            emptyRow = emptyRow + 1;
            continue
        else
            [~, sortedIdx] = sort(pointsArrey(:,5));
            sortedPoints = pointsArrey(sortedIdx,:);
            trackResult = sortedPoints(sortedPoints(:,5)~=0,:);
        end
    end
    
    imNameResult{i} = cell(size(trackResult,1),1);
    for j=1:size(trackResult,1)
        txtImgName = fileNames{currRowIds(incIdx(trackResult(j,4)))};
        [~,exex,~] = fileparts(txtImgName);
        imNameResult{i}{j} = [exex CommonHandles.ImageExtension];
    end
    
    trackResult = [trackResult repmat(uniqueFieldIdx(i),size(trackResult,1),1)]; %#ok<AGROW> no, it is actually reset
    cres{i} = trackResult;
    if i>1 %achieve different trackIDs
        prevMaxTrackID = max(cres{i-1-emptyRow}(:,5));
        cres{i}(:,5) = cres{i}(:,5) + prevMaxTrackID;
    end
    emptyRow = 0;
    trackResult = [];
end
%% This part creating the .csv output
res = cell2mat(cres);

headers = {'Plate','Frame','Field','ObjectID','TrackID', ...
    'tracking__center_x','tracking__center_y', ...
    'ImageName'};

data = table(repmat({plateName},length(res),1), ...
    res(:,4),... %Frame
    res(:,6),... %Field, added as extra
    res(:,3),... %ObjectID
    res(:,5), ... %TrackID
    round(res(:,1)),...
    round(res(:,2)),...
    vertcat(imNameResult{:}),...
    'VariableNames', headers);
end

