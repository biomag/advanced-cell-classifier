function varargout = TrackingWizardGUI(varargin)
% TrackingWizardGUI MATLAB code for TrackingWizardGUI.fig
%      TrackingWizardGUI, by itself, creates a new TrackingWizardGUI or raises the existing
%      singleton*.
%
%      H = TrackingWizardGUI returns the handle to a new TrackingWizardGUI or the handle to
%      the existing singleton*.
%
%      TrackingWizardGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TrackingWizardGUI.M with the given input arguments.
%
%      TrackingWizardGUI('Property','Value',...) creates a new TrackingWizardGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TrackingWizardGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TrackingWizardGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TrackingWizardGUI

% Last Modified by GUIDE v2.5 08-Apr-2019 14:48:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @TrackingWizardGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @TrackingWizardGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TrackingWizardGUI is made visible.
function TrackingWizardGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TrackingWizardGUI (see VARARGIN)

% Choose default command line output for TrackingWizardGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
uData = get(handles.figure1,'UserData');

%Fill out the GUI components 
global CommonHandles;
    
uData.separatorParamcell = {...
    struct('name','Plates for tracking','type','multiple-enum','values',{CommonHandles.PlatesNames}),...
    struct('name','Separators to split the image name (use ; between them)','type','str')
    };
[uData.separatorTextHandles,uData.separatorEditHandles] = generateUIControls(uData.separatorParamcell,handles.PanelImgNomenclature,20,[10,50]);

uData.trackParamcell = {...
    struct('name','How long a cell can be lost','type','int','default',0, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [0 50])),...
    struct('name','What is the maximum displacement allowed?','type','int','default',10, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [0 10e+6])),...
    struct('name','What is the minimum length of trajectories','type','int','default',10, 'numSpecs', struct('integer', 1, 'scalar', 1, 'limits', [1 1024]))...
    struct('name','Do align','type','checkbox','default',0)...
    struct('name','Tracking method','type','enum','default',1,'values',{{'simpleTrack', 'track.pro'}})...
    };
[uData.trackTextHandles,uData.trackEditHandles] = generateUIControls(uData.trackParamcell,handles.Uipanel_TrackParameters,20,[10,50]);

% init uData
uData.page = 1;
uData.maxPage = 4;

set(handles.figure1,'UserData',uData);
placeItemsOnGUI(handles);
uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = TrackingWizardGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
if isfield(handles, 'output')
    varargout{1} = handles.output;
    figure1_CloseRequestFcn(handles.figure1);
else
    varargout{1} = [];
end


% --- Executes on button press in Button_CalcTrajectories.
function Button_CalcTrajectories_Callback(hObject, eventdata, handles)
% hObject    handle to Button_CalcTrajectories (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
global CommonHandles;

handles.output = [];
try
    trackValues = fetchUIControlValues(uData.trackParamcell,uData.trackEditHandles);
catch e
    if strcmp(e.identifier,'Settings_GUI:errorConstraintFault')
        return
    else
        rethrow(e)
    end
end

selectedPlates = uData.selectedPlates;
param.quiet = 1;
param.dim = 2;
param.mem = trackValues{1};
param.good = trackValues{3};
param.align = trackValues{4};
param.trackingMethod = trackValues{5};

infoWin = msgbox(sprintf('Trajecories are being calculated... (Plate %d/%d)',0,length(selectedPlates)));
infoWin.Children(1).Visible = 'off'; pause(0.1);


% Fixed path and file name -------------------------------------------
if ~exist([CommonHandles.DirName, filesep, 'Trackings'], 'dir')
    mkdir(CommonHandles.DirName,'Trackings')
end

timePoint = timeString(5);
trackPath = fullfile(CommonHandles.DirName, 'Trackings', timePoint);
mkdir(trackPath)
CommonHandles.TrackingPath = trackPath;

if param.align == 1
    alignPath = fullfile(CommonHandles.DirName, 'Alignment', timePoint);
    mkdir(alignPath)
    CommonHandles.AlignPath = alignPath;
    ch2align = 0;
    if isfield(CommonHandles,'AlignPath')
        alignCalculation = questdlg(sprintf(['Alignments path is already set.\n',...
            'Would you like to use it?\n',...
            '(Calculation can take some time.)']),...
            'Alignment','Use loaded','Calculate','Load new', 'Use loaded');
    else
        alignCalculation = questdlg(sprintf(['Please provide alignment info!\n',...
            '(Calculation can take some time.)']),...
            'Alignment','Calculate','Load new', 'Calculate');
    end
    if strcmp(alignCalculation,'Calculate')
        list = {'Red', 'Green', 'Blue'};
        [indx,~] = listdlg('PromptString',{'Select a channel to perform alignment on it!',...
            'Only one channel can be selected at a time.',''},...
            'SelectionMode','single','ListString',list);
        ch2align = indx;
    elseif strcmp(alignCalculation,'Load new')
        alignPath = uigetdir('Please set the path for alignment information');
        if ischar(alignPath)
            CommonHandles.AlignPath = alignPath;
        end
    end
end
prevMaxTrackID = 0;
for i=1:length(selectedPlates)
    if ishandle(infoWin), infoWin.Children(2).Children(1).String = sprintf('Trajecories are being calculated... (Plate %d/%d)',i,length(selectedPlates)); pause(0.1); end
    try
        trackTables = createTrackingTable(...
            fullfile(CommonHandles.DirName,selectedPlates{i},CommonHandles.MetaDataFolder),...
            trackValues{2},...
            param,...
            selectedPlates{i},...
            uData.separators,...
            get(handles.Listbox_UniqueField,'Value'),...
            uData.frameIDs{i},...
            alignCalculation,...
            ch2align);
    catch e
        if strcmp('MATLAB:unassignedOutputs', e.identifier)
            errordlg('We could not find any trajectories with the current settings.', 'No trajectories found')
            if ishandle(infoWin), close(infoWin); end
            return;
        else
            rethrow(e);
        end
    end
    
    trackTables.TrackID = trackTables.TrackID + prevMaxTrackID;
    prevMaxTrackID = max(trackTables.TrackID);
    
    fileName = [timePoint, '_track.csv'];
    if i==1
        writetable(trackTables,fullfile(trackPath,fileName));
    else
        writetable(trackTables,fullfile(trackPath,fileName),'WriteMode','Append','WriteVariableNames',false)
    end
    mkdir(fullfile(trackPath,selectedPlates{i}))
    writetable(trackTables,fullfile(trackPath,selectedPlates{i},[selectedPlates{i},'_',fileName]));
end

if ishandle(infoWin), infoWin.Children(2).Children(1).String = sprintf('Trajecories are being calculated... (Plate %d/%d)',i,length(selectedPlates)); pause(0.1); end

fullTable = vertcat(trackTables{:,1});

if ishandle(infoWin), close(infoWin); end

TrackingParameters.maxLostFrame = trackValues{1};
TrackingParameters.maxDisplacement = trackValues{2};
TrackingParameters.minLength = trackValues{3};
TrackingParameters.alignment = trackValues{4};
TrackingParameters.trackingMethod = trackValues{5};
TrackingParameters.separators = uData.separators;
TrackingParameters.selectedPlates = uData.selectedPlates;
TrackingParameters.frameIDidx = get(handles.Listbox_FrameNumber,'Value') - 1;
TrackingParameters.fieldIDidx = get(handles.Listbox_UniqueField,'Value') - 1;
writestruct(TrackingParameters,fullfile(trackPath, 'TrackingParameters.xml'));

f = fopen(fullfile(trackPath, [timePoint,'TrackingParameters.txt']),'w');
fprintf(f,'How long a cell can be lost: %d \n',trackValues{1});
fprintf(f,'What is the maximum displacement allowed: %d \n',trackValues{2});
fprintf(f,'What is the minimum length of trajectories: %d \n',trackValues{3});
fprintf(f,'Do align: %d \n',trackValues{4});
fprintf(f,'Tracking Mehtod: %s \n',trackValues{5});
fprintf(f,'\n*************************\n\n');
fprintf(f,'The following separators were used:\n');

for i=1:numel(uData.separators)
    fprintf(f,'%s\n',uData.separators{i});
end

fprintf(f,'\n*************************\n\n');
fprintf(f,'Selected Plates:\n');

for i=1:numel(selectedPlates)
    fprintf(f,'%s\n',selectedPlates{i});
end

fclose(f);


% ---------------------------------------------------------------

handles.output = fullTable;

guidata(hObject,handles);
uiresume(handles.figure1);



% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

placeItemsOnGUI(handles);

% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

handles = createGUI(hObject);

guidata(hObject, handles);

%% Figure specific internal functions

function placeItemsOnGUI(handles)
%as this will be a "wizard" like object we need to know on which page we
%are, as the drawing depends on it

%First get the figure size

figSize = get(handles.figure1,'Position');

buttonWidth = 200;
buttonHeight = 30;
margin = 20;
buttonIdx = 1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.PageBackwardButton,'Position',buttonPosition);
buttonIdx = buttonIdx+1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.PageForwardButton,'Position',buttonPosition);
buttonIdx = buttonIdx+1;
buttonPosition = [(buttonIdx-1)*buttonWidth+margin*(buttonIdx) 10 buttonWidth buttonHeight];
set(handles.Button_CalcTrajectories,'Position',buttonPosition);

uData = get(handles.figure1,'UserData');

pageStates = cell(1,uData.maxPage);
for i=1:length(pageStates)
    if uData.page == i
        pageStates{i} = 'On';
    else
        pageStates{i} = 'Off';
    end
end

if uData.page == 1
    set(handles.PageBackwardButton,'Enable','Off');
else
    set(handles.PageBackwardButton,'Enable','On');
end
if uData.page == uData.maxPage
    set(handles.PageForwardButton,'Enable','Off');
else
    set(handles.PageForwardButton,'Enable','On');
end
    
%Page number

pgNum = 1;
set(handles.PanelImgNomenclature,'Visible',pageStates{pgNum});

pgNum = 2;
set(handles.Listbox_UniqueField,'Visible',pageStates{pgNum});
set(handles.listInfoTextField,'Visible',pageStates{pgNum});

pgNum = 3;
set(handles.Listbox_FrameNumber,'Visible',pageStates{pgNum});
set(handles.listInfoTextFrame,'Visible',pageStates{pgNum});

pgNum = 4;
set(handles.Uipanel_TrackParameters,'Visible',pageStates{pgNum});
set(handles.Button_CalcTrajectories,'Enable',pageStates{pgNum});

greatPanelPosition = [margin margin*2 + buttonHeight figSize(3)-2*margin figSize(4) - 3*margin - buttonHeight];
greatPanelPositionWithHeader = [margin margin*2 + buttonHeight figSize(3)-2*margin figSize(4) - 4*margin - 2*buttonHeight];
headerPosition = [margin greatPanelPositionWithHeader(4) + buttonHeight + 3*margin figSize(3)-2*margin buttonHeight];
set(handles.PanelImgNomenclature,'Position',greatPanelPosition);
set(handles.listInfoTextField,'Position',headerPosition);
set(handles.Listbox_UniqueField,'Position',greatPanelPositionWithHeader);
set(handles.listInfoTextFrame,'Position',headerPosition);
set(handles.Listbox_FrameNumber,'Position',greatPanelPositionWithHeader);
set(handles.Uipanel_TrackParameters,'Position',greatPanelPosition);

if isfield(uData,'separatorTextHandles') && isfield(uData,'separatorEditHandles')
    resizeUIControls(uData.separatorTextHandles,uData.separatorEditHandles,greatPanelPosition,20, [10 50]); %magic consts same as below for nice look
end

if isfield(uData,'trackTextHandles') && isfield(uData,'trackEditHandles')
    resizeUIControls(uData.trackTextHandles,uData.trackEditHandles,greatPanelPosition,20, [10 50]); %magic consts same as below for nice look
end

function handles = createGUI(hObject)

handles.PageForwardButton = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','>>',...    
    'Callback',@(hObject,eventdata)TrackingWizardGUI('pageForwardCallback',hObject,eventdata,guidata(hObject)));

handles.PageBackwardButton = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','<<',...    
    'Callback',@(hObject,eventdata)TrackingWizardGUI('pageBackwardCallback',hObject,eventdata,guidata(hObject)));

handles.Button_CalcTrajectories = uicontrol(...
    'Parent',hObject,...
    'Style','pushbutton',...
    'Units','pixels',...
    'String','Calculate trajectories',...    
    'Callback',@(hObject,eventdata)TrackingWizardGUI('Button_CalcTrajectories_Callback',hObject,eventdata,guidata(hObject)));

handles.PanelImgNomenclature = uipanel(...
    'Parent',hObject,...    
    'Title','Image nomenclature',...
    'Units','pixels'....
    );

handles.Listbox_UniqueField = uicontrol(...
    'Parent',hObject,...
    'Style','listbox',...
    'Units','pixels',...
    'String','Something wrong');

handles.Listbox_FrameNumber = uicontrol(...
    'Parent',hObject,...
    'Style','listbox',...
    'Units','pixels',...
    'String','SeparatedFields come here');

handles.Uipanel_TrackParameters = uipanel(...
    'Parent',hObject,...
    'Units','pixels',...
    'Title','Tracking parameters');

handles.listInfoTextField = uicontrol(...
    'Style','text',...
    'Parent',hObject,...
    'Units','pixels',...
    'String','Please select the parts of the filename defined by the separators, that provide uniqueID for each field of view!');

handles.listInfoTextFrame = uicontrol(...
    'Style','text',...
    'Parent',hObject,...
    'Units','pixels',...
    'String','Please select the part of the filename that define the frame! (must be numeric)');




function pageForwardCallback(~,~,handles)    

    global CommonHandles;
    
    uData = get(handles.figure1,'UserData');    
    uData.page = uData.page + 1;    
    
    try
        %% Handle transition between pages    
        if uData.page == 2 || uData.page == 3
            paramcell = fetchUIControlValues(uData.separatorParamcell,uData.separatorEditHandles,@checkSeparatorFunc);
            uData.selectedPlates = paramcell{1};
            separators = strsplit(paramcell{2},';');
            fieldList = cell(1,length(separators)+2);
            if isempty(paramcell{2})
                fieldList(2:end) = [];
                separators = [];
            else
                fieldList{2} = [num2str(1) '-____' separators{1}];
                for i=3:length(fieldList)
                    fieldList{i} = [num2str(i-1) '-' separators{i-2} '____'];
                end
            end
            uData.separators = separators;
            
            for i=1:length(uData.selectedPlates)
                platePath = fullfile(CommonHandles.DirName, uData.selectedPlates{i}, CommonHandles.MetaDataFolder, '*.txt');
                d = dir(fullfile(platePath));
                if isempty(d)
                    error('This functionality works only with txt input. No txt file was found at %s',platePath);
                end                
                fileNames = {d.name};
                    
                allFields = cell(length(fileNames),length(separators)+1);
                for j=1:length(fileNames)
                    [~,exEx,~] = fileparts(fileNames{j});
                    if ~isempty(separators) && ~(size(allFields, 2) == size(strsplit(exEx,separators), 2))
                        error(['We found a separational problem with file: ''', num2str(j), '''! '...
                            'Please check the separators! ',...
                            'This could happen if one of your separator apperars more than once in the name of the files!'],...
                            'Separation failure');
                    end
                end
            end                        
        end
        
        if uData.page == 2
            fieldList{1} = 'All images are from the same field';
            set(handles.Listbox_UniqueField,'String',fieldList);
            set(handles.Listbox_UniqueField,'Max',length(fieldList));
        end
        
        if uData.page == 3
            fieldList{1} = 'Number frames consecutively';
            set(handles.Listbox_FrameNumber,'String',fieldList);
            fieldValues = get(handles.Listbox_UniqueField,'Value');
            if ismember(1,fieldValues) && length(fieldValues)>1
                error('It is not allowed to select both unique images AND file name based fields');                
            end
        end

        if uData.page == 4
            frameValue = get(handles.Listbox_FrameNumber,'Value'); 
            if frameValue>1
                frameValue = frameValue - 1; %corrigate for the first input
                for i=1:length(uData.selectedPlates)
                    d = dir(fullfile(CommonHandles.DirName,uData.selectedPlates{i},CommonHandles.MetaDataFolder,'*.txt'));
                    if isempty(d)
                        error('This functionality works only with txt input. No txt file was found at %s',platePath);
                    end
                    for j=1:numel(d)
                        [~,onlyFileName,~] = fileparts(d(j).name);
                        splitFileName = strsplit(onlyFileName,uData.separators);
                        if isnan(str2double(splitFileName{frameValue}))
                            error('The selected frame column is not numeric for all images');
                        else
                            uData.frameIDs{i}(j) = str2double(splitFileName{frameValue});
                        end
                    end
                end
            else                
                for i=1:numel(uData.selectedPlates)
                    d = dir(fullfile(CommonHandles.DirName,uData.selectedPlates{i},CommonHandles.MetaDataFolder,'*.txt'));
                    if isempty(d)                    
                        error('This functionality works only with txt input. No txt file was found at %s',platePath);
                    end
                    uData.frameIDs{i} = 1:numel(d);
                end
            end
        end
    catch e
       uData.page = uData.page - 1;
       errordlg(e.message);
    end

    set(handles.figure1,'UserData',uData);
    placeItemsOnGUI(handles);        
        
    
function pageBackwardCallback(~,~,handles)    
    uData = get(handles.figure1,'UserData');  
    uData.page = uData.page - 1;
    set(handles.figure1,'UserData',uData);
    placeItemsOnGUI(handles);

function [ok,msg] = checkSeparatorFunc(paramcell)
    ok = ~isempty(paramcell{1});
    msg = 'Please select at least one plate';
    