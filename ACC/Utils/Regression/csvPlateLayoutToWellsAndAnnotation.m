function [ wellIDs, wellAnnotation ] = csvPlateLayoutToWellsAndAnnotation( fileName )
% AUTHOR:   Abel Szkalisity
% DATE:     May 18, 2017
% NAME:     csvPlateLayoutToWellsAndAnnotation
%
% Reads in a csv file (can be separated by ',' or ';') which has a header
% and returns the first 2 columns without the headers as cellarrays.
%
% INPUT:
%   fileName        The name of the file with the full path
%
% OUTPUT:
%   wellIDs         A cellarray of the first column that is supposed to be
%                   the column of the wellIDs such as C05
%   wellAnnotation  A cellarray of the second column that is supposed to be
%                   the annotation for the well.
%   
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


    plateLayoutTextJoined = importdata(fileName);
    plateLayoutText = cell(size(plateLayoutTextJoined,1),2);
    for i=1:size(plateLayoutText,1)
        splittedByComma = strsplit(plateLayoutTextJoined{i},{',',';'});
        plateLayoutText{i,1} = splittedByComma{1};
        plateLayoutText{i,2} = splittedByComma{2};
    end
    
    wellIDs = plateLayoutText(2:end,1);
    wellAnnotation = plateLayoutText(2:end,2);

end

