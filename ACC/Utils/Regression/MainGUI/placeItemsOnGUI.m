function placeItemsOnGUI(handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     placeItemsOnGUI
%
% This util function does the layout management for the regression plane.
% It should get the information from the regPlane main plot about
% resizements and then adjust the layout to that. We wish to make greater
% only the regression plane all the others should be maintained.
%
% INPUT:
%   handles         Handles to the graphical objects of the regressionPlane
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

dimensions = get(gcf,'Position');
sizeX = dimensions(3);
sizeY = dimensions(4);

%Big controls (regression plane)
set(handles.axes1,'Units','pixels');
regPlaneAxesX = sizeX-300;
if regPlaneAxesX<=0, regPlaneAxesX = 1; end
regPlaneAxesY = sizeY-170;
if regPlaneAxesY<=0, regPlaneAxesY = 1; end
set(handles.axes1,'Position',[250, 50, regPlaneAxesX, regPlaneAxesY]);
set(handles.axes11,'Units','pixels');
set(handles.axes11,'Position',[250, 50, regPlaneAxesX, regPlaneAxesY]);
set(handles.axes11,'XColor',[1 1 1],'YColor',[1 1 1]);

%TOP controls
set(handles.closePredictionButton,'Position', [650, sizeY-50, 150, 25]);
set(handles.textControlGroupIcon,'Position',[250 sizeY-50, 90 15]);
set(handles.increaseImageSize,'Position',[350 sizeY-50, 75, 25]);
set(handles.resetImageSize,'Position',[450 sizeY-50, 75, 25]);
set(handles.decreaseImageSize,'Position',[550 sizeY-50, 75, 25]);
set(handles.closeColorFrameButton,'Position', [650, sizeY-100, 150, 25]);
set(handles.textControlGroupAxes,'Position',[250 sizeY-100, 90 15]);
set(handles.zoomInButton,'Position',[350 sizeY-100 75 25]);
set(handles.fullViewButton,'Position',[450 sizeY-100 75 25]);
set(handles.zoomOutButton,'Position',[550 sizeY-100 75 25]);

%Left side controls from top to bottom
set(handles.infoTextCellView,'Position',[50,sizeY-35,150,15]);
set(handles.axes4,'Position', [50, sizeY-175, 150, 150]);

set(handles.jumpToCell,'Position',[75, sizeY-200, 102, 18]);

set(handles.infoTextCellNumber,'Position',[50 sizeY-220, 100, 15]);
set(handles.selectedCellText,'Position',[150 sizeY-220, 50, 15]);
set(handles.infoTextSelectedImage,'Position',[50, sizeY-240, 150, 15]);
set(handles.selectedImageText,'Position',[50, sizeY-295, 150, 50]);

set(handles.infoTextNavigationMap,'Position',[50,sizeY-3100,150,15]);
set(handles.axes10,'Position',[50, sizeY-460, 150, 150]);
set(handles.axes10.XAxis,'Visible','off');
set(handles.axes10.YAxis,'Visible','off');
plot(handles.axes10,[0 0 1 1 0],[0 1 1 0 0],'Color',[.99 .99 .99]);

set(handles.posInfo,'Position',[50 sizeY-5100, 150, 15]);
set(handles.infoTextX,'Position',[50 sizeY-540, 20, 20]);
set(handles.editXCoord,'Position',[70 sizeY-540, 50, 25]);
set(handles.infoTextY,'Position',[125 sizeY-540, 25, 20]);
set(handles.editYCoord,'Position',[150 sizeY-540, 50, 25]);

%set(handles.pushbutton1,'Position',[50, sizeY-450, 100, 25]);

set(handles.uipanel5,'Position',[50 sizeY-670 150, 110]);

set(handles.listbox_WellAnnotation,'Position',[50 sizeY-770 150 90]);

uData = get(handles.figure1,'UserData');
if uData.prediction == 1 %prediction mode
    set(handles.closePredictionButton,'String','Close prediction');
    
    set(handles.uipushtool3,'Visible','on'); %delete button
        
    set(handles.toolbarPredictPlatePushtool,'Visible','off');    
    set(handles.toolbarPredictWellPushtool,'Visible','off'); 
    set(handles.toolbarTrainRegression,'Visible','off');
    set(handles.toolbarRegressionPerformance,'Visible','off');
    
    set(handles.closePredictionButton,'Visible','on');
    
    set(handles.toolbarPlotOfPlots,'Visible','off'); %so that you can't start a plotOfPlots mode from the prediction mode
    
    set(handles.toolbarPlotTrajectories,'Visible','on');
    set(handles.listbox_WellAnnotation,'Visible','off');
    
    set(handles.toolbarColorFrame,'Visible','on');
    if CommonHandles.RegressionPlane{idx}.colorFrame.state
        set(handles.closeColorFrameButton,'Visible','on');
    else
        set(handles.closeColorFrameButton,'Visible','off');
    end
elseif uData.prediction == 2 %plotOfPlots mode
    set(handles.closePredictionButton,'String','Close meta visualization');
    
    set(handles.uipushtool3,'Visible','off'); %delete button
    
    set(handles.toolbar_OneDRegression,'Visible','off');
    
    set(handles.toolbarPredictPlatePushtool,'Visible','off');    
    set(handles.toolbarPredictWellPushtool,'Visible','off'); 
    set(handles.toolbarTrainRegression,'Visible','off');
    set(handles.toolbarRegressionPerformance,'Visible','off');
    
    
    set(handles.closePredictionButton,'Visible','on');  
    
    set(handles.toolbarPlotOfPlots,'Visible','off');
    
    set(handles.toolbarPlotTrajectories,'Visible','off');
    set(handles.listbox_WellAnnotation,'Visible','on');
    
    set(handles.toolbarColorFrame,'Visible','off');                
    set(handles.closeColorFrameButton,'Visible','off');    
else %annotating mode
    set(handles.uipushtool3,'Visible','on'); %delete button
    
    set(handles.toolbarPredictPlatePushtool,'Visible','on');
    set(handles.toolbarPredictWellPushtool,'Visible','on');
    set(handles.toolbarTrainRegression,'Visible','on');   
    set(handles.toolbarRegressionPerformance,'Visible','on');
    
    set(handles.closePredictionButton,'Visible','off');
    
    set(handles.toolbarPlotOfPlots,'Visible','on'); %you can only start POP from annotation mode
    
    set(handles.toolbarPlotTrajectories,'Visible','on');
    set(handles.listbox_WellAnnotation,'Visible','off');
    
    set(handles.toolbarColorFrame,'Visible','on');
    if CommonHandles.RegressionPlane{idx}.colorFrame.state
        set(handles.closeColorFrameButton,'Visible','on');
    else
        set(handles.closeColorFrameButton,'Visible','off');
    end
end
