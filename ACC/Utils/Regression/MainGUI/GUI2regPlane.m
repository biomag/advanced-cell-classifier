function [newPos] = GUI2regPlane(newPos)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     GUI2regPlane
%
% This util function helps to convert back the points from the GUI to the
% regression plane.
%
% INPUT:
%   newPos          Position in GUI coordinate system
%
% OUTPUT:
%   newPos          Position in regression plane coordinate system
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global ghandles;
global CommonHandles;
handles = ghandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
YBounds = CommonHandles.RegressionPlane{idx}.YBounds;

regPlaneBoundaries = round(get(handles.axes1,'Position')); 

%Transform back from the displayed coordinate system to the stored one.
distFromXLowerInGUI = newPos(1) - regPlaneBoundaries(1);
distFromYLowerInGUI = newPos(2) - regPlaneBoundaries(2);

newPos(1) = distFromXLowerInGUI*(XBounds(2)-XBounds(1))/regPlaneBoundaries(3) + XBounds(1);
newPos(2) = distFromYLowerInGUI*(YBounds(2)-YBounds(1))/regPlaneBoundaries(4) + YBounds(1);
