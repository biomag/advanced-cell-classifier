function colorOrderForImFrame = calcColorOrder(handles)
% AUTHOR:   Attila Beleon
% DATE:     May 11, 2020
% NAME:     calcColorOrder
%
% This function calculates color for each cell on the Regression Plane,
% for ColorFrame visualization. 
% 
% INPUTS
%   handles
%
% OUTPUTS
%   colorOrderForImFrame  - n-by-3 array that countains 8-bit RGB color codes
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
global CommonHandles;

fID = CommonHandles.RegressionPlane{idx}.colorFrame.featureID; % feature ID
lo = CommonHandles.RegressionPlane{idx}.colorFrame.lo; % low value of interest region
hi = CommonHandles.RegressionPlane{idx}.colorFrame.hi; % hi value of interest region
scaleType = CommonHandles.RegressionPlane{idx}.colorFrame.scaleType; % linear or log


if uData.prediction == 1 % prediction or PlotOfPlots mode
    featureMatrix = CommonHandles.RegressionPlaneHandle.UserData.features;
else
    %selecting the array of indices corresponding to this class
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
    indicesInThisClass = find(mergedClassVectors(:,idx));
    featureMatrix = cell2mat(CommonHandles.TrainingSet.Features(indicesInThisClass));
end

cMap = jet(256);
rescaledFeatures = zeros(length(featureMatrix(:,fID)), 1);
if strcmp(scaleType, 'linear') % linear scale

    if isempty(rescaledFeatures(featureMatrix(:,fID) < lo))
        lower = 1;
    else
        rescaledFeatures(featureMatrix(:,fID) < lo) = 1;
        lower = 40;
    end
    if isempty(rescaledFeatures(featureMatrix(:,fID) > hi))
        upper = 256;
    else
        rescaledFeatures(featureMatrix(:,fID) > hi) = 256;
        upper = 220;
    end
    rescaledFeatures(rescaledFeatures==0) = round(rescale(featureMatrix(rescaledFeatures==0,fID),lower,upper));
    
elseif strcmp(scaleType, 'log') % log scale
    
    if isempty(rescaledFeatures(featureMatrix(:,fID) < lo))
        lower = 1;
    else
        rescaledFeatures(featureMatrix(:,fID) < lo) = 1;
        lower = 1;
    end
    if isempty(rescaledFeatures(featureMatrix(:,fID) > hi))
        upper = 256;
    else
        rescaledFeatures(featureMatrix(:,fID) > hi) = 256;
        upper = 220;
    end
    tempFeatureValues = featureMatrix(rescaledFeatures==0,fID);
    minValue = min(tempFeatureValues);
    tempLog = log(tempFeatureValues / minValue);
    rescaledFeatures(rescaledFeatures==0) = round(rescale(tempLog,lower,upper));
    
end
if any(any(isnan(featureMatrix)))
     warndlg(sprintf('There are NaN-s among the features!\n NaN values going to represented with grey frame.'))
end

colorOrderForImFrame = zeros(size(rescaledFeatures,1), 3);

for i = 1:size(rescaledFeatures,1)
    if isnan(rescaledFeatures(i))
        colorOrderForImFrame(i,:) = [200 200 200];
    else
        colorOrderForImFrame(i,:) = round(cMap(rescaledFeatures(i), :)*256);
    end
end