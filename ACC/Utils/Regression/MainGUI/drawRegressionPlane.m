function fullRegPlane = drawRegressionPlane(handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     drawRegressionPlane
%
% Draws out the required part of regression plane to the main axes of the
% GUI. This is a static draw, simple takes the settings from handles and
% draws out the plane according to that.
%
% INPUT:
%   handles         Handles of the regression plane objects
%
% OUTPUT:
%   fullRegPlane    An image containing the drawn regression plane (NOTE:
%                   not the full plane only the visible part). 
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

% XBounds is a 2 element array with lower (1st element) and upper (2nd
% element) bound to the x coordinates.
% YBounds is the same for y.
% CommonHandles and handles are used for communication.

%global variables
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

% If an object will be selected then it'll be stored here.
selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
if isfield(CommonHandles.RegressionPlane{idx},'selectedAxes')
    % Stored handle to the currently selected cell's axes
    selectedAxes = CommonHandles.RegressionPlane{idx}.selectedAxes;
end
%Boolean, storing the grid switch's status
showGrid = CommonHandles.RegressionPlane{idx}.showGrid;
%real, storing the grid unit
gridDistance = CommonHandles.RegressionPlane{idx}.gridDistance;
%boolean, indicating whether the grid is on the top of every cell or below
%them
gridOnTheTop = CommonHandles.RegressionPlane{idx}.gridOnTheTop;
%the currently apparent picture size
apparentPictureSize = CommonHandles.RegressionPlane{idx}.apparentPictureSize;

%regPlaneBoundaries: the pixel positions for the regression plane in the
%GUI. 1-2 coordinates are the x and y position for the left bottom corner,
%the 3-4 coordinates are the size in x and y direction.
regPlaneBoundaries = round(get(handles.axes1,'Position'));

plotNavigationMap(idx,uData,handles);

XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
YBounds = CommonHandles.RegressionPlane{idx}.YBounds;

%setting the scales to the right place
xlim(handles.axes11,[XBounds(1) XBounds(2)]/1000);
ylim(handles.axes11,[YBounds(1) YBounds(2)]/1000);
%push the scale axes to the back.
uistack(handles.axes11,'bottom');

if uData.prediction == 1 || uData.prediction == 2 % prediction or PlotOfPlots mode
    indicesInThisClass = 1:length(uData.predictedIcons);
elseif uData.prediction == 3 % single trajectory mode
    indicesInThisClass = CommonHandles.RegressionPlane{idx}.selectedObject;
else
    %selecting the array of indices corresponding to this class    
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
    indicesInThisClass = find(mergedClassVectors(:,idx));
end

%the full DISPLAYED regression plane which can be seen on the screen.
fullRegPlane = uint8(zeros(regPlaneBoundaries(4),regPlaneBoundaries(3),3));%uint8(ones(regPlaneBoundaries(4),regPlaneBoundaries(3),3)*125);
selectedRegPlane = uint8(zeros(regPlaneBoundaries(4),regPlaneBoundaries(3),3));

%According to the current bounds we fill the regression plane with a grid.
if (((showGrid+1)==2) && ((gridOnTheTop+1)~=2))
    if isfield(CommonHandles, 'CustomRpBackground') && ~isempty(CommonHandles.CustomRpBackground)
        origBg = CommonHandles.CustomRpBackground;
        
        yB = round(CommonHandles.RegressionPlane{idx}.XBounds);
        xB = 1000 - round(CommonHandles.RegressionPlane{idx}.YBounds);
        
        FOV_Bg = origBg(xB(2)+1:xB(1),yB(1)+1:yB(2));
        [x, y, ~] = size(fullRegPlane);
        bg = imresize(FOV_Bg,[x,y]);
        ch1 = fullRegPlane(:,:,1);
        ch2 = fullRegPlane(:,:,2);
        ch3 = fullRegPlane(:,:,3);
        
        ch1(bg) = 200;
        ch2(bg) = 200;
        ch3(bg) = 200;
        
        fullRegPlane = cat(3,ch1,ch2,ch3);
    else
        XBU = ceil(XBounds(1)/gridDistance)*gridDistance;
        XBL = floor(XBounds(2)/gridDistance)*gridDistance;
        XLines = XBU:gridDistance:XBL;
        XLines = round( (XLines-XBounds(1))./ (XBounds(2)-XBounds(1)).*regPlaneBoundaries(3));
        fullRegPlane(:,XLines+1,:) = 100;
        
        YBU = ceil(YBounds(1)/gridDistance)*gridDistance;
        YBL = floor(YBounds(2)/gridDistance)*gridDistance;
        YLines = YBU:gridDistance:YBL;
        YLines = round( (YLines-YBounds(1))./ (YBounds(2)-YBounds(1)).*regPlaneBoundaries(4));
        fullRegPlane(regPlaneBoundaries(4)-YLines+1,:,:) = 100;
    end
end

% This matrix is the same sized as the full regression plane which is the
% displayed regPlane. This whichObjectThere matrix contains the numbers of
% the object which is the most upper right now at the given pixel position.
% This helps us to determine if a right click was made that which cell is
% selected.
whichObjectThere = uint32(zeros(regPlaneBoundaries(4),regPlaneBoundaries(3)));%false(regPlaneBoundaries(4),regPlaneBoundaries(3));%
whichSelectedObjectThere = uint32(zeros(regPlaneBoundaries(4),regPlaneBoundaries(3)));%false(regPlaneBoundaries(4),regPlaneBoundaries(3));%

%Delete ALL previous selected axes
if exist('selectedAxes','var') && ~isempty(selectedAxes)
    for i=1:length(selectedAxes)        
        if ishandle(selectedAxes{i}) || isa(selectedAxes{i},'fakeAxes')
            delete(selectedAxes{i});
        end
    end
    selectedAxes = {};
end

if CommonHandles.RegressionPlane{idx}.colorFrame.state
    if ~isfield(CommonHandles.RegressionPlane{idx}.colorFrame, 'ColorOrder') || size(indicesInThisClass, 2) ~= size(CommonHandles.RegressionPlane{idx}.colorFrame.ColorOrder, 1) || CommonHandles.RegressionPlane{idx}.colorFrame.calcNew
        colorOrder = calcColorOrder(handles);
        CommonHandles.RegressionPlane{idx}.colorFrame.calcNew = 0;
        CommonHandles.RegressionPlane{idx}.colorFrame.ColorOrder = colorOrder;
    else
        colorOrder = CommonHandles.RegressionPlane{idx}.colorFrame.ColorOrder;
    end
end

toolbarPerformanceStateOn = strcmp(get(handles.toolbarRegressionPerformance,'State'),'on');

if uData.prediction == 1
    feats = cell2mat(CommonHandles.TrainingSet.Features);
end

multIdx = 1; %this is a counter for the selected axes
%For every cell which has it's position on the regression plane (in reversed order.)
%The reversed order is because of the following reasons:
%   1) We would like to have the last cells on the top of the plane
%   2) We would like to spare time by not drawing cells that are not
%   visible (because they are shadowed by the more former objects)
%   3) During drawing only so far empty areas should be filled
for ii=length(indicesInThisClass):-1:1
    i = indicesInThisClass(ii);
    if isempty(i), break; end % I don't think this is possible any more
    %select the information source
    if uData.prediction == 1 || uData.prediction == 2 %prediction or PlotOfPlots mode
        classImgObj = uData.predictedIcons{i};
        centerX = uData.predictedPositions(1,i);
        centerY = uData.predictedPositions(2,i);
    else
        %if the current ith cell doesn't have position or the regplane is empty
        if  ( ~isfield(CommonHandles.TrainingSet,'RegPos') || i>length(CommonHandles.TrainingSet.RegPos) || isempty(CommonHandles.TrainingSet.RegPos{i}) )          
            continue;
        end

        %get the picture from the classImgObjects field.
        [classImgObj,innerIdx] = getClassImgObjByTrainingSetID(CommonHandles,i,mergedClassVectors);

        %Getting out the regression position of the currentImage from
        %CommonHandles.
        centerX = CommonHandles.TrainingSet.RegPos{i}(1);
        centerY = CommonHandles.TrainingSet.RegPos{i}(2);
    end
    
    if CommonHandles.RegressionPlane{idx}.colorFrame.state        
        padRows = 10;
        padCols = 10;                                        
        
        %Get the real size of picture which we'd like to display (the rescale will be done later)
        pictureDataXSize = size(classImgObj.Image,2)+padCols*2;
        pictureDataYSize = size(classImgObj.Image,1)+padRows*2;
    else
        %Get the real size of picture which we'd like to display
        pictureDataXSize = size(classImgObj.Image,2);
        pictureDataYSize = size(classImgObj.Image,1);
    end
    
    [centerX,centerY] = regPlane2GUI(centerX,centerY,XBounds,YBounds,regPlaneBoundaries);
    
    
    %The displayed image size initialization
    apparentPictureXSize = apparentPictureSize;
    apparentPictureYSize = apparentPictureSize;
    
    %Calculating the left upper corner position and round because we need
    %integer coordinates for the fullRegPlane image matrix
    leftUpperX = round(centerX - (apparentPictureXSize / 2));
    leftUpperY = round(centerY - (apparentPictureYSize / 2));
    
    isCurrentObjSelected = ismember(i,selectedObject);
    isInBounds = ((leftUpperX+apparentPictureXSize >= 0) && (leftUpperX <= regPlaneBoundaries(3)) && (leftUpperY+apparentPictureYSize >= 0) && (leftUpperY <= regPlaneBoundaries(4)));
    
    %Transform to the GUI's coordinate system.    
        %If the current picture is in the required area then:
    if isCurrentObjSelected || isInBounds
        %Handle the cases when the picture is out of the regression bound.
        
        %Initializing shown image boundaries.
        apparentStartX = leftUpperX;    
        apparentStartY = leftUpperY;    
        apparentEndX = leftUpperX+apparentPictureXSize-1;
        apparentEndY = leftUpperY+apparentPictureYSize-1;
        
        %Do the cuttings on the edge.
        if apparentStartX<1
           apparentStartX=1;
        end
        if apparentStartY<1
           apparentStartY=1;
        end
        if apparentEndX>regPlaneBoundaries(3)
           apparentEndX=regPlaneBoundaries(3);
        end
        if apparentEndY>regPlaneBoundaries(4)
           apparentEndY=regPlaneBoundaries(4);
        end
        
        %Calculating the start and end position for the picture pixels.
        XStartOfPicture = floor ( 1 + ((apparentStartX-leftUpperX)/(apparentPictureXSize-1))*(pictureDataXSize-1) );
        YStartOfPicture = floor ( 1 + ((apparentStartY-leftUpperY)/(apparentPictureYSize-1))*(pictureDataYSize-1) );
        XEndOfPicture = floor ( 1 + ((apparentEndX-leftUpperX)/(apparentPictureXSize-1))*(pictureDataXSize-1)  );
        YEndOfPicture = floor ( 1 + ((apparentEndY-leftUpperY)/(apparentPictureYSize-1))*(pictureDataYSize-1)  );
                      
        %Final check                   
        if XStartOfPicture < 1, XStartOfPicture = 1; end
        if YStartOfPicture < 1, YStartOfPicture = 1; end                       
        
        %Do the interpolation
        %Calculate the rescale rate
        %tempXMult = (XEndOfPicture-XStartOfPicture)/(apparentEndX-apparentStartX+1);
        %tempYMult = (YEndOfPicture-YStartOfPicture)/(apparentEndY-apparentStartY+1);
        %The offset which is between the picture's coordinate system
        %and the regression plane's coordinate system
        %tempXOffset = (-apparentStartX*tempXMult) + XStartOfPicture;
        %tempYOffset = (-apparentStartY*tempYMult) + YStartOfPicture;

        tempy=apparentStartY:floor(apparentEndY);
        %tempPictureY=ceil(tempy*tempYMult + tempYOffset);                
        tempPictureY = round(linspace(YStartOfPicture,YEndOfPicture,length(tempy)));
        tempx=apparentStartX:floor(apparentEndX);
        %tempPictureX=ceil(tempx*tempXMult + tempXOffset); 
        tempPictureX = round(linspace(XStartOfPicture,XEndOfPicture,length(tempx)));
        
        %Pixel position calculation DONE.
        
        %Check if there is still place to draw this:
        if ( ~isCurrentObjSelected && ~all(all(whichObjectThere(tempy,tempx))) ) ||...
           (  isCurrentObjSelected && ~all(all(whichSelectedObjectThere(tempy,tempx))) ) 
            currentCellIsVisible = true;
        else
            currentCellIsVisible = false;
        end
        
        % Do calculation but only gently
        try            
            %Calculate the image with frame if needed
            if currentCellIsVisible && CommonHandles.RegressionPlane{idx}.colorFrame.state
                tempIm = im2uint8(classImgObj.Image); %This is a costly action
                tempImR = padarray(tempIm(:,:,1), [padRows padCols], colorOrder(ii, 1));
                tempImG = padarray(tempIm(:,:,2), [padRows padCols], colorOrder(ii, 2));
                tempImB = padarray(tempIm(:,:,3), [padRows padCols], colorOrder(ii, 3));
                tempImWFrame = cat(3, tempImR, tempImG, tempImB);
            end
                        
            if currentCellIsVisible && ~isCurrentObjSelected
                idxInTS = 0;
                if uData.prediction == 1
                    idxInTS = isInTrainingSet( uData.predictedIcons{i}.ImageName, uData.predictedIcons{i}.CellNumber, 0, CommonHandles,uData.features(i,:), feats);
                end
                if CommonHandles.RegressionPlane{idx}.colorFrame.state                    
                    %fullRegPlane(tempy,tempx,:) = tempImWFrame(tempPictureY,tempPictureX,:);                    
                    fillImg = tempImWFrame(tempPictureY,tempPictureX,:);
                else
                    %fullRegPlane(tempy,tempx,:) = im2uint8(classImgObj.Image(tempPictureY,tempPictureX,:));
                    fillImg = im2uint8(classImgObj.Image(tempPictureY,tempPictureX,:));
                end
                %Change it to blue if it's in the training set
                if uData.prediction == 1 && idxInTS~=0  && find(CommonHandles.TrainingSet.Class{idxInTS})==uData.lastRegressionPlaneIdx && ~isempty(tempx) && ~isempty(tempy)
                    fillImg([1,end],:,3) = 255;
                    fillImg(:,[1,end],3) = 255;
                end                                
                innerIdx = i;
                %fillEmptyRegPlaneSpace; Should be a script but seems
                %much much faster inline                 
                %INLINE SCRIPT START
                oldPartImg = fullRegPlane(tempy,tempx,:);
                oldPartWOT = whichObjectThere(tempy,tempx);
                newPartImg = fillImg;
                newPartWOT = uint32(ones(size(fillImg,1),size(fillImg,2))*innerIdx);
                smallEmptyIdx = oldPartWOT == 0;
                logIdx3D = repmat(~smallEmptyIdx,1,1,3);
                newPartImg(logIdx3D) = oldPartImg(logIdx3D);
                newPartWOT(~smallEmptyIdx) = oldPartWOT(~smallEmptyIdx);
                fullRegPlane(tempy,tempx,:) = newPartImg;
                whichObjectThere(tempy,tempx) = newPartWOT;
                %INLINE SCRIPT END
                                
            %if it is not visible but at least selected
            elseif isCurrentObjSelected
                %Transform back leftBottomY as the axes' position is
                %counted as a picture.                                                                
                leftBottomY = regPlaneBoundaries(4) - apparentEndY;
                if isInBounds
                    currAxesPosition = [apparentStartX+regPlaneBoundaries(1) leftBottomY+regPlaneBoundaries(2) (apparentEndX-apparentStartX) (apparentEndY-apparentStartY)];
                else
                    currAxesPosition = [...
                        regPlaneBoundaries(1)+centerX-apparentPictureXSize/2 ...
                        regPlaneBoundaries(2)+regPlaneBoundaries(4)-centerY-apparentPictureYSize/2 ...
                        apparentPictureXSize ...
                        apparentPictureYSize];
                end
                if currentCellIsVisible                   

                    if CommonHandles.RegressionPlane{idx}.colorFrame.state
                        selectedObjectPicture = tempImWFrame(tempPictureY,tempPictureX,:);
                    elseif strcmp(handles.TrajColorView.State, 'on')
                        tempIm = im2uint8(classImgObj.Image(tempPictureY,tempPictureX,:));
                        plateAndImName = strsplit(classImgObj.ImageName,{filesep, 'anal1'});
                        trackIdx = find(CommonHandles.Tracking.ObjectID == classImgObj.CellNumber .* strcmp(CommonHandles.Tracking.ImageName,plateAndImName{2}) .* strcmp(CommonHandles.Tracking.Plate,plateAndImName{1}));
                        frameID = CommonHandles.Tracking.Frame(trackIdx);
                        maxFrame = max(CommonHandles.Tracking.Frame);
                        padRows = 10;
                        padCols = 10;
                        colorScale = hsv(maxFrame);
                        tempImR = padarray(tempIm(:,:,1), [padRows padCols], round(colorScale(frameID,1)*255));
                        tempImG = padarray(tempIm(:,:,2), [padRows padCols], round(colorScale(frameID,2)*255));
                        tempImB = padarray(tempIm(:,:,3), [padRows padCols], round(colorScale(frameID,3)*255));
                        selectedObjectPicture = cat(3, tempImR, tempImG, tempImB);
                        selectedObjectPicture = imresize(selectedObjectPicture,size(tempIm,[1 2]));
                    else
                        selectedObjectPicture = im2uint8(classImgObj.Image(tempPictureY,tempPictureX,:));
                    end
                    %Set border to cyan
                    selectedObjectPicture([1,end],:,[2 3]) = 255;
                    selectedObjectPicture(:,[1,end],[2 3]) = 255;
                    fillImgSize = size(selectedObjectPicture);
                    
                    %We need to fill the whichObjectThere array anyway to spare
                    %the computation                
                    innerIdx = i;                
                    
                    %INLINE SCRIPT START
                    oldPartImg = selectedRegPlane(tempy,tempx,:);
                    oldPartWOT = whichSelectedObjectThere(tempy,tempx);
                    newPartImg = selectedObjectPicture;
                    newPartWOT = uint32(ones(fillImgSize(1),fillImgSize(2))*innerIdx);
                    smallEmptyIdx = oldPartWOT == 0;
                    logIdx3D = repmat(~smallEmptyIdx,1,1,3);
                    newPartImg(logIdx3D) = oldPartImg(logIdx3D);
                    newPartWOT(~smallEmptyIdx) = oldPartWOT(~smallEmptyIdx);
                    selectedRegPlane(tempy,tempx,:) = newPartImg;
                    whichSelectedObjectThere(tempy,tempx) = newPartWOT;
                    %INLINE SCRIPT END                                    
                end                                
                % In this case try with always fake axes
                selectedAxes{multIdx} = fakeAxes(currAxesPosition,i);
                
                multIdx = multIdx + 1;
            end
            if toolbarPerformanceStateOn
                predX = CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions(1,ii);
                predY = CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions(2,ii);
                [predX,predY] = regPlane2GUI(predX,predY,XBounds,YBounds,regPlaneBoundaries);                                        
                fullRegPlane = drawLineToPicture(fullRegPlane,round([centerY,centerX]),round([predY,predX]),[255,255,255]);
            end
        catch         
            %There are sometimes (rarely) problem with index bounds, which
            %did not cause problem yet, and its easier to solve with
            %try-catch than with many-many ifs.            
        end                   
    end
end
   
if (((showGrid+1)==2) && ((gridOnTheTop+1)==2))
    if isfield(CommonHandles, 'CustomRpBackground') && ~isempty(CommonHandles.CustomRpBackground)
        origBg = CommonHandles.CustomRpBackground;
        yB = round(CommonHandles.RegressionPlane{idx}.XBounds);
        xB = round(CommonHandles.RegressionPlane{idx}.YBounds);
        
        FOV_Bg = origBg(xB(1):xB(2),yB(1):yB(2));
        [x, y, ~] = size(fullRegPlane);
        bg = imresize(FOV_Bg,[x,y]);
        ch1 = fullRegPlane(:,:,1);
        ch2 = fullRegPlane(:,:,2);
        ch3 = fullRegPlane(:,:,3);
        
        ch1(bg) = 200;
        ch2(bg) = 200;
        ch3(bg) = 200;
        
        fullRegPlane = cat(3,ch1,ch2,ch3);
    else
        XBU = ceil(XBounds(1)/gridDistance)*gridDistance;
        XBL = floor(XBounds(2)/gridDistance)*gridDistance;
        XLines = XBU:gridDistance:XBL;
        XLines = round( (XLines-XBounds(1))./ (XBounds(2)-XBounds(1)).*regPlaneBoundaries(3));
        fullRegPlane(:,XLines+1,:) = 100;
        
        YBU = ceil(YBounds(1)/gridDistance)*gridDistance;
        YBL = floor(YBounds(2)/gridDistance)*gridDistance;
        YLines = YBU:gridDistance:YBL;
        YLines = round( (YLines-YBounds(1))./ (YBounds(2)-YBounds(1)).*regPlaneBoundaries(4));
        fullRegPlane(regPlaneBoundaries(4)-YLines+1,:,:) = 100;
    end
end

% if uData.prediction == 2 %in plotOfPlot the regplane contains doubles    
%     %Do nothing    
% else    
%     fullRegPlane = uint8(fullRegPlane);    
%     selectedRegPlane = uint8(selectedRegPlane);
% end

% Show the image
im = imshow(fullRegPlane,'Parent',handles.axes1);    
% Show also the selected image
if multIdx > 1
    selectedAxes{multIdx} = axes('Units','pixels','Position',handles.axes1.Position);
    imS = imshow(selectedRegPlane,'Parent',selectedAxes{multIdx});
    axis image;
    set(selectedAxes{multIdx},'ButtonDownFcn',{@dragObject, handles});
    set(imS,'HitTest','off');
    set(imS,'ButtonDownFcn',{@dragObject, handles});
    set(imS,'HitTest','on'); %now image button function will work
    set(imS,'AlphaData',double(whichSelectedObjectThere > 0));
end        

%the button down fcn will not work until the image hit test is off
set(im,'HitTest','off');

%now set an image button doen fcn
set(im,'ButtonDownFcn',@(hObject,eventdata)regressionPlane('axes1_ButtonDownFcn',hObject,eventdata,guidata(hObject)));

%the image funtion will not fire until hit test is turned on
set(im,'HitTest','on'); %now image button function will work
        
       
CommonHandles.RegressionPlane{idx}.whichObjectThere = whichObjectThere;
CommonHandles.RegressionPlane{idx}.whichSelectedObjectThere = whichSelectedObjectThere;
CommonHandles.RegressionPlane{idx}.selectedAxes = selectedAxes;

% *************************************************************************
% *************************************************************************
