%function [fullRegPlane,whichObjectThere] = fillEmptyRegPlaneSpace(fullRegPlane,whichObjectThere,tempx,tempy,fillImg,innerIdx)
% AUTHOR:   Abel Szkalisity
% DATE:     May 12, 2020
% NAME:     fillEmptyRegPlaneSpace
%
% The function (ACTUALLY SCRIPT DUE TO CRAZY OVERHEAD IN FUNCTION CALL) is
% used to fill out only the empty space in the regression plane. This is
% needed due to user experience enhancement (drawing cells on regplane in a
% reversed direction) so that the later cells are not shadowing the former
% ones.
%
% INPUT:
%   fullRegPlane    The image matrix representing the visible regression
%                   plane at the moment.
%   whichObjectThere A matrix with similar size to the previous one. Each
%                   entry represents an index of the object which occupy
%                   the matrix above.
%   tempx,tempy     The x and y index arrays that specify which part of the
%                   fullRegPlane should be occupied (but maybe not all will
%                   be becauses its occupied beforehand)
%   fillImg         An image with exactly tempy,tempx dimensions, that
%                   should fit in to the space
%   innerIdx        The index of the image to be filled into
%                   whichObjectThere
%
% OUTPUT:
%   fullRegPlane    The updated image matrix.
%   whichObjectThere The updated index matrix.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland

%{
%Some hacking is needed because of meshgrid output
[tempYFull3D,tempXFull3D,tempZFull3D] = meshgrid(tempy,tempx,1:3);
permOrder = [2 1 3];
tempYFull3D = permute(tempYFull3D,permOrder);
tempXFull3D = permute(tempXFull3D,permOrder);
tempZFull3D = permute(tempZFull3D,permOrder);
[tempYFull2D,tempXFull2D] = meshgrid(tempy,tempx);
permOrder = [2 1];
tempYFull2D = permute(tempYFull2D,permOrder);
tempXFull2D = permute(tempXFull2D,permOrder);

linearBigIndices3D = sub2ind(size(fullRegPlane),tempYFull3D(:),tempXFull3D(:),tempZFull3D(:));
linearBigIndices2D = sub2ind(size(whichObjectThere),tempYFull2D(:),tempXFull2D(:));

toFillSpace = whichObjectThere(tempy,tempx);
emptySpace2Didx = toFillSpace == 0;
emptySmallLinearIndices = find(repmat(emptySpace2Didx,1,1,3));
whichObjectThere(linearBigIndices2D(emptySpace2Didx)) = uint32(i);

fullRegPlane(linearBigIndices3D(emptySmallLinearIndices)) = fillImg(emptySmallLinearIndices);
%}

%draw below

oldPartImg = fullRegPlane(tempy,tempx,:);
oldPartWOT = whichObjectThere(tempy,tempx);
newPartImg = fillImg;
newPartWOT = uint32(ones(size(fillImg,1),size(fillImg,2))*innerIdx); %This should be an outer i
smallEmptyIdx = all(oldPartImg == 0,3);
logIdx3D = repmat(~smallEmptyIdx,1,1,3);

newPartImg(logIdx3D) = oldPartImg(logIdx3D);
newPartWOT(~smallEmptyIdx) = oldPartWOT(~smallEmptyIdx);
fullRegPlane(tempy,tempx,:) = newPartImg;
whichObjectThere(tempy,tempx) = newPartWOT;

%end

