classdef fakeAxes < handle
% AUTHOR:   Abel Szkalisity
% DATE:     May 12, 2020
% NAME:     fakeAxes
%
% Represents an axes object but only with those fields that are used in
% regression plane. This class is used to enhance the user experience (by
% being much simpler than a proper axes objects). Hence its name: faking an
% axes.
%
% INPUT:
%   position        A vector with length 4 representing the position of the
%                   axes
%   uData           The userData of the axes is a single integer
%                   representing the ID of the selected object.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.
    
    properties
        Position
        UserData        
    end
    
    methods
        function obj = fakeAxes(pos,uData)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.Position = pos;
            obj.UserData = uData;
        end
        
        function res = get(obj,fieldStr)
            res = obj.(fieldStr);
        end
        
        function set(obj,fieldStr,value)
            obj.(fieldStr) = value;
        end
    end
end

