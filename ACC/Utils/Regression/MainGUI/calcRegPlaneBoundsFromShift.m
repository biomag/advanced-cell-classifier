function [XBounds,YBounds] = calcRegPlaneBoundsFromShift(posDiff,XBounds,YBounds,regPlaneBoundaries)
% AUTHOR:   Abel Szkalisity
% DATE:     May 12, 2020
% NAME:     calcRegPlaneBoundsFromShift
%
% This util function calculates the updated X and YBounds for the
% regression plane (measued in its intrinsic coordinate system). It checks
% whether a given shift in apparent pixels is taking us out from the bounds
% and blocks the shift if it is.
%
% INPUT:
%   posDiff         A vector with length of 2, specifying the shifts in
%                   each dimension
%   XBounds         A vector of length 2, the limits (minimum and maximum)
%                   of the X axis in the regplane
%   YBounds         A vector of length 2, the limits (minimum and maximum)
%                   of the Y axis in the regplane
%   regPlaneBoundaries The regression plane boundaires in pixels
%
% OUTPUT:
%   XBounds,YBounds The updated arrays
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

posXDiffRegPlane = posDiff(1)*(XBounds(2)-XBounds(1))./regPlaneBoundaries(3)./10;
posYDiffRegPlane = posDiff(2)*(YBounds(2)-YBounds(1))./regPlaneBoundaries(4)./10;

if max(XBounds - posXDiffRegPlane)<1000 && min(XBounds - posXDiffRegPlane)>0
    XBounds = XBounds - posXDiffRegPlane;
end
if max(YBounds - posYDiffRegPlane)<1000 && min(YBounds - posYDiffRegPlane)>0
    YBounds = YBounds - posYDiffRegPlane;
end


end

