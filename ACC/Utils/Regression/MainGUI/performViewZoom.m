function performViewZoom(pos, zoomDir, handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     performViewZoom
%
% This function encapsulates the zoom in and out to the GUI. The pos
% parameter is in the GUI coordinate system the center of zoom.
%
% INPUT:
%   pos             The center of the zoom
%   zoomDir         A numeric value, the sign should determine the
%                   direction of the zoom
%   handles         handles to the properties of the regressionPlane GUI
%
% OUTPUT:
%   changes the view of the regression plane
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;

XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
YBounds = CommonHandles.RegressionPlane{idx}.YBounds;
adaptiveCellSize = CommonHandles.RegressionPlane{idx}.adaptiveCellSize;
apparentPictureSize = CommonHandles.RegressionPlane{idx}.apparentPictureSize;
startOfAdaptionCellSize = CommonHandles.RegressionPlane{idx}.startOfAdaptionCellSize;

regPlaneBoundaries = round(get(handles.axes1,'Position'));

if pos(1)>regPlaneBoundaries(1) && pos(1)<regPlaneBoundaries(1)+regPlaneBoundaries(3) && pos(2)> regPlaneBoundaries(2) && pos(2) < regPlaneBoundaries(2) + regPlaneBoundaries(4)        
    if zoomDir>0 % we're zooming out        
        newSizeX = (XBounds(2) - XBounds(1))*1.2;
        newSizeY = (YBounds(2) - YBounds(1))*1.2;                
        if newSizeX>1000, newSizeX = 1000; end
        if newSizeY>1000, newSizeY = 1000; end 
        if (adaptiveCellSize && apparentPictureSize > startOfAdaptionCellSize)
            apparentPictureSize = apparentPictureSize.*0.8;                        
        end              
    else % we're zooming in
        newSizeX = (XBounds(2) - XBounds(1))*0.9;
        newSizeY = (YBounds(2) - YBounds(1))*0.9;                
        if adaptiveCellSize
            apparentPictureSize = apparentPictureSize.*1.1;
        end
    end    
    calcXYBounds(newSizeX, newSizeY, pos, regPlaneBoundaries,idx);

    CommonHandles.RegressionPlane{idx}.apparentPictureSize = apparentPictureSize;
    
    %From drawRegPlane itself
    if uData.prediction == 1 || uData.prediction == 2 % prediction or PlotOfPlots mode
        nofCellsOnRegPlane = length(uData.predictedIcons);
    else
        %selecting the array of indices corresponding to this class    
        mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
        nofCellsOnRegPlane = length(find(mergedClassVectors(:,idx)));
    end
    
    if nofCellsOnRegPlane < 1000 %User defined threshold
       drawRegressionPlane(handles);
    else
        drawDynamicRegressionPlane(handles);    
    end

end


