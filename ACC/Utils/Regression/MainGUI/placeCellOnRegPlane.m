function placeCellOnRegPlane(pos,handles,idInTrainingSet)
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
global CommonHandles;
if CommonHandles.RegressionPlane{idx}.horizontal1DRegression
    regressionHalf = 500;
    CommonHandles.TrainingSet.RegPos{idInTrainingSet} = [pos(1) regressionHalf];
else
    CommonHandles.TrainingSet.RegPos{idInTrainingSet} = [pos(1) pos(2)];
end
