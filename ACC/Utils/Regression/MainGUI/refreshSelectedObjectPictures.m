function refreshSelectedObjectPictures(idx,uData)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     refreshSelectedObjectPictures
%
%This util function refreshes the pictures of the selectedCells. 
%
% INPUT:
%   idx          The index of the class of the current regression plane
%   uData        UserData structure of the regression plane
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
global ghandles;
handles = ghandles;

if length(selectedObject) == 1
    if uData.prediction == 1 || uData.prediction == 2 %in prediction and plot of plots mode
        classImgObj = uData.predictedIcons{selectedObject};
    else
        classImgObj = getClassImgObjByTrainingSetID(CommonHandles,selectedObject);
    end
    imshow(classImgObj.Image,'Parent',handles.axes4);
else
    imshow(ones(100,100,3).*0.5,'Parent',handles.axes4);
end
