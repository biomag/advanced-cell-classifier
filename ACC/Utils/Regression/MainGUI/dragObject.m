function dragObject(hObject,~,handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     dragObject
%
% This function is a callback used for dragging the object (connected to
% the buttonDown function)
%
% INPUT:
%   Traditional inputs for callbacks.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
selType = get(gcf,'SelectionType');
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

%First we need to define if this is on a selected object or not
pos = round(get(gcf,'CurrentPoint'));
regPlaneBoundaries = round(get(handles.axes1,'Position'));

if pos(1)<regPlaneBoundaries(1), pos(1) = regPlaneBoundaries(1); end
if pos(1)>regPlaneBoundaries(1)+regPlaneBoundaries(3), pos(1) = regPlaneBoundaries(1)+regPlaneBoundaries(3); end
if pos(2)<regPlaneBoundaries(2), pos(2) = regPlaneBoundaries(2); end
if pos(2)>regPlaneBoundaries(2)+regPlaneBoundaries(4), pos(2) = regPlaneBoundaries(2)+regPlaneBoundaries(4); end

distFromXLowerInGUI = pos(1) - regPlaneBoundaries(1);
distFromYLowerInGUI = pos(2) - regPlaneBoundaries(2);
possibleObjIdx = CommonHandles.RegressionPlane{idx}.whichSelectedObjectThere(regPlaneBoundaries(4)-distFromYLowerInGUI,distFromXLowerInGUI);

if possibleObjIdx
    switch selType
        case 'normal'        
            CommonHandles.RegressionPlane{idx}.dragging = CommonHandles.RegressionPlane{idx}.selectedAxes;
            CommonHandles.RegressionPlane{idx}.orPos = round(get(gcf,'CurrentPoint'));
        case 'extend'        
            CommonHandles.RegressionPlane{idx}.selectedObject = setdiff(CommonHandles.RegressionPlane{idx}.selectedObject,possibleObjIdx);
            drawRegressionPlane(handles);
    end
    set(gcf,'Pointer','hand');
else
    regressionPlane('axes1_ButtonDownFcn',handles.axes1,[],handles);
end