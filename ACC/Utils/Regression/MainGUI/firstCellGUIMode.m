function firstCellGUIMode(enable,handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     firstCellGUIMode
%
% Turns on and off the toolbar buttons that must be disabled in prediction
% mode.
%
% INPUT:
%   enable          Boolean indicating if to turn on or off the toolbar
%                   buttons
%   handles         Handles of the regression plane objects
%
% OUTPUT:
%   Modifies the GUI properties
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    if enable
        string = 'Off';
    else
        string = 'On';
    end
    set(handles.uipushtool3,'Enable',string);
    set(handles.toolbarTrainRegression,'Enable',string);
    set(handles.toolbarRegressionPerformance,'Enable',string);
    set(handles.toolbarPredictWellPushtool,'Enable',string);
    set(handles.toolbarPredictPlatePushtool,'Enable',string);
    set(handles.toolbarPlotOfPlots,'Enable',string);  
    set(handles.editXCoord,'Enable',string);
    set(handles.editYCoord,'Enable',string);
