function bool = isInRegPlane(newPos, regPlaneBoundaries)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     isInRegPlane
%
% This util function is more like a macro, it is used for shortening long
% conditions when we're examing whether a point is in the regression plane
% or not.
%
% INPUT:
%   newPos          Position of the coordinate to be queried
%   regPlaneBoundaries The position property of the regression plane 
%                       (posX,posY, width, height) 
%
% OUTPUT:
%   bool            Indicates if the position is within the regression
%                   plane boundaries
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

bool = newPos(1)>regPlaneBoundaries(1) && newPos(1)<regPlaneBoundaries(1)+regPlaneBoundaries(3) && newPos(2)> regPlaneBoundaries(2) && newPos(2) < regPlaneBoundaries(2) + regPlaneBoundaries(4);
