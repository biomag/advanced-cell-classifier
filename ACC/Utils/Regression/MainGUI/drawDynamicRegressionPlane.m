function drawDynamicRegressionPlane(handles)
% AUTHOR:   Abel Szkalisity, Beleon Attila
% DATE:     May 21, 2020
% NAME:     drawDynamicRegressionPlane
%
% 
% INPUT:
%   pos             The center of the zoom
%   zoomDir         A numeric value, the sign should determine the
%                   direction of the zoom
%   handles         handles to the properties of the regressionPlane GUI
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

myTicTime = tic;
%fprintf('Timer out: %d\n',CommonHandles.RegressionPlane{idx}.dynamicEvents.lastDynamicTic);
if CommonHandles.RegressionPlane{idx}.dynamicEvents.dynamicDrawNow %If the previous dynamic draw is ready
    return;
end
    CommonHandles.RegressionPlane{idx}.dynamicEvents.dynamicDrawNow = true;
    CommonHandles.RegressionPlane{idx}.dynamicEvents.lastDynamicTic = myTicTime;
    
    currImg = getframe(handles.axes1);
    currImg = currImg.cdata;

    oldXBounds = CommonHandles.RegressionPlane{idx}.dynamicEvents.XBoundsOld;
    oldYBounds = CommonHandles.RegressionPlane{idx}.dynamicEvents.YBoundsOld;
    newXBounds = CommonHandles.RegressionPlane{idx}.XBounds;
    newYBounds = CommonHandles.RegressionPlane{idx}.YBounds;

    currImgSize = [NaN, NaN, size(currImg,2), size(currImg,1)];

    [XLB,YLB] = regPlane2GUI(newXBounds(1),newYBounds(1),oldXBounds,oldYBounds,currImgSize);
    [XRU,YRU] = regPlane2GUI(newXBounds(2),newYBounds(2),oldXBounds,oldYBounds,currImgSize);

    XLB = round(XLB);
    YLB = round(YLB);
    XRU= round(XRU);
    YRU = round(YRU);

    if XLB == 0, XLB = 1; end
    if YRU == 0, YRU = 1; end

    if XLB>=1 && YRU>=1 && YLB<=size(currImg,1) && XRU<=size(currImg,2) %ZOom in    
        newImg = imresize(currImg(YRU:YLB,XLB:XRU,:),[size(currImg,1) size(currImg,2)]);
    else % Zoom out
        newImg = cast(zeros(...
                round(YLB)-round(YRU),...
                round(XRU)-round(XLB),...
                size(currImg,3)...
            ),class(currImg));  

        newImg(abs(YRU):abs(YRU)+size(currImg,1)-1,abs(XLB):abs(XLB)+size(currImg,2)-1,:) = currImg;    
        newImg = imresize(newImg,[size(currImg,1) size(currImg,2)]);        
    end

    for i=1:length(CommonHandles.RegressionPlane{idx}.selectedAxes)
        if isa(CommonHandles.RegressionPlane{idx}.selectedAxes{i},'matlab.graphics.axis.Axes') && ishandle(CommonHandles.RegressionPlane{idx}.selectedAxes{i})
            delete(CommonHandles.RegressionPlane{idx}.selectedAxes{i})
        end
    end
    imshow(newImg,'Parent',handles.axes1);
    plotNavigationMap(idx,uData,handles);
    %fprintf('fake draw happened\n');
    
    CommonHandles.RegressionPlane{idx}.dynamicEvents.dynamicDrawNow = false;

    waitedDrawing(handles,idx,myTicTime);

end

function waitedDrawing(handles,idx,myTicTime)
    global CommonHandles;    
    pause(CommonHandles.RegressionPlane{idx}.dynamicEvents.dynamicEventLatency);
    if CommonHandles.RegressionPlane{idx}.dynamicEvents.lastDynamicTic == myTicTime
        %fprintf('I''m last: %d\n',myTicTime); 
        drawRegressionPlane(handles);
        %fprintf('real draw happened\n');
    end
end

