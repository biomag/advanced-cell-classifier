function calcXYBounds(newSizeX, newSizeY, pos, regPlaneBoundaries,idx)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     calcXYBounds
%
% This util function modifies the global X and YBounds variables according to the
% new XY sizes and the center of the view which is stored in pos. We also
% need the regPlaneBoundaries variable which stores the position of the
% regression plane.
%
% INPUT:
%   newSizeX        The new size in X dimension
%   newSizeY        The new size in Y dimension
%   pos             The viewpoint (center of view)
%   regPlaneBoundaries A 4 long vector containing the position property of
%                   the main axis in the regression plane window (the plane
%                   itself)
%   idx             The index of the class of the current regression plane
%
% OUTPUT:
%   This function modifies the CommonHandles
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
    YBounds = CommonHandles.RegressionPlane{idx}.YBounds;
    
    posXInRegPlane = (pos(1) - regPlaneBoundaries(1))/regPlaneBoundaries(3)*(XBounds(2) - XBounds(1))+XBounds(1);
    posYInRegPlane = (pos(2) - regPlaneBoundaries(2))/regPlaneBoundaries(4)*(YBounds(2) - YBounds(1))+YBounds(1);            
    
    XNewLower = posXInRegPlane - (pos(1)-regPlaneBoundaries(1))*newSizeX./regPlaneBoundaries(3);
    if XNewLower<0, XNewLower = 0; end    
    XNewUpper = XNewLower + newSizeX;
    if XNewUpper>1000, XNewUpper = 1000; end    
    YNewLower = posYInRegPlane - (pos(2)-regPlaneBoundaries(2))*newSizeY./regPlaneBoundaries(4);
    if YNewLower<0, YNewLower = 0; end    
    YNewUpper = YNewLower + newSizeY;        
    if YNewUpper>1000, YNewUpper = 1000; end
    
    CommonHandles.RegressionPlane{idx}.dynamicEvents.XBoundsOld = XBounds;
    CommonHandles.RegressionPlane{idx}.dynamicEvents.YBoundsOld = YBounds;
    CommonHandles.RegressionPlane{idx}.XBounds = [ XNewLower XNewUpper ];
    CommonHandles.RegressionPlane{idx}.YBounds = [ YNewLower YNewUpper ];
