function plotNavigationMap(idx,uData,handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     plotNavigationMap
%
% This util function is plotting the navigation on which we can see where we
% are currently on the regression plane. (how much we're zoomed in etc.)
%
% INPUT:
%   idx          The index of the class of the current regression plane
%   uData        UserData structure of the regression plane
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
YBounds = CommonHandles.RegressionPlane{idx}.YBounds;

%cla(handles.axes10);
%hold(handles.axes10,'on');

greenPositions = zeros(length(CommonHandles.TrainingSet.RegPos),2);

j = 1;
if uData.prediction==1 || uData.prediction == 2 %in prediction or plot of plots mode
    bluePositions = (uData.predictedPositions)'; %#ok<NASGU> used in eval
    greenPositions(j:end,:) = []; %#ok<NASGU> used in eval
else
    %check if this is the first cell to place
    if isfield(CommonHandles.TrainingSet,'RegPos')
        mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
        indicesInThisClass = find(mergedClassVectors(:,idx));
        if ~isempty(indicesInThisClass)
            %when placing new object it does not have position
            if indicesInThisClass(end)>length(CommonHandles.TrainingSet.RegPos)
                indicesInThisClass(end) = [];
            end
            %fill out array to plot
            greenPositions = cell2mat(CommonHandles.TrainingSet.RegPos(indicesInThisClass)');        

            if ~isempty(CommonHandles.RegressionPlane{idx}.selectedObject) && ~CommonHandles.RegressionPlane{idx}.newObject %&& length(CommonHandles.TrainingSet.RegPos)>=max(CommonHandles.RegressionPlane{idx}.selectedObject)                
                redPositions = zeros(length(CommonHandles.RegressionPlane{idx}.selectedObject),2);
                for i = 1:length(CommonHandles.RegressionPlane{idx}.selectedObject)                    
                    redPositions(i,:) = CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject(i)}';
                end
            end
        end
        
        %If requested then error vectors on the training set
         if strcmp(get(handles.toolbarRegressionPerformance,'State'),'on')
            linesX = zeros(2,length(indicesInThisClass));
            linesY = zeros(2,length(indicesInThisClass));
            for i=1:length(indicesInThisClass)
                idxInTS = indicesInThisClass(i);                
                greenPositions(i,:) = CommonHandles.TrainingSet.RegPos{idxInTS};
                linesX(:,i) = [CommonHandles.TrainingSet.RegPos{idxInTS}(1) ; CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions(1,i)];
                linesY(:,i) = [CommonHandles.TrainingSet.RegPos{idxInTS}(2) ; CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions(2,i)]; 
            end
            bluePositions = CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions'; %#ok<NASGU> used in eval
         end        
    end
end

%Check for existing given color objects
prevChild = handles.axes10.Children;
childrenArray = cell(5,3);

%WARNING! COLORS MUST BE MATCHED WITH THE ONES BELOW
childrenArray(:,1:2) = {...
        [0 1 0],'greenPositions';...
        [0 0 1],'bluePositions';...
        [1 0 0],'redPositions';...
        [.95 .95 .95],'';...
        [1 1 1],'' ...
    };

for i=1:size(childrenArray,1)
    for j=1:length(prevChild)
        if all(prevChild(j).Color == childrenArray{i,1})
            childrenArray{i,3}{end+1} = prevChild(j);
        end
    end
end
tol = 10^-6;

%NOTE: in green, red and blue cases there is only 1 single element to be found
for i=1:3    
    if exist(childrenArray{i,2},'var') && eval(['~isempty(' childrenArray{i,2} ')'])
        if ~isempty(childrenArray{i,3})
            if ~eval(['length(childrenArray{i,3}{1}.XData) == size(' childrenArray{i,2} ',1) && length(childrenArray{i,3}{1}.YData) == size(' childrenArray{i,2} ',1) && '  ...
                    'all(abs(childrenArray{i,3}{1}.XData*1000 - ' childrenArray{i,2} '(:,1)'')<tol) && all(abs(childrenArray{i,3}{1}.YData*1000 - ' childrenArray{i,2} '(:,2)'')<tol)'])
                for jj=1:length(childrenArray{i,3}), delete(childrenArray{i,3}{jj}); end
                eval(['plot(handles.axes10,' childrenArray{i,2} '(:,1)/1000,' childrenArray{i,2} '(:,2)/1000,''*'',''Color'',childrenArray{i,1});']);
            end
        else
            eval(['plot(handles.axes10,' childrenArray{i,2} '(:,1)/1000,' childrenArray{i,2} '(:,2)/1000,''*'',''Color'',childrenArray{i,1});']);
        end
    else
        if ~isempty(childrenArray{i,3})
            for jj=1:length(childrenArray{i,3}), delete(childrenArray{i,3}{jj}); end
        end
    end
end

%This cannot change dynamically, it's always either on or off.
currIdx = 4;
if exist('linesX','var') && ~isempty(linesX) && exist('linesY','var') && ~isempty(linesY) % We need to draw
    if ~isempty(childrenArray{currIdx,3}) %If there sg already there        
        %For simplicity only basic tests are carried out                
        %The condition is for matching, and it is negated so, it's true if
        %it's not the same
        if ~(length(childrenArray{currIdx,3}) == size(linesX,2) && length(childrenArray{currIdx,3}) == size(linesY,2) && ...
            all(all(abs(cell2mat(cellfun(@get,childrenArray{currIdx,3},repmat({'XData'},1,length(childrenArray{currIdx,3})),'UniformOutput',false)')*1000 - linesX(:,end:-1:1)')<tol)) && ...
            all(all(abs(cell2mat(cellfun(@get,childrenArray{currIdx,3},repmat({'YData'},1,length(childrenArray{currIdx,3})),'UniformOutput',false)')*1000 - linesY(:,end:-1:1)')<tol)))
            %Refresh needed
            for jj=1:length(childrenArray{currIdx,3}), delete(childrenArray{currIdx,3}{jj}); end
            plot(handles.axes10,linesX/1000,linesY/1000,'Color',childrenArray{currIdx,1});
        end
    else %Otherwise fresh draw
        plot(handles.axes10,linesX/1000,linesY/1000,'Color',childrenArray{currIdx,1});
    end    
else %We don't need to draw
    if ~isempty(childrenArray{currIdx,3}) %Check for existence of previus one
        for jj=1:length(childrenArray{currIdx,3}), delete(childrenArray{currIdx,3}{jj}); end %Delete if there is
    end
end

%Also there should be only 1 single zoom view
currIdx = 5;
zoomRectX = [XBounds(1) XBounds(1) XBounds(2) XBounds(2) XBounds(1)]/1000;
zoomRectY = [YBounds(1) YBounds(2) YBounds(2) YBounds(1) YBounds(1)]/1000;
if isempty(childrenArray{currIdx,3})
    plot(handles.axes10,zoomRectX,zoomRectY,'w');
else
    if ~(all(abs(zoomRectX - childrenArray{currIdx,3}{1}.XData)<tol) && all(abs(zoomRectY - childrenArray{currIdx,3}{1}.YData)<tol))
        delete(childrenArray{currIdx,3}{1});
        plot(handles.axes10,zoomRectX,zoomRectY,'w');
    end
end      

%hold(handles.axes10,'off');
