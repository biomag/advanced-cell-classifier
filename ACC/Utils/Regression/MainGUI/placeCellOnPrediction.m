function uData = placeCellOnPrediction(uData, newPos,~,selectedObject)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     placeCellOnPrediction
%
% Function to be used for placing a cell on the plane in prediction mode.
% Used to force cells to the horizontal line in case of 1D regression
%
% INPUT:
%   uData           UserData structure of the regression planne
%   newPos          The position to which the cell is predicted
%   handles         handles to the properties of the regressionPlane GUI
%   selectedObject  The index of the current object
%
% OUTPUT:
%   uData           The updated UserData structure
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

idx = uData.classIndex;
global CommonHandles;
if CommonHandles.RegressionPlane{idx}.horizontal1DRegression
    regressionHalf = 500;
    uData.predictedPositions(:,selectedObject) = [newPos(1); regressionHalf];
else
    uData.predictedPositions(:,selectedObject) = [newPos(1); newPos(2)];
end


