function saveCellInPrediction(uData,selectedObject,newPos,handles)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     saveCellInPrediction
%
% This function is used to add easily a cell from prediction mode to the training set.
% It essentially places the selected object (cell) into the training set
% structure
%
% INPUT:
%   uData           UserData structure of the regression plane
%   selectedObject  Index of the selected object in the current prediction
%   newPos          The predicted position of this selected object
%   handles         Handles to the graphical objects of the regressionPlane
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;
idxInTS = isInTrainingSet( uData.predictedIcons{selectedObject}.ImageName, uData.predictedIcons{selectedObject}.CellNumber, 0, CommonHandles,uData.features(selectedObject,:));
if idxInTS
    %CommonHandles.TrainingSet.RegPos{idxInTS} = [newPos(1) newPos(2)];
    placeCellOnRegPlane(newPos,handles,idxInTS);
    uData = placeCellOnPrediction(uData,newPos,handles,selectedObject);
    set(handles.figure1,'UserData',uData);
else
    
    %save CommonHandles info
    tempCurrentCellInfo.CellNumber = CommonHandles.SelectedCell;
    tempCurrentCellInfo.OriginalImageName = CommonHandles.SelectedOriginalImageName;
    tempCurrentCellInfo.ImageName = CommonHandles.SelectedImageName;
    tempCurrentCellInfo.PlateName = CommonHandles.SelectedPlate;
    
    %load data to CommonHandles fields
    jumpToCell(uData.predictedIcons{selectedObject},0);
    %save the new cell to the training set classification related fields
    saveClassificationData(uData.lastRegressionPlaneIdx, uData.predictedIcons{selectedObject}.Features);
    %add the image to ClassImages
    addToClassImagesByObject(uData.predictedIcons{selectedObject});
    %add the new regressionPosition
    placeCellOnRegPlane(newPos,handles,length(CommonHandles.TrainingSet.RegPos));
    uData = placeCellOnPrediction(uData,newPos,handles,selectedObject);
    %CommonHandles.TrainingSet.RegPos{end} = [newPos(1) newPos(2)];
    %uData.predictedPositions(:,selectedObject) = [newPos(1); newPos(2)];
    set(handles.figure1,'UserData',uData);
    %load back the CommonHandles info
    jumpToCell(tempCurrentCellInfo,0);
    %refresh class icons and labels
    refreshClassesList();
        
end
