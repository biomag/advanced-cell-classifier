function [centerX,centerY] = regPlane2GUI(centerX,centerY,XBounds,YBounds,regPlaneBoundaries)
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     regPlane2GUI
%
% Transforms the regressionPlane coordinate to the image coordinate system
%
% INPUT:
%   centerX         The X coordinate to be transformed
%   centerY         The Y coordinate to be transformed
%   XBounds         The current bounds of the regression plane in X (in reg plane reference)
%   YBounds         The current bounds of the regression plane in Y (in reg plane reference)
%   regPlaneBoundaries The position property of the regression plane axes
%
% OUTPUT:
%   centerX         The transformed X coordinate
%   centerY         The transformed Y coordinate
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    
%calculating the relative position to the displayed part of the regPlan     
distFromLowerXInRegPlane = centerX - XBounds(1);
distFromLowerYInRegPlane = centerY - YBounds(1);

%Transformation of the coordinates to the zoomed in space.
centerX = regPlaneBoundaries(3)*distFromLowerXInRegPlane/(XBounds(2)-XBounds(1));
centerY = regPlaneBoundaries(4)*distFromLowerYInRegPlane/(YBounds(2)-YBounds(1));
%Flip of Y because of different image and matrix indexing.
centerY = regPlaneBoundaries(4) - centerY;


