function refreshSelectedObjectInfoData()
% AUTHOR:   Abel Szkalisity
% DATE:     May 19, 2016
% NAME:     refreshSelectedObjectInfoData
%
% This util function refreshes the selectedObject's data like coordinates
% and cell number.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global ghandles;
uData = get(ghandles.figure1,'UserData');
idx = uData.classIndex;
global CommonHandles;
selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;

if uData.prediction == 1 && length(selectedObject)==1
    selectedInTraining = isInTrainingSet( uData.predictedIcons{selectedObject}.ImageName, uData.predictedIcons{selectedObject}.CellNumber, 0, CommonHandles,uData.features(selectedObject,:));
end

if length(selectedObject) ~= 1
    set(ghandles.selectedCellText,'String', '');
    set(ghandles.selectedImageText,'String', '');
    set(ghandles.editXCoord,'String', 'NaN');                
    set(ghandles.editYCoord,'String', 'NaN');                
elseif (uData.prediction==1 && (~selectedInTraining || find(CommonHandles.TrainingSet.Class{selectedInTraining})~=idx)) %in prediction mode if the selected is not in the training set, or in the training set but in a wrong class    
    onlyImageName = strsplit(uData.predictedIcons{selectedObject}.ImageName,{filesep,'.'});
    onlyImageName = onlyImageName{end-1};        
    set(ghandles.selectedCellText,'String',num2str(uData.predictedIcons{selectedObject}.CellNumber,'%2d'));
    set(ghandles.selectedImageText,'String',onlyImageName);
    set(ghandles.editXCoord,'String',num2str(uData.predictedPositions(1,selectedObject)/1000));
    set(ghandles.editYCoord,'String',num2str(uData.predictedPositions(2,selectedObject)/1000));
elseif (uData.prediction == 2) % in plot of plots mode
    set(ghandles.selectedImageText,'String',uData.predictedIcons{selectedObject}.drugInfo);
    set(ghandles.selectedCellText,'String','');
    set(ghandles.editXCoord,'String',num2str(uData.predictedPositions(1,selectedObject)/1000));
    set(ghandles.editYCoord,'String',num2str(uData.predictedPositions(2,selectedObject)/1000));    
else %training set element display
    if uData.prediction==1        
        selectedObject = selectedInTraining;            
    end
    onlyImageName = strsplit(CommonHandles.TrainingSet.ImageName{selectedObject},{filesep,'.'});
    onlyImageName = onlyImageName{end-1};    
    set(ghandles.selectedCellText,'String', num2str(CommonHandles.TrainingSet.CellNumber{selectedObject},'%2d'));
    set(ghandles.selectedImageText,'String',onlyImageName);
    if ~isempty(CommonHandles.TrainingSet.RegPos{selectedObject})
        set(ghandles.editXCoord,'String', num2str(CommonHandles.TrainingSet.RegPos{selectedObject}(1)/1000));                        
        set(ghandles.editYCoord,'String', num2str(CommonHandles.TrainingSet.RegPos{selectedObject}(2)/1000));
    else
        set(ghandles.editXCoord,'String', 'NaN');                
        set(ghandles.editYCoord,'String', 'NaN');
    end
end
