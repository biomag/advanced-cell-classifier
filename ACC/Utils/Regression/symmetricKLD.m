function divergence = symmetricKLD(A,B)
% AUTHOR:   Abel Szkalisity
% DATE:     Oct 12, 2016
% NAME:     symmetricKLD
%
% Calculate the symmetric Kullback-Leibler divergence between the two
% inputs.
%
% INPUT:
%   A               A probability density observation. Make sure there
%                   isn't any 0 elements in there.
%   B               Another one with the same size
%
% OUTPUT:
%   divergence      Their symmetric KLD.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

ABmat = A.*log(A./B);
BAmat = B.*log(B./A);
DKLAB = sum(ABmat(:));
DKLBA = sum(BAmat(:));

divergence = DKLAB + DKLBA;