function [ poster ] = makePoster(classImgObj,position,posterSize)
% AUTHOR:   Abel Szkalisity
% DATE:     Sep 5, 2016
% NAME:     makePoster
%
% To make a huge 'decoration' image with no overlapping cells in it.
%
% TODO: remove DISGUSTING 'for' solutions, especially from the delete
% function
%
% The algorithm:
%   Do an initial placement by rounding the positions to the closest poster
%   grid position.
%
% INPUT:
%   classImgObj     A huge cellarray with 'class image objects', which
%                   contain the images and their identification. This is
%                   the database from which we're selecting our cells.                  
%   position        Matrix 2 by length of classImgObj. The predicted
%                   position of the image
%   posterSize      height and width of the poster. (rows and cols of the
%                   grid. The actual size will be multiplied by the small
%                   image size)
%
% OUTPUT:
%   poster          The poster image with the specified size.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

regressionPlaneSize = [1000,1000];

%posterDB cell
posterDB = cell(posterSize);
for i=1:numel(posterDB)
    posterDB{i}.idx = 0;
    posterDB{i}.dist = inf;
    posterDB{i}.img = [];
end

if ~isempty(classImgObj) && ~isempty(position)
    imSize = [101,101,3];%size(classImgObj{1}.Image);
    imSize = imSize(1:2);
    poster = uint8(zeros([(convert2RowVector(posterSize).*convert2RowVector(imSize)) 3]));
    for i=1:size(position,2)
        %transform position from regression plane coordinate system to
        %matrix (image) coordinate system
        position(:,i) = position([2,1],i);
        position(1,i) = regressionPlaneSize(1)-position(1,i);
        
        %convert to center coordinates to 0-1.
        position(:,i) = (position(:,i)'./convert2RowVector(regressionPlaneSize))';        
        position(:,i) = (position(:,i)'.*convert2RowVector(posterSize))';
        position(:,i) = position(:,i)-0.5;  
        
        p = ceil(position(:,i));
        d = sqrt(sum((p-position(:,i)).^2));
        %if the rounded position is within the grid
        if p(1)>0 && p(1)<=posterSize(1) && p(2)>0 && p(2)<=posterSize(2)
            if d < posterDB{p(1),p(2)}.dist
                posterDB{p(1),p(2)}.idx = i;
                posterDB{p(1),p(2)}.dist = d;
                posterDB{p(1),p(2)}.img = classImgObj{i}.Image;                
            end
        end            
    end
        
    
    usedIndeces = zeros(0);
    %store already placed indices
    %
    for i=1:numel(posterDB)
        if posterDB{i}.idx        
            usedIndeces = union(usedIndeces,posterDB{i}.idx);
        end
    end
    
    for i=1:size(posterDB,1)
        for j=1:size(posterDB,2)
            %if its empty
            while posterDB{i,j}.idx==0                
                
                %find the closest correct cell
                cidx = findClosest(i,j,position,posterSize,usedIndeces);                
                if cidx==0
                    disp('Could not found closest one');
                    continue;
                end;
                %determine it's box
                box = ceil(position(:,cidx));                
                %shift
                if box(1)>i
                    upDown = 1;
                else
                    upDown = -1;
                end
                if box(2)>j
                    leftRight = 1;
                else
                    leftRight = -1;
                end         
                k = i;
                l = j;
                while (k~=box(1))
                    posterDB{k,l} = posterDB{k+upDown,l};
                    k = k + upDown;                    
                end
                while (l~=box(2))
                    posterDB{k,l} = posterDB{k,l+leftRight};
                    l = l + leftRight;
                end                
                posterDB{k,l}.img = classImgObj{cidx}.Image;
                usedIndeces = union(usedIndeces,cidx);                
            end
        end
    end
    %
    
    for i=1:size(posterDB,1)
        for j=1:size(posterDB,2)
            if ~isempty(posterDB{i,j}.img)
                image = imresize(posterDB{i,j}.img,imSize);
                poster((i-1)*imSize(1)+1:i*imSize(1) , (j-1)*imSize(2)+1:j*imSize(2),:) = image(:,:,:);
            end
        end
    end
    
end

end

function idx = findClosest(i,j,position,posterSize,usedIndices)
minD = inf;
idx = 0;
    for k=1:size(position,2)
        d = sqrt(sum((position(:,k)-[i-1;j-1]).^2));
        p = ceil(position(:,k));
        if ( d < minD ) && p(1)>0 && p(1)<=posterSize(1) && p(2)>0 && p(2)<=posterSize(2) && ~ismember(k,usedIndices)
            idx = k;
            minD = d;
        end            
    end
end

