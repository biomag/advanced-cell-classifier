function [shiftVectors,shiftPicture,densityOneByOne,wellIcons] = calculateShiftVectors( results,row,col,resolution)
% AUTHOR:   Abel Szkalisity
% DATE:     Aug 04, 2016
% NAME:     calculateShiftVectors
%
% This function calculates a very simple statistic about difference
% between regression predictions from different wells. There are control
% wells to which all the other wells are compared. The comparison is based
% on the most dense region of the regression plane. (e.g. where the
% majority of the cells are located). The density calculation can be
% changed by changing the appropriate function. This function also makes
% heatmaps out of each well. The heatmaps represents the regression plane.
% If the total sum of the density estimation in a well is very small then
% for numerical reasons it is neglected. (The reason for this last one
% is that it can happen that only a few cells were predicted on the
% regression plane and also they were predicted out of bounds. In this case
% the regression plane is highly uninhabited practically completely 0. In
% this case this well cannot be compared to other wells with KLD (as it is
% completely 0), which will be done later with the KDEs.)
%
% INPUT:
%   results         A cell array sized exactly as the plate that we
%                   examine. Every cell contains a 2 by n matrix, these are
%                   the predicted positions of the specific well. From
%                   these coordinates the density is estimated.
%   row             The row indices of the control well.
%   col             The column indices of the control well.
%   resolution      The resolution for the small heatmaps plotted.
%
% OUTPUT:
%   shiftVectors    A cell array sized the same as results, and every cell
%                   contains a 4 element row vector. The first 2 element of
%                   this vector is the most dense position in the control
%                   well (start point of the shift vector), the last 2
%                   element are the most dense point within the specific
%                   well (end point)
%   shiftPicture    Cellarray with the same size as results. Each entry is the
%                   difference image between the density estimations of the
%                   current well and the weighted average of the controls.
%   densityOneByOne This output parameter contains for each well the kernel
%                   density estimations (KDE). The resolution of the KDE is
%                   the same as of the shift picture image, can be set with
%                   x1 and x2 at the start of the code.
%   wellIcons       Cellarray sized as results. Each entry is a small
%                   heatmap with size 'resolution'+2 input parameter (+2
%                   are edges).
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

[rowNum,colNum] = size(results);

%Regression plane resolution in the density estimation
x1 = linspace(0,1000,resolution);
x2 = linspace(0,1000,resolution);
%threshold for 'empty' regression plane.
emptyThreshold = 5;
shiftPicture = cell(rowNum,colNum);

allControls = cell(1,length(row));
allControlWeights = zeros(1,length(row));
for i=1:length(allControls)
    if  ~isempty(results{row(i),col(i)})
        [X1,X2] = meshgrid(x1,x2);
        dataToPlot = results{row(i),col(i)};
        if size(dataToPlot,2) == 1 %single data
            allControls{i} = densityEstimationByGaussians(dataToPlot,x1,x2);
        else
            allControls{i} = reshape(ksdensity(dataToPlot',[X1(:) X2(:)]),resolution,resolution);
            if ~any(any(allControls{i}))
                allControls{i} = densityEstimationByGaussians(dataToPlot,x1,x2);
            end
        end        
        allControlWeights(i) = size(results{row(i),col(i)},2);            
    else
        allControls{i} = zeros(resolution);
        allControlWeights(i) = 0;
    end
end
densityInControl = zeros(length(x2),length(x1));
if sum(allControlWeights)>0
    nofAllCellsInControl = sum(allControlWeights);
    for i=1:length(allControls)
        densityInControl = densityInControl + allControlWeights(i)*allControls{i}/nofAllCellsInControl;        
    end    
else
    warndlg('Your control is empty, the results might be unreliable!');
end

[~,idx] = max(densityInControl(:));
controlY = mod(idx-1,length(x2))+1;
controlX = idivide(int32(idx-1),int32(length(x2)))+1;

if (sum(allControlWeights(:))<emptyThreshold)
    warndlg('Your control is practically empty, the results might be unreliable!');
end

%the max function also starts from the upper left corner which is correct
%because the densityInControl picture is flipped so finally the controlY
%position will be in the original (Cartesian coordinate system)

shiftVectors = cell(size(results));

waitBarHandle = waitbar(0,'Density estimation...');
densityOneByOne = cell(rowNum,colNum);

wellIcons = cell(size(results));

controlWellIcons = zeros(resolution,resolution,length(row));
k = 1;

for i=1:rowNum
    for j=1:colNum        
           
        practicallyEmpty = 1;
        if ~isempty(results{i,j}) && size(results{i,j},2)>emptyThreshold
            practicallyEmpty = 0;
            
            dataToPlot = results{i,j};
            if size(dataToPlot,2) == 1
                densityInWell = densityEstimationByGaussians(dataToPlot,x1,x2);                        
            else
               [X1,X2] = meshgrid(x1,x2);            
                densityInWell = reshape(ksdensity(dataToPlot',[X1(:) X2(:)]),resolution,resolution);             
                if ~any(any(densityInWell))
                    densityInWell = densityEstimationByGaussians(dataToPlot,x1,x2);                
                end
            end            
                        
            %place the heatmap to the wellIcon
            wellIcons{i,j} = plotCurrentRegression2D(results{i,j},resolution);
            if ~isempty(intersect(find(i == row),find(j == col)))
                controlWellIcons(:,:,k) = wellIcons{i,j};
                k = k+1;
            end                               

            densityOneByOne{i,j} = densityInWell;
            [~,idx] = max(densityInWell(:));
            wellY = mod(idx-1,length(x2))+1;
            wellX = idivide(int32(idx-1),int32(length(x2)))+1;
            shiftVectors{i,j} = double([controlX-1 controlY-1 wellX-1 wellY-1]);
            shiftVectors{i,j} = shiftVectors{i,j}./(length(x1)-1);                        
        end
        if (practicallyEmpty == 1)
            %in this case the vector should not point anywhere
            densityInWell = densityInControl; %this will result in empty difference image
            shiftVectors{i,j} = double([0 0 1 1]);
            
            %place constant black color to the wellIcon if its empty.
            wellIcons{i,j} = zeros(resolution, resolution);
        end
        
        %here we flip the vertical axes to the right position!
        shiftPicture{i,j} = fliplr((densityInWell-densityInControl)')';        
       
        donePercent = ((i-1)*colNum+j)/(rowNum*colNum);
        waitText = sprintf('Smooth well predictions...  %d%% done', int16(donePercent * 100));
        if ishandle(waitBarHandle)
            waitbar(donePercent, waitBarHandle, waitText);
        end
    end
end


if ishandle(waitBarHandle)
    close(waitBarHandle);
end

end

