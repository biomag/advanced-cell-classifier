function [imgForColormap] = imgToColormap(origImg,nofBins,flip)
% AUTHOR:    Abel Szkalisity
% DATE:     July 23, 2019
% NAME:     imgToColormap
%
% Transforms the image into a binned version (quantizing) according to a
% given scale. E.g. if origImg is a 0-1 double and the nofBins is 64 then
% the pixel values will be between 1-64 in the output and all integers.
%
% INPUT:   
%   origImg         A single channel image, each pixel must be non-nagative
%   nofBins         An integer specifying the output image field
%   flip            String: none,ud,lr specifying if the origImage needs to
%                   be transformed in some way. ud stands for up-down flip,
%                   lr for left-right flip.
%   
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

switch flip            
    case 'ud'
        imgForColormap = flipud(origImg);
    case 'lr'
        imgForColormap = fliplr(origImg);
    otherwise
        imgForColormap = origImg;
end

maxV = max(imgForColormap(:));
if maxV ~= 0, imgForColormap = imgForColormap./maxV; end
imgForColormap = ceil(imgForColormap.*nofBins);

imgForColormap(imgForColormap<=0) = 1;

end

