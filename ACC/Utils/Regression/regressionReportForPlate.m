function regressionReportForPlate(plateNumber,html,thumbnail,plotOfPlots,resolution,baseFolder,idx,controls,varargin)
% AUTHOR:   Abel Szkalisity
% DATE:     July 10, 2016
% NAME:     regressionReportForPlate
%
% Te encapsulate the process of generating reports of regression. We
% suppose that the target folder is already there, and there is a trained
% classifier and regressor. Within the baseFolder
% this function makes a subfolder (named time_platename).
%
% INPUT:
%   plateNumber     The # of the plate of which we make the prediction
%   html            bool value whether we want to make html report too
%   thumbnail       bool value if we generate thumbnails in html report.
%   plotOfPlots     A structure describing what unsupervised methods we'd
%                   like to run on our data. The fields are the following:
%                       .selectedULMethods: Cellarray of strings with the
%                       following possible values:
%                       'NeRV','T-SNE','PCA','Hierarchical clustering'
%                       .wellIDs, .wellAnnotation: 2 cellarray with the
%                       wellIDs (like A01 etc.) and the corresponding
%                       annotation (e.g. drug name in that well)
%                       .iconToUse A string identifying which icon to use.
%                   An empty structure can indicate the request for not
%                   running any analysis
%   resolution      resolution of the regression plane. (to how many parts one side of the plane is divided when the distribution is calculated)
%   baseFolder      the folder name where the report will be saved.
%   idx             The index of the regression plane to use
%   controls        A structure which describes the control type.
%                   2D character array containing strings to identify the
%                   control images. (e.g. for well the wellID like A06 , for unstructured data each row is an image name)
%
%   OPTIONAL NAME-VALUE pairs:
%
%   waitbarInfo     A structure with 2 fields:
%                       .h  Handle to an existing waitbar
%                       .progressMeter A vector with 2 values each between
%                       0 and 1. The proportion of the waitbar to be used
%                       in this call of the function
%
%
% OUTPUT:
%   The files will be written to the projectFolder [(CommonHandles.DirName)
%   filesep basefolder].
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

p = inputParser();
p.addParameter('waitbarInfo',[]);
p.parse(varargin{:});
if ~isempty(p.Results.waitbarInfo)
    waitBarHandle = p.Results.waitbarInfo.h;
else
    waitBarHandle = [];
end

tgtFolder = [CommonHandles.DirName filesep baseFolder filesep CommonHandles.PlatesNames{plateNumber}];
tmpFolder = [tgtFolder filesep 'temp'];
mkdir(tgtFolder);
mkdir(tmpFolder);

createRegressionPredictorMetaData(tgtFolder,idx,controls);

%open csv file
%do CSVs.
singleCellFID = fopen([tgtFolder filesep CommonHandles.PlatesNames{plateNumber} '_singleCellRegressionPositions.csv'],'w');

colmax = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;
rowmaxnum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;

%control IDs, all indices (numbers)
controlRows = zeros(1,length(controls));
controlCols = zeros(1,length(controls));

%unstructured data handling
if colmax > -1
    %controlRows for plate data    
    for i=1:length(controls)
        [controlRows(i),controlCols(i)] = wellId2Index(controls{i});
    end            
    structuredData = 1;
else
    %a bit complicated process to identify the images in the project
    ImageFiles = dir(fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{plateNumber},CommonHandles.ImageFolder,['*' CommonHandles.ImageExtension]));
    allMetaDataFiles = cell(size(ImageFiles,1),1);
    for i=1:size(ImageFiles,1)
        [~,imgExEx,~] = fileparts(ImageFiles(i).name);
        matchingMetaDataFile = dir(fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{plateNumber},CommonHandles.MetaDataFolder,['*' imgExEx '*']));
        allMetaDataFiles{i} = matchingMetaDataFile(1).name;
    end    
    [rowmaxnum,colmax] = bestMatrixSizeForElementNumber(length(allMetaDataFiles));
    for i=1:length(controls)
        [~,controlExex,~] = fileparts(controls{i});        
        controlIndex = find(contains(allMetaDataFiles,controlExex),1);
        controlRows(i) = floor((controlIndex-1)/colmax)+1;
        controlCols(i) = mod((controlIndex-1),colmax)+1;
    end    
    structuredData = 0;
end
%generate a platelayout if the user doesn't have it
generatedPlateLayoutFID = fopen([tmpFolder filesep CommonHandles.PlatesNames{plateNumber} '_generatedPlateLayout.csv'],'w');
fprintf(generatedPlateLayoutFID,'%s,%s\n','WellID','Annotation');

rowmax = char(rowmaxnum+'A'-1);
            
%for the waitbar
wellcount = colmax*rowmaxnum;
counter = 0;

results = cell(rowmaxnum,colmax);    
    
%header for csv    
fprintf(singleCellFID, 'PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, regPosX, regPosY\n');
    
%iterate through every well (or image in case of unstructured data)
for row ='A':rowmax
    for col=1:colmax    
        counter = counter+1;
        rownum = row - 'A' + 1;
        % create base file names
        colString = sprintf('%02d', col);
        if structuredData
            FileNameRegEx = [CommonHandles.PlatesNames{plateNumber} '*_w' row colString '*'];
            FileNamePrefix = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{plateNumber},CommonHandles.MetaDataFolder,FileNameRegEx);
        else            
            if length(allMetaDataFiles)>=counter %if there is still images to be placed
                FileNameRegEx = allMetaDataFiles{counter}; 
                FileNamePrefix = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{plateNumber},CommonHandles.MetaDataFolder,FileNameRegEx);                
            else
                FileNameRegEx = '';
                FileNamePrefix = ''; %empty string nothing to search for
            end            
        end    
                
        MetadataFiles = dir(FileNamePrefix);
        
        if ~isempty(MetadataFiles) %write out to the generated file only if there are corresponding images
            fprintf(generatedPlateLayoutFID,'%s,%s\n',[row num2str(col,'%02d')],strrep(FileNameRegEx,'*',''));
        end

        predForWell = cell(1,size(MetadataFiles,1));

        for i=1:size(MetadataFiles,1)
            [~, fileNameexEx, ~] = fileparts(MetadataFiles(i).name);
            %read in features
            [~,featuresFull] = readMetaData(CommonHandles, fileNameexEx, plateNumber);
            features = featuresFull(:,3:end);
            if length(CommonHandles.Classes) > 1
                %do classification
                [ out, ~, ~ ] = predictClassifier( features , CommonHandles);
                %select cells into regression
                features = features(out==idx,:);
                regressionCellsIdx = find(out==idx);
            else
                regressionCellsIdx = 1:size(features,1);
            end
            %check for regression cells' existnace
            if ~isempty(features)
                predForWell{i} = CommonHandles.RegressionPlane{idx}.Predictor.predict(features');
            end

            for cellIdx = 1:size(predForWell{i},2)
                %Printing out to file PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class
                fprintf(singleCellFID, '%s, %s, %d, %s, %d, %d, %f, %f, %f, %f\n', ...
                    char(CommonHandles.PlatesNames(plateNumber)), ...
                    row, ...
                    col, ...
                    fileNameexEx,...
                    i, ...
                    regressionCellsIdx(cellIdx), ...
                    featuresFull(regressionCellsIdx(cellIdx), 1),...
                    featuresFull(regressionCellsIdx(cellIdx), 2),...
                    predForWell{i}(1,cellIdx)/1000,...
                    predForWell{i}(2,cellIdx)/1000 ...
                );
            end

        end

        results{rownum,col} = cell2mat(predForWell);

        if ~isempty(waitBarHandle) && ishandle(waitBarHandle)
            pM = p.Results.waitbarInfo.progressMeter;
            donePercent = counter./wellcount;
            donePercent = pM(1) + donePercent*(pM(2)-pM(1));
            waitText = sprintf('Perform regression [%d/%d]...  %d%% done',p.Results.waitbarInfo.counter,p.Results.waitbarInfo.allPlateNumber, int16(donePercent * 100));
            waitbar(donePercent, waitBarHandle, waitText);
        end
    end
end

%close csv files   
fclose(singleCellFID);    
fclose(generatedPlateLayoutFID);
                            
    
    %here calculate the change vectors            
    [shiftVectors,shiftPicture,densityOneByOne,wellIcons] = calculateShiftVectors(results,controlRows,controlCols,resolution);
    try
        createRegressionPDFReport(results, CommonHandles.PlatesNames{plateNumber},tgtFolder,controlRows,controlCols,resolution,shiftVectors,densityOneByOne,shiftPicture,idx,wellIcons);
    catch e
        if strcmp(e.identifier, 'MATLAB:savefig:InvalidHandle')
            errordlg('PDFReport figure has been closed before saving process')
            return
        else
            rethrow(e)
        end
    end
    if html
        createRegressionHTML(results', CommonHandles.PlatesNames{plateNumber}, tgtFolder ,thumbnail,wellIcons);        
    end        
    
    %save results as mat file
    save([tmpFolder filesep 'regressionsByPlate_class' num2str(idx,'%02d') '.mat'],'results','shiftVectors','wellIcons','densityOneByOne','controls');
    
    if ~isempty(plotOfPlots)
        try
            plotOfPlotsDREDVIZ( wellIcons, densityOneByOne, tgtFolder,tmpFolder, plateNumber,idx,controlRows,controlCols,plotOfPlots);
        catch e
            if any(strcmp(e.identifier, {'MATLAB:savefig:InvalidHandle',...
                                         'MATLAB:class:InvalidHandle',...
                                         'MATLAB:print:HandleArgs'}))
                errordlg('Clustergram figure has been closed before saving process')
                return
            elseif strcmp(e.identifier,'ACC:Density')
                errordlg(e.message);
                return;
            else
                rethrow(e)
            end
        end
    end
                
end


    