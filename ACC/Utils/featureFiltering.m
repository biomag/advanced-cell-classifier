function featureFiltering(projectPath, plateIDs, metaDataFolder, metaDataFileName)

featureNames = textread(fullfile(projectPath,plateIDs{1},metaDataFolder,metaDataFileName), '%s', 'delimiter', '\n');

idx = listdlg('ListString', featureNames(3:end),... % the first 2 values are the coordinates of the cell
                'SelectionMode','multiple',...
                'Name','Feature Selection',...
                'OKString','Apply Filter',...
                'PromptString','Select all the features to keep:');

idx = [1 2 idx+2]; % shift the idx by 2, because of the x & y coordinates
            
subfolder = metaDataFolder;
for i = 1:length(plateIDs)
    tempSubFolders = dir(fullfile(projectPath,plateIDs{i}));
    tempSubFolders = tempSubFolders(3:end);
    subfolders = unique({subfolder, tempSubFolders([tempSubFolders.isdir]).name});
end
            
answer = Settings_GUI({...
                struct('name', 'Metadata Folder', 'type', 'str', 'default', 'anal2_new', 'strSpecs', struct('forbidden', {subfolders}, 'capital', [], 'number', [], 'glyph', [], 'space', 0)),...
                },...
                'infoText', 'Enter the name of the new metadata folder!', 'title', 'New folder');

wb = waitbar(0, sprintf('Calculating new feature files... \n Please wait! \n %d / %d Plates', 0, length(plateIDs)));
for i = 1:length(plateIDs)
    origFeatureFileList = dir(fullfile(projectPath,plateIDs{i},metaDataFolder, '*.txt'));
    newPath = fullfile(projectPath,plateIDs{i},answer);
    mkdir(newPath{1});
    if ishandle(wb)
        waitbar(0, wb, sprintf('Calculating new feature files... \n Please wait! \n %d / %d Plates', i, length(plateIDs)));
    else
        wb = waitbar(0, sprintf('Calculating new feature files... \n Please wait! \n %d / %d Plates', i, length(plateIDs)));
    end
    for j = 1:length(origFeatureFileList)
        origMetaData = load(fullfile(origFeatureFileList(j).folder,origFeatureFileList(j).name));
        filteredMetaData = origMetaData(:,idx);
        newPathAndName = fullfile(newPath, origFeatureFileList(j).name);
        csvwrite(newPathAndName{1},filteredMetaData)
        if ishandle(wb)
            waitbar(j/length(origFeatureFileList), wb, sprintf('Calculating new feature files... \n Please wait! \n %d / %d Plates', i, length(plateIDs)));
        else
            wb = waitbar(j/length(origFeatureFileList), sprintf('Calculating new feature files... \n Please wait! \n %d / %d Plates', i, length(plateIDs)));
        end
    end
    newFeatureNames = featureNames(idx);
    featureNamesTable = table(newFeatureNames);
    writetable(featureNamesTable, fullfile(newPath{1}, 'featureNames.acc'), 'FileType', 'text', 'WriteVariableNames', 0);
end

if ishandle(wb), close(wb), end