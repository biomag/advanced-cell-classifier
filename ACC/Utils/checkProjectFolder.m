function [ errorCode, dirStr ] = checkProjectFolder(dirName,newData,segmentedImageName,featureName,originalImageName)
% AUTHOR:   Abel Szkalisity
% DATE:     May 23, 2016
% NAME:     checkProjectFolder
%
% Checks the structure of the loaded project. The base folder of the
% project is provided in dirName. In that there are folders
% with the plates. It does not necessary that all the direct subfolders of
% the base folder are plate folders (i.e. they fulfil the conditions of the
% plate folder described later), but there must be at least one subfolder
% which is a valid plate folder. 
%
% A plate folder contains at least 3 subfolders for the original images,
% features and segmented images. The name should be the 
% same as provided in the inputs. The function can be used to validate the
% path for new data set, but also to validate a new path for old data. The
% function outputs the first error code which break the rules.
% 
% For new data set the checks are done
%
% INPUT:
%   dirName             the examined base project folder
%   newData             a bool value indicating that the check is needed
%                       for new data or old one. The difference is that in
%                       new data we do not check for the project's plates
%   segmentedImageName  folder name for the segmented images
%   featureName         folder name for the features
%   originalImageName   folder name for the original images
%
%   Additionally global CommonHandles is used as input.
%
% OUTPUT:
%   errorCode           A value indicating the correctness of
%                       CommonHandles folder. Interpretation of it:
%                       0: the given base folder is correct
%                       1: the given base folder does not contain any
%                       folders (plates)
%                       2: none of the base folder's subfolders is a valid
%                       plate folder: they do not contain the specified
%                       image, feature etc. folders. 
%                       3: Only with old data: the given base folder does
%                       not contain the plates listed in the project
%                       4: The dirName is not a valid directory on this
%                       computer.
%   dirStr              Cellarray of string: the directories inside the
%                       dirName folder (plates)
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

global CommonHandles;

if exist(dirName,'dir')
    
    %list all files in the folder
    DirList = dir(dirName);
    %get only the names
    listStr = {DirList.name};
    %filter for the folders    
    DirStrWithDots = listStr(vertcat(DirList.isdir));    
    %filter the actual (.) and parent (..) folder    
    dirStr = cell(1,length(DirStrWithDots));
    j = 1;
    for i=1:length(DirStrWithDots)
        if ~strcmp(DirStrWithDots{i},'.') && ~strcmp(DirStrWithDots{i},'..')
            dirStr{j} = DirStrWithDots{i};
            j = j+1;
        end
    end    
    dirStr(j:end) = [];
    
    errorCode = 0;
    
    if isempty(dirStr)
        %there isn't any folders within the project (no plates)
        errorCode = 1;
        return;
    else
        %for all the folders within dirName
        i = 1;
        while i<=length(dirStr)        
            %check if the plate contains the right named subfolders
            if ~( exist([dirName filesep dirStr{i} filesep segmentedImageName],'dir') && exist([dirName filesep dirStr{i} filesep featureName],'dir') && exist([dirName filesep dirStr{i} filesep originalImageName],'dir') )
                dirStr(i) = []; %delete the non valid entries from the dirStr.                
            else
                i = i+1;
            end
        end
        %if there isn't any valid plate folder than report error. Otherwise
        %accept the folder.
        if isempty(dirStr)
           errorCode = 2;
           return; 
        end        
    end
    
    if ~newData
        for j=1:size(CommonHandles.PlatesNames,1)
            if ~ismember(CommonHandles.PlatesNames{j},dirStr)
                errorCode = 3;
                return;
            end
        end
    end
else
    errorCode = 4;
end

end

