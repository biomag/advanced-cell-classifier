%TODO: this file should be extended with: training data (number and actual cells also!)

metaDataF = fopen(fullfile(outputFolder,'platePredictionMetaData.txt'),'w');
if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')   
    classifierString = 'OneClassClassifier';
else
    classifierString = CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier};
end
fprintf(metaDataF,'Classifier used was: %s\n',classifierString);
fprintf(metaDataF,'\nClass names were:\n');

for iiiii=1:length(CommonHandles.SALT.trainingData.classNames)
    fprintf(metaDataF,'%s\n',CommonHandles.SALT.trainingData.classNames{iiiii});
end

fclose(metaDataF);
