function commonHandlesVersionControl(runningCommonHandles, handles) %#ok<INUSD> Its OK, because we use handles with evals.
% AUTHOR:	Abel Szkalisity
% DATE: 	April 22, 2016
% NAME: 	commonHandlesVersionControl
% 
% When we load a previously saved dataset we must do some transformations
% to make the old CommonHandles appropriate for the running the current
% version of ACC. This also where we handle operating system related
% compatibility issues such as filesep change in CommonHandles fields.
%
% In every new release of ACC this file must be updated.
%
% INPUT:
%   runningCommonHandles    The info of the running system can be
%                           extracted from this parameter.
%
% OUTPUT:
%   The variable "global CommonHandles" become updated to allow the usage
%   of the current version of ACC with old saved projects.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

    global CommonHandles;
    
    if (~isfield(CommonHandles,'VersionInfo'))
        CommonHandles.VersionInfo = 1.3;
    end      
        
    %Delete all GUI objects reamining from the previous version (in CommonHandles)
    closeSubGUI(runningCommonHandles);
    
    
    %Here we collect information to fill out checklist correctly
    if (isfield(CommonHandles,'DirName') && isfield(CommonHandles,'PlatesNames') && isfield(CommonHandles,'MetaDataFolder') && isfield(CommonHandles,'ImageFolder'))
        if isempty(dir([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.MetaDataFolder filesep '*.h5']))
            DataType = 1; %txt data
        else
            DataType = 2; %h5 data
        end        
        splittedDirectoryName = strsplit(CommonHandles.DirName,{'/','\'});         %#ok<NASGU> Because we use it in eval, and in string MATLAB cannot find it.        
        filelist = dir([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.ImageFolder filesep '*' CommonHandles.ImageExtension]);
        if length(filelist)<=2
            errordlg(['No images in the specified folder: ' CommonHandles.DirName]);
        else
            splittedImageName = strsplit(filelist(3).name,'.');
            imageExtension = splittedImageName{end};
        end                        
    else
        DataType = 1; % default data type.
        splittedDirectoryName{1} = 'default HCS project'; %#ok<NASGU> Because we use it in eval, and in string MATLAB cannot find it.
        imageExtension = '';
    end
    
    plateTypeInfo = runningCommonHandles.PlateTypeInfo; %#ok<NASGU> Used in eval below.    
    imageExtensions = runningCommonHandles.ImageExtensionNames;  %#ok<NASGU> Used in eval below.
    if isfieldRecursive(runningCommonHandles,'SALT.folderName')
        saltFolderName = runningCommonHandles.SALT.folderName; %#ok<NASGU> Used in eval.
    else
        saltFolderName = []; %#ok<NASGU> Used in eval
    end

    
    
    %checkList: a cellarray of fieldnames and default values if the field
    %doesn't exist
    checkList = {
        {'ACCPATH','runningCommonHandles.ACCPATH'},...
        {'ActiveLearning','0'},...
        {'ActiveRegression','struct(''on'',0,''selectedClass'','''')'},...
        {'AllCoordinates','{}'},...
        {'AllFeatures','{}'},...
        {'AllFeaturesMapping','{}'},...
        {'Classes','{}'},...
        {'ClassImageInfo','struct(''ImageSize'',[100 100],''SepSize'',10,''Cols'',6)'},...
        {'ClassImages','containers.Map'},...
        {'ClassTextFields','{}'},...        
        {'ColorView','1'},...
        {'Counter','0'},...
        {'CurrentImageSizeX','1'},...
        {'CurrentImageSizeY','1'},...
        {'CurrentPrediction','{}'},...
        {'CutSize','50'},...
        {'DataStructureType',num2str(DataType)},...
        {'DisplayAnnotatedCells','0'},...        
        {'HC.Default.NumberOfOutliers','100'},...
        {'HC.Default.SupportInfoIG','15'},...
        {'HC.Default.TrainParams',' ''-s 2 -t 2 -e 0.000001 -n 0.0001 -g 0.008'' '},...
        {'HC.Default.Method',' ''average'' '},...
        {'HC.Default.Metric',' ''cosine'' '},...
        {'HC.Node','{}'},...
        {'HC.Node.createClass','0'},...
        {'ImageExtension',['''.' imageExtension '''']},...
        {'ImageExtensionNames','imageExtensions'},...
        {'MainView','struct(''Blue'',1,''Red'',1,''Green'',1,''StrechImage'',0,''StrechMin'',0,''StrechMax'',255,''StrechMinR'',0,''StrechMaxR'',255,''StrechMinG'',0,''StrechMaxG'',255,''StrechMinB'',0,''StrechMaxB'',255,''SwapMap'',1)'},...
        {'MainView.XBounds','[]'},...
        {'MainView.YBounds','[]'},...
        {'MatlabVersion','version'},...        
        {'ProjectName','splittedDirectoryName{end}'},...
        {'PlateTypeInfo','plateTypeInfo'},...        
        {'RandomTrain','0'},...              
        {'RegressionPlane','[]'},...
        {'RegressionPlaneHandle','[]'},...
        {'Report.ClassifiedPlates','[]'},...
        {'SamplingRatioForAllFeatures','1'},...
        {'SelectedCell','1'},...
        {'SelectedClassifier','1'},...
        {'SelectedImage','1'},...
        {'SelectedPlate','1'},...
        {'ShowCellNumbers','1'},...        
        {'ShowContour','1'},...
        {'ShowTrainedCellsFig','[]'},...
        {'Success','0'},...
        {'TrainingTime','0'},...
        {'TrainingSet','struct(''CellNumber'',[],''Class'',[],''BloomFilter'',''bloomFilter()'',''ImageName'',[],''Features'',[],''MetaDataFilename'',[],''OriginalImageName'',[],''RegPos'',[],''Timestamp'',[])'},...
        {'UploadCAMIOFigure','[]'}...
        };          
        
        
    %resetList: elements in this list are not checked whether they're in
    %the structure already but instead completly overwritten by the new
    %value.    
    resetList = {...        
        {'ALBatched','struct(''batchSize'',5,''currentCellInBatch'',1,''data'',[],''usePCT'',1)'},...
        {'CAMI.Network','struct(''Host'',''127.0.0.1'',''Port'',''8080'',''Path'',''camio-webapp'',''UploadServiceIsWorking'',0)'},...
        {'CAMI.PhysicalExperiment','struct(''NucleiDir'',''anal4'',''CellsDir'',''anal5'',''SelectedUploadDir'',''CellsDir'',''DescriptorXML'',''screenDescriptor.xml'',''MarkerXML'',''markerXML.xml'',''MarkerImage'',''r01c01f01p01-ch1sk1fk1fl1.tiff'')'},...
        {'Classbuttons','{}'},... 
        {'ClassifierNames','sacGetSupportedList(''classifiers'')'},...
        {'ClassTypes','{''Normal'',''Child'',''Regression''}'},...
        {'DragImage','struct(''State'',0,''OriginalPosition'',[0,0])'},...
        {'HC.ShowCellsFig','[]'},...
        {'ImageListHandle','[]'},...
        {'MainViewHandle','handles.mainView'},...
        {'MainWindow','handles.figure1'},...
        {'PlateSelectWindow','[]'},...
        {'PlateListHandle','[]'},...
        {'PredictRegressionGUI','[]'},...
        {'SALT.initialized','runningCommonHandles.SALT.initialized'},...
        {'SALT.folderName','saltFolderName'},...
        {'ShowHighProbCellsHandle','[]'},...
        {'ShowSimilarCellsHandle','[]'},...
        {'SmallColorViewHandle','handles.windowMerged'},...
        {'SmallRedViewHandle','handles.windowRed'},...
        {'SmallGreenViewHandle','handles.windowGreen'},...
        {'SmallBlueViewHandle','handles.windowBlue'}...        
        {'UploadCAMIBookFigure','[]'}...
        };                    
    
    %if there is an extension to a field then it should go from checkList
    %to reset list. These checks are runned after the total checks.    
    resetList14 = {
        {'MainView','struct(''Blue'',1,''Red'',1,''Green'',1,''StrechImage'',1,''StrechMin'',0,''StrechMax'',255,''StrechMinR'',0,''StrechMaxR'',255,''StrechMinG'',0,''StrechMaxG'',255,''StrechMinB'',0,''StrechMaxB'',255,''SwapMap'',1)'}...
        };
    resetList141 = {
        {'ClassTypes','{''Normal'',''Child'',''Regression''}'}...
        };
    checkList20 = {
        {'TrainingSet.Timestamp','cell(size(CommonHandles.TrainingSet.Class))'},...
        {'TrainingSet.RegPos','cell(size(CommonHandles.TrainingSet.Class,2),size(CommonHandles.TrainingSet.Class,1))'},...
        };
%     Note: Version 2.12 has changed the size of the bloom filter, so even
%     though bloom filter was available already in version 2.11 it needs to
%     be recalculated also in those cases.
%     checkList211 = {
%         {'TrainingSet.BloomFilter','bloomFilter()'}...
%         };
    checkList212 = {
        {'TrainingSet.BloomFilter','bloomFilter()'}...
        };
    
    %TOTAL CHECKS:    
    for i=1:length(checkList)
        if ~isfieldRecursive(CommonHandles,checkList{i}{1})
            eval(['CommonHandles.' , checkList{i}{1} , '=', checkList{i}{2} ';']);
        end
    end
    for i=1:length(resetList)
        eval(['CommonHandles.' , resetList{i}{1} , '=', resetList{i}{2} ';']);
    end
    
    %VERSION DEPENDENT CHECKS:
    %Compatibility between ACC versions.
    if (CommonHandles.VersionInfo<1.4)        
        for i=1:length(resetList14)
            eval(['CommonHandles.' , resetList14{i}{1} , '=', resetList14{i}{2} ';']);
        end
    end
    if (CommonHandles.VersionInfo<1.41)
        for i=1:length(resetList141)
            eval(['CommonHandles.' , resetList141{i}{1} , '=', resetList141{i}{2} ';']);
        end        
    end
    if (CommonHandles.VersionInfo<2.0)
        for i=1:length(checkList20)
            if ~isfieldRecursive(CommonHandles,checkList20{i}{1})
                eval(['CommonHandles.' , checkList20{i}{1} , '=', checkList20{i}{2} ';']);
            end
        end        
    end
    if (CommonHandles.VersionInfo<2.12)
        for i=1:length(checkList212)
%             if ~isfieldRecursive(CommonHandles,checkList212{i}{1})
                eval(['CommonHandles.' , checkList212{i}{1} , '=', checkList212{i}{2} ';']);
%             end
        end        
    end
    
    %Remove burnt in data path
    if (length(CommonHandles.TrainingSet.Class )>=1)        
        %TODO: reorganize this code
        if (length(strsplit(CommonHandles.TrainingSet.MetaDataFilename{1},{'\','/'})) >= 3 )
            for i=1:length(CommonHandles.TrainingSet.MetaDataFilename) 
                SIN = strsplit(CommonHandles.TrainingSet.MetaDataFilename{i},{'\','/'});
                CommonHandles.TrainingSet.MetaDataFilename{i} = [SIN{end-2} filesep SIN{end-1} filesep SIN{end}];
            end        
        end
        if (length(strsplit(CommonHandles.TrainingSet.ImageName{1},{'\','/'})) >= 3 )
            for i=1:length(CommonHandles.TrainingSet.ImageName) 
                SIN = strsplit(CommonHandles.TrainingSet.ImageName{i},{'\','/'});
                CommonHandles.TrainingSet.ImageName{i} = [SIN{end-2} filesep SIN{end-1} filesep SIN{end}];
            end        
        end
        if (length(strsplit(CommonHandles.TrainingSet.OriginalImageName{1},{'\','/'})) >= 3 )
            for i=1:length(CommonHandles.TrainingSet.OriginalImageName) 
                SIN = strsplit(CommonHandles.TrainingSet.OriginalImageName{i},{'\','/'});                
                CommonHandles.TrainingSet.OriginalImageName{i} = [SIN{end-2} filesep SIN{end-1} filesep SIN{end}];                
            end        
        end

        if (isempty(CommonHandles.Classes))
            recreateClasses();
        end
        if (isempty(CommonHandles.ClassImages))
            recreateClassImages();
        else
            % the empty classes are not listed as key-value pair
            tempKeys = CommonHandles.ClassImages.keys;
            % if it is listed, than it must contain at least one value
            tempStruct = CommonHandles.ClassImages(tempKeys{1}).Images{1};
            tempFields = fieldnames(tempStruct);
            % we need at least 6 variables for each cell image in the class
            if size(tempFields,1) < 6
                recreateClassImages();
            else
                CommonHandles = resetOSInClassImgs(CommonHandles);
            end
        end
    end
    
    
    %Change filesep to match the current operating system
    matchOSfilesep;
    
    %recreate bloom filter
    recreateBloomFilter();
    
    recreateRegression();
    
    %Check for duplication clearance
    clearDuplicationsFromTrainingSet();
    
    runningACCVersion = runningCommonHandles.VersionInfo;
    CommonHandles.VersionInfo = runningACCVersion; 
end