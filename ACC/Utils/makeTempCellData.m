function [ tempCurrentCellInfo ] = makeTempCellData( )
% AUTHOR:   Abel Szkalisity
% DATE:     Sep 12, 2016
% NAME:     makeTempCellData
%
% Saves all current selected cell data to a struct which can be used later
% on as input to jumpToCell.
%
% OUTPUT:
%   tempCurrentCellInfo     Struct with the following fields:
%               - CellNumber
%               - ImageName
%               - OriginalImageName
%               - PlateName (which is the number in fact)
%   
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academy of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.


global CommonHandles;

tempCurrentCellInfo.CellNumber = CommonHandles.SelectedCell;
tempCurrentCellInfo.OriginalImageName = CommonHandles.SelectedOriginalImageName;
tempCurrentCellInfo.ImageName = CommonHandles.SelectedImageName;
tempCurrentCellInfo.PlateName = CommonHandles.SelectedPlate;

end

