function [SelectedMetaDataFileName, SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, plateNumber)
% AUTHOR:	Peter Horvath
% DATE: 	April 22, 2016
% NAME: 	readMetaData
% 
% Given a CommonHandles, and a fileName within the selectedPlate this
% function will return the features for this image. If the last argument
% (plateNumber is not provided), then the CommonHandles.selectedPlate will
% be used.
%
% INPUT:
%   CommonHandles           Settings of the system.
%   fileNameexEx            File name of the  of the metadata file without
%                           the extension.
%   plateNumber             Number of the selected plate.
%
% OUTPUT:
%   SelectedMetaDataFileName    Full path of the metadata file including
%                               PlateName, MetaDataFolder, and extension.
%   SelectedMetaData            MxN matrix containing feature vectors. M is
%                               the number of the cells, N is the numebr of
%                               the features.
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright (C) 2016 Peter Horvath,
% Synthetic and System Biology Unit, Hungarian Academia of Sciences,
% Biological Research Center, Szeged, Hungary; Institute for Molecular
% Medicine Finland, University of Helsinki, Helsinki, Finland.

if nargin == 2
    plateNumber = CommonHandles.SelectedPlate;
end

% read txt file
 if  CommonHandles.DataStructureType == 1     
     SelectedMetaDataFileName = fullfile(char(CommonHandles.PlatesNames(plateNumber)),CommonHandles.MetaDataFolder,[fileNameexEx '.txt']);
     if exist(char(SelectedMetaDataFileName), 'file')
         SelectedMetaData = load([CommonHandles.DirName filesep SelectedMetaDataFileName]);
     else
         fileList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx  '*.txt']);
         SelectedMetaDataFileName = [filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileList(1).name];
%          % ATTENTION: this 4 lines below are the old modality to load the file from anal2. Maybe was faster
%          SelectedMetaDataFileName = [filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx  '.tif.txt']; 
%          if ~exist(SelectedMetaDataFileName,'file')
%              SelectedMetaDataFileName = [filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx  '.txt'];
%          end
         SelectedMetaData = load([CommonHandles.DirName SelectedMetaDataFileName]);
     end
     
     % read hdf5
 elseif CommonHandles.DataStructureType == 2
    
     SelectedMetaDataFileName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.MetaDataFolder filesep fileNameexEx '.tif.txt']; %ATTENTION! .tif present here!
     try
        SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '.h5'], fileNameexEx);
     catch        
        SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateNumber)) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '.h5'], [fileNameexEx '.tif']);         
     end
 end
