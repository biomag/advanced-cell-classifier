function transList = stackAlignerDRP(inFile, startIm, stopIm, ch, writeOutput)
% AUTHOR: Peter Horvath
%
% This functions computes the traslation between slices and gives back the
% coordinates.
%
% inFile: prefix of the input bmp series
% startIm, stopIm : first and last image index
% ch: channel used for calclations
% writeOutput: bool value to save the output images or not

% CellTracker Toolbox
% Copyright (C) 2015 Peter Horvath, Filippo Piccinini
% Synthetic and Systems Biology Unit
% Hungarian Academia of Sciences, BRC, Szeged. All rights reserved.
%
% This program is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License version 3 (or higher)
% as published by the Free Software Foundation. This program is
% distributed WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details.

in1 = imread(inFile{startIm});
[xs, ys, ~] = size(in1);
transList = zeros(stopIm - startIm, 2);
h_wait = waitbar(0, sprintf(['Calculating the alignment.\n',...
                    'Please wait...']));
steps = (stopIm-startIm + 1);
align = zeros(stopIm-1,2);
for i = startIm+1:stopIm
    waitbar(i/steps,h_wait, sprintf(['Calculating the alignment.\n',...
                    'Please wait...']))
    in2 = imread(inFile{i});
    corr = standardCorrelation(in1(:,:,ch), in2(:,:,ch), 0);
    [maxv, maxi] = max(corr);
    [~, maxii] = max(maxv);
    align(i-startIm, 1) = maxi(maxii);
    align(i-startIm, 2) = maxii;
    in1 = in2;
end

if exist('h_wait','var'); if ishandle(h_wait); close(h_wait); end; end

for i= startIm+1:stopIm
    transList(i-startIm+1, 1) = transList(i-startIm, 1) + align(i-startIm, 1)-(floor(xs/2) + 1);
    transList(i-startIm+1, 2) = transList(i-startIm, 2) + align(i-startIm, 2)-(floor(ys/2) + 1);
end

%% Creating a new file with the aligned data set
if writeOutput
    minx = min( transList(:, 1));
    maxx = max( transList(:, 1));
    miny = min( transList(:, 2));
    maxy = max( transList(:, 2));
    
    sizeX = xs + minx - maxx;
    sizeY = ys + miny - maxy;
    
    h_wait = waitbar(0, 'Please wait...');
    steps = (stopIm-startIm + 1);
    for i=startIm:stopIm
        waitbar(i/steps)
        [pathstr, fileName, ext] = fileparts(inFile{i});
        pathstr = strsplit(pathstr,'anal3');
        fileName = fullfile(pathstr{1}, 'tmpct', [fileName, '_al', ext]);
        % open image
        in = imread(inFile{i});
        
        % delete the upper and left parts
        in(1:(maxx-transList(i-startIm+1, 1)), :,:) = [];
        in(:, 1:(maxy-transList(i-startIm+1, 2)),:) = [];
        in = in(1:sizeX, 1:sizeY,:);
        
        % Write output
        imwrite(in, fileName);
    end
end

if exist('h_wait','var'); if ishandle(h_wait); close(h_wait); end; end