% Script for plotting the training set

imPath = 'D:\BRC\Deep Regression Plane\HeLa trSet\Images';
labelPath = 'D:\BRC\Deep Regression Plane\HeLa trSet\Labels';

imList = dir(fullfile(imPath,'*.png'));
LabelList = dir(fullfile(labelPath,'*.png'));

rp = uint16(zeros(10000,10000,3));

im = imread(fullfile(imPath,imList(1).name));



for i = 1:length(imList)
    im = imread(fullfile(imPath,imList(i).name));
    [x, y, ch] = size(im);
    coor = imread(fullfile(labelPath,imList(i).name));
    
    BB = getBB(coor,[y x]);
    
    rp(BB(3):BB(4),BB(1):BB(2), :) = im;
end

f = figure;
ax = axes(f);
imshow(rp,'Parent',ax)
ax.YDir = 'normal';
