%% params
model_path = 'DRP_train_result_ws-28-Jun-2021_04_23_21-Flora_grid_1222.mat';
model_input_size = [224, 224]; % img size reguired by the model
% input_folder = '/home/koosk/data/data/DRP/210424-HelaKyoto_ACC_Project_Nuc';
acc_folder = 'C:\Users\BIOMAG\Desktop\DVP2\ACC_210617-HelaKyoto-40x-live__2021-06-17T12_08_49-Measurement1\';

cropSize = 120;

%% real stuff
an2_folder = fullfile(acc_folder, 'anal2');
an3_folder = fullfile(acc_folder, 'anal3');

model = load(model_path);
model = model.net;
if an3_folder(end) ~= filesep
    an3_folder = [an3_folder, filesep];
end
temp_image_files = dir(an3_folder);

lastFrameIDX =[];
for ii = 1:length(temp_image_files)
    if contains(temp_image_files(ii).name,'_t60_m')
        lastFrameIDX = [lastFrameIDX;ii];
    end
end
image_files = temp_image_files(lastFrameIDX);
PlateName = {};
Row = {};
Col = {};
ImageName = {};
ImageNumber = [];
ObjectNumber = [];
xPixelPos = [];
yPixelPos = [];
regPosX = [];
regPosY = [];
wb = waitbar(0,'Predicting image');
for img_idx = 1:numel(image_files)
    file = image_files(img_idx);
    image = imread(fullfile(an3_folder, file.name));
    [im_sy, im_sx, ~] = size(image);
    [~, fname, ext] = fileparts(file.name);
    cell_data = readtable(fullfile(an2_folder, [fname, '.txt']));
    num_cells = size(cell_data, 1);
    crops = zeros(model_input_size(1), model_input_size(2),3, size(cell_data,1));
    for cell_idx = 1:num_cells
        cx = cell_data{cell_idx, 1};
        cy = cell_data{cell_idx, 2};
        crop = image(max(1,cy-cropSize):min(im_sy, cy+cropSize), max(1,cx-cropSize):min(im_sx, cx+cropSize), :);
        crop = im2uint16(crop);
        if cy < cropSize+1
            crop = padarray(crop, [cropSize+1-cy, 0], 'pre');
        end
        if cy > im_sy-cropSize
            crop = padarray(crop, [cropSize-(im_sy-cy), 0], 'post');
        end
        if cx < cropSize+1
            crop = padarray(crop, [0, cropSize+1-cx], 'pre');
        end
        if cx > im_sx-cropSize
            crop = padarray(crop, [cropSize-(im_sx-cx), 0], 'post');
        end
        
        crop = imresize(crop, [model_input_size(1), model_input_size(2)]);
%         fig=figure; imshow(crop);
%         close(fig);
        crops(:,:,:,cell_idx) = crop;
        
    end
    preds = predict(model,crops);
    
    if ishandle(wb)
        waitbar(img_idx/numel(image_files),wb,'Predicting image')
    end
    PlateName(end+1:end+num_cells) = {'induced'};
    f = file.name;
    row_value = f(strfind(f, '_w')+2);
    col_value = f(strfind(f, '_w')+3);
    Row(end+1:end+num_cells) = {row_value};
    Col(end+1:end+num_cells) = {col_value};
    ImageName(end+1:end+num_cells) = {f};
    ImageNumber(end+1:end+num_cells) = ones(num_cells, 1);
    ObjectNumber(end+1:end+num_cells) = 1:num_cells;
    xPixelPos(end+1:end+num_cells) = round(preds(:,1));
    yPixelPos(end+1:end+num_cells) = round(preds(:,2));
    regPosX(end+1:end+num_cells) = preds(:,1)./10000;
    regPosY(end+1:end+num_cells) = preds(:,2)./10000;
end

if ishandle(wb)
    close(wb)
end
helpdlg('Prediction completed')
t = table(PlateName', Row', Col', ImageName', ImageNumber', ObjectNumber', xPixelPos', yPixelPos', regPosX', regPosY');
t.Properties.VariableNames = {'PlateName','row','col','ImageName','ImageNumber','ObjectNumber','xPixelPos','yPixelPos','regPosX','regPosY'};
writetable(t, 'drp_predictions_40xlive.csv')




