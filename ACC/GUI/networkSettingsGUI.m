function varargout = networkSettingsGUI(varargin)
% NETWORKSETTINGSGUI MATLAB code for networkSettingsGUI.fig
%      NETWORKSETTINGSGUI, by itself, creates a new NETWORKSETTINGSGUI or raises the existing
%      singleton*.
%
%      H = NETWORKSETTINGSGUI returns the handle to a new NETWORKSETTINGSGUI or the handle to
%      the existing singleton*.
%
%      NETWORKSETTINGSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NETWORKSETTINGSGUI.M with the given input arguments.
%
%      NETWORKSETTINGSGUI('Property','Value',...) creates a new NETWORKSETTINGSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before networkSettingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to networkSettingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help networkSettingsGUI

% Last Modified by GUIDE v2.5 25-Sep-2016 23:41:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @networkSettingsGUI_OpeningFcn, ...
    'gui_OutputFcn',  @networkSettingsGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before networkSettingsGUI is made visible.
function networkSettingsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to networkSettingsGUI (see VARARGIN)

% Choose default command line output for networkSettingsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes networkSettingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = networkSettingsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function hostInput_Callback(hObject, eventdata, handles)
% hObject    handle to hostInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of hostInput as text
%        str2double(get(hObject,'String')) returns contents of hostInput as a double

global CommonHandles;

if isfieldRecursive(CommonHandles,'CAMI.Network.Host') && strcmp(get(hObject, 'String'), CommonHandles.CAMI.Network.Host)
    set(handles.testConnectionButton, 'Enable', 'on');
    set(handles.saveSettingsButton, 'Enable', 'off');
else
    set(handles.testConnectionButton, 'Enable', 'off');
    set(handles.saveSettingsButton, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function hostInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hostInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global CommonHandles;
if isfieldRecursive(CommonHandles, 'CAMI.Network.Host')
    set(hObject, 'String', CommonHandles.CAMI.Network.Host);
else
    set(hObject, 'String', '127.0.0.1');
end

function portInput_Callback(hObject, eventdata, handles)
% hObject    handle to portInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of portInput as text
%        str2double(get(hObject,'String')) returns contents of portInput as a double

global CommonHandles;

if isfieldRecursive(CommonHandles,'CAMI.Network.Port') && strcmp( str2num(get(hObject, 'String')), CommonHandles.CAMI.Network.Port)
    set(handles.testConnectionButton, 'Enable', 'on');
    set(handles.saveSettingsButton, 'Enable', 'off');
else
    set(handles.testConnectionButton, 'Enable', 'off');
    set(handles.saveSettingsButton, 'Enable', 'on');
end


% --- Executes during object creation, after setting all properties.
function portInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to portInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global CommonHandles;
if isfieldRecursive(CommonHandles,'CAMI.Network.Port')
    set(hObject, 'String', num2str(CommonHandles.CAMI.Network.Port));
else
    set(hObject, 'String', '8080');
end

% --- Executes on button press in saveSettingsButton.
function saveSettingsButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveSettingsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
CommonHandles.CAMI.Network.Host = get(handles.hostInput,'String');
CommonHandles.CAMI.Network.Port = get(handles.portInput,'String');
CommonHandles.CAMI.Network.Path = get(handles.pathInput,'String');

set(handles.testConnectionButton, 'Enable', 'on');
set(handles.saveSettingsButton, 'Enable', 'off');

% --- Executes on button press in testConnectionButton.
function testConnectionButton_Callback(hObject, eventdata, handles)
% hObject    handle to testConnectionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

hostBaseName = ['http://' CommonHandles.CAMI.Network.Host ':' CommonHandles.CAMI.Network.Port '/' CommonHandles.CAMI.Network.Path];
testUploadServiceURL = [hostBaseName '/test/upload/'];
try
    status = urlread(testUploadServiceURL);
catch e
    status = 'error';
end

status = str2double(status);
if status==0
    CommonHandles.CAMI.Network.UploadServiceIsWorking = 1;
    msgbox('The upload service is running','Success');
else
    CommonHandles.CAMI.Network.UploadServiceIsWorking = 0;
    warndlg('Remote upload service is not running on this address!');
end


function pathInput_Callback(hObject, eventdata, handles)
% hObject    handle to pathInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pathInput as text
%        str2double(get(hObject,'String')) returns contents of pathInput as a double

global CommonHandles;

if isfieldRecursive(CommonHandles,'CAMI.Network.Path') && strcmp( get(hObject, 'String'), CommonHandles.CAMI.Network.Path)
    set(handles.testConnectionButton, 'Enable', 'on');
    set(handles.saveSettingsButton, 'Enable', 'off');
else
    set(handles.testConnectionButton, 'Enable', 'off');
    set(handles.saveSettingsButton, 'Enable', 'on');
end



% --- Executes during object creation, after setting all properties.
function pathInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pathInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global CommonHandles;

set(hObject,'String',CommonHandles.CAMI.Network.Path);
