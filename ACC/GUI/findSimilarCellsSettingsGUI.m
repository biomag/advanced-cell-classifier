    function varargout = findSimilarCellsSettingsGUI(varargin)
% FINDSIMILARCELLSSETTINGSGUI MATLAB code for findSimilarCellsSettingsGUI.fig
%      FINDSIMILARCELLSSETTINGSGUI, by itself, creates a new FINDSIMILARCELLSSETTINGSGUI or raises the existing
%      singleton*.
%
%      H = FINDSIMILARCELLSSETTINGSGUI returns the handle to a new FINDSIMILARCELLSSETTINGSGUI or the handle to
%      the existing singleton*.
%
%      FINDSIMILARCELLSSETTINGSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FINDSIMILARCELLSSETTINGSGUI.M with the given input arguments.
%
%      FINDSIMILARCELLSSETTINGSGUI('Property','Value',...) creates a new FINDSIMILARCELLSSETTINGSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before findSimilarCellsSettingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to findSimilarCellsSettingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help findSimilarCellsSettingsGUI

% Last Modified by GUIDE v2.5 06-May-2016 16:19:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @findSimilarCellsSettingsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @findSimilarCellsSettingsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before findSimilarCellsSettingsGUI is made visible.
function findSimilarCellsSettingsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to findSimilarCellsSettingsGUI (see VARARGIN)
global CommonHandles;

% Choose default command line output for findSimilarCellsSettingsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes findSimilarCellsSettingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

if ~isfield(CommonHandles,'FindSimilarCellsSettings')
    CommonHandles.FindSimilarCellsSettings.MaxSimilarCount = 10;
    CommonHandles.FindSimilarCellsSettings.MinSimilarityScore = 0.6;
    CommonHandles.FindSimilarCellsSettings.MaxFeatureSimilarity = 0.95;
    CommonHandles.FindSimilarCellsSettings.FeatureVariance = 0.7;
    %uiwait(findSimilarCellsSettingsGUI());
end

set(hObject, 'Name', 'Find similar cells settings');
set(handles.maxSimilarCount,'String',num2str(CommonHandles.FindSimilarCellsSettings.MaxSimilarCount));
set(handles.minSimilarityScore,'String',num2str(CommonHandles.FindSimilarCellsSettings.MinSimilarityScore));
set(handles.maxFeatureSimilarity,'String',num2str(CommonHandles.FindSimilarCellsSettings.MaxFeatureSimilarity));
set(handles.featureVariance,'String',num2str(CommonHandles.FindSimilarCellsSettings.FeatureVariance));


% --- Outputs from this function are returned to the command line.
function varargout = findSimilarCellsSettingsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
num_similar = str2double(get(handles.maxSimilarCount,'String'));
min_similar_score = str2double(get(handles.minSimilarityScore,'String'));
max_feat_similar = str2double(get(handles.maxFeatureSimilarity,'String'));
feat_var = str2double(get(handles.featureVariance,'String'));

validInputs = 1;
% Check that fields have numeric values
if isnan(num_similar)
    errordlg('Please give an integer number for the maximum number of similar cells!','Not a number error');
    validInputs = 0;
end
if isnan(min_similar_score)
    errordlg('Please give a real number for the minimum similarity score!','Not a number error');
    validInputs = 0;
end
if isnan(max_feat_similar)
    errordlg('Please give a real number for the maximum feature similarity!','Not a number error');
    validInputs = 0;
end
if isnan(feat_var)
    errordlg('Please give a real number for the feature variance!','Not a number error');
    validInputs = 0;
end

% Check that numeric values are in correct range
if validInputs
    if num_similar > length(CommonHandles.AllFeaturesMapping)
        errordlg('The maximum number of similar cells can not be bigger than the sampling pool','Interval error');
        validInputs = 0;
    end
    if num_similar < 1 || num_similar > 100
        errordlg('The maximum number of similar cells must be between 1 and 100','Interval error');
        validInputs = 0;
    end
    if min_similar_score < 0.0 || min_similar_score > 1.0
        errordlg('The minimum similarity score must be between 0.0 and 1.0','Interval error');
        validInputs = 0;
    end
    if max_feat_similar < 0.0 || max_feat_similar > 1.0
        errordlg('The maximum feature similarity must be between 0.0 and 1.0','Interval error');
        validInputs = 0;
    end
    if feat_var < 0.0 || feat_var > 1.0
        errordlg('The feature variance must be between 0.0 and 1.0','Interval error');
        validInputs = 0;
    end
end

if validInputs
    CommonHandles.FindSimilarCellsSettings.MaxSimilarCount = num_similar;
    CommonHandles.FindSimilarCellsSettings.MinSimilarityScore = min_similar_score;
    CommonHandles.FindSimilarCellsSettings.MaxFeatureSimilarity = max_feat_similar;
    CommonHandles.FindSimilarCellsSettings.FeatureVariance = feat_var;
    uiresume(gcbf);
    close(gcbf);
end


function maxFeatureSimilarity_Callback(hObject, eventdata, handles)
% hObject    handle to maxFeatureSimilarity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxFeatureSimilarity as text
%        str2double(get(hObject,'String')) returns contents of maxFeatureSimilarity as a double


% --- Executes during object creation, after setting all properties.
function maxFeatureSimilarity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxFeatureSimilarity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function featureVariance_Callback(hObject, eventdata, handles)
% hObject    handle to featureVariance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of featureVariance as text
%        str2double(get(hObject,'String')) returns contents of featureVariance as a double


% --- Executes during object creation, after setting all properties.
function featureVariance_CreateFcn(hObject, eventdata, handles)
% hObject    handle to featureVariance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxSimilarCount_Callback(hObject, eventdata, handles)
% hObject    handle to maxSimilarCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxSimilarCount as text
%        str2double(get(hObject,'String')) returns contents of maxSimilarCount as a double


% --- Executes during object creation, after setting all properties.
function maxSimilarCount_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxSimilarCount (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minSimilarityScore_Callback(hObject, eventdata, handles)
% hObject    handle to minSimilarityScore (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minSimilarityScore as text
%        str2double(get(hObject,'String')) returns contents of minSimilarityScore as a double


% --- Executes during object creation, after setting all properties.
function minSimilarityScore_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minSimilarityScore (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
