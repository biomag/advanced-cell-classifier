function varargout = uploadSelectedCellsGUI(varargin)
% UPLOADSELECTEDCELLSGUI MATLAB code for uploadSelectedCellsGUI.fig
%      UPLOADSELECTEDCELLSGUI, by itself, creates a new UPLOADSELECTEDCELLSGUI or raises the existing
%      singleton*.
%
%      H = UPLOADSELECTEDCELLSGUI returns the handle to a new UPLOADSELECTEDCELLSGUI or the handle to
%      the existing singleton*.
%
%      UPLOADSELECTEDCELLSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UPLOADSELECTEDCELLSGUI.M with the given input arguments.
%
%      UPLOADSELECTEDCELLSGUI('Property','Value',...) creates a new UPLOADSELECTEDCELLSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before uploadSelectedCellsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to uploadSelectedCellsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help uploadSelectedCellsGUI

% Last Modified by GUIDE v2.5 27-Sep-2016 13:52:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @uploadSelectedCellsGUI_OpeningFcn, ...
    'gui_OutputFcn',  @uploadSelectedCellsGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before uploadSelectedCellsGUI is made visible.
function uploadSelectedCellsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to uploadSelectedCellsGUI (see VARARGIN)

% Choose default command line output for uploadSelectedCellsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes uploadSelectedCellsGUI wait for user response (see UIRESUME)
% uiwait(handles.uploadFigure);

global CommonHandles;

if ~isempty( get(handles.classesListbox, 'String') )
    classID = get(handles.classesListbox, 'Value');
    imshow(CommonHandles.Classes{classID}.Icon, 'Parent', handles.classImageAxes);
end


% --- Outputs from this function are returned to the command line.
function varargout = uploadSelectedCellsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in classesListbox.
function classesListbox_Callback(hObject, eventdata, handles)
% hObject    handle to classesListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns classesListbox contents as cell array
%        contents{get(hObject,'Value')} returns selected item from classesListbox

global CommonHandles;

classID = get(hObject, 'Value');

imshow(CommonHandles.Classes{classID}.Icon,'Parent',handles.classImageAxes);


% --- Executes during object creation, after setting all properties.
function classesListbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classesListbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

global CommonHandles;
if ~isempty(CommonHandles.Classes)
    ClassNames = cell(length(CommonHandles.Classes),1);
    for i=1:length(CommonHandles.Classes)
        ClassNames{i} = CommonHandles.Classes{i}.Name;
    end
else
    ClassNames = [];
end

set(hObject, 'String', ClassNames);

guidata(hObject, handles);



% --- Executes when user attempts to close uploadFigure.
function uploadFigure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to uploadFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%TODO check if there is anything open (connection, progress etc.)

% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes during object creation, after setting all properties.
function uploadFigure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uploadFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --------------------------------------------------------------------
function networkSettingsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to networkSettingsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = networkSettingsGUI();
uiwait(h);


function cellNumberInput_Callback(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellNumberInput as text
%        str2double(get(hObject,'String')) returns contents of cellNumberInput as a double


% --- Executes during object creation, after setting all properties.
function cellNumberInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showCellsButton.
function showCellsButton_Callback(hObject, eventdata, handles)
% hObject    handle to showCellsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

classID = get(handles.classesListbox, 'Value');

numOfCellsToShow = str2double(get(handles.cellNumberInput,'String'));

validInput = 1;

if isnan(numOfCellsToShow)
    errordlg('Please give an integer number for the maximum number of similar cells!','Not a number error');
    validInput = 0;
end

if validInput
    if numOfCellsToShow < 1 || numOfCellsToShow > 1000
        errordlg('The maximum number of similar cells must be between 1 and 1000','Interval error');
        validInput = 0;
    end
end

if validInput
    showStruct = struct('classID', classID, 'numOfCellsToShow', numOfCellsToShow);
    
    if ishandle(CommonHandles.ShowHighProbCellsHandle)
        delete(CommonHandles.ShowHighProbCellsHandle);
    end
    CommonHandles.ShowHighProbCellsHandle = showHighProbCells_GUI('UserData',showStruct);    
    showStruct = get(CommonHandles.ShowHighProbCellsHandle,'UserData');
    if isfield(showStruct,'failure') && ishandle(CommonHandles.ShowHighProbCellsHandle)
        close(CommonHandles.ShowHighProbCellsHandle);
    end
end
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function showCellsButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showCellsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function classImageAxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classImageAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate classImageAxes



function numOfSelectedInput_Callback(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellNumberInput as text
%        str2double(get(hObject,'String')) returns contents of cellNumberInput as a double


% --- Executes during object creation, after setting all properties.
function numOfSelectedInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellNumberInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function networkSettingsMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to networkSettingsMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

networkSettingsGUI();


% --------------------------------------------------------------------
function fileSettingsMenu_Callback(hObject, eventdata, handles)
% hObject    handle to fileSettingsMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

h = camiDirectorySettingsGUI();
uiwait(h);
