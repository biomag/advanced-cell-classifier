function varargout = showTrainedCells_GUI(varargin)
% SHOWTRAINEDCELLS_GUI MATLAB code for showTrainedCells_GUI.fig
%      SHOWTRAINEDCELLS_GUI, by itself, creates a new SHOWTRAINEDCELLS_GUI or raises the existing
%      singleton*. NOTE: this figure is not singleton as it is used for
%      multiple purposes (for show similar cells and trained cells too)
%
%      H = SHOWTRAINEDCELLS_GUI returns the handle to a new SHOWTRAINEDCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWTRAINEDCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWTRAINEDCELLS_GUI.M with the given input arguments.
%
%      SHOWTRAINEDCELLS_GUI('Property','Value',...) creates a new SHOWTRAINEDCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showTrainedCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showTrainedCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showTrainedCells_GUI

% Last Modified by GUIDE v2.5 14-Jun-2016 14:46:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showTrainedCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showTrainedCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showTrainedCells_GUI is made visible.
function showTrainedCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showTrainedCells_GUI (see VARARGIN)

global CommonHandles;

% Choose default command line output for showTrainedCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes showTrainedCells_GUI wait for user response (see UIRESUME)
% uiwait(handles.showTrainedCellsFig);

counter = get(hObject, 'UserData');
[img,className,~] = createClassImage(counter);

ax = axes('Parent',hObject);
imHandle = imshow(img,'Parent',ax);
hSP = imscrollpanel(hObject,imHandle);
set(hSP,'Units','pixels','Position',[10 70 720 500],'Tag','scrollPanel');
api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);
api.setImageButtonDownFcn({@scrollPanel_ButtonDownFcn,hObject});

% GUI changes for showing similar cells
if counter == 0
    
    %clear off previous selections
    CommonHandles.SimilarCells.sHandle = [];
    CommonHandles.SimilarCells.selected = [];
    
    set(handles.unclassifierButton, 'Visible', 'Off');
    set(hObject, 'Name', 'List of similar cells');
else
    %clear up previous selections
    className = CommonHandles.Classes{counter}.Name;
    classImgObj = CommonHandles.ClassImages(className);
    classImgObj.sHandle = [];
    classImgObj.selected = [];
    CommonHandles.ClassImages(className) = classImgObj;
    
    set(hObject, 'Name', ['Class name: ' className]);
end
    names = getSpecClassList;
    idx = strcmp(names,className);  
    if (isempty(names(~idx)))
        set(handles.addToClass,'Enable','off');
    else
        set(handles.classesList, 'string', names(~idx));  
    end



% % --- Outputs from this function are returned to the command line.
function varargout = showTrainedCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function scrollPanel_ButtonDownFcn(hObject,eventdata,fHandle)
%
%
%
global CommonHandles;

%x = eventdata.IntersectionPoint(1);
%y = eventdata.IntersectionPoint(2);
% dummy code needed for old matlab
curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);
counter = get(fHandle, 'UserData');

[~,~,mapping] = createClassImage(counter);
if counter > 0
    className = CommonHandles.Classes{counter}.Name;
    classImgObj = CommonHandles.ClassImages(className);
else
    className = 'Similar cells';
    classImgObj = CommonHandles.SimilarCells;
end

[selimgInfo,hit,xcoord,ycoord] = selectCellFromClassImage(x,y,mapping);

if hit
    % Get axes object
    hSp = get(fHandle,'Children');
    axes = findobj(hSp,'Type','Axes');
    seltype = get(fHandle,'SelectionType');
    
    % Remove previous rectangle if exists, seltype == normal or counter>0
    % seltype == 'alt' for ctrl-click, 'extend' for shift-click
    if strcmp(seltype,'normal')
        if ~isempty(classImgObj.selected)
            for i=1:length(classImgObj.selected)
                if ~isempty(fieldnames(classImgObj.selected{i}))
                    delete(classImgObj.sHandle{i});
                end
            end
            classImgObj.sHandle = [];
            classImgObj.selected = [];
        end
    end
    
    % Draw borders around the image
    if ~isempty(selimgInfo)
        lw = 3;
        imgxsize = CommonHandles.ClassImageInfo.ImageSize(1);
        imgysize = CommonHandles.ClassImageInfo.ImageSize(2);
        sepsize = CommonHandles.ClassImageInfo.SepSize;
        drawX = (xcoord-1) * imgxsize + (xcoord) * sepsize - lw;
        drawY = (ycoord-1) * imgysize + (ycoord) * sepsize - lw;
        hRec = rectangle('Position',[drawX, drawY, imgxsize + 2*lw - 1, imgysize + 2*lw - 1],'EdgeColor', 'red', 'LineWidth',lw, 'Parent', axes);
        classImgObj.sHandle{end+1} = hRec;
        classImgObj.selected{end+1} = selimgInfo;
    end
    if counter > 0
        CommonHandles.ClassImages(className) = classImgObj;
    else
        CommonHandles.SimilarCells = classImgObj;
    end    
end


% --- Executes on button press in unclassifierButton.
function unclassifierButton_Callback(hObject, eventdata, handles)
% hObject    handle to unclassifierButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
className = CommonHandles.Classes{counter}.Name;

if ~isempty(CommonHandles.ClassImages(className).selected)
    for i=1:length(CommonHandles.ClassImages(className).selected)
        if ~isempty(fieldnames(CommonHandles.ClassImages(className).selected{i}))
            deleteFromClassImages(CommonHandles.ClassImages(className).selected{i}.CellNumber, CommonHandles.ClassImages(className).selected{i}.ImageName, counter);
            delete(CommonHandles.ClassImages(className).sHandle{i});                        
        end
    end    
    classImgObj = CommonHandles.ClassImages(className);
    classImgObj.selected = [];
    classImgObj.sHandle = [];
    CommonHandles.ClassImages(className) = classImgObj;
end

sp = findobj(figHandle,'Tag','scrollPanel');
refreshClassImage(counter, sp);


% --- Executes on button press in closeButton.
function closeButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
if counter > 0
    className = CommonHandles.Classes{counter}.Name;
    classImgObj = CommonHandles.ClassImages(className);
else
    className = 'Similar cells';
    classImgObj = CommonHandles.SimilarCells;
end

if ~isempty(classImgObj.selected)
    for i=1:length(classImgObj.selected)
        if ~isempty(fieldnames(classImgObj.selected{i}))
            delete(classImgObj.sHandle{i});
        end
    end
    classImgObj.selected = [];
    classImgObj.sHandle = [];
    if counter > 0
        CommonHandles.ClassImages(className) = classImgObj;
    else
        CommonHandles.SimilarCells = classImgObj;
    end
end

uiresume(gcbf); 
close;


% --- Executes on button press in jumpToCell.
function jumpToCell_Callback(hObject, eventdata, handles)
% hObject    handle to jumpToCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
if counter > 0
    className = CommonHandles.Classes{counter}.Name;
    classImgObj = CommonHandles.ClassImages(className);
else
    className = 'Similar cells';
    classImgObj = CommonHandles.SimilarCells;
end

info = classImgObj.selected;

if ~isempty(info) && length(info) == 1 && isfield(info{1},'ImageName')
    temp = strsplit(info{1}.ImageName,filesep);
    if info{1}.PlateName == 0
        errordlg('You cannot jump to this cell. Probably its plate was removed.');
    elseif info{1}.PlateName <= length(CommonHandles.PlatesNames) && strcmp(temp{1},char(CommonHandles.PlatesNames(info{1}.PlateName)))
        jumpToCell(info{1},1);
    else
        errordlg('You cannot jump to this cell.');
    end
else
    errordlg('No cell or multiple cells were selected.','Jump to cell error');
end


% --- Executes on button press in addToClass.
function addToClass_Callback(hObject, eventdata, handles)
% hObject    handle to addToClass (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

figHandle = get(hObject,'Parent');
counter = get(figHandle, 'UserData');
if counter > 0
    className = CommonHandles.Classes{counter}.Name;
    classImgObj = CommonHandles.ClassImages(className);
else
    classImgObj = CommonHandles.SimilarCells;
end

madeChange = 0;
tempCurrentCellInfo = makeTempCellData();

if isempty(classImgObj.selected)
    errordlg('Please, select cell or cells to be added to the class', 'Add to class error');
else
    classNameString = get(handles.classesList,'String');
    className = classNameString{get(handles.classesList,'Value')};
    % Collect features and preview images of all cells
    features = cell(1,length(classImgObj.selected));
    images = cell(1,length(classImgObj.selected));
    h = waitbar(0,sprintf('Adding cells to class [%s] (%d/%d)',className,0,length(classImgObj.selected)));
    for i=1:length(classImgObj.selected)        
        selinfo = classImgObj.selected{i};
        for j=1:length(classImgObj.Images)
            selimg = classImgObj.Images{j};
            if selinfo.CellNumber == selimg.CellNumber && strcmp(selinfo.ImageName,selimg.ImageName) && selinfo.PlateName == selimg.PlateName
                images{i} = selimg.Image;
                jumpToCell(selinfo,0);
                features{i} = CommonHandles.SelectedMetaData(selinfo.CellNumber,3:end);                
            end
        end
        if ishandle(h), waitbar(i/length(classImgObj.selected),h,sprintf('Adding cells to class [%s] (%d/%d)',className,i,length(classImgObj.selected))); end
    end    
    
    % Add cells to class
    for i=1:size(CommonHandles.Classes,2)
        classNames{i} = CommonHandles.Classes{i}.Name;
    end            
    
    if (exist('classNames','var')) % if there are no classes then classNames will not exist
        classIdx = find(strcmp(classNames,className));
        if ~isempty(classIdx)                           
            for i=1:length(classImgObj.selected)        
                selinfo = classImgObj.selected{i};                
                featvec = features{i};

                alreadyInTrainingSet = isInTrainingSet( selinfo.ImageName, selinfo.CellNumber, 0, CommonHandles, featvec);

                if ~alreadyInTrainingSet && ~(counter > 0)
                    jumpToCell(selinfo,0);
                    %Was here before

                    selinfo.Image = images{i};
                    selinfo.ClassName = className;
                    if ~CommonHandles.ClassImages.isKey(className)
                        newclass = struct('Images',[],'selected',[],'sHandle',[]);
                        CommonHandles.ClassImages(className) = newclass;
                    end
                    cimgs = CommonHandles.ClassImages(className);
                    cimgs.Images{end+1} = selinfo;
                    CommonHandles.ClassImages(className) = cimgs;
                    
                    addToTrainingSet(classIdx, featvec); 
                    %The order is important! AddToTrainingSet assumes that
                    %the new classImgObj is added to the ClassImages before
                    %(during regression rollback case)

                    madeChange = madeChange + 1;
                    
                elseif alreadyInTrainingSet && counter > 0
                                                                                                      
                    % from class
                    oldClassIdx = find(strcmp(classNames,selimg.ClassName));

                    % to class
                    classNameString = get(handles.classesList,'String');
                    className = classNameString{get(handles.classesList,'Value')};
                    classIdx = find(strcmp(classNames,className));

                    % get cell info
                    jumpToCell(selinfo,0);
                    % add cell to ClassImages
                    addToClassImages(classIdx);
                    % add cell to TrainingSet
                    moveClassImage(CommonHandles.SelectedCell, CommonHandles.SelectedImageName, classIdx);   
                                                                    
                end
            end
            refreshClassesList();
        end
    end
    jumpToCell(tempCurrentCellInfo,1);
    if ishandle(h), close(h); end
    
    if ishandle(CommonHandles.ShowTrainedCellsFig)
        close(CommonHandles.ShowTrainedCellsFig)
    end 
end

if madeChange    
    CommonHandles.LabeledInstances = sum(cell2mat(CommonHandles.TrainingSet.Class'), 2)';
    for i=1:length(CommonHandles.LabeledInstances)
        CommonHandles.Classes{i}.LabelNumbers = CommonHandles.LabeledInstances(i);
    end
    
    refreshClassesList();
    
    msgbox([num2str(madeChange) ' cells successfully added to class ' className], 'Cells added to class');    
end


% --- Executes on selection change in classesList.
function classesList_Callback(hObject, eventdata, handles)
% hObject    handle to classesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns classesList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from classesList


% --- Executes during object creation, after setting all properties.
function classesList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classesList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
