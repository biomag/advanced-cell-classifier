function varargout = trainRegression_GUI(varargin)
% TRAINREGRESSION_GUI MATLAB code for trainRegression_GUI.fig
%      TRAINREGRESSION_GUI, by itself, creates a new TRAINREGRESSION_GUI or raises the existing
%      singleton*.
%
%      H = TRAINREGRESSION_GUI returns the handle to a new TRAINREGRESSION_GUI or the handle to
%      the existing singleton*.
%
%      TRAINREGRESSION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAINREGRESSION_GUI.M with the given input arguments.
%
%      TRAINREGRESSION_GUI('Property','Value',...) creates a new TRAINREGRESSION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before trainRegression_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to trainRegression_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help trainRegression_GUI

% Last Modified by GUIDE v2.5 07-Jul-2016 18:24:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @trainRegression_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @trainRegression_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before trainRegression_GUI is made visible.
function trainRegression_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to trainRegression_GUI (see VARARGIN)

% Choose default command line output for trainRegression_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes trainRegression_GUI wait for user response (see UIRESUME)
% uiwait(handles.trainRegressionFigure);

list = listAvailableRegressors;
set(handles.regressionList,'String',list);


% --- Outputs from this function are returned to the command line.
function varargout = trainRegression_GUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in regressionList.
function regressionList_Callback(~,~,~)
% hObject    handle to regressionList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns regressionList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from regressionList


% --- Executes during object creation, after setting all properties.
function regressionList_CreateFcn(hObject, ~, ~)
% hObject    handle to regressionList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in trainRegressionButton.
function trainRegressionButton_Callback(~, ~, handles)
% hObject    handle to trainRegressionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.trainRegressionFigure,'UserData');
idx = uData.classIndex;

mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';

mergedFeatures = reshape(cell2mat(CommonHandles.TrainingSet.Features(logical(mergedClassVectors(:,idx)))),CommonHandles.Classes{idx}.LabelNumbers,[])';
mergedTargets = reshape(cell2mat(CommonHandles.TrainingSet.RegPos(logical(mergedClassVectors(:,idx)))'),CommonHandles.Classes{idx}.LabelNumbers,[])';

if size(mergedFeatures,2)<5
    warndlg('Please put at least 5 training samples to the regression plane.');
    return;
end
    
contents = cellstr(get(handles.regressionList,'String'));

paramarray = eval([contents{get(handles.regressionList,'Value')} '.getParameters();']);
checkHandle = [contents{get(handles.regressionList,'Value')} '.checkParameters'];
if ~isempty(paramarray)
    try
        paramcell = Settings_GUI(paramarray,'infoText','Please specify parameters for the regression model:','checkFunction',checkHandle);
    catch ME
        if strcmp(ME.identifier,'Settings_GUI:noParameterProvided')            
            if ishandle(handles.trainRegressionFigure)
                close(handles.trainRegressionFigure);
            end
            return;
        else
            rethrow(ME);
        end        
    end
else
    paramcell = {};
end

%put info to the user
screensize = get(0,'Screensize');
infoD = dialog('Name','Train regression','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
uicontrol('Parent',infoD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','The machine is learning...');
pause(0.1);

%Default constructor
try
    CommonHandles.RegressionPlane{idx}.Predictor = eval([contents{get(handles.regressionList,'Value')} ';']);
catch e
    if strcmp(e.identifier,'MATLAB:minrhs')
        errordlg('The selected predictor doesn''t have default constructor. Please add a default constructor or contact the authors.')
    else
        rethrow(e);
    end
end
CommonHandles.RegressionPlane{idx}.Predictor = CommonHandles.RegressionPlane{idx}.Predictor.setParameters(paramcell);
CommonHandles.RegressionPlane{idx}.Predictor.databounds = '[0 1000; 0 1000]';
%Train
CommonHandles.RegressionPlane{idx}.Predictor = CommonHandles.RegressionPlane{idx}.Predictor.train(mergedFeatures,mergedTargets);

if ishandle(infoD)
    delete(infoD);
end

if ishandle(handles.trainRegressionFigure)
    close(handles.trainRegressionFigure);
end


function list = listAvailableRegressors
% This functions should list all the available regressors. The regressors
% are listed from the ActiveRegressionModule. That is the place where you
% should place newly implemented algorithms. It should implement the
% predictor interface.
% OUTPUT: cellarray with the strings of the regression algorithms

%list the AL folder and the implementations
wS = what(['Utils' filesep 'ActiveRegression' filesep 'Model' filesep 'Predictors']);
w = struct2cell(wS);
m = w{2};
% for only the math files
for i = 1:length(m)
    %see its name without .m
    splitted = strsplit(m{i},'.');
    %rename
    m{i} = splitted{1};
end
%return the cellarray
list = m;
