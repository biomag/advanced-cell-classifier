function varargout = setIntensityGUI(varargin)
% SETINTENSITYGUI MATLAB code for SetIntensityGUI.fig
%      SETINTENSITYGUI, by itself, creates a new SETINTENSITYGUI or raises the existing
%      singleton*.
%
%      H = SETINTENSITYGUI returns the handle to a new SETINTENSITYGUI or the handle to
%      the existing singleton*.
%
%      SETINTENSITYGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETINTENSITYGUI.M with the given input arguments.
%
%      SETINTENSITYGUI('Property','Value',...) creates a new SETINTENSITYGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SetIntensityGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SetIntensityGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SetIntensityGUI

% Last Modified by GUIDE v2.5 18-Dec-2015 10:26:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setIntensityGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @setIntensityGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SetIntensityGUI is made visible.
function setIntensityGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setIntensityGUI (see VARARGIN)

% Choose default command line output for SetIntensityGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SetIntensityGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global CommonHandles;

set(handles.edit3, 'String', num2str(CommonHandles.MainView.StrechMinR));
set(handles.edit4, 'String', num2str(CommonHandles.MainView.StrechMaxR));
set(handles.edit5, 'String', num2str(CommonHandles.MainView.StrechMinG));
set(handles.edit6, 'String', num2str(CommonHandles.MainView.StrechMaxG));
set(handles.edit7, 'String', num2str(CommonHandles.MainView.StrechMinB));
set(handles.edit8, 'String', num2str(CommonHandles.MainView.StrechMaxB));

set(handles.slider2,'Value', CommonHandles.MainView.StrechMinR);
set(handles.slider3,'Value', CommonHandles.MainView.StrechMaxR);
set(handles.slider4,'Value', CommonHandles.MainView.StrechMinG);
set(handles.slider5,'Value', CommonHandles.MainView.StrechMaxG);
set(handles.slider6,'Value', CommonHandles.MainView.StrechMinB);
set(handles.slider7,'Value', CommonHandles.MainView.StrechMaxB);

switch CommonHandles.MainView.SwapMap
    case 1
        set(handles.radiobutton2, 'Value', 1);
    case 2
        set(handles.radiobutton3, 'Value', 1);
    case 3
        set(handles.radiobutton4, 'Value', 1);
    case 4
        set(handles.radiobutton5, 'Value', 1);
    case 5
        set(handles.radiobutton6, 'Value', 1);
    case 6
        set(handles.radiobutton7, 'Value', 1);
    otherwise
        disp('Wouppp');
end


        
updateChanges(handles);

set(hObject, 'Name', 'Colour settings');

% --- Outputs from this function are returned to the command line.
function varargout = setIntensityGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double

% check if correct
num = str2num(get(handles.edit3, 'String'));
Rmax = get(handles.slider3,'Value');


if isempty(num)
    set(handles.edit3, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit3, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit3, 'String', '255');
    num = 255;
elseif num>Rmax
    num = Rmax;
end    

% set slider accordingly
set(handles.slider2, 'Value', num);
set(handles.edit3, 'String', num2str(num));
% reset all intensity structure

% update global changes
updateChanges(handles)



% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double
% check if correct
num = str2num(get(handles.edit4, 'String'));
Rmin = get(handles.slider2,'Value');

if isempty(num)
    set(handles.edit4, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit4, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit4, 'String', '255');
    num = 255;
elseif num < Rmin
    num = Rmin;
end    

% set slider accordingly
set(handles.slider3, 'Value', num);
set(handles.edit4, 'String', num2str(num));
% reset all intensity structure
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Rmin = get(handles.slider2, 'Value');
Rmax = get(handles.slider3, 'Value');
% handle extrems
if Rmax == 0;
    set(handles.slider3, 'Value', 1);
    Rmax = get(handles.slider3,'Value');
end
if Rmin >= Rmax;
    set(handles.slider2, 'Value', Rmax-1);
    Rmin = get(handles.slider2,'Value');
end

% set text
set(handles.edit3, 'String', num2str(round(Rmin)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Rmin = get(handles.slider2, 'Value');
Rmax = get(handles.slider3, 'Value');
% handle extrems
if Rmin == 255;
    set(handles.slider2, 'Value', 254);
    Rmin = get(handles.slider2,'Value');
end
if Rmin >= Rmax
    set(handles.slider3, 'Value', Rmin+1);
    Rmax = get(handles.slider3,'Value');
end

% set text
set(handles.edit4, 'String', num2str(round(Rmax)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function updateChanges(handles)
global CommonHandles;

% get params
% reset all intensity structure
Rmin = round(get(handles.slider2,'Value'));
Rmax = round(get(handles.slider3,'Value'));
Gmin = round(get(handles.slider4,'Value'));
Gmax = round(get(handles.slider5,'Value'));
Bmin = round(get(handles.slider6,'Value'));
Bmax = round(get(handles.slider7,'Value'));
invertR = get(handles.checkbox1, 'Value');
invertG = get(handles.checkbox2, 'Value');
invertB = get(handles.checkbox3, 'Value');

% red scalebar
imgR = zeros(40, 256, 3);
imgR(:, 1: Rmin+1, 1) = 0;
imgR(:,  Rmax+1:256, 1) = 255;
transR = repmat(round(linspace(0, 255,  Rmax -  Rmin)), 40, 1);
imgR(:,  Rmin+1: Rmax, 1) = transR;
if  invertR
    imgR(:,:, 1) = 255-imgR(:,:, 1);
end
imshow(uint8(imgR), 'Parent', handles.axes1);

% red scalebar
imgR = zeros(40, 256, 3);
imgR(:, 1: Rmin+1, 1) = 0;
imgR(:,  Rmax+1:256, 1) = 255;
transR = repmat(round(linspace(0, 255,  Rmax -  Rmin)), 40, 1);
imgR(:,  Rmin+1: Rmax, 1) = transR;
if  invertR
    imgR(:,:, 1) = 255-imgR(:,:, 1);
end
imshow(uint8(imgR), 'Parent', handles.axes1);


% green scalebar
imgG = zeros(40, 256, 3);
imgG(:, 1: Gmin+1, 2) = 0;
imgG(:,  Gmax+1:256, 2) = 255;
transG = repmat(round(linspace(0, 255,  Gmax -  Gmin)), 40, 1);
imgG(:,  Gmin+1: Gmax, 2) = transG;
if  invertG
    imgG(:,:, 2) = 255-imgG(:,:, 2);
end
imshow(uint8(imgG), 'Parent', handles.axes2);



% blue scalebar
imgB = zeros(40, 256, 3);
imgB(:, 1: Bmin+1, 3) = 0;
imgB(:,  Bmax+1:256, 3) = 255;
transB = repmat(round(linspace(0, 255,  Bmax -  Bmin)), 40, 1);
imgB(:,  Bmin+1: Bmax, 3) = transB;
if  invertB
    imgB(:,:, 3) = 255-imgB(:,:, 3);
end
imshow(uint8(imgB), 'Parent', handles.axes3);

CommonHandles.MainView.StrechMinR = Rmin;
CommonHandles.MainView.StrechMaxR = Rmax;
CommonHandles.MainView.StrechMinG = Gmin;
CommonHandles.MainView.StrechMaxG = Gmax;
CommonHandles.MainView.StrechMinB = Bmin;
CommonHandles.MainView.StrechMaxB = Bmax;
invertR = get(handles.checkbox1, 'Value');
invertG = get(handles.checkbox2, 'Value');
invertB = get(handles.checkbox3, 'Value');


CommonHandles = setImageIntensity(CommonHandles);
CommonHandles = showSelectedCell(CommonHandles);

figHandles = findobj('Tag','showTrainedCellsFig');

for i = 1:length(figHandles)
    counter = get(figHandles(i), 'UserData');
    scrollPanel = findobj('Tag','scrollPanel','Parent',figHandles(i));
    objects = get(figHandles(i), 'Children');
    
    refreshClassImage(counter, scrollPanel);
end

function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double
% check if correct
num = str2num(get(handles.edit7, 'String'));
Bmax = get(handles.slider7,'Value');


if isempty(num)
    set(handles.edit7, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit7, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit7, 'String', '255');
    num = 255;
elseif num>Bmax
    num = Bmax;
end    

% set slider accordingly
set(handles.slider6, 'Value', num);
set(handles.edit7, 'String', num2str(num));
% reset all intensity structure

% update global changes
updateChanges(handles)


% --- Executes duri ng object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double
num = str2num(get(handles.edit8, 'String'));
Bmin = get(handles.slider6,'Value');

if isempty(num)
    set(handles.edit8, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit8, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit8, 'String', '255');
    num = 255;
elseif num < Bmin
    num = Bmin;
end    

% set slider accordingly
set(handles.slider7, 'Value', num);
set(handles.edit8, 'String', num2str(num));
% reset all intensity structure
updateChanges(handles);



% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider6_Callback(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Bmin = get(handles.slider6, 'Value');
Bmax = get(handles.slider7, 'Value');
% handle extrems
if Bmax == 0;
    set(handles.slider7, 'Value', 1);
    Bmax = get(handles.slider7,'Value');
end
if Bmin >= Bmax
    set(handles.slider6, 'Value', Bmax-1);
    Bmin = get(handles.slider6,'Value');
end

% set text
set(handles.edit7, 'String', num2str(round(Bmin)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider7_Callback(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Bmin = get(handles.slider6,'Value');
Bmax = get(handles.slider7, 'Value');
% handle extrems
if Bmin == 255;
    set(handles.slider6, 'Value', 254);
    Bmin = get(handles.slider6,'Value');
end
if Bmin >= Bmax
    set(handles.slider7, 'Value', Bmin+1);
    Bmax = get(handles.slider7,'Value');
end

% set text
set(handles.edit8, 'String', num2str(round(Bmax)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double
% check if correct
num = str2num(get(handles.edit5, 'String'));
Gmax = get(handles.slider5,'Value');


if isempty(num)
    set(handles.edit5, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit5, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit5, 'String', '255');
    num = 255;
elseif num>Gmax
    num = Gmax;
end    

% set slider accordingly
set(handles.slider4, 'Value', num);
set(handles.edit5, 'String', num2str(num));
% reset all intensity structure

% update global changes
updateChanges(handles)


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double
num = str2num(get(handles.edit6, 'String'));
Gmin = get(handles.slider4,'Value');

if isempty(num)
    set(handles.edit6, 'String', '0');
    num = 0;
elseif num < 0
    set(handles.edit6, 'String', '0');
    num = 0;
elseif num > 255
    set(handles.edit6, 'String', '255');
    num = 255;
elseif num < Gmin
    num = Gmin;
end    

% set slider accordingly
set(handles.slider5, 'Value', num);
set(handles.edit6, 'String', num2str(num));
% reset all intensity structure
updateChanges(handles);



% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider4_Callback(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Gmin = get(handles.slider4, 'Value');
Gmax = get(handles.slider5, 'Value');
% handle extrems
if Gmax == 0;
    set(handles.slider5, 'Value', 1);
    Gmax = get(handles.slider5,'Value');
end
if Gmin >= Gmax
    set(handles.slider4, 'Value', Gmax-1);
    Gmin = get(handles.slider4,'Value');
end

% set text
set(handles.edit5, 'String', num2str(round(Gmin)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider5_Callback(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

Gmin = get(handles.slider4, 'Value');
Gmax = get(handles.slider5, 'Value');
% handle extrems
if Gmin == 255;
    set(handles.slider4, 'Value', 254);
    Gmin = get(handles.slider4,'Value');
end
if Gmin >= Gmax
    set(handles.slider5, 'Value', Gmin+1);
    Gmax = get(handles.slider5,'Value');
end

% set text
set(handles.edit6, 'String', num2str(round(Gmax)));

% update global changes
updateChanges(handles);


% --- Executes during object creation, after setting all properties.
function slider5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.MainView.StrechMin = 0;
CommonHandles.MainView.StrechMax = 255;
CommonHandles.MainView.StrechMinR = 0;
CommonHandles.MainView.StrechMaxR = 255;
CommonHandles.MainView.StrechMinG = 0;
CommonHandles.MainView.StrechMaxG = 255;
CommonHandles.MainView.StrechMinB = 0;
CommonHandles.MainView.StrechMaxB = 255;
CommonHandles.MainView.SwapMap = 1;

set(handles.edit3, 'String', num2str(CommonHandles.MainView.StrechMinR));
set(handles.edit4, 'String', num2str(CommonHandles.MainView.StrechMaxR));
set(handles.edit5, 'String', num2str(CommonHandles.MainView.StrechMinG));
set(handles.edit6, 'String', num2str(CommonHandles.MainView.StrechMaxG));
set(handles.edit7, 'String', num2str(CommonHandles.MainView.StrechMinB));
set(handles.edit8, 'String', num2str(CommonHandles.MainView.StrechMaxB));

set(handles.slider2,'Value', CommonHandles.MainView.StrechMinR);
set(handles.slider3,'Value', CommonHandles.MainView.StrechMaxR);
set(handles.slider4,'Value', CommonHandles.MainView.StrechMinG);
set(handles.slider5,'Value', CommonHandles.MainView.StrechMaxG);
set(handles.slider6,'Value', CommonHandles.MainView.StrechMinB);
set(handles.slider7,'Value', CommonHandles.MainView.StrechMaxB);

set(handles.radiobutton2, 'Value', 1);

updateChanges(handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(gcbf); 
close;



% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
updateChanges(handles);


% --- Executes when selected object is changed in uipanel6.
function uipanel6_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel6 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

% bloody coding
if get(handles.radiobutton2, 'Value')
    CommonHandles.MainView.SwapMap = 1;
elseif get(handles.radiobutton3, 'Value')
    CommonHandles.MainView.SwapMap = 2;
elseif get(handles.radiobutton4, 'Value')
    CommonHandles.MainView.SwapMap = 3;
elseif get(handles.radiobutton5, 'Value')
    CommonHandles.MainView.SwapMap = 4;
elseif get(handles.radiobutton6, 'Value')
    CommonHandles.MainView.SwapMap = 5;
elseif get(handles.radiobutton7, 'Value')
    CommonHandles.MainView.SwapMap = 6;
end
    
updateChanges(handles);


% --- Executes on button press in pushbutton1.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
