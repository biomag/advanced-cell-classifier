function varargout = train_algorithmGUI(varargin)
% TRAIN_ALGORITHMGUI M-file for train_algorithmGUI.fig
%      TRAIN_ALGORITHMGUI, by itself, creates a new TRAIN_ALGORITHMGUI or raises the existing
%      singleton*.
%
%      H = TRAIN_ALGORITHMGUI returns the handle to a new TRAIN_ALGORITHMGUI or the handle to
%      the existing singleton*.
%
%      TRAIN_ALGORITHMGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAIN_ALGORITHMGUI.M with the given input arguments.
%
%      TRAIN_ALGORITHMGUI('Property','Value',...) creates a new TRAIN_ALGORITHMGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before train_algorithmGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to train_algorithmGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help train_algorithmGUI

% Last Modified by GUIDE v2.5 02-Feb-2017 13:40:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @train_algorithmGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @train_algorithmGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before train_algorithmGUI is made visible.
function train_algorithmGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to train_algorithmGUI (see VARARGIN)

% Choose default command line output for train_algorithmGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes train_algorithmGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global CommonHandles;

% set up classifier names:
%for i=1:length(CommonHandles.ClassifierNames)
    set(handles.popupmenu1, 'String', CommonHandles.ClassifierNames);
if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
    set(handles.popupmenu1, 'Value', 1);
else
    set(handles.popupmenu1, 'Value', CommonHandles.SelectedClassifier);
end

set(hObject, 'Name', 'Classification settings');


% --- Outputs from this function are returned to the command line.
function varargout = train_algorithmGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

CommonHandles.SelectedClassifier = get(handles.popupmenu1, 'Value');

h = msgbox('The model is training, please wait. In some cases this can take very long.');
hc=get(h,'Children'); 
set(hc(1),'Visible','off'); drawnow;
tic;
CommonHandles = trainClassifier(CommonHandles);
CommonHandles.TrainingTime = toc;
delete(h);

x = get(handles.checkbox1,'Value');
if x 
    h = msgbox('Measuring the performance.');
    hc=get(h,'Children'); 
    set(hc(1),'Visible','off'); drawnow;
    try
        training_model_properties_GUI;
    catch
        warndlg('Performance measurement: Please provide more training instances.');    
    end
    delete(h);
end

close(gcbf);

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(gcbf); 
close;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1
