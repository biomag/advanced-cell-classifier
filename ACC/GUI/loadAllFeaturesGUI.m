function varargout = loadAllFeaturesGUI(varargin)
% LOADALLFEATURESGUI MATLAB code for loadAllFeaturesGUI.fig
%      LOADALLFEATURESGUI, by itself, creates a new LOADALLFEATURESGUI or raises the existing
%      singleton*.
%
%      H = LOADALLFEATURESGUI returns the handle to a new LOADALLFEATURESGUI or the handle to
%      the existing singleton*.
%
%      LOADALLFEATURESGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOADALLFEATURESGUI.M with the given input arguments.
%
%      LOADALLFEATURESGUI('Property','Value',...) creates a new LOADALLFEATURESGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before loadAllFeaturesGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to loadAllFeaturesGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help loadAllFeaturesGUI

% Last Modified by GUIDE v2.5 22-Apr-2016 14:04:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @loadAllFeaturesGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @loadAllFeaturesGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before loadAllFeaturesGUI is made visible.
function loadAllFeaturesGUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to loadAllFeaturesGUI (see VARARGIN)

% Choose default command line output for loadAllFeaturesGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;
if isfield(CommonHandles,'AllFeaturesMapping') && ~isempty(CommonHandles.AllFeaturesMapping)
    set(handles.yesNo,'String','YES','foregroundcolor',[0 0 1]);
else
    set(handles.yesNo,'String','NO','foregroundcolor',[1 0 0]);
end

% UIWAIT makes loadAllFeaturesGUI wait for user response (see UIRESUME)
% uiwait(handles.loadAllFeatures);


% --- Outputs from this function are returned to the command line.
function varargout = loadAllFeaturesGUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function samplingRatio_edit_Callback(~,~,~)
% hObject    handle to samplingRatio_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of samplingRatio_edit as text
%        str2double(get(hObject,'String')) returns contents of samplingRatio_edit as a double


% --- Executes during object creation, after setting all properties.
function samplingRatio_edit_CreateFcn(hObject,~,~)
% hObject    handle to samplingRatio_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in clearEmpty_checkbox.
function clearEmpty_checkbox_Callback(~,~,~)
% hObject    handle to clearEmpty_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of clearEmpty_checkbox


% --- Executes on button press in generateSamplingPool_button.
function generateSamplingPool_button_Callback(~,~,handles)
% hObject    handle to generateSamplingPool_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

ratio = str2double(get(handles.samplingRatio_edit,'String'));

if ~isnan(ratio)
    %check whether ratio is in the correct interval
    if (ratio<=0 || ratio>1)    
        errordlg('Sampling ratio must be between 0 and 1. 0 < r <= 1','Interval error');
        set(handles.samplingRatio_edit,'String','1.0');
        return;
    end
else           
    errordlg('Please give a real number for the ratio!','Not a number error');        
    set(handles.samplingRatio_edit,'String','1.0');
    return;
end
try
    [CommonHandles.AllFeatures,CommonHandles.AllFeaturesMapping,CommonHandles.AllCoordinates] = loadAllFeatures(CommonHandles,ratio,1);
    [cleanFeatures,cleanMapping,cleanCoords] = clearEmptyRowsFromPlate(CommonHandles.AllFeatures,CommonHandles.AllFeaturesMapping,CommonHandles.AllCoordinates);
    CommonHandles.AllFeatures = cleanFeatures;
    CommonHandles.AllFeaturesMapping = cleanMapping;
    CommonHandles.AllCoordinates = cleanCoords;
catch e
    if strcmp(e.identifier, 'MATLAB:unassignedOutputs')
        return
    else
        rethrow(e)
    end
end

% Delete FindSimilarCellsFeatures if loading new feature set
if isfield(CommonHandles,'SimilarCellsFeatures')
    CommonHandles = rmfield(CommonHandles,'SimilarCellsFeatures');
end

uiresume();
close(handles.loadAllFeatures);
