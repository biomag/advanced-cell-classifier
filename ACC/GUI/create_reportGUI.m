function varargout = create_reportGUI(varargin)
% CREATE_REPORTGUI M-file for create_reportGUI.fig
%      CREATE_REPORTGUI, by itself, creates a new CREATE_REPORTGUI or raises the existing
%      singleton*.
%
%      H = CREATE_REPORTGUI returns the handle to a new CREATE_REPORTGUI or the handle to
%      the existing singleton*.
%
%      CREATE_REPORTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATE_REPORTGUI.M with the given input arguments.
%
%      CREATE_REPORTGUI('Property','Value',...) creates a new CREATE_REPORTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before create_reportGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to create_reportGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help create_reportGUI

% Last Modified by GUIDE v2.5 15-Jan-2019 20:30:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @create_reportGUI_OpeningFcn, ...
    'gui_OutputFcn',  @create_reportGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before create_reportGUI is made visible.
function create_reportGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to create_reportGUI (see VARARGIN)

% Choose default command line output for create_reportGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes create_reportGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

% fill in the lists

% plate names and select all
set(handles.listbox2, 'String', CommonHandles.PlatesNames);
set(handles.listbox2, 'Value', [1:length(CommonHandles.PlatesNames)]);

% Phenotype name list
for i=1:length(CommonHandles.Classes)
    phenotypeNames{i} = CommonHandles.Classes{i}.Name;
end

set(handles.listbox1, 'String', phenotypeNames);
set(handles.listbox3, 'String', phenotypeNames);
set(handles.listbox3, 'Value', [1:length(phenotypeNames)]);


% --- Outputs from this function are returned to the command line.
function varargout = create_reportGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% here starts the show
global CommonHandles;

CommonHandles.Report.SelectedPlates = get(handles.listbox2, 'Value');

% reclassify if nescessary -> non ratio based statistics and reclassify
% enabled


colmax = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;
rowmaxnum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;
wellcount = colmax*rowmaxnum;
rowmax = char(rowmaxnum+'A'-1);

counter = 0;
cellcounter = 0;
if ~isfield(CommonHandles.Report, 'ClassifiedPlates')
    CommonHandles.Report.ClassifiedPlates = [];
end;

waitBarHandle = waitbar(0,'Classifying cells...');

for plateidx = 1:length(CommonHandles.Report.SelectedPlates)

    outputFolder = [CommonHandles.DirName filesep 'Output'];
    if ~(exist(outputFolder,'dir') == 7)
        mkdir(outputFolder);
    end
    
    createMetadataForPlateClassification;
    
    % if not unstructured data
    if colmax > -1
        PredictionResult = zeros(colmax, rowmaxnum, length(CommonHandles.Classes));
        cellNumber = zeros(colmax, rowmaxnum);

        cellPrediction = cell(colmax, rowmaxnum);
        cellProps = cell(colmax, rowmaxnum);

        % single cell output for CAMI
        if get(handles.checkbox6, 'Value')
            % create a csv file
            singeCellFID = fopen([outputFolder filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) '_singleCellData.csv'], 'w');
            % header
            fprintf(singeCellFID, 'PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class\n');
            
            singleCellDataPerImgFolder = fullfile(outputFolder,'SingleCellDataPerImage');
            if ~exist(singleCellDataPerImgFolder,'dir'), mkdir(singleCellDataPerImgFolder); end
        end
        if get(handles.checkboxSingleCellClassProb,'Value')
            singleCellProbFID = fopen(fullfile(outputFolder,[CommonHandles.PlatesNames{CommonHandles.Report.SelectedPlates(plateidx)}  '_singleCellClassProbability.csv']),'w');
            fprintf(singleCellProbFID, 'PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class');
            for i=1:length(CommonHandles.Classes)
                fprintf(singleCellProbFID,',ClassProb_%s',CommonHandles.Classes{i}.Name); 
            end
            fprintf(singleCellProbFID,'\n');
        end
        for col = 1:colmax
            for row = 'A':rowmax
                rownum = row - 'A' + 1;
                counter = counter + 1;
                % create base file names
                colString = sprintf('%02d', col);
                FileNamePrefix =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) filesep  CommonHandles.ImageFolder filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) '*_w' row colString '*'];
                % list them
                MetadataFiles = dir(FileNamePrefix);
                cellNumberThisWell = 0;
                cpCell = {};
                propCell = {};
                for i=1:size(MetadataFiles, 1)
                    [~, fileNameexEx, ~] = fileparts(MetadataFiles(i).name);

                    [CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.Report.SelectedPlates(plateidx));

                    SelectedMetaData =  CommonHandles.SelectedMetaData;
                    %                     FileName =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) filesep  CommonHandles.MetaDataFolder filesep MetadataFiles(i).name];
                    %                     %SelectedMetaData = load(FileName);
                    %                     SelectedMetaData = hdf5read([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) filesep CommonHandles.MetaDataFolder filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) '.h5'], ['/' fileNameexEx '.tif']);
                    
                    features = SelectedMetaData(:, 3:size(SelectedMetaData, 2));

                    if CommonHandles.SALT.initialized
                        CommonHandles.SALT.trainingData.instances = features;
                        if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
                            [out, ~ ,props] = svmpredict(ones(size(features,1),1), CommonHandles.SALT.trainingData.instances, CommonHandles.SALT.model);
                            out(out<0) = 2;
                        else
                            CommonHandles.SALT.trainingData.labels = ones(size(features, 1));
                            if size(features, 2) ~= 0
                                [out,props] = sacPredict(CommonHandles.SALT.model, CommonHandles.SALT.trainingData);
                            else
                                out = [];
                                props = [];
                            end
                        end
                    else
                        errordlg('Please initalize SALT/sac!');
                        return;
                    end


                    % print single cell file for CAMI
                    if get(handles.checkbox6, 'Value')
                        j2C = cell(1,numel(out));
                        for cellIdx = 1:numel(out)
                            %Printing out to file PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class
                            fprintf(singeCellFID, '%s, %s, %d, %s, %d, %d, %f, %f, %d\n', ...
                                char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), ...
                                row, ...
                                col, ...
                                fileNameexEx,...
                                i, ...
                                cellIdx, ...
                                SelectedMetaData(cellIdx, 1),...
                                SelectedMetaData(cellIdx, 2),...
                                out(cellIdx));   
                            %Artificially creating jump2CellStructs
                            j2C{cellIdx}.CellNumber = cellIdx;
                            j2C{cellIdx}.clusterIdx = out(cellIdx);
                            j2C{cellIdx}.allClusterDistance = props(cellIdx,:);
                        end    
                        %writing for CL2M
                        colors = getManyColor();
                        singleFileToClusterCsv(j2C,fileNameexEx,length(CommonHandles.Classes),singleCellDataPerImgFolder,colors)
                    end
                    
                    %Print single cell class probabilities
                    if get(handles.checkboxSingleCellClassProb,'Value')
                        for cellIdx = 1:numel(out)
                            %Printing out to file PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class
                            fprintf(singleCellProbFID, '%s, %s, %d, %s, %d, %d, %f, %f, %d', ...
                                char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), ...
                                row, ...
                                col, ...
                                fileNameexEx,...
                                i, ...
                                cellIdx, ...
                                SelectedMetaData(cellIdx, 1),...
                                SelectedMetaData(cellIdx, 2),...
                                out(cellIdx));
                            for iii=1:size(props,2)
                                fprintf(singleCellProbFID,',%f',props(cellIdx,iii));
                            end
                            fprintf(singleCellProbFID,'\n');
                        end
                    end

                    for ii=1:size(SelectedMetaData, 1)
                        if ~isempty(out)
                            PredictionResult(col, rownum, out(ii)) = PredictionResult(col, rownum, out(ii)) + 1;
                        end;
                    end;

                    cpCell{i,1} = out;
                    propCell{i,1} = props;
                    cellcounter = cellcounter + length(out);
                    cellNumberThisWell = cellNumberThisWell + length(out);
                end;

                cellPrediction{col,rownum} = cpCell;
                cellProps{col,rownum} = propCell;

                cellNumber(col, rownum) = cellNumberThisWell;
                donePercent = double(counter/(length(CommonHandles.Report.SelectedPlates) * wellcount));
                waitText = sprintf('Classifying cells...  %d%% done ( %d cells)', int16(donePercent * 100), cellcounter);
                waitbar(donePercent, waitBarHandle, waitText);
            end
        end

        % close singel cell file for CAMI
        if get(handles.checkbox6, 'Value')
            % close single cell file
            fclose(singeCellFID);
        end
        
        if get(handles.checkboxSingleCellClassProb,'Value')
            fclose(singleCellProbFID);
        end

        CommonHandles.Report.ClassResult(CommonHandles.Report.SelectedPlates(plateidx)) = {PredictionResult};
        CommonHandles.Report.ClassifiedPlates = union(CommonHandles.Report.ClassifiedPlates, CommonHandles.Report.SelectedPlates(plateidx));
        CommonHandles.Report.CellNumber(CommonHandles.Report.SelectedPlates(plateidx)) = {cellNumber};
        CommonHandles.Report.CellPrediction(CommonHandles.Report.SelectedPlates(plateidx)) = {cellPrediction};
        CommonHandles.Report.CellProps(CommonHandles.Report.SelectedPlates(plateidx)) = {cellProps};

        %save cell number
        name = sprintf('%s_cellnumber.mat', char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))));
        name = [outputFolder filesep name];
        save(name, 'cellNumber');
        name = sprintf('%s_cellnumber.csv', char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))));
        name = [outputFolder filesep name];
        dlmwrite(name, cellNumber, ';');
    else
        % unstructured data

        % all files in the current folder
        FileNamePrefix =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) filesep  CommonHandles.ImageFolder filesep  '*' CommonHandles.ImageExtension];
        fileList = dir(FileNamePrefix);
        PredictionResult = zeros(numel(fileList), length(CommonHandles.Classes));

        % single cell output for CAMI
        if get(handles.checkbox6, 'Value')
            % create a csv file
            singeCellFID = fopen([outputFolder filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) '_singleCellData.csv'], 'w');
            % header
            fprintf(singeCellFID, 'PlateName, ImageName, ObjectNumber, xPixelPos, yPixelPos, Class\n');
            
            %CL2M separate files
            singleCellDataPerImgFolder = fullfile(outputFolder,'SingleCellDataPerImage');
            if ~exist(singleCellDataPerImgFolder,'dir'), mkdir(singleCellDataPerImgFolder); end
        end        
        
        if get(handles.checkboxSingleCellClassProb,'Value')
            singleCellProbFID = fopen(fullfile(outputFolder,[CommonHandles.PlatesNames{CommonHandles.Report.SelectedPlates(plateidx)}  '_singleCellClassProbability.csv']),'w');
            fprintf(singleCellProbFID, 'PlateName, ImageName, ObjectNumber, xPixelPos, yPixelPos, Class');
            for i=1:length(CommonHandles.Classes)
                fprintf(singleCellProbFID,',ClassProb_%s',CommonHandles.Classes{i}.Name); 
            end
            fprintf(singleCellProbFID,'\n');
        end
        
        
        fid = fopen([outputFolder filesep char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))) '.csv'], 'w');

        fprintf(fid, 'File name');

        for cl = 1:length(CommonHandles.Classes)
            fprintf(fid, ',%s', CommonHandles.Classes{cl}.Name);
        end

        fprintf(fid,'\n');
        counter = 0;
        for fIdx = 1:numel(fileList)
            counter = counter + 1;
            cellNumberThisWell = 0;
            [pathstr, fileNameexEx, ext] = fileparts(fileList(fIdx).name);
            [CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, CommonHandles.Report.SelectedPlates(plateidx));
            SelectedMetaData =  CommonHandles.SelectedMetaData;
            features = [];
            for ii=1:size(SelectedMetaData, 1)
                features(ii, :) = SelectedMetaData(ii, 3:size(SelectedMetaData, 2));
            end
            if CommonHandles.SALT.initialized
                CommonHandles.SALT.trainingData.instances = features;
                CommonHandles.SALT.trainingData.labels = ones(size(features, 1));
                if features(1 , 1) ~= 0
                    [out,props] = sacPredict(CommonHandles.SALT.model, CommonHandles.SALT.trainingData);
                else
                    out = [];
                    props = [];
                end
            else
                errordlg('Please initalize SALT/sac!');
                return;
            end
            for ii=1:size(SelectedMetaData, 1)
                if ~isempty(out)
                    PredictionResult(fIdx, out(ii)) = PredictionResult(fIdx, out(ii)) + 1;
                end
            end
            
            
                    % print single cell file for CAMI
                    if get(handles.checkbox6, 'Value')
                        j2C = cell(1,numel(out));
                        for cellIdx = 1:numel(out)
                            %Printing out to file PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class
                            fprintf(singeCellFID, '%s, %s, %d, %f, %f, %d\n', ...
                                char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), ...
                                fileNameexEx,...
                                cellIdx, ...
                                SelectedMetaData(cellIdx, 1),...
                                SelectedMetaData(cellIdx, 2),...
                                out(cellIdx));
                            %Artificially creating jump2CellStructs
                            j2C{cellIdx}.CellNumber = cellIdx;
                            j2C{cellIdx}.clusterIdx = out(cellIdx);
                            j2C{cellIdx}.allClusterDistance = props(cellIdx,:);
                        end
                        %writing for CL2M
                        colors = getManyColor();
                        singleFileToClusterCsv(j2C,fileNameexEx,length(CommonHandles.Classes),singleCellDataPerImgFolder,colors)
                    end            
                    
                    %Print single cell class probabilities
                    if get(handles.checkboxSingleCellClassProb,'Value')
                        for cellIdx = 1:numel(out)
                            %Printing out to file PlateName, Row, Col, ImageName, ImageNumber, ObjectNumber, xPixelPos, yPixelPos, Class
                            fprintf(singleCellProbFID, '%s, %s, %d, %f, %f, %d', ...
                                char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), ...
                                fileNameexEx,...
                                cellIdx, ...
                                SelectedMetaData(cellIdx, 1),...
                                SelectedMetaData(cellIdx, 2),...
                                out(cellIdx));
                            for i=1:size(props,2)
                                fprintf(singleCellProbFID,',%f',props(cellIdx,i));
                            end
                            fprintf(singleCellProbFID,'\n');
                        end
                    end
            
            
            cellcounter = cellcounter + length(out);
            cellNumberThisWell = cellNumberThisWell + length(out);

            fprintf(fid, '%s', fileList(fIdx).name);

            for cl = 1:length(CommonHandles.Classes)
                fprintf(fid, ',%d', PredictionResult(fIdx, cl));
            end

            fprintf(fid,'\n');                        

            donePercent = double(counter/numel(fileList));
            waitText = sprintf('Plate %s/%s; Classifying cells...  %d%% done ( %d cells)', num2str(plateidx),  num2str(length(CommonHandles.Report.SelectedPlates)), int16(donePercent * 100), cellcounter);
            waitbar(donePercent, waitBarHandle, waitText);
        end
        fclose(fid);
        
        % close singel cell file for CAMI
        if get(handles.checkbox6, 'Value')
            % close single cell file
            fclose(singeCellFID);
        end       
        
        if get(handles.checkboxSingleCellClassProb,'Value')
            fclose(singleCellProbFID);
        end
        
    end
end
close(waitBarHandle);


% cumulative statistics
if colmax > -1
    CommonHandles.Report.FenotypesStat = get(handles.listbox1, 'Value');
    CommonHandles.Report.FenotypesNorm = get(handles.listbox3, 'Value');
    for plateidx = 1:length(CommonHandles.Report.SelectedPlates)
        PredictionResult = cell2mat(CommonHandles.Report.ClassResult(CommonHandles.Report.SelectedPlates(plateidx)));
        norm = sum(PredictionResult(:,:,CommonHandles.Report.FenotypesNorm), 3);
        cumul = sum(PredictionResult(:,:,CommonHandles.Report.FenotypesStat), 3);
        total = sum(PredictionResult(:,:,:), 3);
        hitmap = cumul ./ norm;
        idxlist = isnan(hitmap);
        idxlist = find(idxlist == 1);
        hitmap(idxlist) = 0;


        %% create acc file
        name = sprintf('%s.csv', char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))));
        name = [outputFolder filesep name];        
        retry = 1;
        while retry            
            try
                fout = fopen(name, 'w');
                % header
                fprintf(fout, 'PlateName, Row, Col, ObjectNumber, MainHitrate');
                for cl = 1:length(CommonHandles.Classes)
                    fprintf(fout, ',%s', CommonHandles.Classes{cl}.Name);
                end
                fprintf(fout,'\n');

                % data
                for i=1:size(norm, 1)
                    for j=1:size(norm, 2)
                        fprintf(fout, '%s, %s, %d, %d, %f', char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), char(j+'A'-1), i, total(i, j), hitmap(i, j));
                        for cl = 1:length(CommonHandles.Classes)
                            fprintf(fout, ',%d', PredictionResult(i, j, cl));
                        end
                        fprintf(fout,'\n');
                    end
                end
                fclose(fout);
                retry = 0;
            catch
                answer = questdlg(['Failed to open file: ' name],'Access denied','Try again','Cancel','Try again');
                if ~strcmp(answer,'Try again')
                    retry = 0;
                end
            end
        end
        %save statistics
        filename = sprintf('%s_cumul', char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))));
        for ft = 1:length(CommonHandles.Report.FenotypesStat)
            filename = sprintf('%s_%d', filename, CommonHandles.Report.FenotypesStat(ft));
        end;
        filename = sprintf('%s_v', filename);
        for ft = 1:length(CommonHandles.Report.FenotypesNorm)
            filename = sprintf('%s_%d', filename, CommonHandles.Report.FenotypesNorm(ft));
        end;
        name = sprintf('%s.mat', filename);
        name = [outputFolder filesep name];
        save(name, 'hitmap');
        name = sprintf('%s.csv', filename);
        name = [outputFolder filesep name];
        dlmwrite(name, hitmap, ';');

        %%% cumulative ratio based
        outName = [outputFolder filesep filename '.pdf']; 
        h1 = figure(1);
        try
            createPDFReport(outName, char(CommonHandles.PlatesNames(CommonHandles.Report.SelectedPlates(plateidx))), hitmap, norm, h1);
        catch
            errordlg('Please select the right plate size!');
            close(h1);
        end

    end

%     if license('test', 'matlab_report_gen')
%         report('platereport_cumul.rpt')
%     end
end

tempSplittedImageName = strsplit(CommonHandles.SelectedImageName,{filesep,'.'});
[CommonHandles.SelectedMetaDataFileName,CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, char(tempSplittedImageName(end-1)), CommonHandles.SelectedPlate);

uiresume(gcbf);
close(gcbf);
% show is over... :)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(gcbf);
close;

% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% this function creates a hit report called hit_report_phenotypes_date_threshold.csv

% input threshold level


% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


% --- Executes on button press in checkboxSingleCellClassProb.
function checkboxSingleCellClassProb_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxSingleCellClassProb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxSingleCellClassProb
