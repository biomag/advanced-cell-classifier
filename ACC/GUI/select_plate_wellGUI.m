function varargout = select_plate_wellGUI(varargin)
% SELECT_PLATE_WELLGUI M-file for select_plate_wellGUI.fig
%      SELECT_PLATE_WELLGUI, by itself, creates a new SELECT_PLATE_WELLGUI or raises the existing
%      singleton*.
%
%      H = SELECT_PLATE_WELLGUI returns the handle to a new SELECT_PLATE_WELLGUI or the handle to
%      the existing singleton*.
%
%      SELECT_PLATE_WELLGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SELECT_PLATE_WELLGUI.M with the given input arguments.
%
%      SELECT_PLATE_WELLGUI('Property','Value',...) creates a new SELECT_PLATE_WELLGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before select_plate_wellGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to select_plate_wellGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help select_plate_wellGUI

% Last Modified by GUIDE v2.5 14-Jan-2019 16:43:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @select_plate_wellGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @select_plate_wellGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before select_plate_wellGUI is made visible.
function select_plate_wellGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to select_plate_wellGUI (see VARARGIN)

% Choose default command line output for select_plate_wellGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes select_plate_wellGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global CommonHandles;

CommonHandles.PlateListHandle = handles.listbox1;
CommonHandles.ImageListHandle = handles.listbox2;
set(handles.listbox1,'Value',1);

LoadPlateList(handles);

% --- Outputs from this function are returned to the command line.
function varargout = select_plate_wellGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
global CommonHandles;

RefreshImageList(handles);


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

global CommonHandles;
CommonHandles.SelectedImage = get(handles.listbox2,'Value');
ImageList = get(handles.listbox2,'String');

tempSelectedImage = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
[~, fileNameexEx, ~] = fileparts(tempSelectedImage);
[~,tempSelectedMetaData] = readMetaData(CommonHandles, fileNameexEx);

first = size(tempSelectedMetaData,1);
second = size(tempSelectedMetaData,2);
if first==1 || second==1
    temp = zeros(first,second);
else
    temp = 0;
end
if ~isequal(tempSelectedMetaData,temp)
    
    CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
    CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];
    
    % showimage funciton call
    CommonHandles = loadSelectedImage(CommonHandles);
else
    indx = getNextNonEmptyImage();
    set(handles.listbox2,'Value',indx);
    
    CommonHandles.SelectedImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(ImageList(indx))];
    CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(indx))];
    
    % showimage funciton call
    CommonHandles = loadSelectedImage(CommonHandles);
end

% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


function LoadPlateList(handles)
global CommonHandles;

% set the folders
set(handles.listbox1, 'String', CommonHandles.PlatesNames);

RefreshImageList(handles);


function RefreshImageList(handles)
global CommonHandles;

CommonHandles.SelectedPlate = get(handles.listbox1,'Value');
CommonHandles.SelectedImage = 1;

%ImageList = dir([CommonHandles.DirName filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.ImageFolder filesep char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) '*' CommonHandles.ImageExtension]);
% removed plate name from the file name
ImageList = dir(fullfile(CommonHandles.DirName, char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)), CommonHandles.ImageFolder, ['*',CommonHandles.ImageExtension]));

ImageListStr = {ImageList.name}';
set(handles.listbox2, 'String', ImageListStr);
set(handles.listbox2, 'Value', CommonHandles.SelectedImage);
ImageList = get(handles.listbox2,'String');

CommonHandles.SelectedImageName = fullfile(char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)), CommonHandles.ImageFolder, char(ImageList(CommonHandles.SelectedImage)));
CommonHandles.SelectedOriginalImageName = [char(CommonHandles.PlatesNames(CommonHandles.SelectedPlate)) filesep CommonHandles.OriginalImageFolder filesep char(ImageList(CommonHandles.SelectedImage))];

% showimage function call
CommonHandles = loadSelectedImage(CommonHandles);


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(hObject);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

figPos = get(hObject,'Position');
%{
newFigPos = figPos;
if figPos(4)<300    
    newFigPos(4) = 301;
    set(hObject,'Position',newFigPos);
end
if figPos(3)<200    
    newFigPos(3) = 201;
    set(hObject,'Position',newFigPos);
end
%}

if figPos(4) - 224>1, handles.listbox2.Position(4) = figPos(4) - 224; end %224 is the original space for the top
if figPos(3)-16>1
    handles.listbox2.Position(3) = figPos(3) - 16; %padding on both sides
    handles.listbox1.Position(3) = figPos(3) - 16; %padding on both sides
end
if figPos(4)-181>1
    handles.listbox1.Position(2) = figPos(4) - 181;
end
if figPos(4)-21>1
handles.text1.Position(2) = figPos(4) -  21;
end
if figPos(4)-206>1
    handles.text2.Position(2) = figPos(4) -  206;
end
