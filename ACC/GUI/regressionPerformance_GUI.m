function varargout = regressionPerformance_GUI(varargin)
% REGRESSIONPERFORMANCE MATLAB code for RegressionPerformance.fig
%      REGRESSIONPERFORMANCE, by itself, creates a new REGRESSIONPERFORMANCE or raises the existing
%      singleton*.
%
%      H = REGRESSIONPERFORMANCE returns the handle to a new REGRESSIONPERFORMANCE or the handle to
%      the existing singleton*.
%
%      REGRESSIONPERFORMANCE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REGRESSIONPERFORMANCE.M with the given input arguments.
%
%      REGRESSIONPERFORMANCE('Property','Value',...) creates a new REGRESSIONPERFORMANCE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RegressionPerformance_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RegressionPerformance_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RegressionPerformance

% Last Modified by GUIDE v2.5 30-May-2017 09:24:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @regressionPerformance_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @regressionPerformance_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before regressionPerformance_GUI is made visible.
function regressionPerformance_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RegressionPerformance (see VARARGIN)

%Varargin order:
%   1:  The cellarray of performance change from
%   CommonHandles.RegressionPlane{idx}.CrossValidation.PerformanceChange
%   2:  The ground truth positions
%   3:  The predicted positions
%   4:  The number of folds
%   5:  Image names
%   6:  Cell numbers

% Choose default command line output for RegressionPerformance
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RegressionPerformance wait for user response (see UIRESUME)
% uiwait(handles.figure1);

uData = get(handles.figure1,'UserData');

performanceChange = varargin{1};
uData.predictions = varargin{2};
uData.targets = varargin{3};
nofFolds = varargin{4};
uData.imageName = varargin{5};
uData.cellNumber = varargin{6};

infoStr = sprintf(['RMSE with %d-fold cross validation is:\n'...
                   '%f\n'...
                   '***********************\n'...
                   'Relative RMSE:\n'...
                   '%f\n'...                   
                   '(%4.2f%% better than predicting mean for every cell)'],...
    nofFolds,performanceChange{end}.RMSE/1000,performanceChange{end}.RRMSE,-(performanceChange{end}.RRMSE-1)*100);
set(handles.RMSEText,'String',infoStr);

set(handles.figure1,'UserData',uData);



% --- Outputs from this function are returned to the command line.
function varargout = regressionPerformance_GUI_OutputFcn(~, ~, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_saveCSV.
function button_saveCSV_Callback(~, ~, handles) %#ok<DEFNU> called from gui
% hObject    handle to button_saveCSV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

global CommonHandles

filePath = uigetdir(CommonHandles.DirName, 'Specify a location to save the errors');

if filePath == 0    
    return;
end

fileName = 'RegressionPlaneErrorVectors.csv';

try
    fileID = fopen(fullfile(filePath,fileName),'w');

    %print header
    fprintf(fileID,'%s, %s, %s, %s, %s, %s, %s\n','Image name','Cell number','Training X','Training Y','Predicted X', 'Predicted Y','Euclidean distance');
    
    for i=1:length(uData.predictions)                
        fprintf(fileID,'%s, %d, %f, %f, %f, %f, %f\n',...
            uData.imageName{i},...
            uData.cellNumber{i},...
            uData.targets(1,i)/1000,...
            uData.targets(2,i)/1000,...  
            uData.predictions(1,i)/1000,...
            uData.predictions(2,i)/1000,...
            pdist2(uData.targets(:,i)',uData.predictions(:,i)')/1000 ...
            );        
    end
    fclose(fileID);
catch e
    disp(e.message);
    fclose(fileID);
    warndlg(['An error occured during the save. Make sure that the previous ''' fileName ''''...
        ' at your desired location: ' filePath ' is closed.']);
    return;
end

msgbox(['The regression errors were successfully saved to: '...
    fullfile(filePath,fileName)
    ]);
