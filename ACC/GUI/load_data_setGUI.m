function varargout = load_data_setGUI(varargin)
% LOAD_DATA_SETGUI M-file for load_data_setGUI.fig
%      LOAD_DATA_SETGUI, by itself, creates a new LOAD_DATA_SETGUI or raises the existing
%      singleton*.
%
%      H = LOAD_DATA_SETGUI returns the handle to a new LOAD_DATA_SETGUI or the handle to
%      the existing singleton*.
%
%      LOAD_DATA_SETGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LOAD_DATA_SETGUI.M with the given input arguments.
%
%      LOAD_DATA_SETGUI('Property','Value',...) creates a new LOAD_DATA_SETGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before load_data_setGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to load_data_setGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help load_data_setGUI

% Last Modified by GUIDE v2.5 14-Oct-2019 22:02:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @load_data_setGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @load_data_setGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before load_data_setGUI is made visible.
function load_data_setGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to load_data_setGUI (see VARARGIN)

% Choose default command line output for load_data_setGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;

% read in the existing plate type names
for i=1:numel(CommonHandles.PlateTypeInfo)
    plateTypeName{i} = CommonHandles.PlateTypeInfo{i}.name;
end

set(handles.popupmenu1, 'String', plateTypeName);

for i=1:numel(CommonHandles.ImageExtensionNames)
    ImageExtensionName{i} = CommonHandles.ImageExtensionNames{i}.name;
end

set(handles.popupmenu2, 'String', ImageExtensionName);
set(hObject, 'Name', 'Load a dataset');

% UIWAIT makes load_data_setGUI wait for user response (see UIRESUME)
uiwait(handles.loadDataSetFigure);


% --- Outputs from this function are returned to the command line.
function varargout = load_data_setGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
varargout{2} = handles.exitMessage;

delete(handles.loadDataSetFigure)


function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a
%        double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

if isfield(CommonHandles,'DirName')
    newFolder = uigetdir(CommonHandles.DirName);
else
    newFolder = uigetdir();
end

% remove zero when cancelled
if newFolder == 0
    set(handles.edit1, 'String', '');
else
    set(handles.edit1, 'String', newFolder);
end

% load plate list if filename selection was successfull
if newFolder ~= 0
    
    [ errorCode, DirListStr ] = checkProjectFolder(newFolder,1,get(handles.edit2, 'String'),get(handles.edit3, 'String'),get(handles.edit4, 'String'));
    
    if ~errorCode
        set(handles.listbox1, 'String', DirListStr);
        set(handles.listbox1, 'Value', 1:length(DirListStr));
        CommonHandles.DirName = newFolder;
    else
        set(handles.edit1, 'String', '');
        if (errorCode == 1)
            warndlg({'The selected path does not contain any folders! (plate folders)'});        
        elseif (errorCode == 2)
            warndlg({'The selected path does not contain the folders declared!'});        
        end
    end
        
                
end



% --- Executes on button press in pushbutton2.
% This is OK button which fires the loading based on the given data in the
% GUI.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;
addpath('Utils');

CommonHandles.DirName = get(handles.edit1, 'String');
CommonHandles.ImageFolder = get(handles.edit2, 'String');
CommonHandles.MetaDataFolder = get(handles.edit3, 'String');
CommonHandles.OriginalImageFolder = get(handles.edit4, 'String');
CommonHandles.WellType = get(handles.popupmenu1, 'Value');
imageExtensionList = get(handles.popupmenu2, 'String');
CommonHandles.ImageExtension = char(imageExtensionList(get(handles.popupmenu2, 'Value')));
CommonHandles.DataStructureType = get(handles.popupmenu3, 'Value');
CommonHandles.CutSize = str2double(get(handles.editBox_CutSize,'String'));

CommonHandles.PlatesNames = {};
CommonHandles.SelectedPlate = 1;
CommonHandles.SelectedImage = 1;

% read selected folders
if (length(CommonHandles.DirName) > 1) && (~isempty(CommonHandles.ImageFolder)) && (~isempty(CommonHandles.MetaDataFolder))
    DirListStr = get(handles.listbox1, 'String');    
    SelectedPlateList = get(handles.listbox1, 'Value');    
    if ~isempty(SelectedPlateList)
        for i=1:length(SelectedPlateList)
            CommonHandles.PlatesNames(i) = DirListStr(SelectedPlateList(i));
        end
        CommonHandles.Success = 1;
    else
        CommonHandles.Success = 0;    
    end
else
    CommonHandles.Success = 0;
end

flag_imgExtension = 0;
flag_dataStructure = 0;
if CommonHandles.Success == 1
    
    imageList = dir([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.ImageFolder]);
    for ii=3:size(imageList,1)
        if length(imageList(ii).name) > length(CommonHandles.ImageExtension)
            tmp = findstr(imageList(ii).name,CommonHandles.ImageExtension);
            if ~isempty(tmp)
                flag_imgExtension = 1;
            end
        end
    end
    
    if CommonHandles.DataStructureType == 1
        dsType = '.txt';
    elseif CommonHandles.DataStructureType == 2
        dsType = '.h5';
    end
    
    metaDataList = dir([CommonHandles.DirName filesep CommonHandles.PlatesNames{1} filesep CommonHandles.MetaDataFolder]);
    for ii=3:size(metaDataList,1)
       if length(metaDataList(ii).name) > length(dsType)
           tmp = findstr(metaDataList(ii).name,dsType);
           if ~isempty(tmp)
               flag_dataStructure = 1;
           end
       end
    end
    
    if flag_imgExtension == 0
        warndlg({'This type of image does not exist in the dataset.'});
        CommonHandles.Success = 0;
    elseif flag_dataStructure == 0
        warndlg({'This structure of data does not exist in the dataset.'});
        CommonHandles.Success = 0;
    elseif flag_imgExtension == 1 && flag_dataStructure == 1
        
        if isfield(CommonHandles,'AllFeatures')
            CommonHandles = rmfield(CommonHandles,'AllFeatures');
        end
        if isfield(CommonHandles,'AllFeaturesMapping')
            CommonHandles = rmfield(CommonHandles,'AllFeaturesMapping');
        end
        if isfield(CommonHandles,'AllCoordinates')
            CommonHandles = rmfield(CommonHandles,'AllCoordinates');
        end
        if isfield(CommonHandles,'AllFeaturesReduced')
            CommonHandles = rmfield(CommonHandles,'AllFeaturesReduced');
        end
        
        % call reset function with the handles object of the main window
        resetCommonHandlesToDefault();
               
        refreshClassesList();
        
        handles = guidata(hObject);
        handles.exitMessage = 'OK';
        guidata(hObject,handles);
        
        uiresume(handles.loadDataSetFigure);
    end    
end



% --------------------------------------------------------------------
function uipushtool1_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object deletion, before destroying properties.
function loadDataSetFigure_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to loadDataSetFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(hObject);


% --- Executes when user attempts to close loadDataSetFigure.
function loadDataSetFigure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to loadDataSetFigure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global CommonHandles;

if isfield(CommonHandles,'Success')
    if CommonHandles.Success == 1
        CommonHandles.Success = 1;
    else
        CommonHandles.Success = 0;
    end
end

handles = guidata(hObject);
handles.exitMessage = 'Closed';
guidata(hObject,handles);

uiresume(handles.loadDataSetFigure);



function editBox_CutSize_Callback(hObject, eventdata, handles)
% hObject    handle to editBox_CutSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBox_CutSize as text
%        str2double(get(hObject,'String')) returns contents of editBox_CutSize as a double

possNum = str2double(get(handles.editBox_CutSize,'String'));

[bool,msg] = checkNumber( possNum, 1, 1, [1,Inf], 'Cut size');

global CommonHandles;
if ~bool    
    set(handles.editBox_CutSize,'String',num2str(CommonHandles.CutSize));
    warndlg(msg);
end


% --- Executes during object creation, after setting all properties.
function editBox_CutSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBox_CutSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
