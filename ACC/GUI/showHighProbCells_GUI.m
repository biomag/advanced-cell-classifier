function varargout = showHighProbCells_GUI(varargin)
% SHOWHIGHPROBCELLS_GUI MATLAB code for showHighProbCells_GUI.fig
%      SHOWHIGHPROBCELLS_GUI, by itself, creates a new SHOWHIGHPROBCELLS_GUI or raises the existing
%      singleton*.
%
%      H = SHOWHIGHPROBCELLS_GUI returns the handle to a new SHOWHIGHPROBCELLS_GUI or the handle to
%      the existing singleton*.
%
%      SHOWHIGHPROBCELLS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SHOWHIGHPROBCELLS_GUI.M with the given input arguments.
%
%      SHOWHIGHPROBCELLS_GUI('Property','Value',...) creates a new SHOWHIGHPROBCELLS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before showHighProbCells_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to showHighProbCells_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help showHighProbCells_GUI

% Last Modified by GUIDE v2.5 27-Jul-2016 11:55:55

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @showHighProbCells_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @showHighProbCells_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before showHighProbCells_GUI is made visible.
function showHighProbCells_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to showHighProbCells_GUI (see VARARGIN)

% Choose default command line output for showHighProbCells_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;

everyCell = CommonHandles.AllFeatures;
mapping = CommonHandles.AllFeaturesMapping;
coords = CommonHandles.AllCoordinates;

mapping = mapping(~cellfun('isempty',mapping));

%predict all cells
[out,props] = predictClassifier(everyCell,CommonHandles);

showStruct = get(hObject, 'UserData');

if size(props,2)>=showStruct.classID
    [~,index] = sort(props(:,showStruct.classID));
else    
    errordlg('Your classifier was trained on different classes. Train it again!');        
    handles.imgMapping = [];
    handles.uploadStructure = [];
    showStruct.failure = 1;
    set(hObject,'UserData',showStruct);
    guidata(hObject, handles);
    return;
end

sortedOut = out(index);

sortedClassIndices = sortedOut==showStruct.classID;

classIndices = index(sortedClassIndices);

classIndices = flip(classIndices);

toShowClassID = showStruct.classID;

if length(classIndices)<showStruct.numOfCellsToShow && ~isempty(classIndices)
    warndlg(['Sorry, there were less than ' num2str(showStruct.numOfCellsToShow) ' cells classified into class ''' CommonHandles.Classes{toShowClassID}.Name '''.']);    
    sortedIndex = classIndices;
    showStruct.numOfCellsToShow = length(classIndices);
elseif isempty(classIndices)
    errordlg(['Operation canceled because 0 cell was predicted to class ''' CommonHandles.Classes{toShowClassID}.Name '''. Try to put more cells to the selected class or train a classifier again!']);            
    handles.imgMapping = [];
    handles.uploadStructure = [];
    showStruct.failure = 1;
    set(hObject,'UserData',showStruct);
    guidata(hObject, handles);
    return;
else
    sortedIndex = classIndices(1:showStruct.numOfCellsToShow);
end


uploadStructure.Img = {};
uploadStructure.FeatureVector = [];
uploadStructure.ImageName = {};
uploadStructure.SelectedCell = {};
uploadStructure.PlateName = {};
uploadStructure.Coord = {};

for i=1:showStruct.numOfCellsToShow
    
    currentCell = sortedIndex(i);
    
    % TODO remove this function and use jumptocell instead
    % (getCurrentCellImage)
    cellImage = getCurrentCellImage(mapping{currentCell}.ImageName, coords(currentCell,:));
    
    uploadStructure.Img{i} = cellImage;
    uploadStructure.FeatureVector{i} = everyCell(currentCell,:);
    uploadStructure.ImageName{i} = mapping{currentCell}.ImageName;
    uploadStructure.SelectedCell{i} = mapping{currentCell}.CellNumberInImage;
    uploadStructure.PlateName{i} = CommonHandles.PlatesNames{mapping{currentCell}.CurrentPlateNumber};
    uploadStructure.Coord{i} = coords(currentCell,:);
    
end

set(handles.showHighProbCells,'Name', sprintf('Cells with the highest probability of belonging to class %s', CommonHandles.Classes{toShowClassID}.Name));

hUicontextMenu = uicontextmenu('Parent',handles.showHighProbCells);
uimenu('Parent',hUicontextMenu,'Label','Add/edit comment to the cell','Callback',@editCommentOnUpload);
handles.hUicontextMenu = hUicontextMenu;

[imgs, imgMapping] = createImagesForHighProb(uploadStructure);
hAxes = axes('Parent',handles.showHighProbCells);
hImShow = imshow(imgs,'Parent',hAxes);
set(hImShow, 'UIContextMenu',hUicontextMenu);

hSP = imscrollpanel(handles.showHighProbCells,hImShow);

set(hSP,'Units','pixels','Position',[10 60 720 560],'Tag','scrollPanel');

set(handles.showHighProbCells,'Position', [0 0 750 700]);
infoPanelPos = get(handles.infoPanel,'Position');
set(handles.infoPanel,'Position', [20 630 infoPanelPos(3) infoPanelPos(4)]);

api = iptgetapi(hSP);
api.setVisibleLocation([0 0]);
api.setImageButtonDownFcn({@scrollPanel_ButtonDownFcn,hSP} );

haxes = findobj(hSP,'Type','Axes');

% Draw borders around the image
lw = 3;
imgxsize = CommonHandles.ClassImageInfo.ImageSize(1);
imgysize = CommonHandles.ClassImageInfo.ImageSize(2);
sepsize = CommonHandles.ClassImageInfo.SepSize;

cols = CommonHandles.ClassImageInfo.Cols;

for i=1:showStruct.numOfCellsToShow
    c = mod(i,cols);
    r = floor(i/cols)+1;
    if c==0
        c=cols;
        r=r-1;
    end
    
    drawX = (c-1) * imgxsize + c * sepsize - lw + 1;
    drawY = (r-1) * imgysize + r * sepsize - lw + 1;
    hRec = rectangle('Position',[drawX, drawY, imgxsize+2*lw-2, imgysize+2*lw-2],'EdgeColor', 'red', 'LineWidth',lw, 'Parent', haxes);

    imgMapping{r,c}.fHandle = hRec;
end

handles.imgMapping = imgMapping;
handles.uploadStructure = uploadStructure;

handles.classID = showStruct.classID;

% Update handles structure
guidata(hObject, handles);



% --- Outputs from this function are returned to the command line.
function varargout = showHighProbCells_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes during object creation, after setting all properties.
function showHighProbCells_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showHighProbCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



% --- Executes when user attempts to close showHighProbCells.
function showHighProbCells_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to showHighProbCells (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    % Hint: delete(hObject) closes the figure
    delete(hObject);
end



% --- Executes on click over scrollPanel.
function scrollPanel_ButtonDownFcn(hObject,eventdata,handles)
% hObject    handle to scrollPanel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles = guidata(hObject);
imgMapping = handles.imgMapping;

%x = eventdata.IntersectionPoint(1);
%y = eventdata.IntersectionPoint(2);
% dummy code needed for old matlab
curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);

[selimgInfo,hit,xcoord,ycoord] = selectCellFromClassImage(x,y,imgMapping);

if hit && ~isempty(imgMapping{ycoord,xcoord}) && ~strcmp(get(handles.showHighProbCells,'SelectionType'),'alt')
    
    if imgMapping{ycoord,xcoord}.Selected
        imgMapping{ycoord,xcoord}.Selected = 0;
        set(imgMapping{ycoord,xcoord}.fHandle, 'Visible','off');
    else
        imgMapping{ycoord,xcoord}.Selected = 1;
        set(imgMapping{ycoord,xcoord}.fHandle, 'Visible','on');
    end
 
end

handles.imgMapping = imgMapping;

guidata(hObject, handles);


% --- Executes on button press in selectAllButton.
function selectAllButton_Callback(hObject, eventdata, handles)
% hObject    handle to selectAllButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgMapping = handles.imgMapping;

for i=1:size(imgMapping,1)
    for j=1:size(imgMapping,2)
        if ~isempty(imgMapping{i,j})
            imgMapping{i,j}.Selected = 1;
            set(imgMapping{i,j}.fHandle,'Visible', 'on');
        end
    end
end

handles.imgMapping = imgMapping;

guidata(hObject, handles);


% --- Executes on button press in deselectAllButton.
function deselectAllButton_Callback(hObject, eventdata, handles)
% hObject    handle to deselectAllButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

imgMapping = handles.imgMapping;

for i=1:size(imgMapping,1)
    for j=1:size(imgMapping,2)
        if ~isempty(imgMapping{i,j})
            imgMapping{i,j}.Selected = 0;
            set(imgMapping{i,j}.fHandle,'Visible', 'off');
        end
    end
end

handles.imgMapping = imgMapping;

guidata(hObject, handles);


% --- Executes on press edit comment contextmenu
function editCommentOnUpload(hObject, eventdata, handles)
% EDITCOMMENTONUPLOAD opens an input dialog to set a comment to a specific
% cell before uploading to database

handles = guidata(hObject);
imgMapping = handles.imgMapping;

curpoint = get(gca,'CurrentPoint');
x = curpoint(1,1,1);
y = curpoint(1,2,1);

[selimgInfo,hit,xcoord,ycoord] = selectCellFromClassImage(x,y,imgMapping);
set(selimgInfo.fHandle, 'Visible', 'on');
set(selimgInfo.fHandle, 'EdgeColor', [ 1 1 0]);

[~,imageBaseName,ext] = fileparts(selimgInfo.ImageName);

prompt = {sprintf('Add comment on upload to\n cell number %d\n in image %s\n in plate %s\n',selimgInfo.CellNumber,[imageBaseName ext],selimgInfo.PlateName)};
dlgTitle = '';
numOfLines = 1;
if isfield(selimgInfo, 'CommentOnUpload') 
    defaultAnswer = {selimgInfo.CommentOnUpload};
else
    defaultAnswer = {''};
end
answer = inputdlg(prompt, dlgTitle, numOfLines, defaultAnswer);
if ~isempty(answer)
    imgMapping{ycoord,xcoord}.CommentOnUpload = answer{1};
end

if selimgInfo.Selected
    set(selimgInfo.fHandle, 'Visible', 'on');
else
    set(selimgInfo.fHandle, 'Visible', 'off');
end
set(selimgInfo.fHandle, 'EdgeColor', [1 0 0]);

handles.imgMapping = imgMapping;
guidata(hObject, handles);


% --- Executes on button press in uploadButton.
function uploadButton_Callback(hObject, eventdata, handles)
% hObject    handle to uploadButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
if CommonHandles.CAMI.Network.UploadServiceIsWorking
    tempMapping = cat(1,handles.imgMapping{:});
    isAnySelected = any(cat(1,tempMapping.Selected));
    if ~isempty(handles.imgMapping) && isAnySelected
        uploadExperiment(handles.imgMapping, handles.uploadStructure, handles.classID);
    else
        warndlg('There are no selected cells!');
    end
else
    warndlg('Remote upload service is not running!');
end
