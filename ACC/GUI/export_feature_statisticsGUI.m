 function varargout = export_feature_statisticsGUI(varargin)
% EXPORT_FEATURE_STATISTICSGUI M-file for export_feature_statisticsGUI.fig
%      EXPORT_FEATURE_STATISTICSGUI, by itself, creates a new EXPORT_FEATURE_STATISTICSGUI or raises the existing
%      singleton*.
%
%      H = EXPORT_FEATURE_STATISTICSGUI returns the handle to a new EXPORT_FEATURE_STATISTICSGUI or the handle to
%      the existing singleton*.
%
%      EXPORT_FEATURE_STATISTICSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EXPORT_FEATURE_STATISTICSGUI.M with the given input arguments.
%
%      EXPORT_FEATURE_STATISTICSGUI('Property','Value',...) creates a new EXPORT_FEATURE_STATISTICSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before export_feature_statisticsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to export_feature_statisticsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help export_feature_statisticsGUI

% Last Modified by GUIDE v2.5 19-Feb-2016 21:20:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @export_feature_statisticsGUI_OpeningFcn, ...
    'gui_OutputFcn',  @export_feature_statisticsGUI_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before export_feature_statisticsGUI is made visible.
function export_feature_statisticsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to export_feature_statisticsGUI (see VARARGIN)

% Choose default command line output for export_feature_statisticsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes export_feature_statisticsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global CommonHandles;

% fill in the lists

% plate names and select all
set(handles.listbox2, 'String', CommonHandles.PlatesNames);
set(handles.listbox2, 'Value', [1:length(CommonHandles.PlatesNames)]);

featureList = getFeatureList();

CommonHandles.featureList = featureList;

set(handles.listbox1, 'String', featureList);



% --- Outputs from this function are returned to the command line.
function varargout = export_feature_statisticsGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

% selected features
selectedFeatures = get(handles.listbox1, 'value');
CommonHandles.Report.SelectedPlates = get(handles.listbox2, 'Value');
colmax = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;
rowmax = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;
wellcount = colmax*rowmax;
rowmax = char(rowmax+'A'-1);

cellcounter = 0;
numfeatures = 0;
numselplates = length(CommonHandles.Report.SelectedPlates);

waitBarHandle = waitbar(0,'Calculating features...');

for piter = 1:numselplates
    plateidx = CommonHandles.Report.SelectedPlates(piter);
    if length(CommonHandles.Report.CellPrediction) < plateidx || isempty(CommonHandles.Report.CellPrediction{plateidx})
        uiwait(errordlg(['Skipping plate ' char(CommonHandles.PlatesNames(plateidx)) '. The plate is not predicted.'],'Skip plate'));
        continue
    end
    counter = 0;
    % structured data
    if colmax > -1
        cellNumber = zeros(colmax, rowmax-'A'+1);
        for col = 1:colmax
            for row = 'A':rowmax
                counter = counter + 1;
                rownum = row-'A'+1;
                
                % create base file names
                colString = sprintf('%02d', col);
                FileNamePrefix =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateidx)) filesep  CommonHandles.ImageFolder filesep char(CommonHandles.PlatesNames(plateidx)) '*_w' row colString '*'];
                % list them
                MetadataFiles = dir(FileNamePrefix);
                cellNumberThisWell = 0;
                features = [];
                for i=1:size(MetadataFiles, 1)
                    [pathstr, fileNameexEx, ext] = fileparts(MetadataFiles(i).name);
                    
                    [CommonHandles.SelectedMetaDataFileName, CommonHandles.SelectedMetaData] = readMetaData(CommonHandles, fileNameexEx, plateidx);
                    selectedMetaData = CommonHandles.SelectedMetaData;
                    numberOfCells = size(selectedMetaData, 1);
                    
                    for ii=1:size(selectedMetaData, 1)
                        cellNumberThisWell = cellNumberThisWell + 1;                    
                        features(cellNumberThisWell, :) = selectedMetaData(ii, selectedFeatures);
                    end
                    
                    cellcounter = cellcounter + numberOfCells;
                end
                
                if size(features,2) > numfeatures
                    numfeatures = size(features,2);
                end
                
                % calculate mean and other stats
                featureCells(col, rownum) = size(features,1);
                featureMeans{col, rownum} = mean(features,1);
                if (get(handles.checkbox2, 'Value'))
                    featureMedians{col, rownum} = median(features,1);
                    if size(features,1) == 1
                        featureStds{col, rownum} = mean(features,1);
                        featureMads{col, rownum} = median(features,1);
                    else
                        featureStds{col, rownum} = std(features,1);
                        featureMads{col, rownum} = mad(features,1);
                    end
                end
                
                % calculate correlations between feature pairs
                if (get(handles.radiobutton2, 'Value'))
                    featureCorrs{col, rownum} = corrcoef(features);
                end
                
                % Calculate class specific mean and other stats if
                % requested
                if (get(handles.checkbox6, 'Value'))
                    cellClasses = vertcat(CommonHandles.Report.CellPrediction{plateidx}{col,row-'A'+1}{:});
                    for c=1:size(CommonHandles.Classes,2)
                        fn = @(x)(x==c);
                        cfeat = features(fn(cellClasses),:);
                        featureCellsClasses{col, rownum, c} = size(cfeat,1);
                        featureMeansClasses{col, rownum, c} = mean(cfeat,1);
                        if (get(handles.checkbox2, 'Value'))
                            featureMediansClasses{col, rownum, c} = median(cfeat,1);
                            if size(cfeat,1) == 1
                                featureStdsClasses{col, rownum, c} = mean(cfeat,1);
                                featureMadsClasses{col, rownum, c} = median(cfeat,1);
                            else
                                featureStdsClasses{col, rownum, c} = std(cfeat,1);
                                featureMadsClasses{col, rownum, c} = mad(cfeat,1);
                            end
                        end
                    end
                end
                
                donePercent = double(counter/wellcount);
                waitText = sprintf('Calculating features...  %d%% done ( %d cells)', int16(donePercent*100), cellcounter);
                waitbar((piter-1)/numselplates + donePercent*0.5/numselplates, waitBarHandle, waitText);
            end
        end
        
        % Check that number of features is the same
        nans = NaN(1,numfeatures);
        for col = 1:colmax
            for row = 'A':rowmax
                rownum = row-'A'+1;
                if size(featureMeans{col, rownum},2) < numfeatures
                    featureMeans{col, rownum} = nans;
                    if (get(handles.checkbox2, 'Value'))
                        featureMedians{col, rownum} = nans;
                        featureStds{col, rownum} = nans;
                        featureMads{col, rownum} = nans;
                    end
                    if (get(handles.checkbox6, 'Value'))
                        for c=1:size(CommonHandles.Classes,2)
                            featureCellsClasses{col, rownum, c} = nans;
                            featureMeansClasses{col, rownum, c} = nans;
                            if (get(handles.checkbox2, 'Value'))
                                featureMediansClasses{col, rownum, c} = nans;
                                featureStdsClasses{col, rownum, c} = nans;
                                featureMadsClasses{col, rownum, c} = nans;
                            end
                        end
                    end
                end
                if (get(handles.radiobutton2, 'Value') && size(featureCorrs{col, rownum},2) < numfeatures) 
                    featureCorrs{col, rownum} = NaN(numfeatures,numfeatures);
                end
            end
        end
        
        % write csv out        
        if ~exist([CommonHandles.DirName filesep 'Output'],'dir'), mkdir([CommonHandles.DirName filesep 'Output']); end
        name = [CommonHandles.DirName filesep 'Output' char(CommonHandles.PlatesNames(plateidx))] ;
        counter2 = 0;
        if (get(handles.radiobutton1, 'Value'))
            % feature based statistics
            fout = fopen([name '_fb.csv'], 'w');
            
            % header
            fprintf(fout, 'PlateName, Row, Col, cellNumber');
            for feat = 1:length(selectedFeatures)
                fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '.mean']);
                % if detailed statistics is required
                if (get(handles.checkbox2, 'Value'))
                    fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '.median']);
                    fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '.std']);
                    fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '.mad']);
                end
                if (get(handles.checkbox6, 'Value'))
                    for c=1:size(CommonHandles.Classes,2)
                        fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '_' CommonHandles.Classes{c}.Name '.mean']);
                        if (get(handles.checkbox2, 'Value'))
                            fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '_' CommonHandles.Classes{c}.Name '.median']);
                            fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '_' CommonHandles.Classes{c}.Name '.std']);
                            fprintf(fout, ',%s', [char(CommonHandles.featureList{selectedFeatures(feat)}) '_' CommonHandles.Classes{c}.Name '.mad']);
                        end
                    end
                end
            end
            
            fprintf(fout,'\n');
            % data
            
            for i=1:colmax
                for j=1:rowmax-'A'+1
                    counter2 = counter2 + 1;
                    % create base file names
                    colString = sprintf('%02d', i);
                    rowString = char('A'+j-1);
                    FileNamePrefix =  [CommonHandles.DirName filesep char(CommonHandles.PlatesNames(plateidx)) filesep  CommonHandles.ImageFolder filesep char(CommonHandles.PlatesNames(plateidx)) '*_w' rowString colString '*'];
                    % list them
                    MetadataFiles = dir(FileNamePrefix);
                    if length(MetadataFiles) == 0 % Skip positions without data
                        continue
                    end
                    
                    fprintf(fout, '%s, %s, %d, %d', char(CommonHandles.PlatesNames(plateidx)), char(j+'A'-1), i, featureCells(i, j));
                    for feat = 1:length(selectedFeatures)
                        fprintf(fout, ',%f', featureMeans{i, j}(feat));
                        % if detailed statistics is required
                        if (get(handles.checkbox2, 'Value'))
                            fprintf(fout, ',%f', featureMedians{i, j}(feat));
                            fprintf(fout, ',%f', featureStds{i, j}(feat));
                            fprintf(fout, ',%f', featureMads{i, j}(feat));
                        end
                        if (get(handles.checkbox6, 'Value'))
                            for c=1:size(CommonHandles.Classes,2)
                                fprintf(fout, ',%f', featureMeansClasses{i, j, c}(feat));
                                % if detailed statistics is required
                                if (get(handles.checkbox2, 'Value'))
                                    fprintf(fout, ',%f', featureMediansClasses{i, j, c}(feat));
                                    fprintf(fout, ',%f', featureStdsClasses{i, j, c}(feat));
                                    fprintf(fout, ',%f', featureMadsClasses{i, j, c}(feat));
                                end
                            end
                        end
                    end
                    fprintf(fout,'\n');
                    donePercent = double(counter2/wellcount);
                    waitText = sprintf('Exporting features...  %d%% done ( %d cells)', int16(donePercent * 100), cellcounter);
                    waitbar((piter-1+0.5)/numselplates+donePercent*0.5/numselplates, waitBarHandle, waitText);
                end
            end
            fclose(fout);
        elseif (get(handles.radiobutton2, 'Value'))
            % cross correlation-based statistics
            fout = fopen([name '_ccb.csv'], 'w');
            
            % header
            fprintf(fout, 'PlateName, Row, Col, cellNumber');
            for cl1 = 1:length(selectedFeatures)
                for cl2 = cl1+1:length(selectedFeatures)
                    fprintf(fout, ',cc_f%d_vs_f%d', selectedFeatures(cl1), selectedFeatures(cl2));
                end
            end
            fprintf(fout,'\n');
            % data

            for i=1:colmax
                for j=1:rowmax-'A'+1
                    counter2 = counter2 + 1;
                    fprintf(fout, '%s, %s, %d, %d', char(CommonHandles.PlatesNames(plateidx)), char(j+'A'-1), i, featureCells(i, j));
                    ccc = featureCorrs{i, j};
                    for cl1 = 1:length(selectedFeatures)-1
                        for cl2 = cl1+1:length(selectedFeatures)
                            fprintf(fout, ',%f', ccc(cl1, cl2));
                        end
                    end
                    fprintf(fout,'\n');
                    donePercent = double(counter2/wellcount);
                    waitText = sprintf('Exporting features...  %d%% done ( %d cells)', int16(donePercent * 100), cellcounter);
                    waitbar((piter-1+0.5)/numselplates+donePercent*0.5/numselplates, waitBarHandle, waitText);
                end                
            end;
            fclose(fout);
        end
    else
        % unstructured data
    end
end

close(waitBarHandle);

uiresume(gcbf);
close;


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uiresume(gcbf); 
close;


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in uipanel4.
function uipanel4_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel4 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

if (strcmp(hObject.Tag,'radiobutton1'))
    set(findobj(hObject.Parent.Parent.Children,'Tag','checkbox2'), 'Enable', 'on');
    set(findobj(hObject.Parent.Parent.Children,'Tag','checkbox6'), 'Enable', 'on');
else
    set(findobj(hObject.Parent.Parent.Children,'Tag','checkbox2'), 'Enable', 'off');
    set(findobj(hObject.Parent.Parent.Children,'Tag','checkbox6'), 'Enable', 'off');    
end;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

% --- Executes on selection change in listbox1.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2

% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6
