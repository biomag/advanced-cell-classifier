function varargout = changeSettings_GUI(varargin)
% CHANGESETTINGS_GUI M-file for changeSettings_GUI.fig
%      CHANGESETTINGS_GUI, by itself, creates a new CHANGESETTINGS_GUI or raises the existing
%      singleton*.
%
%      H = CHANGESETTINGS_GUI returns the handle to a new CHANGESETTINGS_GUI or the handle to
%      the existing singleton*.
%
%      CHANGESETTINGS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CHANGESETTINGS_GUI.M with the given input arguments.
%
%      CHANGESETTINGS_GUI('Property','Value',...) creates a new CHANGESETTINGS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before changeSettings_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to changeSettings_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help changeSettings_GUI

% Last Modified by GUIDE v2.5 14-Aug-2022 14:29:28

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @changeSettings_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @changeSettings_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before changeSettings_GUI is made visible.
function changeSettings_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to changeSettings_GUI (see VARARGIN)

% Choose default command line output for changeSettings_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes changeSettings_GUI wait for user response (see UIRESUME)
% uiwait(handles.loadDataSetFigure);
global CommonHandles;

set(handles.dataPathEditBox, 'String', CommonHandles.DirName);
set(handles.edit2, 'String', CommonHandles.ImageFolder);
set(handles.edit3, 'String', CommonHandles.MetaDataFolder);
set(handles.edit4, 'String', CommonHandles.OriginalImageFolder);
set(handles.editBox_CutSize, 'String', num2str(CommonHandles.CutSize));
if ~isfield(CommonHandles,'TrackingPath') || isempty(CommonHandles.TrackingPath)
    set(handles.trackingPathEditBox, 'String', '');
else
    set(handles.trackingPathEditBox, 'String', CommonHandles.TrackingPath);
end
if ~isfield(CommonHandles,'AlignPath') || isempty(CommonHandles.AlignPath)
    set(handles.alignmentPathEditBox, 'String', '');
else
    set(handles.alignmentPathEditBox, 'String', CommonHandles.AlignPath);
end

% read in the existing plate type names
for i=1:numel(CommonHandles.PlateTypeInfo)
    plateTypeName{i} = CommonHandles.PlateTypeInfo{i}.name;
end
for i=1:numel(CommonHandles.ImageExtensionNames)
    imExt{i} = CommonHandles.ImageExtensionNames{i}.name;
end
set(handles.popupmenu1, 'String', plateTypeName);
set(handles.popupmenu1, 'Value', CommonHandles.WellType);

set(handles.imType, 'String', imExt);
set(handles.imType, 'Value', find(strcmp(imExt,CommonHandles.ImageExtension)));

set(hObject, 'Name', 'Change settings');



% --- Outputs from this function are returned to the command line.
function varargout = changeSettings_GUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object deletion, before destroying properties.
function loadDataSetFigure_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to changeSettings_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
delete(hObject);


% --- Executes when user attempts to close loadDataSetFigure.
function loadDataSetFigure_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to changeSettings_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject)


function dataPathEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to dataPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dataPathEditBox as text
%        str2double(get(hObject,'String')) returns contents of dataPathEditBox as a
%        double

'test'


% --- Executes during object creation, after setting all properties.
function dataPathEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dataPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonSearchDataPath.
function buttonSearchDataPath_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSearchDataPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

oldFolder = CommonHandles.DirName;
newFolder = uigetdir(CommonHandles.DirName);

if newFolder == 0
    set(handles.dataPathEditBox, 'String', oldFolder);
else
    % Check for names of subfolders (typically Anal1, Anal2, Anal3)    
      errorCode = checkProjectFolder(newFolder,0,get(handles.edit2,'String'),get(handles.edit3,'String'),get(handles.edit4,'String'));
    
      %If everything is OK change the CommonHandles.DirName      
      if ~errorCode
          CommonHandles.DirName = newFolder;
          CommonHandles.SelectedPlate = 1;
          CommonHandles.SelectedImage = 1;
          set(handles.dataPathEditBox, 'String', CommonHandles.DirName);
          msgbox('You have successfully changed the data path.','Change path done');          
      else
          set(handles.dataPathEditBox, 'String', oldFolder);
          if errorCode == 1
              warndlg({'The path defined does not contain any folders!'});
          elseif errorCode == 2
              warndlg({'The path defined does not contain the folders declared!'});
          elseif errorCode == 3
              warndlg({'The selected path is wrong, the plates from this project are not there.'});                                    
          end
      end
end


% --- Executes on button press in buttonOK.
% This is OK button which fires the loading based on the given data in the
% GUI.
function buttonOK_Callback(hObject, eventdata, handles)
% hObject    handle to buttonOK (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

errorCode = checkProjectFolder(CommonHandles.DirName,1,get(handles.edit2, 'String'),get(handles.edit3, 'String'),get(handles.edit4, 'String'));

if errorCode
    warndlg({'The defined path does not contain the declared folders!'});
    return;
else
    
    CommonHandles.ImageFolder = get(handles.edit2, 'String');
    if ~strcmp(CommonHandles.MetaDataFolder, get(handles.edit3, 'String'))
        oldMetaDataFoler = CommonHandles.MetaDataFolder;
        CommonHandles.MetaDataFolder = get(handles.edit3, 'String');
        refreshTrSetMetaData(oldMetaDataFoler, CommonHandles.MetaDataFolder);
    end
    CommonHandles.OriginalImageFolder = get(handles.edit4, 'String');
    
    CommonHandles.WellType = get(handles.popupmenu1, 'Value');
        
    CommonHandles.CutSize = str2double(get(handles.editBox_CutSize,'String'));
    
    CommonHandles.ImageExtension = handles.imType.String{get(handles.imType, 'Value')};
    
    select_plate_wellGUI;
    closeSubGUI(CommonHandles);
    uiresume(gcbf);
    delete(gcbf)
end





function editBox_CutSize_Callback(hObject, eventdata, handles)
% hObject    handle to editBox_CutSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBox_CutSize as text
%        str2double(get(hObject,'String')) returns contents of editBox_CutSize as a double

possNum = str2double(get(handles.editBox_CutSize,'String'));

[bool,msg] = checkNumber( possNum, 1, 1, [1,Inf], 'Cut size');

global CommonHandles;
if ~bool    
    set(handles.editBox_CutSize,'String',num2str(CommonHandles.CutSize));
    warndlg(msg);
end


% --- Executes during object creation, after setting all properties.
function editBox_CutSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBox_CutSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in metaFilterButton.
function metaFilterButton_Callback(hObject, eventdata, handles)
% hObject    handle to metaFilterButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles

featureFiltering(CommonHandles.DirName, CommonHandles.PlateListHandle.String, CommonHandles.MetaDataFolder, 'featureNames.acc')


% --- Executes on selection change in imType.
function imType_Callback(hObject, eventdata, handles)
% hObject    handle to imType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns imType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from imType


% --- Executes during object creation, after setting all properties.
function imType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function trackingPathEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to trackingPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trackingPathEditBox as text
%        str2double(get(hObject,'String')) returns contents of trackingPathEditBox as a double


% --- Executes during object creation, after setting all properties.
function trackingPathEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trackingPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonSearchTrackingPath.
function buttonSearchTrackingPath_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSearchTrackingPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

oldFolder = CommonHandles.DirName;
newFolder = uigetdir(CommonHandles.DirName);

if newFolder == 0
    set(handles.trackingPathEditBox, 'String', oldFolder);
else
    CommonHandles.TrackingPath = newFolder;
    set(handles.trackingPathEditBox, 'String', CommonHandles.TrackingPath);
    msgbox('You have successfully changed the tracking folder path.','Change path done');
end



function alignmentPathEditBox_Callback(hObject, eventdata, handles)
% hObject    handle to alignmentPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alignmentPathEditBox as text
%        str2double(get(hObject,'String')) returns contents of alignmentPathEditBox as a double


% --- Executes during object creation, after setting all properties.
function alignmentPathEditBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alignmentPathEditBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonSearchAlignPath.
function buttonSearchAlignPath_Callback(hObject, eventdata, handles)
% hObject    handle to buttonSearchAlignPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

oldFolder = CommonHandles.DirName;
newFolder = uigetdir(CommonHandles.DirName);

if newFolder == 0
    set(handles.alignmentPathEditBox, 'String', oldFolder);
else
    CommonHandles.AlignPath = newFolder;
    set(handles.alignmentPathEditBox, 'String', CommonHandles.AlignPath);
    msgbox('You have successfully changed the alignment folder path.','Change path done');
end


% --- Executes on button press in pushbutton11_deriv.
function pushbutton11_deriv_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11_deriv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

calcDerivFeatures(CommonHandles.DirName, CommonHandles.PlateListHandle.String,...
    CommonHandles.MetaDataFolder, 'featureNames.acc', CommonHandles.TrackingPath,...
    CommonHandles.CommonHandles.TrackingPath);