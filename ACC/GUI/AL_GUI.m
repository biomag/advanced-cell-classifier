function varargout = AL_GUI(varargin)
% AL_GUI MATLAB code for AL_GUI.fig
%      AL_GUI, by itself, creates a new AL_GUI or raises the existing
%      singleton*.
%
%      H = AL_GUI returns the handle to a new AL_GUI or the handle to
%      the existing singleton*.
%
%      AL_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AL_GUI.M with the given input arguments.
%
%      AL_GUI('Property','Value',...) creates a new AL_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AL_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AL_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AL_GUI

% Last Modified by GUIDE v2.5 24-Jun-2016 13:23:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AL_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @AL_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AL_GUI is made visible.
function AL_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AL_GUI (see VARARGIN)

% Choose default command line output for AL_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AL_GUI wait for user response (see UIRESUME)
% uiwait(handles.activeLearningWindow);

availableAL = listAvailableActiveLearners();
set(handles.activeLearner_dropdownList,'String',availableAL);

clearvars -global GHandles;

global Ghandles;
Ghandles.ACParamNames = [];
Ghandles.ACParamEdits = [];

global CommonHandles;
if (CommonHandles.ActiveLearning)
    set(handles.infoText_ALstate,'String','Active learning is currently running.');
else
    set(handles.infoText_ALstate,'String','Active learning is currently not running.');
end

if checkToolboxByName('Parallel Computing Toolbox')
    set(handles.pctCheckBox,'Enable','on');
    set(handles.pctCheckBox,'Value',CommonHandles.ALBatched.usePCT);
    pctCheckBox_Callback(handles.pctCheckBox,0,handles);
else    
    set(handles.pctCheckBox,'Enable','off');
    set(handles.pctCheckBox,'Value',0);
    CommonHandles.ALBatched.usePCT = 0;
    set(handles.batchSizeLabel,'Enable','on');
    set(handles.batchSizeEdit,'Enable','on');
end

set(handles.infoText_selectedClassifier,'String',['The currently selected classifier is: ' CommonHandles.ClassifierNames{CommonHandles.SelectedClassifier}]);

set(handles.batchSizeEdit,'String',num2str(CommonHandles.ALBatched.batchSize));

activeLearner_dropdownList_Callback(handles.activeLearner_dropdownList,0,handles);

set(hObject, 'Name', 'Active learning settings');

if (isfield(CommonHandles,'AL') && isa(CommonHandles.AL,'ActiveLearner') && isfield(CommonHandles,'ActiveLearning') && CommonHandles.ActiveLearning ==1) 
    alname = class(CommonHandles.AL);
    for i=1:length(availableAL)
        if (strcmp(availableAL{i},alname))
            break;
        end
    end
    %set the AL dropdown list to the current value
    set(handles.activeLearner_dropdownList,'Value',i);
    
    activeLearner_dropdownList_Callback(handles.activeLearner_dropdownList,0,handles);                                 
    
    paramValues = CommonHandles.AL.getParameterValues();
    paramarray = feval([alname,'.getParameters']);
    
    fillOutUIControls( paramarray, paramValues,Ghandles.ACParamEdits);      
    
end

% --- Outputs from this function are returned to the command line.
function varargout = AL_GUI_OutputFcn(~,~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in activeLearner_dropdownList.
function activeLearner_dropdownList_Callback(hObject,~,handles)
% hObject    handle to activeLearner_dropdownList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns activeLearner_dropdownList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from activeLearner_dropdownList

global Ghandles;

contents = cellstr(get(hObject,'String'));
ACName = contents{get(hObject,'Value')};

%call the getParameter function of this class
paramarray = feval([ACName,'.getParameters']);

for i=1:length(Ghandles.ACParamNames)
    delete(Ghandles.ACParamNames{i});
    delete(Ghandles.ACParamEdits{i});
end

[Ghandles.ACParamNames,Ghandles.ACParamEdits] = generateUIControls(paramarray,handles.activeLearningPropertiesPanel,20,[10,50]);

pause(0.1);


% --- Executes during object creation, after setting all properties.
function activeLearner_dropdownList_CreateFcn(hObject, ~,~)
% hObject    handle to activeLearner_dropdownList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in startAL_button.
function startAL_button_Callback(~,~,handles)
% hObject    handle to startAL_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Ghandles;
global CommonHandles;

contents = cellstr(get(handles.activeLearner_dropdownList,'String'));
ACName = contents{get(handles.activeLearner_dropdownList,'Value')};

%check the correctness of the batch input field
batchSizeString = get(handles.batchSizeEdit,'String');
batchSizeLocal = str2double(batchSizeString);

if isnan(batchSizeLocal)
    errordlg('The batch size has to be a number!','Parameter type error');
    return;
elseif rem(batchSizeLocal,1)~=0
    errordlg('The batch size has to be integer','Parameter type error');
    return;
elseif batchSizeLocal < 1
    errordlg('The batch size has to be at least 1','Parameter type error');
    return;
else
    CommonHandles.ALBatched.batchSize = batchSizeLocal;
    CommonHandles.ALBatched.currentCellInBatch = 1;
    CommonHandles.ALBatched.data = [];
end

paramarray = feval([ACName,'.getParameters']);

try
    paramcell = fetchUIControlValues(paramarray,Ghandles.ACParamEdits,[ACName '.checkParameters']); %#ok<NASGU> used in eval
catch ME
    %if something is wrong with the parameters then we return
    if strcmp(ME.identifier,'ACC:errorConstraintFault')
        return;
    else
        rethrow(ME);
    end
end

try
    CommonHandles.AL = eval([ACName '(paramcell)']);
catch exception
    errordlg('Failed to instantiate active learner. Check for the correct parameters.','Constructor fail')
    getReport(exception)
    return;
end

global bgAL;
if CommonHandles.ALBatched.usePCT   
    %clearedCH = clearGUIhandles(CommonHandles);
    clearedCH = prepareCHforBatchAL(CommonHandles);
    h = msgbox('Sending data to batch processing','Batch processing');
    bgAL = batch('backgroundAL',1,{clearedCH});
    if ishandle(h), close(h); end
end

%close the window
close(handles.AL_GUI)
uiresume;

function activeLearners = listAvailableActiveLearners()
% This functions should list all the available active learner names.
% OUTPUT: cellarray with the strings of the active learner names.

%list the AL folder and the implementations
wS = what(['Utils' filesep 'ActiveLearning' filesep 'Implementations']);
w = struct2cell(wS);
m = w{2};
% for only the math files
for i = 1:length(m)
    %see its name without .m
    splitted = strsplit(m{i},'.');
    %rename
    m{i} = splitted{1};
end
%return the cellarray
activeLearners = m;



% --- Executes during object creation, after setting all properties.
function AL_GUI_CreateFcn(~, ~, ~)
% hObject    handle to AL_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global CommonHandles;
CommonHandles.ALBatched.batchSize = 5;


% --- Executes on button press in pctCheckBox.
function pctCheckBox_Callback(hObject, ~, handles)
% hObject    handle to pctCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pctCheckBox

global CommonHandles;
pctOn = get(hObject,'Value');

if pctOn
    CommonHandles.ALBatched.usePCT = 1;
    set(handles.batchSizeLabel,'Enable','off');
    set(handles.batchSizeEdit,'Enable','off');
else
    CommonHandles.ALBatched.usePCT = 0;
    set(handles.batchSizeLabel,'Enable','on');
    set(handles.batchSizeEdit,'Enable','on');
end
