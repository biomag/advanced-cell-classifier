function varargout = predictRegression_GUI(varargin)
% PREDICTREGRESSION_GUI MATLAB code for predictRegression_GUI.fig
%      PREDICTREGRESSION_GUI, by itself, creates a new PREDICTREGRESSION_GUI or raises the existing
%      singleton*.
%
%      H = PREDICTREGRESSION_GUI returns the handle to a new PREDICTREGRESSION_GUI or the handle to
%      the existing singleton*.
%
%      PREDICTREGRESSION_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PREDICTREGRESSION_GUI.M with the given input arguments.
%
%      PREDICTREGRESSION_GUI('Property','Value',...) creates a new PREDICTREGRESSION_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before predictRegression_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to predictRegression_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help predictRegression_GUI

% Last Modified by GUIDE v2.5 20-Jul-2019 19:38:45

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @predictRegression_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @predictRegression_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before predictRegression_GUI is made visible.
function predictRegression_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to predictRegression_GUI (see VARARGIN)

% Choose default command line output for predictRegression_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes predictRegression_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

uData = get(handles.figure1,'UserData');

global CommonHandles;

set(handles.plateSelectorList,'String',CommonHandles.PlatesNames);
set(handles.resultFolderEdit,'String',CommonHandles.RegressionPlane{uData.classIndex}.RegressionResultFolderName);
set(handles.resolutionEdit,'String',CommonHandles.RegressionPlane{uData.classIndex}.RegressionResultResolution);

%set(handles.controlSelectorPopup,'Value',[51 65 77 109 139 156 214 231 272]); %QN
%set(handles.controlSelectorPopup,'Value',[57 70 84 90 112 124 187 202 223]); GL2

rowNum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;
colNum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;

if (rowNum ~= -1 && colNum ~= -1) %so we have proper plates
   setWells(handles,rowNum,colNum);   
else %unstructured
   plateSelectorList_Callback([], [], handles);   
   set(handles.pushbutton_controlsFromPlateLayout,'Enable','off');
end


% --- Outputs from this function are returned to the command line.
function varargout = predictRegression_GUI_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function resolutionEdit_Callback(~, ~, ~)
% hObject    handle to resolutionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resolutionEdit as text
%        str2double(get(hObject,'String')) returns contents of resolutionEdit as a double


% --- Executes during object creation, after setting all properties.
function resolutionEdit_CreateFcn(hObject, ~, ~)
% hObject    handle to resolutionEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in predictButton.
function predictButton_Callback(~, ~, handles)
% hObject    handle to predictButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%
% This functions reads out the necessary information from the GUIs controls
% and generates the reports to the project folder.

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

newRegResFolder = get(handles.resultFolderEdit,'String');
newRegResResolution = get(handles.resolutionEdit,'String');
if ~isempty(newRegResFolder)
    for i=1:length(CommonHandles.RegressionPlane)
        CommonHandles.RegressionPlane{i}.RegressionResultFolderName = newRegResFolder;
        CommonHandles.RegressionPlane{i}.RegressionResultResolution = newRegResResolution;
    end
end

if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
    warndlg('Train a classifier after using ''Phenotype finder'' module!','Classifier error');
elseif ~isnumeric(CommonHandles.SelectedClassifier)
    warndlg('Chosen classifier error!','Classifier error');
else           
    %check for the existance of the SALT model. If not, then we train a
    %model.
    screensize = get(0,'Screensize');
    if length(CommonHandles.Classes) > 1
        if ~isfield(CommonHandles.SALT,'model') || isempty(CommonHandles.SALT.model)
            iD = dialog('Name','Missing classifer','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
            uicontrol('Parent',iD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Train a classifier...');
            CommonHandles = trainClassifier(CommonHandles);
            if ishandle(iD)
                close(iD);
            end
        end
    end
    
    
    if isfield(CommonHandles.RegressionPlane{idx},'Predictor') && ~isempty(CommonHandles.RegressionPlane{idx}.Predictor)
        %HERE STARTS THE SHOW ;)
        selectedPlateNumbers = get(handles.plateSelectorList,'Value');
        %make result folder
        uniqFolderName = timeString(5);
        baseFolder = fullfile(get(handles.resultFolderEdit,'String'), uniqFolderName);
        if ~exist([CommonHandles.DirName filesep baseFolder],'dir')
            mkdir([CommonHandles.DirName filesep baseFolder]);      
        end
        %ALL PARAMETERS BURNT IN. But here it still can be modified.
        html = 0; %get(handles.htmlCheckBox,'Value');
        thumbnail = 0;%get(handles.thumbnailCheckBox,'Value');
        %plotOfPlots = 1; %Changed to plotOfPLots structure on 2020.05.21.
        resolution = str2double(get(handles.resolutionEdit,'String'));
        if isnan(resolution)
            errordlg('The resolution has to be a number!','Parameter type error');
            return
        elseif rem(resolution,1)~=0
            errordlg('The resolution has to be integer','Parameter type error');
            return
        elseif resolution < 1
            warndlg('Resolution must be greater than 0!')
            return
        end
                
        h = waitbar(0,'Please wait...');
        pause(.5)
        w = 0;                        
        mm = length(selectedPlateNumbers);
        
        %Query unsup methods before the plate for loop for enhancing user
        %experience, and possibility for one-run batch analysis
        unsupMethods = {'NeRV','T-SNE','PCA','Hierarchical clustering'};
        [selectedULMethods,ok] = listdlg(...
            'ListString',unsupMethods,...
            'PromptString','Please select which data-discovery tools to run: (unsupervised learning methods)',...
            'ListSize',[450 100]);
        
        if ~ok || isempty(selectedULMethods)
            plotOfPlots = [];
        else
            selectedULMethods = unsupMethods(selectedULMethods);            
            %Init values
            wellIDs = [];
            wellAnnotation = [];
            if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col>0 %if the data is structured
                plateLayoutAvailable = questdlg('Do you have a plate layout file?','Define the plate layout file','Yes','No','No');
                if strcmp(plateLayoutAvailable,'Yes')
                    [ wellIDs,wellAnnotation ] = getPlateLayout( CommonHandles.DirName );                        
                end
            end
            answer = questdlg('Which icons do you want to use for the meta-visualization?','Icon selection','Heatmaps (histograms)','Kernel density estimations (KDE - smoother)','Heatmaps (histograms)');
            plotOfPlots.selectedULMethods = selectedULMethods;
            plotOfPlots.wellIDs = wellIDs;
            plotOfPlots.wellAnnotation = wellAnnotation;
            plotOfPlots.iconToUse = answer;
        end                       
        
        for i=convert2RowVector(selectedPlateNumbers)
            w = w+1;            

            %generate for this plate
            if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row ~=-1 || numel(selectedPlateNumbers) == 1
                selectedControlIndices = get(handles.controlSelectorPopup,'Value');
            else
                selectedControlIndices = [];
            end

            if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row ==-1 %only if unstructured data
                ImageList = dir(fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{i},CommonHandles.ImageFolder,['*' CommonHandles.ImageExtension]));
                wellNames = {ImageList.name};
            else
                wellNames = get(handles.controlSelectorPopup,'String');
            end

            if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col == -1, imgTypeName = 'image'; else, imgTypeName = 'well'; end
            if isempty(selectedControlIndices)
                 warndlg(sprintf('You haven''t selected any control, hence the %ss are compared to the average of ALL %ss.',imgTypeName,imgTypeName));
                selectedControls = wellNames;
            else
                selectedControls = wellNames(selectedControlIndices);
            end
            
            waitbarInfo.h = h;
            waitbarInfo.progressMeter = [(w-1)/mm,w/mm]; %Ratios in between to put
            waitbarInfo.counter = w;
            waitbarInfo.allPlateNumber = mm;
            regressionReportForPlate(i,html,thumbnail,plotOfPlots,resolution,baseFolder,idx,selectedControls,'waitbarInfo',waitbarInfo);
            
            pause(0.5)
        end

        % Creating a new folder 'allRegCSVs', and copy all the regResult
        % file to this folder, to make it easier to load all at once
        % into the TrajGUI
        if numel(selectedPlateNumbers) > 1

            tempPath = fullfile(CommonHandles.DirName, baseFolder);
            tempPathAll = fullfile(CommonHandles.DirName, baseFolder, 'allRegCSVs');
            folders = dir(tempPath);
            mkdir(tempPathAll);
            for i=3:length(folders)
                copyfile(fullfile(tempPath, folders(i).name, [folders(i).name, '_singleCellRegressionPositions.csv']),...
                    fullfile(tempPathAll, [folders(i).name, '_singleCellRegressionPositions.csv']));
            end
        end

        if ishandle(handles.figure1)
            close(handles.figure1);
        end
        if ishandle(h), close(h); end

        msgbox(['The reports are generated and saved to the folder: ' CommonHandles.DirName filesep baseFolder],'Report generation is done');

    else
        warndlg('Train a regressor to predict plates.');
    end                    
end


% --- Executes on selection change in plateSelectorList.
function plateSelectorList_Callback(~, ~, handles)
% hObject    handle to plateSelectorList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plateSelectorList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plateSelectorList

global CommonHandles;
if CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row ==-1 %only if unstructured data
    selectedPlate = get(handles.plateSelectorList,'Value');
    if length(selectedPlate) > 1
        set(handles.controlSelectorPopup,'Enable', 'off');
        set(handles.controlSelectorPopup,'Value',[]);
        warndlg('You cannot select controls for multiple plates in case of unstructured data! You may proceed but all images will be treated as controls.', 'Unstructured data warning');
    else
        set(handles.controlSelectorPopup,'Enable', 'on');
        ImageList = dir(fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{selectedPlate},CommonHandles.ImageFolder,['*' CommonHandles.ImageExtension]));
        set(handles.controlSelectorPopup,'Value',1);
        set(handles.controlSelectorPopup,'String',{ImageList.name});
        set(handles.controlSelectorPopup,'Max',size(ImageList,1));
    end
else
    rowNum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row;
    colNum = CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col;
    setWells(handles,rowNum,colNum);
end


% --- Executes during object creation, after setting all properties.
function plateSelectorList_CreateFcn(hObject, ~, ~)
% hObject    handle to plateSelectorList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function resultFolderEdit_Callback(~, ~, ~)
% hObject    handle to resultFolderEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resultFolderEdit as text
%        str2double(get(hObject,'String')) returns contents of resultFolderEdit as a double


% --- Executes during object creation, after setting all properties.
function resultFolderEdit_CreateFcn(hObject, ~, ~)
% hObject    handle to resultFolderEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in htmlCheckBox.
function htmlCheckBox_Callback(~, ~, ~)
% hObject    handle to htmlCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of htmlCheckBox


% --- Executes on button press in thumbnailCheckBox.
function thumbnailCheckBox_Callback(~, ~, ~)
% hObject    handle to thumbnailCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of thumbnailCheckBox


% --- Executes on selection change in controlSelectorPopup.
function controlSelectorPopup_Callback(~, ~, ~)
% hObject    handle to controlSelectorPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns controlSelectorPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from controlSelectorPopup


% --- Executes during object creation, after setting all properties.
function controlSelectorPopup_CreateFcn(hObject, ~, ~)
% hObject    handle to controlSelectorPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in colSelectorPopup.
function colSelectorPopup_Callback(~, ~, ~)
% hObject    handle to colSelectorPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns colSelectorPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from colSelectorPopup


% --- Executes during object creation, after setting all properties.
function colSelectorPopup_CreateFcn(hObject, ~, ~)
% hObject    handle to colSelectorPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in plotOfPlotsCheckBox.
function plotOfPlotsCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to plotOfPlotsCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of plotOfPlotsCheckBox

% --- Executes on button press in pushbutton_controlsFromPlateLayout.
function pushbutton_controlsFromPlateLayout_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_controlsFromPlateLayout (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
[wellIDs,wellAnnotation] = getPlateLayout( CommonHandles.DirName);

if isempty(wellIDs), return; end
allDrugNames = unique(wellAnnotation);
[selectedDrugs,isOK] = listdlg('PromptString','Select drugs(s) for control:', 'Name', 'Control selection from plate layout', 'SelectionMode','multiple','ListString', allDrugNames);
if isOK
    controlIdx = cell(1,numel(selectedDrugs));
    for i=1:numel(selectedDrugs)
        currentDrugName = allDrugNames{selectedDrugs};
        currentDrugIndices = find(strcmp(wellAnnotation,currentDrugName));
        controlIdx{i} = zeros(1,numel(currentDrugIndices));
        for j=1:numel(currentDrugIndices)            
            [row,col] = wellId2Index(wellIDs{currentDrugIndices(j)});
            controlIdx{i}(j) = sub2ind(...
                [CommonHandles.PlateTypeInfo{CommonHandles.WellType}.col...
                 CommonHandles.PlateTypeInfo{CommonHandles.WellType}.row],...
                 col,row);
        end
    end
    controlIdx = cell2mat(controlIdx);
    set(handles.controlSelectorPopup,'Value',controlIdx);
end

function setWells(handles,rowNum,colNum)
   global CommonHandles;
   
   rowmax = (char(rowNum+'A'-1));
   rowString = cellstr(('A':rowmax)')';
   colString = strsplit(num2str(1:colNum,'%02d '),' ');
   wellStrings = cell(1,length(rowString)*length(colString));
   for i=1:length(rowString)
       for j=1:length(colString)
           wellStrings{(i-1)*length(colString)+j} = [rowString{i} colString{j}];           
       end
   end
   set(handles.controlSelectorPopup,'String',wellStrings);
   set(handles.controlSelectorPopup,'Max',length(wellStrings));
   set(handles.plateSelectorList,'Max',length(CommonHandles.PlatesNames));
