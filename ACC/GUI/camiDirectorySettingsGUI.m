function varargout = camiDirectorySettingsGUI(varargin)
% CAMIDIRECTORYSETTINGSGUI MATLAB code for camiDirectorySettingsGUI.fig
%      CAMIDIRECTORYSETTINGSGUI, by itself, creates a new CAMIDIRECTORYSETTINGSGUI or raises the existing
%      singleton*.
%
%      H = CAMIDIRECTORYSETTINGSGUI returns the handle to a new CAMIDIRECTORYSETTINGSGUI or the handle to
%      the existing singleton*.
%
%      CAMIDIRECTORYSETTINGSGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CAMIDIRECTORYSETTINGSGUI.M with the given input arguments.
%
%      CAMIDIRECTORYSETTINGSGUI('Property','Value',...) creates a new CAMIDIRECTORYSETTINGSGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before camiDirectorySettingsGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to camiDirectorySettingsGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help camiDirectorySettingsGUI

% Last Modified by GUIDE v2.5 17-Oct-2016 10:31:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @camiDirectorySettingsGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @camiDirectorySettingsGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before camiDirectorySettingsGUI is made visible.
function camiDirectorySettingsGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to camiDirectorySettingsGUI (see VARARGIN)

% Choose default command line output for camiDirectorySettingsGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;
set(handles.nucleiSegmentationFolderInput, 'String', CommonHandles.CAMI.PhysicalExperiment.NucleiDir);
set(handles.cellSegmentationFolderInput, 'String', CommonHandles.CAMI.PhysicalExperiment.CellsDir);
set(handles.screenXMLInput, 'String', CommonHandles.CAMI.PhysicalExperiment.DescriptorXML);
set(handles.markerXMLInput, 'String', CommonHandles.CAMI.PhysicalExperiment.MarkerXML);
set(handles.markerImageInput, 'String', CommonHandles.CAMI.PhysicalExperiment.MarkerImage);
if strcmp(CommonHandles.CAMI.PhysicalExperiment.SelectedUploadDir,'NucleiDir')
    set(handles.nucleiButton,'Value',1);
    set(handles.cellsButton,'Value',0);
else
    set(handles.nucleiButton,'Value',0);
    set(handles.cellsButton,'Value',1);
end


% UIWAIT makes camiDirectorySettingsGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = camiDirectorySettingsGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function nucleiSegmentationFolderInput_Callback(hObject, eventdata, handles)
% hObject    handle to nucleiSegmentationFolderInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nucleiSegmentationFolderInput as text
%        str2double(get(hObject,'String')) returns contents of nucleiSegmentationFolderInput as a double


% --- Executes during object creation, after setting all properties.
function nucleiSegmentationFolderInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to nucleiSegmentationFolderInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cellSegmentationFolderInput_Callback(hObject, eventdata, handles)
% hObject    handle to cellSegmentationFolderInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of cellSegmentationFolderInput as text
%        str2double(get(hObject,'String')) returns contents of cellSegmentationFolderInput as a double


% --- Executes during object creation, after setting all properties.
function cellSegmentationFolderInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to cellSegmentationFolderInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in screenXMLBrowseButton.
function screenXMLBrowseButton_Callback(hObject, eventdata, handles)
% hObject    handle to screenXMLBrowseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
[fileName,pathName,filterIndex] = uigetfile({'*.xml';'*.txt';'*.*'},'Select screen descriptor XML',fullfile(CommonHandles.DirName));
if fileName
    set(handles.screenXMLInput,'String',fullfile(pathName,fileName));
end


% --- Executes on button press in markerXMLBrowseButton.
function markerXMLBrowseButton_Callback(hObject, eventdata, handles)
% hObject    handle to markerXMLBrowseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
[fileName,pathName,filterIndex] = uigetfile({'*.xml';'*.txt';'*.*'},'Select marker descriptor XML',fullfile(CommonHandles.DirName));
if fileName
    set(handles.markerXMLInput,'String',fullfile(pathName,fileName));
end


% --- Executes on button press in markerImageBrowseButton.
function markerImageBrowseButton_Callback(hObject, eventdata, handles)
% hObject    handle to markerImageBrowseButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
[fileName,pathName,filterIndex] = uigetfile({'*.tiff;*.tif';'*.*'},'Select marker image',fullfile(CommonHandles.DirName));
if fileName
    set(handles.markerImageInput,'String',fullfile(pathName,fileName));
end


function screenXMLInput_Callback(hObject, eventdata, handles)
% hObject    handle to screenXMLInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of screenXMLInput as text
%        str2double(get(hObject,'String')) returns contents of screenXMLInput as a double


% --- Executes during object creation, after setting all properties.
function screenXMLInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to screenXMLInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function markerXMLInput_Callback(hObject, eventdata, handles)
% hObject    handle to markerXMLInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of markerXMLInput as text
%        str2double(get(hObject,'String')) returns contents of markerXMLInput as a double


% --- Executes during object creation, after setting all properties.
function markerXMLInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to markerXMLInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function markerImageInput_Callback(hObject, eventdata, handles)
% hObject    handle to markerImageInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of markerImageInput as text
%        str2double(get(hObject,'String')) returns contents of markerImageInput as a double


% --- Executes during object creation, after setting all properties.
function markerImageInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to markerImageInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function figure1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
% hObject    handle to okButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

nucleiDir = get(handles.nucleiSegmentationFolderInput,'String');
cellsDir = get(handles.cellSegmentationFolderInput,'String');
screenXML = get(handles.screenXMLInput,'String');
markerXML = get(handles.markerXMLInput, 'String');
markerImage = get(handles.markerImageInput, 'String');
if get(handles.nucleiButton, 'Value')==1
    selectedObjectDir = 'NucleiDir';
else
    selectedObjectDir = 'CellsDir';
end

warningMessages = {};

if ~exist( fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},nucleiDir), 'dir' ) && strcmp(selectedObjectDir,'NucleiDir')
    tempMessage = sprintf('�  The given folder (%s) for nuclei segmentation does not exist!',nucleiDir);
    warningMessages{end+1} = tempMessage;
end

if ~exist( fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},cellsDir), 'dir' ) && strcmp(selectedObjectDir,'CellsDir')
    tempMessage = sprintf('�  The given folder (%s) for cell segmentation does not exist!',cellsDir);
    warningMessages{end+1} = tempMessage;
end

[path, ~, ~] = fileparts(screenXML);
if isempty(path)
    screenXML = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, screenXML);
end

if ~exist( screenXML, 'file' )
    tempMessage = sprintf('�  The given XML file of the screening (%s) does not exist!',screenXML);
    warningMessages{end+1} = tempMessage;
end

[path, ~, ~] = fileparts(markerXML);
if isempty(path)
    markerXML = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, markerXML);
end

if ~exist( markerXML, 'file' )
    tempMessage = sprintf('�  The given XML file of the marker (%s) does not exist!',markerXML);
    warningMessages{end+1} = tempMessage;
end

[path, ~, ~] = fileparts(markerImage);
if isempty(path)
    markerImage = fullfile(CommonHandles.DirName,CommonHandles.PlatesNames{1},CommonHandles.MetaDataFolder, markerImage);
end

if ~exist( markerImage, 'file' )
    tempMessage = sprintf('�  The given XML file of the screening (%s) does not exist!',markerImage);
    warningMessages{end+1} = tempMessage;
end

saveSettingsFlag = 0;
if ~isempty(warningMessages)
    choice = questdlg([{'The following settings are reqiured for successful contour upload:'}, {''}, warningMessages, {''},{'Do you want to save settings?'}],'Missing data for upload','Yes', 'No', 'No');
    if strcmp(choice,'Yes')
        saveSettingsFlag = 1;
    end
else
    saveSettingsFlag = 1;
end

if saveSettingsFlag
    CommonHandles.CAMI.PhysicalExperiment.NucleiDir = nucleiDir;
    CommonHandles.CAMI.PhysicalExperiment.CellsDir = cellsDir;
    CommonHandles.CAMI.PhysicalExperiment.DescriptorXML = screenXML;
	CommonHandles.CAMI.PhysicalExperiment.MarkerXML = markerXML;
    CommonHandles.CAMI.PhysicalExperiment.MarkerImage = markerImage;
    CommonHandles.CAMI.PhysicalExperiment.SelectedUploadDir = selectedObjectDir;
    
    uiresume(gcbf);
    delete(gcbf);
else
    return;
end

% --- Executes when selected object is changed in selectObjectButtonGroup.
function selectObjectButtonGroup_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in selectObjectButtonGroup 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
