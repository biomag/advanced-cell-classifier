function varargout = colorFrame(varargin)
% AUTHOR:   Attila Beleon
% DATE:     May 11, 2020
% NAME:     colorFrame
%
% Color Frame allows the user to visualize the destribution of any cell
% feature in heatmap style. Pick a feature from the list, define the range
% of interest with the knobs.
%
%
% COPYRIGHT
% Advanced Cell Classifier (ACC) Toolbox. All rights reserved.
% Copyright � 2019 Peter Horvath
% Synthetic and System Biology Unit, Biological Research Center, Szeged Hungary;
% Institute for Molecular Medicine Finland, University of Helsinki, Helsinki, Finland.
%
%COLORFRAME MATLAB code file for colorFrame.fig
%      COLORFRAME, by itself, creates a new COLORFRAME or raises the existing
%      singleton*.
%
%      H = COLORFRAME returns the handle to a new COLORFRAME or the handle to
%      the existing singleton*.
%
%      COLORFRAME('Property','Value',...) creates a new COLORFRAME using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to colorFrame_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      COLORFRAME('CALLBACK') and COLORFRAME('CALLBACK',hObject,...) call the
%      local function named CALLBACK in COLORFRAME.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help colorFrame

% Last Modified by GUIDE v2.5 17-May-2020 00:54:14

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @colorFrame_OpeningFcn, ...
                   'gui_OutputFcn',  @colorFrame_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before colorFrame is made visible.
function colorFrame_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for colorFrame
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

global CommonHandles;

handles.listbox1_features.String = varargin{1};
handles.listbox1_features.UserData = varargin{2};
idx = handles.listbox1_features.UserData.classIndex;

fID = CommonHandles.RegressionPlane{idx}.colorFrame.featureID;
handles.listbox1_features.Value = fID;

lo = CommonHandles.RegressionPlane{idx}.colorFrame.lo;
hi = CommonHandles.RegressionPlane{idx}.colorFrame.hi;
scaleType = CommonHandles.RegressionPlane{idx}.colorFrame.scaleType;

if strcmp(scaleType, 'linear')
    set(handles.scaleType_groupbutton,'selectedobject',handles.radiobutton_lin)
else
    set(handles.scaleType_groupbutton,'selectedobject',handles.radiobutton_log)
end

if handles.listbox1_features.UserData.prediction == 1 % prediction or PlotOfPlots mode
    featureMatrix = CommonHandles.RegressionPlaneHandle.UserData.features;
else
    %selecting the array of indices corresponding to this class
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
    indicesInThisClass = find(mergedClassVectors(:,idx));
    featureMatrix = cell2mat(CommonHandles.TrainingSet.Features(indicesInThisClass));
end
if CommonHandles.RegressionPlane{idx}.colorFrame.featureID > length(featureMatrix)
    fID = 1;
    handles.listbox1_features.Value = fID;
end

minFeature = min(featureMatrix(:, fID));
maxFeature = max(featureMatrix(:, fID));

lowKnob = rescale([lo,minFeature,maxFeature],1,256);
highKnob = rescale([hi,minFeature,maxFeature],1,256);


cmap = round(jet(256)*256);
C(:,:,1) = cmap(:,1)';
C(:,:,2) = cmap(:,2)';
C(:,:,3) = cmap(:,3)';
C = uint8(C);
image(handles.ax2_colorbar, C)
handles.ax2_colorbar.XAxis.Visible = 'off';
handles.ax2_colorbar.YAxis.Visible = 'off';
handles.ax2_colorbar.UserData.lo = lo;
handles.ax2_colorbar.UserData.hi = hi;

hold(handles.ax1_distribution, 'on');
xline(handles.ax1_distribution, 1, 'LineStyle',':', 'Color',[.5000 0 0], 'LineWidth', 2, 'Visible', 'off');
xline(handles.ax1_distribution, 0, 'LineStyle',':', 'Color',[0 0 .5156], 'LineWidth', 2, 'Visible', 'off');
handles.scaleType_groupbutton.UserData.jRangeSlider = tryoutrangeslider2(handles, max(1,floor(lowKnob(1))), min(256,round(highKnob(1))));
listbox1_features_Callback(handles.listbox1_features, [], handles)
jRangeSlider_Callback(handles.scaleType_groupbutton.UserData.jRangeSlider, [], handles)
% UIWAIT makes colorFrame wait for user response (see UIRESUME)
CommonHandles.colorFrameHandle = handles.figure1;
uiwait(handles.figure1);
    

% --- Outputs from this function are returned to the command line.
function varargout = colorFrame_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.figure1.UserData;
delete(handles.figure1);

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

if isequal(get(handles.figure1, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(handles.figure1);
else
    % The GUI is no longer waiting, just close it
    delete(handles.figure1);
end



% --- Executes on button press in pb1_apply.
function pb1_apply_Callback(hObject, eventdata, handles)
% hObject    handle to pb1_apply (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if numel(handles.ax1_distribution.Children) > 2
    cF_output.SelectedFeature = handles.listbox1_features.Value;
    cF_output.loKnobValue = handles.ax1_distribution.Children(2).Value(1);
    cF_output.hiKnobValue = handles.ax1_distribution.Children(3).Value(1);
    cF_output.scaleType = handles.scaleType_groupbutton.SelectedObject.String;
    handles.figure1.UserData = cF_output;
    close(handles.figure1);
end



% --- Executes on button press in pb2_cancel.
function pb2_cancel_Callback(hObject, eventdata, handles)
% hObject    handle to pb2_cancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

close(handles.figure1);


% --- Executes on selection change in listbox1_features.
function listbox1_features_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1_features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1_features contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1_features

global CommonHandles;
uData = handles.listbox1_features.UserData;
idx = uData.classIndex;

if uData.prediction == 1 % prediction or PlotOfPlots mode
    featureMatrix = CommonHandles.RegressionPlaneHandle.UserData.features;
else
    %selecting the array of indices corresponding to this class
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
    indicesInThisClass = find(mergedClassVectors(:,idx));
    featureMatrix = cell2mat(CommonHandles.TrainingSet.Features(indicesInThisClass));
end

if numel(handles.ax1_distribution.Children) < 3
    histogram(handles.ax1_distribution, featureMatrix(:,hObject.Value),'Normalization','probability', 'FaceColor', [0 .4470 .7410]);
else
    handles.ax1_distribution.Children(1).Data = featureMatrix(:,hObject.Value);
end
handles.ax1_distribution.Children(1).NumBins = size(featureMatrix, 1);
binHeight = max(handles.ax1_distribution.Children(1).Values);
binRange = handles.ax1_distribution.Children(1).BinLimits(2) - handles.ax1_distribution.Children(1).BinLimits(1);
handles.ax1_distribution.Children(2).Value = handles.ax1_distribution.Children(1).BinLimits(1) + handles.ax2_colorbar.UserData.lo * (binRange / 256);
handles.ax1_distribution.Children(3).Value = handles.ax1_distribution.Children(1).BinLimits(1) + handles.ax2_colorbar.UserData.hi * (binRange / 256);



% --- Executes during object creation, after setting all properties.
function listbox1_features_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1_features (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected object is changed in scaleType_groupbutton.
function scaleType_groupbutton_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in scaleType_groupbutton 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

jRangeSlider_Callback(handles.scaleType_groupbutton.UserData.jRangeSlider, [], handles)


function jRangeSlider = tryoutrangeslider2(handles, lo, hi)

hF = handles.figure1;

    % more direct instantiation
    % jRS = com.jidesoft.swing.RangeSlider(0,100,20,70); %min,max,low,high
    % [jRangeSlider{i}, hRangeSlider{i}] = javacomponent(jRS,[0,0,200,80],hF);%posx,posy,width,height
      jRS = com.jidesoft.swing.RangeSlider;
      [jRangeSlider, hRangeSlider] = javacomponent(jRS,[],hF);
      % modify rangeslider position
      set(hRangeSlider,'Units', 'Pixel');
      sliderPosition(1) = handles.ax1_distribution.Position(1)+15; % left
      sliderPosition(2) = 55; % bottom
      sliderPosition(3) = handles.ax1_distribution.Position(3)-30; % width
      sliderPosition(4) = 40; % hight
      %cancelButtonPosition = handles.pb2_cancel.Position;
      set(hRangeSlider,'Position',sliderPosition);
      % modify range slider properties
      set(jRangeSlider,'Maximum',256,...
          'Minimum',1,...
          'LowValue',lo,...
          'HighValue',hi,...
          'Name','Range',...
          'MajorTickSpacing',85,...
          'MinorTickSpacing',17,...
          'PaintTicks',true,...
          'PaintLabels',true, ...
          'StateChangedCallback',{@jRangeSlider_Callback, handles});
      

function jRangeSlider_Callback(jRangeSlider,event, handles)

high = max(2,jRangeSlider.HighValue);
low = min(255, jRangeSlider.LowValue);
cmap = round(jet(256)*256);
range = (high - low);
cmap(1:low, :) = repmat([0 0 132], low, 1);
cmap(high : 256, :) = repmat([128 0 0], 257-high, 1);
if range == 2
    cmap(low+1, :) = jet(1);
elseif range > 2
    if range > 10 && handles.radiobutton_log.Value
        MIDcmap = jet(range-1);
        tempMap = 1:range-1;
        logTempMap = round(rescale(log(tempMap),1,range-1));
        cmap(low+1 : high-1, :) = MIDcmap(logTempMap, :)*256;
    else
        cmap(low+1 : high-1, :) = round(jet(range-1)*256);
    end
end
C(:,:,1) = cmap(:,1)';
C(:,:,2) = cmap(:,2)';
C(:,:,3) = cmap(:,3)';
C = uint8(C);
handles.ax2_colorbar.Children.CData = C;

if numel(handles.ax1_distribution.Children) < 3
else
    binRange = handles.ax1_distribution.Children(1).BinLimits(2) - handles.ax1_distribution.Children(1).BinLimits(1);
    if low == 1
        handles.ax1_distribution.Children(2).Visible = 'off';
        handles.ax1_distribution.Children(2).Value = handles.ax1_distribution.Children(1).BinLimits(1);
    else
        handles.ax1_distribution.Children(2).Visible = 'on';
        loCorr = (low/256)*binRange;
        handles.ax1_distribution.Children(2).Value = handles.ax1_distribution.Children(1).BinLimits(1) + loCorr;
    end
    
    if high == 256
        handles.ax1_distribution.Children(3).Visible = 'off';
    else
        handles.ax1_distribution.Children(3).Visible = 'on';
    end
        hiCorr = (high/256)*binRange;
        handles.ax1_distribution.Children(3).Value = handles.ax1_distribution.Children(1).BinLimits(1) + hiCorr;
end

handles.ax2_colorbar.UserData.lo = low;
handles.ax2_colorbar.UserData.hi = high;


% --- Executes on mouse press over axes background.
function ax1_distribution_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to ax1_distribution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


answer = questdlg('Select scale type for Y-axis', 'Scale selection',...
    'linear','log','log');
if ~isempty(answer)
    handles.ax1_distribution.YAxis.Scale = answer;
end
