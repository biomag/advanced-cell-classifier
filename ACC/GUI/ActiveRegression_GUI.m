function varargout = ActiveRegression_GUI(varargin)
% ActiveRegression_GUI MATLAB code for ActiveRegression_GUI.fig
%      ActiveRegression_GUI, by itself, creates a new ActiveRegression_GUI or raises the existing
%      singleton*.
%
%      H = ActiveRegression_GUI returns the handle to a new ActiveRegression_GUI or the handle to
%      the existing singleton*.
%
%      ActiveRegression_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ActiveRegression_GUI.M with the given input arguments.
%
%      ActiveRegression_GUI('Property','Value',...) creates a new ActiveRegression_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ActiveRegression_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ActiveRegression_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ActiveRegression_GUI

% Last Modified by GUIDE v2.5 10-Aug-2016 15:52:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ActiveRegression_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ActiveRegression_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ActiveRegression_GUI is made visible.
function ActiveRegression_GUI_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ActiveRegression_GUI (see VARARGIN)

% Choose default command line output for ActiveRegression_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ActiveRegression_GUI wait for user response (see UIRESUME)
% uiwait(handles.activeLearningWindow);

availableAL = listAvailableActiveLearners();
set(handles.activeLearner_dropdownList,'String',availableAL);

clearvars -global GHandles;

global Ghandles;
Ghandles.ACParamNames = [];
Ghandles.ACParamEdits = [];

global CommonHandles;
if (CommonHandles.ActiveRegression.on)
    set(handles.infoText_ALstate,'String','Active regression is currently running.');
else
    set(handles.infoText_ALstate,'String','Active regression is currently not running.');
end

set(handles.infoText_selectedClassifier,'String',['The currently selected regression algorithm is: ' ' MISSING']);

if checkToolboxByName('Parallel Computing Toolbox')
    set(handles.pctCheckBox,'Enable','on');
    set(handles.pctCheckBox,'Value',CommonHandles.ALBatched.usePCT);
    pctCheckBox_Callback(handles.pctCheckBox,0,handles);
else    
    set(handles.pctCheckBox,'Enable','off');
    set(handles.pctCheckBox,'Value',0);
    CommonHandles.ALBatched.usePCT = 0;
    set(handles.batchSizeLabel,'Enable','on');
    set(handles.batchSizeEdit,'Enable','on');
end

set(handles.batchSizeEdit,'String',num2str(CommonHandles.ALBatched.batchSize));

regressionNames = cell(0);
for i=1:length(CommonHandles.Classes)
    if strcmp(CommonHandles.Classes{i}.Type,'Regression')        
        regressionNames = [regressionNames CommonHandles.Classes{i}.Name]; %#ok<AGROW> not problem as we don't have many classes
    end
end

set(handles.selectClassPopup,'String',regressionNames);

selectClassPopup_Callback(handles.selectClassPopup, 0, handles);

activeLearner_dropdownList_Callback(handles.activeLearner_dropdownList,0,handles);

set(hObject, 'Name', 'Active regression settings');

if (isfield(CommonHandles,'ALRegression') && isa(CommonHandles.ALRegression,'ALalgorithm') && isfield(CommonHandles,'ActiveRegression') && CommonHandles.ActiveRegression.on ==1) 
    alname = class(CommonHandles.ALRegression);
    for i=1:length(availableAL)
        if (strcmp(availableAL{i},alname))
            break;
        end
    end
    %set the AL dropdown list to the current value
    set(handles.activeLearner_dropdownList,'Value',i);
    
    activeLearner_dropdownList_Callback(handles.activeLearner_dropdownList,0,handles);                                 
        
    paramValues = CommonHandles.ALRegression.getParameterValues();
    paramarray = feval([alname,'.getParameters']);
    
    fillOutUIControls( paramarray, paramValues,Ghandles.ACParamEdits);     
    
end

% --- Outputs from this function are returned to the command line.
function varargout = ActiveRegression_GUI_OutputFcn(~,~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in activeLearner_dropdownList.
function activeLearner_dropdownList_Callback(hObject,~,handles)
% hObject    handle to activeLearner_dropdownList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns activeLearner_dropdownList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from activeLearner_dropdownList

global Ghandles;

contents = cellstr(get(hObject,'String'));
ACName = contents{get(hObject,'Value')};

%call the getParameter function of this class
paramarray = feval([ACName,'.getParameters']);

for i=1:length(Ghandles.ACParamNames)
    delete(Ghandles.ACParamNames{i});
    delete(Ghandles.ACParamEdits{i});
end

[Ghandles.ACParamNames,Ghandles.ACParamEdits] = generateUIControls(paramarray,handles.activeLearningPropertiesPanel,20,[10,50]);

pause(0.1);


% --- Executes during object creation, after setting all properties.
function activeLearner_dropdownList_CreateFcn(hObject, ~,~)
% hObject    handle to activeLearner_dropdownList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in startAL_button.
function startAL_button_Callback(~,~,handles)
% hObject    handle to startAL_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Ghandles;
global CommonHandles;

contents = cellstr(get(handles.activeLearner_dropdownList,'String'));
ACName = contents{get(handles.activeLearner_dropdownList,'Value')};

contents = cellstr(get(handles.selectClassPopup,'String'));
if isempty(contents)
    errordlg('There must be at least one regression class to use active regression');
    return;
end
CommonHandles.ActiveRegression.selectedClass = contents{get(handles.selectClassPopup,'Value')};


%check the correctness of the batch input field
batchSizeString = get(handles.batchSizeEdit,'String');
batchSizeLocal = str2double(batchSizeString);

if isnan(batchSizeLocal)
    errordlg('The batch size has to be a number!','Parameter type error');
    return;
elseif rem(batchSizeLocal,1)~=0
    errordlg('The batch size has to be integer','Parameter type error');
    return;
elseif batchSizeLocal < 1
    errordlg('The batch size has to be at least 1','Parameter type error');
    return;
else
    CommonHandles.ALBatched.batchSize = batchSizeLocal;
    CommonHandles.ALBatched.currentCellInBatch = 1;
    CommonHandles.ALBatched.data = [];
end

paramarray = feval([ACName,'.getParameters']);
try
paramcell = fetchUIControlValues(paramarray,Ghandles.ACParamEdits,[ACName '.checkParameters']); %#ok<NASGU> used in eval
catch
    %if something is wrong with the parameters then we return
    if strcmp(ME.identifier,'ACC:errorConstraintFault')
        return;
    else
        rethrow(ME);
    end
end    

try
    CommonHandles.ALRegression = eval([ACName '(paramcell)']);
catch exception
    errordlg('Failed to instantiate active learner. Check for the correct parameters.','Constructor fail')
    getReport(exception)
    return;
end

global bgAL;
if CommonHandles.ALBatched.usePCT    
    %clearedCH = clearGUIhandles(CommonHandles);
    clearedCH = prepareCHforBatchAL(CommonHandles);
    h = msgbox('Sending data to batch processing','Batch processing');
    bgAL = batch('backgroundALRegression',1,{clearedCH});    
    if ishandle(h), close(h); end
end

%close the window
close(handles.AL_GUI)
uiresume;

function activeLearners = listAvailableActiveLearners()
% This functions should list all the available active learner names.
% OUTPUT: cellarray with the strings of the active learner names.

%list the AL folder and the implementations
wS = what(fullfile('Utils','ActiveRegression','Model','ALalgorithms'));
w = struct2cell(wS);
m = w{2};
% for only the math files
for i = 1:length(m)
    %see its name without .m
    splitted = strsplit(m{i},'.');
    %rename
    m{i} = splitted{1};
end
%return the cellarray
activeLearners = m;



% --- Executes during object creation, after setting all properties.
function AL_GUI_CreateFcn(~, ~, ~)
% hObject    handle to ActiveRegression_GUI (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

global CommonHandles;
CommonHandles.ALBatched.batchSize = 5;


% --- Executes on button press in pctCheckBox.
function pctCheckBox_Callback(hObject, ~, handles)
% hObject    handle to pctCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of pctCheckBox

global CommonHandles;
pctOn = get(hObject,'Value');

if pctOn
    CommonHandles.ALBatched.usePCT = 1;
    set(handles.batchSizeLabel,'Enable','off');
    set(handles.batchSizeEdit,'Enable','off');
else
    CommonHandles.ALBatched.usePCT = 0;
    set(handles.batchSizeLabel,'Enable','on');
    set(handles.batchSizeEdit,'Enable','on');
end


% --- Executes on selection change in selectClassPopup.
function selectClassPopup_Callback(hObject, ~, handles)
% hObject    handle to selectClassPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns selectClassPopup contents as cell array
%        contents{get(hObject,'Value')} returns selected item from selectClassPopup

global CommonHandles;

contents = cellstr(get(hObject,'String'));
if ~isempty(contents)
    regName = contents{get(hObject,'Value')};

    for i=1:length(CommonHandles.Classes)
        if strcmp(regName,CommonHandles.Classes{i}.Name)
            selectedRegressionClass = i;
            break;        
        end
    end
end

if exist('selectedRegressionClass','var') && isfield(CommonHandles.RegressionPlane{selectedRegressionClass},'Predictor')
    set(handles.infoText_selectedClassifier,'String',['The currently selected regression algorithm is: ' class(CommonHandles.RegressionPlane{selectedRegressionClass}.Predictor)]);
else
    set(handles.infoText_selectedClassifier,'String','Regression algorithm is missing. Active regression will not work.');
end


% --- Executes during object creation, after setting all properties.
function selectClassPopup_CreateFcn(hObject, ~, ~)
% hObject    handle to selectClassPopup (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
