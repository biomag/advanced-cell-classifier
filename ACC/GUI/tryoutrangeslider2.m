function tryoutrangeslider2(data, CF_GUI_handle)
Label = 'Scale Range';
Mind = min(data);
Maxd = max(data);
hF = figure;
pos = handle.pos;

% more direct instantiation
% jRS = com.jidesoft.swing.RangeSlider(0,100,20,70); %min,max,low,high
% [jRangeSlider{i}, hRangeSlider{i}] = javacomponent(jRS,[0,0,200,80],hF);%posx,posy,width,height
jRS = com.jidesoft.swing.RangeSlider;
[jRangeSlider, hRangeSlider] = javacomponent(jRS,[],hF);
% modify rangeslider position
set(hRangeSlider,'Position',[100,80,200,80])
% modify range slider properties
set(jRangeSlider,'Maximum',Maxd,...
    'Minimum',Mind,...
    'LowValue',1,...
    'HighValue',256,...
    'Name',Label,...
    'MajorTickSpacing',51,...  
    'MinorTickSpacing',16,...
    'PaintTicks',true,...
    'PaintLabels',true,...
    'StateChangedCallback',{@jRangeSlider_Callback,1});
% add text label
uicontrol(hF,'Style','Text','Position',pos,'String',Label)
end

function jRangeSlider_Callback(jRangeSlider,event,i)
   disp([jRangeSlider.Name ' ,extra parameter =' num2str(i)])
end