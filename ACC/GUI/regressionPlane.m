function varargout = regressionPlane(varargin)
% REGRESSIONPLANE M-file for regressionPlane.fig
%      REGRESSIONPLANE, by itself, creates a new REGRESSIONPLANE or raises the existing
%      singleton*.
%
%      H = REGRESSIONPLANE returns the handle to a new REGRESSIONPLANE or the handle to
%      the existing singleton*.
%
%      REGRESSIONPLANE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REGRESSIONPLANE.M with the given input arguments.
%
%      REGRESSIONPLANE('Property','Value',...) creates a new REGRESSIONPLANE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before regressionPlane_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to regressionPlane_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help regressionPlane

% Last Modified by GUIDE v2.5 29-Jul-2021 19:37:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @regressionPlane_OpeningFcn, ...
                   'gui_OutputFcn',  @regressionPlane_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before regressionPlane is made visible.
function regressionPlane_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to regressionPlane (see VARARGIN)

% Choose default command line output for regressionPlane
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes regressionPlane wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% GLOBAL variables and global 'constants'.
global CommonHandles;
global ghandles;
ghandles = handles;

%get the index of the current reg class.
uData = get(handles.figure1,'UserData');
set(handles.figure1,'UserData',uData);
% %% **********
%UserData uData udata partial documentation:
%
% STRUCTURE FIELDS:
% * uData.prediction: this variable determines for which usage the regression
% plane is opened.
%   Values:
%      -1: opening for examining results, seeing current state, or return
%      from other (e.g. prediction) mode
%       0: opening for placing a new cell
%       1: opening for examining the predicted positions for cells from
%       selected images
%       2: opening for plot of plots mode
% * classIndex: the entry of CommonHandles.RegressionPlane cellarray to
% which we can write the data for this plane
% %% ***********
idx = uData.classIndex;
idInTrainingSet = uData.idInTrainingSet;
CommonHandles.RegressionPlane{idx}.initialApparentPictureSize = 50;
CommonHandles.RegressionPlane{idx}.mainFigureHandle = hObject;

%If the regression plane is opened in normal mode /0 or -1/ then a check is
%performed if there isn't any remaining regression plane from extra modes
%(1 or 2).
if uData.prediction<1 %0 or -1
    maxRegIdx = 0;
    for i=1:length(CommonHandles.Classes)
        if strcmp(CommonHandles.Classes{i}.Type,'Regression')
            maxRegIdx = i;
        end
    end
    if length(CommonHandles.RegressionPlane)>maxRegIdx
        CommonHandles.RegressionPlane(maxRegIdx+1:end) = []; %clear out
    end
end

% newObject is a boolean to show whether we're putting down now a cell to
% distinguish between selecting cell and putting down new cell
if uData.prediction == 0
    CommonHandles.RegressionPlane{idx}.newObject = 1;        
    firstCellGUIMode(1,handles);
else
    CommonHandles.RegressionPlane{idx}.newObject = 0;
    firstCellGUIMode(0,handles);
end

if idInTrainingSet == 0
    CommonHandles.RegressionPlane{idx}.selectedObject = [];
else
    CommonHandles.RegressionPlane{idx}.selectedObject = idInTrainingSet;
end
for i=1:length(CommonHandles.RegressionPlane)
    if isfield(CommonHandles.RegressionPlane{i},'selectedAxes') && ~isempty(CommonHandles.RegressionPlane{i}.selectedAxes)
        for j = 1:length(CommonHandles.RegressionPlane{i}.selectedAxes)
            if ishandle(CommonHandles.RegressionPlane{i}.selectedAxes{j}) || isa(CommonHandles.RegressionPlane{i}.selectedAxes{j},'fakeAxes')
                delete(CommonHandles.RegressionPlane{i}.selectedAxes{j});
            end
        end                
    end
end
CommonHandles.RegressionPlane{idx}.selectedAxes = {};

if get(handles.checkbox1,'Value')
    CommonHandles.RegressionPlane{idx}.showGrid = 1;
else
    CommonHandles.RegressionPlane{idx}.showGrid = -1;
end
if get(handles.checkbox3,'Value')
    CommonHandles.RegressionPlane{idx}.gridOnTheTop = 1;
else
    CommonHandles.RegressionPlane{idx}.gridOnTheTop = -1;
end

if strcmp(get(handles.adaptiveCellSize,'State'),'off')
    CommonHandles.RegressionPlane{idx}.adaptiveCellSize = 0;
else
    CommonHandles.RegressionPlane{idx}.adaptiveCellSize = 1;
end

handles.toolbarRegressionPerformance.State = 'off';

if isfield(CommonHandles,'colorFrameHandle') && ~isempty(CommonHandles.colorFrameHandle) && ishandle(CommonHandles.colorFrameHandle)
    close(CommonHandles.colorFrameHandle);
end

if isfield(CommonHandles,'trainReg_GUI') && ~isempty(CommonHandles.trainReg_GUI) && ishandle(CommonHandles.trainReg_GUI)
    close(CommonHandles.trainReg_GUI);
end

if isfield(CommonHandles,'PredictRegressionGUI') && ~isempty(CommonHandles.PredictRegressionGUI) && ishandle(CommonHandles.PredictRegressionGUI)
    close(CommonHandles.PredictRegressionGUI);
end

pause(0.1)

%search for remembered regression result parameters
if ~isempty(CommonHandles.RegressionPlane)
    if isfield(CommonHandles.RegressionPlane{1},'RegressionResultFolderName')
        regResFolderName = CommonHandles.RegressionPlane{1}.RegressionResultFolderName; %#ok<NASGU> used in eval
    else
        regResFolderName = 'RegResult'; %#ok<NASGU> used in eval
    end
    if isfield(CommonHandles.RegressionPlane{1},'RegressionResultResolution')    
        regResResolution = CommonHandles.RegressionPlane{1}.RegressionResultResolution; %#ok<NASGU> used in eval
    else
        regResResolution = '5'; %#ok<NASGU> used in eval
    end    
else
    regResFolderName = 'RegResult'; %#ok<NASGU> used in eval
    regResResolution = '5'; %#ok<NASGU> used in eval
end

%TODO: At some point it would be nice to show out of bounds cells for the
%user in prediction mode. This would require parametrizing the so far burnt
%in 0,1000 limits for the reg plane.

%Init other CommonHandles fields
fieldsToCheck = {...
    {'dragging','[]'},...
    {'orPos','[]'},...
    {'mapDragged','[]'},...    
    {'startOfAdaptionCellSize','CommonHandles.RegressionPlane{idx}.initialApparentPictureSize'},...
    {'apparentPictureSize','CommonHandles.RegressionPlane{idx}.initialApparentPictureSize'},...
    {'XBounds','[0 1000]'},...
    {'YBounds','[0 1000]'},...    
    {'adaptiveCellSize','0'},...
    {'gridDistance','10'},...
    {'horizontal1DRegression','0'},...
    {'RegressionResultFolderName','regResFolderName'},...
    {'RegressionResultResolution','regResResolution'},...
    {'colorFrame', 'struct(''state'', 0, ''hi'', 256, ''lo'', 0, ''featureID'', 1, ''scaleType'', ''linear'',''calcNew'',0)'},...
    {'dynamicEvents','struct(''lastDynamicTic'',[],''dynamicEventLatency'',1,''XBoundsOld'',[0 1000],''YBoundsOld'',[0 1000],''dynamicDrawNow'',0)'},...
    };
for i=1:length(fieldsToCheck)
    if ~isfield(CommonHandles.RegressionPlane{idx},fieldsToCheck{i}{1}) || isempty(CommonHandles.RegressionPlane{idx}.(fieldsToCheck{i}{1}))
        eval(['CommonHandles.RegressionPlane{idx}.' fieldsToCheck{i}{1} '=' fieldsToCheck{i}{2} ';']);
    end
end
    
placeItemsOnGUI(handles);

refreshSelectedObjectPictures(idx,uData);

%set(handles.axes11,'Ydir','reverse');
drawRegressionPlane(handles);

refreshSelectedObjectInfoData();



% --- Outputs from this function are returned to the command line.
function varargout = regressionPlane_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
    

% --- Executes on scroll wheel click while the figure is in focus.
function figure1_WindowScrollWheelFcn(~, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see FIGURE)
%	VerticalScrollCount: signed integer indicating direction and number of clicks
%	VerticalScrollAmount: number of lines scrolled for each click
% handles    structure with handles and user data (see GUIDATA)

pos = round(get(gcf,'CurrentPoint'));

performViewZoom(pos,eventdata.VerticalScrollCount,handles);
    
% --- Executes when figure1 is resized.
function figure1_ResizeFcn(~,~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

placeItemsOnGUI(handles);
drawRegressionPlane(handles);

% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(~,~, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%global variables
global CommonHandles;
global ghandles;

uData = get(ghandles.figure1,'UserData');
idx = uData.classIndex;
idInTrainingSet = uData.idInTrainingSet;

CommonHandles.RegressionPlane{idx}.mapDragged = 0;

selType = get(gcf,'SelectionType');

%Position determination
pos = round(get(gcf,'CurrentPoint'));

if ~exist('handles','var')
    handles = ghandles;
    regPlaneBoundaries = round(get(handles.axes1,'Position'));
else
    %Get the bounds of the axes1 which is the regression plane.
    regPlaneBoundaries = round(get(handles.axes1,'Position'));
end

%Handle the out of bounds cases.
if pos(1)<regPlaneBoundaries(1), pos(1) = regPlaneBoundaries(1); end
if pos(1)>regPlaneBoundaries(1)+regPlaneBoundaries(3), pos(1) = regPlaneBoundaries(1)+regPlaneBoundaries(3); end
if pos(2)<regPlaneBoundaries(2), pos(2) = regPlaneBoundaries(2); end
if pos(2)>regPlaneBoundaries(2)+regPlaneBoundaries(4), pos(2) = regPlaneBoundaries(2)+regPlaneBoundaries(4); end

distFromXLowerInGUI = pos(1) - regPlaneBoundaries(1);
distFromYLowerInGUI = pos(2) - regPlaneBoundaries(2);

pos = GUI2regPlane(pos);

%End of positioning
switch selType            
    case 'extend'
        if ~CommonHandles.RegressionPlane{idx}.newObject % only if we select an object
            possibleObj = CommonHandles.RegressionPlane{idx}.whichObjectThere(regPlaneBoundaries(4)-distFromYLowerInGUI,distFromXLowerInGUI);
            if possibleObj
                CommonHandles.RegressionPlane{idx}.selectedObject(end+1) = possibleObj;                                        
            end
            refreshSelectedObjectPictures(idx,uData);                
            refreshSelectedObjectInfoData();
            drawRegressionPlane(handles);
            %plotNavigationMap(idx,uData,handles);
        end
    case 'normal'                                            
        if CommonHandles.RegressionPlane{idx}.newObject % if we're placing an object to the plane.
            placeCellOnRegPlane(pos,handles,idInTrainingSet)
            CommonHandles.RegressionPlane{idx}.newObject = 0;   
            firstCellGUIMode(0,handles);
            prevGUI = gcbf;
            drawRegressionPlane(handles);
            refreshSelectedObjectInfoData();
            %CommonHandles.RegressionPlane{idx}.newObject = 0;
            %plotNavigationMap(idx,uData,handles);
            uiresume(prevGUI);            
        else % if we want to select an object            
            possibleObj = CommonHandles.RegressionPlane{idx}.whichObjectThere(regPlaneBoundaries(4)-distFromYLowerInGUI,distFromXLowerInGUI);
            if possibleObj
                CommonHandles.RegressionPlane{idx}.selectedObject = possibleObj;
            else
                CommonHandles.RegressionPlane{idx}.selectedObject = [];
            end            
            refreshSelectedObjectPictures(idx,uData);                
            refreshSelectedObjectInfoData();
            drawRegressionPlane(handles);
            %plotNavigationMap(idx,uData,handles);
        end                
    case 'alt'        
        CommonHandles.RegressionPlane{idx}.mapDragged = 1;
        CommonHandles.RegressionPlane{idx}.orPos = round(get(gcf,'CurrentPoint'));
        set(gcf,'Pointer','fleur');        
        plotNavigationMap(idx,uData,handles);
    case 'open'        
        if uData.prediction == 1 %in prediction mode            
            selectedObject = CommonHandles.RegressionPlane{idx}.whichObjectThere(regPlaneBoundaries(4)-distFromYLowerInGUI,distFromXLowerInGUI);
            saveCellInPrediction(uData,selectedObject,uData.predictedPositions(:,selectedObject),handles);
            drawRegressionPlane(handles);
        end
end

% --- Executes on mouse motion over figure - except title and menu.
function figure1_WindowButtonMotionFcn(hObject,~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)    
    hObject.SelectionType;
    hObject.CurrentCharacter;
    global CommonHandles;    
    newPos = round(get(gcf,'CurrentPoint'));    
    regPlaneBoundaries = round(get(handles.axes1,'Position'));         
    
    uData = get(handles.figure1,'UserData');
    idx = uData.classIndex;
    if isfield(CommonHandles.RegressionPlane{idx},'XBounds')
        orPos = CommonHandles.RegressionPlane{idx}.orPos;
        dragging = CommonHandles.RegressionPlane{idx}.dragging;
        XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
        YBounds = CommonHandles.RegressionPlane{idx}.YBounds;
        
        if CommonHandles.RegressionPlane{idx}.mapDragged
            posDiff = newPos - orPos;
            %the magic number 10 is needed for unknown reason. Without this the
            %regression plane moves too fast.
            [XBounds,YBounds] = calcRegPlaneBoundsFromShift(posDiff,XBounds,YBounds,regPlaneBoundaries);
            
            CommonHandles.RegressionPlane{idx}.XBounds = XBounds;
            CommonHandles.RegressionPlane{idx}.YBounds = YBounds;
            drawRegressionPlane(handles);
        end
        
        if ~isempty(dragging)
            if isInRegPlane(newPos,regPlaneBoundaries)
                for i=1:length(dragging)
                    posDiff = newPos - orPos;
                    if CommonHandles.RegressionPlane{idx}.horizontal1DRegression
                        posDiff(2) = 0;
                    end
                    set(dragging{i},'Position',get(dragging{i},'Position') + [posDiff(1) posDiff(2) 0 0]);
                end
                orPos = newPos;
            end
        end
        CommonHandles.RegressionPlane{idx}.orPos = orPos;
    end

% --- Executes on mouse press over figure background, over a disabled or
% --- inactive control, or over an axes background.
function figure1_WindowButtonUpFcn(~,~, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

regPlaneBoundaries = round(get(handles.axes1,'Position'));    
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;

%selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
CommonHandles.RegressionPlane{idx}.mapDragged = 0;
dragging = CommonHandles.RegressionPlane{idx}.dragging;
orPos = CommonHandles.RegressionPlane{idx}.orPos;


set(gcf,'Pointer','arrow');

if ~isempty(dragging)
    realRegPlaneBounds = [0 0 1000 1000]; %Reg plane bounds in its own system (theoretical)
    newPos = round(get(gcf,'CurrentPoint'));
    %if we are on a valid point    
    if isInRegPlane(newPos, regPlaneBoundaries)    
        valid = 1;
        posDiff = newPos - orPos;
        draggedNewPos = cell(1,length(dragging)); %This is position in GUI system
        regPos = cell(1,length(dragging)); %This is position in RegPlane system
        for i=1:length(dragging)
            draggedNewPos{i} = get(dragging{i},'Position') + [posDiff(1) posDiff(2)  0 0];
            regPos{i} = GUI2regPlane(draggedNewPos{i}(1:2)+draggedNewPos{i}(3:4)/2);
            if i~=length(dragging) && ~isInRegPlane(regPos{i},realRegPlaneBounds)
            % The first condition is for not testing the LAST axes, which is the real axes for showing the selected cells. 
            % As that one occupies the whole image it should NOT be tested for being within regplane.
            % Furthermore the test should be for the regplane and not for
            % the GUI
                valid = 0;
            end
        end       
        if valid
            set(dragging{end},'Position',draggedNewPos{end}); %Set the position for the big axes (although it'll be deleted almost instantly in drawRegPlane)
            for i=1:length(dragging)-1 %Do not try to put the last axes into reg plane (it does not have user data either)
                set(dragging{i},'Position',draggedNewPos{i});               

                if (uData.prediction==0 || uData.prediction == -1) % in annotation mode
                    placeCellOnRegPlane(regPos{i},handles,get(dragging{i},'UserData'));
                    %CommonHandles.TrainingSet.RegPos{selectedObject} = [newPos(1) newPos(2)];
                elseif (uData.prediction == 1) %in prediction mode
                    uData = get(handles.figure1,'UserData'); %needed because saveCellInPredicion modifies the UserData (prediction positions) and we call this function in a for loop since the multiple selection
                    saveCellInPrediction(uData,get(dragging{i},'UserData'),regPos{i},handles);
                end
                refreshSelectedObjectInfoData();
            end                   
        end           
    end    
    drawRegressionPlane(handles); %This is going to delete the real selected axes anyway.
    dragging = {};
    CommonHandles.RegressionPlane{idx}.dragging = dragging;
    plotNavigationMap(idx,uData,handles);
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, ~, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of checkbox1

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
CommonHandles.RegressionPlane{idx}.showGrid = CommonHandles.RegressionPlane{idx}.showGrid * -1;
drawRegressionPlane(handles);


% --- Executes on button press in checkbox3.
function checkbox3_Callback(~,~, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3
global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
CommonHandles.RegressionPlane{idx}.gridOnTheTop = CommonHandles.RegressionPlane{idx}.gridOnTheTop*-1;
drawRegressionPlane(handles);



function edit1_Callback(hObject, ~, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;
gridDistance = str2double(get(hObject,'String'));
if isnan(gridDistance)
    warndlg('The distance must be a number','Unexpected value');
    set(hObject,'String',10);    
elseif gridDistance<1 || gridDistance>100
    warndlg('The distance must be between 1 and 100','Unexpected value');
    set(hObject,'String',10);        
else
    CommonHandles.RegressionPlane{idx}.gridDistance = gridDistance;
    drawRegressionPlane(handles);    
end


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, ~,~)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function uipushtool3_ClickedCallback(~,~, handles)
%Delete pushtool. It deletes the selected object
% hObject    handle to uipushtool3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;
selectedAxes = CommonHandles.RegressionPlane{idx}.selectedAxes;
mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';

h = waitbar(0,sprintf('Removing cells from training set... [%d/%d]',0,length(selectedAxes)-1));

if (exist('selectedAxes','var') && ~isempty(selectedAxes) )
    for i=1:length(selectedAxes)
       if ishandle(selectedAxes{i}) || isa(selectedAxes{i},'fakeAxes')
           delete(selectedAxes{i});
       end
    end    
end

if uData.prediction == 0 || uData.prediction == -1 %annotation modes
    idInTS = CommonHandles.RegressionPlane{idx}.selectedObject;
    globalIdx = idx;
elseif (uData.prediction == 1)
    selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
    idInTS = zeros(1,length(selectedObject));
    for i=1:length(selectedObject)
        idInTS(i) = isInTrainingSet( uData.predictedIcons{selectedObject(i)}.ImageName, uData.predictedIcons{selectedObject(i)}.CellNumber, 0, CommonHandles,uData.features(selectedObject(i),:));
    end
    globalIdx = uData.lastRegressionPlaneIdx;
end

sortedIdx = sort(idInTS,'descend');
for i=1:length(sortedIdx)   
    if sortedIdx(i)
        deleteFromClassImages( CommonHandles.TrainingSet.CellNumber{sortedIdx(i)}, CommonHandles.TrainingSet.ImageName{sortedIdx(i)}, globalIdx )
    end
    if ishandle(h) && i~=length(sortedIdx), waitbar((i+1)/length(sortedIdx),h,sprintf('Removing cells from training set... [%d/%d]',i+1,length(selectedAxes)-1)); end
end

if ishandle(CommonHandles.ShowTrainedCellsFig)
    close(CommonHandles.ShowTrainedCellsFig);
end

if isfieldRecursive(CommonHandles.RegressionPlane{idx},'CrossValidation.Predictions') && ~isempty(CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions)    
%Delete also from the CommonHandles CrossValidation predictions (if there is such)    
    binaryIndexInClass = mergedClassVectors(:,idx);
    idxWithinClass = cumsum(binaryIndexInClass);
    for i=1:length(sortedIdx)        
        CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions(:,idxWithinClass(sortedIdx(i))) = [];
    end
end

if ishandle(h), close(h); end

CommonHandles.RegressionPlane{idx}.selectedObject = [];
refreshSelectedObjectPictures(idx,uData);
refreshSelectedObjectInfoData;
drawRegressionPlane(handles);



% --- Executes on button press in increaseImageSize.
function increaseImageSize_Callback(~,~, handles)
% hObject    handle to increaseImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

CommonHandles.RegressionPlane{idx}.apparentPictureSize = CommonHandles.RegressionPlane{idx}.apparentPictureSize*1.1;
drawRegressionPlane(handles);


% --- Executes on button press in decreaseImageSize.
function decreaseImageSize_Callback(~,~, handles)
% hObject    handle to decreaseImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

CommonHandles.RegressionPlane{idx}.apparentPictureSize = CommonHandles.RegressionPlane{idx}.apparentPictureSize/1.1;
drawRegressionPlane(handles);

% --- Executes on button press in resetImageSize.
function resetImageSize_Callback(~,~, handles)
% hObject    handle to resetImageSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
CommonHandles.RegressionPlane{idx}.apparentPictureSize = CommonHandles.RegressionPlane{idx}.initialApparentPictureSize;
drawRegressionPlane(handles);


%Callback function for the X coordinate modification. We check the validity
%of the coordinate and move the selected object to the desired position.
function editXCoord_Callback(hObject, ~, handles)
% hObject    handle to editXCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editXCoord as text
%        str2double(get(hObject,'String')) returns contents of editXCoord as a double

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

if length(CommonHandles.RegressionPlane{idx}.selectedObject)==1
    XCoord = str2double(get(hObject,'String'))*1000;
    if isnan(XCoord)
        warndlg('Coordinate must be number','Unexpected value');
        set(hObject,'String',num2str(CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(1)/1000));
    elseif XCoord>1000 || XCoord <0
        warndlg('Coordinate must be between 0 and 1','Unexpected value');
        set(hObject,'String',num2str(CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(1)/1000));
    elseif uData.prediction < 1 % annotation mode
        CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(1) = XCoord;
        drawRegressionPlane(handles);
    elseif uData.prediction == 1 %prediction mode
        selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
        newPos = uData.predictedPositions(:,selectedObject);
        newPos(1) = XCoord;
        saveCellInPrediction(uData,selectedObject,newPos,handles);
    end
end


% --- Executes during object creation, after setting all properties.
function editXCoord_CreateFcn(hObject, ~,~)
% hObject    handle to editXCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editYCoord_Callback(hObject, ~, handles)
% hObject    handle to editYCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editYCoord as text
%        str2double(get(hObject,'String')) returns contents of editYCoord as a double
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

if length(CommonHandles.RegressionPlane{idx}.selectedObject)==1
    YCoord = str2double(get(hObject,'String'))*1000;
    if isnan(YCoord)
        warndlg('Coordinate must be number','Unexpected value');
        set(hObject,'String',num2str(CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(2)/1000));
    elseif YCoord>1000 || YCoord <0
        warndlg('Coordinate must be between 0 and 1','Unexpected value');
        set(hObject,'String',num2str(CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(2)/1000));
    elseif uData.prediction < 1   
        CommonHandles.TrainingSet.RegPos{CommonHandles.RegressionPlane{idx}.selectedObject}(2) = YCoord;
        drawRegressionPlane(handles);
    elseif uData.prediction == 1 %prediction mode
        selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
        newPos = uData.predictedPositions(:,selectedObject);
        newPos(2) = YCoord;
        saveCellInPrediction(uData,selectedObject,newPos,handles);    
    end
end

% --- Executes during object creation, after setting all properties.
function editYCoord_CreateFcn(hObject, ~,~)
% hObject    handle to editYCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function adaptiveCellSize_OnCallback(~,~,handles)
% hObject    handle to adaptiveCellSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

CommonHandles.RegressionPlane{idx}.adaptiveCellSize = 1;
CommonHandles.RegressionPlane{idx}.startOfAdaptionCellSize = CommonHandles.RegressionPlane{idx}.apparentPictureSize;



% --------------------------------------------------------------------
function adaptiveCellSize_OffCallback(~,~,handles)
% hObject    handle to adaptiveCellSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;


CommonHandles.RegressionPlane{idx}.adaptiveCellSize = 0;


% --------------------------------------------------------------------
function toolbarTrainRegression_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarTrainRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;


CommonHandles.trainReg_GUI = trainRegression_GUI('UserData',struct('classIndex',idx));


% --------------------------------------------------------------------
function toolbarPredictWellPushtool_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarPredictWellPushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

%check for the existance of the predictor
if isfield(CommonHandles.RegressionPlane{idx},'Predictor') && ~isempty(CommonHandles.TrainingSet.Features)
    
    selectedAxes = CommonHandles.RegressionPlane{idx}.selectedAxes;
    if ~isempty(selectedAxes)
        for i=1:length(selectedAxes)
            if ishandle(selectedAxes{i}) || isa(selectedAxes{i},'fakeAxes'),  delete(selectedAxes{i}); end
        end
        CommonHandles.RegressionPlane{idx}.selectedAxes = [];
    end
    
    %build up GUI to select the images to predict on the regression plane
    ImagesByPlate = cell(length(CommonHandles.PlatesNames),1);
    imageIdxBoundary = zeros(1,length(CommonHandles.PlatesNames));
    for i=1:length(CommonHandles.PlatesNames)
        listedDir = dir([CommonHandles.DirName filesep CommonHandles.PlatesNames{i} filesep CommonHandles.ImageFolder]);
        ImagesByPlate{i} = {listedDir(3:end).name}';
        imageIdxBoundary(i+1) = imageIdxBoundary(i)+size(ImagesByPlate{i},1);
    end
    
    imageIdxBoundary = imageIdxBoundary(2:end);
    
    ImageListStr = vertcat(ImagesByPlate{:});
    
    [SelectedImages,isOK] = listdlg('PromptString','Select image(s):', 'Name', 'Predict images', 'SelectionMode','multiple','ListString', ImageListStr,'ListSize',[222,474]);
    
    if isOK && ~isempty(SelectedImages)
        
        answer = questdlg('Would you like to use predictor or previously calculated RegResult file?','Prediction type','Predictor','File','Predictor');
        
        if ~isempty(answer)
            
            if strcmp(answer,'File')
                [regFile, regPath] = uigetfile('.csv','Select regResult file');
                regResult = readtable(fullfile(regPath,regFile));
                fromFile = 1;
            else
                fromFile = 0;
            end
            
            %Save the current cell infos.
            tempCurrentCellInfo = makeTempCellData();
            
            iconsByImage = cell(1,length(SelectedImages));
            positionsByImage = cell(1,length(SelectedImages));
            %distributionByImage = cell(length(SelectedImages),1);
            allFeatures = cell(length(SelectedImages),1);
            selectedImgIdx = convert2RowVector(SelectedImages);
            
            %User info
            ii = 0;
            h = waitbar(ii/double(length(selectedImgIdx)),sprintf('Predicting images... %d/%d\n[%2.0f%%] %s',ii,length(selectedImgIdx),(ii/double(length(selectedImgIdx)))*100,''));
            set(findall(h,'type','text'),'Interpreter','none')
            
            for ii=1:length(selectedImgIdx)
                i = selectedImgIdx(ii);
                [~, fileNameexEx, ~] = fileparts(ImageListStr{i});
                plateNumber = find(i<=imageIdxBoundary,1);
                plateName = CommonHandles.PlatesNames{plateNumber};
                %read features to know the amount of cells to iterate through
                [~,features] = readMetaData(CommonHandles,fileNameexEx,plateNumber);
                
                %User info
                if ishandle(h), waitbar(ii/double(length(selectedImgIdx)),h,sprintf('Predicting images... %d/%d\n[%2.0f%%] %s',ii,length(selectedImgIdx),(ii/double(length(selectedImgIdx)))*100,fileNameexEx)); end
                
                %collect images if there are any
                if ~((size(features,1) == 1 || size(features,2) == 1) && (isequal(zeros(size(features)),features)))
                    
                    %check for the existance of the SALT model. If not, then we train a
                    %model.
                    screensize = get(0,'Screensize');
                    if length(CommonHandles.Classes) > 1
                        if strcmp(CommonHandles.SelectedClassifier,'OneClassClassifier')
                            warndlg('Train a classifier after using ''Phenotype finder'' module!','Classifier error');
                            return;
                        elseif ~isfield(CommonHandles.SALT,'model') || isempty(CommonHandles.SALT.model)
                            iD = dialog('Name','Missing classifer','Position',[screensize(3)/2-180, screensize(4)/2-30,360,60]);
                            uicontrol('Parent',iD,'Style','text','Units','Pixels','Position',[10 20 340 20],'String','Train a classifier...');
                            CommonHandles = trainClassifier(CommonHandles);
                            if ishandle(iD)
                                close(iD);
                            end
                        end
                    end
                    
                    if length(CommonHandles.Classes) > 1
                        [ out, ~, ~ ] = predictClassifier( features(:,3:end) , CommonHandles);
                        %select cells classified to regression class.
                        features = features(out==idx,:);
                        regressionCellIdx = find(out==idx);
                    else
                        regressionCellIdx = 1:size(features,1);
                    end
                    
                    
                    icons = cell(1,size(features,1));
                    features = features(:,3:end);
                    for j=1:size(features,1) % iterate throuh all the cells on an image
                        cellInfoStruct.PlateName = plateNumber;
                        cellInfoStruct.ImageName = [plateName filesep CommonHandles.ImageFolder filesep ImageListStr{i}];
                        cellInfoStruct.OriginalImageName = [plateName filesep CommonHandles.OriginalImageFolder filesep ImageListStr{i} ];
                        cellInfoStruct.CellNumber = regressionCellIdx(j);
                        jumpToCell(cellInfoStruct,0);
                        classImgObj.Image = CommonHandles.CurrentSelectedCell;
                        classImgObj.ClassName = CommonHandles.Classes{idx}.Name;
                        classImgObj.CellNumber = regressionCellIdx(j);
                        classImgObj.ImageName = cellInfoStruct.ImageName;
                        classImgObj.OriginalImageName = cellInfoStruct.OriginalImageName;
                        classImgObj.PlateName = plateNumber;
                        classImgObj.Features = features(j,:);
                        icons{j} = classImgObj;
                    end
                    iconsByImage{ii} = icons;
                    
                    %features transposed because predictors waiting for column
                    %observation
                    %The distributionByImage is reserved for future releases
                    
                    %if CommonHandles.RegressionPlane{idx}.Predictor.isDistribution
                    %    [positions,fullCovMatrix] = CommonHandles.RegressionPlane{idx}.Predictor.predict(features');
                    %    distributionByImage{ii} = cell(1,2);
                    %    distributionByImage{ii}{1} = diag(fullCovMatrix(1:end/2,1:end/2));
                    %    distributionByImage{ii}{2} = diag(fullCovMatrix(end/2+1:end,end/2+1:end));
                    %else
                    %    positions = CommonHandles.RegressionPlane{idx}.Predictor.predict(features');
                    %end
                    
                    if fromFile
                        regFileIdx = strcmp(plateName,regResult.PlateName) & strcmp([fileNameexEx,CommonHandles.ImageExtension],regResult.ImageName);
                        positions = regResult{regFileIdx,9:10}'*1000;
                    else
                        positions = CommonHandles.RegressionPlane{idx}.Predictor.predict(features');
                    end
                    
                    positionsByImage{ii} = positions;
                    
                    allFeatures{ii} = features;
                end
            end
            
            set(handles.toolbarTrainRegression,'Visible','off');
            set(handles.toolbarRegressionPerformance,'Visible','off');
            set(handles.toolbarPredictWellPushtool,'Visible','off');
            set(handles.toolbarPredictPlatePushtool,'Visible','off');
            
            predictedIcons = [iconsByImage{:}];
            predictedPositions = [positionsByImage{:}];
            %predictedDistribution = cell2mat(reshape([distributionByImage{:}],2,[])');
            
            %jump back to the original cell after collecting the images to put
            %on regplane.
            jumpToCell(tempCurrentCellInfo,0);
            
            %Make poster
            %makePoster(predictedIcons,predictedPositions,[10,10]);
            if ishandle(h), waitbar(1,h,'Opening regression plane...'); end
            
            CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',0,'classIndex',length(CommonHandles.Classes)+1,'prediction',1,'predictedIcons',{predictedIcons},'predictedPositions',predictedPositions,'lastRegressionPlaneIdx',idx,'features',cell2mat(allFeatures)) );
            if ishandle(h), close(h); end
        end
    else
        set(handles.toolbarTrainRegression,'Visible','on');
        set(handles.toolbarRegressionPerformance,'Visible','on');
        set(handles.toolbarPredictWellPushtool,'Visible','on');
        set(handles.toolbarPredictPlatePushtool,'Visible','on');
    end
elseif ~isfield(CommonHandles.RegressionPlane{idx},'Predictor')
    warndlg('Please train a predictor before using prediction!','Trained predictor is missing');
else
    warndlg('Please create at leat 2 classes in the main view of ACC!','Not enough classes');
end


% --- Executes on button press in closePredictionButton.
function closePredictionButton_Callback(~, ~, handles)
% hObject    handle to closePredictionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
lastIndex = uData.lastRegressionPlaneIdx;

idx = uData.classIndex;
for i=1:length(CommonHandles.RegressionPlane{idx}.selectedAxes)
    if ishandle(CommonHandles.RegressionPlane{idx}.selectedAxes{i}) || isa(CommonHandles.RegressionPlane{idx}.selectedAxes{i},'fakeAxes')
        delete(CommonHandles.RegressionPlane{idx}.selectedAxes{i});    
    end
end
CommonHandles.RegressionPlane{idx}.selectedObject = [];
CommonHandles.RegressionPlane{idx}.selectedAxes = {};

%set(handles.toolbarTrainRegression,'Visible','on');
%set(handles.toolbarPredictWellPushtool,'Visible','on');
%set(handles.toolbarPredictPlatePushtool,'Visible','on');
CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',0,'classIndex',lastIndex,'prediction',-1));
refreshSelectedObjectInfoData;


% --------------------------------------------------------------------
function toolbarPredictPlatePushtool_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarPredictPlatePushtool (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');

global CommonHandles;
CommonHandles.PredictRegressionGUI = predictRegression_GUI('UserData',uData);


% --------------------------------------------------------------------
function toolbarPlotOfPlots_ClickedCallback(~, ~, handles)
% hObject    handle to toolbarPlotOfPlots (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

%with this function we build up the structure to be plotted by the
%regression plane.
uData = get(handles.figure1,'UserData');
prevIdx = uData.classIndex;
resultFolder = uigetdir(fullfile(CommonHandles.DirName),'Specify the result folder that you want to load');
if resultFolder == 0
    return;
elseif ~isfolder(resultFolder)
    warndlg('The path you selected is not a folder');
    return;
else
    tmpFolder = fullfile(resultFolder,'temp');    
    metaVisFiles = dir(fullfile(tmpFolder,'*metaVisualizationCoordinates_class*.txt'));
    annotationFile = dir(fullfile(tmpFolder,'*annotationByWell.mat'));
    if isempty(annotationFile)
        warndlg('The selected folder is not a valid outpout folder for regression. The correct folder has a temp folder in it.');
        return;
    elseif isempty(metaVisFiles)
        warndlg('The plot of plots seems to be uncompatible with your machine. The meta visualization coordinate files are missing.');
        return;
    end
    %everything ok.
end

%select which meta visualization to load
splitULName = cellfun(@strsplit,{metaVisFiles.name},repmat({'_'},1,numel(metaVisFiles)),'UniformOutput',false);
ULmethods = cell(length(splitULName),1);
for j=1:length(ULmethods), ULmethods{j} = splitULName{j}{end}(1:end-4); end
[selectedULidx,ok] = listdlg('ListString',ULmethods,'PromptString','Select meta visualization to load','ListSize',[200 100],'SelectionMode','single');
if ~ok, return; end

%in this file we load in variables as: 'annotationByWell','realHeatmaps'
load( fullfile(tmpFolder,annotationFile.name),'annotationByWell','perWellHeatmapInLine','perWellKDEInLine');
%To work with older exports
if ~exist('perWellHeatmapInLine','var') || ~exist('perWellKDEInLine','var')
    load( fullfile(tmpFolder,annotationFile.name),'annotationByWell','realHeatmaps');
    iconsUsedForMetaVis = realHeatmaps;
else
    answer = questdlg('Which icons do you want to use for the meta-visualization?','Icon selection','Heatmaps (histograms)','Kernel density estimations (KDE - smoother)','Heatmaps (histograms)');
    iconsUsedForMetaVis = askUserForIconsToUse(perWellHeatmapInLine,perWellKDEInLine,answer);
end
posFile = fopen(fullfile(tmpFolder,metaVisFiles(selectedULidx).name),'r');

fullFile = fscanf(posFile,'%f');
fclose(posFile);

columnNumber = fullFile(1); %this should be 2 if everything is correct
positions = reshape(fullFile(2:end),columnNumber,[]);

%rescale the positions to fit regression plane.
figureSize = 900;
positions = positions - repmat(min(positions,[],2),1,size(positions,2));
positions = positions ./ repmat( (max(positions,[],2)-min(positions,[],2)), 1,size(positions,2) );
positions = positions * figureSize;
positions = positions + 50; %to see clearly the plots on the boundary

%build up predictedIcons.
predictedIcons = cell(1,size(positions,2));
for k=1:length(predictedIcons)
    cmapres = 32;
    cmap = hot(cmapres);
    imgForColormap = imgToColormap(iconsUsedForMetaVis{k},cmapres,'none');
    for kk=1:3, predictedIcons{k}.Image(:,:,kk) = reshape(cmap(imgForColormap,kk),size(imgForColormap,1),size(imgForColormap,2)); end
    predictedIcons{k}.drugInfo = annotationByWell{k};
    predictedIcons{k}.highlighted = false;
end

onlyAnnotation = cell(1,numel(annotationByWell)); for j=1:numel(annotationByWell), onlyAnnotation{j} = annotationByWell{j}(1:end-6); end %remove well from the end
simplyDrugs = unique(onlyAnnotation);
set(handles.listbox_WellAnnotation,'String',simplyDrugs);
set(handles.listbox_WellAnnotation,'Min',0); set(handles.listbox_WellAnnotation,'Max',length(simplyDrugs));
set(handles.listbox_WellAnnotation,'Value',[]);

CommonHandles.RegressionPlaneHandle = regressionPlane('UserData',struct('idInTrainingSet',0,'classIndex',length(CommonHandles.RegressionPlane)+1,'prediction',2,'predictedIcons',{predictedIcons},'predictedPositions',positions,'lastRegressionPlaneIdx',prevIdx) );



% --------------------------------------------------------------------
function screenshot_ClickedCallback(~, ~, handles)
% hObject    handle to screenshot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fileName,path,spec] = uiputfile({'*.pdf','.pdf';'*.png','.png'},'Select a location to save your screenshot of the RP!');
%ls = dir(path);
%fileName = ['image' num2str(length(ls)-3,'%02d') '.png'];
%spec = 2;
if fileName == 0, return; end    

uData = get(handles.figure1,'UserData');

h = figure('Name','Regression plane screenshot','PaperOrientation','landscape');
currentImage = drawRegressionPlane(handles);
if (uData.prediction == 2)    
    imshow(currentImage);
else
    imshow(uint8(currentImage));
end

if spec == 1
    print(h,fullfile(path,fileName),'-dpdf','-fillpage');
elseif spec == 2
    imwrite(currentImage,fullfile(path,fileName));
end

if ishandle(h)
    close(h)
end



% --------------------------------------------------------------------
function toolbar_OneDRegression_OffCallback(hObject, eventdata, handles)
% hObject    handle to toolbar_OneDRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
CommonHandles.RegressionPlane{idx}.horizontal1DRegression = 0;


% --------------------------------------------------------------------
function toolbar_OneDRegression_OnCallback(hObject, eventdata, handles)
% hObject    handle to toolbar_OneDRegression (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;
CommonHandles.RegressionPlane{idx}.horizontal1DRegression = 1;


% --------------------------------------------------------------------
function toolbarRegressionPerformance_OnCallback(~, ~, handles)
% hObject    handle to toolbarRegressionPerformance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

if isfield(CommonHandles.RegressionPlane{idx},'Predictor')
    mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
    logicalIndexInClass = logical(mergedClassVectors(:,idx));

    mergedFeatures = reshape(cell2mat(CommonHandles.TrainingSet.Features(logicalIndexInClass)),CommonHandles.Classes{idx}.LabelNumbers,[])';
    mergedTargets = reshape(cell2mat(CommonHandles.TrainingSet.RegPos(logicalIndexInClass)'),CommonHandles.Classes{idx}.LabelNumbers,[])';
    imageName = CommonHandles.TrainingSet.ImageName(logicalIndexInClass);
    cellNumber = CommonHandles.TrainingSet.CellNumber(logicalIndexInClass);

    N = size(mergedFeatures,2);
    if N<5
        warndlg('Please put at least 5 training samples to the regression plane.');
        return;
    end
            
    %Cross validation
    rng('default');
    rng(940919); %fixing the random permutation for reproducibility
    nofFolds = 10;
    %It is important to re-init the classifiers so that the
    %Cross-validation is not biased by parameters learnt from the full data
    crossValidatePredictor = eval([class(CommonHandles.RegressionPlane{idx}.Predictor) '();']);
    crossValidatePredictor = crossValidatePredictor.setParameters(CommonHandles.RegressionPlane{idx}.Predictor.getParameterValues());
    try
        predictions = crossValidate(nofFolds,crossValidatePredictor,mergedFeatures,mergedTargets,1);
    catch e
        if strcmp(e.identifier, 'ACC:crossValidate')
            handles.toolbarRegressionPerformance.State = 'off';
            warndlg(e.message)
            return
        else
            rethrow(e)
        end
    end
    CommonHandles.RegressionPlane{idx}.CrossValidation.Predictions = predictions; 
     
    tempStruct.N = N;
    %Different error measures:
    %Root mean squared error
    tempStruct.RMSE = sqrt(mean(sum((mergedTargets - predictions).^2,1)));
    avgTargetPos = mean(mergedTargets,2);
    %Reletive Root Mean Squared Error
    tempStruct.RRMSE = sqrt(sum(  sum((mergedTargets - predictions).^2,1)  ) / sum(sum((mergedTargets - avgTargetPos).^2,1)));
    
    tempStruct.Predictor = class(CommonHandles.RegressionPlane{idx}.Predictor);
    if ~isfield(CommonHandles.RegressionPlane{idx}.CrossValidation,'PerformanceChange')        
        CommonHandles.RegressionPlane{idx}.CrossValidation.PerformanceChange = {tempStruct};
    else
        CommonHandles.RegressionPlane{idx}.CrossValidation.PerformanceChange{end+1} = tempStruct;
    end
    
    drawRegressionPlane(handles);
    
    %Open GUI
    regressionPerformance_GUI(...
        CommonHandles.RegressionPlane{idx}.CrossValidation.PerformanceChange,...
        predictions,...
        mergedTargets,...
        nofFolds,...
        imageName,...
        cellNumber);

else
    warndlg('Please train a predictor before using regression performance!','Trained predictor is missing');
end



% --------------------------------------------------------------------
function toolbarRegressionPerformance_OffCallback(~, ~, handles)
% hObject    handle to toolbarRegressionPerformance (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

drawRegressionPlane(handles);


% --- Executes on button press in zoomInButton.
function zoomInButton_Callback(hObject, eventdata, handles)
% hObject    handle to zoomInButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

regPlaneBoundaries = round(get(handles.axes1,'Position'));
pos = [regPlaneBoundaries(1)+regPlaneBoundaries(3)/2 regPlaneBoundaries(2)+regPlaneBoundaries(4)/2];
performViewZoom(pos,-1,handles);

% --- Executes on button press in zoomOutButton.
function zoomOutButton_Callback(hObject, eventdata, handles)
% hObject    handle to zoomOutButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

regPlaneBoundaries = round(get(handles.axes1,'Position'));
pos = [regPlaneBoundaries(1)+regPlaneBoundaries(3)/2 regPlaneBoundaries(2)+regPlaneBoundaries(4)/2];
performViewZoom(pos,1,handles);

% --- Executes on button press in fullViewButton.
function fullViewButton_Callback(~, ~, handles)
% hObject    handle to fullViewButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;
CommonHandles.RegressionPlane{idx}.XBounds = [0 1000];
CommonHandles.RegressionPlane{idx}.YBounds = [0 1000];

if CommonHandles.RegressionPlane{idx}.adaptiveCellSize
    CommonHandles.RegressionPlane{idx}.apparentPictureSize = CommonHandles.RegressionPlane{idx}.startOfAdaptionCellSize;
end
   
drawRegressionPlane(handles);



% --- Executes on button press in jumpToCell.
function jumpToCell_Callback(hObject, eventdata, handles)
% hObject    handle to jumpToCell (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

global CommonHandles;
selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
if size(selectedObject,2)==1
    if uData.prediction < 1 % in annotation mode
        currentCellInfo.CellNumber = CommonHandles.TrainingSet.CellNumber{selectedObject};
        currentCellInfo.OriginalImageName = CommonHandles.TrainingSet.OriginalImageName{selectedObject};
        currentCellInfo.ImageName = CommonHandles.TrainingSet.ImageName{selectedObject};
        tempPlateName = strsplit(currentCellInfo.ImageName, filesep);
        plateInd=find(ismember(CommonHandles.PlatesNames,tempPlateName{1}));
        currentCellInfo.PlateName = plateInd;
        jumpToCell(currentCellInfo,1);
    elseif uData.prediction == 1 % in prediction mode
        jumpToCell(uData.predictedIcons{selectedObject},1);
    elseif uData.prediction == 2 % in plot of plots mode
        % nothing happens in case of plot of plots
    end
end


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

    switch eventdata.Key
        case 'r' % Multiple selection
            if uData.prediction == 0 || uData.prediction == -1
                if ~isempty(eventdata.Modifier) && length(eventdata.Modifier)==1 && strcmp(eventdata.Modifier,'shift')           
                    color = 'r';
                else            
                    color = 'b';
                end        
                if ~isempty(CommonHandles.RegressionPlane{idx}.selectedAxes) && ishandle(CommonHandles.RegressionPlane{idx}.selectedAxes{end})
                    polygonHandle = drawpolygon(CommonHandles.RegressionPlane{idx}.selectedAxes{end},'Color',color);
                else
                    polygonHandle = drawpolygon(handles.axes1,'Color',color);
                end
                if ~ishandle(polygonHandle) || ( ishandle(polygonHandle) && isempty(polygonHandle.Position) ), return; end
                polyXcoords = polygonHandle.Position(:,1);
                polyYcoords = polygonHandle.Position(:,2);
                
                mergedClassVectors = reshape(cell2mat(CommonHandles.TrainingSet.Class),length(CommonHandles.Classes),[])';
                indicesInThisClass = find(mergedClassVectors(:,idx));

                XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
                YBounds = CommonHandles.RegressionPlane{idx}.YBounds;
                regPlaneBoundaries = round(get(handles.axes1,'Position'));
                RegPositions = cell2mat(CommonHandles.TrainingSet.RegPos(indicesInThisClass)');

                [guiImgPositionX,guiImgPositionY] = regPlane2GUI(RegPositions(:,1),RegPositions(:,2),XBounds,YBounds,regPlaneBoundaries);
                toBeSelected = inpolygon(guiImgPositionX,guiImgPositionY,polyXcoords,polyYcoords);
                toBeSelectedIdx = indicesInThisClass(toBeSelected);

                if strcmp(color,'b')
                    CommonHandles.RegressionPlane{idx}.selectedObject = union(CommonHandles.RegressionPlane{idx}.selectedObject,toBeSelectedIdx);
                elseif strcmp(color,'r')
                    CommonHandles.RegressionPlane{idx}.selectedObject = setdiff(CommonHandles.RegressionPlane{idx}.selectedObject,toBeSelectedIdx);
                end
                delete(polygonHandle);
                refreshSelectedObjectPictures(idx,uData);
                refreshSelectedObjectInfoData();
                drawRegressionPlane(handles);
                %plotNavigationMap(idx,uData,handles);
            elseif uData.prediction == 1
                color = 'g';
                polygonHandle = drawpolygon(handles.axes1,'Color',color);
                if ~ishandle(polygonHandle) || ( ishandle(polygonHandle) && isempty(polygonHandle.Position) ), return; end
                binaryMask = polygonHandle.createMask;
                selectedImSegment = CommonHandles.RegressionPlane{idx}.whichObjectThere(binaryMask);
                selectedObject = unique(selectedImSegment);
                CommonHandles.RegressionPlane{idx}.selectedObject = selectedObject(selectedObject~=0);
                'Region selected'
                delete(polygonHandle);
            end
        case 'p' % print screen
            screenshot_ClickedCallback([],[],handles);
            
        case 'e' % export csv for BIAS
            "test"
            
%             switch dataType
%                 case 'directSpace'
%                     [jump2CellStructs,j2CFiltered,maxClustID] = clusterM.clusterCells( featuresMatrixNormalized, jump2CellStructs);
%                 case 'PCAreducedSpace'
%                     [jump2CellStructs,j2CFiltered,maxClustID] = clusterM.clusterCells( pcaReduced, jump2CellStructs);
%             end
%             jump2CellStructs
%             [sortByFilesMap] = sortCellsByFileName(jump2CellStructs);
%             writeAllClustersToFiles(targetDir,sortByFilesMap,maxClustID,perImgTargetDir)
%             
            targetDir = uigetdir(CommonHandles.DirName,'Select a folder to save the representative elements');
            
            selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
            jump2CellStructs = cell(numel(selectedObject), 1);
            if uData.prediction < 1 % in annotation mode
                j = 0;
                for i = selectedObject
                    j = j + 1;
                    currentCellInfo.CellNumber = CommonHandles.TrainingSet.CellNumber{i};
                    currentCellInfo.OriginalImageName = CommonHandles.TrainingSet.OriginalImageName{i};
                    currentCellInfo.ImageName = CommonHandles.TrainingSet.ImageName{i};
                    tempPlateName = strsplit(currentCellInfo.ImageName, filesep);
                    plateInd=find(ismember(CommonHandles.PlatesNames,tempPlateName{1}));
                    currentCellInfo.PlateName = plateInd;
                    currentCellInfo.clusterIdx = 1;
                    jump2CellStructs{j, 1} = currentCellInfo;
                end
            elseif uData.prediction == 1 % in prediction mode
                for i = 1:length(selectedObject)
                    tempStruct = uData.predictedIcons{selectedObject(i)};
                    tempStruct.clusterIdx = 1;
                    jump2CellStructs{i, 1} = rmfield(tempStruct,{'Image', 'ClassName', 'Features'});
                end
            elseif uData.prediction == 2 % in plot of plots mode
                % nothing happens in case of plot of plots
            end
            sortByFilesMap = sortCellsByFileName(jump2CellStructs);
            writeAllClustersToFiles(targetDir,sortByFilesMap,1)
            
        case 'b' % Background
            
            answer = questdlg('Would you like to add a new background or reset to blanc black?','Background',...
                'Add','Remove','Cancel','Cancel');
            if strcmp(answer,'Add')
                [file, path] = uigetfile({'*.png';'*.jpg';'*.tif';'*.tiff';'*.bmp'},'Backgroun selection');
                
                if ischar(file)
                    tempBg = logical(imread(fullfile(path,file)));
                    bg = imresize(tempBg,[1000, 1000]);
                    CommonHandles.CustomRpBackground = bg;
                end
            elseif strcmp(answer,'Remove')
                CommonHandles.CustomRpBackground = [];
            end
            drawRegressionPlane(handles);
            
        case 't' % Trajectories
            disp('Trajectory highlight visualization test');
            selectedObject = CommonHandles.RegressionPlane{idx}.selectedObject;
            if numel(selectedObject) == 1
                tempPlateAndName = cellfun(@strsplit, CommonHandles.TrainingSet.OriginalImageName,repmat({{filesep, 'anal3'}},[length(CommonHandles.TrainingSet.OriginalImageName) 1]),'UniformOutput',false);
                plateAndName = vertcat(tempPlateAndName{:});
                cellNumber = CommonHandles.TrainingSet.CellNumber;
                relativeID = CommonHandles.TrainingSet.CellNumber{selectedObject};
                idxOfFrame = find(CommonHandles.Tracking.ObjectID == relativeID .* strcmp(CommonHandles.Tracking.ImageName,plateAndName{selectedObject,2}) .* strcmp(CommonHandles.Tracking.Plate,plateAndName{selectedObject,1}));
                trajID = CommonHandles.Tracking.TrackID(idxOfFrame);
                trajTable = CommonHandles.Tracking(CommonHandles.Tracking.TrackID == trajID,:);
                RP_objectIDs = zeros(size(trajTable,1),1);
                for i = 1:size(trajTable,1)
                    tempRP_id = find(vertcat(CommonHandles.TrainingSet.CellNumber{:}) == trajTable.ObjectID(i) .* strcmp(plateAndName(:,2),trajTable.ImageName(i)) .* strcmp(plateAndName(:,1),trajTable.Plate(i)));
                    if ~isempty(tempRP_id)
                        RP_objectIDs(i) = tempRP_id;
                    end
                end
                RP_objectIDs(RP_objectIDs == 0) = [];
                CommonHandles.RegressionPlane{idx}.selectedObject = RP_objectIDs;
                refreshSelectedObjectPictures(idx,uData);
                refreshSelectedObjectInfoData();
                drawRegressionPlane(handles);
            end
        case 'l' % List of trajectories
            if isfield(CommonHandles,'Tracking')
                disp('Trajectory list.');
                CommonHandles.TrainingSet.TrajIDs = cell(size(CommonHandles.TrainingSet.Class, 1),1);
                CommonHandles.TrainingSet.FrameIdx = zeros(size(CommonHandles.TrainingSet.Class, 1),1);
                tempPlateAndName = cellfun(@strsplit, CommonHandles.TrainingSet.OriginalImageName,repmat({{filesep, 'anal3'}},[length(CommonHandles.TrainingSet.OriginalImageName) 1]),'UniformOutput',false);
                plateAndName = vertcat(tempPlateAndName{:});
                for k = 1:size(CommonHandles.TrainingSet.Class, 1)
                    if isempty(CommonHandles.TrainingSet.TrajIDs{k,1})
                        relativeID = CommonHandles.TrainingSet.CellNumber{k};
                        idxOfFrame = find(CommonHandles.Tracking.ObjectID == relativeID .* strcmp(CommonHandles.Tracking.ImageName,plateAndName{k,2}) .* strcmp(CommonHandles.Tracking.Plate,plateAndName{k,1}));
                        trajID = CommonHandles.Tracking.TrackID(idxOfFrame);
                        trajTable = CommonHandles.Tracking(CommonHandles.Tracking.TrackID == trajID,:);
                        RP_objectIDs = zeros(size(trajTable,1),2);
                        for i = 1:size(trajTable,1)
                            tempRP_id = find(vertcat(CommonHandles.TrainingSet.CellNumber{:}) == trajTable.ObjectID(i) .*... % cell has a same ID on the image
                                                    strcmp(plateAndName(:,2),trajTable.ImageName(i)) .*... % it is on the same image
                                                    strcmp(plateAndName(:,1),trajTable.Plate(i))); % it is on the same plate
                            if ~isempty(tempRP_id)
                                RP_objectIDs(i, 1) = tempRP_id;
                                RP_objectIDs(i, 2) = trajTable.Frame(i);
                            end
                        end
                        RP_objectIDs(RP_objectIDs(:,1) == 0,:) = [];
                        CommonHandles.TrainingSet.TrajIDs(RP_objectIDs(:,1),1) = {trajID};
                        CommonHandles.TrainingSet.FrameIdx(RP_objectIDs(:,1),1) = RP_objectIDs(:,2);
                    end
                end
                disp('Traj matching is done.')
                RPtrajList;
            end
        otherwise
    end
    
    if ismember(eventdata.Key,{'uparrow','downarrow','leftarrow','rightarrow'})
        regPlaneBoundaries = round(get(handles.axes1,'Position'));
        XBounds = CommonHandles.RegressionPlane{idx}.XBounds;
        YBounds = CommonHandles.RegressionPlane{idx}.YBounds;
        
        dragSize = 100;
        if strcmp(eventdata.Key,'uparrow'), posDiff = [0,-dragSize]; end
        if strcmp(eventdata.Key,'downarrow'), posDiff = [0,dragSize]; end
        if strcmp(eventdata.Key,'leftarrow'), posDiff = [dragSize,0]; end
        if strcmp(eventdata.Key,'rightarrow'), posDiff = [-dragSize,0]; end
        [XBounds,YBounds] = calcRegPlaneBoundsFromShift(posDiff,XBounds,YBounds,regPlaneBoundaries);
        CommonHandles.RegressionPlane{idx}.XBounds = XBounds;
        CommonHandles.RegressionPlane{idx}.YBounds = YBounds;
        
        drawRegressionPlane(handles);
    end


% --------------------------------------------------------------------
function toolbarPlotTrajectories_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarPlotTrajectories (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;
uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

if uData.prediction == 1
    CommonHandles.RegressionPlane{idx}.trajGUIhandle = openTrajectoryGUI(true);
elseif uData.prediction == 0 || uData.prediction == -1
    CommonHandles.RegressionPlane{idx}.trajGUIhandle = openTrajectoryGUI(false);
end

% uData = set(handles.figure1,'UserData',uData);


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
global CommonHandles;
if isempty(CommonHandles)
    delete(hObject);
    return
else
    if isfield(CommonHandles,'colorFrameHandle') && ~isempty(CommonHandles.colorFrameHandle) && ishandle(CommonHandles.colorFrameHandle)
        close(CommonHandles.colorFrameHandle);
    end
    
    if isfield(CommonHandles,'trainReg_GUI') && ~isempty(CommonHandles.trainReg_GUI) && ishandle(CommonHandles.trainReg_GUI)
        close(CommonHandles.trainReg_GUI);
    end
    
    if isfield(CommonHandles,'PredictRegressionGUI') && ~isempty(CommonHandles.PredictRegressionGUI) && ishandle(CommonHandles.PredictRegressionGUI)
        close(CommonHandles.PredictRegressionGUI);
    end
    delete(hObject);
end



% --- Executes on selection change in listbox_WellAnnotation.
function listbox_WellAnnotation_Callback(hObject, eventdata, handles)
% hObject    handle to listbox_WellAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox_WellAnnotation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox_WellAnnotation

uData = get(handles.figure1,'UserData');
borderSize = 3;

if uData.prediction == 2 %sanity check, do only in annotation mode
    simplyDrugs = cellstr(get(hObject,'String'));
    selectedIdx = get(hObject,'Value');
    
    selectedDrugs = simplyDrugs(selectedIdx);
    for i=1:numel(uData.predictedIcons)
        if contains(uData.predictedIcons{i}.drugInfo,selectedDrugs)
            if ~uData.predictedIcons{i}.highlighted                
                origImg = uData.predictedIcons{i}.Image;                
                colorIdx = selectedIdx(strcmp(uData.predictedIcons{i}.drugInfo(1:end-6),selectedDrugs));
                bgColor = getManyColor(colorIdx)./255;
                uData.predictedIcons{i}.Image = repmat(reshape(bgColor,1,1,3),size(origImg,1)+2*borderSize,size(origImg,2)+2*borderSize,1);
                uData.predictedIcons{i}.Image(borderSize+1:end-borderSize,borderSize+1:end-borderSize,:) = origImg;
                uData.predictedIcons{i}.highlighted = true;
            end
        else
            if uData.predictedIcons{i}.highlighted                
                uData.predictedIcons{i}.Image = uData.predictedIcons{i}.Image(borderSize+1:end-borderSize,borderSize+1:end-borderSize,:);
                uData.predictedIcons{i}.highlighted = false;
            end
        end
    end
    set(handles.figure1,'UserData',uData);
    drawRegressionPlane(handles);
end

% --- Executes during object creation, after setting all properties.
function listbox_WellAnnotation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox_WellAnnotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function toolbarColorFrame_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to toolbarColorFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

if isfield(CommonHandles,'colorFrameHandle') && ~isempty(CommonHandles.colorFrameHandle) && ishandle(CommonHandles.colorFrameHandle)
    CommonHandles.colorFrameHandle.Visible = 'off';
    pause(0.2)
    CommonHandles.colorFrameHandle.Visible = 'on';
    pause(0.2)
    return
end

featureNameFile = fullfile(CommonHandles.DirName, CommonHandles.PlatesNames{CommonHandles.SelectedPlate}, CommonHandles.MetaDataFolder, 'featureNames.acc');
featureNameList = textread(featureNameFile, '%s', 'delimiter', '\n');

cF_output = colorFrame(featureNameList(3:end), uData);
CommonHandles.colorFrameHandle = [];

if ~isempty(cF_output)
    if CommonHandles.RegressionPlane{idx}.colorFrame.featureID ~= cF_output.SelectedFeature || ...
            CommonHandles.RegressionPlane{idx}.colorFrame.lo ~= cF_output.loKnobValue || ...
            CommonHandles.RegressionPlane{idx}.colorFrame.hi ~= cF_output.hiKnobValue || ...
            ~strcmp(CommonHandles.RegressionPlane{idx}.colorFrame.scaleType,cF_output.scaleType)
        CommonHandles.RegressionPlane{idx}.colorFrame.calcNew = 1;
    end
    CommonHandles.RegressionPlane{idx}.colorFrame.featureID = cF_output.SelectedFeature;
    CommonHandles.RegressionPlane{idx}.colorFrame.lo = cF_output.loKnobValue;
    CommonHandles.RegressionPlane{idx}.colorFrame.hi = cF_output.hiKnobValue;
    CommonHandles.RegressionPlane{idx}.colorFrame.scaleType = cF_output.scaleType;
    CommonHandles.RegressionPlane{idx}.colorFrame.state = 1;
    set(handles.closeColorFrameButton, 'Visible', 'on');
    drawRegressionPlane(handles);
end

% --- Executes on button press in closeColorFrameButton.
function closeColorFrameButton_Callback(hObject, eventdata, handles)
% hObject    handle to closeColorFrameButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global CommonHandles;

uData = get(handles.figure1,'UserData');
idx = uData.classIndex;

CommonHandles.RegressionPlane{idx}.colorFrame.state = 0;
set(handles.closeColorFrameButton, 'Visible', 'off');
drawRegressionPlane(handles);


% --------------------------------------------------------------------
function TrajColorView_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to TrajColorView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Models_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Models (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
uData = get(handles.figure1,'UserData');
uiwait(modelManagement(uData))
